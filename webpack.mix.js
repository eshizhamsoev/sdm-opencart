// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');

const mix = require('laravel-mix');
// require('laravel-mix-alias')
require('laravel-mix-svg-sprite');

mix
  .alias({
    '@': 'assets',
  })
  .setResourceRoot('/assets')
  .setPublicPath('public/assets')
  .svgSprite('assets/svg/', 'svg/sprite.svg')
  // .webpackConfig({
  //   plugins: [
  //     new webpack.ProvidePlugin({
  //       $: 'jquery',
  //       jQuery: 'jquery',
  //     }),
  //   ],
  // })
  // .copy('src/img', 'public/assets/img')
  .sass('assets/main.scss', 'css/')
  .js('assets/main.js', 'js/')
  .js('assets/for-simple-checkout-page.js', 'js/')
  .js('assets/top.js', 'js/')
  .js('assets/page-with-filter.js', 'js/')
  .sourceMaps(false)
  .version();

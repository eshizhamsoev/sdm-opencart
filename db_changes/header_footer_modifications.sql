/* delete header.twig modifications */
UPDATE `oc_modification` SET `xml` = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                                  <modification>
                                    <name>OCFilter Modification</name>
                                    <code>ocfilter-product-filter</code>
                                    <version>4.7.5</version>
                                    <author>Aleksandr Surutkovich</author>
                                    <link>http://ocfilter.com</link>

                                    <!-- CONTROLLER -->
                                  	<file path=\"admin/controller/catalog/product.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[function getForm() {]]></search>
                                        <add position=\"after\"><![CDATA[
                                      // OCFilter start
                                      $this->document->addStyle('view/stylesheet/ocfilter/ocfilter.css');
                                      $this->document->addScript('view/javascript/ocfilter/ocfilter.js');
                                      // OCFilter end
                                        ]]></add>
                                      </operation>
                                      <operation error=\"skip\">
                                        <search><![CDATA[$this->language->get('tab_general');]]></search>
                                        <add position=\"after\"><![CDATA[
                                      // OCFilter start
                                      $data['tab_ocfilter'] = $this->language->get('tab_ocfilter');
                                      $data['entry_values'] = $this->language->get('entry_values');
                                      $data['ocfilter_select_category'] = $this->language->get('ocfilter_select_category');
                                      // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"admin/controller/common/column_left.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[if ($this->user->hasPermission('access', 'catalog/filter')) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                        // OCFilter start
                                  			$ocfilter = array();

                                  			if ($this->user->hasPermission('access', 'extension/module/ocfilter')) {
                                  				$ocfilter[] = array(
                                  					'name'     => $this->language->get('text_ocfilter_option'),
                                  					'href'     => $this->url->link('extension/module/ocfilter/filter', 'user_token=' . $this->session->data['user_token'], true),
                                  					'children' => array()
                                  				);
                                  			}

                                  			if ($this->user->hasPermission('access', 'extension/module/ocfilter')) {
                                  				$ocfilter[] = array(
                                  					'name'	   => $this->language->get('text_ocfilter_page'),
                                  					'href'     => $this->url->link('extension/module/ocfilter/page', 'user_token=' . $this->session->data['user_token'], true),
                                  					'children' => array()
                                  				);
                                  			}

                                  			if ($ocfilter) {
                                  				$catalog[] = array(
                                  					'name'	   => $this->language->get('text_ocfilter'),
                                  					'href'     => '',
                                  					'children' => $ocfilter
                                  				);
                                  			}
                                  		  // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file><!-- /admin/controller/common/column_left.php -->
                                    <!-- /CONTROLLER -->

                                    <!-- LANGUAGE -->
                                  	<file path=\"admin/language/{en}*/catalog/product.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$_['text_success']]]></search>
                                        <add position=\"before\"><![CDATA[
                                  // OCFilter start
                                  $_['entry_values']          		= 'Add the values ​​for this option.';
                                  $_['tab_ocfilter']          		= 'OCFilter Options';
                                  $_['ocfilter_select_category'] 	= 'To start, select a category for this product.';
                                  // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"admin/language/{ru}*/catalog/product.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$_['text_success']]]></search>
                                        <add position=\"before\"><![CDATA[
                                  // OCFilter start
                                  $_['entry_values']          		= 'Добавьте значения для этой опции.';
                                  $_['tab_ocfilter']          		= 'Опции фильтра';
                                  $_['ocfilter_select_category'] 	= 'Для начала, выберите категории для этого товара.';
                                  // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"admin/language/{en}*/common/column_left.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$_['text_option']]]></search>
                                        <add position=\"before\"><![CDATA[
                                  // OCFilter start
                                  $_['text_ocfilter']                    = 'OCFilter';
                                  $_['text_ocfilter_option']             = 'Filters';
                                  $_['text_ocfilter_page']               = 'SEO Pages';
                                  $_['text_ocfilter_setting']            = 'Settings';
                                  // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"admin/language/{ru}*/common/column_left.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$_['text_option']]]></search>
                                        <add position=\"before\"><![CDATA[
                                  // OCFilter start
                                  $_['text_ocfilter']                    = 'OCFilter';
                                  $_['text_ocfilter_option']             = 'Фильтры';
                                  $_['text_ocfilter_page']               = 'Страницы';
                                  $_['text_ocfilter_setting']            = 'Настройки';
                                  // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                    <!-- /LANGUAGE -->

                                    <!-- MODEL -->
                                  	<file path=\"admin/model/catalog/product.php\">
                                      <operation error=\"skip\">
                                        <search index=\"0\"><![CDATA[if (isset($data['product_recurring'])) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                      // OCFilter start
                                  		if (isset($data['ocfilter_product_option'])) {
                                  			foreach ($data['ocfilter_product_option'] as $option_id => $values) {
                                  				foreach ($values['values'] as $value_id => $value) {
                                  					if (isset($value['selected'])) {
                                  						$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"ocfilter_option_value_to_product SET product_id = '\" . (int)$product_id . \"', option_id = '\" . (int)$option_id . \"', value_id = '\" . (string)$value_id . \"', slide_value_min = '\" . (isset($value['slide_value_min']) ? (float)$value['slide_value_min'] : 0) . \"', slide_value_max = '\" . (isset($value['slide_value_max']) ? (float)$value['slide_value_max'] : 0) . \"'\");
                                  					}
                                  				}
                                  			}
                                  		}
                                  		// OCFilter end
                                        ]]></add>
                                      </operation>
                                      <operation error=\"skip\">
                                        <search index=\"1\"><![CDATA[if (isset($data['product_recurring'])) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                      // OCFilter start
                                      $this->db->query(\"DELETE FROM \" . DB_PREFIX . \"ocfilter_option_value_to_product WHERE product_id = '\" . (int)$product_id . \"'\");

                                  		if (isset($data['ocfilter_product_option'])) {
                                  			foreach ($data['ocfilter_product_option'] as $option_id => $values) {
                                  				foreach ($values['values'] as $value_id => $value) {
                                  					if (isset($value['selected'])) {
                                  						$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"ocfilter_option_value_to_product SET product_id = '\" . (int)$product_id . \"', option_id = '\" . (int)$option_id . \"', value_id = '\" . (string)$value_id . \"', slide_value_min = '\" . (isset($value['slide_value_min']) ? (float)$value['slide_value_min'] : 0) . \"', slide_value_max = '\" . (isset($value['slide_value_max']) ? (float)$value['slide_value_max'] : 0) . \"'\");
                                  					}
                                  				}
                                  			}
                                  		}
                                  		// OCFilter end
                                        ]]></add>
                                      </operation>
                                      <operation error=\"skip\">
                                        <search><![CDATA[$data['product_attribute'] = $this->getProductAttributes($product_id);]]></search>
                                        <add position=\"after\"><![CDATA[
                                   		// OCFilter start
                                  		$this->load->model('catalog/ocfilter');

                                  		$data['ocfilter_product_option'] = $this->model_catalog_ocfilter->getProductOCFilterValues($product_id);
                                  		// OCFilter end
                                        ]]></add>
                                      </operation>
                                      <operation error=\"skip\">
                                        <search><![CDATA[$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"product WHERE product_id = '\" . (int)$product_id . \"'\");]]></search>
                                        <add position=\"after\"><![CDATA[
                                  		// OCFilter start
                                  		$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"ocfilter_option_value_to_product WHERE product_id = '\" . (int)$product_id . \"'\");
                                  		// OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file><!-- /admin/model/catalog/product.php -->

                                    <!-- /MODEL -->

                                    <!-- VIEW -->
                                  	<file path=\"admin/view/template/catalog/product_form.twig\">
                                      <operation error=\"skip\">
                                        <search index=\"0\"><![CDATA[</script></div>]]></search>
                                        <add position=\"replace\"><![CDATA[
                                    </script>
                                    <!-- OCFilter start -->
                                    <script type=\"text/javascript\"><!--
                                    ocfilter.php = {
                                    	text_select: '{{ text_select }}',
                                    	ocfilter_select_category: '{{ ocfilter_select_category }}',
                                    	entry_values: '{{ entry_values }}',
                                    	tab_ocfilter: '{{ tab_ocfilter }}'
                                    };

                                    ocfilter.php.languages = [];

                                    {% for language in languages %}
                                    ocfilter.php.languages.push({
                                    	'language_id': {{ language.language_id }},
                                    	'name': '{{ language.name }}',
                                      'image': '{{ language.image }}'
                                    });
                                    {% endfor %}
                                    //--></script>
                                    <!-- OCFilter end -->
                                    </div>
                                        ]]></add>
                                      </operation>
                                    </file><!-- /admin/view/template/catalog/product_form.twig -->
                                    <!-- /VIEW -->

                                    <!-- CATALOG -->

                                    <file path=\"catalog/controller/startup/startup.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[Cart($this->registry));]]></search>
                                        <add position=\"after\"><![CDATA[
                                  		// OCFilter
                                  		$this->registry->set('ocfilter', new OCFilter($this->registry));
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"catalog/controller/startup/seo_url.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$this->url->addRewrite($this);]]></search>
                                        <add position=\"after\"><![CDATA[
                                        // OCFilter start
                                        if ($this->registry->has('ocfilter')) {
                                    			$this->url->addRewrite($this->ocfilter);
                                    		}
                                        // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"catalog/controller/{common,startup}/seo_pro.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$this->url->addRewrite($this);]]></search>
                                        <add position=\"after\"><![CDATA[
                                        // OCFilter start
                                        if ($this->registry->has('ocfilter')) {
                                    			$this->url->addRewrite($this->ocfilter);
                                    		}
                                        // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"catalog/controller/{common,startup}/seo_pro.php\">
                                      <operation error=\"skip\">
                                        <search><![CDATA[$this->url->addRewrite($this, $lang_data);]]></search>
                                        <add position=\"after\"><![CDATA[
                                        // OCFilter start
                                        if ($this->registry->has('ocfilter')) {
                                    			$this->url->addRewrite($this->ocfilter);
                                    		}
                                        // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"catalog/controller/product/category.php\">
                                      <operation error=\"abort\">
                                        <search index=\"0\"><![CDATA[$data['breadcrumbs'] = array();]]></search>
                                        <add position=\"before\"><![CDATA[
                                  		// OCFilter start
                                      if (isset($this->request->get['filter_ocfilter'])) {
                                        $filter_ocfilter = $this->request->get['filter_ocfilter'];
                                      } else {
                                        $filter_ocfilter = '';
                                      }
                                  		// OCFilter end
                                        ]]></add>
                                      </operation>

                                      <!-- Filter params to product model -->

                                      <operation error=\"abort\">
                                        <search><![CDATA[$product_total =]]></search>
                                        <add position=\"before\"><![CDATA[
                                    		// OCFilter start
                                    		$filter_data['filter_ocfilter'] = $filter_ocfilter;
                                    		// OCFilter end
                                        ]]></add>
                                      </operation>

                                      <!-- Add url -->

                                      <operation error=\"skip\">
                                        <search index=\"2\"><![CDATA[if (isset($this->request->get['filter'])) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                        // OCFilter start
                                  			if (isset($this->request->get['filter_ocfilter'])) {
                                  				$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
                                  			}
                                        // OCFilter end
                                        ]]></add>
                                      </operation>

                                      <operation error=\"skip\">
                                        <search index=\"3\"><![CDATA[if (isset($this->request->get['filter'])) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                        // OCFilter start
                                  			if (isset($this->request->get['filter_ocfilter'])) {
                                  				$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
                                  			}
                                        // OCFilter end
                                        ]]></add>
                                      </operation>

                                      <operation error=\"skip\">
                                        <search index=\"4\"><![CDATA[if (isset($this->request->get['filter'])) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                        // OCFilter start
                                  			if (isset($this->request->get['filter_ocfilter'])) {
                                  				$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
                                  			}
                                        // OCFilter end
                                        ]]></add>
                                      </operation>

                                      <operation error=\"skip\">
                                        <search limit=\"1\"><![CDATA[$data['limit'] = $limit;]]></search>
                                        <add position=\"after\"><![CDATA[
                                        // OCFilter Start
                                        if ($this->ocfilter->getParams()) {
                                          if (isset($product_total) && !$product_total) {
                                        	  $this->response->redirect($this->url->link('product/category', 'path=' . $this->request->get['path']));
                                          }

                                          $this->document->setTitle($this->ocfilter->getPageMetaTitle($this->document->getTitle()));
                                  			  $this->document->setDescription($this->ocfilter->getPageMetaDescription($this->document->getDescription()));
                                          $this->document->setKeywords($this->ocfilter->getPageMetaKeywords($this->document->getKeywords()));

                                          $data['heading_title'] = $this->ocfilter->getPageHeadingTitle($data['heading_title']);
                                          $data['description'] = $this->ocfilter->getPageDescription();

                                          if (!trim(strip_tags(html_entity_decode($data['description'], ENT_QUOTES, 'UTF-8')))) {
                                          	$data['thumb'] = '';
                                          }

                                          $breadcrumb = $this->ocfilter->getPageBreadCrumb();

                                          if ($breadcrumb) {
                                    			  $data['breadcrumbs'][] = $breadcrumb;
                                          }

                                          $this->document->deleteLink('canonical');
                                        }
                                        // OCFilter End
                                        ]]></add>
                                      </operation>
                                    </file><!-- /catalog/controller/product/category.php -->

                                    <!-- Document Noindex & Canonical -->

                                  	<file path=\"system/library/document.php\">
                                      <operation error=\"abort\">
                                        <search index=\"0\"><![CDATA[public function getLinks]]></search>
                                        <add position=\"before\"><![CDATA[
                                    // OCFilter canonical fix start
                                  	public function deleteLink($rel) {
                                      foreach ($this->links as $href => $link) {
                                        if ($link['rel'] == $rel) {
                                        	unset($this->links[$href]);
                                        }
                                      }
                                  	}
                                    // OCFilter canonical fix end
                                        ]]></add>
                                      </operation>

                                      <operation error=\"abort\">
                                        <search><![CDATA[private $keywords;]]></search>
                                        <add position=\"after\"><![CDATA[
                                    // OCFilter start
                                    private $noindex = false;
                                    // OCFilter end
                                        ]]></add>
                                      </operation>

                                      <operation error=\"abort\">
                                        <search><![CDATA[public function setTitle($title) {]]></search>
                                        <add position=\"before\"><![CDATA[
                                    // OCFilter start
                                    public function setNoindex($state = false) {
                                    	$this->noindex = $state;
                                    }

                                  	public function isNoindex() {
                                  		return $this->noindex;
                                  	}
                                    // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>

                                  	<file path=\"catalog/controller/common/header.php\">
                                      <operation error=\"abort\">
                                        <search><![CDATA[$data['scripts'] = $this->document->getScripts]]></search>
                                        <add position=\"before\"><![CDATA[
                                      // OCFilter start
                                      $data['noindex'] = $this->document->isNoindex();
                                      // OCFilter end
                                        ]]></add>
                                      </operation>
                                    </file>
                                  </modification>
" WHERE `code` = "ocfilter-product-filter";

UPDATE `oc_modification` SET `xml` = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<modification>
  <name><![CDATA[<b><font color=\"#33b\" size=\"2\">dadata + yamap 4 simple]]></name>
	<version>oc_3.0.2.0 v2</version>
	<link>https://wisesol.ru</link>
  <author><![CDATA[<b><font color=\"#f25\" size=\"4\">W<font color=\"#33b\">S]]></author>
	<id>dadata+yamap4simple</id>
	<code>dadayama-for-simple</code>

	<file path=\"catalog/controller/checkout/simplecheckout.php\">
		<operation error=\"log\">
			<search><![CDATA[$this->loadLibrary('simple/simplecheckout');]]></search>
			<add position=\"after\"><![CDATA[				//villarasa
    $this->document->addStyle('https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css', 'stylesheet', 'all');
$this->document->addScript('https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js');
				//villarasa.
]]></add>
    </operation>
    <operation error=\"log\">
    <search><![CDATA[$this->simplecheckout->clearOrder();]]></search>
			<add position=\"after\"><![CDATA[				//villarasa
    $da_distance = floatval($this->request->cookie['da_distance'] ?? null);
$da_delivery = floatval($this->request->cookie['da_delivery'] ?? null);
$da_total_shipping_delivery = floatval($this->request->cookie['da_total_shipping_delivery'] ?? null);
				//villarasa.
]]></add>
    </operation>
    <operation error=\"log\">
    <search><![CDATA[array_multisort($sort_order, SORT_ASC, $totals);]]></search>
			<add position=\"after\"><![CDATA[				if ($this->request->cookie['da_delivery'] ?? null) {
    for ($i=0; $i < count($totals); $i++) {
    if ($totals[$i]['code'] == 'shipping') {
    $totals[$i]['value'] = $totals[$i]['value'] + $da_delivery;
}
						if ($totals[$i]['code'] == 'total') {
							$totals[$i]['value'] = $totals[$i]['value'] + $da_delivery;
}
					}
				}
				//villarasa.
			]]></add>
    </operation>
    <operation error=\"log\">
    <search><![CDATA[$data['comment'] = $comment;]]></search>
			<add position=\"replace\"><![CDATA[//villarasa
    $delim = $comment == '' ? '' : '\\n';
$data['comment'] = $da_distance > 0 ? $comment . $delim . ' Доставка ' . $da_total_shipping_delivery . ' руб. В том числе за МКАД ' . $da_delivery . ' руб. (' . $da_distance . ' км * 50 руб.)' : $comment;]]></add>
		</operation>
		<operation error=\"log\">
			<search><![CDATA[$data['total'] = $total;]]></search>
			<add position=\"replace\"><![CDATA[$data['total'] = $total + $da_delivery;
//villarasa.]]></add>
</operation>
	</file>

	<file path=\"catalog/controller/common/header.php\">
		<operation>
			<search><![CDATA[$data['title'] = $this->document->getTitle();]]></search>
			<add position=\"before\"><![CDATA[if (isset($this->request->get['route'])) {$data['route'] = explode('/',$this->request->get['route'])[0];}]]></add>
		</operation>
	</file>
</modification>" WHERE `code` = "dadayama-for-simple";

UPDATE `oc_modification` SET `xml` = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<modification>
    <name>Авторизация через соц.сети: ВК, ФБ, ОК, ТВ, Mail.ru, Google</name>
    <code>socnetauth2</code>
    <version>1.0</version>
    <author>kin208</author>
    <link>http://softpodkluch.ru/socnetauth2</link>


    <file path=\"catalog/view/theme/*/template/checkout/newstorecheckout.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
				</form>
            ]]></search>
            <add position=\"before\"><![CDATA[
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/controller/checkout/newstorecheckout.php\">
        <operation error=\"skip\">
            <search><![CDATA[
				$this->response->setOutput($this->load->view('checkout/newstorecheckout', $data));
            ]]></search>
            <add position=\"before\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA_BUTTONS'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1, \"str\" => \"checkout\") );
			/* end socnetauth2 metka */
            ]]></add>
        </operation>
    </file>


    <file path=\"catalog/view/theme/*/template/extension/module/uni_register.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
				register_button
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
            ]]></add>
        </operation>
        <operation error=\"skip\">
            <search><![CDATA[
				register__btn
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/view/theme/*/template/extension/module/uni_login.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
				login_button
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
            ]]></add>
        </operation>
        <operation error=\"skip\">
            <search><![CDATA[
				login__btn
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/controller/extension/module/uni_login_register.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA_BUTTONS'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1, \"str\" => \"account\") );
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/controller/checkout/simplecheckout_login.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$this->_templateData['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1, \"str\" => \"account\"));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/controller/checkout/simplecheckout_customer.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$this->_templateData['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/view/theme/*/template/checkout/simplecheckout_login.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
				simplecheckout_button_login
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA['code'] }}
            ]]></add>
        </operation>
    </file>

	<file path=\"catalog/view/theme/oct_ultrastore/template/octemplates/module/oct_popup_login.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
				<button id=\"popup-login-button\"
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA['code'] }}
            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/controller/octemplates/module/oct_popup_login.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1, \"str\" => \"account\"));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/controller/account/simpleregister.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$this->_templateData['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/controller/checkout/login.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/controller/account/login.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/controller/account/register.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1));
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>



    <file path=\"catalog/view/theme/*/template/checkout/login.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
				button-login
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA['code'] }}
            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/view/theme/*/template/account/login.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
            </form>
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA['code'] }}

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/view/theme/*/template/account/register.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
            already
            ]]></search>
            <add position=\"after\"><![CDATA[
				{{ SOCNETAUTH2_DATA['code'] }}

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/view/theme/*/template/account/simpleregister.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
            already
            ]]></search>
            <add position=\"after\"><![CDATA[

				{{ SOCNETAUTH2_DATA['code'] }}

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/view/theme/*/template/checkout/simplecheckout_customer.twig\">
        <operation error=\"skip\">
            <search><![CDATA[
            <div class=\"simplecheckout-block-content\"
            ]]></search>
            <add position=\"after\"><![CDATA[

				{{ SOCNETAUTH2_DATA['code'] }}

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/controller/common/header.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[

            /* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode' );
			$data['SOCNETAUTH2_DATA_DOBOR'] = $this->load->controller('account/socnetauth2/showcode', array(\"dobor_only\"=>1) );
			$data['SOCNETAUTH2_DATA_BUTTONS'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1) );
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>
    <!--<file path=\"catalog/view/theme/*/template/common/header.twig\">

        <operation error=\"skip\">
            <search><![CDATA[
				login-help
            ]]></search>
            <add position=\"after\"><![CDATA[
			  <div>
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
			  </div><br>

            ]]></add>
        </operation>
        <operation error=\"skip\">
            <search><![CDATA[
				button-login-popup
            ]]></search>
            <add position=\"after\"><![CDATA[

			  <div>
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
			  </div><br>

            ]]></add>
        </operation>



    </file>
    <file path=\"catalog/view/theme/*/template/common/header.twig\">

        <operation error=\"skip\">
            <search><![CDATA[
				<body
            ]]></search>
            <add position=\"after\"><![CDATA[

				{{ SOCNETAUTH2_DATA_DOBOR['code'] }}

            ]]></add>
        </operation>

    </file>

    <file path=\"catalog/view/theme/*/template/common/header.twig\">

        <operation error=\"skip\">
            <search><![CDATA[
				<input type=\"button\" value=\"{{ button_login }}\" id=\"button-login-popup\" class=\"btn-login btn-block\" />
            ]]></search>
            <add position=\"after\"><![CDATA[

			  <div>
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
			  </div>

            ]]></add>
        </operation>


    </file> -->


    <file path=\"admin/controller/sale/order.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            sale/order_list
            ]]></search>
            <add position=\"before\"><![CDATA[
			/* start socnetauth2 */
			if( $this->config->get('module_socnetauth2_show_source_in_order') )
			{
				$this->load->model('extension/module/socnetauth2');
				foreach( $data['orders'] as $i=>$item )
				{
					$data['orders'][$i]['customer'] .= $this->model_extension_module_socnetauth2->getCustomerSocnetsByOrderId($item['order_id']);
				}
			}
			/* end socnetauth2 */
            ]]></add>
        </operation>
    </file>
    <file path=\"admin/controller/customer/customer.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            customer/customer_list
            ]]></search>
            <add position=\"before\"><![CDATA[
			/* start socnetauth2 */
			if( $this->config->get('module_socnetauth2_show_source_in_customer') )
			{
				$this->load->model('extension/module/socnetauth2');
				foreach( $data['customers'] as $i=>$item )
				{
					$data['customers'][$i]['name'] .= $this->model_extension_module_socnetauth2->getCustomerSocnets($item['customer_id']);
				}
			}
			/* end socnetauth2 */

            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/controller/common/header_login.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            common/header_login
            ]]></search>
            <add position=\"before\"><![CDATA[

			/* start socnetauth2 metka */
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode',
				array(\"buttons_only\"=>1, \"format\"=>\"lline\") );
			/* end socnetauth2 metka */

            ]]></add>
        </operation>
    </file>

    <file path=\"catalog/controller/journal2/checkout.php\">
        <operation error=\"skip\">
            <search><![CDATA[
            function index
            ]]></search>
            <add position=\"after\"><![CDATA[
			$this->data['SOCNETAUTH2_DATA_BUTTONS'] = $this->load->controller('account/socnetauth2/showcode', array(\"buttons_only\"=>1) );
			]]></add>
        </operation>
    </file>

    <file path=\"catalog/view/theme/journal2/template/journal2/checkout/checkout.twig\">

        <operation error=\"skip\">
            <search><![CDATA[
				</fieldset>
            ]]></search>
            <add position=\"before\"><![CDATA[
			  <div>
				{{ SOCNETAUTH2_DATA_BUTTONS['code'] }}
			  </div>

            ]]></add>
        </operation>


    </file>




</modification>" WHERE `code` = "socnetauth2";

/* delete footer.twig modifications */
UPDATE `oc_modification` SET `xml` = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<modification>
    <code>ProgRoman.CityManager</code>
    <name>ProgRoman - CityManager Pro</name>
    <version>8.0</version>
    <author>Roman Shipilov (ProgRoman)</author>
    <link>http://opencart.progroman.ru/demo/1/</link>

    <!-- Подключаем модуль -->
    <file path=\"catalog/controller/startup/startup.php\">
        <operation>
            <search index=\"0\"><![CDATA[$code = '';]]></search>
            <add position=\"after\"><![CDATA[
        $this->load->controller('extension/module/progroman/citymanager/startup');]]></add>
        </operation>
        <!-- Приоритет над группой зарегистрированного покупателя -->
        <operation>
            <search index=\"0\"><![CDATA[if ($this->customer->isLogged()) {]]></search>
            <add position=\"
replace
\"><![CDATA[if ($this->config->get('module_progroman_citymanager_status')
            && $this->progroman_citymanager->isCustomerGroupHighPriority()
            && ($prmn_customer_group_id = $this->progroman_citymanager->getCustomerGroupId())) {
            $this->config->set('config_customer_group_id', $prmn_customer_group_id);
        } elseif ($this->customer->isLogged()) {]]></add>
        </operation>
    </file>

    <!-- Загружаем стили и шаблон -->
    <!--<file path=\"
system
/
config
/
catalog
.
php
\" error=\"
skip
\">-->
    <!--    <operation error=\"
skip
\">-->
    <!--        <search><![CDATA['startup/maintenance',]]></search>-->
    <!--        <add position=\"
before
\"><![CDATA['extension/module/progroman/citymanager/load',]]></add>-->
    <!--    </operation>-->
    <!--</file>-->

    <!-- Не присваивать группу города новому клиенту при регистрации -->
    <file path=\"
catalog
/
model
/
account
/
customer
.
php
\">
        <operation>
            <search index=\"
0
\"><![CDATA[$customer_group_id = $this->config->get('config_customer_group_id');]]></search>
            <add position=\"
replace
\"><![CDATA[if ($this->config->get('module_progroman_citymanager_status') && $this->progroman_citymanager->isCustomerGroupForNewUser()
                && $this->progroman_citymanager->getCustomerGroupId() == $this->config->get('config_customer_group_id')) {
		        $customer_group_id = $this->progroman_citymanager->getDefaultCustomerGroupId();
            } else {
                $customer_group_id = $this->config->get('config_customer_group_id');
            }]]></add>
        </operation>
    </file>

    <!-- Добавляем вывод города при оформлении заказа с регистрацией -->
    <file path=\"
catalog
/
controller
/
checkout
/
register
.
php
|
catalog
/
controller
/
checkout
/
payment_address
.
php
|
catalog
/
controller
/
checkout
/
shipping_address
.
php
\">
        <operation>
            <search index=\"
0
\">
                <![CDATA[$this->load->model('localisation/country');]]>
            </search>
            <add position=\"
before
\">
                <![CDATA[
        if (isset($this->session->data['shipping_address']['city'])) {
            $data['city'] = $this->session->data['shipping_address']['city'];
        } else {
            $data['city'] = '';
        }
        ]]>
            </add>
        </operation>
    </file>
    <file path=\"
catalog
/
view
/
theme/*/template/checkout/register.twig|catalog/view/theme/*/template/checkout/payment_address.twig|catalog/view/theme/*/template/checkout/shipping_address.twig\" error=\"skip\">
        <operation error=\"skip\">
            <search>
                <![CDATA[<input type=\"text\" name=\"city\" value=\"\"]]>
            </search>
            <add position=\"replace\">
                <![CDATA[<input type=\"text\" name=\"city\" value=\"{{ city }}\"]]>
            </add>
        </operation>
    </file>

    <!-- Добавляем $progroman_citymanager в шаблоны -->
    <file path=\"system/engine/loader.php\">
        <operation>
            <search>
                <![CDATA[foreach ($data as $key => $value) {]]>
            </search>
            <add position=\"before\">
                <![CDATA[            $data['progroman_citymanager'] = $this->registry->get('progroman_citymanager');
			$data['prmn_cmngr'] = $this->registry->get('prmn_cmngr');
            ]]>
            </add>
        </operation>
    </file>

    <!-- Добавляем города в исходный код -->
    <file path=\"catalog/controller/common/footer.php\">
        <operation>
            <search>
                <![CDATA[$data['newsletter'] = $this->url->link('account/newsletter', '', true);]]>
            </search>
            <add position=\"after\">
                <![CDATA[
        if ($this->config->get('module_progroman_citymanager_status') && $this->progroman_citymanager->setting('cities_in_source')) {
            $data['prmn_cmngr_cities'] = $this->load->controller('extension/module/progroman/citymanager/cities');
        } else {
            $data['prmn_cmngr_cities'] = '';
        }]]>
            </add>
        </operation>
    </file>

    <!-- Интеграция с Simple -->
    <file path=\"catalog/model/tool/simplegeo.php\" error=\"skip\">
        <operation error=\"skip\">
            <search>
                <![CDATA[if ($mode == self::SIMPLE_GEO_OWN)]]>
            </search>
            <add position=\"replace\">
                <![CDATA[if ($this->progroman_citymanager && $this->progroman_citymanager->checkIntegrationSimple() && $this->progroman_citymanager->getCityName()) {
	            ModelToolSimpleGeo::$geo = $this->progroman_citymanager->getFullInfo();
            } elseif ($mode == self::SIMPLE_GEO_OWN)]]>
            </add>
        </operation>
        <operation error=\"skip\">
            <search>
                <![CDATA[$query = $this->db->query($sql);]]>
            </search>
            <add position=\"replace\">
                <![CDATA[if ($this->progroman_citymanager && $this->progroman_citymanager->checkIntegrationSimple()) {
	            $query = $this->progroman_citymanager->autocompleteForSimple($city, 25);
	            $geoip_used = true;
            } else {
                $query = $this->db->query($sql);
            }]]>
            </add>
        </operation>
    </file>

    <!-- Module \"Custom Quick Checkout\" -->
    <file path=\"catalog/controller/extension/module/d_quickcheckout.php\" error=\"skip\">
        <operation error=\"skip\">
            <search regex=\"true\">
                <![CDATA[#this->setSessionValue\('(country_id|zone_id|city|postcode)','(payment|shipping)_address', \$data, \$account, false#]]>
            </search>
            <add position=\"replace\">
                <![CDATA[this->setSessionValue('$1','$2_address', $data, $account, true]]>
            </add>
        </operation>
    </file>
</modification>" WHERE `code` = "ProgRoman.CityManager";

UPDATE `oc_modification` SET `xml` = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<modification>
    <name>X-Shipping Pro</name>
    <code>xshippingpro</code>
    <version>3.3.0</version>
    <author>OpenCartMart</author>
    <link>http://www.opencartmart.com</link>
       <file path=\"catalog/controller/extension/module/xtensions/checkout/xfooter.php\">
        <operation error=\"skip\">
            <ignoreif regex=\"true\"><![CDATA[ /ocm->getScript/ ]]></ignoreif>
            <search><![CDATA[ $this->load->model('catalog/information'); ]]></search>
            <add position=\"before\"><![CDATA[
                  $ocm = ($ocm = $this->registry->get('ocm_front')) ? $ocm : new OCM\\Front($this->registry);
                  $this->data['_ocm_script'] = $ocm->getScript();
            ]]></add>
        </operation>
    </file>
    <file path=\"catalog/view/theme/*/template/extension/module/xtensions/checkout/xfooter.twig\">
      <operation error=\"skip\">
        <ignoreif regex=\"true\"><![CDATA[ /_ocm_script/ ]]></ignoreif>
        <search><![CDATA[ </body> ]]></search>
        <add position=\"before\"><![CDATA[
            {{ _ocm_script }}
        ]]></add>
      </operation>
   </file>
    <file path=\"catalog/controller/common/footer.php\">
        <operation error=\"log\">
            <ignoreif regex=\"true\"><![CDATA[ /ocm->getScript/ ]]></ignoreif>
            <search index=\"0\"><![CDATA[ $this->load->model('catalog/information'); ]]></search>
            <add position=\"before\"><![CDATA[
                  $ocm = ($ocm = $this->registry->get('ocm_front')) ? $ocm : new OCM\\Front($this->registry);
                  $data['_ocm_script'] = $ocm->getScript();
            ]]></add>
        </operation>
    </file>
</modification>" WHERE `code` = "xshippingpro";

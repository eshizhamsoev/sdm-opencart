#!/usr/bin/env bash

echo "start build server..."
GitBr=$(grep ENV_BRANCH ./.env | cut -d '=' -f2)
GitUrl=$(grep ENV_GITURL ./.env | cut -d '=' -f2)
GitUser=$(grep ENV_GITU ./.env | cut -d '=' -f2)
GitPass=$(grep ENV_GITP ./.env | cut -d '=' -f2)

I=`git --version $1 | grep "version" `
if [ -n "$I" ]
then
    echo "Git installed"
else
    sudo apt update
    sudo apt install git -y
fi

I=`docker-compose -v $1 | grep "version" `
if [ -n "$I" ]
then
    echo "Docker installed"
else
    sudo apt update
    apt install apt-transport-https ca-certificates curl software-properties-common -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    apt update
    apt install docker-ce -y
    curl -L https://github.com/docker/compose/releases/download/1.26.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    apt install docker-compose -y
fi

mkdir public
git init .
git remote add -t \* -f origin https://$GitUser:$GitPass@$GitUrl
#git remote add -t \* -f origin https://v.kotov:%9nU8Xfdgrfbffgc@git.itsn.pro/sdm/sdm.git
git checkout $GitBr
chmod 777 build.sh
chmod 644 firstrun.sh
docker-compose up -d
echo "server builded"
sh deploy.sh
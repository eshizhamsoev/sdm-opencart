### **Репозиторий проекта sdmclimate.ru**

Для запуска локально необходимо запустить скрипт deploy.sh, предварительно переименовав 
.env.example в .env и внести туда данные конфигурационные.
Открывать локально по домену _sdm.localhost_ (что указан в .env)

PHPMyAdmin доступен по адресу http://sdm.localhost:8083/

Для запуска на пустом сервере скачайте в папку где будет расположены файлы сайта 
скрипт firstrun.sh, назначьте ему права 777, так же скачайте и поставьте .env с заполненными 
данными и после запустите скрипт firstrun.sh командой `sh firstrun.sh`

Далее для заливки обновлений на сервер или локально, запускайте скрипт 
deploy.sh командой `sh deploy.sh`

Получение информации по бонусам
-
Для получения данных по количеству бонусов у пользователя необходимо отправить запрос по адресу:
`./index.php?route=api/customer_bonus`

со следующими параметрами:

* email - email  пользователя или 

* user_id: ид пользователя

Модули для сайта
-
Ключ для модуля Simple для домена sdm.localhost:
TlFzNlF6N2pXT0dGSTM3dklFczVZaXJhcnplcW1pUFovOTFWZEtweXNVdHRIK0p1SmY2U0padmJkcnVwNDlkT21LVGU1aTBrcUtx­ZjUxVWhmRzF1OTNnU082M0ZZNGlTTDlRcjhNYjhWVlAxZFpnWXBIbU1kYUI2RjJhcEN6YkxWcUdGcVdOK1F3ZWZldzR0RFViWXZk­bDJQQjhHTjljOHhSVlp5YlhKSnVrPXwxMzMyMjc2MDA

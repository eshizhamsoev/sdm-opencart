#!/usr/bin/env bash
GitBr=$(grep ENV_BRANCH ./.env | cut -d '=' -f2)
GitUrl=$(grep ENV_GITURL ./.env | cut -d '=' -f2)
GitUser=$(grep ENV_GITU ./.env | cut -d '=' -f2)
GitPass=$(grep ENV_GITP ./.env | cut -d '=' -f2)
SiteUrl=$(grep ENV_HOSTNAME ./.env | cut -d '=' -f2)
SiteHttp=$(grep ENV_HTTP ./.env | cut -d '=' -f2)

git remote set-url origin https://$GitUser:$GitPass@$GitUrl
git config --global credential.helper 'cache --timeout=3600'
git fetch origin
git reset --hard origin/$GitBr

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker kill $(docker ps -q)
docker volume rm $(docker volume ls -q)
#docker rmi $(docker images -a -q)
#docker system prune -a -f
#docker-compose up -d --no-deps --build php
docker-compose up -d
chmod 777 deploy.shS

echo "open Site in $SiteHttp://$SiteUrl"

<?php

// ENV
define('MAX_EXECUTION_TIME', 600);
define('LANGUAGE_ID', 1);
define('OUT_OF_SALE_STOCK_STATUS_ID', 9);

	define('DEFAULT_GIFT_OPTION_ID', 1);

	// UTM
	define('USE_UTM', true);
	//define('USE_UTM', false);
	define('UTM_SOURCE', 'market.yandex.ru');

// AUTH
define('USE_TOKEN', false);
define('TOKEN', '12345');

// HTTP
define('HTTP_SERVER', 'https://'.$_SERVER['HTTP_HOST'].'/');

// HTTPS
define('HTTPS_SERVER', 'https://'.$_SERVER['HTTP_HOST'].'/');

// DIR
define('DIR_ROOT', getenv('ENV_ROOTPATH'));
define('DIR_FEED', DIR_ROOT . 'multiyamfeed/');
define('DIR_TEMP', DIR_FEED . 'temp/');
define('DIR_LOGS', DIR_FEED . 'logs/');
define('DIR_IMAGE', getenv('ENV_IMAGEPATH'));

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', getenv('DB_HOST'));
define('DB_USERNAME', getenv('DB_USERNAME'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_DATABASE', getenv('DB_DATABASE'));
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// FILE
define('FEED', 'feedyml18.xml');
define('LOG', 'log.txt');

?>

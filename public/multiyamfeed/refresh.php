<?php
	//header('Access-Control-Allow-Origin: *');
	//error_reporting(0);

require_once('../admin/config.php');
require_once(DIR_SYSTEM . 'startup.php');
require_once(DIR_SYSTEM . 'library/cart/currency.php');
require_once(DIR_SYSTEM . 'library/cart/user.php');
require_once(DIR_SYSTEM . 'library/cart/weight.php');
require_once(DIR_SYSTEM . 'library/cart/length.php');

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

// Store
if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
} else {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
}

if ($store_query->num_rows) {
	$config->set('config_store_id', $store_query->row['store_id']);
} else {
	$config->set('config_store_id', 0);
}

// Settings
$query = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $result) {
	if (!$result['serialized']) {
		$config->set($result['key'], $result['value']);
	} else {
		$config->set($result['key'], json_decode($result['value'], true));
	}
}

if (!$store_query->num_rows) {
	$config->set('config_url', HTTP_SERVER);
	$config->set('config_ssl', HTTPS_SERVER);
}

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

// Error Handler
function error_handler($errno, $errstr, $errfile, $errline) {
	global $config, $log;

	if (0 === error_reporting()) return TRUE;
	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$error = 'Notice';
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$error = 'Warning';
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$error = 'Fatal Error';
			break;
		default:
			$error = 'Unknown';
			break;
	}

	if ($config->get('config_error_display')) {
		echo '<b>' . $error . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
	}

	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	}

	return TRUE;
}

// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$registry->set('response', $response);

// Session
$registry->set('session', new Session('file')); // db  ???  file  ???  native

// Cache
$registry->set('cache', new Cache('file'));

// Document
$registry->set('document', new Document());

// Languages
$languages = array();

$query = $db->query("SELECT * FROM " . DB_PREFIX . "language");

foreach ($query->rows as $result) {
	$languages[$result['code']] = array(
		'language_id'	=> $result['language_id'],
		'name'		=> $result['name'],
		'code'		=> $result['code'],
		'locale'	=> $result['locale'],
		'directory'	=> $result['directory']
	);
}

$config->set('config_language_id', $languages[$config->get('config_admin_language')]['language_id']);

// Language
$language = new Language($languages[$config->get('config_admin_language')]['directory']);
$language->load($languages[$config->get('config_admin_language')]['directory']);
$registry->set('language', $language);

// Currency
$registry->set('currency', new Cart\Currency($registry));

// Weight
$registry->set('weight', new Cart\Weight($registry));

// Length
$registry->set('length', new Cart\Length($registry));

// User
$registry->set('user', new Cart\User($registry));

//OpenBay Pro
$registry->set('openbay', new Openbay($registry));

// Event
$event = new Event($registry);
$registry->set('event', $event);

// Route
$route = new Router($registry);

	$db->query("TRUNCATE " . DB_PREFIX . "product_option");
	$db->query("TRUNCATE " . DB_PREFIX . "product_option_value");
	
	$option_data = array();
	$options = array();
	
	$query = $db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "gift_product WHERE status = '1'");
	
	$settings = $query->rows;
	
	foreach ($settings as $setting) {
		
		$query = $db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "gift_product WHERE setting_id = '" . (int)$setting['setting_id'] . "'");

		$setting_data = $query->row;
		
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "product WHERE status = '1'";
		if ((float)$setting_data['price_from'] > 0) {
			$sql .= " AND price >= '" . (float)$setting_data['price_from'] . "'";
		}
		if ((float)$setting_data['price_to'] > 0) {
			$sql .= " AND price <= '" . (float)$setting_data['price_to'] . "'";
		}
		if ((int)$setting_data['stock_from'] > 0) {
			$sql .= " AND quantity >= '" . (int)$setting_data['stock_from'] . "'";
		}
		if ((int)$setting_data['stock_to'] > 0) {
			$sql .= " AND quantity <= '" . (int)$setting_data['stock_to'] . "'";
		}
		if ($setting_data['setting_manufacturers'] != '') {
			$sql .= " AND manufacturer_id IN (" . $setting_data['setting_manufacturers'] . ")";
		}
		if ($setting_data['setting_statuses'] != '') {
			$sql .= " AND stock_status_id IN (" . $setting_data['setting_statuses'] . ")";
		}
		if ($setting_data['setting_categories'] != '') {
			$query = $db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p2c.category_id IN (" . $setting_data['setting_categories'] . ")");
			$results = $query->rows;
			$arr = array();
			foreach ($results as $result) {
				$arr[] = $result['product_id'];
			}
			$product_range = implode(",", $arr);
			$sql .= " AND product_id IN (" . $product_range . ")";
		}
		
		$query = $db->query($sql);
		$products = $query->rows;
		$product_quantity = count($products);
		$db->query("UPDATE " . DB_PREFIX . "gift_product SET product_quantity = '" . (int)$product_quantity . "' WHERE setting_id = '" . (int)$setting['setting_id'] . "'");
		foreach ($products as $product) {
			$option_data[] = array(
				'product_id' 			=> $product['product_id'],
				'option_id'       => $setting_data['option_id'],
				'option_value_id'	=> $setting_data['option_value_id'],
				'quantity'				=> $setting_data['gift_quantity']
			);
		}
	}
	
	$option_data = array_unique($option_data, SORT_REGULAR);
	
	usort($option_data, function($a, $b){
		return $a['product_id'] <= $b['product_id'];
	});
	
	foreach ($option_data as $option) {
		$product_option_id = false;
		$query = $db->query("SELECT * FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$option['product_id'] . "' AND option_id = '" . (int)$option['option_id'] . "'");

		if ($query->num_rows) {
			$product_option_id = $query->row['product_option_id'];
		} else {
			$db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$option['product_id'] . "', option_id = '" . (int)$option['option_id'] . "', required = '0', value = ''");
			$product_option_id = $db->getLastId();
		}
		$db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_id = '" . (int)$option['product_id'] . "', product_option_id = '" . (int)$product_option_id . "', subtract = '1', option_id = '" . (int)$option['option_id'] . "', option_value_id = '" . (int)$option['option_value_id'] . "', quantity = '" . (int)$option['quantity'] . "'");
	}
		
// Output
$response->output();
?>
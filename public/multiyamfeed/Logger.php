<?php
/**
 * Class Logger
 *
 * CREATE TABLE `oc_yam_task` (
 * `id` int(11) UNSIGNED NOT NULL,
 * `created` timestamp NOT NULL
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
 *
 * CREATE TABLE `oc_yam_task_items` (
 * `product_id` int(11) NOT NULL,
 * `task_id` int(11) UNSIGNED NOT NULL,
 * `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
 * `price` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
 * `quantity` int(11) NOT NULL
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
 *
 * ALTER TABLE `oc_yam_task`
 * ADD PRIMARY KEY (`id`);
 *
 * ALTER TABLE `oc_yam_task_items`
 * ADD KEY `task_id` (`task_id`);
 *
 * ALTER TABLE `oc_yam_task`
 * MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
 *
 * ALTER TABLE `oc_yam_task_items`
 * ADD CONSTRAINT `oc_yam_task_items_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `oc_yam_task` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

class Logger
{
    private $dbLink;

    private $config;

    private static $instance = null;
    
    private $feedName;

    protected function __construct()
    {
        require_once('config.php');

        $this->dbLink = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        mysqli_select_db($this->dbLink, DB_DATABASE);

        $res = mysqli_query($this->dbLink, "SELECT `key`, `value` FROM ".DB_PREFIX."setting WHERE `code` = 'config'");
        if ($res) {
            while ($row = mysqli_fetch_assoc($res)) {
                $this->config[$row['key']] = $row['value'];
            }
        } else {
            $this->log(mysqli_error($this->dbLink));
        }
    }

    public static function init()
    {
        if ( ! self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * метод запускает анализ
     *
     * @param array $products
     *
     * @return false
     */
    public function analyze(array $products)
    {

        if ( ! $products) {
            $this->log('empty products, analyze stop');

            return false;
        }

        $this->log('analyze start');

        $res = mysqli_query($this->dbLink, "INSERT INTO ".DB_PREFIX."yam_task SET `created` = NOW()");
        if ($res) {
            $query   = mysqli_query($this->dbLink, "SELECT LAST_INSERT_ID() AS id");
            $data    = $query->fetch_object();
            $task_id = $data->id;

            if ($task_id) {
                $this->saveItems($task_id, $products);

                $this->compareItems($task_id, $products);
                
                $this->sendFullReport($task_id, $products);
            }
        } else {
            $this->log(mysqli_error($this->dbLink));
        }
        
        $this->clearOldTasks();
    }

    /**
     * метод сохраняет новые записи
     *
     * @param int   $task_id
     * @param array $products
     */
    private function saveItems(int $task_id, array $products)
    {
        foreach ($products as $product) {
            $res = mysqli_query($this->dbLink,
                "INSERT INTO ".DB_PREFIX
                ."yam_task_items SET `task_id` = '{$task_id}', `product_id` = '{$product['product_id']}', 
                `title` = '{$product['name']}', `price` = '{$product['price']}', `quantity` = '{$product['quantity']}'");
            if ( ! $res) {
                $this->log(mysqli_error($this->dbLink));
            }
        }
    }

    /**
     * метод сравнивает предыдущую и текущую выгрузку
     *
     * @param int   $task_id
     * @param array $products
     */
    private function compareItems(int $task_id, array $products)
    {
        $notfoundProducts = [];
        $newProducts      = [];
        foreach ($products as $product) {
            $newProducts[$product['product_id']] = $product;
        }

        $query = mysqli_query($this->dbLink,
            "SELECT id FROM ".DB_PREFIX."yam_task WHERE `id` < '{$task_id}' ORDER BY id DESC LIMIT 1");
        if ($query) {
            $data              = $query->fetch_object();
            $task_id           = $data->id;
            $lastProductsQuery = mysqli_query($this->dbLink,
                "SELECT * FROM ".DB_PREFIX."yam_task_items WHERE `task_id` = '{$task_id}'");
            $lastProducts      = [];
            while ($row = mysqli_fetch_assoc($lastProductsQuery)) {
                $lastProducts[] = $row;
            }

            if ($lastProducts) {
                foreach ($lastProducts as $product) {
                    if ( ! array_key_exists($product['product_id'], $newProducts)) {

                        $productQuery = mysqli_query($this->dbLink,
                            "SELECT price, quantity FROM ".DB_PREFIX
                            ."product WHERE `product_id` = '{$product['product_id']}'");

                        $actualProduct = $productQuery ? $productQuery->fetch_assoc() : [];

                        $notfoundProducts[] = [
                            'old' => $product,
                            'new' => $actualProduct,
                        ];
                    }
                }
            }

        } else {
            $this->log(mysqli_error($this->dbLink));
        }

        if ($notfoundProducts) {
            $this->sendAlert($notfoundProducts);
            $this->log(count($notfoundProducts).' - not found');
        }

        $this->log('analyze end');
    }

    private function log(string $message)
    {
        file_put_contents(DIR_LOGS.'analyze.log', date('m.d.Y H:i:s').' - '.$message.PHP_EOL, FILE_APPEND);
    }

    /**
     * метод отправляет email уведомление
     *
     * @param array $notfoundProducts
     */
    private function sendAlert(array $notfoundProducts)
    {
        $adminEmails = [
            'tech@sdmclimate.ru',
            'ea@sdmclimate.ru',
            'ava@sdmclimate.ru',
            'm.spirin@sdmclimate.ru',
        ];

        $msg     = 'Во время выгрузки в яндекс.маркет некоторые товары не были выгружены. Подробности в csv файле';
        $csvFile = 'compare.csv';

        $head = [
            'id товара',
            'наименование',
            'ссылка на товар',
            'цена',
            'остаток',
            'предыдущая цена',
            'предыдущий остаток',
        ];

        $fp = fopen($csvFile, 'w');

        $head = array_map(['self', 'encodeCSV'], $head);
        fputcsv($fp, $head, ';');

        foreach ($notfoundProducts as $product) {
            $product['old'] = array_map(['self', 'encodeCSV'], $product['old']);
            $product['new'] = array_map(['self', 'encodeCSV'], $product['new']);
            fputcsv($fp, [
                $product['old']['product_id'],
                $product['old']['title'],
                HTTPS_SERVER.'index.php?route=product/product&product_id='.$product['old']['product_id'],
                $product['new']['price'],
                $product['new']['quantity'],
                $product['old']['price'],
                $product['old']['quantity'],
            ], ';');
        }

        fclose($fp);

        $mail           = new PHPMailer(true);
        $mail->CharSet  = PHPMailer::CHARSET_UTF8;
        $mail->Encoding = PHPMailer::ENCODING_BASE64;
        
        try {
            $mail->SMTPDebug = SMTP::DEBUG_OFF;
            if ($this->config['config_mail_engine'] === 'smtp') {
                $mail->isSMTP();
                $mail->Host       = $this->config['config_mail_smtp_hostname'];
                $mail->SMTPAuth   = true;
                $mail->Username   = $this->config['config_mail_smtp_username'];
                $mail->Password   = $this->config['config_mail_smtp_password'];
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                $mail->Port       = $this->config['config_mail_smtp_port'];
                $mail->Timeout    = $this->config['config_mail_smtp_timeout'];
            }

            $mail->setFrom($this->config['config_email']);
            foreach ($adminEmails as $email) {
                $mail->addAddress($email);
            }

            $mail->addAttachment($csvFile);
            $mail->Subject = '=?UTF-8?B?'.base64_encode('Различия в выгрузке товаров фида '. $this->feedName).'?=';
            $mail->Body    = $msg;
            $mail->AltBody = $msg;

            $mail->send();
            $this->log('Message has been sent');
        } catch (Exception $e) {
            $this->log("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
        }
    }

    /**
     * переводим кодировку в windows для excel
     *
     * @param $value
     * @param $key
     *
     * @return false|string
     */
    private function encodeCSV($value)
    {
        return iconv('UTF-8', 'Windows-1251', $value);
    }

    /**
     * отправка полного отчета о выгрузке
     * @param       $task_id
     * @param array $products
     */
    private function sendFullReport($task_id, array $products)
    {
        $adminEmails = [
            'tech@sdmclimate.ru',
            'vr@sdmclimate.ru',
            'ea@sdmclimate.ru',
            'm.spirin@sdmclimate.ru',
        ];

        $msg     = 'Полный отчет о выгрузке товаров в яндекс.маркет. Подробности в csv файле';
        $csvFile = 'report.csv';

        $head = [
            'id товара',
            'наименование',
            'ссылка на товар',
            'цена',
            'остаток',
            'тип цены',
            'основной поставщик',
            'поставщик спец.цены',
        ];

        $fp = fopen($csvFile, 'w');

        $head = array_map(['self', 'encodeCSV'], $head);
        fputcsv($fp, $head, ';');

        foreach ($products as $item) {
            $productQuery = mysqli_query($this->dbLink,
                "SELECT * FROM ".DB_PREFIX
                ."product WHERE `product_id` = '{$item['product_id']}'");

            $product = $productQuery ? $productQuery->fetch_assoc() : [];
            
            if(!$product)
                continue;
            
            $product = array_map(['self', 'encodeCSV'], $product);
            fputcsv($fp, [
                $product['product_id'],
                $this->encodeCSV($item['name']),
                HTTPS_SERVER.'index.php?route=product/product&product_id='.$product['product_id'],
                $product['price'],
                $product['quantity'],
                $product['price_type'],
                $product['supplier_main'],
                $product['supplier_spec'],
            ], ';');
        }

        fclose($fp);

        $mail           = new PHPMailer(true);
        $mail->CharSet  = PHPMailer::CHARSET_UTF8;
        $mail->Encoding = PHPMailer::ENCODING_BASE64;

        try {
            $mail->SMTPDebug = SMTP::DEBUG_OFF;
            if ($this->config['config_mail_engine'] === 'smtp') {
                $mail->isSMTP();
                $mail->Host       = $this->config['config_mail_smtp_hostname'];
                $mail->SMTPAuth   = true;
                $mail->Username   = $this->config['config_mail_smtp_username'];
                $mail->Password   = $this->config['config_mail_smtp_password'];
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                $mail->Port       = $this->config['config_mail_smtp_port'];
                $mail->Timeout    = $this->config['config_mail_smtp_timeout'];
            }

            $mail->setFrom($this->config['config_email']);
            foreach ($adminEmails as $email) {
                $mail->addAddress($email);
            }

            $mail->addAttachment($csvFile);
            $mail->Subject = '=?UTF-8?B?'.base64_encode("Отчет о выгрузке товаров №$task_id фида $this->feedName").'?=';
            $mail->Body    = $msg;
            $mail->AltBody = $msg;

            $mail->send();
            $this->log('Message has been sent');
        } catch (Exception $e) {
            $this->log("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
        }
    }

    /**
     * сохраняем в базе только последние 5 заданий сравнения
     */
    private function clearOldTasks()
    {
        $query = mysqli_query($this->dbLink,
            "SELECT id FROM ".DB_PREFIX."yam_task ORDER BY id DESC LIMIT 5");
        if ($query) {
            $ids = [];
            $data = $query->fetch_all(MYSQLI_ASSOC);
            foreach ($data as $item){
                $ids[] = $item['id'];
            }
            
            if($ids) {
                $sql = "DELETE FROM ".DB_PREFIX."yam_task WHERE id NOT IN (".implode(',', $ids).")";
                mysqli_query($this->dbLink, $sql);
                
                $sql = "DELETE FROM ".DB_PREFIX."yam_task_items WHERE task_id NOT IN (".implode(',', $ids).")";
                mysqli_query($this->dbLink, $sql);
            }
        }
    }

    /**
     * @param string $feedName
     *
     * @return Logger
     */
    public function setFeedName(string $feedName): Logger
    {
        $this->feedName = $feedName;

        return $this;
    }
}

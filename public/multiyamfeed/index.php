<?php
	//error_reporting(E_ALL ^ E_DEPRECATED);
	//ini_set('max_execution_time', MAX_EXECUTION_TIME);
	require_once('config.php');
	
	$link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
    $link->set_charset("utf8");
	$db = mysqli_select_db ($link, DB_DATABASE);
	
	$eol = "\n";
	
	$status 	= 0;
	$batch 		= 100;
	$ttl 			= 60;
	
	$settings = array();
	$default = array();
	
	$sql = "SELECT * FROM " . DB_PREFIX . "setting WHERE code = 'feed_multifastfeed'";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			if ($row['key'] == 'feed_multifastfeed_status') $status = $row['value'];
			if ($row['key'] == 'feed_multifastfeed_batch') $batch = $row['value'];
			if ($row['key'] == 'feed_multifastfeed_temp_ttl') $ttl = $row['value'];
		}
	}
	
	$sql = "SELECT * FROM " . DB_PREFIX . "yam_setting ORDER BY setting_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
            $auto_settings = [];
            if($row['auto_settings']){
                foreach (json_decode($row['auto_settings'], true) as $item){
	                array_walk($item,
		                function (&$el, $k) {
			                if (!in_array($k, ['categories', 'manufacturers', 'suppliers'])) {
				                $el = html_entity_decode($el);
			                }			                
		                }
	                );
	                
	                $auto_settings[] = $item;
                }
            }

			$additional_settings = [];
			if($row['additional_settings']){
				$additional_settings = json_decode($row['additional_settings'], true);
			}

			$settings[$row['name']] = array(
				'setting_id'          => $row['setting_id'],
				'name'                => $row['name'],
				'description'         => $row['description'],
				'status'              => $row['status'],
				'is_default'          => $row['is_default'],
				'filename'            => $row['filename'],
				'token'               => $row['token'],
				'sales_note'          => $row['sales_note'],
				'delivery_option'     => $row['delivery_option'],
				'type'                => (int)$row['type'],
				'auto_settings'       => $auto_settings,
				'additional_settings' => $additional_settings,
			);
			if ($row['is_default'] == 1) {
				$default['setting_id']          = $row['setting_id'];
				$default['name']                = $row['name'];
				$default['description']         = $row['description'];
				$default['status']              = $row['status'];
				$default['filename']            = $row['filename'];
				$default['token']               = $row['token'];
				$default['sales_note']          = $row['sales_note'];
				$default['delivery_option']     = $row['delivery_option'];
				$default['type']                = (int)$row['type'];
				$default['auto_settings']       = $auto_settings;
				$default['additional_settings'] = $additional_settings;
			}
		}
	}
	
	$yml  = '<?xml version="1.0" encoding="UTF-8"?>' . $eol;
	
	if (!file_exists(DIR_TEMP)) mkdir(DIR_TEMP, 0755, true);
	if (!file_exists(DIR_LOGS)) mkdir(DIR_LOGS, 0755, true);
	if (!file_exists(DIR_LOGS . LOG)) {
		$log_file = fopen(DIR_LOGS.LOG, 'w');
		fwrite($log_file, date('Y-m-d H:i') . ' (:!:) лог файл создан' . PHP_EOL . PHP_EOL);
		fclose($log_file);
	}
	
	if (isset($_GET['mode']) && $_GET['mode'] == 'debug') {
		$debug = true;
	} else {
		$debug = false;
	}
	
	$start_time = time();
	_log('(:s:) начало генерации фида');
	
	$setting_id = false;
	$feedName = false;
	$description = false;
	$status = false;
	$is_default = false;
	$filename = false;
	$token = false;
	$default_sales_note = false;
	$default_delivery_option = false;
	
	if (isset($_GET['set'])) {
		$setting_id 			= $settings[$_GET['set']]['setting_id'];	
		$feedName 				= $settings[$_GET['set']]['name'];	
		$description 			= $settings[$_GET['set']]['description'];	
		$status 					= $settings[$_GET['set']]['status'];	
		$is_default 			= $settings[$_GET['set']]['is_default'];	
		$filename 				= $settings[$_GET['set']]['filename'];	
		$token 						= $settings[$_GET['set']]['token'];	
		$default_sales_note 			= $settings[$_GET['set']]['sales_note'];	
		$default_delivery_option 	= $settings[$_GET['set']]['delivery_option'];
        $setting_type = $settings[$_GET['set']]['type'];
        $auto_settings = $settings[$_GET['set']]['auto_settings'];
		$additional_settings = $settings[$_GET['set']]['additional_settings'];
	} else {
		$setting_id 			= $default['setting_id'];	
		$feedName 				= $default['name'];	
		$description 			= $default['description'];	
		$status 					= $default['status'];	
		$is_default 			= $default['is_default'];	
		$filename 				= $default['filename'];	
		$token 						= $default['token'];	
		$default_sales_note 			= $default['sales_note'];	
		$default_delivery_option 	= $default['delivery_option'];
        $setting_type = $default['type'];
        $auto_settings = $default['auto_settings'];
		$additional_settings = $default['additional_settings'];
	}
	
	$html  = 'Фиды<br>';
	$html .= '<pre>' . print_r($settings,1) . '</pre>';
	$html .= '<hr>';
	$html .= 'Фид по умолчанию<br>';
	$html .= '<pre>' . print_r($default,1) . '</pre>';
	
	if (!$setting_id) {
		echo $_GET['set'] . ' - нет такой настройки'; 
		exit();
	}
	
	if (!$status) {
		echo $_GET['set'] . ' - настройка не активна'; 
		exit();
	}
	
	$filename = $filename.'.xml';
	
	$html .= $feedName . ' - выбранная настройка активна <br>'; 
	$html .= $filename . '.xml - имя файла <br>'; 
	$html .= $token . ' - token <br>'; 
	$html .= $default_sales_note . ' - default_sales_note <br>'; 
	$html .= $default_delivery_option . ' - default_delivery_option <br>'; 
	
	$handle = FALSE;
	
	$feedDir = '';
	$filenameArr = explode('/', $filename);
	if(count($filenameArr) > 1){
		$filename = array_pop($filenameArr);
		$feedDir = implode('/', $filenameArr);
		/** если папка для фида не существует - пытаемся создать ее */
		if(!is_dir(DIR_ROOT. $feedDir) && !mkdir(DIR_ROOT. $feedDir, 0755, true)){
			$feedDir = '';
		}
	}
	
	$handle = fopen(DIR_TEMP.$filename, "w");
	
	$arr_products = array();

	$additional_settings_sql = ' AND p.actual_price = 1';
	$price_type_sql = [];
	$additional_settings_join = '';
	$yml_type = 1; //1 - стандартный (по умолчанию), 2 - витрина
 	
	if($additional_settings){
		foreach ($additional_settings as $key => $values){
			switch ($key){
				case 'manufacturer':
					$additional_settings_sql .= " AND p.manufacturer_id > 0";
					break;
				case 'sku':
					$additional_settings_sql .= " AND p.sku != '' AND p.sku IS NOT NULL";
					break;
				case 'dimensions':
					$additional_settings_sql .= " AND p.length > 0 AND p.width > 0 AND p.height > 0";
					break;
				case 'weight':
					$additional_settings_sql .= " AND p.weight > 0";
					break;
				case 'spec_price':
					$price_type_sql[] = "p.price_type = 'Специальная цена'";
					break;
				case 'discount_price':
					$price_type_sql[] = "p.price_type = 'Акция'";
					break;
				case 'attributes':
					foreach ($values as $attribute_id) {
						$additional_settings_sql  .= " AND attrs{$attribute_id}.attribute_id = '". (int)$attribute_id ."'";
						$additional_settings_join .= " LEFT JOIN ".DB_PREFIX
							."product_attribute attrs{$attribute_id} ON attrs{$attribute_id}.product_id = p.product_id";
					}
					break;
				case 'yml_type':
					$yml_type = (int)$values;
					break;
			}
		}
		
		if($price_type_sql){
			/** если выбраны несколько типов цены */
			if(count($price_type_sql) > 1){
				$additional_settings_sql .= " AND (" . implode(' OR ', $price_type_sql) . ")";
			} else {
				$additional_settings_sql .= " AND $price_type_sql[0]";
			}
		}
	}

    $allCategories      = [];
    $excludedCategories = [];
	$sql = "SELECT category_id FROM ". DB_PREFIX . "category WHERE status = '1'";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$allCategories[] = $row['category_id'];
		}
	}
	
    /** если включен автоматический режим работы фида */
    if($setting_type == 2) {
        $sql = "SELECT DISTINCT p.product_id, p.price, p.sup_price, p.price_type";
	    $diapasons = [];
	    $filterCategories = false;
        
        foreach ($auto_settings as $autoSetting) {
	        $diapason = "(p.price >= ". $autoSetting['priceFrom']." AND p.price <= ". $autoSetting['priceTo']." AND p.quantity >= ". $autoSetting['quantityFrom'] ." AND p.quantity <= ". $autoSetting['quantityTo'] ."";
            
            if(isset($autoSetting['categories'])){
	            $filterCategories = true;
	            $diapason .= " AND cp.path_id IN (" . implode(',', $autoSetting['categories']) . ")";
	            
                foreach ($allCategories as $categoryId){
                    if(!in_array($categoryId, $autoSetting['categories'])){
                        $excludedCategories[] = $categoryId;
                    }
                }
                
                if($excludedCategories){
                    $additional_settings_sql .= " AND p.product_id NOT IN (
                        SELECT DISTINCT p2.product_id FROM " . DB_PREFIX . "product p2 
                        LEFT JOIN " . DB_PREFIX . "product_to_category p2c2 ON p2c2.product_id = p2.product_id 
                        WHERE p2c2.category_id IN (" . implode(',', $excludedCategories) . ")
                    )";
                }
            }
            if(isset($autoSetting['manufacturers'])){
	            $diapason .= " AND p.manufacturer_id IN (" . implode(',', $autoSetting['manufacturers']) . ")";
            }
            if(isset($autoSetting['suppliers'])){
	            $diapason .= " AND p.supplier_main IN ('" . implode("','", $autoSetting['suppliers']) . "')";
            }

	        $diapason .= ")";

	        $diapasons[] = $diapason;
        }
        
        if($filterCategories){
        	$sql .= " FROM " . DB_PREFIX . "category_path cp 
        	LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id) 
        	LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
        }else{
        	$sql .= " FROM " . DB_PREFIX . "product AS p";
        }

	    $sql .= $additional_settings_join;        
        $sql .= $yml_type !== 4 ? " WHERE p.status = 1 AND p.price > 0 AND p.stock_status_id != " . OUT_OF_SALE_STOCK_STATUS_ID . "" : " WHERE 1";
        $sql .= " AND (". implode(" OR ", $diapasons) .")";
	    $sql .= $additional_settings_sql;
    } else {
        $sql = "SELECT DISTINCT yp.product_id, p.price, p.sup_price, p.price_type FROM ".DB_PREFIX."yam_product yp
        LEFT JOIN " . DB_PREFIX . "product p ON yp.product_id = p.product_id";
	    $sql .= $additional_settings_join;
        $sql .= " WHERE yp.setting_id = '".(int)$setting_id."' AND yp.yam_status = '1'";
	    $sql .= $additional_settings_sql;
        $sql .= " ORDER BY yp.product_id";
    }
    
    $res = mysqli_query($link, $sql);
    if ($res) {
        while ($row = mysqli_fetch_array($res)) {
            /** если включен расчет маржи */
            if (isset($additional_settings['marginOn']) && (int)$additional_settings['marginPercent']
                && (int)$additional_settings['marginSumEnd']
            ) {
                if ($row['price'] >= (int)$additional_settings['marginSumStart']
                    && $row['price'] <= (int)$additional_settings['marginSumEnd']
                    && $row['price_type'] != 'Акция'
                    && $row['price_type'] != 'Специальная цена'
                ) {
                    $margin = ($row['price'] - $row['sup_price']) / $row['price'] * 100;
                    if ($margin < (int)$additional_settings['marginPercent']) {
                        continue;
                    }
                }
            }
            
            $arr_products[] = $row['product_id'];
        }
    }
    $count_products = count($arr_products);
	
	$html .= 'arr_products : ' . implode(',', $arr_products) . '<br>'; 
	$html .= 'count_products : ' . $count_products . '<br>'; 
	
	$arr_categories = array();
	
	$sql = "SELECT DISTINCT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id IN(" . implode(',', $arr_products) . ") ORDER BY category_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$arr_categories[] = $row['category_id'];
		}
	}
	
	$stock_statuses = [];
	$sql = "SELECT stock_status_id, name FROM " . DB_PREFIX . "stock_status WHERE language_id = '1'";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$stock_statuses[$row['stock_status_id']] = $row['name'];
		}
	}
	
	$html .= 'arr_categories : ' . implode(',', $arr_categories) . '<br>'; 
	
	$arr_parents = array();
	
	$sql = "SELECT parent_id FROM " . DB_PREFIX . "category c WHERE c.category_id IN(" . implode(',', $arr_categories) . ") ORDER BY c.category_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$arr_categories[] = $row['parent_id'];
			$arr_parents[] = $row['parent_id'];
		}
	}

	$do = true;
	while ($do) {
		$sql = "SELECT parent_id FROM " . DB_PREFIX . "category c WHERE c.category_id IN(" . implode(',', $arr_parents) . ") ORDER BY c.category_id";
		$res = mysqli_query ($link, $sql);
		if($res){
			$arr_parents = array();		
			while($row=mysqli_fetch_array($res)){
				$arr_categories[] = $row['parent_id'];
				$arr_parents[] = $row['parent_id'];
			}
		} else {
			$do = false;
		}
	}

	$arr_categories = array_unique($arr_categories);

	$html .= 'arr_categories : ' . implode(',', $arr_categories) . '<br>'; 
	
	$categories = array();
	
	$sql = "SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id IN(" . implode(',', $arr_categories) . ") AND cd.language_id = '" . LANGUAGE_ID . "' ORDER BY c.category_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$categories[] = array(
				'category_id' 	=> $row['category_id'], 
				'parent_id' 		=> $row['parent_id'], 
				'name'        	=> cleanHtml($row['name'], isset($additional_settings['htmlCleanCategoryTitle']))
			);
		}
	}
	
	$html .= 'categories : <pre>' . print_r($categories,1) . '</pre><br>'; 
	_log('(:i:) список категорий  (' . count($categories) . ' поз. )');
	
	$delivery_options = array();
	
	$sql = "SELECT DISTINCT delivery_option FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND yam_status = '1' AND delivery_option !='' AND product_id IN(" . implode(',', $arr_products) . ") ORDER BY delivery_option LIMIT 5";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$delivery_options[] = $row['delivery_option'];
		}
	}
	
	if ($default_delivery_option) {
		if (count($delivery_options) > 4) {
			$delivery_options[4] = $default_delivery_option;
		} else {
			$delivery_options[] = $default_delivery_option;
		}
	}
	
	$html .= 'delivery_options : <pre>' . print_r($delivery_options,1) . '</pre><br>'; 
	_log('(:i:) список delivery_options  (' . count($delivery_options) . ' поз. )');
	
	$manufacturers = array();
	
	$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer ORDER BY manufacturer_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$manufacturers[$row['manufacturer_id']] = cleanHtml($row['name'], isset($additional_settings['htmlCleanManufacturer']));
		}
	}

	$html .= 'manufacturers : <pre>' . print_r($manufacturers,1) . '</pre><br>'; 
	_log('(:i:) список производителей  (' . count($manufacturers) . ' поз. )');
	
	$attributes = array();
	
	$sql = "SELECT * FROM " . DB_PREFIX . "attribute_description WHERE language_id = '" . LANGUAGE_ID . "' ORDER BY attribute_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$attributes[$row['attribute_id']] = cleanHtml($row['name'], isset($additional_settings['htmlCleanAttrTitle']));
		}
	}

	$html .= 'attributes : <pre>' . print_r($attributes,1) . '</pre><br>'; 
	_log('(:i:) список атрибутов  (' . count($attributes) . ' поз. )');

	$promos = array();
	
	$sql = "SELECT * FROM " . DB_PREFIX . "coupon WHERE ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1' ORDER BY code ASC";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$promo = array();
			$promo['coupon_id'] = $row['coupon_id'];
			$promo['name'] = $row['name'];
			$promo['code'] = $row['code'];
			$promo['type'] = $row['type'];
			$promo['total'] = $row['total'];
			$promo['discount'] = $row['discount'];
			$promo['date_start'] = date('Y-m-d', strtotime($row['date_start']));
			$promo['date_end'] = date('Y-m-d', strtotime($row['date_end']));
			$promo['status'] = $row['status'];
			$promos[] = $promo;
		}
	}

	$html .= 'promos : <pre>' . print_r($promos,1) . '</pre><br>'; 
	_log('(:i:) список промо кодов  (' . count($promos) . ' поз. )');
	
	$products = array();
	
	$defaultSql = $yml_type !== 4 ? "AND p.status = 1 AND p.price > 0 AND p.stock_status_id != " . OUT_OF_SALE_STOCK_STATUS_ID . "" : '';
    $sql = "SELECT *, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . LANGUAGE_ID . "' $defaultSql AND p.product_id IN(" . implode(',', $arr_products) . ") ORDER BY p.product_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		$num = 0;
		while($row=mysqli_fetch_array($res)){

            $product                    = [];
            $product['product_id']      = $row['product_id'];
            $product['name']            = $row['name'];
            $product['description']     = $row['description'];
            $product['model']           = $row['model'];
            $product['sku']             = $row['sku'];
            $product['price']           = $row['price'];
            $product['sup_price']       = $row['sup_price'];
            $product['special']         = $row['special'];
            $product['quantity']        = $row['quantity'];
            $product['image']           = $row['image'];
            $product['manufacturer']    = $manufacturers[$row['manufacturer_id']];
            $product['manufacturer_id'] = $row['manufacturer_id'];
            $product['vendor_code']     = $row['vendor_code'];
            $product['weight']          = $row['weight'];
            $product['length']          = $row['length'];
            $product['width']           = $row['width'];
            $product['height']          = $row['height'];
            $product['weight_class_id'] = $row['weight_class_id'];
            $product['length_class_id'] = $row['length_class_id'];
            $product['price_type']      = $row['price_type'];
            $product['supplier_spec']   = $row['supplier_spec'];
            $product['supplier_main']   = $row['supplier_main'];
            $product['stock_status_id'] = $row['stock_status_id'];

            $products[] = $product;
		}
	}
	
	$html .= 'products : <pre>' . print_r($products,1) . '</pre><br>'; 
	_log('(:i:) список товаров  (' . count($products) . ' поз. )');
	
	$gifts = array();
	$sql = "SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '" . DEFAULT_GIFT_OPTION_ID . "' AND ovd.language_id = '" . LANGUAGE_ID . "' ORDER BY ov.sort_order";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$gifts[] = array(
				'id'	=> $row['option_value_id'], 
				'name' 	=> $row['name'], 
				'image'	=> HTTPS_SERVER . 'image/' . $row['image']
			);
		}
	}
	
	if ($debug) echo $html;
	
	
	$yml .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' . $eol;
	$yml .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . $eol;
	$yml .= '<shop>' . $eol;

	$yml .= '<name>'.$additional_settings['shopName'].'</name>' . $eol;
	$yml .= '<company>'.$additional_settings['companyName'].'</company>' . $eol;
	$yml .= '<url>' . HTTPS_SERVER . '</url>' . $eol;
	$yml .= '<version>multi.fastfeed.v3</version>' . $eol;

	$yml .= '<currencies>' . $eol;
	$yml .= '<currency id="RUB" rate="1" />' . $eol;
	$yml .= '</currencies>' . $eol;

	$yml .= '<categories>' . $eol;
	
	foreach ($categories as $category) {
		$category_id = $category['category_id']; 
		$parent_id = $category['parent_id']; 
		$name = $category['name'];
		if ($parent_id == 0) {
			$yml .= '<category id="' . $category_id . '">' . $name . '</category>' . $eol;
		} else {
			$yml .= '<category id="' . $category_id . '" parentId="' . $parent_id . '">' . $name . '</category>' . $eol;
		}
	}
	_log('(:i:) категории выведены во временный файл');
	
	$yml .= '</categories>' . $eol;
	
	$yml .= '<delivery-options>' . $eol;
	
	foreach ($delivery_options as $delivery_option) {
		if ($delivery_option != '') {
			$yml .= html_entity_decode($delivery_option) . $eol;
		}
	}
	
	$yml .= '</delivery-options>' . $eol;
	
	$yml .= '<offers>' . $eol;
	
	if($handle){
		fwrite($handle, $yml);
		fclose($handle);
	}
	
	$num = 0;
	$yml = '';
	$offerIds = [];
	$productsToCsv = [];	
	foreach ($products as $product) {
		$num++;
		
		$product_categories = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			while($row=mysqli_fetch_array($res)){
				$product_categories[] = $row['category_id'];
			}
		}
		
		$product_attributes = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "product_attribute WHERE language_id = '" . LANGUAGE_ID . "' AND product_id = '" . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			while($row=mysqli_fetch_array($res)){
				$product_attributes[] = array(
					'attribute_id' 	=> $row['attribute_id'], 
					'text' 		=> cleanHtml($row['text'], isset($additional_settings['htmlCleanAttrValue']))
				);
			}
		}
		
		$alias = false;
		$sql = "SELECT DISTINCT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			$row = mysqli_fetch_array($res, MYSQLI_NUM);
			$alias = $row[0];
		}
		$url = $alias ? HTTPS_SERVER . $alias : HTTPS_SERVER . 'index.php?route=product/product&amp;product_id=' . $product['product_id'];
		
		if (USE_UTM && isset($additional_settings['utmOn'])) {
			$url .= '?utm_source=' . UTM_SOURCE . '&amp;utm_term=' . $product['product_id'];
		}
		
		$price = number_format($product['price'], 2, '.', '');
		$sup_price = number_format($product['sup_price'], 2, '.', '');
		$special = $product['special'] ? number_format($product['special'], 2, '.', '') : false;

		$picture = HTTPS_SERVER . 'image/' . $product['image'];
		
		$productId = $product['product_id'];
		$quantity = $product['quantity'];
		$name = cleanHtml($product['name'], isset($additional_settings['htmlCleanTitle']));
		$model = strip_tags($product['model']);
		
		$description = '<![CDATA[' . (isset($additional_settings['sanitizeDesc']) ? strip_tags(html_entity_decode($product['description']), '<p><br>') : html_entity_decode($product['description'])) . ']]>';
		
		
		$sales_note = false;
		$sql = "SELECT DISTINCT sales_note FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND product_id = " . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			$row = mysqli_fetch_array($res, MYSQLI_NUM);
			$sales_note = $row[0];
		}
		$sales_note = $sales_note ? $sales_note : $default_sales_note;
		$sales_note = strip_tags($sales_note);
		
		$delivery_option = false;
		$sql = "SELECT DISTINCT delivery_option FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND product_id = " . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			$row = mysqli_fetch_array($res, MYSQLI_NUM);
			$delivery_option = $row[0];
		}
		$delivery_option = $delivery_option ? $delivery_option : $default_delivery_option;
		$delivery_option = html_entity_decode($delivery_option);
		
		/** если включен автоматический режим работы фида */
		if($setting_type == 2) {
            $checkPrice = $special ? $special : $price;
            foreach ($auto_settings as $autoSetting) {
                if(
                    $checkPrice >= $autoSetting['priceFrom'] 
                    and $checkPrice <= $autoSetting['priceTo'] 
                    and $quantity >= $autoSetting['quantityFrom'] 
                    and $quantity <= $autoSetting['quantityTo']
                ){
                    $delivery_option = $autoSetting['delivery_option'];
                    $sales_note      = strip_tags($autoSetting['sales_note']);
                    break;
                }
            }
        }
		
		$vendor = $product['manufacturer'];
		$vendor_code = $additional_settings['vendorCodeField'] ? $product[$additional_settings['vendorCodeField']] : '';
	
//		if ($quantity >= 500 && $price >= 5000) {
//			$available = 'false';
//		} else {
//			$available = 'true';
//		}
//		if ($quantity < 1 || $quantity == '') {
//			$available = 'false';
//		}

		if ($quantity < 1 || $quantity == '') {
		$available = 'false';
			}else {
		$available = 'true';
		}


		switch ($yml_type){
			case 1:
				$yml .= '<offer id="' . $productId . '" available="' . $available . '" >' . $eol;
				$yml .= '<url>' . $url . '</url>' . $eol;
				if ($special) {
					$yml .= '<price>' . $special . '</price>' . $eol;
					$yml .= '<oldprice>' . $price . '</oldprice>' . $eol;
				} else {
					$yml .= '<price>' . $price . '</price>' . $eol;
				}
				if ($sup_price > 0) {
					$yml .= '<purchase_price>' . $sup_price . '</purchase_price>' . $eol;
				}
				$yml .= '<currencyId>RUB</currencyId>' . $eol;
				foreach ($product_categories as $product_category) {
					$yml .= '<categoryId>' . $product_category . '</categoryId>' . $eol;
				}
				$yml .= '<picture>' . $picture . '</picture>' . $eol;
				$yml .= '<store>false</store>' . $eol;
				$yml .= '<pickup>false</pickup>' . $eol;
				$yml .= '<delivery>true</delivery>' . $eol;
				$yml .= '<delivery-options>' . $eol;
				$yml .= '' . $delivery_option . $eol;
				$yml .= '</delivery-options>' . $eol;
				$yml .= '<name>' . $name . '</name>' . $eol;
				$yml .= '<vendor>' . $vendor . '</vendor>' . $eol;
				if ($vendor_code !== '') {
					$yml .= '<vendorCode>' . $vendor_code . '</vendorCode>' . $eol;
				}
				$yml .= '<model>' . $model . '</model>' . $eol;
				$yml .= '<description>' . $description . '</description>' . $eol;
				$yml .= '<sales_notes>' . $sales_note . '</sales_notes>' . $eol;
				$yml .= '<manufacturer_warranty>true</manufacturer_warranty>' . $eol;

				foreach ($product_attributes as $product_attribute) {
					$yml .= '<param name="' . $attributes[$product_attribute['attribute_id']] . '">' . $product_attribute['text'] . '</param>' . $eol;
				}

				$yml .= '</offer>' . $eol;
				break;
			case 2:
				$dimensions = '';
				if($product['length'] and $product['width'] and $product['height']){
					switch ($product['length_class_id']) {
						case 1: //cm
							$dimensions = sprintf("%s/%s/%s", round($product['length'], 3), 
								round($product['width'], 3), round($product['height'], 3));
							break;
						case 2: //mm
							$dimensions = sprintf("%s/%s/%s", round($product['length'] / 10, 3),
								round($product['width'] / 10, 3), round($product['height'] / 10, 3));
							break;
						case 3: //m
							$dimensions = sprintf("%s/%s/%s", round($product['length'] * 100, 3),
								round($product['width'] * 100, 3), round($product['height'] * 100, 3));
							break;
					}
				}
				$weight = '';
				if($product['weight']){
					switch ($product['weight_class_id']) {
						case 1: //kg
							$weight = round($product['weight'], 3);
							break;
						case 2: //g
							$weight = round($product['weight'] / 1000, 3);
							break;
					}
				}
				
				$yml .= '<offer id="' . $productId . '">' . $eol;
				$yml .= '<name>' . $name . '</name>' . $eol;
				$yml .= '<shop-sku>' . $product['sku'] . '</shop-sku>' . $eol;

				if ($special) {
					$yml .= '<price>'.$special.'</price>'.$eol;
					$yml .= '<oldprice>'.$price.'</oldprice>'.$eol;
				} else {
					$yml .= '<price>'.$price.'</price>'.$eol;
				}
				if ($sup_price > 0) {
					$yml .= '<purchase_price>'.$sup_price.'</purchase_price>'.$eol;
				}
				$yml .= '<currencyId>RUB</currencyId>'.$eol;
				
				foreach ($product_categories as $product_category) {
					$yml .= '<categoryId>' . $product_category . '</categoryId>' . $eol;
				}
				$yml .= '<vendor>' . $vendor . '</vendor>' . $eol;
				$yml .= '<url>' . $url . '</url>' . $eol;
				$yml .= '<manufacturer>' . $vendor . '</manufacturer>' . $eol;				
				
				if ($vendor_code !== '') {
					$yml .= '<vendorCode>' . $vendor_code . '</vendorCode>' . $eol;
				}
				
				$yml .= '<description>' . $description . '</description>' . $eol;
				$yml .= '<dimensions>' . $dimensions . '</dimensions>' . $eol;
				$yml .= '<weight>' . $weight . '</weight>' . $eol;

				foreach ($product_attributes as $product_attribute) {
					switch ($product_attribute['attribute_id']){
						case 41:
							$yml .= '<warranty-days>' . $product_attribute['text'] . '</warranty-days>' . $eol;
							break;
						case (int)$additional_settings['countryAttributeId']:
							$yml .= '<country_of_origin>' . $product_attribute['text'] . '</country_of_origin>' . $eol;
							break;
					}
				}

				$yml .= '</offer>' . $eol;
				break;

			case 3:
				$catPath = '';
				$catId = (int)end($product_categories);
				$res = mysqli_query ($link, "SELECT GROUP_CONCAT(DISTINCT su.`keyword` ORDER BY cp.`level` SEPARATOR '/') AS path FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "seo_url su ON (CONCAT('category_id=', cp.path_id) = su.`query`) WHERE cp.category_id = '" . $catId . "'");

				if ($res) {
					$row = mysqli_fetch_assoc($res);
					$catPath = $row['path'];
				}
				
				$manPath = '';
				$res = mysqli_query ($link, "SELECT `keyword` FROM " . DB_PREFIX . "seo_url WHERE `query` = 'manufacturer_id=" . $product['manufacturer_id'] . "'");

				if ($res) {
					$row = mysqli_fetch_assoc($res);
					$manPath = $row['keyword'];
				}

				$categoryName = '';
				$res = mysqli_query ($link, "SELECT meta_description, name, seo_h1 FROM " . DB_PREFIX . "category_description WHERE category_id = '" . $catId . "'");

				if ($res) {
					$row = mysqli_fetch_assoc($res);
					$categoryName = $row['name'];
					$name = !empty($row['seo_h1']) ? $row['seo_h1'] : $row['name'];
					$description = $row['meta_description'];
					$name .= ' ' . $product['manufacturer'];
				}
				
				
				$url = HTTPS_SERVER . $catPath . '/' . $manPath;

				$offerParams = [$catId, $product['manufacturer_id']];
				$offerId     = implode('_', $offerParams);
				if ( in_array($offerId, $offerIds)) {
					continue 2;
				}

				$offerIds[] = $offerId;
				
				$yml .= '<offer id="' . $offerId . '" available="' . $available . '" >' . $eol;
				$yml .= '<url>' . $url . '</url>' . $eol;
				foreach ($product_categories as $product_category) {
					$yml .= '<categoryId>' . $product_category . '</categoryId>' . $eol;
				}
				$yml .= '<picture>' . $picture . '</picture>' . $eol;
				$yml .= '<name>' . $name . '</name>' . $eol;
				$yml .= '<param name="origin_cat">'. $categoryName .'</param>' . $eol;
				$yml .= '<param name="origin_man">'. $product['manufacturer'] .'</param>' . $eol;
				$yml .= '<description>' . $description . '</description>' . $eol;

				$yml .= '</offer>' . $eol;
				break;
            case 4:                
                $catId = (int)end($product_categories);                
                $productCategories = array_filter($categories, function ($category) use ($catId){
                    return (int)$category['category_id'] === $catId;
                });

                $productCategory = current($productCategories);
                $categoryName    = $productCategory ? $productCategory['name'] : '';

                $productsToCsv[] = [
                    'name'          => $name,
                    'model'         => $model,
                    'supplier_main' => $product['supplier_main'],
                    'supplier_spec' => $product['supplier_spec'],
                    'price_type'    => $product['price_type'],
                    'price'         => $product['price'],
                    'sup_price'     => $product['sup_price'],
                    'special'       => $product['special'],
                    'quantity'      => $product['quantity'],
                    'category'      => $categoryName,
                    'manufacturer'  => $product['manufacturer'],
                    'stock_status'  => $stock_statuses[$product['stock_status_id']]
                ];
				break;
            case 5:
                $dimensions = '';
				if($product['length'] and $product['width'] and $product['height']){
					switch ($product['length_class_id']) {
						case 1: //cm
							$dimensions = sprintf("%s/%s/%s", round($product['length'], 3), 
								round($product['width'], 3), round($product['height'], 3));
							break;
						case 2: //mm
							$dimensions = sprintf("%s/%s/%s", round($product['length'] / 10, 3),
								round($product['width'] / 10, 3), round($product['height'] / 10, 3));
							break;
						case 3: //m
							$dimensions = sprintf("%s/%s/%s", round($product['length'] * 100, 3),
								round($product['width'] * 100, 3), round($product['height'] * 100, 3));
							break;
					}
				}
				$weight = '';
				if($product['weight']) {
                    switch ($product['weight_class_id']) {
                        case 1: //kg
                            $weight = round($product['weight'], 3);
                            break;
                        case 2: //g
                            $weight = round($product['weight'] / 1000, 3);
                            break;
                    }
                }
                
				$yml .= '<offer id="' . $productId . '" available="' . $available . '" >' . $eol;
				$yml .= '<url>' . $url . '</url>' . $eol;
				if ($special) {
					$yml .= '<price>' . $special . '</price>' . $eol;
					$yml .= '<oldprice>' . $price . '</oldprice>' . $eol;
				} else {
					$yml .= '<price>' . $price . '</price>' . $eol;
				}
				if ($sup_price > 0) {
					$yml .= '<purchase_price>' . $sup_price . '</purchase_price>' . $eol;
				}
				$yml .= '<currencyId>RUB</currencyId>' . $eol;
				foreach ($product_categories as $product_category) {
					$yml .= '<categoryId>' . $product_category . '</categoryId>' . $eol;
				}
				$yml .= '<picture>' . $picture . '</picture>' . $eol;
				$yml .= '<store>false</store>' . $eol;
				$yml .= '<pickup>false</pickup>' . $eol;
				$yml .= '<delivery>true</delivery>' . $eol;
				$yml .= '<delivery-options>' . $eol;
				$yml .= '' . $delivery_option . $eol;
				$yml .= '</delivery-options>' . $eol;
				$yml .= '<name>' . $name . '</name>' . $eol;
				$yml .= '<vendor>' . $vendor . '</vendor>' . $eol;
				if ($vendor_code !== '') {
					$yml .= '<vendorCode>' . $vendor_code . '</vendorCode>' . $eol;
				}
				$yml .= '<model>' . $model . '</model>' . $eol;
				$yml .= '<description>' . $description . '</description>' . $eol;
				$yml .= '<sales_notes>' . $sales_note . '</sales_notes>' . $eol;
				$yml .= '<manufacturer_warranty>true</manufacturer_warranty>' . $eol;

				foreach ($product_attributes as $product_attribute) {
					$yml .= '<param name="' . $attributes[$product_attribute['attribute_id']] . '">' . $product_attribute['text'] . '</param>' . $eol;
				}
                $yml .= '<dimensions>' . $dimensions . '</dimensions>' . $eol;
				$yml .= '<weight>' . $weight . '</weight>' . $eol;

                if (isset($additional_settings['showCount'])) {
                    $yml .= '<count>'.(int)$product['quantity'].'</count>'.$eol;
                }

				$yml .= '</offer>' . $eol;
				break;
            case 6:
                switch ($product['length_class_id']) {
                    case 1: //cm
                        $length = round($product['length'], 3);
                        $width  = round($product['width'], 3);
                        $height = round($product['height'], 3);
                        break;
                    case 2: //mm
                        $length = round($product['length'] / 10, 3);
                        $width  = round($product['width'] / 10, 3);
                        $height = round($product['height'] / 10, 3);
                        break;
                    case 3: //m
                        $length = round($product['length'] * 100, 3);
                        $width  = round($product['width'] * 100, 3);
                        $height = round($product['height'] * 100, 3);
                        break;
                    default:
                        $length = '';
                        $width  = '';
                        $height = '';
                        break;
                }

                $weight = '';
                if ($product['weight']) {
                    switch ($product['weight_class_id']) {
                        case 1: //kg
                            $weight = round($product['weight'], 3);
                            break;
                        case 2: //g
                            $weight = round($product['weight'] / 1000, 3);
                            break;
                    }
                }

                $product_images = getProductImages($link, $productId);
                $description    = html_entity_decode($product['description']);

                if ($product_attributes) {
                    $description .= '<p style="font-size:28px; text-align:center; font-weight:bolder; margin:40px 0 10px 0;color:#333333;text-transform: uppercase">характеристики</p>';
                    $description .= '<table border="1" style="font-size:16px;text-align:left;color:#666666;border: 1px solid #e7e7e7;" width="100%">';

                    foreach ($product_attributes as $product_attribute) {
                        $description .= '<tr style="height:50px;border: 1px solid #e7e7e7;">
                                    <td style="border: 1px solid #e7e7e7; color:#666666; background:#f4f4f4; width:40%; padding:10px;font-weight:bold">'
                            .$attributes[$product_attribute['attribute_id']].'</td>
                                    <td style="border: 1px solid #e7e7e7;padding:10px;">'.$product_attribute['text'].'</td>
                                </tr>';
                    }

                    $description .= '</table>';
                }

                if ($product_images) {
                    $description .= '<p style="font-size:28px; text-align:center; font-weight:bolder; margin:40px 0 10px 0;color:#333333;text-transform: uppercase">внешний вид товара</p>';
                    $description .= '<p style="text-align:center;margin-top:20px"><img width="100%" src="'.$picture
                        .'"></p>';

                    foreach ($product_images as $productImage) {
                        $description .= '<p style="text-align:center;margin-top:20px"><img width="100%" src="'
                            .$productImage.'"></p>';
                    }
                }

                $yml .= '<offer id="'.$productId.'" group_id="'.$productId.'" >'.$eol;
                $yml .= '<name>'.$name.'</name>'.$eol;
                $yml .= '<description><![CDATA['.$description.']]></description>'.$eol;
                $yml .= '<vendor>'.$vendor.'</vendor>'.$eol;
                if ($special) {
                    $yml .= '<price>'.$special.'</price>'.$eol;
                } else {
                    $yml .= '<price>'.$price.'</price>'.$eol;
                }
                foreach ($product_categories as $product_category) {
                    $yml .= '<categoryId>'.$product_category.'</categoryId>'.$eol;
                }
                $yml .= '<picture>'.$picture.'</picture>'.$eol;

                foreach ($product_images as $productImage) {
                    $yml .= '<picture>'.$productImage.'</picture>'.$eol;
                }

                foreach ($product_attributes as $product_attribute) {
                    $yml .= '<param name="'.$attributes[$product_attribute['attribute_id']].'">'
                        .$product_attribute['text'].'</param>'.$eol;
                }

                if ($weight) {
                    $yml .= '<weight>'.$weight.'</weight>'.$eol;
                }
                if ($length) {
                    $yml .= '<length>'.$length.'</length>'.$eol;
                }
                if ($width) {
                    $yml .= '<width>'.$width.'</width>'.$eol;
                }
                if ($height) {
                    $yml .= '<height>'.$height.'</height>'.$eol;
                }

                $yml .= '<quantity>'.(int)$product['quantity'].'</quantity>'.$eol;

                $yml .= '</offer>'.$eol;
                break;
		}
		
		if ($num == $batch) {
		
			$handle = FALSE;
			$handle = fopen(DIR_TEMP.$filename, "a");
			
			if($handle){
				fwrite($handle, $yml);
				fclose($handle);
			}
			
			$num = 0;
			$yml = '';
		}
	}
	
	if($yml_type === 4){
	    exportToCsv($productsToCsv, $filename, $feedDir);	    
        _log('csv экспорт завершен');
	    exit;
    }
	
	_log('(:i:) товары выведены во временный файл');
	
	$yml .= '</offers>' . $eol;
	
	/** для витрины не показываем подарки и promo */
	if($yml_type === 1) {
		$yml .= '<gifts>'.$eol;

		foreach ($gifts as $gift) {

			$yml .= '<gift id="'.$gift['id'].'" >'.$eol;
			$yml .= '<name>'.$gift['name'].'</name>'.$eol;
			$yml .= '<picture>'.$gift['image'].'</picture>'.$eol;
			$yml .= '</gift>'.$eol;

		}
		$yml .= '</gifts>'.$eol;

		$date_start = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 7);
		$date_end   = date('Y-m-d H:i:s', time() + 60 * 60 * 24 * 7);

		$yml .= '<promos>'.$eol;

		foreach ($promos as $promo) {

			$promo_products = array();
			$sql            = "SELECT * FROM ".DB_PREFIX."coupon_product WHERE coupon_id = '".(int)$promo['coupon_id']
				."'";
			$res            = mysqli_query($link, $sql);
			if ($res) {
				while ($row = mysqli_fetch_array($res)) {
					$promo_products[] = $row['product_id'];
				}
			}

			$promo_categories = array();
			$sql              = "SELECT * FROM ".DB_PREFIX."coupon_category WHERE coupon_id = '"
				.(int)$promo['coupon_id']."'";
			$res              = mysqli_query($link, $sql);
			if ($res) {
				while ($row = mysqli_fetch_array($res)) {
					$promo_categories[] = $row['category_id'];
				}
			}

			foreach ($promo_categories as $key => $value) {
				$sql = "SELECT * FROM ".DB_PREFIX."product_to_category WHERE category_id = '".(int)$value."'";
				$res = mysqli_query($link, $sql);
				if ($res) {
					while ($row = mysqli_fetch_array($res)) {
						$promo_products[] = $row['product_id'];
					}
				}
			}

			$promo_products = array_unique($promo_products);
			asort($promo_products);

			$purchase = array();
			foreach ($promo_products as $key => $value) {
				$k = array_search($value, array_column($products, 'product_id'));
				if (strlen($k) > 0) {
					if ( ! $products[$k]['special'] && $products[$k]['price'] > $promo['total']) {
						$purchase[] = $value;
					}
				}
			}

			if ($purchase) {
				$yml .= '<promo id="'.$promo['coupon_id'].'" type="promo code">'.$eol;
				$yml .= '<start-date>'.$promo['date_start'].'</start-date>'.$eol;
				$yml .= '<end-date>'.$promo['date_end'].'</end-date>'.$eol;
				$yml .= '<description>'.$promo['name'].'</description>'.$eol;
//			$yml .= '<url>' . 'http://инфо.страница.акции' . '</url>' . $eol;
				$yml .= '<promo-code>'.$promo['code'].'</promo-code>'.$eol;
				if ($promo['type'] == 'F') {
					$yml .= '<discount unit="currency" currency="RUR">'.number_format($promo['discount'], 2, '.', '')
						.'</discount>'.$eol;
				} else {
					if ($promo['type'] == 'P') {
						$yml .= '<discount unit="percent">'.number_format($promo['discount'], 2, '.', '').'</discount>'
							.$eol;
					}
				}
				$yml .= '<purchase>'.$eol;
				foreach ($purchase as $key => $value) {
					$yml .= '<product offer-id="'.$value.'"/>'.$eol;
				}
				$yml .= '</purchase>'.$eol;
				$yml .= '</promo>'.$eol;
			}
		}

		foreach ($products as $product) {

			$sql = "SELECT * FROM ".DB_PREFIX."product_option_value pov LEFT JOIN ".DB_PREFIX
				."option_value ov ON(pov.option_value_id = ov.option_value_id) WHERE pov.option_id = '"
				.DEFAULT_GIFT_OPTION_ID."' AND pov.product_id = '".(int)$product['product_id']
				."' ORDER BY ov.sort_order ASC";

			$res = mysqli_query($link, $sql);
			if ($res) {

				if (is_numeric(mysqli_num_rows($res)) && mysqli_num_rows($res) > 0) {

					$yml .= '<promo id="'.$product['product_id'].'" type="gift with purchase">'.$eol;
					$yml .= '<start-date>'.$date_start.'</start-date>'.$eol;
					$yml .= '<end-date>'.$date_end.'</end-date>'.$eol;
					$yml .= '<description>Купи '.strip_tags($product['name']).' и получи подарок.</description>'.$eol;
					$yml .= '<url>'.HTTPS_SERVER.'specials</url>'.$eol;
					$yml .= '<purchase>'.$eol;
					$yml .= '<required-quantity>1</required-quantity>'.$eol;
					$yml .= '<product offer-id="'.$product['product_id'].'"/>'.$eol;
					$yml .= '</purchase>'.$eol;
					$yml .= '<promo-gifts>'.$eol;

					while ($row = mysqli_fetch_array($res)) {

						$yml .= '<promo-gift gift-id="'.$row['option_value_id'].'"/>'.$eol;

					}

					$yml .= '</promo-gifts>'.$eol;
					$yml .= '</promo>'.$eol;

				}
				mysqli_free_result($res);
			}
		}

		$yml .= '</promos>'.$eol;
	}
	
	$yml .= '</shop>' . $eol;
	$yml .= '</yml_catalog>';
	
	$handle = FALSE;
	$handle = fopen(DIR_TEMP.$filename, "a");
	
	if($handle){
		fwrite($handle, $yml);
		fclose($handle);
	}
	
	copy(DIR_TEMP.$filename, DIR_TEMP.'temp-feed-'.time().'.xml');
	copy(DIR_TEMP.$filename, DIR_ROOT.$feedDir.DIRECTORY_SEPARATOR.$filename);
	unlink(DIR_TEMP.$filename);
	
	$files = glob(DIR_TEMP.'*.xml');
	if ($files) {
		foreach ($files as $file) {
			if (filemtime($file) < time() - $ttl * 60) {
				unlink($file);
			}
		}
	}
	
	_log('(:i:) фид записан в файл ' . DIR_ROOT.$feedDir.DIRECTORY_SEPARATOR.$filename);
	
	$finish_time = time();
	
	$time = $finish_time - $start_time;
	
	_log('(:!:) всего времени ушло ' . $time . '  сек.');
	
	if($debug){
		
		$help = '';
		$help .= date('Y-m-d H:i') . '<br><br>';
		$help .= 'fastfeed для sdmclimate.ru<br><br>';
		$help .= 'Фид для Яндекс Маркета сформирован:<br><br>';
		$help .= '<a href="' . HTTPS_SERVER . 'multiyamfeed/' . FEED . '" target="_blank">' . HTTPS_SERVER . 'multiyamfeed/' . FEED . '</a><br><br>';
		$help .= 'режим отладки ;)<br>';
		print($help);
	}
	
	require_once 'Logger.php';
	Logger::init()->setFeedName($feedName)->analyze($products);
	
	function _log($string) {
		file_put_contents(DIR_LOGS.LOG, date('Y-m-d H:i') . ' ' . $string . PHP_EOL, FILE_APPEND);
	}
	
	function cleanHtml(string $str, bool $entity = true): string
	{
		if($entity){
			$str = html_entity_decode(str_replace('&apos;', '\'', $str), ENT_QUOTES, 'UTF-8');
			return str_replace(['"', '\'', '<', '>'], ['&quot;','&apos;','&lt;','&gt;'], htmlentities($str, ENT_XML1, 'UTF-8'));
		}
		else{
			return $str;
		}
	}

	function exportToCsv(array $products, string $filename, ?string $feedDir){
	    $filename = str_replace('.xml', '.csv', $filename);
	    $handle = fopen(DIR_ROOT.$feedDir.DIRECTORY_SEPARATOR.$filename, "w");	    
	    
        $header = [
            "Номенклатура",
            "Артикул",
            "Поставщик",
            "Закупка",
            "Цена на сайте",
            "Акция",
            "Остаток",
            "Группа номенклатуры",
            "Бренд",
            "Статус товара"
        ];

        $header = array_map('encodeCSV', $header);
        fputcsv($handle, $header, ";");

        foreach($products as $product){            
            $row = [
                $product['name'],
                $product['model'],
                ($product['price_type'] === 'Специальная цена') ? $product['supplier_spec'] :$product['supplier_main'],
                $product['sup_price'],
                $product['price'],
                $product['special'],
                $product['quantity'],
                $product['category'],
                $product['manufacturer'],
                $product['stock_status'],
            ];
            
            $row = array_map('encodeCSV', $row);
            fputcsv($handle, $row, ";");
        }
        
        fclose($handle);
    }
    
    function encodeCSV($value)
    {
        return iconv('UTF-8', 'Windows-1251//IGNORE', $value);
    }
    
    function getProductImages($db, $product_id): array
    {
        $images = [];
        $res = mysqli_query ($db, "SELECT image FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");
    
        while($row=mysqli_fetch_array($res)){
            if (is_file(DIR_IMAGE . $row['image'])) {
                $images[] = HTTPS_SERVER.'image/'.$row['image'];
            }
        }
        
        return $images;
    }
?>

var myMap;
$(document).ready(function() {
	var fields = $("#simplecheckout_shipping_address .simplecheckout-block-content:first");
	var shipping = $("#simplecheckout_shipping");
	var address = $('<div id="dadata_address" class="form-address"><label class="control-label">Адрес строкой в свободном формате</label><div><input class="form-control" type="text" name="address" id="address" value="" placeholder="Начните ввод адреса"></div><div id="no_coords" style="width:100%; padding:10px;color:#c00;display:none;">Мы не смогли определить Ваше местоположение на карте. Точная стоимость доставки будет рассчитана менеджером. Продолжите, пожалуйста, Ваше оформление заказа.</div><input type="hidden" id="full-address" value=""><input type="hidden" id="geo_lat" value=""><input type="hidden" id="geo_lon" value=""><input type="hidden" id="coords" value=""></div>');
	fields.before(address);
	var mapwrapper = $('<div id="mapwrapper" class="mapwrapper"></div>');
	address.after(mapwrapper);
	var map = $('<div id="map" class="map" style="display:none;"></div>');
	mapwrapper.prepend(map);
	var foreign = $('<div id="foreign" class="foreign" style="display:none;"> <strong>Мы работаем с крупнейшими транспортными компаниями, среди которых: <br> <div><img src="catalog/view/theme/oct_ultrastore/image/pictures/delivery/advantages1.png" alt="В офисе нашего магазина"><img src="catalog/view/theme/oct_ultrastore/image/pictures/delivery/advantages2.png" alt="На сайте нашего онлайн-ресурса"><img src="catalog/view/theme/oct_ultrastore/image/pictures/delivery/advantages33.png" alt="Через банковский кабинет онлайн"><img src="catalog/view/theme/oct_ultrastore/image/pictures/delivery/advantages55.png" alt="Через мобильное приложение"></div> <br><br>Точная стоимость доставки будет рассчитана индивидуально менеджером, в зависимости от того, какая транспортная компания будет выбрана. <br><br>Продолжите, пожалуйста, Ваше оформление заказа.</p></div>');
	mapwrapper.append(foreign);
	
	if ($('#shipping_address_zone_id').val() == 0) {
		shipping.css('display', 'none');
	}
	
	if (!$('#shipping_address_country_id').val()) {
		$('#dadata_address').css('display', 'none');
		$('.row-shipping_address_postcode').css('display', 'none');
		$('.row-shipping_address_zone_id').css('display', 'none');
		$('.row-shipping_address_city').css('display', 'none');
		$('.row-shipping_address_address_1').css('display', 'none');
		$('.row-shipping_address_address_2').css('display', 'none');
	} else if ($('#shipping_address_country_id').val() == 643) {
		$('#foreign').css('display', 'none');
		
		$('#dadata_address').css('display', 'block');
		$('.row-shipping_address_postcode').css('display', 'none');
		$('.row-shipping_address_zone_id').css('display', 'none');
		$('.row-shipping_address_city').css('display', 'none');
		$('.row-shipping_address_address_1').css('display', 'none');
		$('.row-shipping_address_address_2').css('display', 'none');
		
		if (getCookie('da_geo_lat')&&getCookie('da_geo_lon')) {
			$('#no_coords').css('display', 'none');
		} else {
			if (getCookie('da_full-address')) $('#no_coords').css('display', 'block');
		}
		
	} else {
		$('#foreign').css('display', 'block');
		$('#dadata_address').css('display', 'none');
		$('.row-shipping_address_postcode').css('display', 'block');
		$('.row-shipping_address_zone_id').css('display', 'block');
		$('.row-shipping_address_city').css('display', 'block');
		$('.row-shipping_address_address_1').css('display', 'block');
		$('.row-shipping_address_address_2').css('display', 'block');
	}
	
	$('#address').val(getCookie('da_full-address'));
	$('#full-address').val(getCookie('da_full-address'));
	$('#geo_lat').val(getCookie('da_geo_lat'));
	$('#geo_lon').val(getCookie('da_geo_lon'));
	$('#coords').val(getCookie('da_coords'));
	
	$('#shipping_address_postcode').val(getCookie('da_postcode'));
	$('#shipping_address_zone_id').val(getCookie('da_zone_id'));
	$('#shipping_address_city').val(getCookie('da_city'));
	$('#shipping_address_address_1').val(getCookie('da_address_1'));
	$('#shipping_address_address_2').val(getCookie('da_address_2'));
	
	if ($('#shipping_address_postcode').val()) $('.row-shipping_address_postcode').css('display', 'block');
	if ($('#shipping_address_zone_id').val()) $('.row-shipping_address_zone_id').css('display', 'block');
	if ($('#shipping_address_city').val()) $('.row-shipping_address_city').css('display', 'block');
	if ($('#shipping_address_address_1').val()) $('.row-shipping_address_address_1').css('display', 'block');
	if ($('#shipping_address_address_2').val()) $('.row-shipping_address_address_2').css('display', 'block');
	
	$("#address").suggestions({
		token: "80cbd489c130aaaf02e6389880b14599341b8289",
		type: "ADDRESS",
		onSelect: function(suggestion) {
			
			shipping.css('display', 'block');

			$('.row-shipping_address_postcode').css('display', 'block');
			$('.row-shipping_address_zone_id').css('display', 'block');
			$('.row-shipping_address_city').css('display', 'block');
			$('.row-shipping_address_address_1').css('display', 'block');
			$('.row-shipping_address_address_2').css('display', 'block');
			
			if (suggestion.data.geo_lat&&suggestion.data.geo_lon) {
				$('#no_coords').css('display', 'none');
			} else {
				$('#no_coords').css('display', 'block');
			}

			$('#full-address').val(suggestion.value);
			$('#geo_lat').val(suggestion.data.geo_lat);
			$('#geo_lon').val(suggestion.data.geo_lon);
			$('#coords').val(suggestion.data.geo_lat+','+suggestion.data.geo_lon);
			$('#shipping_address_postcode').val(suggestion.data.postal_code);
			
			var reg = suggestion.data.region_kladr_id;
			if (reg.charAt(0) == '0') {reg=reg.substring(1,2)}else{reg=reg.substring(0,2)}
			$('#shipping_address_zone_id').val(reg);
			$("#shipping_address_zone_id option[value='+reg+']").attr("selected", "selected");
			
			$('#shipping_address_city').val(suggestion.data.city_with_type);
			
			var address_1 = '';
			if (suggestion.data.area_with_type) address_1 += suggestion.data.area_with_type;
			if (suggestion.data.settlement_with_type) address_1 += (', ' + suggestion.data.settlement_with_type);
			$('#shipping_address_address_1').val(address_1);
			
			var address_2 = '';
			if (suggestion.data.street_with_type) address_2 += suggestion.data.street_with_type;
			if (suggestion.data.house) address_2 += (', ' + suggestion.data.house_type + '. ' + suggestion.data.house);
			if (suggestion.data.block) address_2 += (', ' + suggestion.data.block_type + '. ' + suggestion.data.block);
			if (suggestion.data.flat) address_2 += (', ' + suggestion.data.flat_type + '. ' + suggestion.data.flat);
			$('#shipping_address_address_2').val(address_2);
			
			if ($('#shipping_address_postcode').val()) $('.row-shipping_address_postcode').css('display', 'block');
			if ($('#shipping_address_zone_id').val()) $('.row-shipping_address_zone_id').css('display', 'block');
			if ($('#shipping_address_city').val()) $('.row-shipping_address_city').css('display', 'block');
			if ($('#shipping_address_address_1').val()) $('.row-shipping_address_address_1').css('display', 'block');
			if ($('#shipping_address_address_2').val()) $('.row-shipping_address_address_2').css('display', 'block');
			
			document.cookie = 'da_full-address=' + $('#full-address').val();
			document.cookie = 'da_geo_lat=' + $('#geo_lat').val();
			document.cookie = 'da_geo_lon=' + $('#geo_lon').val();
			document.cookie = 'da_coords=' + $('#coords').val();
			
			document.cookie = 'da_postcode=' + $('#shipping_address_postcode').val();
			document.cookie = 'da_old_zone_id=' + $('#shipping_address_zone_id').val();// убрать?
			document.cookie = 'da_zone_id=' + $('#shipping_address_zone_id').val();
			document.cookie = 'da_city=' + $('#shipping_address_city').val();
			document.cookie = 'da_address_1=' + $('#shipping_address_address_1').val();
			document.cookie = 'da_address_2=' + $('#shipping_address_address_2').val();

			$("#shipping_address_zone_id").trigger("change");
			$('#coords').trigger('change');
			
		}
	});
	
	if ($('#coords').val()) {
		$('#coords').trigger('change');
	}
	
	if (document.location.href.indexOf("#step_3") + 1) {
		
		var total_shipping_delivery = getCookie('da_total_shipping_delivery');
		var total_total = getCookie('da_total_total');
		
		if (total_shipping_delivery) {
			$('#simplecheckout_form_0 #total_shipping').html('<span><b>Доставка по Москве и области (+50₽ за каждый километр от МКАД):</b></span> <span class="simplecheckout-cart-total-value">' + total_shipping_delivery + ' руб.</span>');
		}
		if (total_total) {
			$('#simplecheckout_form_0 #total_total').html('<span><b>Итого:</b></span> <span class="simplecheckout-cart-total-value">' + total_total + ' руб.</span>')
		}
		
	}

	if (document.location.href.indexOf('simplecheckout#step_2') + 1) {
		ymaps.ready(init);
	}
	
});

$("#shipping_address_country_id").on("change", function() {
	
	$('#shipping_address_zone_id').val('');
	$('#shipping_address_postcode').val('');
	$('#shipping_address_city').val('');
	$('#shipping_address_address_1').val('');
	$('#shipping_address_address_2').val('');
	
	if ($('#shipping_address_country_id').val() == 643) {
		$('#dadata_address').css('display', 'block');
		$('#foreign').css('display', 'none');
	} else {
		$('#dadata_address').css('display', 'none');
		$('#map').css('display', 'none');
		$('#foreign').css('display', 'block');
		$('.row-shipping_address_postcode').css('display', 'block');
		$('.row-shipping_address_zone_id').css('display', 'block');
		$('.row-shipping_address_city').css('display', 'block');
		$('.row-shipping_address_address_1').css('display', 'block');
		$('.row-shipping_address_address_2').css('display', 'block');
	}
	
	document.cookie = 'da_zone_id=';
	document.cookie = 'da_full-address=';
	document.cookie = 'da_geo_lat=';
	document.cookie = 'da_geo_lon=';
	document.cookie = 'da_coords=';
	
	document.cookie = 'da_country_id=' + $('#shipping_address_country_id').val();
	document.cookie = 'da_postcode=';
	document.cookie = 'da_zone_id=';
	document.cookie = 'da_city=';
	document.cookie = 'da_address_1=';
	document.cookie = 'da_address_2=';
	
	document.cookie = 'da_distance=';
	document.cookie = 'da_delivery=';
	document.cookie = 'da_total_shipping_delivery=';
	//document.cookie = 'da_total_shipping=';
	
});

$("#shipping_address_zone_id").on("change", function() {
	
	console.log('shipping_address_zone_id change root');
	
	if(getCookie('da_old_zone_id') != $("#shipping_address_zone_id").val()) {
		$('#shipping_address_postcode').val('');
		$('#shipping_address_city').val('');
		$('#shipping_address_address_1').val('');
		$('#shipping_address_address_2').val('');
		
		document.cookie = 'da_full-address=' + $("#shipping_address_zone_id option:selected").text();
		document.cookie = 'da_geo_lat=';
		document.cookie = 'da_geo_lon=';
		document.cookie = 'da_coords=';
		
		document.cookie = 'da_postcode=';
		document.cookie = 'da_zone_id=' + $('#shipping_address_zone_id').val();
		document.cookie = 'da_city=';
		document.cookie = 'da_address_1=';
		document.cookie = 'da_address_2=';
	
		document.cookie = 'da_distance=';
		document.cookie = 'da_delivery=';
		document.cookie = 'da_total_shipping_delivery=';
		//document.cookie = 'da_total_shipping=';
	
	}
			
});

$('#simplecheckout_button_prev').on('click', function() {
		ymaps.ready(init);
});
$('.simple-step').on('click', function() {
		ymaps.ready(init);
});

$('#coords').bind('change', function() {
	if (finishPlacemark) {myMap.geoObjects.remove(finishPlacemark);}
	finishCoords = [$('#geo_lat').val(),$('#geo_lon').val()];
	finishPlacemark = createPlacemark(finishCoords);
	myMap.geoObjects.add(finishPlacemark);
	setRoute(finishCoords);
});

function getCookie ( cookie_name ) {
  var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');
	return results ? unescape(results[2]) : null;
}

function init() {
	
	if ($('#shipping_address_country_id').val() != 643) {
		return;
	}
	if (!$('#geo_lat').val()||!$('#geo_lat').val()) {
		return;
	}
	
	$('#map').css('display', 'block');
	$('#foreign').css('display', 'none');

	var startCoords = [55.752, 37.616], 
			showStartPlacemark = false,
			showTownBound = true,
			showRegionBound = true,
			showTownRoute = false,
			boundTown = 'dadayama/mkad-out.json',
			boundRegion = 'dadayama/mosobl.json',
			colorTown = '#f00',
			colorRegion = '#00f',
			colorOut = '#b0d';
	var distanceInt,
			distanceOut,
			finishCoords, 
			finishPlacemark, 
			myEdges,
			townPolygon,
			regionPolygon;
			
	if ($("#geo_lat")&&$("#geo_lon")) {
		var center = [$('#geo_lat').val(),$('#geo_lon').val()];
	} else {
		var center = startCoords;
	}
			
	myMap = new ymaps.Map('map', {
			center: center,
			zoom: 9,
			type: 'yandex#map',
			controls: ['zoomControl']
	});
			
	if ($('#coords').val()) {
		setTimeout(function() {$('#coords').trigger('change');}, 2000);
	}
	
	$('#coords').bind('change', function() {
		if (finishPlacemark) {myMap.geoObjects.remove(finishPlacemark);}
		finishCoords = [$('#geo_lat').val(),$('#geo_lon').val()];
		finishPlacemark = createPlacemark(finishCoords);
		myMap.geoObjects.add(finishPlacemark);
		setRoute(finishCoords);
	});
	
	if (showStartPlacemark) {
		var startPlacemark = new ymaps.Placemark(startCoords, {
			balloonContent: 'Склад компании <strong>СДМ Климат</strong>'
		},{
			preset: 'islands#circleDotIcon',
			iconColor: '#3caa3c'
		});
		myMap.geoObjects.add(startPlacemark);
	}
			
	function createPlacemark(finishCoords) {
		return new ymaps.Placemark(finishCoords, {
			iconCaption: 'поиск...'
		}, {
			preset: 'islands#circleDotIcon',
			draggable: false //true
		});
		setRoute(finishCoords);
	}

	function setRoute(finishCoords) {

		var region = ymaps.geoQuery(finishPlacemark).searchInside(regionPolygon).getLength();
		var town = ymaps.geoQuery(finishPlacemark).searchInside(townPolygon).getLength();

		if (myEdges) {myEdges.removeFromMap(myMap);}
		
		var zoom = $('#shipping_address_address_2').val() == '' ? 9 : 15;
		
		if (town) {
			finishPlacemark.properties
				.set({
					iconCaption: '',
					balloonContent: 'В Москве доставим заказ до двери.'
				});
			finishPlacemark.options
				.set({
					preset: 'islands#circleDotIcon',
					iconColor: colorTown
				});
			myMap.setCenter(finishCoords, zoom);
			calcDelivery(0);
		} else if (region) {
			
			finishPlacemark.properties
				.set({
					iconCaption: ''
				});
			finishPlacemark.options
				.set({
					preset: 'islands#circleDotIcon',
					iconColor: colorRegion
				});
				
			if ($('input[name="shipping_method_current"]').val() == 'xshippingpro.xshippingpro1') {
				
				var start = startCoords;
				var finish = finishPlacemark.geometry.getCoordinates();
				var minX = start[0] < finish[0] ? start[0] : finish[0];
				var maxX = start[0] > finish[0] ? start[0] : finish[0];
				var minY = start[1] < finish[1] ? start[1] : finish[1];
				var maxY = start[1] > finish[1] ? start[1] : finish[1];
				
				myMap.setBounds([[minX,minY],[maxX,maxY]]);
				
				ymaps.route([startCoords, finishCoords]).then(function(router){
						var pathsObjects = ymaps.geoQuery(router.getPaths()), edges = [];
						pathsObjects.each(function(path){
								var coordinates = path.geometry.getCoordinates();
								for (var i = 1, l = coordinates.length; i < l; i++) {
										edges.push({
												type: 'LineString',
												coordinates: [coordinates[i], coordinates[i - 1]]
										});
								}
						});
	
						myEdges = ymaps.geoQuery(edges);
						var routeObjects = myEdges.addToMap(myMap);
						
						var objectsIn = routeObjects.searchInside(townPolygon);
						objectsIn.setOptions({
								strokeColor: colorTown,
								opacity: showTownRoute?0.9:0,
								strokeWidth: 2
					});
						
						var objectsOut = routeObjects.remove(objectsIn);
						objectsOut.setOptions({
								strokeColor: colorRegion,
								opacity: 0.9,
								strokeWidth: 2
						});
	
						var distanceOut = 0;
						objectsOut.each(function (obj) {
						distanceOut += ymaps.coordSystem.geo.getDistance(obj.geometry.getCoordinates()[0], obj.geometry.getCoordinates()[1]);							
						});
						
						distanceOut = Math.ceil(distanceOut/1000);
						calcDelivery(distanceOut);
					},
					this); 
				
			} else {
				
				calcDelivery(0);
				
			}
				
		} else {
			finishPlacemark.properties
				.set({
					iconCaption: ''
				});
			finishPlacemark.options
				.set({
					preset: 'islands#circleDotIcon',
					iconColor: colorOut
				});
			myMap.setCenter(finishCoords, zoom); 
			calcDelivery(0);
		}
		setTimeout(function(){
			finishPlacemark.balloon.open();
		},3000);

  }
		
	function townPolygonLoad (json) {
        townPolygon = new ymaps.Polygon(json.coordinates);
				townPolygon.options.set('interactivityModel', 'default#transparent');
				townPolygon.options.set('visible', showTownBound);
				townPolygon.options.set('fillColor', '#FF000000');
				townPolygon.options.set('strokeColor', '#ff000099');
				townPolygon.options.set('strokeWidth', '2');
        myMap.geoObjects.add(townPolygon);
    }
    
	function regionPolygonLoad (json) {
        regionPolygon = new ymaps.Polygon(json.coordinates);
				regionPolygon.options.set('interactivityModel', 'default#transparent');
				regionPolygon.options.set('visible', showRegionBound);
				regionPolygon.options.set('fillColor', '#00FF0000');
				regionPolygon.options.set('strokeColor', '#00ff0099');
				regionPolygon.options.set('strokeWidth', '2');
        myMap.geoObjects.add(regionPolygon);
    }
    
	$.ajax({url:boundRegion,dataType:'json',success:regionPolygonLoad});
		
	$.ajax({url:boundTown,dataType:'json',success:townPolygonLoad});
	
	function calcDelivery(distance) {
		
		var total_sub_total = $('#total_sub_total').text();
		total_sub_total = total_sub_total.split(':')[1];
		total_sub_total = total_sub_total.trim();
		total_sub_total = total_sub_total.replace(' руб.', '');
		total_sub_total = total_sub_total.replace(' ', '');
		document.cookie = 'da_total_sub_total=' + total_sub_total;
		
		var total_shipping = 0;
		if (total_sub_total < 10000) total_shipping = 300;
		if (total_sub_total < 5000) total_shipping = 300;
		document.cookie = 'da_total_shipping=' + total_shipping;
		
		if ($('input[name="shipping_method_current"]').val() == 'xshippingpro.xshippingpro1') {
			
			var radio = $('#xshippingpro.xshippingpro1');

			if (distance > 0) {
				
				document.cookie = 'da_distance=' + distance;
				document.cookie = 'da_delivery=' + distance * 50;
				
				total_shipping_delivery = Number(total_shipping) + distance * 50;
				$('#total_shipping').html('<span><b>Доставка по Москве и области (+50₽ за каждый километр от МКАД):</b></span> <span class="simplecheckout-cart-total-value">' + total_shipping_delivery +' руб.</span>')
				document.cookie = 'da_total_shipping_delivery=' + total_shipping_delivery;
				
				finishPlacemark.properties.set({
					iconCaption: '',
					balloonContent: 'Доставка по Москве - ' + total_shipping + ' руб. + за МКАД ' + distance +' км * 50 руб.<br>Всего ' + total_shipping_delivery + ' руб.'
				});
				
				var total_total = $('#total_total').text();
				total_total = Number(total_total.split(':')[1].trim().replace(' руб.', '').replace(' ', '')) + distance * 50;
				document.cookie = 'da_total_total=' + total_total;
				$('#total_total').html('<span><b>Итого:</b></span> <span class="simplecheckout-cart-total-value">' + total_total + ' руб.</span>')
				
				$( "label[for~='xshippingpro.xshippingpro1']" ).html('<input type="radio" data-onchange="reloadAll" name="shipping_method" value="xshippingpro.xshippingpro1" id="xshippingpro.xshippingpro1" checked="checked">Доставка по Москве - ' + total_shipping + ' руб. + за МКАД ' + distance +' км * 50 руб. Всего ' + total_shipping_delivery + ' руб.');
				
			} else {
				document.cookie = 'da_distance=';
				document.cookie = 'da_delivery=';
			}
			
		} else {
			
			document.cookie = 'da_distance=';
			document.cookie = 'da_delivery=';
			document.cookie = 'da_total_shipping_delivery=';
			document.cookie = 'da_total_total=';
			
			finishPlacemark.properties.set({
				iconCaption: '',
				balloonContent: 'Доставка до терминала транспорной <br>компании в Москве - ' + total_shipping + ' руб.'
			});
		}
	}
}

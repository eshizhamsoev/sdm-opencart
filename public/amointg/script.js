var thousandSeparator = function(str) {
    var parts = (str + '').split('.'),
        main = parts[0],
        len = main.length,
        output = '',
        i = len - 1;
    
    while(i >= 0) {
        output = main.charAt(i) + output;
        if ((len - i) % 3 === 0 && i > 0) {
            output = '\'' + output;
        }
        --i;
    }

    if (parts.length > 1) {
        output += '.' + parts[1];
    }
    return output;
};

function updateReport() {
	
	var filters = new Object();
	$(".the_filter").each(function(){
		if ($(this).attr("multiple")=="multiple") {
			var tmpval = "";
			if ($(this).val()!=null) {
			for (var i=0; i<$(this).val().length; i++) {				
				tmpval+=$(this).val()[i]+",";
			}
			tmpval = tmpval.substring(0,tmpval.length-1);
			}
			filters[$(this).attr("id")]=tmpval;
		}
		else
			filters[$(this).attr("id")]=$(this).val();
	});
	console.log(JSON.stringify(filters));
	
	$.ajax({
		url: "https://sdmtable.000webhostapp.com/functions.php",
		type: "GET",
		data: {"filters" : JSON.stringify(filters)},
		success: function(resdata) {
			
			var rObj = JSON.parse(resdata);
			var textres = "<table class='res_table'>";
			textres+="<thead>";
			textres+="<tr><td rowspan='2'></td>";
			var month_select ="<option value=''>не выбран</option>";
			var msumm = new Array(rObj["months"].length+1);
			for (var i=0; i<rObj["months"].length; i++) {
				textres+="<td colspan='3'>"+rObj["months"][i]+"</td>";	
				msumm[i]=[0,0,0,];
				month_select+="<option value='"+rObj["months"][i]+"'>"+rObj["months"][i]+"</option>";
			}
			if ($("#month_from").html()=="") {
				$("#month_from").html(month_select);
				$("#month_to").html(month_select);
			}
			console.log($("#month_to"));
			msumm[rObj["months"].length]=[0,0,0,];
			
			
			var manager_select ="<option value=''>не выбран</option>";
			for (var i=0; i<rObj["users"].length; i++) {
				manager_select+="<option value='"+rObj["users"][i]["ID"]+"'>"+rObj["users"][i]["NAME"]+"</option>";
			}
			if ($("#manager").html()=="") {
				$("#manager").html(manager_select);
			}
			
			textres+="<th colspan='3'>Итого</th>";
			textres+="</tr>";
			textres+="<tr>";
			for (var i=0; i<rObj["months"].length; i++) {
				textres+="<th>Маржа</th>";
				textres+="<th>Сумма</th>";
				textres+="<th>Проигр</th>";
			}
				textres+="<th>Маржа</th>";
				textres+="<th>Сумма</th>";
				textres+="<th>Проигр</th>";
			textres+="</tr>";
			textres+="</thead>";
			textres+="<tbody>";
			var ressum = [0,0,0,];
			for (var i=0; i<rObj["users"].length; i++) {
				var sum = [0,0,0,];
				textres+="<tr><td>"+rObj["users"][i]["NAME"]+"</td>";			
				for (var k=0; k<rObj["months"].length; k++) {	
					var sum_m = [0,0,0,];
					for (var n=0; n<rObj["status"].length; n++) {
						var empty= true;
						for (var j=0; j<rObj["leads_stat"].length; j++) {
							if ((rObj["leads_stat"][j]["RESP_USER"]==rObj["users"][i]["ID"])&&(rObj["leads_stat"][j]["LDATE"]==rObj["months"][k])&&(rObj["leads_stat"][j]["STYPE"]==rObj["status"][n])) {
								empty=false;
								if (rObj["status"][n]==0) {
									textres+="<td>"+thousandSeparator(rObj["leads_stat"][j]["SPROFIT"])+"</td>";
									sum[0]+=rObj["leads_stat"][j]["SPROFIT"]*1;				
									sum_m[0]+=rObj["leads_stat"][j]["SPROFIT"]*1;								
								}
								textres+="<td>"+thousandSeparator(rObj["leads_stat"][j]["SPRICE"])+"</td>";	
								sum[n+1]+=rObj["leads_stat"][j]["SPRICE"]*1;		
								sum_m[n+1]+=rObj["leads_stat"][j]["SPRICE"]*1;	
							}
						}
						if (empty) {textres+="<td>0</td>";	
							if (rObj["status"][n]==0) {textres+="<td>0</td>";	}
						}
					}
					for (var m=0; m<=rObj["status"].length; m++) {
						msumm[k][m]+=sum_m[m];
					}						
				}
				for (var m=0; m<=rObj["status"].length; m++) {
					textres+="<th>"+thousandSeparator(sum[m])+"</th>";
					ressum[m]+=sum[m];	
					msumm[rObj["months"].length][m]+=sum[m];
				}
				textres+="</tr>";
			}
			textres+="</tbody>";
			textres+="<tfoot>";
			textres+="<tr><th>Итого</th>";
			for (var m=0; m<msumm.length; m++) {
				for (var p=0; p<msumm[m].length; p++) {
					textres+="<th>"+thousandSeparator(msumm[m][p])+"</th>";
				}
			}			
			textres+="</tr>";
			textres+="</tfoot>";
			textres += "</table>";
			$("#res_table").html(textres);
		},
		error: function(resdata) {
			console.log("no");
		}
	});
	
}


$(document).ready(function(){
	updateReport();
	$("#apply_filters").click(function(){updateReport()});
	$("#clear_filters").click(function(){$(".the_filter").val(""); updateReport()});
});
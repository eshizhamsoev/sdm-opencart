<?php

class ModelExtensionModuleOzonSeller extends Model
{

    public function getOzonCategory($data = array())
    {
        $ozon_category_id = implode(',', $data);

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_category WHERE ozon_category_id IN ("
            .$ozon_category_id.")");

        return $query->rows;
    }

    public function searchOzonCategory($data = array())
    {
        $sql = "SELECT * FROM ".DB_PREFIX."ozon_category";

        if ( ! empty($data['filter_name'])) {
            $sql .= " WHERE title LIKE '%".$this->db->escape($data['filter_name'])."%'";
        }

        $sort_data = array(
            'title',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY ".$data['sort'];
        } else {
            $sql .= " ORDER BY title";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function searchOzonDictionary($data = array())
    {
        $sql = "SELECT * FROM ".DB_PREFIX."ozon_dictionary";

        if ( ! empty($data['ozon_dictionary_id'])) {
            $sql .= " WHERE dictionary_id = '".$this->db->escape($data['ozon_dictionary_id'])."' AND text LIKE '%"
                .$this->db->escape($data['filter_name'])."%'";
        }

        $sort_data = array(
            'text',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY ".$data['sort'];
        } else {
            $sql .= " ORDER BY text";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOzonDictionaryNoCategory()
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX
            ."ozon_dictionary WHERE category_id != 0 ORDER BY text ASC");

        return $query->rows;
    }

    public function getOzonDictionary($ozon_dictionary_id)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_dictionary WHERE dictionary_id = '"
            .$ozon_dictionary_id."' ORDER BY text ASC");

        return $query->rows;
    }

    public function getOzonDictionaryType($category_ozon)
    {
        $query = $this->db->query("SELECT * FROM `".DB_PREFIX."ozon_dictionary` WHERE `category_id` = '".$category_ozon
            ."'");

        return $query->rows;
    }

    public function getShopDictionary($shop_attribute_id)
    {

        $query = $this->db->query("SELECT MIN(`product_id`) `product_id`, `attribute_id`, `text` FROM `".DB_PREFIX
            ."product_attribute` WHERE `attribute_id` = '".$shop_attribute_id
            ."' GROUP BY `attribute_id`, `text` ORDER BY `text` ASC");

        return $query->rows;
    }

    public function getOzonAttribute()
    {
        $query = $this->db->query("SELECT * FROM `".DB_PREFIX."ozon_attribute`");

        return $query->rows;
    }

    public function getOzonAttributeDescription()
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_attribute_description ORDER BY ozon_attribute_name");

        return $query->rows;
    }

    public function truncateAttribute()
    {
        $this->db->query("TRUNCATE ".DB_PREFIX."ozon_attribute");
        $this->db->query("TRUNCATE ".DB_PREFIX."ozon_attribute_description");
        $this->db->query("TRUNCATE ".DB_PREFIX."ozon_attribute_required");
        $this->db->query("TRUNCATE ".DB_PREFIX."ozon_dictionary");
    }

    public function saveDictionary(
        $ozon_attribute_id,
        $shop_attribute_id,
        $dictionary_value_id,
        $value,
        $text_shop_attribute
    ) {
        $this->db->query("REPLACE INTO `".DB_PREFIX."ozon_to_shop_dictionary` SET `ozon_attribute_id` = '"
            .$this->db->escape($ozon_attribute_id)."', `dictionary_value_id` = '".$dictionary_value_id."', `value` = '"
            .$this->db->escape($value)."', `text_shop_attribute` = '".$this->db->escape($text_shop_attribute)
            ."', `shop_attribute_id` = '".$this->db->escape($shop_attribute_id)."'");
    }

    public function unsetDictionary($ozon_attribute_id, $shop_attribute_id)
    {
        $this->db->query("DELETE FROM  ".DB_PREFIX."ozon_to_shop_dictionary WHERE ozon_attribute_id = '"
            .$ozon_attribute_id."' AND shop_attribute_id = '".$shop_attribute_id."'");
    }

    public function deleteDictionaryValue($dictionary_value_id)
    {
        $this->db->query("DELETE FROM  ".DB_PREFIX."ozon_to_shop_dictionary WHERE dictionary_value_id = '"
            .$dictionary_value_id."'");
    }

    public function getDictionaryShoptoOzon()
    {
        $query = $this->db->query("SELECT * FROM `".DB_PREFIX."ozon_to_shop_dictionary`");

        return $query->rows;
    }

    public function getDictionaryShoptoOzonDictionaryId($value_id)
    {
        $query = $this->db->query("SELECT * FROM `".DB_PREFIX."ozon_to_shop_dictionary` WHERE dictionary_value_id = '"
            .$this->db->escape($value_id)."'");

        return $query->rows;
    }

    public function getTotalProducts($data = array())
    {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM ".DB_PREFIX."ozon_products p LEFT JOIN ".DB_PREFIX
            ."product_description pd ON (p.product_id = pd.product_id)";

        $sql .= " WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."'";

        if ( ! empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%".$this->db->escape($data['filter_name'])."%'";
        }

        if ( ! empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '".$this->db->escape($data['filter_model'])."%'";
        }

        if ( ! empty($data['filter_sku'])) {
            $sql .= " AND p.sku LIKE '".$this->db->escape($data['filter_sku'])."%'";
        }

        if ( ! empty($data['filter_status'])) {
            $sql .= " AND p.status = '".$this->db->escape($data['filter_status'])."'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getProducts($data = array())
    {
        $sql = "SELECT * FROM ".DB_PREFIX."ozon_products p LEFT JOIN ".DB_PREFIX
            ."product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '"
            .(int)$this->config->get('config_language_id')."'";

        if ( ! empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%".$this->db->escape($data['filter_name'])."%'";
        }

        if ( ! empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '".$this->db->escape($data['filter_model'])."%'";
        }

        if (isset($data['filter_sku']) && ! is_null($data['filter_sku'])) {
            $sql .= " AND p.sku LIKE '".$this->db->escape($data['filter_sku'])."%'";
        }

        if ( ! empty($data['filter_status'])) {
            $sql .= " AND p.status = '".$this->db->escape($data['filter_status'])."'";
        }

        $sql .= " GROUP BY p.product_id";

        $sort_data = array(
            'pd.name',
            'p.sku',
            'p.model',
            'p.status',
            'p.date',
            'p.sort_order',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY ".$data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function deletedExportProduct($product_id)
    {
        $this->db->query("DELETE FROM ".DB_PREFIX."ozon_products WHERE product_id = '".$product_id."'");
    }

    public function deletedExportProductStatus($status)
    {
        $this->db->query("DELETE FROM ".DB_PREFIX."ozon_products WHERE status = '".$status."'");
    }

    public function getManufacturer($shop_id)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_manufacturer WHERE shop_id = '"
            .$this->db->escape($shop_id)."'");

        return $query->rows;
    }

    public function updateManufacturer($shop_id, $ozon_id)
    {

        if ($ozon_id == 'delete') {

            $this->db->query("UPDATE ".DB_PREFIX."ozon_manufacturer SET shop_id = '' WHERE shop_id = '"
                .$this->db->escape($shop_id)."'");

        } else {

            $this->db->query("UPDATE ".DB_PREFIX."ozon_manufacturer SET shop_id = '".$this->db->escape($shop_id)
                ."' WHERE ozon_id = '".(int)$ozon_id."'");
        }
    }

    public function searchOzonManufacturer($data = array())
    {
        $sql = "SELECT * FROM ".DB_PREFIX."ozon_manufacturer";

        if ( ! empty($data['filter_name'])) {
            $sql .= " WHERE value LIKE '".$this->db->escape($data['filter_name'])."%'";
        }

        $sort_data = array(
            'value',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY ".$data['sort'];
        } else {
            $sql .= " ORDER BY value";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalOrders($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM ".DB_PREFIX."ozon_ms_order_guid";

        if (isset($data['filter_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "status = '".$order_status_id."'";
            }

            if ($implode) {
                $sql .= " WHERE (".implode(" OR ", $implode).")";
            }
        } else {
            $sql .= " WHERE status != ''";
        }

        if ( ! empty($data['filter_posting_number'])) {
            $sql .= " AND posting_number  LIKE '%".$this->db->escape($data['filter_posting_number'])."%'";
        }

        if ( ! empty($data['filter_shipment_date'])) {
            $sql .= " AND DATE(shipment_date) = DATE('".$this->db->escape($data['filter_shipment_date'])."')";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getOrders($data = array())
    {
        $sql = "SELECT * FROM ".DB_PREFIX."ozon_ms_order_guid";

        if (isset($data['filter_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "status = '".$order_status_id."'";
            }

            if ($implode) {
                $sql .= " WHERE (".implode(" OR ", $implode).")";
            }
        } else {
            $sql .= " WHERE status != ''";
        }

        if ( ! empty($data['filter_posting_number'])) {
            $sql .= " AND posting_number  LIKE '%".$this->db->escape($data['filter_posting_number'])."%'";
        }

        if ( ! empty($data['filter_shipment_date'])) {
            $sql .= " AND DATE(shipment_date) = DATE('".$this->db->escape($data['filter_shipment_date'])."')";
        }

        $sort_data = array(
            'posting_number',
            'status',
            'shipment_date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY ".$data['sort'];
        } else {
            $sql .= " ORDER BY shipment_date";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAttributeRequired($category_id)
    {

        $query = $this->db->query("SELECT attribute_id FROM ".DB_PREFIX."ozon_attribute_required WHERE category_id = '"
            .(int)$category_id."'");

        return $query->rows;
    }

    public function getProductByModel($model)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE model = '".$this->db->escape($model)."'");

        return $query->rows;
    }

    public function getProductBySku($sku)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE sku = '".$this->db->escape($sku)."'");

        return $query->rows;
    }

    public function getAllCategories($data = [])
    {
        $sql
            = "SELECT cp.category_id AS category_id, cd2.name AS name, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS fullname, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from "
            .DB_PREFIX."product_to_category pc where pc.category_id = c1.category_id) as product_count FROM ".DB_PREFIX
            ."category_path cp LEFT JOIN ".DB_PREFIX."category c1 ON (cp.category_id = c1.category_id) LEFT JOIN "
            .DB_PREFIX."category c2 ON (cp.path_id = c2.category_id) LEFT JOIN ".DB_PREFIX
            ."category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN ".DB_PREFIX
            ."category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = '1' AND cd1.language_id = '"
            .(int)$this->config->get('config_language_id')."' AND cd2.language_id = '"
            .(int)$this->config->get('config_language_id')."'";

        if ( ! empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '".$this->db->escape($data['filter_name'])."%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = [
            'product_count',
            'fullname',
            'sort_order',
        ];

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY ".$data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getSuppliers()
    {
        $query = $this->db->query("SELECT DISTINCT `supplier_main` FROM ".DB_PREFIX
            ."product WHERE `supplier_main` != ''");

        return $query->rows;
    }

    public function prepareFilter(array $filter, ?array $categories, ?array $manufacturers): array
    {
        $categoriesArr = [];
        foreach ($categories as $category){
            $categoriesArr[$category['category_id']] = $category['fullname'];
        }
        
        $manufacturersArr = [];
        foreach ($manufacturers as $manufacturer){
            $manufacturersArr[$manufacturer['manufacturer_id']] = $manufacturer['name'];
        }
        
        $ozon_seller_filter = [];
        if ($filter) {
            foreach ($filter as $item) {
                array_walk($item, function (&$el, $k) use ($categoriesArr, $manufacturersArr) {
                    /** если это настройка категорий или брендов - получаем данные для вывода в форме */
                    if (in_array($k, ['categories', 'manufacturers', 'suppliers'])) {
                        $items = [];
                        switch ($k) {
                            case 'categories':
                                foreach ($el as $id) {
                                    $items[] = [
                                        'category_id' => $id,
                                        'name'        => $categoriesArr[$id],
                                    ];
                                }
                                break;
                            case 'manufacturers':
                                foreach ($el as $id) {
                                    $items[] = [
                                        'manufacturer_id' => $id,
                                        'name'            => $manufacturersArr[$id],
                                    ];
                                }
                                break;
                            case 'suppliers':
                                foreach ($el as $value) {
                                    $items[] = html_entity_decode($value);
                                }
                                break;
                        }

                        $el = $items;
                    } else {
                        $el = html_entity_decode($el);
                    }
                });

                $ozon_seller_filter[] = $item;
            }
        }

        return $ozon_seller_filter;
    }
    
    public function getAttributeNamesById(array $attribute_ids): array
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_id IN (" . implode(',', $attribute_ids) . ") AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $attributes = [];
        foreach ($query->rows as $attribute){
            $attributes[$attribute['attribute_id']] = $attribute['name'];
        }
        
        return $attributes;
    }
    
    public function prepareAdditionalSettings(array $settings): array
    {
        $additional_settings = [];
        if($settings){
            foreach ($settings as $key => $item){
                if($key === 'attributes'){
                    $attributes = [];
                    $attributeNames = $this->getAttributeNamesById($item);
                    
                    foreach ($item as $value){
                        $attributes[] = [
                            'attribute_id' => $value,
                            'name' => $attributeNames[$value]
                        ];
                    }
                    
                    $item = $attributes;
                }

                $additional_settings[$key] = $item;
            }
        }
        
        return $additional_settings;
    }

    public function install()
    {
        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_category` (
			 	`ozon_category_id` INT(11) NOT NULL AUTO_INCREMENT,
				`title` varchar(255) NOT NULL,
				PRIMARY KEY (`ozon_category_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_attribute` (
				`id` varchar(255) NOT NULL,
				`ozon_category_id` INT(11) NOT NULL,
				`ozon_attribute_id` INT(11) NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");        
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_attribute` ADD INDEX(`ozon_attribute_id`);");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_attribute_description` (
				`ozon_attribute_id` INT(11) NOT NULL,
				`ozon_attribute_name` varchar(128) NOT NULL,
				`ozon_attribute_description` text(255),
				`ozon_dictionary_id` INT(11) NOT NULL,
				`shop_attribute_id` varchar(10) NOT NULL,
				`required` varchar(10) NOT NULL,
				PRIMARY KEY (`ozon_attribute_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		"); 
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_attribute_description` ADD INDEX(`ozon_dictionary_id`);");
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_attribute_description` ADD INDEX(`shop_attribute_id`);");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_dictionary` (
				`dictionary_id` INT(11) NOT NULL,
				`attribute_value_id` INT(11) NOT NULL,
				`category_id` INT(11) NOT NULL,
				`attribute_group_id` INT(11) NOT NULL,
				`text` varchar(255) NOT NULL,
				PRIMARY KEY (`attribute_value_id`, `category_id`, `attribute_group_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");        
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_dictionary` ADD INDEX(`dictionary_id`);");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_ms_order_guid` (
				`posting_number` varchar(128) NOT NULL,
				`guid` varchar(128) NOT NULL,
				`status` varchar(128) NOT NULL,
				`shipment_date` DATETIME NOT NULL,
				PRIMARY KEY (`posting_number`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_to_shop_dictionary` (
				`ozon_attribute_id` INT(11) NOT NULL,
				`dictionary_value_id` varchar(128) NOT NULL,
				`value` varchar(255) NOT NULL,
				`text_shop_attribute` varchar(128) NOT NULL,
				`shop_attribute_id` INT(11) NOT NULL,
				PRIMARY KEY (`text_shop_attribute`, `shop_attribute_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_to_shop_dictionary` ADD INDEX(`dictionary_value_id`);");
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_to_shop_dictionary` ADD INDEX(`ozon_attribute_id`);");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_products` (
				`product_id` INT(11) NOT NULL,
				`model` varchar(64) NOT NULL,
				`sku` varchar(64) NOT NULL,
				`status` varchar(128) NOT NULL,
				`date` datetime NOT NULL,
				`ozon_sku` INT(11) NOT NULL,
				`ozon_product_id` INT(11) NOT NULL,
				`task_id` INT(11) NOT NULL,
				`error` varchar(255) NOT NULL,
				PRIMARY KEY (`product_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_products` ADD INDEX(`ozon_product_id`);");
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_products` ADD INDEX(`task_id`);");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_manufacturer` (
				`ozon_id` INT(11) NOT NULL,
				`value` varchar(128) NOT NULL,
				`picture` varchar(255) NOT NULL,
				`shop_id` varchar(128) NOT NULL,
				PRIMARY KEY (`ozon_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_manufacturer` ADD INDEX(`shop_id`);");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ozon_attribute_required` (
				`category_id` INT(11) NOT NULL,
				`attribute_id` INT(11) NOT NULL,
				PRIMARY KEY (`category_id`, `attribute_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");        
        $this->db->query("ALTER TABLE `".DB_PREFIX."ozon_attribute_required` ADD INDEX(`attribute_id`);");
    }

    public function uninstall()
    {
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_category");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_attribute");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_attribute_description");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_dictionary");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_ms_order_guid");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_to_shop_dictionary");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_products");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_manufacturer");
        $this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."ozon_attribute_required");
    }
}

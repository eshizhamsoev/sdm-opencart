<?php
class ModelExtensionModuleImpex extends Model {
	
	// VARS
	private $STORE_ID			= 0;
	private $LANG_ID			= 0;
	
	private $FULL_IMPORT	= false;
	private $NOW 					= '';
	private $TAB_FIELDS		= array();
	private $ERROR				= 0;
	private $XML_VER			= "";

	// Классификатор
	private $TAXES			= array();
	private	$MANUFACTURERS	= array();
	private	$CATEGORIES		= array();
	private	$ATTRIBUTES		= array();
	private	$ATTRIBUTE_GROUPS	= array();
	private $PRODUCT_CATEGORIES = array();

	// Статистика
	private $STAT			= array();
	
	//Settings
	private $SET_BASE					= '';
	private $SET_MODE					= 'insert';
	private $SET_CATEGORIES				= false;
	private $SET_IMAGE					= false;
	private $SET_FILES					= false;
	private $SET_PRICE					= false;
	private $SET_STOCK					= false;
	private $SET_BATCHES				= 0;
	private $SET_BATCH					= 0;
	private $SET_BATCH_SIZE				= 0;


	private function error() {
		$this->log->write("ОШИБКА " . $this->ERROR . ". Смотрите описание ошибки в справке модуля обмена.");
		return $this->ERROR;
	} 

	private function log($message, $level = 1, $line = '') {
		//if ($level <= $this->config->get('exchange1c_log_level')) {

			if ($this->config->get('exchange1c_log_debug_line_view') == 1) {
				if (!$line) {
					list ($di) = debug_backtrace();
					$line = sprintf("%04s",$di["line"]);
				}
			} else {
				$line = '';
			}

			if (is_array($message) || is_object($message)) {
				$this->log->write($line . "M:");
				$this->log->write(print_r($message, true));
			} else {
				if (mb_substr($message,0,1) == '~') {
					$this->log->write('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
					$this->log->write($line . "M " . mb_substr($message, 1));
				} else {
					$this->log->write($line . "M " . $message);
				}
			}
		//}
	} 

	private function errorLog($error_num, $arg1 = '', $arg2 = '', $arg3 = '') {
		$this->ERROR = $error_num;
		$message = $this->language->get('error_' . $error_num . '_log');
		if (!$message) {
			$this->language->get('error_' . $error_num);
		}
		if ($message && $this->config->get('exchange1c_log_level') > 0) {
			list ($di) = debug_backtrace();
			$debug = "Строка ошибки: " . sprintf("%04s",$di["line"]) . " - ";
			$this->log->write(sprintf($debug . $message, $arg1, $arg2, $arg3));
		}
	} 
	
	public function getLanguageId($lang) {

		if ($this->LANG_ID) {
			return $this->LANG_ID;
		}
		$query = $this->db->query("SELECT `language_id` FROM `" . DB_PREFIX . "language` WHERE `code` = '" . $this->db->escape($lang) . "'");
		$this->LANG_ID = $query->row['language_id'];
		return $this->LANG_ID;

	} 
	
	public function importFile($importFile, $type) {

		$this->log("~НАЧАЛО ЗАГРУЗКИ ДАННЫХ");
		
		$this->NOW = date('Y-m-d H:i:s');
		$this->log("Единое время загрузки: " . $this->NOW, 2);

		$this->getLanguageId($this->config->get('config_language'));
		$this->log("Язык загрузки, id: " . $this->LANG_ID, 2);

		$this->log("~ТИП ЗАГРУЗКИ: " . $type, 2);

		// Читаем XML
		libxml_use_internal_errors(true);
		$path_parts = pathinfo($importFile);
		
//$this->log($path_parts, 2);
		
		$filename = $path_parts['basename'];
		$this->log("Читаем XML файл: '" . $filename . "'", 2);

		if (is_file($importFile)) {

			$xml = simplexml_load_file($importFile);
			
			if (!$xml) {
				$this->log(libxml_get_errors(), 2);
				return $this->error();
			}

		} else {
			return $this->error();
		}
		
		if ($xml->settings->base) {
			$this->SET_BASE = $xml->settings->base; 
			$this->log("~База 1С в файле = " . $this->SET_BASE,2);
			$this->log("~База 1С к обработке = " . $this->config->get('module_impex_base'),2);
//			if ($this->config->get('module_impex_base') != $xml->settings->base) {
//				$this->log("Загрузка отменена - несоответствие базы данных 1С",2);
//				return $this->error();
//				//exit();
//			}
			
			if ($xml->settings->base == 'WIN-MP1GU7FRCK9/UT' || $xml->settings->base == '127.0.0.1/UT' || $xml->settings->base == '185.75.88.251/UT') {
				//exit();
			} else {
				$this->log("Загрузка отменена - несоответствие базы данных 1С",2);
				return $this->error();
			}
			
		}
		
		if ($xml->settings->mode) {
			$this->SET_MODE = $xml->settings->mode; 
		}
		
		if ($xml->settings->categories) {
			$this->SET_CATEGORIES = true; 
		}
		
		if ($xml->settings->image) {
			$this->SET_IMAGE = true; 
		}
		
		if ($xml->settings->files) {
			$this->SET_FILES = true; 
		}
		
		if ($xml->settings->price) {
			$this->SET_PRICE = true; 
		}
		
		if ($xml->settings->stock) {
			$this->SET_STOCK = true; 
		}
		
		if ($xml->settings->batches) {
			$this->SET_BATCHES = $xml->settings->batches; 
		}
		
		if ($xml->settings->batches) {
			$this->SET_BATCH = $xml->settings->batch; 
		}
		
		if ($xml->settings->batches) {
			$this->SET_BATCH_SIZE = $xml->settings->batch_size; 
		}
		
		$this->log("~РЕЖИМ ЗАГРУЗКИ = " . $this->SET_MODE,2);
		$this->log("~ЗАГРУЖАТЬ КАТЕГОРИИ = " . $this->SET_CATEGORIES,2);
		$this->log("~ЗАГРУЖАТЬ КАРТИНКИ = " . $this->SET_IMAGE,2);
		$this->log("~ЗАГРУЖАТЬ ФАЙЛЫ = " . $this->SET_FILES,2);
		$this->log("~ЗАГРУЖАТЬ ЦЕНЫ = " . $this->SET_PRICE,2);
		$this->log("~ЗАГРУЖАТЬ ОСТАТКИ = " . $this->SET_STOCK,2);
		$this->log("~КОЛИЧЕСТВО БАТЧЕЙ = " . $this->SET_BATCHES,2);
		$this->log("~РАЗМЕР БАТЧА = " . $this->SET_BATCH,2);
		$this->log("~НОМЕР БАТЧА = " . $this->SET_BATCH_SIZE,2);

		if ($xml->categories) {
			$this->log("~ЗАГРУЗКА КАТЕГОРИЙ---",2);
			$this->importCategories($xml->categories->category, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->categories);
		}

		if ($xml->manufacturers) {
			$this->log("~ЗАГРУЗКА ПРОИЗВОДИТЕЛЕЙ---",2);
			$this->importManufacturers($xml->manufacturers->manufacturer, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->manufacturers);
		}
		
		if ($xml->attributes) {
			$this->log("~ЗАГРУЗКА АТРИБУТОВ---",2);
			$this->importAttributes($xml->attributes->attribute, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->attributes);
		}

		if ($xml->products) {
			$this->log("~ЗАГРУЗКА ТОВАРОВ---",2);
			$this->importProducts($xml->products->product, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->products);
		}

		if ($xml->stockprice) {
			$this->log("~ЗАГРУЗКА ОСТАТКОВ и ЦЕН---",2);
			$this->importStockPrice($xml);
			if ($this->ERROR) return $this->error();
			unset($xml->stockprice);
		}

		if ($xml->gift) {
			$this->log("~ЗАГРУЗКА ПОДАРКОВ---",2);
			$this->importGift($xml);
			if ($this->ERROR) return $this->error();
			unset($xml->gift);
		}

		if ($xml->stockprices) {
			$this->log("~ЗАГРУЗКА ОСТАТКОВ и ЦЕН в фоне с параметрами---",2);
			$this->importStockPriceParam($xml->stockprices->stockprice);
			if ($this->ERROR) return $this->error();
			unset($xml->stockprice);
		}

//		if ($xml->gifts) {
//			$this->log("~ЗАГРУЗКА ПОДАРКОВ в фоне с параметрами---",2);
//			$this->importGiftParam($xml->gifts->gift);
//			if ($this->ERROR) return $this->error();
//			unset($xml->gift);
//		}

		if ($xml->content) {
			$this->log("~ЗАГРУЗКА КОНТЕНТА",2);
			$this->importContent($xml);
			if ($this->ERROR) return $this->error();
			unset($xml->content);
		}
		
		if ($xml->category) {
			$this->log("~ЗАГРУЗКА КАТЕГОРИЙ",2);
			$this->importCategories($xml, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->category);
		}
		
		if ($xml->manufacturer) {
			$this->log("~ЗАГРУЗКА ПРОИЗВОДИТЕЛЕЙ",2);
			$this->importManufacturers($xml, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->manufacturer);
		}
		
		if ($xml->attribute) {
			$this->log("~ЗАГРУЗКА АТРИБУТОВ",2);
			$this->importAttributes($xml, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->attribute);
		}
		
		$this->log("~КОНЕЦ ЗАГРУЗКИ ДАННЫХ");

		return "";
	}
	
	private function importStockPrice($xml) {
		
		foreach ($xml as $product){
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			if ($product_id) {
				
				if ($product->delete) {
					$this->log->write("УДАЛИТЬ Товар : " . $product_id . " - " . $product->product_name);
					$this->log->write("---------------------------");
					
					$this->model_catalog_product->deleteProduct($product_id);
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$product_id . "' AND object = 'product'");	
					
					continue;
				}
				
				$price = $product->price == 0 ? 0 : $product->price;
				$stock = $product->stock == 0 ? 0 : $product->stock;
				$special = $product->special == 0 ? 0 : $product->special; 
				
				$mpn = '';
				
				if ($product->specprice == 'Специальная цена') {
					$mpn = 'Специальная цена';
				}
				
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
					
				if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
					
					if ($product->action == 'Акция') {
						$mpn = 'Акция';
					}
					
//					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
				}
				
				
				if ($product->out_of_sale) {
					//$stock_status_id = $this->config->get('config_out_of_sale_stock_status_id');
					$stock_status_id = 9;
					$stock = 0;	
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', price = '" . (float)$price . "', stock_status_id = '" . (int)$stock_status_id . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				} else {
					//$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', price = '" . (float)$price . "', mpn = '" . $this->db->escape($mpn) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', price = '" . (float)$price . "', mpn = '" . $this->db->escape($mpn) . "', price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				}
			} else {
				$this->log->write("!!! ТОВАР НЕ НАЙДЕН: " . $product->product_uid);
			}
		}
	}
	
	private function importStockPriceParam($xml) {
		
		$this->log->write("---------------------------");
		$this->log->write("          ---------------------------");
		$this->log->write("---------------------------");
		$this->log->write("SET_STOCK = " . $this->SET_STOCK);
		$this->log->write("SET_PRICE = " . $this->SET_PRICE);
		$this->log->write("---------------------------");
		$this->log->write("          ---------------------------");
		$this->log->write("---------------------------");
		
		
		$num = 0;
		foreach ($xml as $product){
			
			$num++;
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			if ($product_id) {
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET vendor_code = '" . $this->db->escape($product->vendor_code) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				if ($this->SET_STOCK) {
				
					$stock = $product->stock == 0 ? 0 : $product->stock;
					
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					
					$this->log->write($num . " - product_id = " . $product_id . " :: установлено количество = " . $stock);
				}
				
				if ($this->SET_PRICE) {
					
					$price = $product->price == 0 ? 0 : $product->price;
					$sup_price = $product->sup_price == 0 ? 0 : $product->sup_price;
					$special = $product->special == 0 ? 0 : $product->special; 

					$mpn = '';

					if ($product->specprice == 'Специальная цена') {
						$mpn = 'Специальная цена';
						//$special = 0;
					}

					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

					if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {

						if ($product->action == 'Акция') {
							$mpn = 'Акция';
						}

	//					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
					}
					
					//$this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$price . "', mpn = '" . $this->db->escape($mpn) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					$this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$price . "', sup_price = '" . (float)$sup_price . "', mpn = '" . $this->db->escape($mpn) . "', price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					
					$this->log->write($num . " - product_id = " . $product_id . " :: установлена цена = " . $price);
					$this->log->write($num . " - product_id = " . $product_id . " :: установлена закупочная цена = " . $sup_price);
				}
				
				//$this->log->write("Цена = " . $price . "  |  Остаток = " . $stock);
				
			} else {
				//$this->log->write($num . " - !!! ТОВАР НЕ НАЙДЕН: " . $product->product_uid);
				$this->log->write($num . " - !!! ТОВАР НЕ НАЙДЕН: " . $product->product_name);
			}
		}
	}
	
	private function importGift($xml) {
		
		foreach ($xml as $product){
			
			$this->log->write("Опция : " . $product->category_uid . " - " . $product->category_name);
			
			$option_id = $this->getObjectIdByUid($product->category_uid, 'option');
			
			if ($option_id) {
				
				$this->log->write("Опция существует : " . $option_id . " - " . $product->category_name);
				
				$this->db->query("UPDATE " . DB_PREFIX . "option SET type = 'radio', sort_order = '0' WHERE option_id = '" . (int)$option_id . "'");	
				
				$this->db->query("UPDATE " . DB_PREFIX . "option_description SET option_id = '" . $option_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->category_name) . "' WHERE option_id = '" . (int)$option_id . "'");	
				
			} else {
				
				$this->log->write("Новая опция : " . $product->category_name);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = 'radio', sort_order = '0'");
				$option_id = $this->db->getLastId();
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . $option_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->category_name) . "'");
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$option_id . "', `object_uid` = '" . $this->db->escape($product->category_uid) . "', `object` = 'option'");
				
			}
			
			$option_value_id = $this->getObjectIdByUid($product->product_uid, 'option_value');
			
			if ($option_value_id) {
				
$this->log->write('Значение опции существует : ' . $product->product_name . ' : ' . $option_value_id);
				
				$this->db->query("UPDATE " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape($product->image) . "', sort_order = '0' WHERE option_value_id = '" . (int)$option_value_id . "'");	
				
				$this->db->query("UPDATE " . DB_PREFIX . "option_value_description SET option_id = '" . (int)$option_id . "', option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->product_name) . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
				
			} else {
				
$this->log->write('Картинка опции : ' . $product->image . ' : ' . $option_value_id);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "option_value` SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape($product->image) . "', sort_order = '0'");
				$option_value_id = $this->db->getLastId();
$this->log->write('Новое значение опции : ' . $product->product_name . ' создано: ' . $option_value_id);
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_id = '" . (int)$option_id . "', option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->product_name) . "'");
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$option_value_id . "', `object_uid` = '" . $this->db->escape($product->product_uid) . "', `object` = 'option_value'");
				
			}
			
			$this->db->query("UPDATE " . DB_PREFIX . "gift_product SET gift_quantity = '" . (int)$product->stock . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
			
			$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = '" . (int)$product->stock . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
			
			
		}
	}
	
//	private function importGiftParam($xml) {
//		
//		$this->log->write("---------------------------");
//		$this->log->write("          ---------------------------");
//		$this->log->write("---------------------------");
//		$this->log->write("SET_STOCK = " . $this->SET_STOCK);
//		$this->log->write("SET_PRICE = " . $this->SET_PRICE);
//		$this->log->write("---------------------------");
//		$this->log->write("          ---------------------------");
//		$this->log->write("---------------------------");
//		
//		
//		$num = 0;
//		foreach ($xml as $product){
//			
//			$num++;
//			
//			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
//			
//			if ($product_id) {
//				
//				if ($this->SET_STOCK) {
//				
//					$stock = $product->stock == 0 ? 555 : $product->stock;
//					
//					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
//					
//					$this->log->write($num . " - product_id = " . $product_id . " :: установлено количество = " . $stock);
//				}
//				
//				if ($this->SET_PRICE) {
//					
//					$price = $product->price == 0 ? 0 : $product->price;
//					
//					$this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . (float)$price . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
//					
//					$this->log->write($num . " - product_id = " . $product_id . " :: установлена цена = " . $price);
//				}
//				
//				//$this->log->write("Цена = " . $price . "  |  Остаток = " . $stock);
//				
//			} else {
//				$this->log->write($num . " - !!! ТОВАР НЕ НАЙДЕН: " . $product->product_uid);
//			}
//		}
//	}
	
	private function importContent($xml) {
		
//		$this->log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
//		foreach ($xml as $content){
//			$this->log($content, 2);
//		}
//		$this->log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
		
		
		if ($xml->content->product) {
			$this->log('Импорт товара: ' . $xml->content->product->product_uid, 2);
			
			$product = $xml->content->product;
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			if ($product_id) {
				
				$this->log->write("Товар существует: " . $product_id);
				
				$this->load->model('catalog/product');
				
				$old_data = $this->model_catalog_product->getProduct($product_id);
				$old_descriptions = $this->model_catalog_product->getProductDescriptions($product_id);
				$old_specials = $this->model_catalog_product->getProductSpecials($product_id);
				$old_related = $this->model_catalog_product->getProductRelated($product_id);
				$old_filters = $this->model_catalog_product->getProductFilters($product_id);
				$old_stores = $this->model_catalog_product->getProductStores($product_id);
				$old_layouts = $this->model_catalog_product->getProductLayouts($product_id);
				$old_seourls = $this->model_catalog_product->getProductSeoUrls($product_id);
				$old_attributes = $this->model_catalog_product->getProductAttributes($product_id);
				$old_categories = $this->model_catalog_product->getProductCategories($product_id);
				
				$new_data = $old_data;
				$new_data['product_special'] = $old_specials;
				$new_data['product_related'] = $old_related;
				$new_data['product_filter'] = $old_filters;
				$new_data['product_store'] = $old_stores;
				$new_data['product_layout'] = $old_layouts;
				$new_data['product_seo_url'] = $old_seourls;
				$new_data['product_attribute'] = $old_attributes;
				$new_data['product_category'] = $old_categories;
				
				if ($product->images->image) {
				
					$img_count = count($product->images->image);
					if ($img_count > 0) {
						$new_data['image'] = $product->images->image[0];
					} else {
						$new_data['image'] = '';
					}
					if ($img_count > 1) {
						$product_images = array();
						for ($i = 1; $i < $img_count; $i++) {
							$product_images[] = array(
								 'image'         			=> $product->images->image[$i]
								,'sort_order'            		=> $i
							);
						}
						$new_data['product_image'] = $product_images;	
					}
				}
				
				if ($product->files->file) {
				
					$files_count = count($product->files->file);
					if ($files_count > 0) {
						if ($product->files->file[0]->type == "description") {
							$description_file = $product->files->file[0]->address;
							$this->log("~ФАЙЛ ОПИСАНИЯ",2);
							$this->log(DIR_IMAGE . $description_file[0]);
	
							$new_description = file_get_contents(DIR_IMAGE . $description_file[0]);
							
						}
					} else {
						$new_description = '';
					}
				}
				
				$new_descriptions = $old_descriptions;
				//$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				if ($new_description != '') {
					$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				}
				$new_data['product_description'] = $new_descriptions;
				
				$this->model_catalog_product->editProduct($product_id, $new_data);
				
			} else {
				
				$this->log->write("Новый товар: " . $product->product_uid);
				
			}
			
		}
		
		if ($xml->content->category) {
			$this->log('Импорт категории: ' . $xml->content->category->category_uid, 2);
			
			$category = $xml->content->category;
			
			$category_id = $this->getObjectIdByUid($category->category_uid, 'category');

			if ($category_id) {
				
				$this->log->write("Категория существует: " . $category->category_uid);
				
				$this->load->model('catalog/category');
				
				$old_data = $this->model_catalog_category->getCategory($category_id);
				
				$old_descriptions = $this->model_catalog_category->getCategoryDescriptions($category_id);
				$old_pathes = $this->model_catalog_category->getCategoryPath($category_id);
				$old_filters = $this->model_catalog_category->getCategoryFilters($category_id);
				$old_stores = $this->model_catalog_category->getCategoryStores($category_id);
				$old_layouts = $this->model_catalog_category->getCategoryLayouts($category_id);
				$old_seourls = $this->model_catalog_category->getCategorySeoUrls($category_id);
				
				$new_data = $old_data;
				
				if ($category->image) {
					$new_image = (string)$category->image;
				} else {
					$new_image = '';
				}
				
				if ($category->description) {
					$description_file = (string)$category->description;
					$new_description = file_get_contents(DIR_IMAGE . $description_file);
				} else {
					$new_description = '';
				}
		
				$new_descriptions = $old_descriptions;
				//$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				if ($new_description != '') {
					$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				}
				$new_data['category_description'] = $new_descriptions;
				$new_data['image'] = $new_image;
				$new_data['category_filter'] = $old_filters;
				$new_data['category_store'] = $old_stores;
				$new_data['category_layout'] = $old_layouts;
				$new_data['category_seo_url'] = $old_seourls;
				
				$this->model_catalog_category->editCategory($category_id, $new_data);
		
			} else {
				
				$this->log->write("Новая категория: " . $category->category_uid);
				
			}
				
		}
		
		if ($xml->content->manufacturer) {
			$this->log('Импорт производителя: ' . $xml->content->manufacturer->manufacturer_uid, 2);
			
			$manufacturer = $xml->content->manufacturer;
			
			$manufacturer_id = $this->getObjectIdByUid($manufacturer->manufacturer_uid, 'manufacturer');

			if ($manufacturer_id) {
				
				$this->load->model('catalog/manufacturer');
				
				$this->log->write("Есть такой производитель: " . $manufacturer->manufacturer_uid);
				
				$old_data = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
				$old_stores = $this->model_catalog_manufacturer->getManufacturerStores($manufacturer_id);
				$old_seourls = $this->model_catalog_manufacturer->getManufacturerSeoUrls($manufacturer_id);
				
				$new_data = $old_data;
				
				if ($manufacturer->image) {
					$new_image = (string)$manufacturer->image;
				} else {
					$new_image = '';
				}
				
				$new_data['image'] = $new_image;
				$new_data['manufacturer_store'] = $old_stores;
				$new_data['manufacturer_seo_url'] = $old_seourls;
				
				$this->model_catalog_manufacturer->editManufacturer($manufacturer_id, $new_data);
				
			} else {
				
				$this->log->write("Новый производитель: " . $manufacturer->manufacturer_uid);
				
			}

		}
		
	}
	
	private function importProducts($xml, $language_id) {
		
		$this->load->model('catalog/product');
		
		foreach ($xml as $product){
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			$price = $product->price == 0 ? 0 : $product->price;
			$special = $product->special == 0 ? 0 : $product->special; 
			$mpn = '';

			if ($product->specprice == 'Специальная цена') {
				$mpn = 'Специальная цена';
				//$special = 0;
			}

			if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
				if ($product->action == 'Акция') {
					$mpn = 'Акция';
				}
			}

			if ($product_id) {
				
				$this->log->write("Товар существует: " . $product->product_name);
				
				if ($product->delete) {
					$this->log->write("УДАЛИТЬ Товар : " . $product_id . " - " . $product->product_name);
					$this->log->write("---------------------------");
					
					$this->model_catalog_product->deleteProduct($product_id);
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$product_id . "' AND object = 'product'");	
					
					continue;
				}
				
				if ($this->SET_MODE == 'update') {
					
					$this->log->write(" Обновляем все данные товара ");
					
					$old_data = $this->model_catalog_product->getProduct($product_id);
					$old_descriptions = $this->model_catalog_product->getProductDescriptions($product_id);
					$old_images = $this->model_catalog_product->getProductImages($product_id);
					$old_specials = $this->model_catalog_product->getProductSpecials($product_id);
					$old_related = $this->model_catalog_product->getProductRelated($product_id);
					$old_filters = $this->model_catalog_product->getProductFilters($product_id);
					$old_stores = $this->model_catalog_product->getProductStores($product_id);
					$old_layouts = $this->model_catalog_product->getProductLayouts($product_id);
					$old_seourls = $this->model_catalog_product->getProductSeoUrls($product_id);
					$old_tags = $this->getProductTags($product_id);
					
					$new_data = $old_data;
					
					$new_data['product_special'] = $old_specials;
					$new_data['product_related'] = $old_related;
					$new_data['product_filter'] = $old_filters;
					$new_data['product_store'] = $old_stores;
					$new_data['product_layout'] = $old_layouts;
					$new_data['product_seo_url'] = $old_seourls;
					$new_data['product_tagn'] = $old_tags;
					
					$new_data['manufacturer_id'] = $this->getObjectIdByUid($product->manufacturer_uid, 'manufacturer');
					
					$new_data['sku'] = $product->product_sku;
					$new_data['mpn'] = $mpn;
					
					if ($product->out_of_sale) {
						//$new_data['stock_status_id'] = $this->config->get('config_out_of_sale_stock_status_id');	
						$new_data['stock_status_id'] = 9;	
					} else {
						$new_data['stock_status_id'] = '7';	
					}
					
					$product_attributes = array();	
					foreach ($product->attributes->attribute as $attribute) {
						if (trim($attribute->attribute_value) != '' && trim($attribute->attribute_value) != '<>') {
							$product_attributes[] = array(
								 'attribute_id'											=> $this->getObjectIdByUid($attribute->attribute_uid, 'attribute')
								,'product_attribute_description'		=> array(
									$this->LANG_ID => array(
										 'text'             	=> $attribute->attribute_value
									)
								)
							);
						}
					}
					$new_data['product_attribute'] = $product_attributes;
					
					$category_id = $this->getObjectIdByUid($product->category_uid, 'category');
					$product_categories = array();
					$product_categories[] = $category_id;
					$parent_id = $this->getParentCategory($category_id);
					while ($parent_id != '0') {
						$product_categories[] = $parent_id;
						$parent_id = $this->getParentCategory($parent_id);
					}
					$new_data['product_category'] = $product_categories;
					
					if ($this->SET_IMAGE) {
						
						if ($product->images->image) {
						
							$img_count = count($product->images->image);
							if ($img_count > 0) {
								$new_data['image'] = $product->images->image[0];
							} else {
								$new_data['image'] = '';
							}
							if ($img_count > 1) {
								$product_images = array();
								for ($i = 1; $i < $img_count; $i++) {
									$product_images[] = array(
										 'image'         			=> $product->images->image[$i]
										,'sort_order'            		=> $i
									);
								}
								$new_data['product_image'] = $product_images;	
							}
						}
					} else {
						$new_data['product_image'] = $old_images;	
					}

					if ($this->SET_PRICE) $new_data['price'] = $product->price;
					
					if ($this->SET_STOCK) $new_data['quantity'] = $product->quantity == 0 ? 0 : $product->quantity;
					
					if ($product->out_of_sale) {
						$new_data['quantity'] = 0;	
					}
					
					if ($this->SET_FILES) {
						$files_count = count($product->files->file);
						if ($files_count > 0) {
							if ($product->files->file[0]->type == "description") {
								$description_file = $product->files->file[0]->address;
								$this->log("~ФАЙЛ ОПИСАНИЯ",2);
								$this->log(DIR_IMAGE . $description_file[0]);

								$new_description = file_get_contents(DIR_IMAGE . $description_file[0]);
								
							}
						} else {
							//$new_description = '';
							$new_description = '';
						}
					} else {
						//$new_description = '';	
						$new_description = '';	
					}
					
					$new_descriptions = $old_descriptions;
					
					//$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					if ($new_description != '') {
						$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					}
					
					$new_descriptions[$this->LANG_ID]['name'] =	$product->product_name;
					if ($old_descriptions[$this->LANG_ID]['meta_title'] == $old_descriptions[$this->LANG_ID]['name']) {
						$new_descriptions[$this->LANG_ID]['meta_title'] =	$product->product_name;
					} else {
						if ($old_descriptions[$this->LANG_ID]['meta_title'] == '') {
							$new_descriptions[$this->LANG_ID]['meta_title'] =	$product->product_name;
						}
					}
					$new_data['product_description'] = $new_descriptions;
					
					$this->model_catalog_product->editProduct($product_id, $new_data);
					
$this->db->query("UPDATE " . DB_PREFIX . "product SET price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					
					if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
						
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
					}
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				if ($product->delete) {
					$this->log->write("НОВЫЙ Товар : с маркером delete - " . $product->product_name);
					$this->log->write("Ничего не делаем");
					$this->log->write("---------------------------");
					
					continue;
				}
				
				$this->log->write("Новый Товар: " . $product->product_name);
				
				$data = $this->getProductData($product);
				
				if ($product->out_of_sale) {
					//$new_data['stock_status_id'] = $this->config->get('config_out_of_sale_stock_status_id');	
					$data['stock_status_id'] = 9;	
				}
					
				$product_id = $this->model_catalog_product->addProduct($data);
				
$this->db->query("UPDATE " . DB_PREFIX . "product SET price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
				}
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$product_id . "', `object_uid` = '" . $this->db->escape($product->product_uid) . "', `object` = 'product'");
				
			}
		}
	}
	
	private function importCategories($xml, $parent = 0, $language_id) {
		
		$this->load->model('catalog/category');
		
		foreach ($xml as $category){
			
			$category_id = $this->getObjectIdByUid($category->category_uid, 'category');
			
			if ($category_id) {
				
				$this->log->write("Категория существует: " . $category->category_name);
				
				if ($this->SET_MODE == 'update') {
					
$this->log->write(" Обновляем все данные категории ");
					
					$old_data = $this->model_catalog_category->getCategory($category_id);
					
$this->log('Старые данные категории',2);
$this->log($old_data,2);
					
					$old_descriptions = $this->model_catalog_category->getCategoryDescriptions($category_id);
					$old_pathes = $this->model_catalog_category->getCategoryPath($category_id);
					$old_filters = $this->model_catalog_category->getCategoryFilters($category_id);
					$old_stores = $this->model_catalog_category->getCategoryStores($category_id);
					$old_layouts = $this->model_catalog_category->getCategoryLayouts($category_id);
					$old_seourls = $this->model_catalog_category->getCategorySeoUrls($category_id);
					
					$new_data = $old_data;
					if ($category->level > 0) {
						$parent_id = $this->getObjectIdByUid($category->parent_uid, 'category');
					} else {
						$parent_id = 0;
					}
					$new_data['top'] = $category->level == 0 ? 1 : 0;
					$new_data['parent_id'] = $parent_id;
					
					if ($this->SET_IMAGE) {
						if ($category->image) {
							$new_image = (string)$category->image;
						} else {
							$new_image = '';
						}
					} else {
						$new_image = '';	
					}
					$new_data['image'] = $new_image;
					
					if ($this->SET_FILES) {
						if ($category->description) {
							$description_file = (string)$category->description;
							
//							$this->log("~ФАЙЛ ОПИСАНИЯ",2);
//							$this->log(DIR_IMAGE . $description_file);

							$new_description = file_get_contents(DIR_IMAGE . $description_file);
								
						} else {
							$new_description = '';
						}
					} else {
						$new_description = '';	
					}
					
					$new_descriptions = $old_descriptions;
					
//					$this->log($this->LANG_ID,2);
//					$this->log($old_descriptions,2);
					
					//$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					if ($new_description != '') {
						$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					}
					$new_descriptions[$this->LANG_ID]['name'] =	(string)$category->category_name;
					if ($old_descriptions[$this->LANG_ID]['meta_title'] == $old_descriptions[$this->LANG_ID]['name']) {
						$new_descriptions[$this->LANG_ID]['meta_title'] =	(string)$category->category_name;
					} else {
						if ($old_descriptions[$this->LANG_ID]['meta_title'] == '') {
							$new_descriptions[$this->LANG_ID]['meta_title'] =	(string)$category->category_name;
						}
					}
					$new_data['category_description'] = $new_descriptions;
					$new_data['category_filter'] = $old_filters;
					$new_data['category_store'] = $old_stores;
					$new_data['category_layout'] = $old_layouts;
					$new_data['category_seo_url'] = $old_seourls;
					
$this->log('Новые данные категории',2);
$this->log($new_data,2);
					
					$this->model_catalog_category->editCategory($category_id, $new_data);
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				$this->log->write("Новая категория: " . $category->category_name);
				
				$data = $this->getCategoryData($category);

				$category_id = $this->model_catalog_category->addCategory($data);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$category_id . "', `object_uid` = '" . $this->db->escape($category->category_uid) . "', `object` = 'category'");
				
			}
		}
	}
	
	private function importAttributes($xml, $language_id) {
		
		$this->load->model('catalog/attribute');
		
		foreach ($xml as $attribute){
			
			$attribute_id = $this->getObjectIdByUid($attribute->attribute_uid, 'attribute');
			
			if ($attribute_id) {

				$this->log->write("Атрибут существует: " . $attribute->attribute_name);
				
				if ($this->SET_MODE == 'update') {
					
					$this->log->write(" Обновляем атрибут ");
					
					$old_data = $this->model_catalog_attribute->getAttribute($attribute_id);
					$old_descriptions = $this->model_catalog_attribute->getAttributeDescriptions($attribute_id);
					
					$new_data = $old_data;
					$new_descriptions[$this->LANG_ID]['name'] =	(string)$attribute->attribute_name;
					$new_data['attribute_description'] = $new_descriptions;
					
					$this->model_catalog_attribute->editAttribute($attribute_id, $new_data);
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				$this->log->write("Новый атрибут: " . $attribute->attribute_name);
				
				$data = $this->getAttributeData($attribute);
				
				$attribute_id = $this->model_catalog_attribute->addAttribute($data);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$attribute_id . "', `object_uid` = '" . $this->db->escape($attribute->attribute_uid) . "', `object` = 'attribute'");
				
			}
			
		}
	}
	
	private function importManufacturers($xml, $parent = 0, $language_id) {
		
		$this->load->model('catalog/manufacturer');
		
		foreach ($xml as $manufacturer){
			
			$manufacturer_id = $this->getObjectIdByUid($manufacturer->manufacturer_uid, 'manufacturer');
			
			if ($manufacturer_id) {
				
				$this->log->write("Есть такой производитель: " . $manufacturer->manufacturer_name);
				
				if ($this->SET_MODE == 'update') {
					
					$this->log->write(" Обновляем все данные производителя ");
					
					$old_data = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
					$old_stores = $this->model_catalog_manufacturer->getManufacturerStores($manufacturer_id);
					$old_seourls = $this->model_catalog_manufacturer->getManufacturerSeoUrls($manufacturer_id);
					
					$new_data = $old_data;
					
					if ($this->SET_IMAGE) {
						if ($manufacturer->image) {
							$new_image = (string)$manufacturer->image;
						} else {
							$new_image = '';
						}
					} else {
						$new_image = '';	
					}
					
					$new_data['image'] = $new_image;
					$new_data['name'] = $manufacturer->manufacturer_name;
					$new_data['manufacturer_store'] = $old_stores;
					$new_data['manufacturer_seo_url'] = $old_seourls;
					
					$this->model_catalog_manufacturer->editManufacturer($manufacturer_id, $new_data);
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				$this->log->write("Новый производитель: " . $manufacturer->manufacturer_name);
				
				$data = $this->getManufacturerData($manufacturer);
				
				$manufacturer_id = $this->model_catalog_manufacturer->addManufacturer($data);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$manufacturer_id . "', `object_uid` = '" . $this->db->escape($manufacturer->manufacturer_uid) . "', `object` = 'manufacturer'");
				
			}
		}
	}

	private function getObjectIdByUid($object_uid, $object) {
		$query = $this->db->query("SELECT object_id FROM " . DB_PREFIX . "to_impex WHERE object_uid = '" . $object_uid . "' AND object = '" . $object . "'");

		if ($query->num_rows) {
			return $query->row['object_id'];
		}
		else {
			return false;
		}
	}

	private function getObjectKeyByUid($object_uid, $object) {
		$query = $this->db->query("SELECT object_key FROM " . DB_PREFIX . "to_impex WHERE object_uid = '" . $object_uid . "' AND object = '" . $object . "'");

		if ($query->num_rows) {
			return $query->row['object_key'];
		}
		else {
			return false;
		}
	}
	
	private function getObjectUidById($id, $object) {
		
		if ($object == 'total') {
			
			$query = $this->db->query("SELECT object_uid FROM `" . DB_PREFIX . "to_impex` WHERE object_key = '" . $this->db->escape($id) . "' AND object = '" . $this->db->escape($object) . "'");
	
			if ($query->num_rows) {
				return $query->row['object_uid'];
			}
			else {
				return false;
			}
	
		} else {
			
			$query = $this->db->query("SELECT object_uid FROM `" . DB_PREFIX . "to_impex` WHERE `object_id` = '" . (int)$id . "' AND object = '" . $this->db->escape($object) . "'");
	
			if ($query->num_rows) {
				return $query->row['object_uid'];
			} else {
				return false;
			}
	
		}
  }
	
	private function getParentCategory($category_id) {
		$query = $this->db->query("SELECT parent_id FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");

		if ($query->num_rows) {
			return $query->row['parent_id'];
		} else {
			return false;
		}
	}
	
	private function getProductData($product) {

		$category_id = $this->getObjectIdByUid($product->category_uid, 'category');
		$manufacturer_id = $this->getObjectIdByUid($product->manufacturer_uid, 'manufacturer');
		
		$quantity = $product->quantity; 
		$price = $product->price; 
		$special = $product->special; 
		
		if ($quantity) {
			$quantity = (float)$quantity; 
		} else {
			$quantity = 0; 
		}
//		if ($quantity == 0) {
//			$quantity = 0; 
//		}
		if ($product->out_of_sale) {
			$this->log->write("Товар снят с производства");
			$quantity = 0;	
		}
		$this->log->write("quantity = " . $quantity);
		
		if ($price) {
			$price = (float)$price; 
		} else {
			$price = 0; 
		}
		$this->log->write("price = " . $price);
		
		if ($special) {
			$special = (float)$special; 
		} else {
			$special = 0; 
		}
		$this->log->write("price = " . $special);
		
		$mpn = '';

		if ($product->specprice == 'Специальная цена') {
			$mpn = 'Специальная цена';
			//$special = 0;
		}

		if ($special > 0 && $price > 0 && $special < $price) {
			if ($product->action == 'Акция') {
				$mpn = 'Акция';
			}
		}
		
		$data = array(
			 'model'         		=> ''
			,'sku'            		=> $product->product_sku
			,'upc'            		=> ''
			,'ean'            		=> ''
			,'jan'            		=> ''
			,'isbn'            		=> ''
			,'mpn'            		=> $mpn
			,'location'           	=> ''
			,'quantity'      		=> $quantity
			,'minimum' 				=>  1
			,'subtract'        		=> 0
			,'stock_status_id'		=> 7
			,'date_available'     	=> ''
			,'manufacturer_id'		=> $manufacturer_id
			,'shipping'         	=> 1
			,'price'         		=> $price
			,'points'         		=> 0
			,'weight'         		=> 0
			,'weight_class_id'		=> 1
			,'length'         		=> 0
			,'width'         		=> 0
			,'height'         		=> 0
			,'length_class_id'		=> 1
			,'status'         		=> 1
			,'tax_class_id'			=> 0
			,'sort_order'         	=> 0
			
			,'delivery_option'    	=> ''
			,'sales_note'         	=> ''
			,'yam_status'         	=> 0
		);
		
		if ($product->images->image) {
		
			$img_count = count($product->images->image);
			if ($img_count > 0) {
				$data['image'] = $product->images->image[0];
			}
			
			if ($img_count > 1) {
				$data['product_image'] = array();
				for ($i = 1; $i < $img_count; $i++) {
					$data['product_image'][] = array(
						 'image'         			=> $product->images->image[$i]
						,'sort_order'            		=> $i
					);
				}
			}
	
		}
	
		$data['product_category'] = array();
			
		$data['product_category'][] = $category_id;
		
		$parent_id = $this->getParentCategory($category_id);
		while ($parent_id != '0') {
			$data['product_category'][] = $parent_id;
			$parent_id = $this->getParentCategory($parent_id);
		}
		
		if ($this->SET_FILES) {
			
			if ($product->files->file) {
			
				$files_count = count($product->files->file);
				if ($files_count > 0) {
					if ($product->files->file[0]->type == "description") {
						$description_file = $product->files->file[0]->address;
						$description = file_get_contents(DIR_IMAGE . $description_file[0]);
					}
				} else {
					$description = '';
				}
			
			}
			
		} else {
			$description = '';	
		}
		
		$data['product_description'] = array(
			$this->LANG_ID => array(
				 'name'             	=> $product->product_name
				 ,'description'				=> $description
				 ,'tag'             	=> ''
				 ,'meta_title'				=> $product->product_name
				 ,'meta_description'	=> ''
				 ,'meta_keyword'			=> ''
			)
		);
	
		$data['product_store'] = array(
			$this->STORE_ID
		);
		
		$data['product_layout'] = array(
			$this->STORE_ID 	=>  0
		);
		
		$data['product_attribute'] = array();	
		
		foreach ($product->attributes->attribute as $attribute) {
			$data['product_attribute'][] = array(
				 'attribute_id'											=> $this->getObjectIdByUid($attribute->attribute_uid, 'attribute')
				,'product_attribute_description'		=> array(
					$this->LANG_ID => array(
						 'text'             	=> $attribute->attribute_value
					)
				)
			);
		}
		
		return $data;
	}
	
	private function getCategoryData($category) {
	
		if ($category->level > 0) {
			$parent_id = $this->getObjectIdByUid($category->parent_uid, 'category');
		} else {
			$parent_id = 0;
		}
		
		if ($this->SET_IMAGE) {
			if ($category->image) {
				$image = (string)$category->image;
			} else {
				$image = '';
			}
		} else {
			$image = '';	
		}

		$data = array(
			 'status'         => 1
			,'top'            => $category->level == 0 ? 1 : 0
			,'parent_id'      => $parent_id
			,'category_store' =>  array($this->STORE_ID)
			,'keyword'        => ''
			,'image'          => $image
			,'sort_order'     => 0
			,'column'         => 1
		);
		
		if ($this->SET_FILES) {
			if ($category->description) {
				$description_file = (string)$category->description;
				$description = file_get_contents(DIR_IMAGE . $description_file);
			} else {
				$description = '';
			}
		} else {
			$description = '';	
		}

		$data['category_description'] = array(
			$this->LANG_ID 				=> array(
				 'name'             => (string)$category->category_name
				,'meta_keyword'     => ''
				,'meta_description'	=> ''
				,'description'		  => $description
				,'meta_title'				=> (string)$category->category_name
				,'meta_h1'					=> ''
				,'tagline'					=> ''
				,'color'						=> ''
			),
		);
	
		return $data;
	}
	
	private function getManufacturerData($manufacturer) {
		
		if ($this->SET_IMAGE) {
			if ($manufacturer->image) {
				$image = (string)$manufacturer->image;
			} else {
				$image = '';
			}
		} else {
			$image = '';	
		}

		$data = array(
			 'status'         		=> 1
			,'name'								=> $manufacturer->manufacturer_name
			,'image'          		=> $image
			,'sort_order'     		=> 0
		);
		
		$data['manufacturer_store'] = array(
			$this->STORE_ID
		);
		
		$data['manufacturer_seo_url'] = array(
			$this->LANG_ID => array(
				 'keyword'					=> ''
			),
		);
		
		return $data;
	}
	
	private function getAttributeData($attribute) {
		
		$attribute_group_id = 1;
		
		$data = array(
			'attribute_group_id'	=> $attribute_group_id
			,'sort_order'					=> 0
		);

		$data['attribute_description'] = array(
			$this->LANG_ID 	=> array(
			 'name'					=> $attribute->attribute_name
			)
		);
		
		return $data;
	}

	public function getProductTags($product_id) {
		$query = $this->db->query("SELECT *, IF(td.name_short IS NULL or td.name_short = '', td.name, td.name_short) as name,
			(select category_id from " . DB_PREFIX . "tag_to_category where tag_id = t.tag_id) as category_id,
			(select count(*) from " . DB_PREFIX . "product_to_tag where tag_id = pt.tag_id) as kol  
			FROM " . DB_PREFIX . "product_to_tag pt 
			INNER JOIN " . DB_PREFIX . "tag t ON (t.tag_id = pt.tag_id) 
			INNER JOIN " . DB_PREFIX . "tag_description td ON (t.tag_id = td.tag_id) 
			INNER JOIN " . DB_PREFIX . "tag_to_store ts ON (pt.tag_id = ts.tag_id) 
			WHERE td.language_id = '" . (int)$this->config->get('config_language_id') . "' 
				AND ts.store_id = '" . (int)$this->config->get('config_store_id') . "'  
				AND t.status = '1'
				AND pt.product_id = ".(int)$product_id." 
			group by t.tag_id ORDER BY kol desc");

		return $query->rows;
	}
	
}
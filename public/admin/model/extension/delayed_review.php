<?php


class ModelExtensionDelayedReview extends Model
{
    private $authors;

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->authors = $this->getReviewAuthorsByGenders();
    }

    public function addReviewsToProducts()
    {
        $ids = $this->getProductIdsForUpdate();
        foreach ($ids as $id) {
            $this->createReview($id);
        }
        return count($ids);
    }

    public function getProductIdsForUpdate(): array
    {
        $monthAgo = date('Y-m-d', time() - 3600);
        $sql = "SELECT DISTINCT p.product_id FROM " . DB_PREFIX . "product p"
            . " INNER JOIN " . DB_PREFIX . "product_to_category p2c ON(p2c.product_id = p.product_id) "
            . " WHERE date_added < '{$monthAgo}' AND NOT EXISTS (SELECT 1 FROM " . DB_PREFIX . "review r WHERE r.product_id = p.product_id AND r.delayed_review_id IS NOT NULL AND status != 0) AND p2c.category_id IN (SELECT category_id FROM " . DB_PREFIX . "delayed_review) ORDER BY RAND()";
        $query = $this->db->query($sql);
        $half = array_slice($query->rows, 0, ceil(count($query->rows) / 2));
        return array_map(function ($item) {
            return (int) $item['product_id'];
        }, $half);
    }

    public function createReview(int $productId)
    {
        $review = $this->getReviewForProduct($productId);
        if (!$review) {
            return;
        }
        $author = $this->getAuthorByGender($review['gender']);
        if (!$author) {
            return;
        }
        $date = date('Y-m-d', time() - rand(0, 3600 * 24 * 30 * 2)); // Random date of two month
        $this->db->query("INSERT INTO " . DB_PREFIX . "review SET product_id = '" . (int) $productId . "', author = '" . $this->db->escape($author['name']) . "', `text` = '" . $this->db->escape($review['text']) . "', status = '1', `rating` = '5', date_added = '" . $date . "', delayed_review_id = '" . (int) $review['delayed_review_id'] . "', delayed_review_author_id = '" . (int) $author['delayed_review_author_id'] . "'");

    }

    public function getReviewForProduct(int $productId)
    {
        $delayedReviewStatistic = "SELECT dr.delayed_review_id, COUNT(review_id) as review_count FROM " . DB_PREFIX . "delayed_review dr LEFT JOIN " . DB_PREFIX . "review r ON (r.delayed_review_id = dr.delayed_review_id) GROUP BY dr.delayed_review_id";
        $sql = "SELECT dr.* FROM " . DB_PREFIX . "delayed_review dr INNER JOIN (" . $delayedReviewStatistic . ") stat ON(stat.delayed_review_id = dr.delayed_review_id) WHERE category_id IN (SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $productId . "') ORDER BY stat.review_count ASC, RAND() LIMIT 1";
        return $this->db->query($sql)->row;
    }

    private function getReviewAuthorsByGenders()
    {
        $authorStatistic = "SELECT dr.delayed_review_author_id, COUNT(review_id) as review_count FROM " . DB_PREFIX . "delayed_review_author dr LEFT JOIN " . DB_PREFIX . "review r ON (r.delayed_review_author_id = dr.delayed_review_author_id) GROUP BY dr.delayed_review_author_id";
        $sql = "SELECT dra.*, stat.review_count FROM " . DB_PREFIX . "delayed_review_author dra INNER JOIN (" . $authorStatistic . ") stat ON(stat.delayed_review_author_id = dra.delayed_review_author_id)";
        return $this->db->query($sql)->rows;
    }

    private function getAuthorByGender($gender)
    {
        $index = null;
        foreach ($this->authors as $i => $author) {
            if ($author['gender'] !== $gender) {
                continue;
            }
            if ($index === null || $this->authors[$index]['review_count'] > $author['review_count']) {
                $index = $i;
            }
        }
        if ($index === null) {
            return null;
        }
        $this->authors[$index]['review_count'] += 1;
        return $this->authors[$index];
    }
}

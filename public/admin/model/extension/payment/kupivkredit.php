<?php

/**
 * Class ModelExtensionPaymentKupivkredit
 * @author fonclub
 * @created 17.12.2019
 */
class ModelExtensionPaymentKupivkredit extends Model {    
    public function __construct($registry)
    {
        parent::__construct($registry);
    }

    /**
     * создание таблицы
     */
	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "kupivkredit_product` (
			  `kupivkredit_product_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `product_id` INT(11) NOT NULL,
			  `types` VARCHAR(512),
			  PRIMARY KEY (`kupivkredit_product_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");
		
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "kupivkredit_generate_rules` (
			  `id` INT(11) NOT NULL AUTO_INCREMENT,
			  `rules` TEXT,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT COLLATE=utf8_general_ci;");
	}
	
	/**
     * удаление таблицы
     */
	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "kupivkredit_product`");
	}

    /**
     * список категорий
     * @param array $data
     *
     * @return mixed
     */
    public function getAllCategories($data = array()) {
        $sql = "SELECT cp.category_id AS category_id, cd2.name AS name, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&and;') AS fullname, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from " . DB_PREFIX . "product_to_category pc where pc.category_id = c1.category_id) as product_count FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = '1' AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'product_count',
            'fullname',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * список производителей
     * @param array $data
     *
     * @return mixed
     */
    public function getManufacturers($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "manufacturer";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * метод для установки товарам Оплаты в рассрочку
     *
     * @param $products
     * @param $types
     * @param $insert
     */
    public function setProducts($products, $types, $insert) {
        if(!$products)
            return;
        
        if($insert and !$types)
            return;
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "kupivkredit_product WHERE product_id IN(" . $products . ")");
        $this->delSticker($products);

        $arr_products = explode(',', $products);

        $ean = $insert ? 'installments' : '';
        $this->db->query("UPDATE ".DB_PREFIX."product SET ean = '".$this->db->escape($ean)."' WHERE product_id IN(" . $products . ")");

        if ($insert) {
            if (isset($arr_products)) {
                foreach ($arr_products as $product_id) {
                    if ($product_id != '') {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "kupivkredit_product SET product_id = '" . (int)$product_id . "', types = '". json_encode(explode(',', $types))."'");
                        $this->addSticker($product_id, explode(',', $types));
                    }
                }
            }
        }
    }


    /**
     * получаем стикер по типу рассрочки
     * @param $type
     *
     * @return string
     */
    protected function getStickerNameByType($type){
        $promo_codes = $this->config->get('payment_kupivkredit_promo_code');
        foreach ($promo_codes as $promoCode){
            if($promoCode['code'] == $type)
                return $promoCode['sticker'];
        }
        
        return $this->config->get('payment_kupivkredit_sticker_сode');
    }

    /**
     * получаем все стикеры для рассрочки
     * @return array
     */
    protected function getAllStickers(){
        $promo_codes = $this->config->get('payment_kupivkredit_promo_code');
        $stickers = [];  
        /** стикер по умолчанию */
        $stickers[$this->config->get('payment_kupivkredit_sticker_сode')] = $this->config->get('payment_kupivkredit_sticker_сode');
        
        foreach ($promo_codes as $promoCode){
            $stickers[$promoCode['sticker']] = $promoCode['sticker'];
        }

        return $stickers;
    }

    /**
     * удаляем стикер Оплата в рассрочку
     * @param $products
     */
    protected function delSticker($products){
        $stickers = $this->getAllStickers();
        if(!$stickers)
            return;
        
        $result = $this->db->query("SELECT oct_stickers, product_id FROM " . DB_PREFIX . "product WHERE product_id IN(" . $products . ")");
        
        if($result->num_rows){
            foreach ($result->rows as $row) {
                $oldStickers = unserialize($row['oct_stickers']);
                
                /** если существует такой стикер - удаляем его из товара */
                foreach ($stickers as $sticker)
                    if(isset($oldStickers[$sticker]))
                        unset($oldStickers[$sticker]);

                /** и обновляем товар */
                $this->db->query("UPDATE ".DB_PREFIX."product SET oct_stickers = '"
                    .$this->db->escape(serialize($oldStickers))."' WHERE product_id = '".(int)$row['product_id']."'");
            }
        }
    }

    /**
     * добавляем стикеры в зависимости от типа
     *
     * @param       $product_id
     * @param array $types
     */
    protected function addSticker($product_id, array $types){
        if(!$this->getAllStickers())
            return;

        $stickers = [];
        $result   = $this->db->query("SELECT oct_stickers FROM ".DB_PREFIX."product WHERE product_id = '"
            .(int)$product_id."'");

        if ($result->num_rows) {
            $stickers = unserialize($result->row['oct_stickers']);
        }

        foreach ($types as $type) {
            /** получаем стикер для выбранного типа рассрочки */
            $sticker = $this->getStickerNameByType($type);
            
            /** если он уже был установлен ранее - сбрасываем */
            if(isset($stickers[$sticker]))
                unset($stickers[$sticker]);

            /** добавляем новый стикер к товару */
            $stickers[$sticker] = $sticker;
        }

        /** обновляем товар */
        $this->db->query("UPDATE ".DB_PREFIX."product SET oct_stickers = '"
            .$this->db->escape(serialize($stickers))."' WHERE product_id = '".(int)$product_id."'");
    }

    /**
     * обновление стикеров
     */
    public function updateStickers(){
        $query = $this->db->query("SELECT product_id, types FROM " . DB_PREFIX . "kupivkredit_product");

        $products = [];
        foreach ($query->rows as $row) {
            $products[$row['product_id']] = json_decode($row['types']);
        }
        
        if(!$products)
            return;
        
        $productIds = array_keys($products);
        $this->delSticker(implode(',', $productIds));
        
        foreach ($products as $productId => $types){
            $this->addSticker($productId, $types);
        }
    }

    /**
     * список id товаров, привязанных к модулю
     * @return array
     */
    public function getProductIds() {
        $result = array(-1);

        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "kupivkredit_product");

        foreach ($query->rows as $row) {
            $result[] = $row['product_id'];
        }

        return $result;
    }

    public function getProductTypes($product_id) {
        $query = $this->db->query("SELECT types FROM " . DB_PREFIX . "kupivkredit_product WHERE product_id = '". (int) $product_id ."'");        

        return $query->num_rows ? json_decode($query->row['types']) : [];
    }

    /**
     * всего товаров, подходящих заданным параметрам
     * @param array $data
     *
     * @return mixed
     */
    public function getTotalProducts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        if (isset($data['filter_category']) && $data['filter_category'] !== '') {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['filter_category']) && $data['filter_category'] !== '') {
            $categories = $this->getAllCategoriesAndSubcategories((int)$data['filter_category']);
            $sql .= " AND p2c.category_id IN (" . implode(',', $categories) . ")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_price_from'])) {
            $sql .= " AND p.price >= '" . (float)$data['filter_price_from'] . "'";
        }

        if (!empty($data['filter_price_to'])) {
            $sql .= " AND p.price <= '" . (float)$data['filter_price_to'] . "'";
        }

        if (isset($data['filter_quantity_from']) && $data['filter_quantity_from'] !== '') {
            $sql .= " AND p.quantity >= '" . (int)$data['filter_quantity_from'] . "'";
        }

        if (isset($data['filter_quantity_to']) && $data['filter_quantity_to'] !== '') {
            $sql .= " AND p.quantity <= '" . (int)$data['filter_quantity_to'] . "'";
        }

        if (isset($data['filter_manufacturer']) && $data['filter_manufacturer'] !== '') {
            $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer'] . "'";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }
        
        if(isset($data['action']) and $data['action'] == 'list'){
            $sql .= " AND p.product_id IN(" . implode(',', $this->getProductIds()) . ")";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    /**
     * товары, подходящие заданным параметрам
     * @param array $data
     *
     * @return mixed
     */
    public function getProducts($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        if (isset($data['filter_category']) && $data['filter_category'] !== '') {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['filter_category']) && $data['filter_category'] !== '') {
            $categories = $this->getAllCategoriesAndSubcategories((int)$data['filter_category']);
            $sql .= " AND p2c.category_id IN (" . implode(',', $categories) . ")";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_price_from'])) {
            $sql .= " AND p.price >= '" . (float)$data['filter_price_from'] . "'";
        }

        if (!empty($data['filter_price_to'])) {
            $sql .= " AND p.price <= '" . (float)$data['filter_price_to'] . "'";
        }

        if (isset($data['filter_quantity_from']) && $data['filter_quantity_from'] !== '') {
            $sql .= " AND p.quantity >= '" . (int)$data['filter_quantity_from'] . "'";
        }

        if (isset($data['filter_quantity_to']) && $data['filter_quantity_to'] !== '') {
            $sql .= " AND p.quantity <= '" . (int)$data['filter_quantity_to'] . "'";
        }

        if (isset($data['filter_manufacturer']) && $data['filter_manufacturer'] !== '') {
            $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer'] . "'";
        }

        if (isset($data['filter_status']) && $data['filter_status'] !== '') {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        if(isset($data['action']) and $data['action'] == 'list'){
            $sql .= " AND p.product_id IN(" . implode(',', $this->getProductIds()) . ")";
        }

        $sql .= " GROUP BY p.product_id";

        $sort_data = array(
            'pd.name',
            'p.price',
            'p.quantity',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY pd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * акционные цены
     * @param $product_id
     *
     * @return mixed
     */
    public function getProductSpecials($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

        return $query->rows;
    }
    
    public function setSeoUrl($seo_urls){
        $this->db->query("DELETE FROM ".DB_PREFIX
            ."seo_url WHERE `query` = 'product/installments'");

        if (isset($seo_urls)) {
            foreach ($seo_urls as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (trim($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product/installments', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }
    }

    /**
     * список правил
     * @return mixed
     */
    public function listGenerateRules()
    {
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX. "kupivkredit_generate_rules");
        
        return $query->rows;
    }

    /**
     * сохранение правила
     * @param $data
     */
    public function saveGenerateRules($data)
    {
        $this->db->query("INSERT INTO ". DB_PREFIX. "kupivkredit_generate_rules SET rules = '" . $this->db->escape(json_encode($data)) ."'");
    }

    /**
     * удаление правила
     * @param int $id
     */
    public function deleteGenerateRule(int $id)
    {
        $this->db->query("DELETE FROM ". DB_PREFIX. "kupivkredit_generate_rules WHERE id = '" . $id . "'");
    }

    /**
     * метод для автоматической генерации товаров в рассрочку, используя заданные правила 
     */
    public function generateInstallments() {
        $generate_type = $this->config->get('payment_kupivkredit_generate_type') ?? 1;
        
        if((int)$generate_type !== 2){
            $this->log->write('Нужно установить автоматический тип генерации товаров в рассрочку!');
            echo "Нужно установить автоматический тип генерации товаров в рассрочку!";
            return;
        }
        
        $rules = $this->listGenerateRules();
        if(!$rules){
            $this->log->write('Нужно задать правила автоматического типа генерации товаров в рассрочку!');
            echo "Нужно задать правила автоматического типа генерации товаров в рассрочку!";
            return;
        }
        
        $products = $this->getProductIds();
        $this->delOldInstallmentProducts(implode(',', $products));
        $productsUpdate = [];
        
        foreach ($rules as $rule){
            $rulesObj = json_decode($rule['rules']);
            $types = $rulesObj->types;
            if(!$types)
                continue;
            
            $productIds = $this->getProductsByRule($rulesObj);
            if ($productIds) {
                foreach ($productIds as $row) {
                    $product_id = (int)$row['product_id'];
                    if ($product_id) {
                        foreach ($types as $type)
                            $productsUpdate[$product_id][$type] = $type;
                    }
                }
            }
        }
        
        if($productsUpdate){
            foreach ($productsUpdate as $product_id => $types){
                /** сортируем варианты рассрочки, чтобы рассрочка на 6 месяцев была выше рассрочки на 10 месяцев */
                usort($types, function ($a, $b){
                    $a = preg_replace('/\D/', '', $a);
                    $b = preg_replace('/\D/', '', $b);
                    return (int)$a > (int)$b;
                });
                
                $this->db->query("INSERT INTO " . DB_PREFIX . "kupivkredit_product SET product_id = '" . (int)$product_id . "', types = '". json_encode($types)."'");
                
                $this->db->query("UPDATE ".DB_PREFIX."product SET ean = 'installments' WHERE product_id = '" . (int)$product_id . "'");
                
                $this->addSticker($product_id, $types);
            }
        }
    }

    /**
     * удаление существующих данных о рассрочке
     * @param $products
     */
    protected function delOldInstallmentProducts($products){
        $this->db->query("DELETE FROM " . DB_PREFIX . "kupivkredit_product WHERE product_id IN(" . $products . ")");
        $this->delSticker($products);

        $this->db->query("UPDATE ".DB_PREFIX."product SET ean = '".$this->db->escape('')."' WHERE product_id IN(" . $products . ")");
    }

    /**
     * @param stdClass $rule
     *
     * @return array
     */
    protected function getProductsByRule($rule){
        $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product p";

        if ((int)$rule->category) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE 1";

        if ((int)$rule->category) {
            $categories = $this->getAllCategoriesAndSubcategories((int)$rule->category);
            $sql .= " AND p2c.category_id IN (" . implode(',', $categories) . ")";
        }

        if ($rule->price_from) {
            $sql .= " AND p.price >= '" . (float)$rule->price_from . "'";
        }

        if ($rule->price_to) {
            $sql .= " AND p.price <= '" . (float)$rule->price_to . "'";
        }

        if ((int)$rule->quantity_from) {
            $sql .= " AND p.quantity >= '" . (int)$rule->quantity_from . "'";
        }
        
        if ((int)$rule->quantity_to) {
            $sql .= " AND p.quantity <= '" . (int)$rule->quantity_to . "'";
        }

        if ((int)$rule->manufacturer) {
            $sql .= " AND p.manufacturer_id = '" . (int)$rule->manufacturer . "'";
        }
        
        if ((int)$rule->status) {
            $sql .= " AND p.status = '" . (int)$rule->status . "'";
        }

        $sql .= " GROUP BY p.product_id";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    /**
     * @param int $parent_id
     *
     * @return array
     */
    private function getAllCategoriesAndSubcategories($parent_id = 0) {
        $output = [$parent_id];

        $sql = "SELECT category_id FROM " . DB_PREFIX . "category WHERE status = '1' AND parent_id = '" . (int)$parent_id . "'";
        $query = $this->db->query($sql);

        if($query->num_rows){
            foreach ($query->rows as $row){
                $output[$row['category_id']] = (int)$row['category_id'];
                $output = array_merge($output, $this->getAllCategoriesAndSubcategories((int)$row['category_id']));
            }
        }
        
        return array_unique($output);
    }
}
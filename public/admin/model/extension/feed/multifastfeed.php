<?php
class ModelExtensionFeedMultiFastfeed extends Model {
	
	public function getSettings($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "yam_setting ORDER BY name ASC";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSetting($setting_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "yam_setting WHERE setting_id = '" . (int)$setting_id . "'");
		
        $categories = [];
        foreach ($this->getAllCategories() as $category){
            $categories[$category['category_id']] = $category['fullname'];
        } 
        
        $manufacturers = [];
        foreach ($this->getManufacturers() as $manufacturer){
            $manufacturers[$manufacturer['manufacturer_id']] = $manufacturer['name'];
        }
        
		if ($query->num_rows) {
            $auto_settings = [];
            if($query->row['auto_settings']){
                foreach (json_decode($query->row['auto_settings'], true) as $item){
                    array_walk($item, function (&$el, $k) use ($categories, $manufacturers) {
                        /** если это настройка категорий или брендов - получаем данные для вывода в форме */
                        if(in_array($k, ['categories', 'manufacturers', 'suppliers'])) {
                            $items = [];
                            switch ($k){
                                case 'categories':
                                    foreach ($el as $id){
                                        $items[] = [
                                            'category_id' => $id,
                                            'name' => $categories[$id]
                                        ];
                                    }
                                    break;
                                case 'manufacturers':
                                    foreach ($el as $id){
                                        $items[] = [
                                            'manufacturer_id' => $id,
                                            'name' => $manufacturers[$id]
                                        ];
                                    }
                                    break; 
                                case 'suppliers':
                                    foreach ($el as $value){
                                        $items[] = html_entity_decode($value);
                                    }
                                    break;
                            }
                            
                            $el = $items;
                        }else {
                            $el = html_entity_decode($el);
                        }
                    });
                    
                    $auto_settings[] = $item;
                }
            }
            
            /** Дополнительные настройки и фильтры (элементы контроля) */
            $additional_settings = [];
            if($query->row['additional_settings']){
                foreach (json_decode($query->row['additional_settings'], true) as $key => $item){
                    if($key === 'attributes'){
                        $attributes = [];
                        $attributeNames = $this->getAttributeNamesById($item);
                        
                        foreach ($item as $value){
                            $attributes[] = [
                                'attribute_id' => $value,
                                'name' => $attributeNames[$value]
                            ];
                        }
                        
                        $item = $attributes;
                    }

                    $additional_settings[$key] = $item;
                }
            }
            
			return array(
                'setting_id'      => $query->row['setting_id'],
                'name'            => $query->row['name'],
                'description'     => $query->row['description'],
                'is_default'      => $query->row['is_default'],
                'filename'        => $query->row['filename'],
                'token'           => $query->row['token'],
                'url'             => $query->row['url'],
                'sales_note'      => html_entity_decode($query->row['sales_note']),
                'delivery_option' => html_entity_decode($query->row['delivery_option']),
                'status'          => $query->row['status'],
                'type'            => (int) $query->row['type'],
                'auto_settings'   => $auto_settings,
                'additional_settings'   => $additional_settings,
            );
		}
	}
	
	public function addSetting($data) {
		
	//$this->log->write($data);	
	
		if ((int)$data['is_default'] == 1) {
			$query = $this->db->query("UPDATE " . DB_PREFIX . "yam_setting SET is_default = '0'");
		}

        $data = $this->prepareDataBeforeSave($data);

        try{
            $this->db->query("INSERT INTO " . DB_PREFIX . "yam_setting SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', filename = '" . $this->db->escape($data['filename']) . "', token = '" . $this->db->escape($data['token']) . "', url = '', sales_note = '" . $this->db->escape($data['sales_note']) . "', delivery_option = '" . $this->db->escape($data['delivery_option']) . "', status = '" . (int)$data['status'] . "', is_default = '" . (int)$data['is_default'] . "', type = '" . (int)$data['type'] . "', auto_settings = '" . json_encode($data['auto_settings'], JSON_UNESCAPED_UNICODE) . "', additional_settings = '" . json_encode($data['additional_settings'], JSON_UNESCAPED_UNICODE) . "'");
        }catch (Exception $e){
            echo $e->getMessage();
        }
		
		$setting_id = $this->db->getLastId();
		return $setting_id;
	}
	
	public function editSetting($setting_id,$data) {
		
		if ((int)$data['is_default'] == 1) {
			$query = $this->db->query("UPDATE " . DB_PREFIX . "yam_setting SET is_default = '0'");
		}

        $data = $this->prepareDataBeforeSave($data);
		
		try{
            $this->db->query("UPDATE " . DB_PREFIX . "yam_setting SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', filename = '" . $this->db->escape($data['filename']) . "', token = '" . $this->db->escape($data['token']) . "', url = '', sales_note = '" . $this->db->escape($data['sales_note']) . "', delivery_option = '" . $this->db->escape($data['delivery_option']) . "', status = '" . (int)$data['status'] . "', is_default = '" . (int)$data['is_default'] . "', type = '" . (int)$data['type'] . "', auto_settings = '" . json_encode($data['auto_settings'], JSON_UNESCAPED_UNICODE) . "', additional_settings = '" . json_encode($data['additional_settings'], JSON_UNESCAPED_UNICODE) . "' WHERE setting_id = '" . (int)$setting_id . "'");
		}catch (Exception $e){
		    echo $e->getMessage();
        }
		
		return $setting_id;
	}

    /**
     * @param $data
     *
     * @return mixed
     */
	protected function prepareDataBeforeSave($data){
	    if($data['auto_settings']) {
            foreach ($data['auto_settings'] as &$autoSetting) {
                if (isset($autoSetting['suppliers'])) {
                    array_walk($autoSetting['suppliers'], function (&$el) {
                        $el = $this->db->escape($el);
                    });
                }
            }
        }
        
        return $data;
    }
	
	public function deleteSetting($setting_id) {
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "yam_setting WHERE setting_id = '" . (int)$setting_id . "'");
		
		return true;
	}
	
	public function getSettingProductTotal($setting_id) {
		$sql = "SELECT COUNT(DISTINCT product_id) AS total FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND yam_status = '1'";

		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	
	public function getAllCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, cd2.name AS name, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS fullname, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from " . DB_PREFIX . "product_to_category pc where pc.category_id = c1.category_id) as product_count FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = '1' AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'product_count',
			'fullname',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getManufacturers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getProductIds($setting_id) {
		$query = $this->db->query("SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND yam_status = '1' ORDER BY product_id");
			
		return $query->rows;
	}
	
	public function getCategoryIds($products = '') {
		$query = $this->db->query("SELECT DISTINCT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id IN(" . $products . ") ORDER BY category_id");

		return $query->rows;
	}
	
	public function getManufacturerIds($products = '') {
		$query = $this->db->query("SELECT DISTINCT manufacturer_id FROM " . DB_PREFIX . "product WHERE product_id IN(" . $products . ") ORDER BY manufacturer_id");

		return $query->rows;
	}
	
	public function getAttributeIds($products = '') {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute pa WHERE pa.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pa.product_id IN(" . $products . ") ORDER BY pa.product_id");

		return $query->rows;
	}
	
	public function getParents($range = '') {
		$query = $this->db->query("SELECT parent_id FROM " . DB_PREFIX . "category c WHERE c.category_id IN(" . $range . ") ORDER BY c.category_id");

		return $query->rows;
	}

	public function getCategories($range = '') {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id IN(" . $range . ") AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.category_id");

		return $query->rows;
	}

	public function getProducts($products, $start, $limit) {
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.status = 1 AND p.stock_status_id != " . $this->config->get('config_out_of_sale_stock_status_id') . " AND p.price > 0 AND p.product_id IN(" . $products . ") ORDER BY p.product_id ASC LIMIT " . (int)$start . "," . (int)$limit);
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.status = 1 AND p.stock_status_id != 9 AND p.price > 0 AND p.product_id IN(" . $products . ") ORDER BY p.product_id ASC LIMIT " . (int)$start . "," . (int)$limit);
		
		return $query->rows;
	}
	
	public function getAllProducts($products) {
		$query = $this->db->query("SELECT *, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = 1 AND p.price > 0 AND p.stock_status_id != 9 AND p.product_id IN(" . $products . ") ORDER BY p.product_id");
		//$query = $this->db->query("SELECT *, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . LANGUAGE_ID . "' AND p.status = 1 AND p.price > 0 AND p.yam_status = 1 AND p.stock_status_id != " . OUT_OF_SALE_STOCK_STATUS_ID . " ORDER BY p.product_id");
		
		return $query->rows;
	}
	
	public function getAlias($key) {
		$query = $this->db->query("SELECT DISTINCT keyword FROM " . DB_PREFIX . "seo_url WHERE query = '" . $key . "'");

		return $query->row;

	}
	
	public function getManufacturerName($manufacturer_id) {
		$query = $this->db->query("SELECT DISTINCT name FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . $manufacturer_id . "'");

		return $query->row;

	}
	
	public function getProductCategories($product_id) {
		$sql = ("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}
	
	public function getProductAttributes($product_id) {
		$product_attribute_data = array();

		$product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();

			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}

			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}

		return $product_attribute_data;
	}
	
	public function getAttribute($attribute_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_id = '" . (int)$attribute_id . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		$products = array();
		
		if (isset($data['filter_category']) && $data['filter_category'] !== '') {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
		}

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if (isset($data['filter_category']) && $data['filter_category'] !== '') {
			$sql .= " AND p2c.category_id = '" . (int)$data['filter_category'] . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_price_from'])) {
			$sql .= " AND p.price >= '" . (float)$data['filter_price_from'] . "'";
		}

		if (!empty($data['filter_price_to'])) {
			$sql .= " AND p.price <= '" . (float)$data['filter_price_to'] . "'";
		}

		if (isset($data['filter_quantity_from']) && $data['filter_quantity_from'] !== '') {
			$sql .= " AND p.quantity >= '" . (int)$data['filter_quantity_from'] . "'";
		}

		if (isset($data['filter_quantity_to']) && $data['filter_quantity_to'] !== '') {
			$sql .= " AND p.quantity <= '" . (int)$data['filter_quantity_to'] . "'";
		}

		if (isset($data['filter_manufacturer']) && $data['filter_manufacturer'] !== '') {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer'] . "'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_by_setting']) && $data['filter_by_setting'] !== 0) {

			if (isset($data['filter_setting_id']) && $data['filter_setting_id'] !== 0) {
				
				$subsql = "SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$data['filter_setting_id'] . "'";
				if (isset($data['filter_yam_status']) && $data['filter_yam_status'] !== '') {
					$subsql .= " AND yam_status = '" . (int)$data['filter_yam_status'] . "'";
				}
				if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 1) {
					$subsql .= " AND sales_note != ''";
				}
				if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 0) {
					$subsql .= " AND sales_note = ''";
				}
				if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 1) {
					$subsql .= " AND delivery_option != ''";
				}
				if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 0) {
					$subsql .= " AND delivery_option = ''";
				}
				$subsql .= " ORDER BY product_id";
				
				$query = $this->db->query($subsql);
		
				$results = $query->rows;
				$range = array();
				foreach ($results as $result) {
					$products[] = $result['product_id'];
				}
				
			}
			
			if (count($products) > 0) {
				$sql .= " AND p.product_id IN(" . implode(',', $products) . ")";
//			} else {
//				$sql .= " AND p.product_id = 0";
			}
		
		} else {

			if (isset($data['filter_setting_id']) && $data['filter_setting_id'] !== 0) {
				
				if (isset($data['filter_yam_status']) || isset($data['filter_sales_note']) || isset($data['filter_delivery_option'])) {
				
					$subsql = "SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$data['filter_setting_id'] . "'";
					if (isset($data['filter_yam_status']) && $data['filter_yam_status'] !== '') {
						$subsql .= " AND yam_status = '" . (int)$data['filter_yam_status'] . "'";
					}
					if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 1) {
						$subsql .= " AND sales_note != ''";
					}
					if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 0) {
						$subsql .= " AND sales_note = ''";
					}
					if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 1) {
						$subsql .= " AND delivery_option != ''";
					}
					if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 0) {
						$subsql .= " AND delivery_option = ''";
					}
					$subsql .= " ORDER BY product_id";
				
					$query = $this->db->query($subsql);
			
					$results = $query->rows;
					$range = array();
					foreach ($results as $result) {
						$products[] = $result['product_id'];
					}
					
					if (count($products) > 0) {
						$sql .= " AND p.product_id IN(" . implode(',', $products) . ")";
					} else {
						$sql .= " AND p.product_id = 0";
					}
				
				}
				
			}
		
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}

	public function getProductArr($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		$products = array();
		
		if (isset($data['filter_category']) && $data['filter_category'] !== '') {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
		}

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if (isset($data['filter_category']) && $data['filter_category'] !== '') {
			$sql .= " AND p2c.category_id = '" . (int)$data['filter_category'] . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_price_from'])) {
			$sql .= " AND p.price >= '" . (float)$data['filter_price_from'] . "'";
		}

		if (!empty($data['filter_price_to'])) {
			$sql .= " AND p.price <= '" . (float)$data['filter_price_to'] . "'";
		}

		if (isset($data['filter_quantity_from']) && $data['filter_quantity_from'] !== '') {
			$sql .= " AND p.quantity >= '" . (int)$data['filter_quantity_from'] . "'";
		}

		if (isset($data['filter_quantity_to']) && $data['filter_quantity_to'] !== '') {
			$sql .= " AND p.quantity <= '" . (int)$data['filter_quantity_to'] . "'";
		}

		if (isset($data['filter_manufacturer']) && $data['filter_manufacturer'] !== '') {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer'] . "'";
		}

		if (isset($data['filter_status']) && $data['filter_status'] !== '') {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

//		if (isset($data['filter_by_setting']) && $data['filter_by_setting'] !== 0) {
//
//			if (isset($data['filter_setting_id']) && $data['filter_setting_id'] !== 0) {
//				
//				$subsql = "SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$data['filter_setting_id'] . "'";
//				if (isset($data['filter_yam_status']) && $data['filter_yam_status'] !== '') {
//					$subsql .= " AND yam_status = '" . (int)$data['filter_yam_status'] . "'";
//				}
//				if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 1) {
//					$subsql .= " AND sales_note != ''";
//				}
//				if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 0) {
//					$subsql .= " AND sales_note = ''";
//				}
//				if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 1) {
//					$subsql .= " AND delivery_option != ''";
//				}
//				if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 0) {
//					$subsql .= " AND delivery_option = ''";
//				}
//				$subsql .= " ORDER BY product_id";
//				
//				$query = $this->db->query($subsql);
//		
//				$results = $query->rows;
//				$range = array();
//				foreach ($results as $result) {
//					$products[] = $result['product_id'];
//				}
//				
//			}
//		
//		} else {
//
//			if (isset($data['filter_setting_id']) && $data['filter_setting_id'] !== 0) {
//				
//				if (isset($data['filter_yam_status']) || isset($data['filter_sales_note']) || isset($data['filter_delivery_option'])) {
//				
//					$subsql = "SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$data['filter_setting_id'] . "'";
//					if (isset($data['filter_yam_status']) && $data['filter_yam_status'] !== '') {
//						$subsql .= " AND yam_status = '" . (int)$data['filter_yam_status'] . "'";
//					}
//					if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 1) {
//						$subsql .= " AND sales_note != ''";
//					}
//					if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 0) {
//						$subsql .= " AND sales_note = ''";
//					}
//					if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 1) {
//						$subsql .= " AND delivery_option != ''";
//					}
//					if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 0) {
//						$subsql .= " AND delivery_option = ''";
//					}
//					$subsql .= " ORDER BY product_id";
//				
//					$query = $this->db->query($subsql);
//			
//					$results = $query->rows;
//					$range = array();
//					foreach ($results as $result) {
//						$products[] = $result['product_id'];
//					}
//				
//				}
//				
//			}
//		
//		}
//		
//		if (count($products) > 0) {
//			
//			$sql .= " AND p.product_id IN(" . implode(',', $products) . ")";
//		
//		}
		
		if (isset($data['filter_by_setting']) && $data['filter_by_setting'] !== 0) {

			if (isset($data['filter_setting_id']) && $data['filter_setting_id'] !== 0) {
				
				$subsql = "SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$data['filter_setting_id'] . "'";
				if (isset($data['filter_yam_status']) && $data['filter_yam_status'] !== '') {
					$subsql .= " AND yam_status = '" . (int)$data['filter_yam_status'] . "'";
				}
				if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 1) {
					$subsql .= " AND sales_note != ''";
				}
				if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 0) {
					$subsql .= " AND sales_note = ''";
				}
				if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 1) {
					$subsql .= " AND delivery_option != ''";
				}
				if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 0) {
					$subsql .= " AND delivery_option = ''";
				}
				$subsql .= " ORDER BY product_id";
				
				$query = $this->db->query($subsql);
		
				$results = $query->rows;
				$range = array();
				foreach ($results as $result) {
					$products[] = $result['product_id'];
				}
				
			}
			
			if (count($products) > 0) {
				$sql .= " AND p.product_id IN(" . implode(',', $products) . ")";
			} else {
				$sql .= " AND p.product_id = 0";
			}
		
		} else {

			if (isset($data['filter_setting_id']) && $data['filter_setting_id'] !== 0) {
				
				if (isset($data['filter_yam_status']) || isset($data['filter_sales_note']) || isset($data['filter_delivery_option'])) {
				
					$subsql = "SELECT DISTINCT product_id FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$data['filter_setting_id'] . "'";
					if (isset($data['filter_yam_status']) && $data['filter_yam_status'] !== '') {
						$subsql .= " AND yam_status = '" . (int)$data['filter_yam_status'] . "'";
					}
					if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 1) {
						$subsql .= " AND sales_note != ''";
					}
					if (isset($data['filter_sales_note']) && $data['filter_sales_note'] == 0) {
						$subsql .= " AND sales_note = ''";
					}
					if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 1) {
						$subsql .= " AND delivery_option != ''";
					}
					if (isset($data['filter_delivery_option']) && $data['filter_delivery_option'] == 0) {
						$subsql .= " AND delivery_option = ''";
					}
					$subsql .= " ORDER BY product_id";
				
					$query = $this->db->query($subsql);
			
					$results = $query->rows;
					$range = array();
					foreach ($results as $result) {
						$products[] = $result['product_id'];
					}
				
					if (count($products) > 0) {
						$sql .= " AND p.product_id IN(" . implode(',', $products) . ")";
		//			} else {
		//				$sql .= " AND p.product_id = 0";
					}
				
				}
				
			}
			
		
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
//$this->log->write('sql = ' . $sql);
//$this->log->write(print_r($query->rows));

		return $query->rows;
	}
	
	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}
	
	public function getProductData($setting_id, $product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND product_id = '" . (int)$product_id . "'");

		return $query->row;
	}
	
	public function getDeliveryOptions($setting_id, $products) {
		$query = $this->db->query("SELECT DISTINCT delivery_option FROM " . DB_PREFIX . "yam_product WHERE setting_id = '" . (int)$setting_id . "' AND yam_status = '1' AND product_id IN(" . implode(',', $products) . ") ORDER BY delivery_option LIMIT 4");
		
		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}
	
	public function setSalesNotes($setting_id, $arr_products, $note) {
		
		foreach ($arr_products as $product_id) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "yam_product WHERE product_id = '" . (int)$product_id . "' AND setting_id = '" . (int)$setting_id . "'");
			if ($query->rows) {
				$this->db->query("UPDATE " . DB_PREFIX . "yam_product SET sales_note = '" . $this->db->escape($note) . "' WHERE product_id = '" . (int)$product_id . "' AND setting_id = '" . (int)$setting_id . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "yam_product SET setting_id = '" . (int)$setting_id . "', product_id = '" . (int)$product_id . "', yam_status = '0', sales_note = '" . $this->db->escape($note) . "', delivery_option = ''");
			}
		}

		return true;
	}
	
	public function setDeliveryOptions($setting_id, $arr_products, $option) {
		
		foreach ($arr_products as $product_id) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "yam_product WHERE product_id = '" . (int)$product_id . "' AND setting_id = '" . (int)$setting_id . "'");
			if ($query->rows) {
				$this->db->query("UPDATE " . DB_PREFIX . "yam_product SET delivery_option = '" . $this->db->escape($option) . "' WHERE product_id = '" . (int)$product_id . "' AND setting_id = '" . (int)$setting_id . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "yam_product SET setting_id = '" . (int)$setting_id . "', product_id = '" . (int)$product_id . "', yam_status = '0', sales_note = '', delivery_option = '" . $this->db->escape($option) . "'");
			}
		}

		return true;
	}
	
	public function setStatus($setting_id, $arr_products, $mode) {
		
		foreach ($arr_products as $product_id) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "yam_product WHERE product_id = '" . (int)$product_id . "' AND setting_id = '" . (int)$setting_id . "'");
			if ($query->rows) {
				$this->db->query("UPDATE " . DB_PREFIX . "yam_product SET yam_status = '" . (int)$mode . "' WHERE product_id = '" . (int)$product_id . "' AND setting_id = '" . (int)$setting_id . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "yam_product SET setting_id = '" . (int)$setting_id . "', product_id = '" . (int)$product_id . "', yam_status = '" . (int)$mode . "', sales_note = '', delivery_option = ''");
			}
		}
			
		return true;
	}
	

	public function getPromos() {
		$sql = "SELECT * FROM " . DB_PREFIX . "coupon WHERE ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1' ORDER BY code ASC";
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}

	public function getPromoProducts($coupon_id) {
		$coupon_product_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_product WHERE coupon_id = '" . (int)$coupon_id . "'");

		foreach ($query->rows as $result) {
			$coupon_product_data[] = $result['product_id'];
		}

		return $coupon_product_data;
	}
	
	public function getPromoCategories($coupon_id) {
		$coupon_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "coupon_category WHERE coupon_id = '" . (int)$coupon_id . "'");

		foreach ($query->rows as $result) {
			$coupon_category_data[] = $result['category_id'];
		}

		return $coupon_category_data;
	}

	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p2c.category_id = '" . (int)$category_id . "'");

		return $query->rows;
	}

    public function getAttributeNamesById(array $attribute_ids) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_id IN (" . implode(',', $attribute_ids) . ") AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        $attributes = [];
        foreach ($query->rows as $attribute){
            $attributes[$attribute['attribute_id']] = $attribute['name'];
        }
        
        return $attributes;
    }
    
    public function getSuppliers(){
        $query = $this->db->query("SELECT DISTINCT `supplier_main` FROM ".DB_PREFIX."product WHERE `supplier_main` != ''");

        return $query->rows;
    }
	
	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "yam_setting` (
			`setting_id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(64) NOT NULL,
			`description` text NOT NULL,
			`status` tinyint(1) NOT NULL,
			`is_default` tinyint(1) NOT NULL,
			`filename` varchar(255) NOT NULL,
			`token` varchar(255) NOT NULL,
			`url` varchar(255) NOT NULL,
			`sales_note` varchar(255) NOT NULL,
			`delivery_option` varchar(255) NOT NULL,
			`type` smallint(1) NOT NULL DEFAULT '1',
            `auto_settings` mediumtext NOT NULL,
            `additional_settings` text NOT NULL,
			PRIMARY KEY (`setting_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		");
        $this->log->write('Таблица ' . DB_PREFIX . 'yam_setting - создана');
		
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "yam_product` (
			`setting_id` int(11) NOT NULL,
			`product_id` int(11) NOT NULL,
			`yam_status` tinyint(1) NOT NULL,
			`sales_note` varchar(255) NOT NULL,
			`delivery_option` varchar(255) NOT NULL,
			PRIMARY KEY (`setting_id`,`product_id`) USING BTREE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		");
        $this->log->write('Таблица ' . DB_PREFIX . 'yam_product - создана');

        $this->db->query("
        ALTER TABLE `oc_product` ADD `vendor_code` varchar(128) NOT NULL, 
            ADD `yam_status` tinyint(1) NOT NULL DEFAULT '0',
            ADD `sup_price` decimal(15,4) NOT NULL DEFAULT '0.0000', 
            ADD `price_type` varchar(32) NOT NULL COMMENT 'Тип цены', 
            ADD `supplier_main` varchar(100) NOT NULL COMMENT 'Поставщик основной', 
            ADD `supplier_spec` varchar(100) NOT NULL COMMENT 'Поставщик спец цены', 
            ADD KEY `yam_status` (`yam_status`), ADD KEY `price_type` (`price_type`);");
		
		$dir = DIR_STORAGE . 'multifastfeed/';
		if (!file_exists($dir)) {
			if (mkdir($dir, 0755) === false) {
                $this->log->write('Директория ' . $dir . ' - не создана');
			} else {
                $this->log->write('Директория ' . $dir . ' - создана');
			}
		} else {
            $this->log->write('Директория ' . $dir . ' - существует');
		}

		
	}

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "yam_setting`");
$this->log->write('Таблица ' . DB_PREFIX . 'yam_setting - удалена');
		
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "yam_product`");
$this->log->write('Таблица ' . DB_PREFIX . 'yam_product - удалена');
		
		$dir = DIR_STORAGE . 'multifastfeed/';
		if (file_exists($dir)) {
			rmdir($dir);
$this->log->write('Директория ' . $dir . ' - удалена');
		}

	}

}

<?php
class ModelMarketingGiftProduct extends Model {
	public function addSetting($data) {
		
		$option_name = $this->getOptionName($data['option_id']);
		$option_value_name = $this->getOptionValueName($data['option_value_id']);
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "gift_product SET setting_name = '" . $this->db->escape($data['setting_name']) . "', option_name = '" . $this->db->escape($option_name) . "', option_value_name = '" . $this->db->escape($option_value_name) . "', option_id = '" . (int)$data['option_id'] . "', option_value_id = '" . (int)$data['option_value_id'] . "', gift_quantity = '" . (int)$data['gift_quantity'] . "', price_from = '" . (float)$data['price_from'] . "', price_to = '" . (float)$data['price_to'] . "', stock_from = '" . (int)$data['stock_from'] . "', stock_to = '" . (int)$data['stock_to'] . "', setting_categories = '" . $this->db->escape($data['setting_categories']) . "', setting_manufacturers = '" . $this->db->escape($data['setting_manufacturers']) . "', setting_statuses = '" . $this->db->escape($data['setting_statuses']) . "', status = '" . (int)$data['status'] . "'");

		$setting_id = $this->db->getLastId();
		
		$this->updateOptions();		

		return $setting_id;
	}

	public function editSetting($setting_id, $data) {
		
		$this->log->write('изменение настройки - начало');		
		
		$option_name = $this->getOptionName($data['option_id']);
		$option_value_name = $this->getOptionValueName($data['option_value_id']);
		
		$this->db->query("UPDATE " . DB_PREFIX . "gift_product SET setting_name = '" . $this->db->escape($data['setting_name']) . "', option_name = '" . $this->db->escape($option_name) . "', option_value_name = '" . $this->db->escape($option_value_name) . "', option_id = '" . (int)$data['option_id'] . "', option_value_id = '" . (int)$data['option_value_id'] . "', gift_quantity = '" . (int)$data['gift_quantity'] . "', price_from = '" . (float)$data['price_from'] . "', price_to = '" . (float)$data['price_to'] . "', stock_from = '" . (int)$data['stock_from'] . "', stock_to = '" . (int)$data['stock_to'] . "', setting_categories = '" . $this->db->escape($data['setting_categories']) . "', setting_manufacturers = '" . $this->db->escape($data['setting_manufacturers']) . "', setting_statuses = '" . $this->db->escape($data['setting_statuses']) . "', status = '" . (int)$data['status'] . "' WHERE setting_id = '" . (int)$setting_id . "'");
		
		if ($data['status']) {$this->updateGiftQuantity($data['option_value_id'], $data['gift_quantity']);}
		
		$this->updateOptions();		

		$this->log->write('изменение настройки - конец');		
	}

	public function deleteSetting($setting_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "gift_product WHERE setting_id = '" . (int)$setting_id . "'");
		
		$this->updateOptions();		

	}

	public function updateGiftQuantity($option_value_id, $gift_quantity) {
		$this->db->query("UPDATE " . DB_PREFIX . "gift_product SET gift_quantity = '" . (int)$gift_quantity . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
	}

	public function refreshOptions() {
		
		$this->updateOptions();		
		
	}
		
	public function updateOptions() {
		
		$this->db->query("TRUNCATE " . DB_PREFIX . "product_option");
		$this->db->query("TRUNCATE " . DB_PREFIX . "product_option_value");
		
		$option_data = array();
		$options = array();
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "gift_product WHERE status = '1'");
		
		$settings = $query->rows;
		
		foreach ($settings as $setting) {
			$setting_data = $this->getSetting($setting['setting_id']);
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "product WHERE status = '1'";
			if ((float)$setting_data['price_from'] > 0) {
				$sql .= " AND price >= '" . (float)$setting_data['price_from'] . "'";
			}
			if ((float)$setting_data['price_to'] > 0) {
				$sql .= " AND price <= '" . (float)$setting_data['price_to'] . "'";
			}
			if ((int)$setting_data['stock_from'] > 0) {
				$sql .= " AND quantity >= '" . (int)$setting_data['stock_from'] . "'";
			}
			if ((int)$setting_data['stock_to'] > 0) {
				$sql .= " AND quantity <= '" . (int)$setting_data['stock_to'] . "'";
			}
			if ($setting_data['setting_manufacturers'] != '') {
				$sql .= " AND manufacturer_id IN (" . $setting_data['setting_manufacturers'] . ")";
			}
			if ($setting_data['setting_statuses'] != '') {
				$sql .= " AND stock_status_id IN (" . $setting_data['setting_statuses'] . ")";
			}
			if ($setting_data['setting_categories'] != '') {
				$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p2c.category_id IN (" . $setting_data['setting_categories'] . ")");
				$results = $query->rows;
				$arr = array();
				foreach ($results as $result) {
					$arr[] = $result['product_id'];
				}
				$product_range = implode(",", $arr);
				$sql .= " AND product_id IN (" . $product_range . ")";
			}
			
			$query = $this->db->query($sql);
			$products = $query->rows;
			$product_quantity = count($products);
			$this->db->query("UPDATE " . DB_PREFIX . "gift_product SET product_quantity = '" . (int)$product_quantity . "' WHERE setting_id = '" . (int)$setting['setting_id'] . "'");
			foreach ($products as $product) {
				$option_data[] = array(
					'product_id' 			=> $product['product_id'],
					'option_id'       => $setting_data['option_id'],
					'option_value_id'	=> $setting_data['option_value_id'],
					'quantity'				=> $setting_data['gift_quantity']
				);
			}
		}
		
		$option_data = array_unique($option_data, SORT_REGULAR);
		
		usort($option_data, function($a, $b){
    		//return $a['product_id'] <=> $b['product_id'];
    		return $a['product_id'] <= $b['product_id'];
		});
		
		foreach ($option_data as $option) {
			$product_option_id = false;
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$option['product_id'] . "' AND option_id = '" . (int)$option['option_id'] . "'");

			if ($query->num_rows) {
				$product_option_id = $query->row['product_option_id'];
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$option['product_id'] . "', option_id = '" . (int)$option['option_id'] . "', required = '0', value = ''");
				$product_option_id = $this->db->getLastId();
			}
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_id = '" . (int)$option['product_id'] . "', product_option_id = '" . (int)$product_option_id . "', subtract = '1', option_id = '" . (int)$option['option_id'] . "', option_value_id = '" . (int)$option['option_value_id'] . "', quantity = '" . (int)$option['quantity'] . "'");
		}
	}

	public function getSetting($setting_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "gift_product WHERE setting_id = '" . (int)$setting_id . "'");

		return $query->row;
	}
	
	public function getOptions($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE od.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		$sql .= " ORDER BY od.name";
		$sql .= " ASC";
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getOptionValues() {
		$option_value_data = array();

		$option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.option_id, ov.sort_order, ovd.name");

		foreach ($option_value_query->rows as $option_value) {
			$option_value_data[] = array(
				'option_id' 			=> $option_value['option_id'],
				'option_value_id' => $option_value['option_value_id'],
				'name'            => $option_value['name'],
				'image'           => $option_value['image'],
				'sort_order'      => $option_value['sort_order']
			);
		}

		return $option_value_data;
	}
		
	public function getOptionName($option_id) {
		$sql = "SELECT name FROM `" . DB_PREFIX . "option_description` WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND option_id = '" . (int)$option_id . "'";
		
		$query = $this->db->query($sql);

		return $query->row['name'];
	}
		
	public function getOptionValueName($option_value_id) {
		$sql = "SELECT name FROM `" . DB_PREFIX . "option_value_description` WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND option_value_id = '" . (int)$option_value_id . "'";
		
		$query = $this->db->query($sql);

		return $query->row['name'];
	}
	
	public function getAllCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, cd2.name AS name, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&and;') AS fullname, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from " . DB_PREFIX . "product_to_category pc where pc.category_id = c1.category_id) as product_count FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = '1' AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'product_count',
			'fullname',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getAllManufacturers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getStockStatuses() {
		$sql = "SELECT * FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sql .= " ORDER BY name";
		$sql .= " ASC";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSettings($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "gift_product";

		$sort_data = array(
			'setting_name',
			'option_name',
			'option_value_name',
			'gift_quantity',
			'product_quantity',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY setting_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSettings() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "gift_product");

		return $query->row['total'];
	}

	public function getTotalProducts($data = array()) {
		$setting_data = $this->getSetting($data['setting_id']);
		
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE status = '1'";
		
		if ((float)$setting_data['price_from'] > 0) {
			$sql .= " AND price >= '" . (float)$setting_data['price_from'] . "'";
		}
		if ((float)$setting_data['price_to'] > 0) {
			$sql .= " AND price <= '" . (float)$setting_data['price_to'] . "'";
		}
		if ((int)$setting_data['stock_from'] > 0) {
			$sql .= " AND quantity >= '" . (int)$setting_data['stock_from'] . "'";
		}
		if ((int)$setting_data['stock_to'] > 0) {
			$sql .= " AND quantity <= '" . (int)$setting_data['stock_to'] . "'";
		}
		if ($setting_data['setting_manufacturers'] != '') {
			$sql .= " AND manufacturer_id IN (" . $setting_data['setting_manufacturers'] . ")";
		}
		if ($setting_data['setting_statuses'] != '') {
			$sql .= " AND stock_status_id IN (" . $setting_data['setting_statuses'] . ")";
		}
		if ($setting_data['setting_categories'] != '') {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p2c.category_id IN (" . $setting_data['setting_categories'] . ")");
			$results = $query->rows;
			$arr = array();
			foreach ($results as $result) {
				$arr[] = $result['product_id'];
			}
			$product_range = implode(",", $arr);
			$sql .= " AND product_id IN (" . $product_range . ")";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getProducts($data = array()) {
		$setting_data = $this->getSetting($data['setting_id']);
		
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = 1";
		
		if ((float)$setting_data['price_from'] > 0) {
			$sql .= " AND p.price >= '" . (float)$setting_data['price_from'] . "'";
		}
		if ((float)$setting_data['price_to'] > 0) {
			$sql .= " AND p.price <= '" . (float)$setting_data['price_to'] . "'";
		}
		if ((int)$setting_data['stock_from'] > 0) {
			$sql .= " AND p.quantity >= '" . (int)$setting_data['stock_from'] . "'";
		}
		if ((int)$setting_data['stock_to'] > 0) {
			$sql .= " AND p.quantity <= '" . (int)$setting_data['stock_to'] . "'";
		}
		if ($setting_data['setting_manufacturers'] != '') {
			$sql .= " AND p.manufacturer_id IN (" . $setting_data['setting_manufacturers'] . ")";
		}
		if ($setting_data['setting_statuses'] != '') {
			$sql .= " AND p.stock_status_id IN (" . $setting_data['setting_statuses'] . ")";
		}
		if ($setting_data['setting_categories'] != '') {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p2c.category_id IN (" . $setting_data['setting_categories'] . ")");
			$results = $query->rows;
			$arr = array();
			foreach ($results as $result) {
				$arr[] = $result['product_id'];
			}
			$product_range = implode(",", $arr);
			$sql .= " AND p.product_id IN (" . $product_range . ")";
		}
		
		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'p.quantity',
			'p.stock_status_id'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}
	
	public function getStockStatus($stock_status_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE stock_status_id = '" . (int)$stock_status_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}
	
}
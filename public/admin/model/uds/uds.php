<?php
class ModelUdsUds extends Model {
	
	public function getCatalog() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uds_product");
		if ($query->num_rows) {
			return $query->rows;
		} else {
			return false;
		}
	}
	
	public function getProduct($uds_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uds_product WHERE uds_id = '" . (int)$uds_id . "'");
		if ($query->num_rows) {
			return $query->rows[0];
		} else {
			return false;
		}
	}
	
	public function getProductName($oc_id) {
		$query = $this->db->query("SELECT DISTINCT name FROM  " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$oc_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
		//return $query->row['name'];
		if (isset($query->row['name'])) {
			return $query->row['name'];
		} else {
			return false;
		}
	}
	
	public function addProduct($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "uds_product SET type = '" . $this->db->escape($data['type']) . "', oc_id = '" . (int)$data['oc_id'] . "', uds_id = '" . (int)$data['uds_id'] . "', nodeId = '" . (int)$data['nodeId'] . "', externalId = '" . $this->db->escape($data['externalId']) . "', oc_name = '" . $this->db->escape($data['oc_name']) . "', uds_name = '" . $this->db->escape($data['uds_name']) . "', status = '" . (int)$data['status'] . "', hidden = '" . (int)$data['hidden'] . "', blocked = '" . (int)$data['blocked'] . "', date_modified = NOW(), date_added = NOW()");
	}
	
	public function editProduct($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "uds_product SET type = '" . $this->db->escape($data['type']) . "', oc_id = '" . (int)$data['oc_id'] . "', uds_id = '" . (int)$data['uds_id'] . "', nodeId = '" . (int)$data['nodeId'] . "', externalId = '" . $this->db->escape($data['externalId']) . "', oc_name = '" . $this->db->escape($data['oc_name']) . "', uds_name = '" . $this->db->escape($data['uds_name']) . "', status = '" . (int)$data['status'] . "', hidden = '" . (int)$data['hidden'] . "', blocked = '" . (int)$data['blocked'] . "', date_modified = NOW(), date_added = NOW() WHERE id = '" . (int)$id . "'");
	}
	
	public function deleteProducts($nodeId) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "uds_product WHERE nodeId = '" . (int)$nodeId . "'");
	}
	
	public function getCategory($uds_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uds_product WHERE uds_id = '" . (int)$uds_id . "'");
		if ($query->num_rows) {
			return $query->rows[0];
		} else {
			return false;
		}
	}
	
	public function getCategoryName($oc_id) {
		$query = $this->db->query("SELECT * FROM  " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$oc_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
		if (isset($query->row['name'])) {
			return $query->row['name'];
		} else {
			return false;
		}
	}
	
	public function addCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "uds_product SET type = '" . $this->db->escape($data['type']) . "', oc_id = '" . (int)$data['oc_id'] . "', uds_id = '" . (int)$data['uds_id'] . "', nodeId = '" . (int)$data['nodeId'] . "', externalId = '" . $this->db->escape($data['externalId']) . "', oc_name = '" . $this->db->escape($data['oc_name']) . "', uds_name = '" . $this->db->escape($data['uds_name']) . "', status = '" . (int)$data['status'] . "', hidden = '" . (int)$data['hidden'] . "', blocked = '" . (int)$data['blocked'] . "', date_modified = NOW(), date_added = NOW()");
	}
	
	public function editCategory($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "uds_product SET type = '" . $this->db->escape($data['type']) . "', oc_id = '" . (int)$data['oc_id'] . "', uds_id = '" . (int)$data['uds_id'] . "', nodeId = '" . (int)$data['nodeId'] . "', externalId = '" . $this->db->escape($data['externalId']) . "', oc_name = '" . $this->db->escape($data['oc_name']) . "', uds_name = '" . $this->db->escape($data['uds_name']) . "', status = '" . (int)$data['status'] . "', hidden = '" . (int)$data['hidden'] . "', blocked = '" . (int)$data['blocked'] . "', date_modified = NOW(), date_added = NOW() WHERE id = '" . (int)$id . "'");
	}
	
	public function deleteCategories() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "uds_product WHERE type = '" . $this->db->escape('CATEGORY') . "'");
	}
	
	public function getProductById($product_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}
	
	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p.status = '1' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");
//		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p.status = '1' AND p.quantity > 0 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}
	
	public function getProductOptions($product_id, $option_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND o.option_id = '" . (int)$option_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");
		//$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order");

			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'image'                   => $product_option_value['image'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}

		return $product_option_data;
	}
	
	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}
	
	public function getAllCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, cd2.name AS name, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&and;') AS fullname, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from " . DB_PREFIX . "product_to_category pc where pc.category_id = c1.category_id) as product_count FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = '1' AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'product_count',
			'fullname',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	
//	public function install() {
//		
//		$this->load->model('user/user_group');
//
//		$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/total/uds');
//		$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/total/uds');
//		$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'uds/uds_product');
//		$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'uds/uds_product');
//		
//		$this->db->query(
//			"CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "uds_product` (
//			  `id` int(11) NOT NULL,
//			  `type` varchar(12) NOT NULL,
//			  `oc_id` int(11) NOT NULL,
//			  `uds_id` bigint(13) NOT NULL,
//			  `nodeId` bigint(13) NOT NULL,
//			  `externalId` varchar(64) NOT NULL,
//			  `oc_name` varchar(128) NOT NULL,
//			  `uds_name` varchar(128) NOT NULL,
//			  `status` tinyint(1) NOT NULL,
//			  `hidden` tinyint(1) NOT NULL,
//			  `blocked` tinyint(1) NOT NULL,
//			  `date_added` datetime NOT NULL,
//			  `date_modified` datetime NOT NULL
//			) ENGINE=MyISAM DEFAULT CHARSET=utf8"
//		);
//		
//		$this->db->query(
//			"CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "uds_customer` (
//			  `id` int(11) NOT NULL,
//			  `customer_id` int(11) NOT NULL,
//			  `uds_customer_id` bigint(13) NOT NULL,
//			  `uds_customer_uid` varchar(36) NOT NULL,
//			  `uds_display_name` varchar(128) NOT NULL,
//			  `email` varchar(96) NOT NULL,
//			  `uds_phone` varchar(12) NOT NULL,
//			  `status` tinyint(1) NOT NULL,
//			  `date_added` datetime NOT NULL,
//			  `date_modified` datetime NOT NULL
//			) ENGINE=MyISAM DEFAULT CHARSET=utf8"
//		);
//		
//		$this->db->query(
//			"CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "uds_order` (
//			  `id` int(11) NOT NULL,
//			  `order_id` int(11) NOT NULL,
//			  `order_status_id` int(11) NOT NULL,
//			  `uds_type` varchar(8) NOT NULL,
//			  `uds_customer_id` bigint(13) NOT NULL,
//			  `uds_phone` varchar(12) NOT NULL,
//			  `uds_operation_code` varchar(36) NOT NULL,
//			  `uds_operation_id` int(11) NOT NULL,
//			  `uds_order_total` decimal(15,2) NOT NULL,
//			  `uds_skip_loyalty_total` decimal(15,2) NOT NULL,
//			  `uds_points` decimal(15,2) NOT NULL,
//			  `uds_cash` decimal(15,2) NOT NULL,
//			  `uds_cashback` decimal(15,2) NOT NULL,
//			  `comment` text NOT NULL,
//			  `date_added` datetime NOT NULL,
//			  `date_modified` datetime NOT NULL
//			) ENGINE=MyISAM DEFAULT CHARSET=utf8"
//		);
//		
//		$this->session->data['success'] = $this->language->get('text_success');
//	}
//
//	public function uninstall() {
//
//	}
	
	
}
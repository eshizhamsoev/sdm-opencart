<?php
// Heading
$_['heading_title']        = 'Octemplates - Преимущества магазина';

// Text
$_['text_module']          = 'Модули';
$_['text_success']         = 'Настройки модуля успешно обновлены!';
$_['text_success_install']     	= 'Модуль успешно установлен!';
$_['text_edit']            = 'Редактирование модуля';
$_['text_select_all']      = 'Выделить всё';
$_['text_unselect_all']    = 'Снять выделение';
$_['text_block_1']         = 'Блок 1';
$_['text_block_2']         = 'Блок 2';
$_['text_block_3']         = 'Блок 3';
$_['text_block_4']         = 'Блок 4';

// Entry
$_['entry_name']           = 'Название';
$_['entry_link_heading']   = 'Ссылка на статью';
$_['entry_text']           = 'Текст блока';
$_['entry_icon']           = 'Иконка блока';
$_['entry_icon_title']     = 'Выберите иконку';
$_['entry_background_block'] = 'Фон блока';
$_['entry_heading']        = 'Заголовок';
$_['entry_text_heading']   = 'Текст заголовка';
$_['entry_category']       = 'Выберите категорию';
$_['entry_color_heading']  = 'Цвет заголовка';
$_['entry_color_icon']     = 'Цвет иконки';
$_['entry_width']          = 'Ширина картинки товара';
$_['entry_color_text']     = 'Цвет текста';
$_['entry_background_block_hover'] = 'Цвет плашки блока при наведении';
$_['entry_image']          = 'Отображать картинку категории';
$_['entry_sub_categories'] = 'Отображать подкатегории';
$_['entry_status']         = 'Статус';

// Error
$_['error_permission']     = 'У Вас нет прав для управления этим модулем!';
$_['error_name']           = 'Название модуля должно быть от 3 до 64 символов!';
$_['error_heading_block1']        = 'Заголовок должен быть от 1 до 255 символов!';
$_['error_text_block1']           = 'Необходимо указать текст блока!';
$_['error_color_text_block1']     = 'Необходимо указать цвет текста!';
$_['error_background_block_hover_block1'] = 'Необходимо указать цвет плашки блока при наведении!';
$_['error_color_heading_block1']  = 'Необходимо указать цвет заголовка!';
$_['error_color_icon_block1']     = 'Необходимо указать цвет иконки!';
$_['error_icon_block1']           = 'Необходимо выбрать иконку!';
$_['error_heading_block2']        = 'Заголовок должен быть от 1 до 255 символов!';
$_['error_text_block2']           = 'Необходимо указать текст блока!';
$_['error_color_text_block2']     = 'Необходимо указать цвет текста!';
$_['error_background_block_hover_block2'] = 'Необходимо указать цвет фона блока при наведении!';
$_['error_color_heading_block2']  = 'Необходимо указать цвет заголовка!';
$_['error_color_icon_block2']     = 'Необходимо указать цвет иконки!';
$_['error_icon_block2']           = 'Необходимо выбрать иконку!';
$_['error_heading_block3']        = 'Заголовок должен быть от 1 до 255 символов!';
$_['error_text_block3']           = 'Необходимо указать текст блока!';
$_['error_color_text_block3']     = 'Необходимо указать цвет текста!';
$_['error_background_block_hover_block3'] = 'Необходимо указать цвет фона блока при наведении!';
$_['error_color_heading_block3']  = 'Необходимо указать цвет заголовка!';
$_['error_color_icon_block3']     = 'Необходимо указать цвет иконки!';
$_['error_icon_block3']           = 'Необходимо выбрать иконку!';
$_['error_heading_block4']        = 'Заголовок должен быть от 1 до 255 символов!';
$_['error_text_block4']           = 'Необходимо указать текст блока!';
$_['error_color_text_block4']     = 'Необходимо указать цвет текста!';
$_['error_background_block_hover_block4'] = 'Необходимо указать цвет фона блока при наведении!';
$_['error_color_heading_block4']  = 'Необходимо указать цвет заголовка!';
$_['error_color_icon_block4']     = 'Необходимо указать цвет иконки!';
$_['error_icon_block4']           = 'Необходимо выбрать иконку!';
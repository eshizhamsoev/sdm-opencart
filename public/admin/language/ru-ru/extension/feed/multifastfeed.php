<?php
// Heading
$_['heading_title']    = 'Яндекс Маркет Фид (Мульти)';

// Text
$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';


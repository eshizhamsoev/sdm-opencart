<?php
// Heading
$_['heading_title']			= 'Яндекс.Маркет';
$_['heading_title_groups']	= 'Группы товаров';
$_['heading_title_load']	= 'Загрузка каталога';
$_['heading_title_prices']	= 'Назначение цен';
$_['heading_title_status']	= 'Статус товаров';
$_['heading_offer_binding']	= 'Сопоставление товаров из личного кабинета';
$_['heading_title_market']	= 'Маркет';
$_['heading_orders_cancellation']= 'Отмены заказов';
// Text
$_['text_extension']			= 'Расширения';
$_['text_success']				= 'Настройки сохранены';
$_['text_edit']					= 'Редактирование';
$_['text_store_default']		= 'Основной магазин';
$_['text_fielsets']				= 'Сопоставление полей';
$_['text_auth_token']			= 'Авторизационный токен';
$_['text_auth_token_info']		= 'Укажите, если подключены к маркетплейсу по модели «Витрина + доставка».';
$_['text_companyId']			= 'Идентификатор кампании';
$_['text_yandex_oauth']			= 'OAuth-токен'; 
$_['text_yandex_market'] 		= 'Яндекс.Маркет';
$_['text_yandex_beru']			= 'Яндекс.Маркет';
$_['text_get_oauth_token']		= 'Чтобы получить OAuth-токен, перейдите по ссылке <a href="https://oauth.yandex.ru/authorize?response_type=token&client_id=40fd2df6a0004cea8079b25325d2f2e2" target="_blank">ссылке</a>';
$_['text_element']				= 'Элемент каталога';
$_['text_source']				= 'Источник в OpenCart';
$_['text_opencart_field']		= 'Поле в OpenCart';
$_['text_confirm_delete_field'] = 'Вы действительно хотите удалить это поле?';
$_['text_delete']				= 'Удалить';
$_['text_add']					= 'Добавить';
$_['text_add_attribute']		= 'Добавить дополнительный элемент';
$_['text_close']				= 'Закрыть';
$_['text_filter']				= 'Фильтр';
$_['text_select_attribute']		= 'Выберите элемент';
$_['text_product_list']			= 'Список товаров';
$_['text_group_list']			= 'Группы товаров для выгрузки';
$_['text_group_products']		= 'Товары в группе';
$_['text_add_group']			= 'Добавление группы товаров';
$_['text_edit_group']     		= 'Редактирование группы товаров';
$_['text_products_filter']		= 'Фильтрация и добавление товаров в группу "%s"';
$_['text_group_empty']			= 'В группе пока нет товаров';
$_['text_success_update_price']	= 'Цены для продвижения обновлены';
$_['text_card_finded']			= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Карточка найдена</b>';
$_['text_card_not_finded']		= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Карточка не найдена</b>';
$_['text_delete_user']			= 'Пользователь был удален';
$_['text_offer_loaded']			= 'Предложение загружено';
$_['text_info_status']			= 'Если вы работаете по модели FBS, сопоставьте статусы заказов маркетплейса со статусами OpenCart.';
$_['text_wigth_kg']				= 'Единица измерения веса, соответствующая килограмму';
$_['text_wigth_kg_info']		= 'Изменить единицы измерения веса можно в разделе Система/Локализация/Единицы веса';
$_['text_length_cm']			= 'Единица измерения длины, соответствующая сантиметру';
$_['text_length_cm_info']		= 'Изменить единицы измерения длины можно в разделе Система/Локализация/Единицы измерения';
$_['text_yandex_link']			= 'URL для запросов API';
$_['text_yandex_link_info']		= 'Укажите данный URL в настройках API личного кабинета Яндекс Маркет';
$_['text_dbs_settings_tab']		= 'Основные настройки';
$_['text_dbs_product_tab']      = 'Фильтр товаров';
$_['text_dbs_shipping_heading'] = 'Настройки доставки';
$_['text_dbs_shipping_name']    = 'Название доставки';
$_['text_dbs_shipping_fromDate']= 'Ближайшая возможная дата доставки в днях от даты заказа';
$_['text_dbs_shipping_toDate']  = 'Самая поздняя дата доставки в днях от даты заказа';
$_['text_dbs_shipping_shipmentDate'] = 'Дата отгрузки в днях от даты заказа';
$_['text_dbs_shipping_interval']= 'Интервал доставки';
$_['text_dbs_shipping_interval_from'] = 'Время от';
$_['text_dbs_shipping_interval_to']   = 'Время до';
$_['text_dbs_shipping_interval_from_placeholder'] = '11:00';
$_['text_dbs_shipping_interval_to_placeholder']   = '13:00';
$_['text_dbs_shipping_shipping_zone'] = 'Регион';
$_['text_dbs_shipping_shipping_cost'] = 'Стоимость';
$_['text_dbs_shipping_product_list']  = 'Список товаров';
$_['text_dbs_shipping_type']          = 'Способ доставки заказа';
$_['text_dbs_shipping_type_DELIVERY'] = 'Курьерская доставка';
$_['text_dbs_shipping_type_PICKUP']   = 'Самовывоз';
$_['text_dbs_shipping_type_POST']     = 'Почта';
$_['text_dbs_shipping_paymentMethod'] = 'Способы оплаты, доступные для указанного вида доставки';
$_['text_dbs_shipping_paymentMethod_YANDEX']           = 'банковской картой при оформлении';
$_['text_dbs_shipping_paymentMethod_APPLE_PAY']        = 'Apple Pay при оформлении';
$_['text_dbs_shipping_paymentMethod_GOOGLE_PAY']       = 'Google Pay при оформлении';
$_['text_dbs_shipping_paymentMethod_CARD_ON_DELIVERY'] = 'банковской картой при получении';
$_['text_dbs_shipping_paymentMethod_CASH_ON_DELIVERY'] = 'наличными при получении';
$_['text_dbs_shipping_paymentMethod_CASH_ON_DELIVERY'] = 'наличными при получении';
$_['text_add_shipping']				  = 'Добавить доставку';
$_['text_shippings']				  = 'Доставки';
$_['text_shipping']				  	  = 'Доставка';
$_['text_confirm']				  	  = 'Подтвердить';
$_['text_reject']				  	  = 'Отклонить';
$_['text_reason_rejecting']			  = 'Причина отклонения отмены';
$_['text_reason_cannot_canceled']	  = 'Причина по которой заказ не может быть отменен';
$_['text_ORDER_IN_DELIVERY']		  = 'Заказ уже передан в службу доставки';
$_['text_ORDER_DELIVERED']			  = 'Заказ уже доставлен';
$_['text_subsidy_status']			  = 'Включать субсидии в сумму';
$_['text_lift_support']		     	  = 'Принимать заказы с подъемом на этаж';
$_['text_check_5']			  		  = 'Прохождение самопроверки';
$_['text_main_service_name']		  = 'Основная транспортная компания';
$_['text_main_service_name_placeholder'] = 'Название ТК';
$_['text_orders_cancellation']		  = 'Грузовые места ';
$_['text_trakking']		  			  = 'Трекинг';
$_['text_trak_number']		  		  = 'Трек-номер';
$_['text_send_trak_number']		  	  = 'Отправить трек-номер';
$_['text_json_trak_number_success']	  = 'Трек-номер успешно обновлен';
$_['text_partner_reward']	  		  = 'Вознаграждение партнеру от Маркета';
$_['text_transfer_act_link']		  = 'Акт приема-передачи сегодняшних заказов';
$_['text_shipping_date']		  	  = 'Дата отгрузки';
$_['text_reason']		  	  		  = 'Причина';
$_['text_reason_select']		  	  = 'Выбирите причину';
$_['text_change_time_shipping']		  = 'Изменение сроков доставки';
$_['text_update_notification']		  = 'Доступна новая версия модуля <a href="https://download.cdn.yandex.net/market/opencart/yandexmarket.ocmod.zip" target="_blank">Яндекс.Маркет %s</a> !';
$_['text_dbs_liftType']               = 'Cпособ подъема на этаж';
$_['text_dbs_liftPrice']              = 'Cтоимость подъема на этаж';
$_['text_offer_prices_type']          = 'Привязывать цены по:';
$_['text_shop_sku']          		  = 'Индификатор магазина(shop SKU)';
$_['text_MSKU']          		  	  = 'Индификатор яндекса(MSKU)';
$_['text_real_delivery_date']       = 'Фактическая дата доставки';



$_['lift_type_DBS_NOT_NEEDED']        = 'Не требуется';
$_['lift_type_DBS_MANUAL']            = 'Ручной';
$_['lift_type_DBS_ELEVATOR']          = 'Лифт';
$_['lift_type_DBS_CARGO_ELEVATOR']    = 'Грузовой лифт';
$_['lift_type_DBS_FREE']              = 'Бесплатно';
// Entry
$_['entry_sort_order']			= 'Порядок сортировки:';
$_['entry_product_name']		= 'Название товара';
$_['entry_image']				= 'Изображение';
$_['entry_sku_short']			= 'Ваш SKU';
$_['entry_profile_name']		= 'Название профиля';
$_['entry_group_name']     		= 'Название группы';
$_['entry_name']           		= 'Название товара';
$_['entry_name_offer']     		= 'Название на маркетплейсе';
$_['entry_model']          		= 'Модель';
$_['entry_category']       		= 'Категории';
$_['entry_category_beru']  		= 'Категория на маркетплейсе';
$_['entry_option']         		= 'Опции';
$_['entry_price']          		= 'Цена для продажи на маркетплейсе';
$_['entry_quantity']       	 	= 'Количество';
$_['entry_status']        		= 'Статус';
$_['entry_from']          		= 'От';
$_['entry_to']          		= 'До';
$_['entry_shopSku']        		= 'Ваш SKU';
$_['entry_marketSkuName']  		= 'SKU на Яндексе';
$_['entry_marketCategoryName']	= 'Категория на маркетплейсе';
$_['entry_minPriceOnBeru']		= 'Минимальная цена на Маркете';
$_['entry_byboxPriceOnBeru']    = 'Минимальная цена на маркетплейсе';
$_['entry_defaultPriceOnBeru']  = 'Рекомендованная цена';
$_['entry_maxPriceOnBeru']  	= 'Максимальная старая цена';
$_['entry_outlierPrice']    	= 'Скрытие выше этой цены';
$_['entry_image']           	= 'Изображение';
$_['entry_listProducts']   		= 'Список товаров';
$_['entry_noPrice']         	= 'Товары без цены';

//buttons
$_['button_edit']				= "Редактировать";
$_['button_profile_add']		= "Добавить профиль";
$_['button_link']				= "Получить рекомендации"; 
$_['button_get_recommendations']= "Привязать товары"; 
$_['button_link_cards']			= "Привязать карточки";
$_['button_push_offers']		= "Сохранить связи";
$_['button_save'] 				= 'Сохранить';
$_['button_filter']         	= 'Фильтр';
$_['button_setPrice']       	= 'Установить цены';
$_['button_cancel']         	= 'Отмена';
$_['button_updateRecomendPrice']= 'Обновить цены для продвижения';
$_['button_log']                = 'История изменений';
$_['button_field_print_order']  = 'Распечатать ярлыки для заказа';
$_['button_label_link']			= "Распечатать ярлык";
$_['button_add_cargo_package']	= "Добавить грузовое место";
$_['button_apply']				= "Применить";
$_['button_change_time']		= "Изменить время доставки";
$_['button_updateOfferPrice']	= "Получить установленные цены из ЛК";
$_['button_real_delivery_date']	= "Изменить дату";


//Tabs
$_['tab_fieldsets']						= 'Сопоставление полей';
$_['tab_status']						= 'Статусы заказов';
$_['tab_common_settings']				= 'Общие настройки';
$_['tab_filter_products_and_delivery']	= 'Выбор товаров и доставка';
$_['tab_product_groups']   				= 'Группы товаров';
$_['tab_suggessions']   				= 'Отправка на проверку';
$_['tab_settings']   					= 'Настройки прайс-листа';
$_['tab_field_mapping']   				= 'Сопоставление полей';
$_['tab_filter']   						= 'Фильтр товаров';
$_['tab_DBS']   						= 'Модель DBS';
$_['tab_FBS']   						= 'Модель FBS';
$_['tab_other_settings']   				= 'Остальные настройки';


//Columns
$_['column_name']				= 'Название';
$_['column_action']				= 'Действие';
$_['column_model']				= 'Модель';
$_['column_image']				= 'Изображение';
$_['column_price']				= 'Цена на сайте';
$_['column_quantity']			= 'Количество';
$_['column_status']				= 'Статус';
$_['column_shopSku']			= 'Ваш SKU';
$_['column_marketSku']			= 'SKU на Яндексе';
$_['column_marketSkuName']		= 'Название на маркетплейсе';
$_['column_marketCategoryName']	= 'Категория на маркетплейсе';
$_['column_status_beru']		= 'Статус маркетплейса';
$_['column_status_opencart']	= 'Статус OpenCart';
$_['column_old_name']           = 'Название на момент изменения';
$_['column_user']               = 'Пользователь';
$_['column_established']        = 'Установленная цена';
$_['column_date_updated']       = 'Дата изменения';
$_['column_options']			= 'Опции';
$_['column_depth']				= 'Глубина, см';
$_['column_width']				= 'Ширина, см';
$_['column_height']				= 'Высота, см';
$_['column_weight']				= 'Вес, г';
$_['column_cancellation_time']  = 'До автоматической отмены';

//Select
$_['select_category_placeholder']			= 'Выберите категории';
$_['select_option_placeholder']				= 'Выберите опции';
$_['select_no_results']						= 'Ничего не найдено';
$_['select_option_value']					= 'Выберите значение опции';
$_['select_on']								= 'Включено';
$_['select_off']							= 'Отключено';
$_['select_USER_MOVED_DELIVERY_DATES']		= 'покупатель сам изменил дату';
$_['select_PARTNER_MOVED_DELIVERY_DATES']	= 'магазин не может доставит заказ в срок';

//Statuses
$_['status_READY']			= '<p class="success"><b><i class="fa fa-check" aria-hidden="true"></i> Можно продавать </b></p>';
$_['status_IN_WORK']		= '<p class="warning"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> На проверке </b></p>';
$_['status_NEED_CONTENT']	= '<p class="error"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Нужна информация  </b> Мы не нашли карточку товара по предоставленным данным. Найдите её самостоятельно или создайте через личный кабинет.';
$_['status_NEED_INFO']		= '<p class="error"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Нужна информация </b> При проверке товара были найдены ошибки, или вы предоставили не всю нужную информацию о товаре.</p>';
$_['status_SUSPENDED']		= '<p class="error"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Товар не прошел модерацию </b> Маркетплейс пока не размещает подобные товары.</p>';
$_['status_REJECTED']		= '<p class="error"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Товар не прошел модерацию </b> Маркетплейс не планирует размещать подобные товары.</p>';
$_['status_OTHER']			= '<p class="error"><b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Товар не прошел модерацию </b> Обратитесь в службу поддержки или к вашему менеджеру.</p>';

$_['shipment_status_WAITING_DEPARTURE']      = 'В ожидании отправления';
$_['shipment_status_OUTBOUND_PLANNED']       = 'Отгрузка запланирована';
$_['shipment_status_OUTBOUND_CREATED']       = 'Отгрузка создана';
$_['shipment_status_OUTBOUND_CONFIRMED']     = 'Отгрузка подтверждена';
$_['shipment_status_MOVEMENT_COURIER_FOUND'] = 'Найден курьер или служба доставки';
$_['shipment_status_MOVEMENT_HANDED_OVER']   = 'Заказы отгружены';
$_['shipment_status_MOVEMENT_DELIVERING']    = 'Идет доставка отгрузки';
$_['shipment_status_MOVEMENT_DELIVERED']     = 'Отгрузка доставлена в сортировочный центр';
$_['shipment_status_INBOUND_ARRIVED']        = 'Отгрузка доставлена в пункт приема заказов';
$_['shipment_status_INBOUND_ACCEPTANCE']     = 'Идет приемка отгрузки в пункте приема заказов';
$_['shipment_status_INBOUND_ACCEPTED']       = 'Отгрузка принята в пункте приема заказов';
$_['shipment_status_INBOUND_SHIPPED']        = 'Заказы отгружены из пункта приема в сортировочный центр';

$_['shipment_type_IMPORT'] = 'Доставка в СЦ';
$_['shipment_type_WITHDRAW'] = 'Курьер';

//Statuses text
$_['text_READY']		= 'Можно продавать';
$_['text_IN_WORK']		= 'На проверке';
$_['text_NEED_CONTENT']	= 'Нужна информация. Укажите SKU на Яндексе или создайте карточку в личном кабинете';
$_['text_NEED_INFO']	= 'Нужна информация. Недостаточно сведений в описании товара';
$_['text_SUSPENDED']	= 'Модерация не пройдена. Маркетплейс пока не размещает товары данной категории';
$_['text_REJECTED']		= 'Модерация не пройдена. Маркетплейс не планирует размещать товары данной категории';
$_['text_OTHER']		= 'Модерация не пройдена. Обратитесь в службу поддержки или к вашему менеджеру';

//Offer fields
$_['text_field_name_manufacturerCountries']	= 'Страна производства';
$_['text_field_info_manufacturerCountries'] = 'Страна производства товара.';

$_['text_field_name_manufacturer'] 			= 'Изготовитель';
$_['text_field_info_manufacturer'] 			= 'Изготовитель товара: компания, которая произвела товар, ее адрес и регистрационный номер (если есть).';

$_['text_field_name_vendor'] 				= 'Торговая марка';
$_['text_field_info_vendor'] 				= 'Бренд товара';

$_['text_field_name_weightDimensions'] 		= 'Габариты упаковки и вес товара';
$_['text_field_info_weightDimensions'] 		= 'Если параметр указан, все вложенные в него параметры обязательны.';

$_['text_field_name_weightDimensions'] 		= 'Габариты упаковки и вес товара.';
$_['text_field_info_weightDimensions'] 		= 'Если параметр указан, все вложенные в него параметры обязательны.';
$_['text_field_name_weightDimensions_length'] 		= 'Длина упаковки в см.';
$_['text_field_info_weightDimensions_length'] 		= 'Можно указывать с точностью до тысячных, разделитель целой и дробной части — точка. Пример: 65.55.';
$_['text_field_name_weightDimensions_width'] 		= 'Ширина упаковки в см.';
$_['text_field_info_weightDimensions_width'] 		= 'Можно указывать с точностью до тысячных, разделитель целой и дробной части — точка. Пример: 50.7.';
$_['text_field_name_weightDimensions_height'] 		= 'Высота упаковки в см.';
$_['text_field_info_weightDimensions_height'] 		= 'Можно указывать с точностью до тысячных, разделитель целой и дробной части — точка. Пример: 20.0.';
$_['text_field_name_weightDimensions_weight'] 		= 'Вес товара в кг с учетом упаковки (брутто).';
$_['text_field_info_weightDimensions_weight'] 		= 'Можно указывать с точностью до тысячных, разделитель целой и дробной части — точка. Пример: 1.001.';

$_['text_field_name_vendorCode'] 			= 'Артикул товара от производителя';
$_['text_field_info_vendorCode'] 			= '';

$_['text_field_name_barcodes'] 				= 'Штрихкоды товара';
$_['text_field_info_barcodes'] 				= 'Формат штрихкода: EAN-13, EAN-8, UPC-A или UPC-E. Если товар продается упаковками, укажите штрихкод упаковки. Он не должен совпадать со штрихкодом единицы товара.';

$_['text_field_name_description'] 			= 'Описание товара';
$_['text_field_info_description'] 			= 'Максимальная длина — 3000 символов.В описании запрещено:
				давать инструкции по применению, установке или сборке;
				использовать слова «скидка», «распродажа», «дешевый», «подарок» (кроме подарочных категорий), «бесплатно», «акция», «специальная цена», «новинка», «new», «аналог», «заказ», «хит»;
				указывать номера телефонов, адреса электронной почты, почтовые адреса, номера ICQ, логины мессенджеров, любые URL-ссылки.';

$_['text_field_name_shelfLife'] 			= 'Информация о сроке годности';
$_['text_field_info_shelfLife'] 			= 'Через какое время (в годах, месяцах, днях, неделях или часах) товар станет непригоден для использования. Например, срок годности есть у таких категорий, как продукты питания и медицинские препараты.
				Обязательный параметр, если у товара есть срок годности';
$_['text_field_name_shelfLife_timePeriod'] 		= 'Срок годности';
$_['text_field_info_shelfLife_timePeriod']		= 'Срок годности в единицах, указанных в параметре time-unit / timeUnit.';
$_['text_field_name_shelfLife_timeUnit'] 		= 'Единица измерения срока годности';
$_['text_field_info_shelfLife_timeUnit']		= 'Единица измерения срока годности: HOUR — часы, DAY — дни, WEEK — недели, MONTH — месяцы,YEAR — годы.';
$_['text_field_name_shelfLife_comment'] 		= 'Дополнительные условия использования.';
$_['text_field_info_shelfLife_comment']			= 'Дополнительные условия использования в течение срока годности. Например: Хранить в сухом помещении.';


$_['text_field_name_lifeTime'] 				= 'Информация о сроке службы';
$_['text_field_info_lifeTime']				= 'В течение какого периода (в годах, месяцах, днях, неделях или часах) товар будет исправно выполнять свою функцию, а изготовитель — нести ответственность за его существенные недостатки.';
$_['text_field_name_lifeTime_timePeriod'] 		= 'Срок службы.';
$_['text_field_info_lifeTime_timePeriod']		= 'Срок службы в единицах, указанных в параметре time-unit / timeUnit.';
$_['text_field_name_lifeTime_timeUnit'] 		= 'Единица измерения срока службы';
$_['text_field_info_lifeTime_timeUnit']			= 'Единица измерения срока службы: HOUR — часы, DAY — дни, WEEK — недели, MONTH — месяцы,YEAR — годы.';
$_['text_field_name_lifeTime_comment'] 			= 'Дополнительные условия использования.';
$_['text_field_info_lifeTime_comment']			= 'Дополнительные условия использования в течение срока службы. Например: Использовать при температуре не ниже -10 градусов.';

$_['text_field_name_guaranteePeriod'] 		= 'Информация о гарантийном сроке';
$_['text_field_info_guaranteePeriod'] 		= 'В течение какого периода (в годах, месяцах, днях, неделях или часах) возможны обслуживание и ремонт товара или возврат денег, а изготовитель или продавец будет нести ответственность за недостатки товара.';
$_['text_field_name_guaranteePeriod_timePeriod'] = 'Гарантийный срок службы.';
$_['text_field_info_guaranteePeriod_timePeriod'] = 'Срок службы в единицах, указанных в параметре time-unit / timeUnit.';
$_['text_field_name_guaranteePeriod_timeUnit']	 = 'Единица измерения гарантийного срока';
$_['text_field_info_guaranteePeriod_timeUnit']	 = 'Единица измерения гарантийного срока: HOUR — часы, DAY — дни, WEEK — недели, MONTH — месяцы,YEAR — годы.';
$_['text_field_name_guaranteePeriod_comment'] 	 = 'Дополнительные условия гарантии.';
$_['text_field_info_guaranteePeriod_comment'] 	 = 'Дополнительные условия гарантии. Например: Гарантия на аккумулятор — 6 месяцев.';

$_['text_field_name_customsCommodityCodes'] = 'Код ТН ВЭД';
$_['text_field_info_customsCommodityCodes'] = '';

$_['text_field_name_certificate'] 			= 'Номер документа на товар';
$_['text_field_info_certificate'] 			= 'Перед указанием номера документ нужно загрузить в личном кабинете.';

$_['text_field_name_transportUnitSize'] 	= 'Количество единиц товара в одной упаковке, которую вы поставляете на склад';
$_['text_field_info_transportUnitSize'] 	= 'Например, если вы поставляете детское питание коробками по 6 баночек, укажите значение 6.';

$_['text_field_name_minShipment'] 			= 'Минимальное количество единиц товара, которое вы поставляете на склад';
$_['text_field_info_minShipment'] 			= 'Например, если вы поставляете детское питание партиями минимум по 10 коробок, а в каждой коробке по 6 баночек, укажите значение 60.';

$_['text_field_name_quantumOfSupply'] 		= 'Добавочная партия';
$_['text_field_info_quantumOfSupply'] 		= 'По сколько единиц товара можно добавлять к минимальному количеству min-shipment.
Например, если вы поставляете детское питание партиями минимум по 10 коробок и хотите добавлять к минимальной партии по 2 коробки, а в каждой коробке по 6 баночек, укажите значение 12.';

$_['text_field_name_supplyScheduleDays'] 	= 'Дни недели, в которые вы поставляете товары на склад';
$_['text_field_info_supplyScheduleDays'] 	= '';

$_['text_field_name_deliveryDurationDays']	= 'Срок, за который вы поставляете товары на склад, в днях';
$_['text_field_info_deliveryDurationDays']	= '';

$_['text_field_name_boxCount'] 				= 'Сколько мест (если больше одного) занимает товар';
$_['text_field_info_boxCount'] 				= 'Параметр указывается, только если товар занимает больше одного места (например, кондиционер занимает два места: внешний и внутренний блоки в двух коробках). Если товар занимает одно место, не указывайте этот параметр.';

//Sources
$_['text_source_general']	= 'Основное';
$_['text_source_data']		= 'Данные';
$_['text_source_links']		= 'Связи';
$_['text_source_attribute']	= 'Атрибуты';
$_['text_source_option']	= 'Опции';


//Order_status
$_['order_status_CANCELLED']					= 'Отменен';
$_['order_status_CANCELLED-SHOP_FAILED']		= 'Магазин не может выполнить заказ';
$_['order_status_DELIVERED']					= 'Получен покупателем';
$_['order_status_DELIVERY']						= 'Передан в службу доставки';
$_['order_status_PICKUP']						= 'Доставлен в пункт самовывоза';
$_['order_status_UNPAID']						= 'Оформлен, но еще не оплачен <div>(если выбрана оплата при оформлении)</div>';
$_['order_status_PROCESSING']					= 'В обработке';
$_['order_status_PROCESSING-READY_TO_SHIP']		= 'Собран и готов к отправке';
$_['order_status_PROCESSING-SHIPPED']			= 'Отгружен курьерской службе';
$_['order_status_RESERVED']						= 'Заказ в резерве <div>(ожидается подтверждение от пользователя)</div>';
$_['order_status_CANCELLED-USER_UNREACHABLE'] 	= "Не удается дозвониться до покупателя";

$_['order_status_USER_CHANGED_MIND']		= 'Покупатель отменил заказ по личным причинам.';
$_['order_status_USER_NOT_PAID']		    = 'Покупатель не оплатил заказ (для типа оплаты PREPAID) в течение 30 минут.';
$_['order_status_USER_REFUSED_DELIVERY']	= 'Покупателя не устроили условия доставки.';
$_['order_status_USER_REFUSED_PRODUCT']		= 'Покупателю не подошел товар.';
$_['order_status_USER_REFUSED_QUALITY']		= 'Покупателя не устроило качество товара.';
$_['order_status_USER_UNREACHABLE']		    = 'Невозможно связаться с покупателем';
$_['order_status_CANCELLED-PICKUP_EXPIRED']	= 'Истек срок хранения в ПВЗ';


$_['order_status_CANCELLED-RESERVATION_EXPIRED'] = 'Отменен <div>Покупатель не завершил оформление зарезервированного заказа в течение 10 минут.</div>';
$_['order_status_CANCELLED-USER_NOT_PAID']	     = 'Отменен <div>Покупатель не оплатил заказ (для типа оплаты PREPAID) в течение 30 минут.</div>';
$_['order_status_CANCELLED-PROCESSING_EXPIRED']  = 'Отменен <div>Магазин не обработал заказ в течение семи дней.</div>';
$_['order_status_CANCELLED-USER_CHANGED_MIND']   = 'Покупатель передумал';
$_['order_status_CANCELLED-REPLACING_ORDER']     = 'Отменен <div>Покупатель решил изменить состав заказа.</div>';

$_['order_change_reason_PARTNER_REQUESTED_REMOVE'] = 'Магазин решил изменить состав, если не получается доставить все заказанные товары';
$_['order_change_reason_USER_REQUESTED_REMOVE']    = 'Покупатель решил уменьшить количество товара или удалить товары из заказа';
	
// Error
$_['error_key']						= 'Токен OAuth и/или идентификатор кампании не верны. Проверьте введенные данные';
$_['error_image']					= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не выбрано изображение</b>';
$_['error_shopSku']					= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не указан ваш SKU</b>';
$_['error_name']					= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не указано название</b>';
$_['error_category']				= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не указана категория</b>';
$_['error_manufacturer']			= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не указан производитель</b>';
$_['error_vendor']					= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не указана торговая марка</b>';
$_['error_manufacturerCountries']	= '<b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Не указана страна производства</b>';
$_['error_permission']				= 'У вас нет прав для управления данным модулем';
$_['error_element_already_exists']	= 'Данный элемент уже есть на странице';
$_['error_group_name']				= 'Название группы должно содержать от 1 до 255 символов';
$_['error_form']					= 'Произошла ошибка. Проверьте правильность заполнения формы';
$_['error_sku_already_use']			= 'Данный SKU уже используется';
$_['error_sku_empty']				= 'SKU не может быть пустым';
$_['error_partial_dispatch']		= 'Не удалось отправить все выбранные предложения. Заполните обязательные данные';

$_['error_empty_order_status']		= 'Необходимо сопоставить все статусы заказа';
$_['error_empty_fields']			= 'Необходимо сопоставить все поля';

$_['error_empty_length_cm']			= 'Необходимо указать сантиметры';
$_['error_empty_weight_kg']			= 'Необходимо указать килограммы';

$_['error_binding_product']			= 'Выберите товар';
$_['error_binding_shopSku']			= 'Укажите ваш SKU';
$_['error_binding_yandex_sku']	    = 'Укажите SKU на Маркете';
$_['error_binding_option']			= 'Выберите опцию';
$_['error_binding_dublicate_key']	= 'Карточка для такой комплектации уже создана';
$_['error_binding_dublicate_shop']	= 'Такой SKU магазина уже существует';
$_['error_binding_checkMarket_marketSKU']	= 'Нет такого сочетания вашего SKU и SKU Яндекса';
$_['error_binding_checkMarket_shopSKU']	    = 'В личном кабинете нет товаров с таким вашим SKU';

$_['error_PARTIAL_CONTENT']			= 'Запрос выполнен частично.';
$_['error_BAD_REQUEST']			    = 'Запрос невалидный.';
$_['error_UNAUTHORIZED']			= 'В запросе не указаны авторизационные данные.';
$_['error_FORBIDDEN']			    = 'Неверны авторизационные данные, указанные в запросе, или запрещен доступ к запрашиваемому ресурсу.';
$_['error_NOT_FOUND']			    = 'Запрашиваемый ресурс не найден.';
$_['error_METHOD_NOT_ALLOWED']		= 'Запрашиваемый метод для указанного ресурса не поддерживается.';
$_['error_UNSUPPORTED_MEDIA_TYPE']	= 'Запрашиваемый тип контента не поддерживается методом.';
$_['error_ENHANCE_YOUR_CALM']		= 'Превышено ограничение на доступ к ресурсу.';
$_['error_INTERNAL_SERVER_ERROR']	= 'Внутренняя ошибка сервера. Попробуйте вызвать метод через некоторое время. При повторении ошибки обратитесь в службу технической поддержки Яндекс.Маркета.';
$_['error_SERVICE_UNAVAILABLE']		= 'Сервер временно недоступен из-за высокой загрузки. Попробуйте вызвать метод через некоторое время.';
$_['error_UNKNOWN_ERROR']			= 'Неизвестная ошибка';
$_['error_order_status']			= 'Нельзя добавить трек-номер при текущем статусе заказа';
//Ошибки изменения количества товаров в заказе
$_['error_item_BAD_REQUEST']                     = 'Некорректная передача данных. Возможно, количество переданных кодов идентификации не совпадает со значением параметра count.';
$_['error_item_CANCELLATION_REQUESTED']          = 'Покупатель отправил запрос на отмену заказа, но вы пока что не подтвердили и не отклонили запрос.';
$_['error_item_DELETED_ITEMS_EXCEEDS_THRESHOLD'] = 'Вы удалили товары на сумму больше 99% от стоимости заказа.';
$_['error_item_INVALID_CIS_CODE']                = 'Вы передали код идентификации, который не соответствует принятому формату.';
$_['error_item_ITEM_DUPLICATE']                  = 'В одном запросе вы передали один и тот же товар несколько раз.';
$_['error_item_ITEM_NOT_FOUND']                  = 'Товар с указанным идентификатором не найден.';
$_['error_item_ITEMS_ADDITION_NOT_SUPPORTED']    = 'Вы увеличили количество товара. Его можно только уменьшить.';
$_['error_item_NOT_FOUND']                       = 'Заказ с указанным идентификатором не найден.';
$_['error_item_OTHER_REMOVE_ITEM_ERROR']         = 'Другие проблемы с обработкой запроса.';
$_['error_item_PAYMENT_PROHIBITS_DELETE']        = 'Заказ был оплачен способом, при котором нельзя изменить состав заказа';
$_['error_item_PROMO_PROHIBITS_DELETE']          = 'Товар участвует в акции, из‑за которой его количество нельзя изменить.';
$_['error_item_STATUS_NOT_ALLOWED']              = 'Заказ не принимает статус PROCESSING, DELIVERY или PICKUP, поэтому состав заказа изменить нельзя.';
$_['error_item_STATUS_NOT_ALLOWED_FBS']          = 'Нельзя изменить состав заказа в указанном статусе. Состав можно менять, если заказ находится в статусе PROCESSING.';
$_['error_item_CANNOT_REMOVE_ITEM']              = 'Нельзя удалить этот товар из заказа.';
$_['error_item_CANNOT_REMOVE_LAST_ITEM']         = 'Нельзя удалить последний товар в заказе.';
$_['error_item_INVALID_ITEM']                    = 'В теле запроса в параметре count передано некорректное значение (например, отрицательное).';
$_['error_item_TOO_FEW_CISES_FOR_ITEM_CODE']     = 'Вы передали меньше кодов идентификации, чем единиц товара в заказе, подлежащих маркировке.';
$_['error_item_TOO_MANY_CISES_FOR_ITEM']         = 'Вы передали больше кодов идентификации, чем единиц товара в заказе, подлежащих маркировке.';
$_['error_CANCELLTION_NOT_FOUND']                = 'Запрос на отмену не найден.';

//Ошибки подтверждения отгрузки
$_['error_shipment_confirm_NOT_FOUND']             = 'Отгрузка еще не создана.';
$_['error_shipment_confirm_ALREADY_CONFIRMED']     = 'Отгрузка уже подтверждена.';
$_['error_shipment_confirm_CUTOFF_NOT_REACHED']    = 'Крайнее время поступления заказов в отгрузке еще не наступило.';
$_['error_shipment_confirm_NO_ORDERS']             = 'В отгрузке нет заказов.';
$_['error_shipment_confirm_INTERNAL_ERROR']        = 'Внутренняя ошибка при подтверждении отгрузки.';
$_['success_shipment_confirm']                     = 'Отгрузка успешно подтверждена';
//column_left

//Оповещение о необходиомости обновить государственные праздники
$_['error_warning_holidays']        = 'Необходимо обновить список государственных праздников';
	
$_['column_left_settings']			= 'Настройки';
$_['column_product_group']			= 'Загрузка каталога';
$_['column_offer_status']			= 'Статусы товаров';
$_['column_offer_binding']			= 'Экспорт из ЛК';
$_['column_left_catalog']			= 'Каталог';
$_['column_left_offer_prices']		= 'Назначение цен';
$_['column_left_orders']			= 'Заказы';
$_['column_left_shipments_fbs']  	= 'Отгрузки';
$_['column_left_price_lists']	    = 'Прайс-листы';
$_['column_left_ym']				= 'Яндекс.Маркет';
$_['column_support']				= 'Справка';
$_['column_cancel_reason']			= 'Причина отмены';
$_['column_new_xml']				= 'Создать прайс-лист';
$_['column_yandex_market']			= 'Яндекс.Маркет';
$_['column_orders_cancellation']	= 'Отмены заказов ';

//yandex market

$_['text_field_short_name_shop']				= "Короткое название магазина";
$_['text_field_full_name_company']				= "Полное наименование компании";
$_['text_field_currency']						= "Валюта";
$_['text_field_name_fid']						= "Название фида";
$_['text_field_name_file']						= "Название выгрузки";
$_['text_field_oldprice']						= "Цена до скидки";
$_['text_field_enable_auto_discounts']			= "Автоматический показ скидок";
$_['text_field_link']							= "Ссылка для личного кабинета Маркета";
$_['text_field_type_work']						= "Формирование и обновление прайс-листа";
$_['text_field_type_work_not_cache']			= "Формирование файла 'на лету'";
$_['text_field_type_work_cache']				= "Формирование файла выгрузки по крону";
$_['text_field_type_offer']						= "Тип предложений";
$_['text_field_type_arbitrary']					= "Произвольный";
$_['text_field_type_simplified']				= "Упрощенный";
$_['text_field_type_medicine']					= "Лекарства";
$_['text_field_type_books']						= "Книги";
$_['text_field_type_musicVideo']				= "Музыкальная и видеопродукция";
$_['text_field_type_eventTickets']				= "Билеты на мероприятия";
$_['text_field_type_tours']						= "Туры";
$_['text_field_type_alcohol']					= "Алкоголь";
$_['text_field_type_audiobooks']				= "Аудиокниги";
$_['text_field_upload_all']						= "Выгружать все";
$_['text_field_upload_selected']				= "Выгружать выбранные";
$_['text_field_from']							= "От:";
$_['text_field_to']								= "До:";
$_['text_field_availability_status']			= "Наличие";
$_['text_field_all']							= "Все";
$_['text_field_only_presence']					= "Только в наличии";
$_['text_field_only_presence_and_status']		= "В наличии и со статусом:";
$_['text_field_stock_status']					= "Состояние на складе";
$_['text_field_type_offer_zero']				= "Выберите тип";
$_['text_add_new_filter']						= "Добавить новый фильтр";
$_['text_filter_name']							= "Название фильтра";
$_['text_delivery']								= "Курьерская доставка";
$_['text_delivery_options_cost']				= "Стоимость доставки";
$_['text_delivery_options_days']				= "Срок доставки в рабочих днях";
$_['text_delivery_options_order_before']		= "Время, до которого нужно сделать заказ, чтобы получить его в указанный срок";
$_['text_add_delivery']							= "Добавить вариант доставки";
$_['text_pickup']								= "Самовывоз";
$_['text_pickup_options_cost']					= "Стоимость самовывоза";
$_['text_pickup_options_days']					= "Cрок поставки товара в пункт выдачи в рабочих днях";
$_['text_pickup_options_order_before']			= "Время, до которого нужно сделать заказ, чтобы получить товар в пункте выдачи в указанный срок";
$_['text_unit']									= "Единицы измерения";
$_['text_name_param']							= "Название параметра";
$_['text_year']									= "Год";
$_['text_month']								= "Месяц";
$_['text_success_save']							= "Настройки успешно сохранены";
$_['text_error']								= "Ошибки";
$_['text_generation']							= "генерация";



$_['text_field_name_price']						= "Цена";
$_['text_field_name_oldprice'] 					= "Старая цена товара";
$_['text_field_name_purchase_price'] 			= "Закупочная цена товара";
$_['text_field_name_enable_auto_discounts'] 	= "Автоматический расчет и показ скидок";
$_['text_field_name_supplier'] 					= "ОГРН или ОГРНИП стороннего продавца";
$_['text_field_name_delivery'] 					= "Возможность курьерской доставки";
$_['text_field_name_pickup'] 					= "Возможность самовывоза из пунктов выдачи";
$_['text_field_name_sales_notes'] 				= "Условия продажи товара";
$_['text_field_name_manufacturer_warranty'] 	= "Официальная гарантия производителя";
$_['text_field_name_country_of_origin'] 		= "Страна производства товара";
$_['text_field_name_adult'] 					= "Эксплуатирует интерес к сексу";
$_['text_field_name_barcode'] 					= "Штрихкод товара от производителя";
$_['text_field_name_param'] 					= "Все важные характеристики товара";
$_['text_field_name_condition'] 				= "Причны уценки товара";
$_['text_field_name_expiry'] 					= "Срок годности / срок службы";
$_['text_field_name_weight'] 					= "Вес товара в килограммах с учетом упаковки";
$_['text_field_name_dimensions'] 				= "Габариты товара";
$_['text_field_name_downloadable'] 				= "Продукт можно скачать";
$_['text_field_name_available'] 				= "Конкретный срока доставки";
$_['text_field_name_age'] 						= "Возрастная категория товара";
$_['text_field_name_name'] 						= "Наименование товара";
$_['text_field_name_image'] 					= "Изображение";
$_['text_field_name_model'] 					= "Модель и название товара.";
$_['text_field_name_typePrefix'] 				= "Тип / категория товара";
$_['text_field_name_bid'] 						= "Размер ставки";
$_['text_field_name_store'] 					= "Возможность купить товар без предварительного заказа";
$_['text_field_name_publisher'] 				= "Издательство";
$_['text_field_name_author'] 					= "Автор произведения";
$_['text_field_name_year'] 						= "Год издания";
$_['text_field_name_language'] 					= "Язык, на котором издано произведение";
$_['text_field_name_ISBN'] 						= "International Standard Book Number";
$_['text_field_name_series'] 					= "Серия";
$_['text_field_name_volume'] 					= "Общее количество томов";
$_['text_field_name_part'] 						= "Номер тома";
$_['text_field_name_table_of_contents'] 		= "Оглавление";
$_['text_field_name_binding'] 					= "Формат";
$_['text_field_name_page_extent'] 				= "Количество страниц в книге";
$_['text_field_name_credit-template'] 			= "Идентификатор кредитной программы";
$_['text_field_name_place'] 					= "Место проведения";
$_['text_field_name_date'] 						= "Дата и время сеанса";
$_['text_field_name_hall'] 						= "Зал";
$_['text_field_name_hall_part'] 				= "Ряд и место в зале";
$_['text_field_name_is_premiere'] 				= "Премьера";
$_['text_field_name_is_kids'] 					= "Детский";
$_['text_field_name_min-quantity'] 				= "Минимальное количество";
$_['text_field_name_days'] 						= "Количество дней тура";
$_['text_field_name_included'] 					= "Что включено в стоимость тура";
$_['text_field_name_transport'] 				= "Транспорт";
$_['text_field_name_worldRegion'] 				= "Часть света";
$_['text_field_name_country'] 					= "Страна";
$_['text_field_name_region'] 					= "Курорт или город";
$_['text_field_name_dataTour'] 					= "Даты заездов";
$_['text_field_name_hotel_stars'] 				= "Звезды отеля";
$_['text_field_name_room'] 						= "Тип комнаты";
$_['text_field_name_meal'] 						= "Тип питания";
$_['text_field_name_price_min'] 				= "Минимальная цена тура";
$_['text_field_name_price_max'] 				= "Максимальная цена тура";
$_['text_field_name_options'] 					= "Опции";
$_['text_field_name_performed_by'] 				= "Исполнитель";
$_['text_field_name_performance_type'] 			= "Тип аудиокниги";
$_['text_field_name_storage'] 					= "Носитель аудиокниги";
$_['text_field_name_format'] 					= "Формат аудиокниги";
$_['text_field_name_recording_length'] 			= "Время звучания";
$_['text_field_name_title'] 					= "Название";
$_['text_field_name_artist'] 					= "Исполнитель";
$_['text_field_name_director'] 					= "Режиссер";
$_['text_field_image'] 							= "Выгрузка изображений";
$_['text_field_main'] 							= "Основное";
$_['text_field_not_unload'] 					= "Не выгружать";
$_['text_field_log'] 							= "Логирование";
$_['text_field_link_cron'] 						= "Ссылка для Cron";
$_['text_field_name_type'] 						= "Тип";
$_['text_field_name_cpa'] 						= "Возможность заказать товар на Маркете";


$_['text_field_info_short_name_shop'] 			= "В названии нельзя использовать слова, которые не относятся к наименованию магазина (например «лучший», «дешевый»), указывать номер телефона и т. п.
Название магазина должно совпадать с фактическим названием, которое публикуется на сайте. Если требование не соблюдается, Яндекс.Маркет может самостоятельно изменить название без уведомления магазина.";
$_['text_field_info_full_name_company'] 	 	= "Полное наименование компании, владеющей магазином. Не публикуется.";
$_['text_field_info_oldprice'] 					= "Старая цена товара, должна быть выше текущей. Маркет автоматически рассчитывает разницу и показывает пользователям скидку.
Если у товара есть скидка, показывает основную цену в теге 'oldprice'";
$_['text_field_info_purchase_price'] 			= "Закупочная цена товара. Она нужна для расчета наценки и настройки стратегии по маржинальности в PriceLabs.";
$_['text_field_info_enable_auto_discounts'] 	= "Автоматический расчет и показ скидок для предложения";
$_['text_field_info_link'] 						= "Укажите эту ссылку в личном кабинете на странице Товары → Автоматическое обновление каталога";
$_['text_field_info_typePrefix'] 				= "Например, «мобильный телефон», «стиральная машина», «угловой диван»";
$_['text_field_info_sales_notes'] 				= "Элемент обязателен, если у вас есть ограничения при покупке (например, минимальное количество товаров или необходимость предоплаты).
Также можно указать варианты оплаты, акции и распродажи. В этом случае использование элемента необязательно.
Допустимая длина текста — 50 символов.";
$_['text_field_info_country_of_origin'] 		= "Список стран, которые могут быть указаны в этом элементе: http://partner.market.yandex.ru/pages/help/Countries.pdf";
$_['text_field_info_supplier'] 					= "ОГРН должен содержать 13 символов, ОГРНИП — 15";
$_['text_field_info_delivery'] 					= "По всем регионам, в которые доставляет магазин.
Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_pickup'] 					= "По всем регионам, в которые доставляет магазин.
Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_store'] 					= "Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_manufacturer_warranty'] 	= "Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_adult'] 					= "Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_expiry'] 					= "Срок годности / срок службы либо дата истечения срока годности / срока службы.
Значение элемента должно быть в формате ISO8601:
    Для срока годности / срока службы — P1Y2M10DT2H30M. Расшифровка примера — 1 год, 2 месяца, 10 дней, 2 часа и 30 минут.
    Для даты истечения срока годности / срока службы — YYYY-MM-DDThh:mm.";
$_['text_field_info_weight'] 					= "Вес товара в килограммах с учетом упаковки
В любой категории вес можно указывать с точностью до тысячных (например, 1.001 кг; разделитель целой и дробной части — точка).
Если минимальное значение указано 0, ограничений по минимальному весу нет, и можно указывать начиная с одного грамма (0.001 кг)";
$_['text_field_info_dimensions'] 				= "Габариты товара (длина, ширина, высота) в упаковке. Размеры укажите в сантиметрах.
Формат: три положительных числа с точностью 0.001, разделитель целой и дробной части — точка. Числа должны быть разделены символом «/» без пробелов.";
$_['text_field_info_downloadable'] 				= "Продукт можно скачать. Если указано true, предложение показывается во всех регионах, при этом способы оплаты для него не отображаются. Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_available'] 				= "С помощью элемента available со значением false можно задать для товара вместо конкретного срока доставки надпись «до 60 дней» (или «предзаказ», если в базе данных Маркета есть дата, когда товар официально начнет продаваться).
Если вы хотите использовать available, обязательно изучите подробное описание элемента.";
$_['text_field_info_barcode'] 					= "Штрихкод товара от производителя в одном из форматов: EAN-13, EAN-8, UPC-A, UPC-E.
В YML элемент offer может содержать несколько элементов barcode.
Данные в barcode влияют на привязку предложения к карточке товара и отображение правильных характеристик, которые соответствуют модификации товара на его карточке. Рекомендуем ознакомиться с подробным описанием элемента.";
$_['text_field_info_param'] 					= "Все важные характеристики товара";
$_['text_field_info_name'] 						= "Полное название предложения, в которое входит: тип товара, производитель, модель и название товара, важные характеристики. Составляйте по схеме: что (тип товара) + кто (производитель) + товар (модель, название) + важные характеристики.
Данные в name влияют на привязку к карточке товара. Рекомендуем ознакомиться с подробным описанием элемента.";
$_['text_field_info_bid'] 						= "Указывайте размер ставки в условных центах: например, значение 80 соответствует ставке 0,8 у. е. Значения должны быть целыми и положительными числами.";
$_['text_field_info_ISBN'] 						= "International Standard Book Number — международный уникальный номер книжного издания. Если их несколько, укажите все через запятую.
Форматы ISBN и SBN проверяются на корректность. Валидация кодов происходит не только по длине, также проверяется контрольная цифра (check-digit) — последняя цифра кода должна согласовываться с остальными цифрами по определенной формуле. При разбиении ISBN на части при помощи дефиса (например, 978-5-94878-004-7) код проверяется на соответствие дополнительным требованиям к количеству цифр в каждой из частей.
Используйте номер ISBN, чтобы привязать ваше предложение к карточке товара. Подбронее см. в разделе Как попасть на карточку товара.";
$_['text_field_info_volume'] 					= "Если издание состоит из нескольких томов";
$_['text_field_info_part'] 						= "Если издание состоит из нескольких томов";
$_['text_field_info_performed_by'] 				= "Если их несколько, перечисляются через запятую";
$_['text_field_info_performance_type'] 			= "Радиоспектакль, «произведение начитано» и т. п.";
$_['text_field_info_recording_length'] 			= "Задается в формате mm.ss (минуты.секунды)";
$_['text_field_info_date'] 						= "Предпочтительный формат: YYYY-MM-DD hh:mm:ss";
$_['text_field_info_is_premiere'] 				= "Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_is_kids'] 					= "Должен иметь формат данных Да/Нет, Yes/not, true/false, 1/0";
$_['text_field_info_page_extent'] 				= "Должно быть целым положительным числом";
$_['text_field_info_dataTour'] 					= "Даты заездов. Предпочтительный формат: YYYY-MM-DD hh:mm:ss";
$_['text_field_info_room'] 						= "SNG, DBL и т. п.";
$_['text_field_info_meal'] 						= "All, HB и т. п.";
$_['text_field_info_name_file'] 				= "Только латнинские буквы";
$_['text_field_info_link_cron'] 				= "Ссылка для генерации файла";
$_['text_field_info_age'] 						= "Допустимые значения параметра при годах: 0, 6, 12, 16, 18. Допустимые значения параметра при месяцах: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12.";
$_['text_field_info_condition'] 				= "В атрибуте тип укажите состояние товара:
likenew — как новый (товар не был в употреблении, уценен из‑за недостатков);
used — подержанный (товар был в употреблении).";
$_['text_field_info_cpa'] 				= "
1 — товар можно заказать на Маркете.
0 — товар можно заказать только на сайте магазина.
Значение по умолчанию: 1.
Алкоголь, лекарственные средства и товары, подлежащие маркировке, с кодами идентификации из системы «Честный ЗНАК» не удастся разместить по модели DBS (продажи с доставкой магазина). Поэтому для них элемент всегда принимает значение 0.";


$_['error_short_name_shop'] 					= "Введите короткое название магазина";
$_['error_full_name_company'] 					= "Введите полное наименование компании";
$_['error_name_fid'] 							= "Введите название фида";
$_['error_name_file'] 							= "Введите название выгрузки";
$_['error_name_file_lat'] 						= "Используйте только латинские буквы";
$_['error_type'] 								= "Выберите формат предложения";
$_['error_model'] 								= "Укажите что выгружать в поле 'Модель и название товара'";
$_['error_typePrefix'] 							= "Укажите что выгружать в поле 'Тип / категория товара'";
$_['error_vendor'] 								= "Укажите что выгружать в поле 'Торговая марка'";
$_['error_name_market'] 						= "Укажите что выгружать в поле 'Наименование товара'";
$_['error_vendor_market'] 						= "Укажите что выгружать в поле 'Торговая марка'";
$_['error_barcode'] 							= "Укажите что выгружать в поле 'Штрихкод товара от производителя'";
$_['error_param'] 								= "Укажите что выгружать в поле 'Все важные характеристики товара'";
$_['error_publisher'] 							= "Укажите что выгружать в поле 'Издательство";
$_['error_age'] 								= "Укажите что выгружать в поле 'Возрастная категория товара'";
$_['error_place'] 								= "Укажите что выгружать в поле 'Место проведения'";
$_['error_date'] 								= "Укажите что выгружать в поле 'Дата и время сеанса'";
$_['error_pickup'] 								= "Укажите что выгружать в поле 'Возможность самовывоза из пунктов выдачи'";
$_['error_delivery'] 							= "Укажите что выгружать в поле 'Возможность курьерской доставки'";
$_['error_title'] 								= "Укажите что выгружать в поле 'Название'";
$_['error_days'] 								= "Укажите что выгружать в поле 'Количество дней тура'";
$_['error_included'] 							= "Укажите что выгружать в поле 'Что включено в стоимость тура'";
$_['error_transport'] 							= "Укажите что выгружать в поле 'Транспорт'";
$_['error_main_delivery'] 						= "Заполните Стоимость доставки и/или Срок доставки в рабочих днях";
$_['error_param_name'] 							= "Введите название параметра";
<?php
/**
 * @module   Товары в рассрочку
 * @author   fonclub
 * @created  19.12.2019 11:52
 */

$_ = [
    'heading_title'        => 'Товары в рассрочку',
    'text_module'          => 'Модули',
    'text_success'         => 'Настройки модуля успешно обновлены!',
    'text_success_install' => 'Модуль успешно установлен!',
    'text_edit'            => 'Редактирование модуля',
    'text_add'             => 'Добавление модуля',
    'text_order_asc'       => 'По Возрастанию',
    'text_order_desc'      => 'По Убыванию',
    'text_pdname'          => 'По Названию',
    'text_pprice'          => 'По Цене',
    'text_pmodel'          => 'По Модели',
    'text_pquantity'       => 'По Количеству',
    'text_rating'          => 'По Рейтингу',
    'text_pviewed'         => 'По Просмотрам',
    'text_psort_order'     => 'По Порядку сортировки',
    'text_pdate_added'     => 'По Дате',
    'entry_name'           => 'Системное название модуля',
    'entry_heading'        => 'Заголовок модуля',
    'entry_limit'          => 'Лимит товаров в модуле',
    'entry_width'          => 'Ширина изображения',
    'entry_height'         => 'Высота изображения',
    'entry_status'         => 'Статус',
    'entry_quantity_view'  => 'Отображать товары с 0 количеством',
    'entry_sort'           => 'Сортировка',
    'error_permission'     => 'У вас нет прав для управления этим модулем!',
    'error_name'           => 'Название должно быть от 3 до 64 символа!',
    'error_heading'        => 'Заголовок модуля должен быть от 3 до 64 символов!',
    'error_width'          => 'Необходимо указать Ширину!',
    'error_height'         => 'Необходимо указать Высоту!',
    'error_limit'          => 'Необходимо указать Лимит товаров в модуле!',
];

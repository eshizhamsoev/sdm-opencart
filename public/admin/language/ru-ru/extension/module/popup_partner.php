<?php
// Heading
$_['heading_title'] = 'Форма Стать партнером';

// Button
$_['button_delete_menu']     = 'Удалить';
$_['button_delete_selected'] = 'Удалить выбранное';
$_['button_delete_all']      = 'Удалить все';
$_['button_delete']          = 'Удалить';

// Text
$_['text_module']           = 'Модули';
$_['text_success']          = 'Настройки успешно сохранены!';
$_['text_success_install']  = 'Модуль успешно установлен!';
$_['text_edit']             = 'Редактировать модуль';
// Tabs
$_['tab_setting'] = 'Настройки';

// Help
$_['help_email'] = 'Введите email через запятую, для отправки на несколько адресов.';

// Entry
$_['entry_status']        = 'Статус';
$_['entry_notify_status'] = 'Статус уведомлений';
$_['entry_notify_email']  = 'Почта для уведомлений';

// Column
$_['column_action']     = 'Действие';
$_['column_info']       = 'Пользователь';
$_['column_date_added'] = 'Добавлено';

// Error
$_['error_permission']   = 'У вас нет прав для редактирования модуля!';
$_['error_notify_email'] = 'Поле Почта не должно быть пустым!';
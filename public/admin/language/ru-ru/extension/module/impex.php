<?php
// Heading
$_['heading_title']    	= 'ImpEx';

// Text
$_['text_extension']	= 'Расширения';
$_['text_success']     	= 'Настройки успешно изменены!';
$_['text_edit']        	= 'Настройки модуля';

// Entry
$_['entry_status']     	= 'Статус';
$_['entry_login']     	= 'Логин';
$_['entry_password']	= 'Пароль';
$_['entry_base']		= 'База 1С';
$_['entry_temp_ttl']	= 'Хранить файлы';

// Error
$_['error_permission'] 	= 'У Вас нет прав для управления данным модулем!';


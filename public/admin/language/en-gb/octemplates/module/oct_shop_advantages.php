<?php
// Heading
$_['heading_title']        = 'Octemplates - Shop Advantages';

// Text
$_['text_module']          = 'Modules';
$_['text_success']         = 'Module settings successfully updated!';
$_['text_success_install']  = 'The module settings have been install successfully!';
$_['text_edit']            = 'Editing the module';
$_['text_select_all']      = 'Select all';
$_['text_unselect_all']    = 'Remove selection';
$_['text_block_1']         = 'Block 1';
$_['text_block_2']         = 'Block 2';
$_['text_block_3']         = 'Block 3';
$_['text_block_4']         = 'Block 4';

// Entry
$_['entry_name']           = 'Name';
$_['entry_link_heading']   = 'Link to article';
$_['entry_text']           = 'Block text';
$_['entry_icon']           = 'Block icon';
$_['entry_icon_title']     = 'Select icon';
$_['entry_background_block'] = 'Block background';
$_['entry_heading']        = 'Title';
$_['entry_text_heading']   = 'Title text';
$_['entry_category']       = 'Select a category';
$_['entry_color_heading']  = 'Title color';
$_['entry_color_icon']     = 'Color icon';
$_['entry_width']          = 'Product picture width';
$_['entry_color_text']     = 'Text color';
$_['entry_background_block_hover'] = 'Hover color';
$_['entry_image']          = 'Show category picture';
$_['entry_sub_categories'] = 'Show Subcategories';
$_['entry_status']         = 'Status';

// Error
$_['error_permission']     = 'You do not have permission to manage this module!';
$_['error_name']           = 'Module name must be between 3 and 64 characters!';
$_['error_heading_block1']        = 'Title must be between 1 and 255 characters!';
$_['error_text_block1']           = 'You must enter the block text!';
$_['error_color_text_block1']     = 'You must enter the text color!';
$_['error_background_block_hover_block1'] = 'You must enter hover color!';
$_['error_color_heading_block1']  = 'Title color must be specified!';
$_['error_color_icon_block1']     = 'You must enter the icon color!';
$_['error_icon_block1']           = 'You must select icon!';
$_['error_heading_block2']        = 'Title must be between 1 and 255 characters!';
$_['error_text_block2']           = 'You must enter the block text!';
$_['error_color_text_block2']     = 'You must enter the text color!';
$_['error_background_block_hover_block2'] = 'You must enter color block background on hover!';
$_['error_color_heading_block2']  = 'Title color must be specified!';
$_['error_color_icon_block2']     = 'You must enter the icon color!';
$_['error_icon_block2']           = 'You must select icon!';
$_['error_heading_block3']        = 'Title must be between 1 and 255 characters!';
$_['error_text_block3']           = 'You must enter the block text!';
$_['error_color_text_block3']     = 'You must enter the text color!';
$_['error_background_block_hover_block3'] = 'You must enter color block background on hover!';
$_['error_color_heading_block3']  = 'Title color must be specified!';
$_['error_color_icon_block3']     = 'You must enter the icon color!';
$_['error_icon_block3']           = 'You must select icon!';
$_['error_heading_block4']        = 'Title must be between 1 and 255 characters!';
$_['error_text_block4']           = 'You must enter the block text!';
$_['error_color_text_block4']     = 'You must enter the text color!';
$_['error_background_block_hover_block4'] = 'You must enter color block background on hover!';
$_['error_color_heading_block4']  = 'Title color must be specified!';
$_['error_color_icon_block4']     = 'You must enter the icon color!';
$_['error_icon_block4']           = 'You must select icon!';
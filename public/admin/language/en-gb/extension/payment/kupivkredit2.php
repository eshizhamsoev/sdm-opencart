<?php
// Heading
$_['heading_title']      	= 'Купивкредит var3';

// Text
$_['text_edit']          = 'Редактирование';
$_['text_payment']      	= 'Оплаты';
$_['text_success']       	= 'Настройки модуля обновлены!';
$_['text_kupivkredit'] 		= '<a onclick="window.open(\'http://kupivkredit.ru\');"><img src="/image/payment/kupivkredit.png" alt="Купивкредит" title="Купивкредит" style="border: 1px solid #EEEEEE;" height="25px" /></a>';

$_['entry_total']        = 'Минимальная сумма';
$_['entry_merch_z']      	= 'ID магазина:';
$_['entry_order_status'] 	= 'Статус заказа после оплаты:';
$_['entry_geo_zone']     	= 'Гео. Зона:';
$_['entry_status']       	= 'Состояние:';
$_['entry_sort_order']   	= 'Сортировка:';
$_['entry_server']   	= 'Сервер:';
$_['entry_test']   	= 'тестовый';
$_['entry_work']   	= 'рабочий';
$_['entry_promo_code']   	= 'Промокод';

// Error
$_['error_permission']   	= 'У Вас нет прав для изменения модуля!';
$_['error_merch_z']        	= 'Не указан ID партнера!';
$_['error_promo_code']      = 'Не указан промокод!';

?>
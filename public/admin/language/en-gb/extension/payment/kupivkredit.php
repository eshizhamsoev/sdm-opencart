<?php
// Heading
$_['heading_title'] = 'Оплата в рассрочку';

// Text
$_['text_edit']      = 'Редактирование';
$_['text_extension'] = 'Модули';
$_['text_payment']   = 'Оплаты';
$_['text_success']   = 'Настройки модуля обновлены!';
$_['text_kupivkredit']
                     = '<a onclick="window.open(\'http://kupivkredit.ru\');"><img src="/image/payment/kupivkredit.png" alt="Купивкредит" title="Купивкредит" style="border: 1px solid #EEEEEE;" height="25px" /></a>';

$_['entry_total']        = 'Минимальная сумма';
$_['entry_merch_z']      = 'ID магазина:';
$_['entry_order_status'] = 'Статус заказа после оплаты:';
$_['entry_geo_zone']     = 'Гео. Зона:';
$_['entry_sort_order']   = 'Сортировка:';
$_['entry_server']       = 'Сервер:';
$_['entry_test']         = 'тестовый';
$_['entry_work']         = 'рабочий';
$_['entry_promo_code']   = 'Вариант рассрочки';

// Error
$_['error_permission']       = 'У Вас нет прав для изменения модуля!';
$_['error_merch_z']          = 'Не указан ID партнера!';
$_['error_promo_code']       = 'Не указан вариант рассрочки!';
$_['error_promo_code_empty'] = 'Заполните промокоды и описания всех вариантов рассрочки!';

// Entry
$_['text_enabled_required']  = 'Включить и Сделать обязательным';
$_['text_options']           = 'Разрешить опции:';
$_['text_select_all']        = 'Выбрать все';
$_['text_unselect_all']      = 'Отменить все';
$_['text_allow_stock_check'] = 'Учитывать наличие товара';
$_['entry_status']          = 'Статус';
$_['entry_firstname']       = 'Имя';
$_['entry_email']           = 'Email';
$_['entry_notify_email']    = 'Email по умолчанию';
$_['entry_email_helper']    = 'Если поле Email отключено или имеет пустое значение';
$_['entry_telephone']       = 'Телефон';
$_['entry_address']         = 'Адрес';
$_['entry_comment']         = 'Комментарий';
$_['entry_quantity']        = 'Количество';
$_['entry_description']     = 'Описание товара';
$_['entry_image']           = 'Изображение товара';
$_['entry_description_max'] = 'Кол-во символов в описании:';
$_['entry_width']           = 'Ширина изображения';
$_['entry_height']          = 'Высота изображения';
$_['entry_allow_page']      = 'Выводить модуль на других страницах';
$_['entry_mask']            = 'Маска номера телефона';
$_['entry_mask_info']       = 'Вводите число 9 для обозначения маски числа в номере телефона. Например 8 (000) 000-00-00';
$_['entry_cart_page']       = 'Страница корзины';
$_['entry_popup_cart']      = 'PopUp корзина';
$_['entry_product_view']    = 'Быстрый просмотр';
$_['entry_product_page']    = 'Страница товара';
$_['entry_display_on_pages'] = 'Выводить на страницах';

// Error
$_['error_permission']   = 'Внимание: У вас недостаточно прав для редактирования модуля!';
$_['error_notify_email'] = 'Поле Почта не должно быть пустым!';

?>
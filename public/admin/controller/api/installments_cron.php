<?php

/**
 * Class ControllerApiInstallmentsCron
 * @author fonclub
 * @created 14.04.2020
 */
class ControllerApiInstallmentsCron extends Controller {
   
    public function generate() {
        /** используем ocmod, если есть */
        if(file_exists(DIR_MODIFICATION . '/admin/model/extension/payment/kupivkredit.php'))
            require_once DIR_MODIFICATION . '/admin/model/extension/payment/kupivkredit.php';
        else
            require_once DIR_APPLICATION . '/model/extension/payment/kupivkredit.php';

        $model = new ModelExtensionPaymentKupivkredit($this->registry);

        $this->log->write('Начинаем генерацию товаров в рассрочку');
        
        $model->generateInstallments();
        
        $this->log->write('Закончили генерацию товаров в рассрочку');
        echo 'Закончили генерацию товаров в рассрочку';
    }
}

<?php

class ControllerApiDelayedReviewCron extends Controller
{
    public function addReviewsToProducts()
    {
        $this->load->model('extension/delayed_review');
        $count = $this->model_extension_delayed_review->addReviewsToProducts();
        $this->response->setOutput("Добавлено $count\n");
    }
}

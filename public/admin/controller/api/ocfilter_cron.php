<?php

/**
 * Class ControllerApiOcfilter
 * @author fonclub
 * @created 13.02.2020
 */
class ControllerApiOcfilterCron extends Controller {
    protected $api_token = 'dgf34tg@45dfg45tyww';

    /**
     * метод для копирования фильтров по крону
     */
    public function copy_filter() {
        /** настройки импорта */
        $options = [
            'copy_type' => 'checkbox', // тип скопированных фильтров
            'copy_status' => -1, // статус скопированных атрибутов (-1 - автоматически)
            'copy_attribute' => 1, // копировать атрибуты
            'attribute_separator' => '', // разделитель атрибутов
            'copy_filter' => 0, // копировать стандартные фильтры
            'copy_option' => 1, // копировать опции
            'copy_truncate' => 1, // очистить существующие фильтры OCFilter
            'copy_category' => 0, // привязать фильтры к категориям
        ];

        /** используем ocmod, если есть */
        $this->load->model('extension/ocfilter');


        $this->log->write('Начинаем обновление фильтров');
        $this->model_extension_ocfilter->copyFilters($options);
        $this->cache->delete('*');

        $this->log->write('Закончили обновление фильтров');
    }
}

<?php

class ControllerExtensionModuleOzonSeller extends Controller
{
    private $error = array();

    public function index()
    {
        $this->document->addStyle('view/stylesheet/oct_ultrastore.css');

        $data = $this->load->language('extension/module/ozon_seller');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate())) {
            $this->model_setting_setting->editSetting('ozon_seller', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/module/ozon_seller',
                'user_token='.$this->session->data['user_token'], true));
        }
        // Обработаем ошибки
        if (isset($this->session->data['error_warning'])) {
            $data['error_warning'] = $this->session->data['error_warning'];
            unset($this->session->data['error_warning']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['ozon_seller_client_id'])) {
            $data['error_client_id'] = $this->error['ozon_seller_client_id'];
        } else {
            $data['error_client_id'] = '';
        }

        if (isset($this->error['ozon_seller_api_key'])) {
            $data['error_api_key'] = $this->error['ozon_seller_api_key'];
        } else {
            $data['error_api_key'] = '';
        }

        if (isset($this->error['ozon_seller_cron_pass'])) {
            $data['error_cron_pass'] = $this->error['ozon_seller_cron_pass'];
        } else {
            $data['error_cron_pass'] = '';
        }

        //Крошки
        $data['breadcrumbs']   = [];
        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token='.$this->session->data['user_token'], true),
        ];
        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension',
                'user_token='.$this->session->data['user_token'], true),
        ];
        $data['breadcrumbs'][] = [
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/ozon_seller', 'user_token='.$this->session->data['user_token'],
                true),
        ];
        $data['action']        = $this->url->link('extension/module/ozon_seller',
            'user_token='.$this->session->data['user_token'], true);
        $data['cancel']        = $this->url->link('marketplace/extension',
            'user_token='.$this->session->data['user_token'], true);


        // Запросы
        if (isset($this->request->post['ozon_seller_client_id'])) {
            $data['ozon_seller_client_id'] = $this->request->post['ozon_seller_client_id'];
        } else {
            $data['ozon_seller_client_id'] = $this->config->get('ozon_seller_client_id');
        }

        if (isset($this->request->post['ozon_seller_api_key'])) {
            $data['ozon_seller_api_key'] = $this->request->post['ozon_seller_api_key'];
        } else {
            $data['ozon_seller_api_key'] = $this->config->get('ozon_seller_api_key');
        }

        if (isset($this->request->post['ozon_seller_status'])) {
            $data['ozon_seller_status'][] = $this->request->post['ozon_seller_status'];
        } else {
            $data['ozon_seller_status'] = $this->config->get('ozon_seller_status');
        }

        if (isset($this->request->post['ozon_seller_category'])) {
            $data['ozon_seller_category'] = $this->request->post['ozon_seller_category'];
        } else {
            $data['ozon_seller_category'] = $this->config->get('ozon_seller_category');
        }

        if (isset($this->request->post['ozon_seller_cron_pass'])) {
            $data['ozon_seller_cron_pass'] = $this->request->post['ozon_seller_cron_pass'];
        } else {
            $data['ozon_seller_cron_pass'] = $this->config->get('ozon_seller_cron_pass');
        }

        if (isset($this->request->post['ozon_seller_attribute'])) {
            $data['ozon_seller_attribute'] = $this->request->post['ozon_seller_attribute'];
        } else {
            $data['ozon_seller_attribute'] = $this->config->get('ozon_seller_attribute');
        }

        if (isset($this->request->post['ozon_seller_manufacturer_stop'])) {
            $data['ozon_seller_manufacturer_stop'] = $this->request->post['ozon_seller_manufacturer_stop'];
        } else {
            if (empty($this->config->get('ozon_seller_manufacturer_stop'))) {
                $data['ozon_seller_manufacturer_stop'] = [];
            } else {
                $data['ozon_seller_manufacturer_stop'] = $this->config->get('ozon_seller_manufacturer_stop');
            }
        }

        if (isset($this->request->post['ozon_seller_highway'])) {
            $data['ozon_seller_highway'] = $this->request->post['ozon_seller_highway'];
        } else {
            $data['ozon_seller_highway'] = $this->config->get('ozon_seller_highway');
        }

        if (isset($this->request->post['ozon_seller_percent'])) {
            $data['ozon_seller_percent'] = $this->request->post['ozon_seller_percent'];
        } else {
            $data['ozon_seller_percent'] = $this->config->get('ozon_seller_percent');
        }

        if (isset($this->request->post['ozon_seller_ruble'])) {
            $data['ozon_seller_ruble'] = $this->request->post['ozon_seller_ruble'];
        } else {
            $data['ozon_seller_ruble'] = $this->config->get('ozon_seller_ruble');
        }

        if (isset($this->request->post['ozon_seller_limit'])) {
            $data['ozon_seller_limit'] = $this->request->post['ozon_seller_limit'];
        } else {
            $data['ozon_seller_limit'] = $this->config->get('ozon_seller_limit');
        }

        if (isset($this->request->post['ozon_seller_description'])) {
            $data['ozon_seller_description'] = $this->request->post['ozon_seller_description'];
        } else {
            $data['ozon_seller_description'] = $this->config->get('ozon_seller_description');
        }

        if (isset($this->request->post['ozon_seller_attribute_description'])) {
            $data['ozon_seller_attribute_description'] = $this->request->post['ozon_seller_attribute_description'];
        } else {
            $data['ozon_seller_attribute_description'] = $this->config->get('ozon_seller_attribute_description');
        }

        if (isset($this->request->post['ozon_seller_min_last_mile'])) {
            $data['ozon_seller_min_last_mile'] = $this->request->post['ozon_seller_min_last_mile'];
        } else {
            $data['ozon_seller_min_last_mile'] = $this->config->get('ozon_seller_min_last_mile');
        }

        if (isset($this->request->post['ozon_seller_last_mile'])) {
            $data['ozon_seller_last_mile'] = $this->request->post['ozon_seller_last_mile'];
        } else {
            $data['ozon_seller_last_mile'] = $this->config->get('ozon_seller_last_mile');
        }

        if (isset($this->request->post['ozon_seller_stocks_null'])) {
            $data['ozon_seller_stocks_null'] = $this->request->post['ozon_seller_stocks_null'];
        } else {
            $data['ozon_seller_stocks_null'] = $this->config->get('ozon_seller_stocks_null');
        }

        if (isset($this->request->post['ozon_seller_test_export'])) {
            $data['ozon_seller_test_export'][] = $this->request->post['ozon_seller_test_export'];
        } else {
            $data['ozon_seller_test_export'] = $this->config->get('ozon_seller_test_export');
        }

        if (isset($this->request->post['ozon_seller_entry_offer_id'])) {
            $data['ozon_seller_entry_offer_id'][] = $this->request->post['ozon_seller_entry_offer_id'];
        } else {
            $data['ozon_seller_entry_offer_id'] = $this->config->get('ozon_seller_entry_offer_id');
        }

        if (isset($this->request->post['ozon_seller_autoreturn_fbs'])) {
            $data['ozon_seller_autoreturn_fbs'] = $this->request->post['ozon_seller_autoreturn_fbs'];
        } else {
            $data['ozon_seller_autoreturn_fbs'] = $this->config->get('ozon_seller_autoreturn_fbs');
        }

        if (isset($this->request->post['ozon_seller_status_order_oc'])) {
            $data['ozon_seller_status_order_oc'] = $this->request->post['ozon_seller_status_order_oc'];
        } else {
            $data['ozon_seller_status_order_oc'] = $this->config->get('ozon_seller_status_order_oc');
        }

        if (isset($this->request->post['ozon_seller_status_new'])) {
            $data['ozon_seller_status_new'] = $this->request->post['ozon_seller_status_new'];
        } else {
            $data['ozon_seller_status_new'] = $this->config->get('ozon_seller_status_new');
        }

        if (isset($this->request->post['ozon_seller_status_deliver'])) {
            $data['ozon_seller_status_deliver'] = $this->request->post['ozon_seller_status_deliver'];
        } else {
            $data['ozon_seller_status_deliver'] = $this->config->get('ozon_seller_status_deliver');
        }

        if (isset($this->request->post['ozon_seller_status_cancel'])) {
            $data['ozon_seller_status_cancel'] = $this->request->post['ozon_seller_status_cancel'];
        } else {
            $data['ozon_seller_status_cancel'] = $this->config->get('ozon_seller_status_cancel');
        }

        if (isset($this->request->post['ozon_seller_status_shipping'])) {
            $data['ozon_seller_status_shipping'] = $this->request->post['ozon_seller_status_shipping'];
        } else {
            $data['ozon_seller_status_shipping'] = $this->config->get('ozon_seller_status_shipping');
        }

        if (isset($this->request->post['ozon_seller_status_delevered'])) {
            $data['ozon_seller_status_delevered'] = $this->request->post['ozon_seller_status_delevered'];
        } else {
            $data['ozon_seller_status_delevered'] = $this->config->get('ozon_seller_status_delevered');
        }

        if (isset($this->request->post['ozon_seller_status_return'])) {
            $data['ozon_seller_status_return'] = $this->request->post['ozon_seller_status_return'];
        } else {
            $data['ozon_seller_status_return'] = $this->config->get('ozon_seller_status_return');
        }

        if (isset($this->request->post['ozon_seller_nds'])) {
            $data['ozon_seller_nds'] = $this->request->post['ozon_seller_nds'];
        } elseif ( ! empty($this->config->get('ozon_seller_nds'))) {
            $data['ozon_seller_nds'] = $this->config->get('ozon_seller_nds');
        } else {
            $data['ozon_seller_nds'] = '0';
        }

        if (isset($this->request->post['ozon_seller_fictitious_price'])) {
            $data['ozon_seller_fictitious_price'] = $this->request->post['ozon_seller_fictitious_price'];
        } else {
            $data['ozon_seller_fictitious_price'] = $this->config->get('ozon_seller_fictitious_price');
        }

        if (isset($this->request->post['ozon_seller_product_blacklist'])) {
            $data['ozon_seller_product_blacklist'] = $this->request->post['ozon_seller_product_blacklist'];
        } elseif ( ! empty($this->config->get('ozon_seller_product_blacklist'))) {
            $data['ozon_seller_product_blacklist'] = $this->config->get('ozon_seller_product_blacklist');
        } else {
            $data['ozon_seller_product_blacklist'] = array();
        }

        if (isset($this->request->post['ozon_seller_warehouse_ozon'])) {
            $data['ozon_seller_warehouse_ozon'] = $this->request->post['ozon_seller_warehouse_ozon'];
        } else {
            $data['ozon_seller_warehouse_ozon'] = $this->config->get('ozon_seller_warehouse_ozon');
        }

        if (isset($this->request->post['ozon_seller_price_round'])) {
            $data['ozon_seller_price_round'] = $this->request->post['ozon_seller_price_round'];
        } else {
            $data['ozon_seller_price_round'] = $this->config->get('ozon_seller_price_round');
        }

        if (isset($this->request->post['ozon_seller_product_price_oc'])) {
            $data['ozon_seller_product_price_oc'] = $this->request->post['ozon_seller_product_price_oc'];
        } else {
            $data['ozon_seller_product_price_oc'] = $this->config->get('ozon_seller_product_price_oc');
        }

        if (isset($this->request->post['ozon_seller_act'])) {
            $data['ozon_seller_act'] = $this->request->post['ozon_seller_act'];
        } else {
            $data['ozon_seller_act'] = $this->config->get('ozon_seller_act');
        }

        if (isset($this->request->post['ozon_seller_export_stock_null'])) {
            $data['ozon_seller_export_stock_null'] = $this->request->post['ozon_seller_export_stock_null'];
        } else {
            $data['ozon_seller_export_stock_null'] = $this->config->get('ozon_seller_export_stock_null');
        }


        // Округление цен
        $data['price_round'] = [
            '0'     => $this->language->get('text_round_price_null'),
            'st'    => $this->language->get('text_round_price_st'),
            'ten'   => $this->language->get('text_round_price_ten'),
            'st_so' => $this->language->get('text_round_price_st_so'),
            'so'    => $this->language->get('text_round_price_so'),
            'fifty' => $this->language->get('text_round_price_fifty'),
        ];

        // НДС
        $data['nalog_nds'] = [
            '0'   => $this->language->get('text_nds_null'),
            '0.1' => $this->language->get('text_nds_ten'),
            '0.2' => $this->language->get('text_nds_twenty'),
        ];

        // Черный список
        $data['blacklist'] = [];
        if ( ! empty($this->config->get('ozon_seller_product_blacklist'))) {
            $this->load->model('catalog/product');
            $blacklist = $this->config->get('ozon_seller_product_blacklist');
            foreach ($blacklist as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);
                if ($product_info) {
                    $data['blacklist'][] = [
                        'product_id' => $product_info['product_id'],
                        'name'       => $product_info['name'],
                    ];
                }
            }
        }

        $this->load->model('extension/module/ozon_seller');

        $data['suppliers'] = [];
        $results           = $this->model_extension_module_ozon_seller->getSuppliers();
        foreach ($results as $result) {
            $data['suppliers'][] = array(
                'name' => strip_tags(html_entity_decode($result['supplier_main'], ENT_QUOTES, 'UTF-8')),
            );
        }

        // Категории
        $data['allCategories'] = [];
        $filter_data           = ['sort' => 'fullname', 'order' => 'ASC'];
        $results               = $this->model_extension_module_ozon_seller->getAllCategories($filter_data);
        foreach ($results as $result) {
            $level                   = count(explode("&gt;", $result['fullname']));
            $data['allCategories'][] = [
                'category_id'   => $result['category_id'],
                'name'          => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
                'level'         => $level,
                'parent_id'     => $result['parent_id'],
                'product_count' => $result['product_count'],
                'sort_order'    => $result['sort_order'],
                'status'        => $result['status'],
            ];
        }

        $this->load->model('catalog/category');
        $filter_data        = [
            'sort'  => 'name',
            'order' => 'ASC',
        ];
        $data['categories'] = $this->model_catalog_category->getCategories($filter_data);
        if ( ! empty($data['ozon_seller_category'])) {
            $ozon_categories = [];
            foreach ($data['ozon_seller_category'] as $ozon_category_title) {
                if ( ! in_array($ozon_category_title['ozon'], $ozon_categories)) {
                    $ozon_categories[] = $ozon_category_title['ozon'];
                }
            }
            $data['ozon_categories'] = $this->model_extension_module_ozon_seller->getOzonCategory($ozon_categories);
        }
        $data['dictionarys_ozon'] = $this->model_extension_module_ozon_seller->getOzonDictionaryNoCategory();

        // Атрибуты
        $this->load->model('catalog/attribute');
        $data['button_download_attr'] = $this->buttonDownloadAttr();
        $filter_data                  = ['sort' => 'ad.name'];
        $data['attributes_shop']      = $this->model_catalog_attribute->getAttributes($filter_data);
        $data['attributes_shop'][]    = ['attribute_id' => 'sku', 'name' => 'Артикул'];
        $data['attributes_shop'][]    = ['attribute_id' => 'model', 'name' => 'Модель'];
        $data['attributes_shop'][]    = ['attribute_id' => 'mpn', 'name' => 'MPN'];
        $data['attributes_shop'][]    = ['attribute_id' => 'isbn', 'name' => 'ISBN'];
        $data['attributes_shop'][]    = ['attribute_id' => 'ean', 'name' => 'EAN'];
        $data['attributes_shop'][]    = ['attribute_id' => 'jan', 'name' => 'JAN'];
        $data['attributes_shop'][]    = ['attribute_id' => 'upc', 'name' => 'UPC'];
        $data['attributes_shop'][]    = ['attribute_id' => 'product_dimensions', 'name' => 'Размеры, мм (ДхШхВ)'];
        $data['attributes_shop'][]    = ['attribute_id' => 'product_weight', 'name' => 'Вес товара, г'];
        $data['attributes_shop'][]    = ['attribute_id' => 'product_length', 'name' => 'Длина товара, мм'];
        $data['attributes_shop'][]    = ['attribute_id' => 'product_width', 'name' => 'Ширина товара, мм'];
        $data['attributes_shop'][]    = ['attribute_id' => 'product_height', 'name' => 'Высота товара, мм'];
        $data['attributes_ozon']
                                      = $this->model_extension_module_ozon_seller->getOzonAttribute();//DB table ozon_atribute

        $attr_ozon_descr                     = $this->model_extension_module_ozon_seller->getOzonAttributeDescription();
        $data['attributes_ozon_description'] = [];
        foreach ($attr_ozon_descr as $attr_ozon) {
            if ($attr_ozon['ozon_attribute_id'] == 8229) {
                continue;
            } else {
                $data['attributes_ozon_description'][] = $attr_ozon;
            }
        }

        // Производители
        $this->load->model('catalog/manufacturer');
        $filter_data           = ['sort' => 'name'];
        $data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers($filter_data);

        // Статусы заказа OC
        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        // Связка товара с МС
        $data['connect_shop'] = ['sku' => $data['text_sku'], 'model' => $data['text_model']];
        $data['connect_ms']   = [
            'article' => $data['text_sku_ms'],
            'code'    => $data['text_code_ms'],
            'ex_code' => $data['text_external_сode'],
        ];


        if (isset($this->request->post['ozon_seller_filter'])) {
            $data['ozon_seller_filter'] = $this->request->post['ozon_seller_filter'];
        } else {
            if (empty($this->config->get('ozon_seller_filter'))) {
                $data['ozon_seller_filter'] = [];
            } else {
                $data['ozon_seller_filter']
                    = $this->model_extension_module_ozon_seller->prepareFilter($this->config->get('ozon_seller_filter'),
                    $data['allCategories'], $data['manufacturers']);
            }
        }

        if (isset($this->request->post['ozon_seller_additional_settings'])) {
            $data['ozon_seller_additional_settings'] = $this->request->post['ozon_seller_additional_settings'];
        } else {
            if (empty($this->config->get('ozon_seller_additional_settings'))) {
                $data['ozon_seller_additional_settings'] = [];
            } else {
                $data['ozon_seller_additional_settings']
                    = $this->model_extension_module_ozon_seller->prepareAdditionalSettings($this->config->get('ozon_seller_additional_settings'));
            }
        }


        // URL
        $data['import_url'] = HTTPS_CATALOG.'index.php?route=extension/module/ozon_seller/importproduct&cron_pass='
            .$data['ozon_seller_cron_pass'];

        $data['update_url'] = HTTPS_CATALOG.'index.php?route=extension/module/ozon_seller/updateozonproduct&cron_pass='
            .$data['ozon_seller_cron_pass'];

        $data['url_order_in_ms'] = HTTPS_CATALOG
            .'index.php?route=extension/module/ozon_seller/getnewordersozon&cron_pass='.$data['ozon_seller_cron_pass'];

        $data['url_final_status'] = HTTPS_CATALOG
            .'index.php?route=extension/module/ozon_seller/statusdelivered&cron_pass='.$data['ozon_seller_cron_pass'];

        $data['url_chek_order_list'] = HTTPS_CATALOG
            .'index.php?route=extension/module/ozon_seller/chekorderlist&cron_pass='.$data['ozon_seller_cron_pass'];

        $data['url_update_ozon_product_status'] = HTTPS_CATALOG
            .'index.php?route=extension/module/ozon_seller/updatenewozonproduct&cron_pass='
            .$data['ozon_seller_cron_pass'];


        $data['url_manufacturer_download'] = HTTPS_CATALOG
            .'index.php?route=extension/module/ozon_seller/manufacturerdownload&cron_pass='
            .$data['ozon_seller_cron_pass'];

        $data['url_product'] = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'], true);

        $data['url_order'] = $this->url->link('extension/module/ozon_seller/order',
            'user_token='.$this->session->data['user_token'], true);

        $data['url_warehouse'] = HTTPS_CATALOG.'index.php?route=extension/module/ozon_seller/warehouseozon&cron_pass='
            .$data['ozon_seller_cron_pass'];

        $this->load->model('localisation/length_class');

        $data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();

        if (isset($this->request->post['ozon_seller_length'])) {
            $data['ozon_seller_length'] = $this->request->post['ozon_seller_length'];
        } elseif ( ! empty('ozon_seller_length')) {
            $data['ozon_seller_length'] = $this->config->get('ozon_seller_length');
        } else {
            $data['ozon_seller_length'] = $this->config->get('config_length_class_id');
        }

        $this->load->model('localisation/weight_class');

        $data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

        if (isset($this->request->post['ozon_seller_weight'])) {
            $data['ozon_seller_weight'] = $this->request->post['ozon_seller_weight'];
        } elseif ( ! empty('ozon_seller_weight')) {
            $data['ozon_seller_weight'] = $this->config->get('ozon_seller_weight');
        } else {
            $data['ozon_seller_weight'] = $this->config->get('config_weight_class_id');
        }

        $data['clear'] = $this->url->link('extension/module/ozon_seller/clear',
            'user_token='.$this->session->data['user_token'], 'SSL');

        $data['log'] = '';
        $file        = DIR_LOGS.'ozon_seller.log';
        if (file_exists($file)) {
            $size = filesize($file);

            if ($size >= 5242880) {
                $suffix = array(
                    'B',
                    'KB',
                    'MB',
                    'GB',
                    'TB',
                    'PB',
                    'EB',
                    'ZB',
                    'YB',
                );

                $i = 0;

                while (($size / 1024) > 1) {
                    $size = $size / 1024;
                    $i++;
                }

                $data['error_warning'] = sprintf($this->language->get('error_warning'), basename($file),
                    round(substr($size, 0, strpos($size, '.') + 4), 2).$suffix[$i]);
            } else {
                $data['log'] = file_get_contents($file, FILE_USE_INCLUDE_PATH, null);
            }
        }

        // Отображение
        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');
        $data['user_token']  = $this->session->data['user_token'];

        $this->response->setOutput($this->load->view('extension/module/ozon_seller', $data));
    }

    /* Кнопка скачать атрибуты */
    public function buttonDownloadAttr()
    {

        $this->load->model('setting/setting');
        $this->load->model('extension/module/ozon_seller');
        $categorys  = $this->config->get('ozon_seller_category');
        $attributes = $this->model_extension_module_ozon_seller->getOzonAttribute();
        $output     = '';

        if (isset($categorys)) {
            if ( ! empty($attributes)) {
                $category_attr = array();
                foreach ($attributes as $attribute) {
                    $category_attr[] = $attribute['ozon_category_id'];
                }
                foreach ($categorys as $category) {
                    $category_cat = $category['ozon'];
                    if ( ! in_array($category_cat, $category_attr)) {
                        $check = true;
                        break;
                    }
                }
            } else {
                $check = true;
            }

            if (isset($check)) {
                $output = '<button type="button" class="btn btn-warning load-attribute">'
                    .$this->language->get('attributes_update').'</button>';
            }
        }

        return $output;
    }

    /* Формирование модального окна для сопоставления производителей */
    public function manufacturerSet()
    {

        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
            exit();
        }
        $this->load->model('extension/module/ozon_seller');
        $this->load->model('catalog/manufacturer');
        $filter_data        = array('sort' => 'name');
        $shop_manufacturers = $this->model_catalog_manufacturer->getManufacturers($filter_data);
        $body               = '<form action="" method="post" id="manufacturer-form">
			<div class="table-responsive">
				<table class="table table-sm table-striped table-bordered table-hover">
					<thead>
						<tr>
							<td>Производители в магазине</td>
							<td>Производители в Ozon</td>
							<td>Изображение</td>
						</tr>
					</thead>
					<tbody>';

        foreach ($shop_manufacturers as $shop_manufacturer) {
            $body .= '<tr><td><input type="hidden" name="manufacturer['.$shop_manufacturer['manufacturer_id']
                .'][shop_id]" value="'.$shop_manufacturer['manufacturer_id'].'" />'.$shop_manufacturer['name'].'</td>';
            $ozon_manufacturer
                  = $this->model_extension_module_ozon_seller->getManufacturer($shop_manufacturer['manufacturer_id']);
            if (empty($ozon_manufacturer)) {
                $ozon_img   = '';
                $ozon_value = '';
                $ozon_id    = '';
            } else {
                $ozon_img   = '<img src="'.$ozon_manufacturer[0]['picture'].'" height="30" />';
                $ozon_value = $ozon_manufacturer[0]['value'];
                $ozon_id    = $ozon_manufacturer[0]['ozon_id'];
            }
            $body .= '<td>
			<input type="text" class="form-control manufacturer-ozon" data-id="'.$shop_manufacturer['manufacturer_id']
                .'" value="'.$ozon_value.'" />
			<input type="hidden" name="manufacturer['.$shop_manufacturer['manufacturer_id'].'][ozon_id]" value="'
                .$ozon_id.'" /></td>';

            $body .= '<td><div class="img-manufacturer'.$shop_manufacturer['manufacturer_id'].'">'.$ozon_img
                .'</div></td></tr>';
        }
        $body .= '</tbody></table></div></form>';
        $body .= "<script>
			$('.manufacturer-ozon').focus(function() {
				manufacturer = $(this).attr('data-id');
			});

			var inputOzonId = 'manufacturer[' + window.manufacturer + '][ozon_id]';

			$('.manufacturer-ozon').autocomplete({
				'source': function(request, response) {
					$.ajax({
						url: 'index.php?route=extension/module/ozon_seller/manufacturerautocomplete&user_token="
            .$this->session->data['user_token']."&filter_name=' +  encodeURIComponent(request),
						dataType: 'json',
						success: function(json) {
							json.unshift({
								ozon_id: 'delete',
								picture: '',
								value: '-- очистить --'
							});
							response($.map(json, function(item) {
								return {
									label: item['value'] ,
									icon: item['picture'],
									value: item['ozon_id']
								}
							}));
						}
					});
				},
				'select': function(item) {
					$('input[data-id=\'' + window.manufacturer + '\']').val(item['label']);
					$('input[name=\'manufacturer[' + window.manufacturer + '][ozon_id]\']').val(item['value']);
					$('.img-manufacturer' + window.manufacturer).html(item['icon']);
				}
			});
		</script>";

        $footer
            = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-success save-manufacturer">Сохранить изменения</button>';

        $output['title']  = 'Сопоставьте производителей Ozon и магазина';
        $output['body']   = $body;
        $output['footer'] = $footer;
        echo json_encode($output);
    }

    /* Живой поиск производителей Ozon */
    public function manufacturerAutocomplete()
    {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $this->load->model('extension/module/ozon_seller');
            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start'       => 0,
                'limit'       => 10,
            );
            $results     = $this->model_extension_module_ozon_seller->searchOzonManufacturer($filter_data);
            foreach ($results as $result) {
                $json[] = array(
                    'ozon_id' => $result['ozon_id'],
                    'value'   => strip_tags(html_entity_decode($result['value'], ENT_QUOTES, 'UTF-8')),
                    'picture' => '<img src="'.$result['picture'].'" height="30" />',
                );
            }
        }

        $sort_order = array();
        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['value'];
        }
        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /* Сохранить сопоставленных производителей */
    public function saveManufacturer()
    {
        $this->load->model('extension/module/ozon_seller');
        if (isset($_POST['manufacturer'])) {
            foreach ($_POST['manufacturer'] as $manufacture) {
                if ( ! empty($manufacture['ozon_id'])) {
                    $shop_id  = $manufacture['shop_id'];
                    $ozon_id  = $manufacture['ozon_id'];
                    $response = $this->model_extension_module_ozon_seller->updateManufacturer($shop_id, $ozon_id);
                }
            }
        }
    }

    /* Формирование модального окна для типа товара */
    public function modalType()
    {

        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
            exit();
        }
        $this->load->language('extension/module/ozon_seller');
        $this->load->model('extension/module/ozon_seller');
        if (isset($this->request->get['category_ozon'])) {
            $category_ozon = $this->request->get['category_ozon'];
            $types_ozon    = $this->model_extension_module_ozon_seller->getOzonDictionaryType($category_ozon);
            if ( ! empty($types_ozon)) {
                $body = '<form action="" method="post" id="type-form">
					<div class="table-responsive" style="padding:15px;">
						<table class="table table-sm table-striped table-bordered table-hover">
							<thead>
								<tr>
									<td>Типы товара этой категории на Ozon</td>
									<td>Значение атрибута магазина</td>
								</tr>
							</thead>
							<tbody>';

                $dictionary_row = 0;
                foreach ($types_ozon as $type_ozon) {
                    $body .= '<tr><td>'.$type_ozon['text'].'<input type="hidden" name="type['.$dictionary_row
                        .'][value]" value="'.$type_ozon['text'].'" /><input type="hidden" name="type['.$dictionary_row
                        .'][dictionary_value_id]" value="'.$type_ozon['attribute_value_id'].'" />
					</td>';

                    $types_shop
                        = $this->model_extension_module_ozon_seller->getDictionaryShoptoOzonDictionaryId($type_ozon['attribute_value_id']);

                    if ( ! empty($types_shop)) {
                        foreach ($types_shop as $type_shop) {
                            $type_text_shop = $type_shop['text_shop_attribute'];
                        }
                    } else {
                        $type_text_shop = '';
                    }
                    $body .= '<td>
						<input name="type['.$dictionary_row.'][text_shop_attribute]" value="'.$type_text_shop.'" title="Можно указать несколько значений разделяя их ++. Пример: 1++2++3" />
					</td></tr>';
                    $dictionary_row++;
                }

                $body .= '</tbody></table></div></form>';
                $body .= $this->language->get('help_attribute');

                $footer
                    = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-success save-type">Сохранить изменения</button>';

                $output['title']  = 'Сопоставьте тип товара этой категории на Ozon с атрибутами в магазине';
                $output['body']   = $body;
                $output['footer'] = $footer;
                echo json_encode($output);
            }
        }
    }

    /* Формирование модального окна с атрибутами */
    public function modalDictionary()
    {

        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
            exit();
        }
        $this->load->model('extension/module/ozon_seller');
        if (isset($this->request->get['dictionary'])) {
            $dictionary_id = $this->request->get['dictionary'];
        }
        $attributes_ozon_description = $this->model_extension_module_ozon_seller->getOzonAttributeDescription();

        foreach ($attributes_ozon_description as $attribute_ozon_description) {
            // в озон не уникальные id справочников
            $chek_dictionary = $attribute_ozon_description['ozon_attribute_id']
                .$attribute_ozon_description['ozon_dictionary_id'];
            if ($chek_dictionary == $dictionary_id) {
                $title              = $attribute_ozon_description['ozon_attribute_name'];
                $ozon_attribute_id  = $attribute_ozon_description['ozon_attribute_id'];
                $ozon_dictionary_id = $attribute_ozon_description['ozon_dictionary_id'];
                break;
            }
        }
        $body = '<form action="" method="post" id="dictionary-form">
					<input type="hidden" name="ozon-attribute-id" value="'.$ozon_attribute_id.'" />
					<input type="hidden" name="ozon-dictionary-id" value="'.$ozon_dictionary_id.'" />
					<div class="table-responsive" style="padding:15px;">
						<table class="table table-sm table-striped table-bordered table-hover">
							<thead>
								<tr>
									<td>Атрибут магазина</td>
									<td>Атрибуты Ozon</td>
									<td>Поиск атрибутов Ozon</td>
								</tr>
							</thead>
							<tbody>';

        $this->load->model('setting/setting');
        $ozon_seller_attribute    = $this->config->get('ozon_seller_attribute');
        $shop_attribute_id        = $ozon_seller_attribute[$ozon_attribute_id];
        $dictionary_shop          = $this->model_extension_module_ozon_seller->getShopDictionary($shop_attribute_id);
        $dictionarys_ozon         = $this->model_extension_module_ozon_seller->getOzonDictionary($ozon_dictionary_id);
        $dictionarys_ozon_to_shop = $this->model_extension_module_ozon_seller->getDictionaryShoptoOzon();

        $dictionary_row = 0;
        foreach ($dictionary_shop as $dictionar_shop) {
            $body               .= '<tr><td>'.$dictionar_shop['text'].'<input type="hidden" name="dictionary['
                .$dictionary_row.'][text-shop-attribute]" value="'.$dictionar_shop['text'].'" /></td>';
            $attribute_value_id = '';
            $text               = '';
            $body               .= '<td><div data-attr-list="'.$dictionary_row.'">';

            foreach ($dictionarys_ozon as $dictionary_ozon) {
                // в озон не уникальные id справочников
                $chek_dictionary = $dictionary_ozon['attribute_group_id'].$dictionary_ozon['dictionary_id'];
                if ($chek_dictionary == $dictionary_id) {
                    foreach ($dictionarys_ozon_to_shop as $dictionary_ozon_to_shop) {
                        if (empty($dictionary_ozon_to_shop['dictionary_value_id'])) {
                            continue;
                        }
                        $attribute_value_id = explode('^', $dictionary_ozon_to_shop['dictionary_value_id']);

                        foreach ($attribute_value_id as $attribut_value_id) {
                            if ($attribut_value_id == $dictionary_ozon['attribute_value_id']
                                && $dictionary_ozon_to_shop['text_shop_attribute'] == html_entity_decode($dictionar_shop['text'], ENT_QUOTES, 'UTF-8')
                            ) {

                                $attribut_value_id = $dictionary_ozon['attribute_value_id'];
                                $text              = $dictionary_ozon['text'];
                                $body              .= '<div data-attr="'.$dictionary_row.$attribut_value_id
                                    .'"><i class="fa fa-minus-circle"></i> '.$text.'
									<input type="hidden" name="dictionary['.$dictionary_row
                                    .'][dictionary-value-id][]" value="'.$attribut_value_id.'" />
									<input type="hidden" name="dictionary['.$dictionary_row.'][value][]" value="'.$text.'" />
								</div>';
                            }
                        }
                    }
                }
            }
            $body .= '</div></td><td><input type="text" class="form-control dictionary-search" data-dictionary-id="'
                .$ozon_dictionary_id.'" data-row="'.$dictionary_row.'" /></td></tr>';
            $dictionary_row++;
        }
        $body .= '</tbody></table></div>
		<input type="hidden" name="shop-attribute-id" value="'.$shop_attribute_id.'" /></form>';
        $body .= "<script>
		$('.dictionary-search').focus(function() {
			rows = $(this).attr('data-row');
			dictionary_id = $(this).attr('data-dictionary-id');
		});

		$('.dictionary-search').autocomplete({
			'multiselect': true,
			'source': function(request, response) {
				$.ajax({
					url: 'index.php?route=extension/module/ozon_seller/autocompletedictionary&dictionary=' + dictionary_id + '&user_token="
            .$this->session->data['user_token']."&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item.text,
								value: item.attribute_value_id
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('.dictionary-search').val('');

				$('[data-attr=\'' + window.rows + item['value'] + '\']').remove();

				$('[data-attr-list=\'' + window.rows + '\']').append('<div data-attr=\'' + window.rows + item['value'] + '\'><i class=\'fa fa-minus-circle\'></i> ' + item['label'] + '<input type=\'hidden\' name=\'dictionary[' + window.rows +  '][value][]\' value=\'' + item['label'] + '\' />' + '<input type=\'hidden\' name=\'dictionary[' + window.rows +  '][dictionary-value-id][]\' value=\'' + item['value'] + '\' /></div>');
			}
		});
		$('[data-attr-list]').delegate('.fa-minus-circle', 'click', function() {
			$(this).parent().remove();
		});</script>";

        $footer
            = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button><button type="button" class="btn btn-success save-dictionary">Сохранить изменения</button>';

        $data['dictionary_shop'] = array();

        $output['title']  = $title;
        $output['body']   = $body;
        $output['footer'] = $footer;
        echo json_encode($output);
    }

    /* Сохранить сопоставленные значения типа товара */
    public function saveType()
    {
        $this->load->model('extension/module/ozon_seller');
        foreach ($_POST['type'] as $type) {
            $value               = $type['value'];
            $dictionary_value_id = $type['dictionary_value_id'];
            $text_shop_attribute = trim($type['text_shop_attribute']);
            if (empty($text_shop_attribute)) {
                $this->model_extension_module_ozon_seller->deleteDictionaryValue($dictionary_value_id);
            } else {
                $response = $this->model_extension_module_ozon_seller->saveDictionary($ozon_attribute_id = 8229,
                    $shop_attribute_id = 0, $dictionary_value_id, $value, $text_shop_attribute);
            }
        }
    }

    /* Сохранить сопоставленные значения словарей */
    public function saveDictionary()
    {

        $this->load->model('extension/module/ozon_seller');
        $ozon_attribute_id = $_POST['ozon-attribute-id'];
        $shop_attribute_id = $_POST['shop-attribute-id'];
        $this->model_extension_module_ozon_seller->unsetDictionary($ozon_attribute_id, $shop_attribute_id);
        foreach ($_POST['dictionary'] as $dictionary) {
            if ( ! empty($dictionary['dictionary-value-id']) && ! empty($dictionary['value'])) {
                $dictionary_value_id = implode('^', $dictionary['dictionary-value-id']);
                $value               = implode('^', $dictionary['value']);
                $text_shop_attribute = htmlspecialchars($dictionary['text-shop-attribute']);
                $response            = $this->model_extension_module_ozon_seller->saveDictionary($ozon_attribute_id,
                    $shop_attribute_id, $dictionary_value_id, $value, $text_shop_attribute);
            }
        }
    }

    public function clear()
    {
        if ( ! $this->user->hasPermission('modify', 'extension/module/ozon_seller')) {
            $this->session->data['error'] = $this->language->get('error_permission');
        } else {
            $file   = DIR_LOGS.'ozon_seller.log';
            $handle = fopen($file, 'w+');
            fclose($handle);
            $this->session->data['success'] = 'Лог очищен';
        }
        $this->response->redirect($this->url->link('extension/module/ozon_seller',
            'user_token='.$this->session->data['user_token'], 'SSL'));
    }

    /* Живой поиск категорий Ozon */
    public function autocomplete()
    {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $this->load->model('extension/module/ozon_seller');
            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start'       => 0,
                'limit'       => 70,
            );
            $results     = $this->model_extension_module_ozon_seller->searchOzonCategory($filter_data);
            foreach ($results as $result) {
                $json[] = array(
                    'ozon_category_id' => $result['ozon_category_id'],
                    'title'            => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8')),
                );
            }
        }
        $sort_order = array();
        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['title'];
        }
        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /* Живой поиск справочника Ozon */
    public function autocompleteDictionary()
    {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $this->load->model('extension/module/ozon_seller');
            $filter_data = array(
                'filter_name'        => $this->request->get['filter_name'],
                'ozon_dictionary_id' => $this->request->get['dictionary'],
                'start'              => 0,
                'limit'              => 20,
            );
            $results     = $this->model_extension_module_ozon_seller->searchOzonDictionary($filter_data);
            foreach ($results as $result) {
                $json[] = array(
                    'attribute_value_id' => $result['attribute_value_id'],
                    'text'               => strip_tags(html_entity_decode($result['text'], ENT_QUOTES, 'UTF-8')),
                );
            }
        }
        $sort_order = array();
        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['text'];
        }
        array_multisort($sort_order, SORT_ASC, $json);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    // Удалить товары в статусе
    public function clearProductInStatus()
    {
        if (isset($this->request->get['status'])) {
            $status = $this->request->get['status'];
            $this->load->model('extension/module/ozon_seller');
            $this->model_extension_module_ozon_seller->deletedExportProductStatus($status);
            $this->response->redirect($this->url->link('extension/module/ozon_seller/product&filter_status='.$status,
                'user_token='.$this->session->data['user_token'], true));
        }
    }

    /* Список товаров в Ozon */
    public function product()
    {
        $data = $this->load->language('extension/module/ozon_seller');
        $this->document->setTitle($this->language->get('heading_title_my_product'));
        $this->load->model('extension/module/ozon_seller');
        $this->load->model('setting/setting');
        $this->getList();
    }

    protected function getList()
    {

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_model'])) {
            $filter_model = $this->request->get['filter_model'];
        } else {
            $filter_model = null;
        }

        if (isset($this->request->get['filter_sku'])) {
            $filter_sku = $this->request->get['filter_sku'];
        } else {
            $filter_sku = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.date';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url                       = '';
        $data['btn_update_status'] = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name='.urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model='.urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_sku'])) {
            $url .= '&filter_sku='.$this->request->get['filter_sku'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status='.urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES,
                    'UTF-8'));

            $url_update_status = $this->url->link('extension/module/ozon_seller/clearproductinstatus&status='
                .$this->request->get['filter_status'], 'user_token='.$this->session->data['user_token'], true);

            $data['btn_update_status'] = '<a href="'.$url_update_status
                .'" type="button" class="btn btn-primary status-update">Удалить из таблицы товары в статусе '
                .$this->request->get['filter_status'].'</a>';
        }

        $data['user_token']                = $this->session->data['user_token'];
        $data['ozon_seller_cron_pass']     = $this->config->get('ozon_seller_cron_pass');
        $data['url_update_status_product'] = HTTPS_CATALOG
            .'index.php?route=extension/module/ozon_seller/updatenewozonproduct&cron_pass='
            .$data['ozon_seller_cron_pass'];

        if (isset($this->request->get['sort'])) {
            $url .= '&sort='.$this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order='.$this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page='.$this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token='.$this->session->data['user_token'], true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension',
                'user_token='.$this->session->data['user_token'], true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/ozon_seller', 'user_token='.$this->session->data['user_token'],
                true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('product'),
            'href' => $this->url->link('extension/module/ozon_seller/product',
                'user_token='.$this->session->data['user_token'], true),
        );

        $data['statuses'] = array(
            'failed'            => 'failed',
            'failed_validation' => 'failed validation',
            'failed_moderation' => 'failed moderation',
            'pending'           => 'pending',
            'processing'        => 'processing',
            'imported'          => 'imported',
            'processed'         => 'processed',
        );

        $data['products'] = array();

        $filter_data = array(
            'filter_name'   => $filter_name,
            'filter_model'  => $filter_model,
            'filter_sku'    => $filter_sku,
            'filter_status' => $filter_status,
            'sort'          => $sort,
            'order'         => $order,
            'start'         => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'         => $this->config->get('config_limit_admin'),
        );

        $product_total = $this->model_extension_module_ozon_seller->getTotalProducts($filter_data);

        $results = $this->model_extension_module_ozon_seller->getProducts($filter_data);

        foreach ($results as $result) {

            $status = '';

            switch ($result['status']) {
                case 'imported':
                case 'pending':
                case 'posting':
                    $status .= '<span class="label label-warning">'.$result['status'].'</span>';
                    break;

                case 'processing':
                    $status .= '<span class="label label-info">'.$result['status'].'</span>';
                    break;

                case 'processed':
                    $status .= '<span class="label label-success">'.$result['status'].'</span>';
                    break;

                default:
                    $status .= '<span class="label label-danger">'.$result['status'].'</span>';
                    break;
            }

            $view_ozon = '';

            if ( ! empty($result['ozon_sku'])) {
                $view_ozon .= '<a href="https://www.ozon.ru/context/detail/id/'.$result['ozon_sku']
                    .'" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-eye"></i></a>';
            }

            $data['products'][] = array(
                'product_id'      => $result['product_id'],
                'name'            => $result['name'],
                'sku'             => $result['sku'],
                'model'           => $result['model'],
                'ozon_product_id' => $result['ozon_product_id'],
                'date'            => $result['date'],
                'error'           => $result['error'],
                'view_ozon'       => $view_ozon,
                'status'          => $status,
            );
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name='.urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_sku'])) {
            $url .= '&filter_sku='.urlencode(html_entity_decode($this->request->get['filter_sku'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model='.urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status='.urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES,
                    'UTF-8'));
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page='.$this->request->get['page'];
        }

        $data['sort_name']   = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].'&sort=pd.name'.$url, true);
        $data['sort_sku']    = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].'&sort=p.sku'.$url, true);
        $data['sort_model']  = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].'&sort=p.model'.$url, true);
        $data['sort_status'] = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].'&sort=p.status'.$url, true);
        $data['sort_date']   = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].'&sort=p.date'.$url, true);
        $data['sort_order']  = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].'&sort=p.sort_order'.$url, true);

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name='.urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_model'])) {
            $url .= '&filter_model='.urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_sku'])) {
            $url .= '&filter_sku='.urlencode(html_entity_decode($this->request->get['filter_sku'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status='.urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort='.$this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order='.$this->request->get['order'];
        }

        $pagination        = new Pagination();
        $pagination->total = $product_total;
        $pagination->page  = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url   = $this->url->link('extension/module/ozon_seller/product',
            'user_token='.$this->session->data['user_token'].$url.'&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'),
            ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0,
            ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total
                    - $this->config->get('config_limit_admin'))) ? $product_total
                : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
            $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

        $data['filter_name']   = $filter_name;
        $data['filter_sku']    = $filter_sku;
        $data['filter_model']  = $filter_model;
        $data['filter_status'] = $filter_status;

        $data['sort']  = $sort;
        $data['order'] = $order;

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/ozon_seller_product', $data));
    }

    public function updateProductOzon()
    {
        $this->load->model('extension/module/ozon_seller');
        if (isset($this->request->get['product_shop_id'])) {
            $product_id = $this->request->get['product_shop_id'];
            $this->model_extension_module_ozon_seller->deletedExportProduct($product_id);
        }
        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $product_id) {
                $this->model_extension_module_ozon_seller->deletedExportProduct($product_id);
            }
        }
    }

    //Список заказов Ozon
    public function order()
    {

        $data = $this->load->language('extension/module/ozon_seller');
        $this->document->setTitle($this->language->get('heading_title_order'));
        $this->load->model('extension/module/ozon_seller');
        $this->load->model('setting/setting');
        $this->document->addScript('view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addStyle('view/javascript/jquery/magnific/magnific-popup.css');

        if (isset($this->request->get['filter_posting_number'])) {
            $filter_posting_number = $this->request->get['filter_posting_number'];
        } else {
            $filter_posting_number = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }

        if (isset($this->request->get['filter_shipment_date'])) {
            $filter_shipment_date = $this->request->get['filter_shipment_date'];
        } else {
            $filter_shipment_date = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'shipment_date';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_posting_number'])) {
            $url .= '&filter_posting_number='.urlencode(html_entity_decode($this->request->get['filter_posting_number'],
                    ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_shipment_date'])) {
            $url .= '&filter_shipment_date='.urlencode(html_entity_decode($this->request->get['filter_shipment_date'],
                    ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status='.urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort='.$this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order='.$this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page='.$this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token='.$this->session->data['user_token'], true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension',
                'user_token='.$this->session->data['user_token'], true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/ozon_seller', 'user_token='.$this->session->data['user_token'],
                true),
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('order_ozon'),
            'href' => $this->url->link('extension/module/ozon_seller/order',
                'user_token='.$this->session->data['user_token'], true),
        );

        $data['user_token'] = $this->session->data['user_token'];

        $data['ozon_seller_cron_pass'] = $this->config->get('ozon_seller_cron_pass');

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $data['statuses'] = array(
            'awaiting_packaging' => $data['awaiting_packaging'],
            'awaiting_deliver'   => $data['awaiting_deliver'],
            'cancelled'          => $data['cancelled'],
            'delivering'         => $data['delivering'],
            'delivered'          => $data['delivered'],
            'returned'           => $data['returned'],
        );

        $filter_data = array(
            'filter_posting_number' => $filter_posting_number,
            'filter_shipment_date'  => $filter_shipment_date,
            'filter_status'         => $filter_status,
            'sort'                  => $sort,
            'order'                 => $order,
            'start'                 => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'                 => $this->config->get('config_limit_admin'),
        );

        $order_total = $this->model_extension_module_ozon_seller->getTotalOrders($filter_data);

        $results = $this->model_extension_module_ozon_seller->getOrders($filter_data);

        $data['postings'] = array();

        foreach ($results as $result) {

            if ($result['shipment_date'] != '0000-00-00 00:00:00') {
                $shipment_date = date('d-m-Y', strtotime($result['shipment_date']));
            } else {
                $shipment_date = 'FBO';
            }

            $data['postings'][] = array(
                'posting_number' => $result['posting_number'],
                'status'         => $data[$result['status']],
                'shipment_date'  => $shipment_date,
            );
        }

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_posting_number'])) {
            $url .= '&filter_posting_number='.urlencode(html_entity_decode($this->request->get['filter_posting_number'],
                    ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_shipment_date'])) {
            $url .= '&filter_shipment_date='.urlencode(html_entity_decode($this->request->get['filter_shipment_date'],
                    ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status='.urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES,
                    'UTF-8'));
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page='.$this->request->get['page'];
        }

        $data['sort_posting_number'] = $this->url->link('extension/module/ozon_seller/order',
            'user_token='.$this->session->data['user_token'].'&sort=posting_number'.$url, true);
        $data['sort_status']         = $this->url->link('extension/module/ozon_seller/order',
            'user_token='.$this->session->data['user_token'].'&sort=status'.$url, true);
        $data['sort_shipment_date']  = $this->url->link('extension/module/ozon_seller/order',
            'user_token='.$this->session->data['user_token'].'&sort=sort_shipment_date'.$url, true);
        $data['sort_order']          = $this->url->link('extension/module/ozon_seller/order',
            'user_token='.$this->session->data['user_token'].'&sort=sort_order'.$url, true);

        $url = '';

        if (isset($this->request->get['filter_posting_number'])) {
            $url .= '&filter_posting_number='.urlencode(html_entity_decode($this->request->get['filter_posting_number'],
                    ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_shipment_date'])) {
            $url .= '&filter_shipment_date='.urlencode(html_entity_decode($this->request->get['filter_shipment_date'],
                    ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status='.urlencode(html_entity_decode($this->request->get['filter_status'], ENT_QUOTES,
                    'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort='.$this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order='.$this->request->get['order'];
        }

        $pagination        = new Pagination();
        $pagination->total = $order_total;
        $pagination->page  = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url   = $this->url->link('extension/module/ozon_seller/order',
            'user_token='.$this->session->data['user_token'].$url.'&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'),
            ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0,
            ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total
                    - $this->config->get('config_limit_admin'))) ? $order_total
                : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
            $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['filter_posting_number'] = $filter_posting_number;
        $data['filter_shipment_date']  = $filter_shipment_date;
        $data['filter_status']         = $filter_status;

        $data['sort']  = $sort;
        $data['order'] = $order;

        $this->response->setOutput($this->load->view('extension/module/ozon_seller_order', $data));

    }

    // Информация по заказу Озон
    public function viewOrder()
    {
        if (isset($this->request->post['posting'])) {

            if (isset($this->request->post['posting']['error'])) {
                $output_local   = '<tr data-id="'.$this->request->get['postingnumber'].'">';
                $output_product = $this->request->post['posting']['error']['message'];
            } else {
                $this->load->model('extension/module/ozon_seller');
                $this->load->model('tool/image');
                $posting  = $this->request->post['posting']['result'];
                $created  = 'Создан: '.date('d-m-Y H:i:s', strtotime($posting['created_at']));
                $city     = '<br />Город: '.$posting['analytics_data']['city'];
                $region   = '<br />Регион: '.$posting['analytics_data']['region'];
                $delivery = '<br />Доставка: '.$posting['analytics_data']['delivery_type'];
                $status   = '<br />Текущий статус: '.$posting['status'];
                if ($posting['analytics_data']['is_premium'] == 'false') {
                    $premium = '<br />Премиум: Нет';
                } else {
                    $premium = '<br />Премиум: Да';
                }

                $output_local   = '<tr data-id="'.$posting['posting_number'].'"><td></td><td>'.$created.$city.$region
                    .$delivery.$premium.$status.'</td>';
                $products       = $posting['products'];
                $output_product = '';

                foreach ($products as $product) {
                    if ($this->config->get('ozon_seller_entry_offer_id')) {
                        $prod = $this->model_extension_module_ozon_seller->getProductByModel($product['offer_id']);
                    } else {
                        $prod = $this->model_extension_module_ozon_seller->getProductBySku($product['offer_id']);
                    }

                    if (empty($prod)) {
                        $prod[0]['image'] = 'image/no_image.png';
                    }

                    if ($prod[0]['image']) {
                        $thumb = $this->model_tool_image->resize($prod[0]['image'], 50, 50);
                    } else {
                        $thumb = '';
                    }

                    if ($prod[0]['image']) {
                        $image = $this->model_tool_image->resize($prod[0]['image'], 500, 500);
                    } else {
                        $image = '';
                    }

                    $output_product .= '<a class="image-popup" href="'.$image.'"><img src="'.$thumb.'"> </a>'.' '
                        .$product['offer_id'].' - '.$product['name'].' - <b>'.$product['quantity'].' шт.</b> - '
                        .round($product['price']).' руб.<br /><br />';
                }
            }
            echo $output_local.'<td colspan="3">'.$output_product.'</td></tr>';
        }
    }

    public function getAttributeRequired()
    {

        if (isset($this->request->get['category'])) {
            $this->load->model('extension/module/ozon_seller');
            $category_id = htmlspecialchars($this->request->get['category']);
            $required    = $this->model_extension_module_ozon_seller->getAttributeRequired($category_id);
            echo json_encode($required);
        }
    }

    public function truncateAttribute()
    {

        $this->load->model('extension/module/ozon_seller');
        $this->model_extension_module_ozon_seller->truncateAttribute();
        echo 'OK';
    }

    public function downloadAct()
    {
        $check_download_act = glob(DIR_UPLOAD.date('Y-m-d').'_act_ozon_seller*.pdf');
        foreach ($check_download_act as $download_act) {
            if (file_exists($download_act) && is_file($download_act)) {
                header("Content-type: application/pdf");
                @readfile($download_act);
            }
        }
    }

    protected function validate()
    {
        if ( ! $this->user->hasPermission('modify', 'extension/module/ozon_seller')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ( ! $this->request->post['ozon_seller_client_id']) {
            $this->error['ozon_seller_client_id'] = $this->language->get('error_client_id');
        }

        if ( ! $this->request->post['ozon_seller_api_key']) {
            $this->error['ozon_seller_api_key'] = $this->language->get('error_api_key');
        }

        if ( ! $this->request->post['ozon_seller_cron_pass']) {
            $this->error['ozon_seller_cron_pass'] = $this->language->get('error_cron_pass');
        }

        if (isset($this->request->post['ozon_seller_category'])) {
            foreach ($this->request->post['ozon_seller_category'] as $ozon_seller_category) {
                if ((int)$ozon_seller_category['shop'] < 1 || (int)$ozon_seller_category['ozon'] < 1) {
                    $this->error['warning'] = $this->language->get('error_seller_category');
                    break;
                }
            }
        }

        return ! $this->error;
    }

    public function install()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('module_ozon_seller', array('module_ozon_seller_status' => 1));
        $this->load->model('extension/module/ozon_seller');
        $this->model_extension_module_ozon_seller->install();
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('module_ozon_seller', array('module_ozon_seller_status' => 0));
        $this->load->model('extension/module/ozon_seller');
        $this->model_extension_module_ozon_seller->uninstall();
    }
}

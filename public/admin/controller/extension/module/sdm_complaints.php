<?php
/**
 * Class ControllerExtensionModuleSdmComplaints
 *
 * @module   Рекомендации и жалобы
 * @author   fonclub
 * @created  17.01.2020 18:02
 */

class ControllerExtensionModuleSdmComplaints extends Controller
{

    private $error = array();

    public function index()
    {
        $data = $this->load->language('extension/module/sdm_complaints');
        $this->document->setTitle($data['heading_title']);

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('module_sdm_complaints', $this->request->post);
            $this->session->data['success'] = $data['text_success'];
            $this->response->redirect($this->url->link('extension/module/sdm_complaints',
                'user_token='.$this->session->data['user_token'], 'SSL'));

        }


        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];

            unset($this->error['warning']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'user_token='.$this->session->data['user_token'], 'SSL'),
            'separator' => false,
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('marketplace/extension', 'user_token='.$this->session->data['user_token'], 'SSL'),
            'separator' => ' :: ',
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/module/sdm_complaints', 'user_token='.$this->session->data['user_token'], 'SSL'),
            'separator' => ' :: ',
        );

        $data['action'] = $this->url->link('extension/module/sdm_complaints', 'user_token='.$this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token='.$this->session->data['user_token'], 'SSL');


        if (isset($this->request->post['module_sdm_complaints_status'])) {
            $data['module_sdm_complaints_status'] = $this->request->post['module_sdm_complaints_status'];
        } else {
            $data['module_sdm_complaints_status'] = $this->config->get('module_sdm_complaints_status');
        }
        
        if (isset($this->request->post['module_sdm_complaints_email'])) {
            $data['module_sdm_complaints_email'] = $this->request->post['module_sdm_complaints_email'];
        } else {
            $data['module_sdm_complaints_email'] = $this->config->get('module_sdm_complaints_email');
        }


        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/sdm_complaints', $data));
    }

    /**
     * @return bool
     */
    protected function validate()
    {

        // Block to check the user permission to manipulate the module
        if ( ! $this->user->hasPermission('modify', 'extension/module/sdm_complaints')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (utf8_strlen($this->request->post['module_sdm_complaints_email']) < 5) {
            $this->error['email'] = $this->language->get('error_email');
        }

        // Block returns true if no error is found, else false if any error detected
        if ( ! $this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function install()
    {
    }

    public function uninstall()
    {
    }

}
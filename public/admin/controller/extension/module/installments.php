<?php
/**
 * Class ControllerExtensionModuleInstallments
 *
 * @module   Товары в рассрочку
 * @author   fonclub
 * @created  19.12.2019 11:52
 */

class ControllerExtensionModuleInstallments extends Controller
{

    public function index()
    {
        $data = $this->load->language('extension/module/installments');
        $this->document->setTitle($data['heading_title']);

        $this->document->addScript('view/javascript/octemplates/bootstrap-notify/bootstrap-notify.min.js');
        $this->document->addScript('view/javascript/octemplates/oct_main.js');
        $this->document->addStyle('view/stylesheet/oct_ultrastore.css');

        $this->load->model('setting/module');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->get['module_id'])) {
                $this->model_setting_module->addModule('installments', $this->request->post);
            } else {
                $this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
            }
            $this->session->data['success'] = $data['text_success'];
            $this->response->redirect($this->url->link('marketplace/extension',
                'type=module&user_token='.$this->session->data['user_token'], 'SSL'));

        }

        $errors = [
            'warning',
            'name',
            'heading',
            'width',
            'height',
            'limit'
        ];

        foreach ($errors as $error) {
            if (isset($this->error[$error])) {
                $data['error_'. $error] = $this->error[$error];
            } else {
                $data['error_'. $error] = '';
            }
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['text_form'] = !isset($this->request->get['module_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'user_token='.$this->session->data['user_token'], 'SSL'),
            'separator' => false,
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('marketplace/extension', 'type=module&user_token='.$this->session->data['user_token'], 'SSL'),
            'separator' => ' :: ',
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/module/installments', 'user_token='.$this->session->data['user_token'], 'SSL'),
            'separator' => ' :: ',
        );

        if (!isset($this->request->get['module_id'])) {
            $data['action'] = $this->url->link('extension/module/installments', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('extension/module/installments', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
        }

        $data['cancel'] = $this->url->link('marketplace/extension', 'type=module&user_token=' . $this->session->data['user_token'], true);

        if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($module_info) && isset($module_info['status'])) {
            $data['status'] = $module_info['status'];
        } else {
            $data['status'] = 0;
        }
        
        if (isset($this->request->post['is_carousel'])) {
            $data['is_carousel'] = $this->request->post['is_carousel'];
        } elseif (!empty($module_info) and isset($module_info['is_carousel'])) {
            $data['is_carousel'] = $module_info['is_carousel'];
        } else {
            $data['is_carousel'] = 0;
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($module_info)) {
            $data['name'] = $module_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['heading'])) {
            $data['heading'] = $this->request->post['heading'];
        } elseif (!empty($module_info)) {
            $data['heading'] = $module_info['heading'];
        } else {
            $data['heading'] = [];
        }
        if (isset($this->request->post['width'])) {
            $data['width'] = $this->request->post['width'];
        } elseif (!empty($module_info)) {
            $data['width'] = $module_info['width'];
        } else {
            $data['width'] = 200;
        }

        if (isset($this->request->post['height'])) {
            $data['height'] = $this->request->post['height'];
        } elseif (!empty($module_info)) {
            $data['height'] = $module_info['height'];
        } else {
            $data['height'] = 200;
        }

        if (isset($this->request->post['limit'])) {
            $data['limit'] = $this->request->post['limit'];
        } elseif (!empty($module_info)) {
            $data['limit'] = $module_info['limit'];
        } else {
            $data['limit'] = 25;
        }

        if (isset($this->request->post['sort'])) {
            $data['sort'] = $this->request->post['sort'];
        } elseif (!empty($module_info)) {
            $data['sort'] = isset($module_info['sort']) ? $module_info['sort'] : 'ASC';
        } else {
            $data['sort'] = 'pd.name';
        }

        if (isset($this->request->post['order'])) {
            $data['order'] = $this->request->post['order'];
        } elseif (!empty($module_info)) {
            $data['order'] = isset($module_info['order']) ? $module_info['order'] : 'ASC';
        } else {
            $data['order'] = 'ASC';
        }

        if (isset($this->request->post['quantity_view'])) {
            $data['quantity_view'] = $this->request->post['quantity_view'];
        } elseif (!empty($module_info) && isset($module_info['quantity_view'])) {
            $data['quantity_view'] = $module_info['quantity_view'];
        } else {
            $data['quantity_view'] = 0;
        }
        
        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/installments', $data));
    }

    /**
     * @return bool
     */
    protected function validate()
    {

        // Block to check the user permission to manipulate the module
        if ( ! $this->user->hasPermission('modify', 'extension/module/installments')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if (!$this->request->post['width']) {
            $this->error['width'] = $this->language->get('error_width');
        }

        if (!$this->request->post['height']) {
            $this->error['height'] = $this->language->get('error_height');
        }

        if (!$this->request->post['limit']) {
            $this->error['limit'] = $this->language->get('error_limit');
        }

        if (is_array($this->request->post['heading'])) {
            foreach ($this->request->post['heading'] as $language_id => $heading) {
                if ((utf8_strlen($heading) < 3) || (utf8_strlen($heading) > 64)) {
                    $this->error['heading'][$language_id] = $this->language->get('error_heading');
                    $this->error['warning']                 = $this->language->get('error_heading');
                }
            }
        }

        return ! $this->error;
    }

    public function install() {
        $this->load->model('user/user_group');

        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/installments');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/installments');
    }

    public function uninstall() {
        $this->load->model('setting/setting');
        $this->load->model('user/user_group');

        $this->model_user_user_group->removePermission($this->user->getGroupId(), 'access', 'extension/module/installments');
        $this->model_user_user_group->removePermission($this->user->getGroupId(), 'modify', 'extension/module/installments');

        $this->model_setting_setting->deleteSetting('oct_products_from_category');
    }

}
<?php
class ControllerExtensionModuleImpex extends Controller {
	private $error = array();

	public function index() {
			
		$this->load->language('extension/module/impex');
		
		$this->load->model('extension/module/impex');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_impex', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', 'SSL'));
		}
		
		$data['user_token'] = $this->session->data['user_token'];

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_login'] = $this->language->get('entry_login');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_base'] = $this->language->get('entry_base');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/impex', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/impex', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/impex', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
		}
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		
		if (isset($this->request->post['module_impex_status'])) {
			$data['module_impex_status'] = $this->request->post['module_impex_status'];
		} else {
			$data['module_impex_status'] = $this->config->get('module_impex_status');
		}

		if (isset($this->request->post['module_impex_login'])) {
			$data['module_impex_login'] = $this->request->post['module_impex_login'];
		} else {
			$data['module_impex_login'] = $this->config->get('module_impex_login');
		}

		if (isset($this->request->post['module_impex_password'])) {
			$data['module_impex_password'] = $this->request->post['module_impex_password'];
		} else {
			$data['module_impex_password'] = $this->config->get('module_impex_password');
		}

		if (isset($this->request->post['module_impex_base'])) {
			$data['module_impex_base'] = $this->request->post['module_impex_base'];
		} else {
			$data['module_impex_base'] = $this->config->get('module_impex_base');
		}

		if (isset($this->request->post['module_impex_temp_ttl'])) {
			$data['module_impex_temp_ttl'] = $this->request->post['module_impex_temp_ttl'];
		} else {
			$data['module_impex_temp_ttl'] = $this->config->get('module_impex_temp_ttl');
		}
		
		if (isset($this->request->post['module_impex_stats'])) {
			$data['module_impex_stats'] = $this->request->post['module_impex_stats'];
		} else {
			$data['module_impex_stats'] = $this->config->get('module_impex_stats');
		}

		if (isset($this->request->post['module_impex_stat_ttl'])) {
			$data['module_impex_stat_ttl'] = $this->request->post['module_impex_stat_ttl'];
		} else {
			$data['module_impex_stat_ttl'] = $this->config->get('module_impex_stat_ttl');
		}
		
		
		
//		$data['categories'] = array();
//		$filter_data = array('sort'=>'fullname','order'=>'ASC');
//		$results = $this->getAllCategories($filter_data);
//		foreach ($results as $result) {
//			$level = count(explode("&and;", $result['fullname']));
//			$data['categories'][] = array(
//				'category_id' 	=> $result['category_id'], 
//				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
//				'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
//				'level' 		=> $level, 
//				'parent_id' 	=> $result['parent_id'], 
//				'product_count' => $result['product_count'], 
//				'sort_order' 	=> $result['sort_order'], 
//				'status' 		=> $result['status'] 
//			);
//		}
		
        $this->load->model('catalog/category');

        $filter_data = [
			'sort'        => 'name',
			'order'       => 'ASC'
		];

        $data['categories'] = $this->model_catalog_category->getCategories($filter_data);

        if (isset($this->request->post['module_impex_categories'])) {
            $data['module_impex_categories'] = $this->request->post['module_impex_categories'];
		} else {
			$data['module_impex_categories'] = $this->config->get('module_impex_categories');
		}
			
			
//        } elseif (!empty($module_info)) {
//            $data['module_impex_categories'] = (isset($module_info['module_impex_categories'])) ? $module_info['module_impex_categories'] : [];
//        } else {
//            $data['module_impex_categories'] = [];
//        }
		

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/impex', $data));
	}
	
	public function getAllCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, cd2.name AS name, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS fullname, c1.parent_id, c1.sort_order, c1.status,(select count(product_id) as product_count from " . DB_PREFIX . "product_to_category pc where pc.category_id = c1.category_id) as product_count FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = '1' AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.category_id";

		$sort_data = array(
			'product_count',
			'fullname',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	
	
	
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/impex')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}
	
	public function install() {
		$this->load->language('extension/module/impex');

		$this->load->model('setting/extension');

		if ($this->validate()) {
			$this->model_setting_extension->install('module', $this->request->get['extension']);

			$this->load->model('user/user_group');

			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/' . $this->request->get['extension']);
			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/' . $this->request->get['extension']);

			// Compatibility
			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'module/' . $this->request->get['extension']);
			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'module/' . $this->request->get['extension']);
			
		$this->log->write("Установка/активация модуля impex...");

		// 1С uuid - Opencart id
		$this->log->write("Проверка/создание таблицы " . DB_PREFIX . "to_impex...");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "product_to_1c`");
		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "to_impex` (
				`accord_id` int(11) NOT NULL AUTO_INCREMENT,
				`object` varchar(16) NOT NULL,
				`object_id` int(11) NOT NULL,
				`object_key` varchar(36) NOT NULL,
				`object_uid` varchar(36) NOT NULL,
				PRIMARY KEY (`accord_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8"
		);

		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "catalog_count` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`file` varchar(256) NOT NULL,
				`count` int(11) NOT NULL,
				`date_added` datetime NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8"
		);

		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "catalog_stats` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`file` varchar(256) NOT NULL,
				`author` varchar(32) NOT NULL,
				`product` varchar(128) NOT NULL,
				`delete_now` tinyint(1) NOT NULL,
				`date_added` datetime NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8"
		);

			$this->session->data['success'] = $this->language->get('text_success');
		}

	}

	public function uninstall() {
		$this->load->language('extension/module/impex');

		$this->load->model('setting/extension');

		if ($this->validate()) {
			$this->model_setting_extension->uninstall('captcha', $this->request->get['extension']);

			// Call uninstall method if it exsits
			//$this->load->controller('extension/captcha/' . $this->request->get['extension'] . '/uninstall');

			$this->session->data['success'] = $this->language->get('text_success');
		}
		
	}
	
	public function clear_to_impex() {
		$json = array();
		$total = 0;
		$deleted = 0;
		
		$accords = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "to_impex");
		foreach ($query->rows as $result) {
			$total++;
			$accords[] = array(
				'object_id' 	=> $result['object_id'],
				'object' 			=> $result['object']
			);
		}
		
		foreach ($accords as $accord) {
			if ($accord['object'] == 'product') {
				$query = $this->db->query("SELECT COUNT(product_id) AS total FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$accord['object_id'] . "'");
				//$this->log->write($query->row['total']);
				if ($query->row['total'] == 0) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$accord['object_id'] . "' AND object = 'product'");	
					$deleted++;
				}
			}
			if ($accord['object'] == 'category') {
				$query = $this->db->query("SELECT COUNT(category_id) AS total FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$accord['object_id'] . "'");
				//$this->log->write($query->row['total']);
				if ($query->row['total'] == 0) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$accord['object_id'] . "' AND object = 'category'");	
					$deleted++;
				}
			}
			if ($accord['object'] == 'manufacturer') {
				$query = $this->db->query("SELECT COUNT(manufacturer_id) AS total FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$accord['object_id'] . "'");
				//$this->log->write($query->row['total']);
				if ($query->row['total'] == 0) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$accord['object_id'] . "' AND object = 'manufacturer'");	
					$deleted++;
				}
			}
			if ($accord['object'] == 'attribute') {
				$query = $this->db->query("SELECT COUNT(attribute_id) AS total FROM " . DB_PREFIX . "attribute WHERE attribute_id = '" . (int)$accord['object_id'] . "'");
				//$this->log->write($query->row['total']);
				if ($query->row['total'] == 0) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$accord['object_id'] . "' AND object = 'attribute'");	
					$deleted++;
				}
			}
		}
		
		$json['success'] = true;
		$json['result'] = 'Проверка завершена. Всего сопоставлений = ' . $total . ' Удалено = ' . $deleted;
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function show_to_impex() {
		$json = array();
		$deleted = 0;
		
		$accords = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "to_impex WHERE object = 'product'");
		foreach ($query->rows as $result) {
			$accords[] = array(
				'object_id' 	=> $result['object_id'],
				'object_uid'	=> $result['object_uid']
			);
		}
		
		$num = 0;
		foreach ($accords as $accord) {
			$num++;
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$accord['object_id'] . "'");
			$name = $query->row['name'];
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$accord['object_id'] . "'");
			$status = $query->row['status'];
			$this->log->write($num . '^' . $name . '^' . $accord['object_uid'] . '^статус = ' . $status);
		}
		
		$json['success'] = true;
		$json['result'] = 'Вывод в журнал ошибок выполнен. Всего выведено = ' . $num . ' товаров';
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	private function log($message, $level=1) {
		
		$this->log->write(print_r($message,true));
		
	}
	
	private function echo_message($ok, $message="") {
		if ($ok) {
			echo "success\n";
			$this->log("success",2);
			if ($message) {
				echo $message;
				$this->log($message,2);
			}
		} else {
			echo "failure\n";
			$this->log("failure",2);
			if ($message) {
				echo $message;
				$this->log($message,2);
			}
		};
	}
	
	private function checkAccess($echo = false) {

		// Модуль включен?
		if ($this->config->get('module_impex_status') == '0') { //empty()
			//if ($echo) $this->echo_message(0, "Модуль обмена отключен на стороне сайта");
			echo "failure\n";
			echo "Модуль обмена отключен на стороне сайта.\n";
			$this->log->write('Модуль обмена отключен на стороне сайта.');
			return false;
		}
		// Модуль установлен?
		if (!$this->config->get('module_impex_status')) {
			//if ($echo) $this->echo_message(0, "Модуль обмена не установлен на сайте");
			echo "failure\n";
			echo "Модуль обмена не установлен на сайте.\n";
			$this->log->write('Модуль обмена не установлен на сайте.');
			return false;
		}
		
		return true;
	}
	
	private function checkAuthKey($echo = false) {

		if (!isset($this->request->cookie['key'])) {
			echo "failure";
			if ($echo) $this->echo_message(0, "no cookie key\n");
			return false;
		}
		if ($this->request->cookie['key'] != md5($this->config->get('module_impex_password'))) {
			echo "failure";
			if ($echo) $this->echo_message(0, "Session error\n");
			return false;
		}
		return true;
	}
	
	public function modeCheckauth() {
		
		if (!$this->checkAccess(true))
			exit;

		$auth_user = "";
		$auth_pw = "";

		// Определение авторизации на сервере
		if (!isset($_SERVER['PHP_AUTH_USER'])) {

			// Определяем пользователя
			if (isset($_SERVER["REMOTE_USER"])) {
				$remote_user = $_SERVER["REMOTE_USER"];
				if (isset($_SERVER["REDIRECT_REMOTE_USER"])) {
					$remote_user = $_SERVER["REMOTE_USER"] ? $_SERVER["REMOTE_USER"]: $_SERVER["REDIRECT_REMOTE_USER"];
				}
			} elseif (isset($_SERVER["REDIRECT_HTTP_AUTHORIZATION"])) {
				$remote_user = $_SERVER["REDIRECT_HTTP_AUTHORIZATION"];
			}

			// Если удалось установить пользователя, тогда раскодируем
			if (isset($remote_user)) {
				$strTmp = base64_decode(substr($remote_user,6));
				if($strTmp)
					list($auth_user, $auth_pw) = explode(':', $strTmp);
			} else {
				$this->echo_message(0, "ERROR: 1009");
				$this->log("Проверьте наличие записи в файле .htaccess в корне файла после RewriteEngine On:\nRewriteCond %{HTTP:Authorization} ^(.*)\nRewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]");
				$this->log($_SERVER, 2);
				exit;
			}

		} else {
			$auth_user = $_SERVER['PHP_AUTH_USER'];
			$auth_pw = $_SERVER['PHP_AUTH_PW'];
		}

		// Авторизуем
		if (($this->config->get('module_impex_login') != '') && ($auth_user != $this->config->get('module_impex_login'))) {
			//$this->echo_message(0, "ERROR: 1010");
			echo "failure\n";
			echo "Авторизация не выполнена. Проверьте логин и пароль.\n";
			$this->log->write('Кто-то ломится с неверными учетными данными - L: '.$auth_user.' P: '.$auth_pw);
			exit;
		}
		if (($this->config->get('module_impex_password') != '') && ($auth_pw != $this->config->get('module_impex_password'))) {
			//$this->echo_message(0, "ERROR: 1011");
			echo "failure\n";
			echo "Авторизация не выполнена. Проверьте логин и пароль.\n";
			$this->log->write('Кто-то ломится с неверными учетными данными - L: '.$auth_user.' P: '.$auth_pw);
			exit;
		}
		echo "success\n";
		echo "Авторизация пользователя выполнена.\n";
		echo "key\n";
		echo md5($this->config->get('module_impex_password')) . "\n";
		
		$this->log->write('Успешная авторизация L: '.$auth_user.' P: '.$auth_pw);
		$this->log->write('modeCheckauth');

	}
	
	public function modeInstallment() {

		if (!$this->checkAuthKey(true)) {
			//echo "failure";
			exit;
		}

		$this->log("Mode Installment", 2);
		
		echo "success\n";
		echo "exec installment started";
		
		$this->log("Обновление рассрочки - НАЧАЛО", 2);
		$this->load->controller('api/installments_cron/generate', ['api_token' => 'Rtg$fds@45dfgfKL48tyww']);
		$this->log("Обновление рассрочки - КОНЕЦ", 2);
		
	} 
	
	public function modeOcfilter() {

		if (!$this->checkAuthKey(true)) {
			exit;
		}

		$this->log("Mode Ocfilter", 2);
		
		echo "success\n";
		echo "exec ocfilter started";
		
		$this->log("Обновление фильтров - НАЧАЛО", 2);
		$this->load->controller('api/ocfilter_cron/copy_filter', ['api_token' => 'dgf34tg@45dfg45tyww']);
		$this->log("Обновление фильтров - КОНЕЦ", 2);
		
	} 
	
	public function modeCatalogInit() {

		if (!$this->checkAuthKey(true)) {
			//echo "failure";
			exit;
		}

		$result = $this->modeInit();
		echo $result[0] . "\n";
		echo $result[1] . "\n";
//$this->log($result, 2);
		$this->log("Mode Catalog Init", 2);
//$this->log($result, 2);
		//echo "sessid=" . md5($this->config->get('impex_password')) . "\n";

	} 
	
	public function modeInit() {

		$result = array();

		$zip_support = class_exists('ZipArchive') ? true : false;
		if ($zip_support) {
			//$result[0] = $this->config->get('exchange1c_file_zip') == 1 ?  "zip=yes" : "zip=no";
			$result[0] = "zip=yes";
		} else {
			$result[0] = "zip=no";
		}
//		$manual_size = $this->formatSize($this->config->get('exchange1c_file_max_size'));
		$post_max_size = $this->getPostMaxFileSize();
//		if ($this->config->get('exchange1c_file_max_size') && $manual_size <= $post_max_size) {
//			$result[1] = "file_limit=" . $manual_size;
//		} else {
//			$result[1] = "file_limit=" . $post_max_size;
//		}
		$result[1] = "file_limit=" . $post_max_size;
		
		$this->log('PHP Version: ' . PHP_VERSION_ID, 2);

		// Проверка наличия папки exchange1c в кэше
		//$cache = DIR_CACHE . 'impex/';
		$cache = DIR_APPLICATION . 'impex/';
		$result['error'] = "";
		if (!file_exists($cache)) {
			if (@mkdir($cache, 0755) === false) {
				$result['error'] = "1005";
			}
		}

		return $result;

	}
	
	private function getPostMaxFileSize() {

		$size = $this->formatSize(ini_get('post_max_size'));
		$this->log("POST_MAX_SIZE: " . $size, 2);

//		$size_max_manual = $this->formatSize($this->config->get('exchange1c_file_max_size'));
//		if ($size_max_manual) {
//			$this->log("POST_MAX_SIZE (переопределен в настройках): " . $size_max_manual, 2);
//			if ($size_max_manual < $size) {
//				$size = $size_max_manual;
//			}
//		}

		return $size;
	}

	private function formatSize($size) {
		if (empty($size)) {
			return 0;
		}
		$type = $size{strlen($size)-1};
		if (!is_numeric($type)) {
			$size = (integer)$size;
			switch ($type) {
				case 'K': $size = $size*1024;
					break;
				case 'M': $size = $size*1024*1024;
					break;
				case 'G': $size = $size*1024*1024*1024;
					break;
			}
			return $size;
		}
		return (int)$size;
	} 

	public function modeFileCatalog() {

		$error = '';
		
		$this->log('файл импорта: ' . $this->request->get['filename'], 2);
		
		$path_parts = pathinfo($this->request->get['filename']);
		$filename = $path_parts['filename'];
		$this->log('$filename = ' . $filename, 2);
		
		$this->modeFile($filename, $error);
		
		//$this->modeFile('catalog', $error);

		if ($error) {
			$this->echo_message(0, $error);
		} else {
			$this->echo_message(1, "Successfully import catalog ");
		}

	}

	private function modeFile($mode, &$error) {

		$xmlfiles = array();

		if (!$this->checkAuthKey()) exit;
		//$cache = DIR_CACHE . 'impex/';
		$cache = DIR_APPLICATION . 'impex/';
		
		$ttl = 180;
		$files = glob($cache.'*.*');
		if ($files) {
			foreach ($files as $file) {
				if (filemtime($file) < time() - $ttl * 60) {
					unlink($file);
$this->log($file . ' удален', 2);
				}
			}
		}

		// Проверяем на наличие каталога
		if(!is_dir($cache)) mkdir($cache);

		// Проверяем на наличие имени файла
		if (isset($this->request->get['filename'])) {
			$upload_file = $cache . $this->request->get['filename'];
		} else {
			$error = "modeFile(): No file name variable";
			return false;
		}

		// Проверяем XML или изображения
		if (strpos($this->request->get['filename'], 'impex_files') !== false) {
			$cache = DIR_IMAGE;
			$upload_file = $cache . $this->request->get['filename'];
			$this->checkUploadFileTree(dirname($this->request->get['filename']) , $cache);
		}

		// Проверка на запись файлов в кэш
		if (!is_writable($cache)) {
			$error = "modeFile(): The folder " . $cache . " is not writable!";
			return false;
		}

		$this->log("upload file: " . $upload_file,2);
		
		if (file_exists($upload_file)) {
				
			$this->log("удаляю предыдущую версию: " . $upload_file,2);
			
			unlink($upload_file);

		} else {
			$this->log("нет старой версии файла: " . $upload_file,2);
		}	

		// Получаем данные
		$data = file_get_contents("php://input");
		if ($data !== false) {

			// Записываем в файл
			$filesize = file_put_contents($upload_file, $data, FILE_APPEND | LOCK_EX);
			$this->log("file size: " . $filesize, 2);

			if ($filesize) {
				chmod($upload_file , 0664);

				$xmlfiles = $this->extractZip($upload_file, $error);
				if ($error) {
					$this->echo_message(0, "modeFile(): Error extract file: " . $upload_file);

//					if ($this->config->get('exchange1c_not_delete_files_after_import') != 1) {
//						$this->log("Удален файл: " . $upload_file);
//						unlink($upload_file);
//					}

					return false;
				};
			} else {
				$this->echo_message(0, "modeFile(): Error create file");
			}
		}
		else {
			$this->echo_message(0, "modeFile(): Data empty");
		}

		return $xmlfiles;

	} 
	
	public function modeFileUpload() {

		$this->log('файл импорта: ' . $this->request->get['filename'], 2);
		
		$path_parts = pathinfo($this->request->get['filename']);
		$filename = $path_parts['filename'];
		$this->log('$filename = ' . $filename, 2);
		
		if (!$this->checkAuthKey()) exit;

		$cache = DIR_APPLICATION . 'impex/cron/';
		
		if(!is_dir($cache)) mkdir($cache);
		
		//	$files = glob(DIR_TEMP.'*.xml');
//	if ($files) {
//		foreach ($files as $file) {
//			if (filemtime($file) < time() - $ttl * 60) {
//				unlink($file);
//			}
//		}
//	}


		if (isset($this->request->get['filename'])) {
			$upload_file = $cache . $this->request->get['filename'];
		} else {
			//$error = "modeFile(): No file name variable";
			return false;
		}

		if (!is_writable($cache)) {
			//$error = "modeFile(): The folder " . $cache . " is not writable!";
			return false;
		}

		$this->log("upload file: " . $upload_file,2);
		
		$data = file_get_contents("php://input");
		if ($data !== false) {

			$filesize = file_put_contents($upload_file, $data, FILE_APPEND | LOCK_EX);
			$this->log("file size: " . $filesize, 2);

			if ($filesize) {
				chmod($upload_file , 0664);
				
				$this->echo_message(1, "Файл " . $upload_file . " записан в очередь cron");
			} else {
				$this->echo_message(0, "modeFile(): Error create file");
			}
			
		} else {
			$this->echo_message(0, "modeFile(): Data empty");
		}
	}
	
	private function checkUploadFileTree($path, $curDir = null) {
		//if (!$curDir) $curDir = DIR_CACHE . 'impex/';
		if (!$curDir) $curDir = DIR_APPLICATION . 'impex/';
		foreach (explode('/', $path) as $name) {
			if (!$name) continue;
			if (file_exists($curDir . $name)) {
				if (is_dir( $curDir . $name)) {
					$curDir = $curDir . $name . '/';
					continue;
				}
				unlink ($curDir . $name);
			}
			mkdir ($curDir . $name );
			$curDir = $curDir . $name . '/';
		}
	}

	private function extractZip($zipFile, &$error) {

		$xmlFiles = array();
		$imgFiles = 0;
		//$cache = DIR_CACHE . 'impex/';
		$cache = DIR_APPLICATION . 'impex/';
		
//		$ttl = 180;
//		$files = glob($cache.'*.*');
//		if ($files) {
//			foreach ($files as $file) {
//				if (filemtime($file) < time() - $ttl * 60) {
//					unlink($file);
//$this->log($file . ' удален', 2);
//				}
//			}
//		}

		$zipArc = zip_open($zipFile);
		if (is_resource($zipArc)) {
			$this->log("Начата распаковка архива: " . $zipFile);

			while ($zip_entry = zip_read($zipArc)) {
				$name = zip_entry_name($zip_entry);
				$this->log("Имя файла: " . $name, 2);
				$pos = stripos($name, 'impex_files');

				if ($pos !== false) {
					
//$this->log("Здесь распаковываем картинки ==================== ", 2);
					
					$error = $this->extractImage($zipArc, $zip_entry, substr($name, $pos));
					if ($error) return $xmlFiles;
					$imgFiles ++;
				} else {
					$error = $this->extractXML($zipArc, $zip_entry, $name, $xmlFiles);
					if ($error) return $xmlFiles;
				}
			}

			if ($imgFiles)
				$this->log('Распаковано картинок: ' . $imgFiles);

			$this->log("Завершена распаковка архива", 2);

		} else {
			return $xmlFiles;
		}

		zip_close($zipArc);
		$this->log("Получены XML файлы:", 2);
		$this->log($xmlFiles, 2);
		return $xmlFiles;

	}

	private function extractImage($zipArc, $zip_entry, $name) {

		$error = "";

		if (substr($name, -1) == "/") {

			// проверим каталог
			if (is_dir(DIR_IMAGE . $name)) {
				//$this->log('[zip] directory exist: '.$name, 2);

			} else {
				//$this->log('[zip] create directory: '.$name, 2);
				@mkdir(DIR_IMAGE . $name, 0775) or die ($error = "Ошибка создания директории '" . DIR_IMAGE . $name . "'");
				if ($error) return $error;
				$this->log("Создана директория: '" . $name . "'", 2);
			}

		} elseif (zip_entry_open($zipArc, $zip_entry, "r")) {

			$dump = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			$this->log("Распаковка картинки: '" . $name . "'", 2);

			$error = $this->checkDirectories($name);
			if ($error) return $error;

			if (is_file(DIR_IMAGE . $name)) {
				// Если файл существует
				$size_dump = strlen($dump);
				$size_file = filesize(DIR_IMAGE . $name);

			 	if ($size_dump != $size_file) {
			 		// файл был изменен, нужно заменить
			 		$this->log("Файл '" . $name . "' изменен, старый размер " . $size_file . ", новый " . $size_dump);

					$fd = @fopen(DIR_IMAGE . $name, "wb");
					if ($fd === false) {
						return "Ошибка записи файла: '" . DIR_IMAGE . $name . "'";
					}
					fwrite($fd, $dump);
					fclose($fd);

			 	}

			} else {

				// для безопасности проверим, не является ли этот файл php
				$pos = strpos($dump, "<?php");

				if ($pos !== false) {
					$this->log("[!] ВНИМАНИЕ Файл '" . $name . "' является PHP скриптом и не будет записан!");

				} else {

					$fd = @fopen(DIR_IMAGE . $name, "wb");
					if ($fd === false) {
						return "Ошибка создания файла: " . DIR_IMAGE . $name . ", проверьте права доступа!";
					}
					fwrite($fd, $dump);
					$this->log("Создан файл: " . $name);
					fclose($fd);

					// для безопасности проверим, является ли этот файл картинкой
//					$image_info = getimagesize(DIR_IMAGE.$name);
//					if ($image_info == NULL) {
//						$this->log("[!] ВНИМАНИЕ Файл '" . $name . "' не является картинкой, и будет удален!");
//						unlink(DIR_IMAGE.$name);
//					}
				}
			}
			zip_entry_close($zip_entry);
		}

		//$this->log("Завершена распаковка картинки", 2);
		return $error;

	} 

	private function extractXML($zipArc, $zip_entry, $name, &$xmlFiles) {

		$error = "";
		$this->log("Распаковка XML,  name = " . $name, 2);
		//$cache = DIR_CACHE . 'exchange1c/';
		$cache = DIR_APPLICATION . 'impex/';
		
//		$ttl = 180;
//		$files = glob($cache.'*.*');
//		if ($files) {
//			foreach ($files as $file) {
//				if (filemtime($file) < time() - $ttl * 60) {
//					unlink($file);
//$this->log($file . ' удален', 2);
//				}
//			}
//		}

		if (substr($name, -1) == "/") {
			// это директория
			if (is_dir($cache . $name)) {
				$this->log("Каталог существует: " . $name, 2);
			} else {
				$this->log("Создание каталога: " . $name, 2);
				@mkdir($cache . $name, 0775) or die ($error = "Ошибка создания директории '" . $cache . $name . "'");
				if ($error) return $error;
			}

		} elseif (zip_entry_open($zipArc, $zip_entry, "r")) {

			$this->log($cache . $name, 2);
			$dump = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			
//$this->log("$dump: " . $dump, 2);

			// Удалим существующий файл
			if (file_exists($cache . $name)) {
				unlink($cache . $name);
				$this->log("Удален старый файл: " . $cache . $name, 2);
			}

			// для безопасности проверим, является ли этот файл XML
			$str_xml = substr($dump, 1, 35);
			$this->log($str_xml, 2);

			$path_obj = explode('/', $name);
			$filename = array_pop($path_obj);
			$path = "";

			// Если есть каталоги, нужно их создать
			foreach ($path_obj as $dir_name) {
				if (!is_dir($cache . $path . $dir_name)) {
					@mkdir($cache . $path . $dir_name, 0775) or die ($error = "Ошибка создания директории '" . $cache . $path . $dir_name . "'");
					$this->log("Создание папки: " . $dir_name, 2);
					$this->log("Путь к файлу в кэше: " . $path, 2);
				} else {
					$this->log("Папка уже существует: " . $path . $dir_name, 2);
				}
				$path .= $dir_name . "/";
			}

			if ($fd = @fopen($cache . $path . $filename, "wb")) {

				$xmlFiles[] = $path . $filename;
				$this->log("Создан файл: " . $path . $filename, 2);
				fwrite($fd, $dump);
				fclose($fd);
				
		$this->log(' ------------------- ', 2);
		$this->log($xmlFiles, 2);

			} else {

				$this->log("Ошибка создания файла и открытие на запись: " . $path . $filename);

			}

			zip_entry_close($zip_entry);
		}

		$this->log("Завершена распаковка XML", 2);
		return "";

	} 

	public function modeImport($manual = false) {

		$this->log('modeImport', 2);
		$this->log($this->request->get['filename'], 2);

//		if ($manual) $this->log("Ручная загрузка данных");
//
//		//$cache = DIR_CACHE . 'impex/';
//		$cache = DIR_APPLICATION . 'impex/';
//		if(!is_dir($cache)) mkdir($cache);
//
//		// Определим имя файла
//		if ($manual)
//
//			$importFile = $manual;
//
//		elseif (isset($this->request->get['filename']))
//
//			$importFile = $cache . $this->request->get['filename'];
//
//		else {
//
//			if (!$manual) $this->echo_message(0, "No import file name");
//
//			// Удалим файл
////			if ($this->config->get('exchange1c_not_delete_files_after_import') != 1) {
//				@unlink($importFile);
//				$this->log("Удален файл: " . $importFile, 2);
////			}
//
//			return "ERROR: 1002";
//
//		}
		
		$cache = DIR_APPLICATION . 'impex/';
		if(!is_dir($cache)) mkdir($cache);
		
		$importFile = $cache . $this->request->get['filename'];
		
		$this->log($importFile, 2);
		$this->log("------------", 2);

		$this->load->model('extension/module/impex');

		$this->log("Тип импорта: " . $this->detectFileType($importFile), 2);
		$this->log("------------", 2);
		
		// Загружаем файл
		$error = $this->model_extension_module_impex->importFile($importFile, $this->detectFileType($importFile));
		
		$this->log("Импорт файла: " . $importFile, 2);

		if ($error) {

			if (!$manual) {
				$this->echo_message(0, 'ERROR: ' . $error);
			}

			$this->log("Ошибка при загрузке файла: " . $importFile);

			return $error;

		} else {
			if (!$manual) {
				$this->echo_message(1, "Successfully processed file: " . $importFile);
			}
		}
		
//$this->log("Обновление фильтров и рассрочек", 2);
//		
//		$this->log("Начианем обновление фильтров", 2);
//		/** вызов импорта фильтров в ocfilter */
//    //$this->load->controller('api/ocfilter_cron/copy_filter', ['api_token' => 'dgf34tg@45dfg45tyww']);
//		
//		$this->log("Начианем обновление рассрочки", 2);
//    /** генерация товаров с рассрочкой */
//    //$this->load->controller('api/installments_cron/generate', ['api_token' => 'Rtg$fds@45dfgfKL48tyww']);
		
		return "";
	}

	public function detectFileType($fileName) {

		$types = array('import', 'category', 'manufacturer', 'attribute', 'reference', 'catalog', 'fastcatalogfull', 'fastcatalogshort', 'stockprice', 'faststockprice', 'content');
		foreach ($types as $type) {
			$pos = stripos($fileName, $type);
			if ($pos !== false)
				return $type;
		}
		return '';
	}

	private function checkDirectories($name) {

		$path = DIR_IMAGE;
		$dir = explode("/", $name);
		for ($i = 0; $i < count($dir)-1; $i++) {
			$path .= $dir[$i] . "/";
			if (!is_dir($path)) {
				$error = "";
				@mkdir($path, 0775) or die ($error = "Ошибка создания директории '" . $path . "'");
				if ($error)
					return $error;
				$this->log("Создана директория: " . $path, 2);
			}
		}
		return "";
	}

}
<?php

/**
 * Class ControllerExtensionModulePopupPartner
 * @author fonclub
 * @created 07.04.2020        
 */

class ControllerExtensionModulePopupPartner extends Controller {
    private $error = [];
    
    public function index() {
        $this->load->language('extension/module/popup_partner');
        $this->document->addScript('view/javascript/octemplates/bootstrap-notify/bootstrap-notify.min.js');
		$this->document->addScript('view/javascript/octemplates/oct_main.js');
		$this->document->addStyle('view/stylesheet/oct_ultrastore.css');
		
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');
		
        $this->load->model('localisation/language');

		$popup_partner_page_info = $this->model_setting_setting->getSetting('popup_partner');

		if (!$popup_partner_page_info) {
			$this->response->redirect($this->url->link('extension/module/popup_partner/install', 'user_token=' . $this->session->data['user_token'], true));
        }
		
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('popup_partner', $this->request->post);
            
            $this->session->data['success'] = $this->language->get('text_success');
            
			$this->response->redirect($this->url->link('extension/module/popup_partner', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }
        
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        ];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/popup_partner', 'user_token=' . $this->session->data['user_token'], true)
		];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
		
		if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
		
        $data['action'] = $this->url->link('extension/module/popup_partner', 'user_token=' . $this->session->data['user_token'], true);
        $data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        $data['user_token'] = $this->session->data['user_token'];
        
        if (isset($this->error['notify_email'])) {
            $data['error_notify_email'] = $this->error['notify_email'];
        } else {
            $data['error_notify_email'] = '';
        }
        
        if (isset($this->request->post['popup_partner_status'])) {
            $data['popup_partner_status'] = $this->request->post['popup_partner_status'];
        } else {
            $data['popup_partner_status'] = $this->config->get('popup_partner_status');
        }
        
        if (isset($this->request->post['popup_partner_data'])) {
            $data['popup_partner_data'] = $this->request->post['popup_partner_data'];
        } else {
            $data['popup_partner_data'] = $this->config->get('popup_partner_data');
        }
        
        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');
        
		$this->response->setOutput($this->load->view('extension/module/popup_partner', $data));
    }
    
    
    
    public function install() {
        $this->load->language('extension/module/popup_partner');
        $this->load->model('setting/setting');
		$this->load->model('user/user_group');
        
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/popup_partner');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/popup_partner');
        
        
        $this->model_setting_setting->editSetting('popup_partner', [
            'popup_partner_status' => '1',
            'popup_partner_data' => [
                'notify_status' => '1',
                'notify_email' => $this->config->get('config_email')
            ]
        ]);
        
        $this->session->data['success'] = $this->language->get('text_success_install');
    }
    
    
    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/popup_partner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        
        foreach ($this->request->post['popup_partner_data'] as $key => $field) {
            if (empty($field) && $key == "notify_email") {
                $this->error['notify_email'] = $this->language->get('error_notify_email');
            }
        }
        
        return !$this->error;
    }
}
<?php
class ControllerExtensionTotalUds extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('extension/total/uds');
			
        $this->document->setTitle($this->language->get('heading_title'));
			
        $this->load->model('setting/setting');
		$this->load->model('localisation/order_status');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('total_uds', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
					
            $this->response->redirect($this->url->link('extension/total/uds', 'user_token=' . $this->session->data['user_token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');
			
		$data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();
        array_push($data['breadcrumbs'],
            array( 
                'text' => $this->language->get('text_home'),
				'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=total', true)            
				),
            array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('extension/total/uds', 'user_token=' . $this->session->data['user_token'], 'SSL')
            )
        );

		$data['action'] = $this->url->link('extension/total/uds', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=total', true);
			
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
	
		if (isset($this->error['complete_status'])) {
			$data['error_complete_status'] = $this->error['complete_status'];
		} else {
			$data['error_complete_status'] = '';
		}
			
		if (isset($this->error['refund_status'])) {
			$data['error_refund_status'] = $this->error['refund_status'];
		} else {
			$data['error_refund_status'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

        if (isset($this->request->post['total_uds_status'])) {
            $data['total_uds_status'] = $this->request->post['total_uds_status'];
        } else {
            $data['total_uds_status'] = $this->config->get('total_uds_status');
        }
			
		if (isset($this->request->post['total_uds_sort_order'])) {
			$data['total_uds_sort_order'] = $this->request->post['total_uds_sort_order'];
		} else {
			$data['total_uds_sort_order'] = $this->config->get('total_uds_sort_order');
		}

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['total_uds_complete_status'])) {
			$data['total_uds_complete_status'] = $this->request->post['total_uds_complete_status'];
		} elseif ($this->config->get('total_uds_complete_status')) {
			$data['total_uds_complete_status'] = $this->config->get('total_uds_complete_status');
		} else {
			$data['total_uds_complete_status'] = array();
		}
	
		if (isset($this->request->post['total_uds_refund_status'])) {
			$data['total_uds_refund_status'] = $this->request->post['uds_refund_status'];
		} elseif ($this->config->get('total_uds_refund_status')) {
			$data['total_uds_refund_status'] = $this->config->get('total_uds_refund_status');
		} else {
			$data['total_uds_refund_status'] = array();
		}
	
		if (isset($this->request->post['total_uds_company_id'])) {
			$data['total_uds_company_id'] = $this->request->post['total_uds_company_id'];
		} else {
			$data['total_uds_company_id'] = $this->config->get('total_uds_company_id');
		}

		if (isset($this->request->post['total_uds_api_key'])) {
			$data['total_uds_api_key'] = $this->request->post['total_uds_api_key'];
		} else {
			$data['total_uds_api_key'] = $this->config->get('total_uds_api_key');
		}

		if (isset($this->request->post['total_uds_cashier_id'])) {
			$data['total_uds_cashier_id'] = $this->request->post['total_uds_cashier_id'];
		} else {
			$data['total_uds_cashier_id'] = $this->config->get('total_uds_cashier_id');
		}

		if (isset($this->request->post['total_uds_cashier_name'])) {
			$data['total_uds_cashier_name'] = $this->request->post['total_uds_cashier_name'];
		} else {
			$data['total_uds_cashier_name'] = $this->config->get('total_uds_cashier_name');
		}

		if (isset($this->request->post['total_uds_app_link'])) {
			$data['total_uds_app_link'] = $this->request->post['total_uds_app_link'];
		} else {
			$data['total_uds_app_link'] = $this->config->get('total_uds_app_link');
		}

		if (isset($this->request->post['total_uds_shop_link'])) {
			$data['total_uds_shop_link'] = $this->request->post['total_uds_shop_link'];
		} else {
			$data['total_uds_shop_link'] = $this->config->get('total_uds_shop_link');
		}

		if (isset($this->request->post['total_uds_shipping_status'])) {
			$data['total_uds_shipping_status'] = $this->request->post['total_uds_shipping_status'];
		} else {
			$data['total_uds_shipping_status'] = $this->config->get('total_uds_shipping_status');
		}

		if (isset($this->request->post['total_uds_discount_status'])) {
			$data['total_uds_discount_status'] = $this->request->post['total_uds_discount_status'];
		} else {
			$data['total_uds_discount_status'] = $this->config->get('total_uds_discount_status');
		}

		if (isset($this->request->post['total_uds_block_css'])) {
			$data['total_uds_block_css'] = $this->request->post['total_uds_block_css'];
		} else {
			$data['total_uds_block_css'] = $this->config->get('total_uds_block_css');
		}
		
		if (isset($this->request->post['total_uds_license_server'])) {
			$data['total_uds_license_server'] = $this->request->post['total_uds_license_server'];
		} else {
			$data['total_uds_license_server'] = $this->config->get('total_uds_license_server');
		}
		
		if (isset($this->request->post['total_uds_license_key'])) {
			$data['total_uds_license_key'] = $this->request->post['total_uds_license_key'];
		} else {
			$data['total_uds_license_key'] = $this->config->get('total_uds_license_key');
		}
		
		if (isset($this->request->post['total_uds_expert_mode'])) {
			$data['total_uds_expert_mode'] = $this->request->post['total_uds_expert_mode'];
		} else {
			$data['total_uds_expert_mode'] = $this->config->get('total_uds_expert_mode');
		}
		
		if (isset($this->request->post['total_uds_empty_stock'])) {
			$data['total_uds_empty_stock'] = $this->request->post['total_uds_empty_stock'];
		} else {
			$data['total_uds_empty_stock'] = $this->config->get('total_uds_empty_stock');
		}
		

		if (isset($this->request->post['total_uds_block_html'])) {
			$data['total_uds_block_html'] = $this->request->post['total_uds_block_html'];
		} else {
			$data['total_uds_block_html'] = $this->config->get('total_uds_block_html');
		}
		
//		$data['total_uds_block_html'] = str_replace ( '{{ total_uds_app_link }}' , $data['total_uds_app_link'] , $data['total_uds_block_html'] );
//		$data['total_uds_block_html'] = str_replace ( '{{ total_uds_shop_link }}' , $data['total_uds_shop_link'] , $data['total_uds_block_html'] );
		
		//$data['view_block_html'] = html_entity_decode($this->config->get('total_uds_block_html'));
		$data['view_block_html'] = html_entity_decode($data['total_uds_block_html']);
		
		$data['view_block_html'] = str_replace ( '{{ total_uds_app_link }}' , $data['total_uds_app_link'] , $data['view_block_html'] );
		$data['view_block_html'] = str_replace ( '{{ total_uds_shop_link }}' , $data['total_uds_shop_link'] , $data['view_block_html'] );
		
		$data['view_block_css'] = $this->config->get('total_uds_block_css');
		
//$this->log->write($data);		

		$data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/total/uds', $data));
    }
	
	public function setLicense() {
		$json = array();
		
		$this->load->model('setting/setting');
		$this->model_setting_setting->editSettingValue('total_uds', 'total_uds_license_server', $this->request->post['total_uds_license_server']);
		$this->model_setting_setting->editSettingValue('total_uds', 'total_uds_license_key', $this->request->post['total_uds_license_key']);
		
		//$json['success']['text'] = 'Лицензия обновлена.';
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    private function validate() {
        if (!$this->user->hasPermission('modify', 'extension/total/uds')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

		if (!isset($this->request->post['total_uds_complete_status'])) {
			$this->error['complete_status'] = $this->language->get('error_complete_status');
		}
	
		if (!isset($this->request->post['total_uds_refund_status'])) {
			$this->error['refund_status'] = $this->language->get('error_refund_status');
		}
			
        return !$this->error;
    }
	
	public function install() {
        $this->load->model('setting/setting');
        $this->load->model('setting/extension');
        $this->load->model('user/user_group');

        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/total/uds');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/total/uds');

        $this->load->model('setting/event');

		$this->model_setting_event->addEvent('uds_add_orderhistory_before', 'catalog/model/checkout/order/addOrderHistory/before', 'extension/total/uds/eventAddOrderHistoryBefore');
		$this->model_setting_event->addEvent('uds_add_orderhistory_after', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/uds/eventAddOrderHistoryAfter');

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "uds_order` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`order_id` int(11) NOT NULL,
				`order_status_id` int(11) NOT NULL,
				`uds_type` varchar(8) NOT NULL,
				`uds_customer_id` bigint(13) NOT NULL,
				`uds_phone` varchar(12) NOT NULL,
				`uds_operation_code` varchar(36) NOT NULL,
				`uds_operation_id` int(11) NOT NULL,
				`uds_order_total` decimal(15,2) NOT NULL,
				`uds_skip_loyalty_total` decimal(15,2) NOT NULL,
				`uds_points` decimal(15,2) NOT NULL,
				`uds_cash` decimal(15,2) NOT NULL,
				`uds_cashback` decimal(15,2) NOT NULL,
				`comment` text NOT NULL,
				`date_added` datetime NOT NULL,
				`date_modified` datetime NOT NULL,
  				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
			//) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
			
        $this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "uds_customer` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`customer_id` int(11) NOT NULL,
				`uds_customer_id` bigint(13) NOT NULL,
				`uds_customer_uid` varchar(36) NOT NULL,
				`uds_display_name` varchar(128) NOT NULL,
				`email` varchar(96) NOT NULL,
				`uds_phone` varchar(12) NOT NULL,
				`status` tinyint(1) NOT NULL,
				`date_added` datetime NOT NULL,
				`date_modified` datetime NOT NULL,
  				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
			//) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "uds_product` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`type` varchar(12) NOT NULL,
				`oc_id` int(11) NOT NULL,
				`uds_id` bigint(13) NOT NULL,
				`nodeId` bigint(13) NOT NULL,
				`externalId` varchar(64) NOT NULL,
				`oc_name` varchar(128) NOT NULL,
				`uds_name` varchar(128) NOT NULL,
				`status` tinyint(1) NOT NULL,
				`hidden` tinyint(1) NOT NULL,
				`blocked` tinyint(1) NOT NULL,
				`date_added` datetime NOT NULL,
				`date_modified` datetime NOT NULL,
  				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
			//) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

	}

	public function uninstall() {
		$this->load->model('setting/setting');
		$this->load->model('setting/extension');
        
		//$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code` = 'total_uds'");

        $this->load->model('setting/event');
        $this->model_setting_event->deleteEventByCode('uds_add_orderhistory_before');
        $this->model_setting_event->deleteEventByCode('uds_add_orderhistory_after');
		
	}

// Удалить

    public function checkLicenseServer() {
			
		$json = array();
		
		$license_server = $this->request->post['license_server']; // $this->config->get('total_uds_license_server');
		$status = $this->getHTTPResponseStatusCode($license_server . '/uds/'); // false;
		
		$this->log->write('status = ' . $status);
		
		if ($status == 200) {
			$json['success']['alert'] = 'Указанный сервер лицензирования ' . $license_server . ' доступен.';
			$json['success']['server'] = 'Указанный сервер лицензирования ' . $license_server . ' доступен.';
		} else {
			$json['error']['alert'] = 'Указанный сервер лицензирования ' . $license_server . ' недоступен.';
			$json['error']['text'] = 'Указанный сервер лицензирования ' . $license_server . ' недоступен.';
			$json['error']['errorCode'] = 'Укажите правильный адрес сервера лицензирования.';
			$json['error']['message'] = 'В случае затруднений обратитесь в техническую поддержку.';
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	
	private function getHTTPResponseStatusCode($url) {
		$status = null;
	
		$headers = @get_headers($url, 1);
		if (is_array($headers)) {
			$status = substr($headers[0], 9, 3);
		}
	
		return $status;
	}
	
}
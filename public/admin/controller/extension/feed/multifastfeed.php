<?php
class ControllerExtensionFeedMultiFastfeed extends Controller {
    
	private $error = array();
	private $eol = "\n";

	public function index() {
		$this->load->language('extension/feed/multifastfeed');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('extension/feed/multifastfeed');
        $this->document->addStyle('view/stylesheet/oct_ultrastore.css');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('feed_multifastfeed', $this->request->post);
			
//$this->log->write('POST : ' . $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			
			if ($this->request->post['apply']) {
				$this->response->redirect($this->url->link('extension/feed/multifastfeed', 'user_token=' . $this->session->data['user_token'], true));
			}

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=feed', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=feed', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/feed/multifastfeed', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/feed/multifastfeed', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=feed', true);

		$data['user_token'] = $this->session->data['user_token'];

		$data['feed_multifastfeed_status'] = isset($this->request->post['feed_multifastfeed_status']) ? $this->request->post['feed_multifastfeed_status'] : $this->config->get('feed_multifastfeed_status');
		
		$data['feed_multifastfeed_temp_ttl'] = isset($this->request->post['feed_multifastfeed_temp_ttl']) ? $this->request->post['feed_multifastfeed_temp_ttl'] : $this->config->get('feed_multifastfeed_temp_ttl');
		
		$data['feed_multifastfeed_batch'] = isset($this->request->post['feed_multifastfeed_batch']) ? $this->request->post['feed_multifastfeed_batch'] : $this->config->get('feed_multifastfeed_batch');
		
		$data['manual_url'] = HTTPS_CATALOG . 'multiyamfeed/index.php';
		$data['feed_url'] = HTTPS_CATALOG;
		
		$data['settings'] = array();
		$results = $this->model_extension_feed_multifastfeed->getSettings();
		foreach ($results as $result) {
			$data['settings'][] = array(
				'setting_id'  		=> $result['setting_id'],
				'name'       			=> $result['name'],
				'description'			=> $result['description'],
				'is_default'			=> $result['is_default'],
				'filename'   			=> $result['filename'],
				'token'   				=> $result['token'],
				'url'   					=> $result['url'],
				'status'     			=> $result['status']
			);
		}
		
		
		$data['categories'] = array();
		$filter_data = array('sort'=>'fullname','order'=>'ASC');
		$results = $this->model_extension_feed_multifastfeed->getAllCategories($filter_data);
		foreach ($results as $result) {
			$level = count(explode("&gt;", $result['fullname']));
			$data['categories'][] = array(
				'category_id' 	=> $result['category_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
				'level' 		=> $level, 
				'parent_id' 	=> $result['parent_id'], 
				'product_count' => $result['product_count'], 
				'sort_order' 	=> $result['sort_order'], 
				'status' 		=> $result['status'] 
			);
		}
		
		$data['manufacturers'] = array();
		$filter_data = array('sort'=>'name','order'=>'ASC');
		$results = $this->model_extension_feed_multifastfeed->getManufacturers($filter_data);
		foreach ($results as $result) {
			$data['manufacturers'][] = array(
				'manufacturer_id' 	=> $result['manufacturer_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}

        $data['suppliers'] = [];
        $results = $this->model_extension_feed_multifastfeed->getSuppliers();
        foreach ($results as $result) {
            $data['suppliers'][] = array(
                'name'        	=> strip_tags(html_entity_decode($result['supplier_main'], ENT_QUOTES, 'UTF-8'))
            );
        }
		
		
		
		$page = 1;
		$data['products'] = array();		
		$filter_data = array(
			'filter_name'	  				=> null,//$filter_name,
			'filter_model'	  			=> null,//$filter_model,
			'filter_price'	  			=> null,//$filter_price,
			'filter_price_from'	  	=> null,//$filter_price,
			'filter_price_to'	  		=> null,//$filter_price,
			'filter_quantity_from' 	=> null,//$filter_quantity,
			'filter_quantity_to' 		=> null,//$filter_quantity,
			'filter_quantity' 			=> null,//$filter_quantity,
      'filter_category' 			=> null,//$filter_category,
      'filter_manufacturer' 	=> null,//$filter_category,
			'filter_status'   			=> null,//$filter_status,
			'filter_yam_status'   	=> null,//$filter_yam_status,
			'sort'            			=> 'pd.name',//$sort,
			'order'           			=> 'ASC',//$order,
			'start'           			=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           			=> $this->config->get('config_limit_admin')
		);
		
		$product_total = $this->model_extension_feed_multifastfeed->getTotalProducts($filter_data);

		$results = $this->model_extension_feed_multifastfeed->getProductArr($filter_data);
		
		foreach ($results as $result) {

			$special = false;

			$product_specials = $this->model_extension_feed_multifastfeed->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $this->currency->format($product_special['price'], $this->config->get('config_currency'));

					break;
				}
			}

			$data['products'][] = array(
				'product_id' 	=> $result['product_id'],
				'model'      	=> $result['model'],
				'name'       	=> $result['name'],
//				'yam_status'	=> $result['yam_status'],
//				'sales_note'	=> $result['sales_note'],
//				'delivery_option'	=> $result['delivery_option'],
				'price'      	=> $result['price'],
				'special'    	=> $special,
				'quantity'   	=> $result['quantity'],
				'status'     	=> $result['status']
			);
		}
		
		$paginator = $this->getPaginator($product_total, $page);
		$data['pagination'] = $paginator['paginator'];
		$data['results'] = $paginator['results'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/feed/multifastfeed', $data));
	}
	
	public function getSetting() {
		$json = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		
		$setting_info = $this->model_extension_feed_multifastfeed->getSetting($this->request->post['setting']);
		
		if ($setting_info) {
			
			$total_setting_products = $this->model_extension_feed_multifastfeed->getSettingProductTotal($this->request->post['setting']);
			//$total_setting_products = 1234; // $this->model_marketing_promo_product->getTotalPromoProducts($this->request->post['coupon_id']);
			
			$json['success'] = 'Получены параметры настройки фида ' . $setting_info['name'];
			$json['setting_info'] = $setting_info;
			
			$json['total_setting_products'] = $total_setting_products;
			
		} else {
			$json['error'] = 'Не удалось получить настройки фида id=' . $this->request->post['setting'];
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function saveSetting() {
		$json = array();
		
	//$this->log->write($this->request->post);	
		
		$this->load->model('extension/feed/multifastfeed');
		
        $setting_info                        = [];
        $setting_info['name']                = $this->request->post['name'];
        $setting_info['description']         = $this->request->post['description'];
        $setting_info['status']              = $this->request->post['status'];
        $setting_info['is_default']          = $this->request->post['is_default'];
        $setting_info['filename']            = $this->request->post['filename'];
        $setting_info['token']               = $this->request->post['token'];
        $setting_info['sales_note']          = $this->request->post['sales_note'];
        $setting_info['delivery_option']     = $this->request->post['delivery_option'];
        $setting_info['type']                = $this->request->post['type'];
        $setting_info['auto_settings']       = $this->request->post['auto_settings'] ?? [];
        $setting_info['additional_settings'] = $this->request->post['additional_settings'] ?? [];

		if ($this->request->post['setting'] == 0) {
			$result = $this->model_extension_feed_multifastfeed->addSetting($setting_info);
		} else {
			$result = $this->model_extension_feed_multifastfeed->editSetting($this->request->post['setting'],$setting_info);
		}
		
		if ($result) {
			$json['success'] = 'Успешно записаны настройки фида ' . $this->request->post['name'];
			$json['setting'] = $result;
		} else {
			$json['error'] = 'Не удалось записать настройки фида ' . $this->request->post['name'];
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function deleteSetting() {
		$json = array();
		
	//$this->log->write($this->request->post);	
		
		$this->load->model('extension/feed/multifastfeed');
		
		$result = $this->model_extension_feed_multifastfeed->deleteSetting($this->request->post['setting']);
		
		if ($result) {
			$json['success'] = 'Успешно удалена настройки фида';
		} else {
			$json['error'] = 'Не удалось удалить настройку фида';
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getProducts() {
		$json = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		if ($this->request->post['by_setting'] == '0') {
			$filter_by_setting = null;	
		}else{
			$filter_by_setting = $this->request->post['by_setting'];	
		}
		
		if ($this->request->post['setting_id'] == '0') {
			$filter_setting_id = null;	
		}else{
			$filter_setting_id = $this->request->post['setting_id'];	
		}
		
		if ($this->request->post['filter_name'] == '') {
			$filter_name = null;	
		}else{
			$filter_name = $this->request->post['filter_name'];	
		}
		
		if ($this->request->post['filter_category'] == '*') {
			$filter_category = null;	
		}else{
			$filter_category = $this->request->post['filter_category'];	
		}
		
		if ($this->request->post['filter_manufacturer'] == '*') {
			$filter_manufacturer = null;	
		}else{
			$filter_manufacturer = $this->request->post['filter_manufacturer'];	
		}
		
		if ($this->request->post['filter_price_from'] == '') {
			$filter_price_from = null;	
		}else{
			$filter_price_from = $this->request->post['filter_price_from'];	
		}
		
		if ($this->request->post['filter_price_to'] == '') {
			$filter_price_to = null;	
		}else{
			$filter_price_to = $this->request->post['filter_price_to'];	
		}
		
		if ($this->request->post['filter_quantity_from'] == '') {
			$filter_quantity_from = null;	
		}else{
			$filter_quantity_from = $this->request->post['filter_quantity_from'];	
		}
		
		if ($this->request->post['filter_quantity_to'] == '') {
			$filter_quantity_to = null;	
		}else{
			$filter_quantity_to = $this->request->post['filter_quantity_to'];	
		}
		
		if ($this->request->post['filter_sales_note'] == '*') {
			$filter_sales_note = null;	
		}else{
			$filter_sales_note = $this->request->post['filter_sales_note'];	
		}
		
		if ($this->request->post['filter_delivery_option'] == '*') {
			$filter_delivery_option = null;	
		}else{
			$filter_delivery_option = $this->request->post['filter_delivery_option'];	
		}
		
		if ($this->request->post['filter_status'] == '*') {
			$filter_status = null;	
		}else{
			$filter_status = $this->request->post['filter_status'];	
		}
		
		if ($this->request->post['filter_yam_status'] == '*') {
			$filter_yam_status = null;	
		}else{
			$filter_yam_status = $this->request->post['filter_yam_status'];	
		}
		
		$page = $this->request->post['page'];
		
		$page_products = '';		
		$filter_data = array(
			'filter_by_setting'	  		=> $filter_by_setting,
			'filter_setting_id'	  		=> $filter_setting_id,
			'filter_name'	  					=> $filter_name,
			'filter_yam_status'   		=> $filter_yam_status,
			'filter_sales_note'	  		=> $filter_sales_note,
			'filter_delivery_option'	=> $filter_delivery_option,
			'filter_price_from'	  		=> $filter_price_from,
			'filter_price_to'	  			=> $filter_price_to,
			'filter_quantity_from' 		=> $filter_quantity_from,
			'filter_quantity_to' 			=> $filter_quantity_to,
      'filter_category' 				=> $filter_category,
      'filter_manufacturer' 		=> $filter_manufacturer,
			'filter_status'   				=> $filter_status,
			'sort'            				=> 'pd.name',//$sort,
			'order'           				=> 'ASC',//$order,
			'start'           				=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           				=> $this->config->get('config_limit_admin')
		);
		
		$product_total = $this->model_extension_feed_multifastfeed->getTotalProducts($filter_data);

		$results = $this->model_extension_feed_multifastfeed->getProductArr($filter_data);
		
//$this->log->write('---------------');
//$this->log->write(print_r($results,1));
		
		foreach ($results as $result) {

			$special 					= false;
			$product_data			= false;
			$sales_note   		= '';
			$delivery_option  = '';
			$yam_status   		= 0;

			$product_specials = $this->model_extension_feed_multifastfeed->getProductSpecials($result['product_id']);
			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $this->currency->format($product_special['price'], $this->config->get('config_currency'));
					break;
				}
			}
			
			$product_data = $this->model_extension_feed_multifastfeed->getProductData($filter_setting_id, $result['product_id']);
			
//$this->log->write($product_data);			
			
			
			if ($product_data) {
				//$sales_note   			= html_entity_decode($product_data['sales_note'], ENT_QUOTES, 'UTF-8');
				//$delivery_option   	= html_entity_decode($product_data['delivery_option'], ENT_QUOTES, 'UTF-8');
				$sales_note   = str_replace ('"', '&quot;', trim($product_data['sales_note']));
				$delivery_option   = str_replace ('"', '&quot;', trim($product_data['delivery_option']));
				$yam_status   			= $product_data['yam_status'];
			}

			$product_id   = $result['product_id'];
			$model       	= str_replace ('"', '&quot;', trim($result['model']));
			$name       	= str_replace ('"', '&quot;', trim($result['name']));
			$price      	= $result['price'];
			$special    	= $special;
			$quantity   	= $result['quantity'];
			$status     	= $result['status'];
			
			
			$page_products .= '{"product_id":"'.$product_id.'","model":"'.$model.'","name":"'.$name.'","sales_note":"'.$sales_note.'","delivery_option":"'.$delivery_option.'","price":"'.$price.'","special":"'.$special.'","quantity":"'.$quantity.'","status":"'.$status.'","yam_status":"'.$yam_status.'"},';
			
		}
		
		$page_products = trim($page_products, ",");
		$page_products = '['.$page_products.']';
		
		$paginator = $this->getPaginator($product_total, $page);
		$json['success']['pagination'] = $paginator['paginator'];
		$json['success']['results'] = $paginator['results'];
		
		$json['success']['page_products'] = $page_products;
		$json['success']['count'] = $product_total;
		
		if ($product_total == 0) {
			$json['success']['text'] = 'Запрос выполнен. Пустой результат.';
		} else {
			$json['success']['text'] = 'Запрос выполнен. Загружена страница ' . $page . '.';
		}
		
//$this->log->write('json = ' . print_r($json,1));
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getPaginator($total, $page) {
		
		$limit = $this->config->get('config_limit_admin');
		$num_links = 8;
		$text_first = '|&lt;';
		$text_last = '&gt;|';
		$text_next = '&gt;';
		$text_prev = '&lt;';
		
		$paginator = array();
		
		if ($page < 1) {
			$page = 1;
		}
		if (!(int)$limit) {
			$limit = 20;
		}
		$num_pages = ceil($total / $limit);
		$output = '<ul class="paginator">';
		if ($page > 1) {
			$output .= '<li id="page-' . '1'. '" class="page">' . $text_first . '</li>';
			if ($page - 1 === 1) {
				$output .= '<li id="page-' . $page . '" class="page">' . $text_prev . '</li>';
			} else {
				$output .= '<li id="page-' . ($page - 1) . '" class="page">' . $text_prev . '</li>';
			}
		}
		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);
				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}
			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<li id="page-' . $i . '" class="active">' . $i . '</li>';
				} else {
					$output .= '<li id="page-' . $i . '" class="page">' . $i . '</li>';
				}
			}
		}
		if ($page < $num_pages) {
			$output .= '<li id="page-' . ($page + 1) . '" class="page">' . $text_next . '</li>';
			$output .= '<li id="page-' . $num_pages . '" class="page">' . $text_last . '</li>';
		}
		$output .= '</ul>';
		
		if ($num_pages > 1) {
			$paginator['paginator'] = $output;
		} else {
			$paginator['paginator'] = '';
		}
		
			$paginator['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit), $total, ceil($total / $limit));
		
			return $paginator;
	}
	
	public function setSalesNotes() {
		$json = array();
		
		$arr_products = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$arr_products = explode(',', $this->request->post['multifastfeed_products']);
		$count_products = count($arr_products);

		$result = $this->model_extension_feed_multifastfeed->setSalesNotes($this->request->post['setting_id'], $arr_products, $this->request->post['sales_note']);
		
		$json['success']['text'] = 'Установлено новое значение sales_note для ' . $count_products . ' товаров.';
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function setDeliveryOptions() {
		$json = array();
		
		$arr_products = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$arr_products = explode(',', $this->request->post['multifastfeed_products']);
		$count_products = count($arr_products);

		$result = $this->model_extension_feed_multifastfeed->setDeliveryOptions($this->request->post['setting_id'], $arr_products, $this->request->post['delivery_option']);
		
		$json['success']['text'] = 'Установлено новое значение delivery_option для ' . $count_products . ' товаров.';
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function setStatus() {
		$json = array();
		
		$arr_products = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$arr_products = explode(',', $this->request->post['multifastfeed_products']);
		$count_products = count($arr_products);

		$result = $this->model_extension_feed_multifastfeed->setStatus($this->request->post['setting_id'], $arr_products, $this->request->post['mode']);
		
		$json['success']['text'] = 'Установлено новое значение статуса К вырузке для ' . $count_products . ' товаров.';
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
//	public function setYamStatus() {
//		$json = array();
//		
//		$arr_products = array();
//		
//		$this->load->model('extension/feed/multifastfeed');
//		
//		$arr_products[] = $this->request->post['product_id'];
//		$count_products = count($arr_products);
//
//		$result = $this->model_extension_feed_multifastfeed->setStatus($this->request->post['product_id'], $this->request->post['mode']);
//		
//		$json['success']['text'] = 'Установлено новое значение yam_status для ' . $count_products . ' товаров.';
// 
//		$this->response->addHeader('Content-Type: application/json');
//		$this->response->setOutput(json_encode($json));
//	}
	
	public function getStats() {
		$json = array();
		
		$arr_categories = array();
		$arr_manufacturers = array();
		$arr_products = array();
		$arr_attrubutes = array();
		$arr_attrubute_values = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$products = $this->model_extension_feed_multifastfeed->getProductIds($this->request->post['setting_id']);
		
		foreach ($products as $product) {
			$arr_products[] = $product['product_id']; 
		}
		$count_products = count($arr_products);
		
		$categories = $this->model_extension_feed_multifastfeed->getCategoryIds(implode(',', $arr_products));
		
		foreach ($categories as $category) {
			$arr_categories[] = $category['category_id']; 
		}
		
		$arr_parents = array();

		$parents = $this->model_extension_feed_multifastfeed->getParents(implode(',', $arr_categories));
		foreach( $parents as $parent ) {
			$arr_categories[] = $parent['parent_id'];
			$arr_parents[] = $parent['parent_id'];
		}
		
		$do = true;
		while ($do) {
			$parents = $this->model_extension_feed_multifastfeed->getParents(implode(',', $arr_parents));
			if (count($parents) > 0) {
				$arr_parents = array();		
				foreach( $parents as $parent ) {
					$arr_categories[] = $parent['parent_id'];
					$arr_parents[] = $parent['parent_id'];
				}
			} else {
				$do = false;
			}
		}

		$arr_categories = array_unique($arr_categories);
//		$results = $this->model_extension_feed_multifastfeed->getCategories(implode(',', $arr_categories));
//		$categories = array();
//		foreach ($results as $result) {
//			$categories[] = array(
//				'category_id' 	=> $result['category_id'], 
//				'parent_id' 	=> $result['parent_id'], 
//				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
//			);
//		}
		$count_categories = count($arr_categories);
		
		$manufacturers = $this->model_extension_feed_multifastfeed->getManufacturerIds(implode(',', $arr_products));
		
		foreach ($manufacturers as $manufacturer) {
			$arr_manufacturers[] = $manufacturer['manufacturer_id']; 
		}
		$count_manufacturers = count($arr_manufacturers);

		$arr_attrubute_values = $this->model_extension_feed_multifastfeed->getAttributeIds(implode(',', $arr_products));
		$count_attrubute_values = count($arr_attrubute_values);

		foreach ($arr_attrubute_values as $value) {
			$arr_attrubutes[] = $value['attribute_id']; 
		}

		$arr_attrubutes = array_unique($arr_attrubutes);
		$count_attrubutes = count($arr_attrubutes);
		
		$json['success']['text'] = 'Статистика по товарам к выгрузке:<br> Категорий : ' . $count_categories . ' , Производителей : ' . $count_manufacturers . ' , Товаров : ' . $count_products . ' , Атрибутов : ' . $count_attrubutes . ' , Значений атрибутов : ' . $count_attrubute_values;
		$json['success']['count_categories'] = $count_categories;
		$json['success']['count_manufacturers'] = $count_manufacturers;
		$json['success']['count_products'] = $count_products;
		$json['success']['count_attrubutes'] = $count_attrubutes;
		$json['success']['count_attrubute_values'] = $count_attrubute_values;
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function startFeed() {
		$json = array();
		
		$arr_products = array();
		$arr_categories = array();
//		$arr_manufacturers = array();
//		$arr_attrubutes = array();
//		$arr_attrubute_values = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$setting = $this->model_extension_feed_multifastfeed->getSetting($this->request->post['setting_id']);
		$default_delivery_option = $setting['delivery_option'];
		$default_sales_note = $setting['sales_note'];
		
		$products = $this->model_extension_feed_multifastfeed->getProductIds($this->request->post['setting_id']);
		
		foreach ($products as $product) {
			$arr_products[] = $product['product_id']; 
		}
		$count_products = count($arr_products);
		
		$categories = $this->model_extension_feed_multifastfeed->getCategoryIds(implode(',', $arr_products));
		
		foreach ($categories as $category) {
			$arr_categories[] = $category['category_id']; 
		}
		
		$arr_parents = array();

		$parents = $this->model_extension_feed_multifastfeed->getParents(implode(',', $arr_categories));
		foreach( $parents as $parent ) {
			$arr_categories[] = $parent['parent_id'];
			$arr_parents[] = $parent['parent_id'];
		}
		
		$do = true;
		while ($do) {
			$parents = $this->model_extension_feed_multifastfeed->getParents(implode(',', $arr_parents));
			if (count($parents) > 0) {
				$arr_parents = array();		
				foreach( $parents as $parent ) {
					$arr_categories[] = $parent['parent_id'];
					$arr_parents[] = $parent['parent_id'];
				}
			} else {
				$do = false;
			}
		}

		$arr_categories = array_unique($arr_categories);
		$results = $this->model_extension_feed_multifastfeed->getCategories(implode(',', $arr_categories));
		$categories = array();
		foreach ($results as $result) {
			$categories[] = array(
				'category_id' 	=> $result['category_id'], 
				'parent_id' 	=> $result['parent_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
		$count_categories = count($arr_categories);
		
		$delivery_options = $this->model_extension_feed_multifastfeed->getDeliveryOptions($this->request->post['setting_id'], $arr_products);
		
		
		
		
//		$manufacturers = $this->model_extension_feed_multifastfeed->getManufacturerIds(implode(',', $arr_products));
//		
//		foreach ($manufacturers as $manufacturer) {
//			$arr_manufacturers[] = $manufacturer['manufacturer_id']; 
//		}
//		$count_manufacturers = count($arr_manufacturers);
//
//		$arr_attrubute_values = $this->model_extension_feed_multifastfeed->getAttributeIds(implode(',', $arr_products));
//		$count_attrubute_values = count($arr_attrubute_values);
//
//		foreach ($arr_attrubute_values as $value) {
//			$arr_attrubutes[] = $value['attribute_id']; 
//		}
//
//		$arr_attrubutes = array_unique($arr_attrubutes);
//		$count_attrubutes = count($arr_attrubutes);
		
		$feedDir = DIR_STORAGE . 'multifastfeed/';
		$feedName = 'market_feed.xml'; 			
		$file_name_and_path = $feedDir.$feedName;

		$dirname = dirname($file_name_and_path);

		$handle = FALSE;

		if(!is_dir($dirname)){
			mkdir($dirname,0755,TRUE);
		}

		$handle = fopen($file_name_and_path, "w");

		$yml  = '<?xml version="1.0" encoding="UTF-8"?>' . $this->eol;
		$yml .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' . $this->eol;
		$yml .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . $this->eol;
		$yml .= '<shop>' . $this->eol;

		$yml .= '<name>SDM-climate</name>' . $this->eol;
		$yml .= '<company>SDM-climate</company>' . $this->eol;
		$yml .= '<url>' . HTTPS_CATALOG . '</url>' . $this->eol;
		$yml .= '<version>fastcart.multifeed</version>' . $this->eol;

		$yml .= '<currencies>' . $this->eol;
		$yml .= '<currency id="RUB" rate="1" />' . $this->eol;
		$yml .= '</currencies>' . $this->eol;

		$yml .= '<categories>' . $this->eol;
		
		foreach ($categories as $category) {
			$category_id = $category['category_id']; 
			$parent_id = $category['parent_id']; 
			$name = $category['name'];
			if ($parent_id == 0) {
				$yml .= '<category id="' . $category_id . '" >' . $name . '</category>' . $this->eol;
			} else {
				$yml .= '<category id="' . $category_id . '" parentId="' . $parent_id . '" >' . $name . '</category>' . $this->eol;
			}
		}
		
		$yml .= '</categories>' . $this->eol;
	
		$yml .= '<delivery-options>' . $this->eol;
			$yml .= $default_delivery_option . $this->eol;
			foreach ($delivery_options as $delivery_option) {
				if ($delivery_option['delivery_option'] != '') {
					$yml .= html_entity_decode($delivery_option['delivery_option']) . $this->eol;
				}
			}
		$yml .= '</delivery-options>' . $this->eol;
	
		$yml .= '<offers>' . $this->eol;

		if($handle){
			fwrite($handle, $yml);
			fclose($handle);
		}
		
		$json['success'] = true;
		
		$json['file_name'] = $file_name_and_path;		
		
		$json['count'] = $count_products;
		$json['filepath'] = $file_name_and_path;
		$json['fileurl'] = HTTPS_CATALOG . 'storage/multifastfeed/' . 'market_feed.xml';
//		$json['filepath'] = realpath(DIR_APPLICATION . '..') . '/multifastfeed/' . 'market_feed.xml';
//		$json['fileurl'] = HTTPS_CATALOG . 'multifastfeed/' . 'market_feed.xml';
		
//		$feedDir = DIR_STORAGE . 'multifastfeed/';
//		$feedName = 'market_feed.xml'; 			
//		$file_name_and_path = $feedDir.$feedName;
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function fillBatch() {
		$json = array();
		
		$arr_products = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$setting = $this->model_extension_feed_multifastfeed->getSetting($this->request->post['setting_id']);
		$default_delivery_option = $setting['delivery_option'];
		$default_sales_note = strip_tags($setting['sales_note']);
		
		$products = $this->model_extension_feed_multifastfeed->getProductIds($this->request->post['setting_id']);
		
		foreach ($products as $product) {
			$arr_products[] = $product['product_id']; 
		}
		
		$yml = '';
		
		$products = $this->model_extension_feed_multifastfeed->getProducts(implode(',', $arr_products),$this->request->post['start'],$this->request->post['limit']);
		
		$num = 0; // $this->request->post['start'];
		
		foreach ($products as $product) {
			$num++;
			
			$arr_products[] = $product['product_id']; 
			
			$alias = $this->model_extension_feed_multifastfeed->getAlias('product_id=' . $product['product_id']);
			if ($alias) {
				$url = HTTPS_CATALOG . $alias['keyword'];
			} else {
				$url = HTTPS_CATALOG . 'index.php?route=product/product&amp;product_id=' . $product['product_id'];
			}
			$price = number_format($product['price'], 2, ',', '');
			$picture = HTTPS_CATALOG . 'image/' . $product['image'];
			
			$product_attributes = $this->model_extension_feed_multifastfeed->getProductAttributes($product['product_id']);		
			
			$special = false;
			$product_specials = $this->model_extension_feed_multifastfeed->getProductSpecials($product['product_id']);		
			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = number_format($product_special['price'], 2, ',', '');
					// берем первую найденную акцию
					// это грубо и неправильно, конечно
					break;
				}
			}
			
			$productId = $product['product_id'];
			$quantity = $product['quantity'];
			$name = strip_tags($product['name']);
			$model = strip_tags($product['model']);
			$description = strip_tags($product['description']);
			
			$sales_note				= '';
			$delivery_option	= '';
			
			$product_data = $this->model_extension_feed_multifastfeed->getProductData($this->request->post['setting_id'], $product['product_id']);
			
			if ($product_data) {
				$sales_note   			= strip_tags($product_data['sales_note']);
				$delivery_option   	= html_entity_decode($product_data['delivery_option']);
//				$sales_note   = str_replace ('"', '&quot;', trim($product_data['sales_note']));
//				$delivery_option   = str_replace ('"', '&quot;', trim($product_data['delivery_option']));
			}
			
			if ($sales_note == '') {
				$sales_note = $default_sales_note;	
			}
			
			if ($delivery_option == '') {
				$delivery_option = $default_delivery_option;	
			}
			
			$manufacturer = $this->model_extension_feed_multifastfeed->getManufacturerName($product['manufacturer_id']);
			if ($manufacturer) {
				$vendor = $manufacturer['name'];
			} else {
				$vendor = '';
			}
			
			$categories = $this->model_extension_feed_multifastfeed->getProductCategories($product['product_id']);
			
//			if ($categories) {
//				
//				$categiryId = $categories[0];
//				
//				for ($i = 0;$i < count($categories); $i++) {
//					$categories[$i]['child_num'] = $this->model_extension_feed_multifastfeed->getTotalCategoriesByCategoryId($categories[$i]['category_id']);
//					if (in_array($categories[$i]['category_id'], $arr_categories)) {
//						$categiryId = $categories[$i]['category_id'];
//					}
//				}
//				foreach ($categories as $category) {
//					if ($category['child_num'] == 0) {
//						if (in_array($category['category_id'], $arr_categories)) {
//							$categiryId = $category['category_id'];
//							break;
//						}
//					}
//				}
//			} else {
//				$categiryId = '';
//			}
			// сквозная последовательная нумерация товаров
			//$yml .= '<offer id="' . $num . '" available="true" >' . $this->eol;
			// нумерация товаров по идентификатору товара последовательно по возрастанию
			//$yml .= '<offer id="' . $productId . '" available="true" >' . $this->eol;
			//$yml .= '<productId>' . $productId . '</productId>' . $this->eol;
			if ($quantity > 0) {
				$yml .= '<offer id="' . $productId . '" available="true" >' . $this->eol;
			} else {
				$yml .= '<offer id="' . $productId . '" available="false" >' . $this->eol;
			}
			$yml .= '<url>' . $url . '</url>' . $this->eol;
			if ($special) {
				$yml .= '<price>' . $special . '</price>' . $this->eol;
				$yml .= '<oldprice>' . $price . '</oldprice>' . $this->eol;
			} else {
				$yml .= '<price>' . $price . '</price>' . $this->eol;
			}
			$yml .= '<currencyId>RUB</currencyId>' . $this->eol;
			foreach ($categories as $category) {
				$yml .= '<categoryId>' . $category['category_id'] . '</categoryId>' . $this->eol;
			}
			$yml .= '<picture>' . $picture . '</picture>' . $this->eol;
			$yml .= '<store>false</store>' . $this->eol;
			$yml .= '<pickup>false</pickup>' . $this->eol;
			$yml .= '<delivery>true</delivery>' . $this->eol;
			$yml .= '<delivery-options>' . $this->eol;
			$yml .= '' . $delivery_option . $this->eol;
			$yml .= '</delivery-options>' . $this->eol;
			$yml .= '<name>' . $name . '</name>' . $this->eol;
			$yml .= '<vendor>' . $vendor . '</vendor>' . $this->eol;
			$yml .= '<model>' . $model . '</model>' . $this->eol;
			$yml .= '<description>' . $description . '</description>' . $this->eol;
			$yml .= '<sales_notes>' . $sales_note . '</sales_notes>' . $this->eol;
			
			foreach ($product_attributes as $product_attribute) {
				$attribute_info = $this->model_extension_feed_multifastfeed->getAttribute($product_attribute['attribute_id']);

				if ($attribute_info) {
					if ($attribute_info['name'] == 'Источник') {
						//подавить служебные реквизиты	
					} else {
						$yml .= '<param name="' . $attribute_info['name'] . '">' . $product_attribute['product_attribute_description']['1']['text'] . '</param>' . $this->eol;
						//определить ид языка	
					}
				}
			}
			$yml .= '</offer>' . $this->eol;
		}
		
		$count_products = count($arr_products);
		
//		$rootPath = realpath(DIR_APPLICATION . '..'); 			
//		$feedDir = '/multifastfeed/'; 			
//		$feedName = 'market_feed.xml'; 			
//		$file_name_and_path = $rootPath.$feedDir.$feedName;
		
		$feedDir = DIR_STORAGE . 'multifastfeed/';
		$feedName = 'market_feed.xml'; 			
		$file_name_and_path = $feedDir.$feedName;
		
		$handle = FALSE;
		$handle = fopen($file_name_and_path, "a");
		
		if($handle){
			fwrite($handle, $yml);
			fclose($handle);
		}
		
		$json['success'] = true;
		$json['count'] = $num; //$count_products;
		$json['file'] = $file_name_and_path;
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function closeFeed() {
		$json = array();
		
		$this->load->model('extension/feed/multifastfeed');
		
		$feedDir = DIR_STORAGE . 'multifastfeed/';
		$feedName = 'market_feed.xml'; 			
		$file_name_and_path = $feedDir.$feedName;
		
		$handle = FALSE;
		$handle = fopen($file_name_and_path, "a");
		
		$yml = '</offers>' . $this->eol;
		
		$yml .= '<promos>' . $this->eol;
		
		$arr_products = array();
		$products = $this->model_extension_feed_multifastfeed->getProductIds($this->request->post['setting_id']);
		foreach ($products as $product) {
			$arr_products[] = $product['product_id']; 
		}
		
//$this->log->write('<pre>' . print_r($arr_products,1) . '</pre>');
		
		$products = array();
		$results = $this->model_extension_feed_multifastfeed->getAllProducts(implode(',', $arr_products));
		foreach ($results as $result) {
			$products[] = array(
				'product_id'		=> $result['product_id'],
				'price'				=> $result['price'],
				'special'				=> $result['special']
			);
		}
		
//$this->log->write('<pre>' . print_r($products,1) . '</pre>');
		
//			$product['product_id'] = $row['product_id'];
//			$product['name'] = $row['name'];
//			$product['description'] = $row['description'];
//			$product['model'] = $row['model'];
//			$product['sku'] = $row['sku'];
//			$product['price'] = $row['price'];
//			$product['special'] = $row['special'];
//			$product['quantity'] = $row['quantity'];
//			$product['delivery_option'] = $row['delivery_option'];
//			$product['sales_note'] = $row['sales_note'];
//			$product['image'] = $row['image'];
//			$product['manufacturer'] = $manufacturers[$row['manufacturer_id']];
		
		
		$promos = array();
		$results = $this->model_extension_feed_multifastfeed->getPromos();
		foreach ($results as $result) {
			$promos[] = array(
				'coupon_id'		=> $result['coupon_id'],
				'name'				=> $result['name'],
				'code'				=> $result['code'],
				'type'				=> $result['type'],
				'total'				=> $result['total'],
				'discount'		=> $result['discount'],
				'date_start'	=> date('Y-m-d', strtotime($result['date_start'])),
				'date_end'	=> date('Y-m-d', strtotime($result['date_end'])),
				'status'			=> $result['status']
			);
		}

		foreach ($promos as $promo) {
			
//			$promo_products = array();
//			$results = $this->model_extension_feed_multifastfeed->getPromoProducts($promo['coupon_id']);
//			foreach ($results as $result) {
//				$promo_products[] = $result['product_id'];
//			}
//	
//			$promo_categories = array();
//			$results = $this->model_extension_feed_multifastfeed->getPromoCategories($promo['coupon_id']);
//			foreach ($results as $result) {
//				$promo_categories[] = $result['category_id'];
//			}
			
			$promo_products = $this->model_extension_feed_multifastfeed->getPromoProducts($promo['coupon_id']);
			$promo_categories = $this->model_extension_feed_multifastfeed->getPromoCategories($promo['coupon_id']);
			foreach ($promo_categories as $key => $value) {
				$results = $this->model_extension_feed_multifastfeed->getProductsByCategoryId($value);
				foreach ($results as $result) {
					$promo_products[] = $row['product_id'];
				}
			}
			
			$promo_products = array_unique($promo_products);
			asort($promo_products);
			
			$purchase = array();
			foreach ($promo_products as $key => $value) {
				$k = array_search($value, array_column($products, 'product_id'));
				if (strlen($k) > 0) {
					if (!$products[$k]['special'] && $products[$k]['price'] > $promo['total']) $purchase[] = $value;
				}
			}
			
			if ($purchase) {
				$yml .= '<promo id="' . $promo['coupon_id'] . '" type="promo code">' . $this->eol;
				$yml .= '<start-date>' . $promo['date_start'] . '</start-date>' . $this->eol;
				$yml .= '<end-date>' . $promo['date_end'] . '</end-date>' . $this->eol;
				$yml .= '<description>' . $promo['name'] . '</description>' . $this->eol;
	//			$yml .= '<url>' . 'http://инфо.страница.акции' . '</url>' . $this->eol;
				$yml .= '<promo-code>' . $promo['code'] . '</promo-code>' . $this->eol;
				if ($promo['type'] == 'F') {
					$yml .= '<discount unit="currency" currency="RUR">' . number_format($promo['discount'], 2, '.', '') . '</discount>' . $this->eol;
				} else if ($promo['type'] == 'P') {
					$yml .= '<discount unit="percent">' . number_format($promo['discount'], 2, '.', '') . '</discount>' . $this->eol;
				}
				$yml .= '<purchase>' . $this->eol;
				foreach ($purchase as $key => $value) {
					$yml .= '<product offer-id="' . $value . '"/>' . $this->eol;
				}
				$yml .= '</purchase>' . $this->eol;
				$yml .= '</promo>' . $this->eol;
			}
		}

		$yml .= '</promos>' . $this->eol;
		
		$yml .= '</shop>' . $this->eol;
		$yml .= '</yml_catalog>';

		if($handle){
			fwrite($handle, $yml);
			fclose($handle);
		}

		$json['success'] = true;
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    /**
     * метод отдает список доступных основных поставщиков
     */
    public function autocompleteProductSuppliers()
    {
        $json = array();

        if (isset($this->request->get['filter_supplier'])) {
            $result = $this->db->query("SELECT DISTINCT `supplier_main` FROM ".DB_PREFIX
                ."product WHERE `supplier_main` LIKE '".$this->request->get['filter_supplier']."%'");

            if ($result->num_rows) {
                foreach ($result->rows as $row) {
                    $json[] = [
                        'value' => $row['supplier_main']
                    ];
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
	

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/feed/multifastfeed')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function install() {
		$this->load->model('extension/feed/multifastfeed');
		
//$this->log->write('multifastfeed - install');

		$this->model_extension_feed_multifastfeed->install();
	}

	public function uninstall() {
		$this->load->model('extension/feed/multifastfeed');

//$this->log->write('multifastfeed - uninstall');

		$this->model_extension_feed_multifastfeed->uninstall();
	}

}
<?php
class ControllerExtensionPaymentKupilegko extends Controller {
	private $error = array();
	public function install(){

		/*
		$this->db->query("
			DROP TABLE IF EXISTS `" . DB_PREFIX . "payment_kupilegko_category_to_category`
		");

		$this->db->query("
			CREATE TABLE `" . DB_PREFIX . "payment_kupilegko_category_to_category` (
			  `kupilegko_category_id` int(11) NOT NULL,
			  `category_id` int(11) NOT NULL,
			  PRIMARY KEY (`category_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
		");
		*/

	}

	public function uninstall(){

		/*
		$this->db->query("
			DROP TABLE IF EXISTS `" . DB_PREFIX . "payment_kupilegko_category_to_category`
		");
		*/

	}

	public function index() {
		$this->load->language('extension/payment/kupilegko');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('payment_kupilegko', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
			
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['merch_z'])) {
			$data['error_merch_z'] = $this->error['merch_z'];
		} else {
			$data['error_merch_z'] = '';
		}
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/kupilegko', 'user_token=' . $this->session->data['user_token'], true)
		);
				
		$data['action'] = $this->url->link('extension/payment/kupilegko', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

 		if (isset($this->request->post['payment_kupilegko_total'])) {
			$data['payment_kupilegko_total'] = $this->request->post['payment_kupilegko_total'];
		} else {
			$data['payment_kupilegko_total'] = $this->config->get('payment_kupilegko_total');
		}
		
		if (isset($this->request->post['payment_kupilegko_order_status_id'])) {
			$data['payment_kupilegko_order_status_id'] = $this->request->post['payment_kupilegko_order_status_id'];
		} else {
			$data['payment_kupilegko_order_status_id'] = $this->config->get('payment_kupilegko_order_status_id'); 
		}
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['payment_kupilegko_geo_zone_id'])) {
			$data['payment_kupilegko_geo_zone_id'] = $this->request->post['payment_kupilegko_geo_zone_id'];
		} else {
			$data['payment_kupilegko_geo_zone_id'] = $this->config->get('payment_kupilegko_geo_zone_id'); 
		}
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['payment_kupilegko_status'])) {
			$data['payment_kupilegko_status'] = $this->request->post['payment_kupilegko_status'];
		} else {
			$data['payment_kupilegko_status'] = $this->config->get('payment_kupilegko_status');
		}
		
		if (isset($this->request->post['payment_kupilegko_sort_order'])) {
			$data['payment_kupilegko_sort_order'] = $this->request->post['payment_kupilegko_sort_order'];
		} else {
			$data['payment_kupilegko_sort_order'] = $this->config->get('payment_kupilegko_sort_order');
		}

		if (isset($this->request->post['payment_kupilegko_category'])) {
			$data['payment_kupilegko_category'] = $this->request->post['payment_kupilegko_category'];
		} else {
			$data['payment_kupilegko_category'] = $this->config->get('payment_kupilegko_category');
		}
		if (isset($this->request->post['payment_kupilegko_merch_z'])) {
			$data['payment_kupilegko_merch_z'] = $this->request->post['payment_kupilegko_merch_z'];
		} else {
			$data['payment_kupilegko_merch_z'] = $this->config->get('payment_kupilegko_merch_z');
		}

		

		$this->load->model('catalog/category');

		$results = $this->model_catalog_category->getCategories(['start' => 0, 'limit' => 1000]);

		$data['categories'] = [];

		foreach($results as $result){
			if(isset($data['payment_kupilegko_category'][$result['category_id']])){
 				$kupilegko_category = $data['payment_kupilegko_category'][$result['category_id']];
			}else{
				$kupilegko_category = 0;
			}

			$data['categories'][] = [
				'category_id' => $result['category_id'],
				'name' => $result['name'],
				'kupilegko_category' => $kupilegko_category
			];
		}

		$data['items'] = [
			1 => 'Системы очистки воздуха, кондиционеры',
			2 => 'Аудиотехника',
			3 => 'Крупная бытовая техника',
			4 => 'Строительные инструменты',
			5 => 'Строительные материалы',
			6 => 'Автотовары',
			7 => 'Компьютеры, комплектующие и расходные материалы',
			8 => 'Техника для дачи',
			9 => 'Электротовары',
			10 => 'Одежда из кожи и меха',
			11 => 'Мебель',
			12 => 'Прочая одежда и обувь',
			13 => 'Фото-видео товары',
			14 => 'Сантехника',
			15 => 'Услуги',
			16 => 'Мелкая бытовая техника',
			17 => 'Спортивные товары',
			18 => 'Телефоны (в т.ч. мобильные) и аксесуары',
			19 => 'Телевизоры и видео',
			20 => 'Окна и двери',
			21 => 'Ноутбуки',
			22 => 'Плазменные телевизоры',
			23 => 'Карты',
			24 => 'Игрушки',
			25 => 'Иное',
			26 => 'Ювелирные изделия',
			27 => 'Дома',
			28 => 'Товары для охоты и рыбалки',
			29 => 'Обучение',
			30 => 'Медицина',
			31 => 'Косметика',
			32 => 'Вело-мото-техника',
			34 => 'Туризм'
		];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('extension/payment/kupilegko', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/kupilegko')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!isset($this->request->post['payment_kupilegko_category']) || !is_array($this->request->post['payment_kupilegko_category'])) {
			$this->error['warning'] = $this->language->get('error_category');
		}else{
			foreach($this->request->post['payment_kupilegko_category'] as $category){
				if(!$category){
					$this->error['warning'] = $this->language->get('error_category_empty');
					break;
				}
			}
		}


		if (!$this->request->post['payment_kupilegko_merch_z']) {
			$this->error['merch_z'] = $this->language->get('error_merch_z');
		}
		
		return !$this->error;
	}
}
?>
<?php
class ControllerExtensionPaymentKupivkredit extends Controller {
	private $error = array();
	
	public function index() {
		$this->load->language('extension/payment/kupivkredit');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
        $this->load->model('extension/payment/kupivkredit');

        $this->document->addScript('view/javascript/octemplates/bootstrap-notify/bootstrap-notify.min.js');
        $this->document->addScript('view/javascript/octemplates/oct_main.js');
        $this->document->addStyle('view/stylesheet/oct_ultrastore.css');
        
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

		    /** обновляем чпу */
            if (isset($this->request->post['payment_kupivkredit_url'])) {
                $this->model_extension_payment_kupivkredit->setSeoUrl($this->request->post['payment_kupivkredit_url']);
            }

			$this->model_setting_setting->editSetting('payment_kupivkredit', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment/kupivkredit', 'user_token=' . $this->session->data['user_token'], true));			
		}
		
		$errors = [
            'warning',
            'merch_z',
		    'promo_code',
            'meta_title',
            'notify_email'
        ];
				
		foreach ($errors as $error){
            if (isset($this->error[$error])) {
                $data['error_'.$error] = $this->error[$error];
            } else {
                $data['error_'.$error] = '';
            }
        }
		
		if(isset($this->session->data['success'])){
		    $data['success'] = $this->session->data['success'];
		    unset($this->session->data['success']);
        }
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/kupivkredit', 'user_token=' . $this->session->data['user_token'], true)
		);
				
		$data['action'] = $this->url->link('extension/payment/kupivkredit', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		if (isset($this->request->post['payment_kupivkredit_merch_z'])) {
			$data['payment_kupivkredit_merch_z'] = $this->request->post['payment_kupivkredit_merch_z'];
		} else {
			$data['payment_kupivkredit_merch_z'] = $this->config->get('payment_kupivkredit_merch_z');
		}

		if (isset($this->request->post['payment_kupivkredit_total'])) {
			$data['payment_kupivkredit_total'] = $this->request->post['payment_kupivkredit_total'];
		} else {
			$data['payment_kupivkredit_total'] = $this->config->get('payment_kupivkredit_total');
		}
		
		if (isset($this->request->post['payment_kupivkredit_order_status_id'])) {
			$data['payment_kupivkredit_order_status_id'] = $this->request->post['payment_kupivkredit_order_status_id'];
		} else {
			$data['payment_kupivkredit_order_status_id'] = $this->config->get('payment_kupivkredit_order_status_id'); 
		}
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['payment_kupivkredit_geo_zone_id'])) {
			$data['payment_kupivkredit_geo_zone_id'] = $this->request->post['payment_kupivkredit_geo_zone_id'];
		} else {
			$data['payment_kupivkredit_geo_zone_id'] = $this->config->get('payment_kupivkredit_geo_zone_id'); 
		}
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['payment_kupivkredit_status'])) {
			$data['payment_kupivkredit_status'] = $this->request->post['payment_kupivkredit_status'];
		} else {
			$data['payment_kupivkredit_status'] = $this->config->get('payment_kupivkredit_status');
		}
		
		if (isset($this->request->post['payment_kupivkredit_server'])) {
			$data['payment_kupivkredit_server'] = $this->request->post['payment_kupivkredit_server'];
		} else {
			$data['payment_kupivkredit_server'] = $this->config->get('payment_kupivkredit_server');
		}
		
		if (isset($this->request->post['payment_kupivkredit_promo_code'])) {
			$data['payment_kupivkredit_promo_code'] = $this->request->post['payment_kupivkredit_promo_code'];
		} else {
			$data['payment_kupivkredit_promo_code'] = $this->config->get('payment_kupivkredit_promo_code');
		}
		
		if (isset($this->request->post['payment_kupivkredit_sort_order'])) {
			$data['payment_kupivkredit_sort_order'] = $this->request->post['payment_kupivkredit_sort_order'];
		} else {
			$data['payment_kupivkredit_sort_order'] = $this->config->get('payment_kupivkredit_sort_order');
		}

        if (isset($this->request->post['payment_kupivkredit_meta_title'])) {
            $data['payment_kupivkredit_meta_title'] = $this->request->post['payment_kupivkredit_meta_title'];
        } else {
            $data['payment_kupivkredit_meta_title'] = $this->config->get('payment_kupivkredit_meta_title');
        }
        
        if (isset($this->request->post['payment_kupivkredit_seo_h1'])) {
            $data['payment_kupivkredit_seo_h1'] = $this->request->post['payment_kupivkredit_seo_h1'];
        } else {
            $data['payment_kupivkredit_seo_h1'] = $this->config->get('payment_kupivkredit_seo_h1');
        }
        
        if (isset($this->request->post['payment_kupivkredit_meta_description'])) {
            $data['payment_kupivkredit_meta_description'] = $this->request->post['payment_kupivkredit_meta_description'];
        } else {
            $data['payment_kupivkredit_meta_description'] = $this->config->get('payment_kupivkredit_meta_description');
        }
        
        if (isset($this->request->post['payment_kupivkredit_meta_keywords'])) {
            $data['payment_kupivkredit_meta_keywords'] = $this->request->post['payment_kupivkredit_meta_keywords'];
        } else {
            $data['payment_kupivkredit_meta_keywords'] = $this->config->get('payment_kupivkredit_meta_keywords');
        }
        
        if (isset($this->request->post['payment_kupivkredit_url'])) {
            $data['payment_kupivkredit_url'] = $this->request->post['payment_kupivkredit_url'];
        } else {
            $data['payment_kupivkredit_url'] = $this->config->get('payment_kupivkredit_url');
        }
		
		if (isset($this->request->post['payment_kupivkredit_sticker_сode'])) {
			$data['payment_kupivkredit_sticker_сode'] = $this->request->post['payment_kupivkredit_sticker_сode'];
		} else {
			$data['payment_kupivkredit_sticker_сode'] = $this->config->get('payment_kupivkredit_sticker_сode');
		}

        if (isset($this->request->post['payment_kupivkredit_purchase_data'])) {
            $data['payment_kupivkredit_purchase_data'] = $this->request->post['payment_kupivkredit_purchase_data'];
        } else {
            $data['payment_kupivkredit_purchase_data'] = $this->config->get('payment_kupivkredit_purchase_data');
        }

        if (isset($this->request->post['payment_kupivkredit_generate_type'])) {
            $data['payment_kupivkredit_generate_type'] = $this->request->post['payment_kupivkredit_generate_type'];
        } else {
            $data['payment_kupivkredit_generate_type'] = $this->config->get('payment_kupivkredit_generate_type') ?? 1;
        }

        /** доступные пользовательские стикеры */
        $oct_stickers = $this->config->get('oct_stickers_data');
        $data['stickers'] = [];
        if($oct_stickers['customer']){
            foreach ($oct_stickers['customer'] as $stickerKey => $stickerData){
                if($stickerData['status'] == 'on')
                    $data['stickers']['customer_' . $stickerKey] = current($stickerData['title']);
            }               
        }
		
        $data['categories'] = array();
        $filter_data = array('sort'=>'fullname','order'=>'ASC');
        $results = $this->model_extension_payment_kupivkredit->getAllCategories($filter_data);
        foreach ($results as $result) {
            $level = count(explode("&and;", $result['fullname']));
            $data['categories'][] = array(
                'category_id' 	=> $result['category_id'],
                'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
                'level' 		=> $level,
                'parent_id' 	=> $result['parent_id'],
                'product_count' => $result['product_count'],
                'sort_order' 	=> $result['sort_order'],
                'status' 		=> $result['status']
            );
        }

        $data['manufacturers'] = array();
        $filter_data = array('sort'=>'name','order'=>'ASC');
        $results = $this->model_extension_payment_kupivkredit->getManufacturers($filter_data);
        foreach ($results as $result) {
            $data['manufacturers'][] = array(
                'manufacturer_id' 	=> $result['manufacturer_id'],
                'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('setting/store');

        $data['stores'] = array();

        $data['stores'][] = array(
            'store_id' => 0,
            'name'     => $this->language->get('text_default')
        );

        $stores = $this->model_setting_store->getStores();

        foreach ($stores as $store) {
            $data['stores'][] = array(
                'store_id' => $store['store_id'],
                'name'     => $store['name']
            );
        }

        $data['pagination'] = '';
        $data['results'] = '';
        $data['user_token'] = $this->session->data['user_token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('extension/payment/kupivkredit', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/kupivkredit')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['payment_kupivkredit_merch_z']) {
			$this->error['merch_z'] = $this->language->get('error_merch_z');
		}

        if (!isset($this->request->post['payment_kupivkredit_promo_code'])) {
            $this->error['promo_code'] = $this->language->get('error_promo_code');
        }
        
        if(isset($this->request->post['payment_kupivkredit_promo_code'])) {
            foreach ($this->request->post['payment_kupivkredit_promo_code'] as $item) {
                if (empty($item['code']) or empty($item['name'])) {
                    $this->error['promo_code'] = $this->language->get('error_promo_code_empty');
                }
            }
        }

        foreach ($this->request->post['payment_kupivkredit_purchase_data'] as $key => $field) {
            if (empty($field) && $key == "notify_email") {
                $this->error['notify_email'] = $this->language->get('error_notify_email');
            }
        }
		
		if (!$this->request->post['payment_kupivkredit_meta_title']) {
			$this->error['meta_title'] = 'Meta-title обязателен к заполнению!';
		}
		
		return !$this->error;
	}

    public function setProducts() {
        $json = array();

        $this->load->model('extension/payment/kupivkredit');

        $this->model_extension_payment_kupivkredit->setProducts($this->request->post['products'], $this->request->post['types'], $this->request->post['mode']);

        $json['success'] = 'Обновлен список товаров';
        $json['style'] = 'success';

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProducts() {
        $json = array();

        $this->load->model('extension/payment/kupivkredit');

        if ($this->request->post['filter_name'] == '') {
            $filter_name = null;
        }else{
            $filter_name = $this->request->post['filter_name'];
        }

        if ($this->request->post['filter_category'] == '*') {
            $filter_category = null;
        }else{
            $filter_category = $this->request->post['filter_category'];
        }

        if ($this->request->post['filter_manufacturer'] == '*') {
            $filter_manufacturer = null;
        }else{
            $filter_manufacturer = $this->request->post['filter_manufacturer'];
        }

        if ($this->request->post['filter_price_from'] == '') {
            $filter_price_from = null;
        }else{
            $filter_price_from = $this->request->post['filter_price_from'];
        }

        if ($this->request->post['filter_price_to'] == '') {
            $filter_price_to = null;
        }else{
            $filter_price_to = $this->request->post['filter_price_to'];
        }

        if ($this->request->post['filter_quantity_from'] == '') {
            $filter_quantity_from = null;
        }else{
            $filter_quantity_from = $this->request->post['filter_quantity_from'];
        }

        if ($this->request->post['filter_quantity_to'] == '') {
            $filter_quantity_to = null;
        }else{
            $filter_quantity_to = $this->request->post['filter_quantity_to'];
        }

        if ($this->request->post['filter_status'] == '*') {
            $filter_status = null;
        }else{
            $filter_status = $this->request->post['filter_status'];
        }

        $action = isset($this->request->post['action']) ? $this->request->post['action'] : 'list';

        $page = $this->request->post['page'];

        $filter_data = array(
            'filter_name'	  				=> $filter_name,
            'filter_price_from'	  	=> $filter_price_from,
            'filter_price_to'	  		=> $filter_price_to,
            'filter_quantity_from' 	=> $filter_quantity_from,
            'filter_quantity_to' 		=> $filter_quantity_to,
            'filter_category' 			=> $filter_category,
            'filter_manufacturer' 	=> $filter_manufacturer,
            'filter_status'   			=> $filter_status,
            'sort'            			=> 'pd.name',
            'order'           			=> 'ASC',
            'start'           			=> ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'           			=> $this->config->get('config_limit_admin'),
            'action'                    => $action
        );

        $product_total = $this->model_extension_payment_kupivkredit->getTotalProducts($filter_data);
        $page_products = [];

        if ($product_total == 0) {

            $json['success']['pagination'] = '';
            $json['success']['results'] = '';
            $json['success']['text'] = 'Запрос выполнен. Пустой результат.';

        } else {
            $results = $this->model_extension_payment_kupivkredit->getProducts($filter_data);

            foreach ($results as $result) {
                $special = false;

                $product_specials = $this->model_extension_payment_kupivkredit->getProductSpecials($result['product_id']);
                foreach ($product_specials  as $product_special) {
                    if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
                        $special = $this->currency->format($product_special['price'], $this->config->get('config_currency'));
                        break;
                    }
                }

                $page_products[] = [
                    'product_id' => $result['product_id'],
                    'model'      => str_replace('"', '&quot;', trim($result['model'])),
                    'name'       => str_replace('"', '&quot;', trim($result['name'])),
                    'price'      => $result['price'],
                    'special'    => $special,
                    'quantity'   => $result['quantity'],
                    'status'     => $result['status'],
                    'types'      => $this->model_extension_payment_kupivkredit->getProductTypes($result['product_id']),
                ];

            }

            $paginator = $this->getPaginator($product_total, $page);
            $json['success']['pagination'] = $paginator['paginator'];
            $json['success']['results'] = $paginator['results'];

            $json['success']['text'] = 'Запрос выполнен. Загружена страница ' . $page . '.';
        }

        $json['success']['page_products'] = $page_products;
        $json['success']['count'] = $product_total;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getPaginator($total, $page) {

        $limit = $this->config->get('config_limit_admin');
        $num_links = 8;
        $text_first = '|&lt;';
        $text_last = '&gt;|';
        $text_next = '&gt;';
        $text_prev = '&lt;';

        $paginator = array();

        if ($page < 1) {
            $page = 1;
        }
        if (!(int)$limit) {
            $limit = 20;
        }
        $num_pages = ceil($total / $limit);
        $output = '<ul class="paginator">';
        if ($page > 1) {
            $output .= '<li id="page-' . '1'. '" class="page">' . $text_first . '</li>';
            if ($page - 1 === 1) {
                $output .= '<li id="page-' . $page . '" class="page">' . $text_prev . '</li>';
            } else {
                $output .= '<li id="page-' . ($page - 1) . '" class="page">' . $text_prev . '</li>';
            }
        }
        if ($num_pages > 1) {
            if ($num_pages <= $num_links) {
                $start = 1;
                $end = $num_pages;
            } else {
                $start = $page - floor($num_links / 2);
                $end = $page + floor($num_links / 2);
                if ($start < 1) {
                    $end += abs($start) + 1;
                    $start = 1;
                }
                if ($end > $num_pages) {
                    $start -= ($end - $num_pages);
                    $end = $num_pages;
                }
            }
            for ($i = $start; $i <= $end; $i++) {
                if ($page == $i) {
                    $output .= '<li id="page-' . $i . '" class="active">' . $i . '</li>';
                } else {
                    $output .= '<li id="page-' . $i . '" class="page">' . $i . '</li>';
                }
            }
        }
        if ($page < $num_pages) {
            $output .= '<li id="page-' . ($page + 1) . '" class="page">' . $text_next . '</li>';
            $output .= '<li id="page-' . $num_pages . '" class="page">' . $text_last . '</li>';
        }
        $output .= '</ul>';

        if ($num_pages > 1) {
            $paginator['paginator'] = $output;
        } else {
            $paginator['paginator'] = '';
        }

        $paginator['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit), $total, ceil($total / $limit));

        return $paginator;
    }

    /**
     * обновление стикеров у всех товаров, доступных в рассрочку
     */
    public function updateStickers() {
        $this->load->model('extension/payment/kupivkredit');
    
        $this->model_extension_payment_kupivkredit->updateStickers();
    
        $json['success'] = 'Все стикеры у товаров успешно обновлены!';
    
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * список правил генерации
     */
    public function listGenerateRules(){
        $this->load->model('extension/payment/kupivkredit');

        $rules = [];
        $data = $this->model_extension_payment_kupivkredit->listGenerateRules();
        if($data){

            $categories = array();
            $results = $this->model_extension_payment_kupivkredit->getAllCategories([]);
            foreach ($results as $result) {
                $categories[$result['category_id']] = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
            }

            $manufacturers = array();
            $results = $this->model_extension_payment_kupivkredit->getManufacturers([]);
            foreach ($results as $result) {
                $manufacturers[$result['manufacturer_id']] = strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'));
            }
            
            $statuses = [
                0 => 'отключено',
                1 => 'включено',
            ];
            
            foreach ($data as $item){
                $rulesArr = json_decode($item['rules'], true);
                
                $rules[] = [
                    'id' => $item['id'],
                    'rules' => $rulesArr
                ];
                      
                $html = '';
                if($rulesArr['category'])
                    $html .= "Категория: " . ($rulesArr['category'] == '*' ? "все" : $categories[(int)$rulesArr['category']]);
                if($rulesArr['manufacturer'])
                    $html .= ", производитель: " . ($rulesArr['manufacturer'] == '*' ? "все" : $manufacturers[(int)$rulesArr['manufacturer']]); 
                if($rulesArr['price_from'])
                    $html .= ", цена от: {$rulesArr['price_from']}"; 
                if($rulesArr['price_to'])
                    $html .= ", цена до: {$rulesArr['price_to']}"; 
                if($rulesArr['quantity_from'])
                    $html .= ", кол-во от: {$rulesArr['quantity_from']}"; 
                if($rulesArr['quantity_to'])
                    $html .= ", кол-во до: {$rulesArr['quantity_to']}"; 
                if($rulesArr['status'])
                    $html .= ", статус товара: " . ($rulesArr['status'] == '*' ? "все" : $statuses[(int)$rulesArr['status']]); 
                if($rulesArr['types'])
                    $html .= ", варианты рассрочки: " . implode(", ", $rulesArr['types']); 
                
                $htmlRules[] = [
                    'id' => $item['id'],
                    'text' => $html
                ];
            }
        }
        $json['success'] = true;
        $json['rules'] = $rules;
        $json['htmlRules'] = $htmlRules;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * сохранение нового правила генерации
     */
    public function saveGenerateRules(){
        $json['success'] = false;
        
        if($this->request->post and isset($this->request->post['types'])){
            $this->load->model('extension/payment/kupivkredit');

            $this->model_extension_payment_kupivkredit->saveGenerateRules($this->request->post);
            $json['success'] = true;
        }

        $json['error'] = 'Нужно задать параметры фильтра и указать варианты рассрочки!';
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * удаление правила генерации
     */
    public function deleteGenerateRule(){
        $json['success'] = false;
        
        if($this->request->post['rule_id']){
            $this->load->model('extension/payment/kupivkredit');

            $this->model_extension_payment_kupivkredit->deleteGenerateRule((int) $this->request->post['rule_id']);
            $json['success'] = true;
        }
        
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    
    public function install(){
        $this->load->model('extension/payment/kupivkredit');
        $this->model_extension_payment_kupivkredit->install();
    }

    public function uninstall(){
        $this->load->model('extension/payment/kupivkredit');
        $this->model_extension_payment_kupivkredit->uninstall();
    }
}
?>
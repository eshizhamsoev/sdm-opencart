<?php
class ControllerExtensionPaymentKupivkredit3 extends Controller {
	private $error = array();
	
	public function index() {
		$this->load->language('extension/payment/kupivkredit3');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('payment_kupivkredit3', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
			
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['merch_z'])) {
			$data['error_merch_z'] = $this->error['merch_z'];
		} else {
			$data['error_merch_z'] = '';
		}
		
		if (isset($this->error['promo_code'])) {
			$data['error_promo_code'] = $this->error['promo_code'];
		} else {
			$data['error_promo_code'] = '';
		}
		
	
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/kupivkredit3', 'user_token=' . $this->session->data['user_token'], true)
		);
				
		$data['action'] = $this->url->link('extension/payment/kupivkredit3', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		if (isset($this->request->post['payment_kupivkredit3_merch_z'])) {
			$data['payment_kupivkredit3_merch_z'] = $this->request->post['payment_kupivkredit3_merch_z'];
		} else {
			$data['payment_kupivkredit3_merch_z'] = $this->config->get('payment_kupivkredit3_merch_z');
		}

		if (isset($this->request->post['payment_kupivkredit3_total'])) {
			$data['payment_kupivkredit3_total'] = $this->request->post['payment_kupivkredit3_total'];
		} else {
			$data['payment_kupivkredit3_total'] = $this->config->get('payment_kupivkredit3_total');
		}
		
		if (isset($this->request->post['payment_kupivkredit3_order_status_id'])) {
			$data['payment_kupivkredit3_order_status_id'] = $this->request->post['payment_kupivkredit3_order_status_id'];
		} else {
			$data['payment_kupivkredit3_order_status_id'] = $this->config->get('payment_kupivkredit3_order_status_id'); 
		}
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['payment_kupivkredit3_geo_zone_id'])) {
			$data['payment_kupivkredit3_geo_zone_id'] = $this->request->post['payment_kupivkredit3_geo_zone_id'];
		} else {
			$data['payment_kupivkredit3_geo_zone_id'] = $this->config->get('payment_kupivkredit3_geo_zone_id'); 
		}
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['payment_kupivkredit3_status'])) {
			$data['payment_kupivkredit3_status'] = $this->request->post['payment_kupivkredit3_status'];
		} else {
			$data['payment_kupivkredit3_status'] = $this->config->get('payment_kupivkredit3_status');
		}
		
		if (isset($this->request->post['payment_kupivkredit3_server'])) {
			$data['payment_kupivkredit3_server'] = $this->request->post['payment_kupivkredit3_server'];
		} else {
			$data['payment_kupivkredit3_server'] = $this->config->get('payment_kupivkredit3_server');
		}
		
		if (isset($this->request->post['payment_kupivkredit3_promo_сode'])) {
			$data['payment_kupivkredit3_promo_сode'] = $this->request->post['payment_kupivkredit3_promo_сode'];
		} else {
			$data['payment_kupivkredit3_promo_сode'] = $this->config->get('payment_kupivkredit3_promo_сode');
		}
		
		if (isset($this->request->post['payment_kupivkredit3_sort_order'])) {
			$data['payment_kupivkredit3_sort_order'] = $this->request->post['payment_kupivkredit3_sort_order'];
		} else {
			$data['payment_kupivkredit3_sort_order'] = $this->config->get('payment_kupivkredit3_sort_order');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('extension/payment/kupivkredit3', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/kupivkredit3')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['payment_kupivkredit3_merch_z']) {
			$this->error['merch_z'] = $this->language->get('error_merch_z');
		}
		
		if (!$this->request->post['payment_kupivkredit3_promo_сode']) {
			$this->error['promo_сode'] = $this->language->get('error_promo_code');
		}
		
		return !$this->error;
	}
}
?>
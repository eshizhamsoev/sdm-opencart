<?php
/**************************************************************/
/*	@support	https://wisesol.ru/					  */
/**************************************************************/

class ControllerOCTemplatesModuleOctPopupCallStock extends Controller {
	private $error = [];
    
	public function index() {
		$this->load->language('octemplates/module/oct_popup_call_stock');
		
		$this->document->addScript('view/javascript/octemplates/bootstrap-notify/bootstrap-notify.min.js');
		$this->document->addScript('view/javascript/octemplates/oct_main.js');
		$this->document->addStyle('view/stylesheet/oct_ultrastore.css');
		
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('localisation/language');

		$oct_popup_call_stock_page_info = $this->model_setting_setting->getSetting('oct_popup_call_stock');
		
		if (!$oct_popup_call_stock_page_info) {
			$this->response->redirect($this->url->link('octemplates/module/oct_popup_call_stock/install', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('oct_popup_call_stock', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('octemplates/module/oct_popup_call_stock', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}
        
		$data['breadcrumbs'] = [];

		$data['breadcrumbs'][] = [
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		];

		$data['breadcrumbs'][] = [
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('octemplates/module/oct_popup_call_stock', 'user_token=' . $this->session->data['user_token'], true)
		];

		$data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';	
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['action'] = $this->url->link('octemplates/module/oct_popup_call_stock', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$data['user_token'] = $this->session->data['user_token'];
		
		$data['error_notify_email'] = isset($this->error['notify_email']) ? $this->error['notify_email'] : '';	
		$data['oct_popup_call_stock_status'] = isset($this->request->post['oct_popup_call_stock_status']) ? $this->request->post['oct_popup_call_stock_status'] : $this->config->get('oct_popup_call_stock_status');	
		$data['oct_popup_call_stock_data'] = isset($this->request->post['oct_popup_call_stock_data']) ? $this->request->post['oct_popup_call_stock_data'] : $this->config->get('oct_popup_call_stock_data');	
        
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');
        
		$this->response->setOutput($this->load->view('octemplates/module/oct_popup_call_stock', $data));
	}
    
	public function history() {
			$data = [];
			$this->load->model('octemplates/module/oct_popup_call_stock');
			$this->load->language('octemplates/module/oct_popup_call_stock');
			
			$page          = (isset($this->request->get['page'])) ? $this->request->get['page'] : 1;
			$data['user_token'] = $this->session->data['user_token'];
			
			$data['histories'] = [];
			
			$filter_data = [
					'start' => ($page - 1) * 20,
					'limit' => 20,
					'sort' => 'r.date_added',
					'order' => 'DESC'
			];
			
			$results = $this->model_octemplates_module_oct_popup_call_stock->getCallArray($filter_data);
			
			foreach ($results as $result) {
					$info = [];
					
					$fields = unserialize($result['info']);
					
					foreach ($fields as $field) {
							$info[] = [
									'name' => $field['name'],
									'value' => $field['value']
							];
					}
					
					$data['histories'][] = [
							'request_id' => $result['request_id'],
							'info' => $info,
							'date_added' => $result['date_added']
					];
			}
			
			$history_total = $this->model_octemplates_module_oct_popup_call_stock->getTotalCallArray();
			
			$pagination        = new Pagination();
			$pagination->total = $history_total;
			$pagination->page  = $page;
			$pagination->limit = 20;
			$pagination->url   = $this->url->link('octemplates/module/oct_popup_call_stock/history', 'user_token=' . $this->session->data['user_token'] . '&page={page}', true);
			
			$data['pagination'] = $pagination->render();
			
			$data['results'] = sprintf($this->language->get('text_pagination'), ($history_total) ? (($page - 1) * 20) + 1 : 0, ((($page - 1) * 20) > ($history_total - 20)) ? $history_total : ((($page - 1) * 20) + 20), $history_total, ceil($history_total / 20));
			
			$this->response->setOutput($this->load->view('octemplates/module/oct_popup_call_stock_history', $data));
	}
	
	public function delete_selected() {
			$json = [];
			$this->load->model('octemplates/module/oct_popup_call_stock');
			
			$info = $this->model_octemplates_module_oct_popup_call_stock->getCall((int) $this->request->get['delete']);
			
			if ($info) {
					$this->model_octemplates_module_oct_popup_call_stock->deleteCall((int) $this->request->get['delete']);
			}
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
	}
	
	public function delete_all() {
			$json = [];
			$this->load->model('octemplates/module/oct_popup_call_stock');
			
			$this->model_octemplates_module_oct_popup_call_stock->deleteAllCallArray();
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
	}
	
	public function delete_all_selected() {
			$json = [];
			$this->load->model('octemplates/module/oct_popup_call_stock');
			
			if (isset($this->request->request['selected'])) {
					foreach ($this->request->request['selected'] as $request_id) {
							$info = $this->model_octemplates_module_oct_popup_call_stock->getCall((int) $request_id);
							
							if ($info) {
									$this->model_octemplates_module_oct_popup_call_stock->deleteCall((int) $request_id);
							}
					}
			}
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
	}
	
	public function install() {
			$this->load->language('octemplates/module/oct_popup_call_stock');
	
			$this->load->model('octemplates/module/oct_popup_call_stock');
			$this->load->model('setting/setting');
	$this->load->model('user/user_group');
			
			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'octemplates/module/oct_popup_call_stock');
			$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'octemplates/module/oct_popup_call_stock');
			
			$this->model_octemplates_module_oct_popup_call_stock->makeDB();
			
			$this->model_setting_setting->editSetting('oct_popup_call_stock', [
					'oct_popup_call_stock_status' => '1',
					'oct_popup_call_stock_data' => [
							'notify_status' => '1',
							'notify_email' => $this->config->get('config_email'),
							'name' => '2',
							'telephone' => '2',
			'email' => 0,
							'comment' => '1',
							'link' => '2',
							'mask' => '8 (999) 999-99-99'
					]
			]);
			
			$this->session->data['success'] = $this->language->get('text_success_install');
	
	$this->response->redirect($this->url->link('octemplates/module/oct_popup_call_stock', 'user_token=' . $this->session->data['user_token'], true));
	}
	
	public function uninstall() {
			$this->load->model('setting/setting');
			$this->load->model('octemplates/module/oct_popup_call_stock');
			$this->load->model('user/user_group');
			
			$this->model_user_user_group->removePermission($this->user->getGroupId(), 'access', 'octemplates/module/oct_popup_call_stock');
			$this->model_user_user_group->removePermission($this->user->getGroupId(), 'modify', 'octemplates/module/oct_popup_call_stock');
			
			$this->model_octemplates_module_oct_popup_call_stock->deleteDB();
			
			$this->model_setting_setting->deleteSetting('oct_popup_call_stock');
	}
	
	protected function validate() {
			if (!$this->user->hasPermission('modify', 'octemplates/module/oct_popup_call_stock')) {
					$this->error['warning'] = $this->language->get('error_permission');
			}
			
			foreach ($this->request->post['oct_popup_call_stock_data'] as $key => $field) {
					if (empty($field) && $key == "notify_email") {
							$this->error['notify_email'] = $this->language->get('error_notify_email');
					}
			}
			
			return !$this->error;
	}
}
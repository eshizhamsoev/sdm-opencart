<?php
class ControllerUdsUdsCustomer extends Controller {
    private $error = array();

    public function index() {

			$this->document->setTitle('Проверка клиента'); //($this->language->get('heading_title'));

        $data['heading_title'] = 'Проверка клиента'; //$this->language->get('heading_title');
			
				$data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();
        array_push($data['breadcrumbs'],
            array(  // Главная
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL')
            ),
            array(  // Оплата через {{банк}}
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('uds/uds_customer', 'user_token=' . $this->session->data['user_token'], 'SSL')
            )
        );

        $data['action'] = $this->url->link('uds/uds_customer', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL');
			
				if (isset($this->error['warning'])) {
					$data['error_warning'] = $this->error['warning'];
				} else {
					$data['error_warning'] = '';
				}

				if (isset($this->session->data['success'])) {
					$data['success'] = $this->session->data['success'];

					unset($this->session->data['success']);
				} else {
					$data['success'] = '';
				}

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['text_customer'] = 'Проверить данные клиента'; //$this->language->get('text_setting');

				$data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('uds/uds_customer', $data));
    }

//    private function validate() {
//        if (!$this->user->hasPermission('modify', 'uds/uds_setting')) {
//            $this->error['warning'] = $this->language->get('error_permission');
//        }
//        return !$this->error;
//    }

    public function getCustomerData() {
			
			$json = array();
			
			$code = $this->request->post['code'];
			$phone = $this->request->post['phone'];
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			$date = new DateTime();
			$url = 'https://api.uds.app/partner/v2/customers/find?uid=436e0ccd-4e80-417c-b4b8-da268f68b8b7';
			//$url = 'https://api.uds.app/partner/v2/customers/find?code=494164&phone=+79216497230&uid=436e0ccd-4e80-417c-b4b8-da268f68b8b7';

			$uuid_v4 = $this->uuid();

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';

//    	$date = new DateTime();
//			
//			$this->log->write($code);
//			$this->log->write($phone);
//			$this->log->write($url);
//			$this->log->write($uds_company_id);
//			$this->log->write($uds_api_key);
//			$this->log->write($date);
//
//			$json['success'] = $result; //'Проверка клиента:<br> Проверка';

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getCustomerByPhone() {
			
			$json = array();
			
			$phone = '+'.trim($this->request->post['phone']);
			$date = new DateTime();
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			//получать list пока не найдем phone
			
			$is_result = false;
			$id = false;
			$uid = false;
			$id_arr = array();
			$uid_arr = array();
			$displayName = '';
			
			$max = 1;
			$offset = 0;
			
			$url = 'https://api.uds.app/partner/v2/customers?max='.$max.'&offset='.$offset;
			$uuid_v4 = $this->uuid();

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);
			
			$total = $result_arr['total'];
			
			$max = 50;
			$offset = 0;
			$arr = array();
			while ($offset <= $total && !$is_result):
			//while ($offset <= $total):

				$url = 'https://api.uds.app/partner/v2/customers?max='.$max.'&offset='.$offset;
				$uuid_v4 = $this->uuid();

				$opts = array(
						'http' => array(
								'method' => 'GET',
								'header' => "Accept: application/json\r\n" .
														"Accept-Charset: utf-8\r\n" .
														"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
														"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
														"X-Timestamp: ".$date->format(DateTime::ATOM)
						)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				$result_arr = json_decode($result, true);
			
				foreach($result_arr['rows'] as $row){
					
					$arr[] = $row['phone'];
					
					if ($row['phone'] == $phone) {
						
						$is_result = true;
						$id = $row['participant']['id'];
						$uid = $row['uid'];
						$displayName = $row['displayName'];
						
					}

				}			
			
				$offset += 50;
			
			endwhile;
			
			if ($id) {
				
				$date = new DateTime();
				$url = 'https://api.uds.app/partner/v2/customers/'.$id;
				$uuid_v4 = $this->uuid();
//				$uds_company_id = $this->config->get('uds_company_id');
//				$uds_api_key = $this->config->get('uds_api_key');

				$opts = array(
						'http' => array(
								'method' => 'GET',
								'header' => "Accept: application/json\r\n" .
														"Accept-Charset: utf-8\r\n" .
														"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
														"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
														"X-Timestamp: ".$date->format(DateTime::ATOM)
						)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				$id_arr = json_decode($result, true);
				
			}
			
			if ($uid) {
				
				$date = new DateTime();
				$url = 'https://api.uds.app/partner/v2/customers/find?uid='.$uid;
				$uuid_v4 = $this->uuid();

				$opts = array(
						'http' => array(
								'method' => 'GET',
								'header' => "Accept: application/json\r\n" .
														"Accept-Charset: utf-8\r\n" .
														"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
														"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
														"X-Timestamp: ".$date->format(DateTime::ATOM)
						)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				$result_arr = json_decode($result, true);
				$uid_arr = json_decode($result, true);
				
			}
			
			
			
			$json['success']['total'] = $total;
			$json['success']['phone'] = $phone;
			$json['success']['id'] = $id;
			$json['success']['uid'] = $uid;
			$json['success']['name'] = $displayName;

			$json['success']['obj'] = ''; // $result;

      $json['success']['arr'] = '<pre>' . print_r($arr, 1) . '</pre>';
      $json['success']['id_arr'] = '<pre>' . print_r($id_arr, 1) . '</pre>';
      $json['success']['uid_arr'] = '<pre>' . print_r($uid_arr, 1) . '</pre>';
		
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getCustomerById() {
			
			$json = array();
			
			$date = new DateTime();
			//$customerId = '+79216497230';
			//$customerId = 1099539494164;
			$customerId = $this->request->post['code'];
			//$customerId = 494164;
			$url = 'https://api.uds.app/partner/v2/customers/'.$customerId;
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getCustomerByCode() {
			
			$json = array();
			
			$date = new DateTime();
			//$customerId = '+79216497230';
			//$customerId = 1099539494164;
			$customerId = '990591';
			$phone = '+'.trim($this->request->post['phone']);
			$code = $this->request->post['code'];
			$customerId = $this->request->post['code'];

			//$url = 'https://api.uds.app/partner/v2/customers/find?code='.$customerId;
			$url = 'https://api.uds.app/partner/v2/customers/find?code='.$code.'&exchangeCode=true&total=10'; 
			//$url = 'https://api.uds.app/partner/v2/customers/find?code='.$customerId.'&total=1000&skipLoyaltyTotal=1000'; 
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
		private function uuid() {
	 $uuid = array(
		'time_low'  => 0,
		'time_mid'  => 0,
		'time_hi'  => 0,
		'clock_seq_hi' => 0,
		'clock_seq_low' => 0,
		'node'   => array()
	 );

	 $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
	 $uuid['time_mid'] = mt_rand(0, 0xffff);
	 $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
	 $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
	 $uuid['clock_seq_low'] = mt_rand(0, 255);

	 for ($i = 0; $i < 6; $i++) {
		$uuid['node'][$i] = mt_rand(0, 255);
	 }

	 $uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
		$uuid['time_low'],
		$uuid['time_mid'],
		$uuid['time_hi'],
		$uuid['clock_seq_hi'],
		$uuid['clock_seq_low'],
		$uuid['node'][0],
		$uuid['node'][1],
		$uuid['node'][2],
		$uuid['node'][3],
		$uuid['node'][4],
		$uuid['node'][5]
	 );

	 return $uuid;
		
		}
	
	
}
<?php
class ControllerUdsUdsProduct extends Controller {
    private $error = array();

    public function index() {
        //$this->load->language('uds/uds_setting');
        $this->document->setTitle('Товары и категории магазина'); //($this->language->get('heading_title'));
			
		$this->load->model('uds/uds');

        $data['heading_title'] = 'Товары и категории магазина'; //$this->language->get('heading_title');
			
		$data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();
        array_push($data['breadcrumbs'],
            array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL')
            ),
            array(
                'text' => 'Товары и категории магазина',
                'href' => $this->url->link('uds/uds_product', 'user_token=' . $this->session->data['user_token'], 'SSL')
            )
        );

        $data['action'] = $this->url->link('uds/uds_product', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL');
			
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['total_uds_expert_mode'] = $this->config->get('total_uds_expert_mode');
		
		$data['total_uds_license_server'] = $this->config->get('total_uds_license_server');
		$data['total_uds_license_key'] = $this->config->get('total_uds_license_key');
		$data['total_uds_company_id'] = $this->config->get('total_uds_company_id');
		$data['total_uds_api_key'] = $this->config->get('total_uds_api_key');

        $data['text_customer'] = 'Проверить данные магазина'; //$this->language->get('text_setting');
			
		$data['categories'] = array();
		$filter_data = array('sort'=>'fullname','order'=>'ASC');
		$results = $this->model_uds_uds->getAllCategories($filter_data);
		foreach ($results as $result) {
			$level = count(explode("&and;", $result['fullname']));
			$data['categories'][] = array(
				'category_id' 	=> $result['category_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
				'level' 		=> $level, 
				'parent_id' 	=> $result['parent_id'], 
				'product_count' => $result['product_count'], 
				'sort_order' 	=> $result['sort_order'], 
				'status' 		=> $result['status'] 
			);
		}

		$data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('uds/uds_product', $data));
    }
	
	public function getProducts() {
		$json = array();
		
		$this->load->model('uds/uds');
		
		$products = array();
		$results = $this->model_uds_uds->getProductsByCategoryId($this->request->post['category_id']);
		
		foreach ($results as $result) {
			
			$img_arr = array();
			if (isset($result['image'])) {
				$img_arr[] = HTTPS_CATALOG . 'image/' . $result['image'];
			}
			
			$images = $this->model_uds_uds->getProductImages($result['product_id']);
			
			foreach ($images as $image) {
				$img_arr[] = HTTPS_CATALOG . 'image/' . $image['image'];
				if (count($img_arr) == 5) break;
			}
			
			$img_string = implode(',', $img_arr);
			
			$special = false;
			$product_specials = $this->model_uds_uds->getProductSpecials($result['product_id']);

			foreach ($product_specials as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $product_special['price'];
					break;
				}
			}
			
			$option_id = 1;
			$options_groups = $this->model_uds_uds->getProductOptions($result['product_id'], $option_id);
			
			$opt_string = '';
			
			if ($options_groups) {
				
				$quantity = 0;
				
				$options = array();
				foreach ($options_groups[0]['product_option_value'] as $option) {
					
					$quantity += $option['quantity'];
					
					if ($option['quantity'] > 0 || $this->config->get('total_uds_empty_stock')) {
						
						$opt_string .= ',' . $option['name'];
						
						$options[] = array(
							'name'		=> $option['name'],
							'quantity'	=> $option['quantity']
						);
					}
				}
				
				$opt_string = trim($opt_string, ",");
			
			} else {
				
				$quantity = $result['quantity'];	
				
			}
			
			$price = 0;
			if ($special) {
				$price = min($result['price'], $special);	
			} else {
				$price = $result['price'];	
			}
			
			if ($quantity > 0 || $this->config->get('total_uds_empty_stock')) {
				$products[] = array(
					'product_id'	=> $result['product_id']
					,'name'			=> $result['name']
					,'model'		=> $result['model']
					,'price'		=> $price
					,'opt_string'	=> $opt_string
					,'img_string'	=> $img_string
				);
			}
		}

		$json['success']['products'] = $products;
		$json['success']['products_arr'] = '<pre>' . print_r($products, 1) . '</pre>';
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getCatalog() {
		$json = array();
		
		$this->load->model('uds/uds');
		
		$catalog = array();
		$results = $this->model_uds_uds->getCatalog();
		
		if ($results) {
			
			foreach ($results as $result) {
				
				if ($result['type'] != 'CATEGORY') {
					
//					$product = $this->model_uds_uds->getProductById($result['oc_id']);
//					
//					$img_arr = array();
//					if (isset($result['image'])) {
//						$img_arr[] = HTTPS_CATALOG . 'image/' . $product['image'];
//					}
//					
//					$images = $this->model_uds_uds->getProductImages($product['product_id']);
//					
//					foreach ($images as $image) {
//						$img_arr[] = HTTPS_CATALOG . 'image/' . $image['image'];
//						if (count($img_arr) == 5) break;
//					}
//					
//					$img_string = implode(',', $img_arr);
					
					$img_string = '';
					
					$special = false;
					$product_specials = $this->model_uds_uds->getProductSpecials($product['product_id']);
		
					foreach ($product_specials as $product_special) {
						if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
							$special = $product_special['price'];
							break;
						}
					}
					
					$option_id = 1;
					$options_groups = $this->model_uds_uds->getProductOptions($product['product_id'], $option_id);
					
					$opt_string = '';
					
					if ($options_groups) {
						
						$quantity = 0;
						
						$options = array();
						foreach ($options_groups[0]['product_option_value'] as $option) {
							
							$quantity += $option['quantity'];
							
							if ($option['quantity'] > 0 || $this->config->get('total_uds_empty_stock')) {
								
								$opt_string .= ',' . $option['name'];
								
								$options[] = array(
									'name'		=> $option['name'],
									'quantity'	=> $option['quantity']
								);
							}
						}
						
						$opt_string = trim($opt_string, ",");
					
					} else {
						
						$quantity = $product['quantity'];	
						
					}
					
					$price = 0;
					if ($special) {
						$price = min($product['price'], $special);	
					} else {
						$price = $product['price'];	
					}
					
					$sku = $product['sku'];
					
					if ($quantity > 0 || $this->config->get('total_uds_empty_stock')) {
					
						$catalog[] = array(
							'type'			=> $result['type']
							,'oc_id'		=> $result['oc_id']
							,'uds_id'		=> $result['uds_id']
							,'nodeId'		=> $result['nodeId']
							,'externalId'	=> $result['externalId']
							,'oc_name'		=> $result['oc_name']
							,'uds_name'		=> $result['uds_name']
							,'status'		=> $result['status']
							,'hidden'		=> $result['hidden']
							,'blocked'		=> $result['blocked']
							,'sku'			=> $sku
							,'price'		=> $price
							,'opt_string'	=> $opt_string
							,'img_string'	=> $img_string
						);
						
					}
					
				}
				
			}
				
			$json['success']['catalog'] = $catalog;
			$json['success']['catalog_arr'] = '<pre>' . print_r($catalog, 1) . '</pre>';
		
		} else {
			
			$json['error'] = 'В каталоге UDS нет товаров';
			
		}
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function setCategories() {
		$json = array();
		
		$this->load->model('uds/uds');
		
		$this->log->write($this->request->post['categories']);
		
		$categories = json_decode(html_entity_decode($this->request->post['categories'])); 
		
		$this->model_uds_uds->deleteCategories();
		
		foreach ($categories as $category) {
			
			$category = json_decode(json_encode($category), true);

			if ($category['externalId']{0} == 'c') {
				$oc_id = (int)substr($category['externalId'], 1);
				$oc_name = $this->model_uds_uds->getCategoryName($oc_id);
			} else {
				$oc_id = 0;	
				$oc_name = '';
			}
			
			if (!$oc_name) {
				$oc_name = $category['name'];
			}
			
			$data = [];
			$data['type'] = $category['type'];
			$data['oc_id'] = $oc_id;
			$data['uds_id'] = $category['id'];
			$data['nodeId'] = '';
			$data['externalId'] = $category['externalId'];
			$data['oc_name'] = $oc_name;
			$data['uds_name'] = $category['name'];
			$data['status'] = 1;
			$data['hidden'] = $category['hidden'];
			$data['blocked'] = '';
			
			$result = $this->model_uds_uds->getCategory($category['id']);
			
			if ($result) {
				$this->model_uds_uds->editCategory($result['externalId'], $data);
			} else {
				$this->model_uds_uds->addCategory($data);
			}
			
		}

		$this->log->write('Записано / обновлено ' . count($categories) . ' категорий');

		$json['success']['text'] = 'Записано / обновлено ' . count($categories) . ' категорий';
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	public function setProducts() {
		$json = array();
		
		$this->load->model('uds/uds');
		
		$this->log->write($this->request->post['products']);
		
		$products = json_decode(html_entity_decode($this->request->post['products'])); 
		$nodeId = $this->request->post['nodeId']; 
		
		$this->model_uds_uds->deleteProducts($nodeId);
		
		foreach ($products as $product) {
			
			$product = json_decode(json_encode($product), true);
			
			if ($product['externalId']{0} == 'p') {
				$oc_id = (int)substr($product['externalId'], 1);
				$oc_name = $this->model_uds_uds->getProductName($oc_id);
			} else {
				$oc_id = 0;	
				$oc_name = '';
			}
			
			if (!$oc_name) {
				$oc_name = $product['name'];
			}
			
			$data = [];
			$data['type'] = $product['type'];
			$data['oc_id'] = $oc_id;
			$data['uds_id'] = $product['id'];
			$data['nodeId'] = $product['nodeId'];
			$data['externalId'] = $product['externalId'];
			$data['oc_name'] = $oc_name;
			$data['uds_name'] = $product['name'];
			$data['status'] = 1;
			$data['hidden'] = $product['hidden'];
			$data['blocked'] = $product['blocked'];
			
			$result = $this->model_uds_uds->getProduct($product['id']);
			
			if ($result) {
				$this->model_uds_uds->editProduct($result['externalId'], $data);
			} else {
				$this->model_uds_uds->addProduct($data);
			}
			
		}

		$this->log->write('Записано / обновлено ' . count($products) . ' товаров');

		$json['success']['text'] = 'Записано / обновлено ' . count($products) . ' товаров';
		$json['success']['count'] = count($products);
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

}
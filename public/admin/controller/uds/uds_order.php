<?php

error_reporting(E_ALL & ~E_NOTICE);

class ControllerUdsUdsOrder extends Controller {
    private $error = array();

    public function index() {

			$this->document->setTitle('Заказ клиента'); //($this->language->get('heading_title'));

        $data['heading_title'] = 'Заказ клиента'; //$this->language->get('heading_title');
			
				$data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();
        array_push($data['breadcrumbs'],
            array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL')
            ),
            array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('uds/uds_order', 'user_token=' . $this->session->data['user_token'], 'SSL')
            )
        );

        $data['action'] = $this->url->link('uds/uds_order', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL');
			
				if (isset($this->error['warning'])) {
					$data['error_warning'] = $this->error['warning'];
				} else {
					$data['error_warning'] = '';
				}

				if (isset($this->session->data['success'])) {
					$data['success'] = $this->session->data['success'];

					unset($this->session->data['success']);
				} else {
					$data['success'] = '';
				}

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['text_customer'] = 'Проверить данные клиента'; //$this->language->get('text_setting');

				$data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('uds/uds_order', $data));
    }

//    private function validate() {
//        if (!$this->user->hasPermission('modify', 'uds/uds_setting')) {
//            $this->error['warning'] = $this->language->get('error_permission');
//        }
//        return !$this->error;
//    }

    public function getCustomerData() {
			
			$json = array();
			
			$code = $this->request->post['code'];
			$phone = $this->request->post['phone'];
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			$date = new DateTime();
			$url = 'https://api.uds.app/partner/v2/customers/find?uid=436e0ccd-4e80-417c-b4b8-da268f68b8b7';
			//$url = 'https://api.uds.app/partner/v2/customers/find?code=494164&phone=+79216497230&uid=436e0ccd-4e80-417c-b4b8-da268f68b8b7';

			$uuid_v4 = $this->uuid();

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';

//    	$date = new DateTime();
//			
//			$this->log->write($code);
//			$this->log->write($phone);
//			$this->log->write($url);
//			$this->log->write($uds_company_id);
//			$this->log->write($uds_api_key);
//			$this->log->write($date);
//
//			$json['success'] = $result; //'Проверка клиента:<br> Проверка';

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getCustomerByPhone() {
			
			$json = array();
			
			$phone = '+'.trim($this->request->post['phone']);
			$date = new DateTime();
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			//получать list пока не найдем phone
			
			$is_result = false;
			$id = false;
			$uid = false;
			$id_arr = array();
			$uid_arr = array();
			$displayName = '';
			
			$max = 1;
			$offset = 0;
			
			$url = 'https://api.uds.app/partner/v2/customers?max='.$max.'&offset='.$offset;
			$uuid_v4 = $this->uuid();

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);
			
			$total = $result_arr['total'];
			
			$max = 50;
			$offset = 0;
			$arr = array();
			while ($offset <= $total && !$is_result):
			//while ($offset <= $total):

				$url = 'https://api.uds.app/partner/v2/customers?max='.$max.'&offset='.$offset;
				$uuid_v4 = $this->uuid();

				$opts = array(
						'http' => array(
								'method' => 'GET',
								'header' => "Accept: application/json\r\n" .
														"Accept-Charset: utf-8\r\n" .
														"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
														"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
														"X-Timestamp: ".$date->format(DateTime::ATOM)
						)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				$result_arr = json_decode($result, true);
			
				foreach($result_arr['rows'] as $row){
					
					$arr[] = $row['phone'];
					
					if ($row['phone'] == $phone) {
						
						$is_result = true;
						$id = $row['participant']['id'];
						$uid = $row['uid'];
						$displayName = $row['displayName'];
						
					}

				}			
			
				$offset += 50;
			
			endwhile;
			
			if ($id) {
				
				$date = new DateTime();
				$url = 'https://api.uds.app/partner/v2/customers/'.$id;
				$uuid_v4 = $this->uuid();
//				$uds_company_id = $this->config->get('uds_company_id');
//				$uds_api_key = $this->config->get('uds_api_key');

				$opts = array(
						'http' => array(
								'method' => 'GET',
								'header' => "Accept: application/json\r\n" .
														"Accept-Charset: utf-8\r\n" .
														"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
														"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
														"X-Timestamp: ".$date->format(DateTime::ATOM)
						)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				$id_arr = json_decode($result, true);
				
			}
			
			if ($uid) {
				
				$date = new DateTime();
				$url = 'https://api.uds.app/partner/v2/customers/find?uid='.$uid;
				$uuid_v4 = $this->uuid();

				$opts = array(
						'http' => array(
								'method' => 'GET',
								'header' => "Accept: application/json\r\n" .
														"Accept-Charset: utf-8\r\n" .
														"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
														"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
														"X-Timestamp: ".$date->format(DateTime::ATOM)
						)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				$result_arr = json_decode($result, true);
				$uid_arr = json_decode($result, true);
				
			}
			
			
			
			$json['success']['total'] = $total;
			$json['success']['phone'] = $phone;
			$json['success']['id'] = $id;
			$json['success']['uid'] = $uid;
			$json['success']['name'] = $displayName;

			$json['success']['obj'] = ''; // $result;

      $json['success']['arr'] = '<pre>' . print_r($arr, 1) . '</pre>';
      $json['success']['id_arr'] = '<pre>' . print_r($id_arr, 1) . '</pre>';
      $json['success']['uid_arr'] = '<pre>' . print_r($uid_arr, 1) . '</pre>';
		
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getCustomerById() {
			
			$json = array();
			
			$date = new DateTime();
			//$customerId = '+79216497230';
			//$customerId = 1099539494164;
			$customerId = $this->request->post['code'];
			//$customerId = 494164;
			$url = 'https://api.uds.app/partner/v2/customers/'.$customerId;
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getOperationByCode() {
			
			$json = array();
			
			$date = new DateTime();
			$phone = '+'.trim($this->request->post['phone']);
			$code = $this->request->post['code'];
			$total = $this->request->post['total'];
			$skip = $this->request->post['skip'];

			//$url = 'https://api.uds.app/partner/v2/customers/find?code='.$customerId;
			$url = 'https://api.uds.app/partner/v2/customers/find?code='.$code.'&exchangeCode=true&total='.$total.'&skipLoyaltyTotal='.$skip; 
			//$url = 'https://api.uds.app/partner/v2/customers/find?code='.$customerId.'&total=1000&skipLoyaltyTotal=1000'; 
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';
			
			$json['success']['name'] = $result_arr['user']['displayName'];
			$json['success']['phone'] = $result_arr['user']['phone'];
			$json['success']['uid'] = $result_arr['user']['uid'];
			$json['success']['id'] = $result_arr['user']['participant']['id'];
			$json['success']['totalPoints'] = $result_arr['user']['participant']['points'];
			$json['success']['cashbackRate'] = $result_arr['user']['participant']['cashbackRate'];
			$json['success']['cash'] = $result_arr['purchase']['cash'];
			$json['success']['maxPoints'] = $result_arr['purchase']['maxPoints'];
			$json['success']['points'] = $result_arr['purchase']['points'];
			$json['success']['cashBack'] = $result_arr['purchase']['cashBack'];
			$json['success']['orderTotal'] = $result_arr['purchase']['total'];
			$json['success']['skipLoyaltyTotal'] = $result_arr['purchase']['skipLoyaltyTotal'];
			$json['success']['pointsPercent'] = $result_arr['purchase']['pointsPercent'];
			$json['success']['operationCode'] = $result_arr['code'];
			
			/*
			Array
(
    [user] => Array
        (
            [phone] => +79216497230
            [gender] => NOT_SPECIFIED
            [uid] => 436e0ccd-4e80-417c-b4b8-da268f68b8b7
            [birthDate] => 
            [avatar] => 
            [participant] => Array
                (
                    [cashbackRate] => 10
                    [dateCreated] => 2020-07-20T14:59:32.764Z
                    [discountRate] => 0
                    [id] => 1099539494164
                    [inviterId] => 
                    [lastTransactionTime] => 2020-08-13T08:18:18.290Z
                    [membershipTier] => Array
                        (
                            [conditions] => Array
                                (
                                    [effectiveInvitedCount] => 
                                    [totalCashSpent] => 
                                )

                            [name] => Новичок🥳
                            [rate] => 10
                            [uid] => base
                        )

                    [points] => 1040.23
                )

            [displayName] => Villa Rasa
        )

    [code] => 9b2bc48d-d3fc-4f97-a95a-2a34f8f769d9
    [purchase] => Array
        (
            [cash] => 50
            [discountAmount] => 0
            [certificatePoints] => 0
            [maxPoints] => 50
            [netDiscount] => 50
            [points] => 50
            [netDiscountPercent] => 50
            [cashBack] => 5
            [total] => 100
            [skipLoyaltyTotal] => 0
            [pointsPercent] => 50
            [discountPercent] => 0
        )

)
*/

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function getOperationInfo() {
			
			$json = array();
			
			$date = new DateTime();
			$url = 'https://api.uds.app/partner/v2/operations/calc';
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			//$code = $this->request->post['code'] . '';
			$code = $this->request->post['operationCode'] . '';
			$uid = $this->request->post['uid'] . '';
			$phone = '+'.trim($this->request->post['phone']);
			$orderTotal = (float)$this->request->post['orderTotal'];
			$points = (float)$this->request->post['points'];
			$skipLoyaltyTotal = (float)$this->request->post['skipLoyaltyTotal'];

			$postData = json_encode(
				array(
					'code' => $code,
					'participant' => null,
//					'participant' => array(
//						'uid' => $uid,
//						'phone' => $phone
//					),
					'receipt' => array(
						'total' => $orderTotal,
						'points' => $points //,
						//'skipLoyaltyTotal' => null // $skipLoyaltyTotal
					)
				)
			);

			$opts = array(
					'http' => array(
							'method' => 'POST',
							'header' => "Content-Type: application/json\r\n".
													"Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM),
							'content' => $postData,
							'ignore_errors' => true
					)
			);

			
$this->log->write($opts);			
			
			
			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
			
			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';
			
/*
			Array
(
    [purchase] => Array
        (
            [cash] => 50
            [discountAmount] => 0
            [certificatePoints] => 0
            [maxPoints] => 50
            [netDiscount] => 50
            [points] => 50
            [netDiscountPercent] => 50
            [cashBack] => 5
            [total] => 100
            [skipLoyaltyTotal] => 0
            [pointsPercent] => 50
            [discountPercent] => 0
        )

    [user] => Array
        (
            [phone] => +79216497230
            [gender] => NOT_SPECIFIED
            [uid] => 436e0ccd-4e80-417c-b4b8-da268f68b8b7
            [birthDate] => 
            [avatar] => 
            [participant] => Array
                (
                    [cashbackRate] => 10
                    [dateCreated] => 2020-07-20T14:59:32.764Z
                    [discountRate] => 0
                    [id] => 1099539494164
                    [inviterId] => 
                    [lastTransactionTime] => 2020-08-13T08:18:18.290Z
                    [membershipTier] => Array
                        (
                            [conditions] => Array
                                (
                                    [effectiveInvitedCount] => 
                                    [totalCashSpent] => 
                                )

                            [name] => Новичок🥳
                            [rate] => 10
                            [uid] => base
                        )

                    [points] => 1040.23
                )

            [displayName] => Villa Rasa
        )

)
*/			
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function setOperationByCode() {
			
			$json = array();
			
			$date = new DateTime();
			$url = 'https://api.uds.app/partner/v2/operations';
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			$uds_cashier_id = $this->config->get('total_uds_cashier_id');
			$uds_cashier_name = $this->config->get('total_uds_cashier_name');
			
			$code = $this->request->post['operationCode'] . '';
			$uid = $this->request->post['uid'] . '';
			$phone = '+'.trim($this->request->post['phone']);
			$nonce = $this->uuid(); 
			$orderTotal = (float)$this->request->post['orderTotal'];
			$cash = (float)$this->request->post['cash'];
			$points = (float)$this->request->post['points'];
			$number = '123456'; 
			$skipLoyaltyTotal = (float)$this->request->post['skipLoyaltyTotal'];
			
			$receipt = array('total' => $orderTotal,'cash' => $cash,'points' => $points,'number' => $number);
			if ($skipLoyaltyTotal > 0) $receipt['skipLoyaltyTotal'] = $skipLoyaltyTotal;
			
			$cashier = null;
			if ($uds_cashier_id != '') {
				$cashier = array('externalId' => $uds_cashier_id);
				if ($uds_cashier_name != '') {
					$cashier['name'] = $uds_cashier_name;
				}
			}

			$postData = json_encode(
				array(
					'code' => $code, //'string',
					'participant' => null,
//					'participant' => array(
//			      'uid' => $uid, //'string',
//			      'phone' => $phone //'string'
//			    ),
					'nonce' => $nonce, //'string',
//					'cashier' => null,
			    'cashier' => $cashier,
//			    'cashier' => array(
//			      //'externalId' => $uds_cashier_id
//			      //,
//						'name' => 'ПОКУПКА НА САЙТЕ' //$uds_cashier_name
//			    ),
					'receipt' => $receipt
				)
			);

			$opts = array(
					'http' => array(
							'method' => 'POST',
							'header' => "Content-Type: application/json\r\n".
													"Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM),
							'content' => $postData,
							'ignore_errors' => true
					)
			);
			
$this->log->write($opts);			
			
			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r($result_arr, 1) . '</pre>';
			$json['success']['operationId'] = $result_arr['id'];
			
/*
Array
(
    [id] => 182497415
    [dateCreated] => 2020-08-13T08:18:18.290Z
    [action] => PURCHASE
    [state] => NORMAL
    [customer] => Array
        (
            [id] => 1099539494164
            [uid] => 436e0ccd-4e80-417c-b4b8-da268f68b8b7
            [displayName] => Villa Rasa
            [membershipTier] => Array
                (
                    [uid] => base
                    [name] => Новичок🥳
                    [conditions] => Array
                        (
                            [totalCashSpent] => 
                        )

                    [rate] => 10
                )

        )

    [cashier] => Array
        (
            [id] => 274878336547
            [displayName] => Покупка на сайте
        )

    [points] => -97.75
    [cash] => 402.25
    [total] => 500
    [receiptNumber] => 123456
)
*/			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    public function setOperationByPhone() {
			
			$json = array();
			
			$date = new DateTime();
			$url = 'https://api.uds.app/partner/v2/operations';
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			$uds_cashier_id = $this->config->get('total_uds_cashier_id');
			$uds_cashier_name = $this->config->get('total_uds_cashier_name');
			
			$code = $this->request->post['operationCode'] . '';
			$uid = $this->request->post['uid'] . '';
			$phone = '+'.trim($this->request->post['phone']);
			$nonce = $this->uuid(); 
			$orderTotal = (float)$this->request->post['orderTotal'];
			$cash = (float)$this->request->post['cash'];
			$points = 0.0; //(float)$this->request->post['points'];
			$number = '1234567'; 
			$skipLoyaltyTotal = (float)$this->request->post['skipLoyaltyTotal'];
			
			$receipt = array('total' => $orderTotal,'cash' => $cash,'points' => $points,'number' => $number);
			if ($skipLoyaltyTotal > 0) $receipt['skipLoyaltyTotal'] = $skipLoyaltyTotal;
			
			$cashier = null;
			if ($uds_cashier_id != '') {
				$cashier = array('externalId' => $uds_cashier_id);
				if ($uds_cashier_name != '') {
					$cashier['name'] = $uds_cashier_name;
				}
			}

			$postData = json_encode(
				array(
					'code' => null, //$code, //'string',
//					'participant' => null,
					'participant' => array(
//			      'uid' => $uid, //'string',
			      'phone' => $phone //'string'
			    ),
					'nonce' => $nonce, //'string',
//					'cashier' => null,
			    'cashier' => $cashier,
//			    'cashier' => array(
//			      //'externalId' => $uds_cashier_id
//			      //,
//						'name' => 'ПОКУПКА НА САЙТЕ' //$uds_cashier_name
//			    ),
					'receipt' => $receipt
				)
			);

			$opts = array(
					'http' => array(
							'method' => 'POST',
							'header' => "Content-Type: application/json\r\n".
													"Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM),
							'content' => $postData,
							'ignore_errors' => true
					)
			);
			
$this->log->write($opts);			
			
			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);
			
			$addPoints = $result_arr['cash'] / 100 * $result_arr['customer']['membershipTier']['rate'];

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r($result_arr, 1) . '</pre>';
			$json['success']['operationId'] = $result_arr['id'];
			$json['success']['addPoints'] = $addPoints;
			
/*
Array
(
    [id] => 182559161
    [dateCreated] => 2020-08-13T10:30:35.676Z
    [action] => PURCHASE
    [state] => NORMAL
    [customer] => Array
        (
            [id] => 1099539494164
            [uid] => 436e0ccd-4e80-417c-b4b8-da268f68b8b7
            [displayName] => Villa Rasa
            [membershipTier] => Array
                (
                    [uid] => base
                    [name] => Новичок🥳
                    [conditions] => Array
                        (
                            [totalCashSpent] => 
                        )

                    [rate] => 10
                )

        )

    [cashier] => Array
        (
            [id] => 274878336547
            [displayName] => Покупка на сайте
        )

    [points] => 0
    [cash] => 500
    [total] => 500
    [receiptNumber] => 1234567
)
*/			
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
    
	public function refundOperation() {
			
			$json = array();
			
			$operationId = $this->request->post['operationId'];
			$partialAmount = (float)$this->request->post['partialAmount'];
			
			$date = new DateTime();
			$url = 'https://api.uds.app/partner/v2/operations/'.$operationId.'/refund';
			$uuid_v4 = $this->uuid();
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');

			$postData = json_encode(
				array(
					'partialAmount' => $partialAmount
				)
			);

			$opts = array(
					'http' => array(
							'method' => 'POST',
							'header' => "Content-Type: application/json\r\n".
													"Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM),
							'content' => $postData,
							'ignore_errors' => true
					)
			);


$this->log->write($opts);			
			
			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r($result_arr, 1) . '</pre>';
			//$json['success']['operationId'] = $result_arr['id'];
			
/*
Array
(
    [id] => 182532083
    [dateCreated] => 2020-08-13T09:16:11.203Z
    [action] => PURCHASE
    [state] => REVERSAL
    [customer] => Array
        (
            [id] => 1099539494164
            [uid] => 436e0ccd-4e80-417c-b4b8-da268f68b8b7
            [displayName] => Villa Rasa
            [membershipTier] => 
        )

    [cashier] => Array
        (
            [id] => 274878336547
            [displayName] => Покупка на сайте
        )

    [points] => 100
    [cash] => -100
    [total] => -200
    [receiptNumber] => 
    [origin] => Array
        (
            [id] => 182509751
        )

)
*/			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
		
	
	
	private function uuid() {
	 $uuid = array(
		'time_low'  => 0,
		'time_mid'  => 0,
		'time_hi'  => 0,
		'clock_seq_hi' => 0,
		'clock_seq_low' => 0,
		'node'   => array()
	 );

	 $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
	 $uuid['time_mid'] = mt_rand(0, 0xffff);
	 $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
	 $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
	 $uuid['clock_seq_low'] = mt_rand(0, 255);

	 for ($i = 0; $i < 6; $i++) {
		$uuid['node'][$i] = mt_rand(0, 255);
	 }

	 $uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
		$uuid['time_low'],
		$uuid['time_mid'],
		$uuid['time_hi'],
		$uuid['clock_seq_hi'],
		$uuid['clock_seq_low'],
		$uuid['node'][0],
		$uuid['node'][1],
		$uuid['node'][2],
		$uuid['node'][3],
		$uuid['node'][4],
		$uuid['node'][5]
	 );

	 return $uuid;
		
		}
	
	
}
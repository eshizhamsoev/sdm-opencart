<?php
class ControllerUdsUdsCompany extends Controller {
    private $error = array();

    public function index() {
        //$this->load->language('uds/uds_setting');
        $this->document->setTitle('Проверка магазин'); //($this->language->get('heading_title'));
        //$this->load->model('setting/setting');

//        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
//            $this->model_setting_setting->editSetting('uds', $this->request->post);
//            $this->session->data['success'] = $this->language->get('text_success');
//					
//						$this->testInstall();
//					
//            $this->response->redirect($this->url->link('uds/uds_setting', 'user_token=' . $this->session->data['user_token'], 'SSL'));
//        }

        $data['heading_title'] = 'Проверка магазина'; //$this->language->get('heading_title');
			
				$data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();
        array_push($data['breadcrumbs'],
            array(  // Главная
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL')
            ),
            array(  // Оплата через {{банк}}
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('uds/uds_company', 'user_token=' . $this->session->data['user_token'], 'SSL')
            )
        );

        $data['action'] = $this->url->link('uds/uds_company', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'SSL');
			
				if (isset($this->error['warning'])) {
					$data['error_warning'] = $this->error['warning'];
				} else {
					$data['error_warning'] = '';
				}

				if (isset($this->session->data['success'])) {
					$data['success'] = $this->session->data['success'];

					unset($this->session->data['success']);
				} else {
					$data['success'] = '';
				}

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['text_customer'] = 'Проверить данные магазина'; //$this->language->get('text_setting');

				$data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('uds/uds_company', $data));
    }

    public function getCompanyData() {
			
			$json = array();
			
//			$code = $this->request->post['code'];
//			$phone = $this->request->post['phone'];
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			$date = new DateTime();
			
//			$this->log->write($code);
//			$this->log->write($phone);
//			$this->log->write($uds_url);
//			$this->log->write($uds_company_id);
//			$this->log->write($uds_api_key);
//			//$this->log->write($date);
//			$this->log->write($date->format(DateTime::ATOM));
			
			
//			$date = new DateTime();
////$url = 'https://api.uds.app/partner/v2/customers/find?code=456123&phone=+71234567898&uid=23684cea-ca50-4c5c-b399-eb473e85b5ad';
//$url = 'https://api.uds.app/partner/v2/customers/find?phone=+79216497230&uid=23683cea-ca50-4c5c-b399-eb473e85b5ad';// . $phone; //
//$uuid_v4 = 'd03451e2-fd78-4bda-b609-e42bb55a654a'; // 'UUID'; //generate universally unique identifier version 4 (RFC 4122)
//$companyId = $uds_company_id; //123; //set ID of your company
//$apiKey = $uds_api_key; //'ZDk1MmE5NzMtODcxNi00YzIwLTllZDMtN2ZjM2NjOGI2YTMz'; //set company apikey
//
//
//			$this->log->write($url);
//			$this->log->write($uuid_v4);
//			$this->log->write($companyId);
//			$this->log->write($apiKey);
//			
//			
//			// Create a stream
//
//$opts = array(
//    'http' => array(
//        'method' => 'GET',
//        'header' => "Accept: application/json\r\n" .
//                    "Accept-Charset: utf-8\r\n" .
//                    "Authorization: Basic ". base64_encode("$companyId:$apiKey")."\r\n" .
//                    "X-Origin-Request-Id: ".$uuid_v4."\r\n" .
//                    "X-Timestamp: ".$date->format(DateTime::ATOM)
//    )
//);
//
//$context = stream_context_create($opts);
//$result = file_get_contents($url, false, $context);


			
			
			
//    $date = new DateTime();
//    $url = 'https://udsgame.com/v1/partner/customer?code=+79216497230'; //.$_POST['uds_code'];
//$uuid_v4 = '23684cea-ca90-4c5c-b359-eb473e85b5ad'; // 'UUID'; //generate universally unique identifier version 4 (RFC 4122)
////$companyId = $uds_company_id; //123; //set ID of your company
//$apiKey = $uds_api_key; //'ZDk1MmE5NzMtODcxNi00YzIwLTllZDMtN2ZjM2NjOGI2YTMz'; //set company apikey
//
//    // Create a stream
//    $opts = array(
//        'http' => array(
//            'method' => 'GET',
//            'header' => "Accept: application/json\r\n" .
//                        "Accept-Charset: utf-8\r\n" .
//                        "X-Api-Key: ".$apiKey."\r\n" .
//                        "X-Origin-Request-Id: ".$uuid_v4."\r\n" .
//                        "X-Timestamp: ".$date->format(DateTime::ATOM)
//        )
//    );
			
			
			
			
			
			
$url = 'https://api.uds.app/partner/v2/settings';
$uuid_v4 = $this->uuid(); //'d03451e2-fd78-4bda-b609-e42bb55a654a'; //generate universally unique identifier version 4 (RFC 4122)

$opts = array(
    'http' => array(
        'method' => 'GET',
        'header' => "Accept: application/json\r\n" .
                    "Accept-Charset: utf-8\r\n" .
                    "Authorization: Basic ".base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
                    "X-Origin-Request-Id: ".$uuid_v4."\r\n" .
                    "X-Timestamp: ".$date->format(DateTime::ATOM)
    )
);

$context = stream_context_create($opts);
    
$result = file_get_contents($url, false, $context);
			

			
			
			$this->log->write($uds_url);
			$this->log->write($uds_company_id);
			$this->log->write($uds_api_key);
			$this->log->write($uuid_v4);
			$this->log->write($date->format(DateTime::ATOM));
			
			
			

//			$curl = curl_init(OPENCART_SERVER . 'index.php?route=marketplace/api' . $url);
//
//			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
//			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//			curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
//			curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
//			curl_setopt($curl, CURLOPT_POST, 1);
//
//			$response = curl_exec($curl);
//
//			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//
//			curl_close($curl);
//
//			$response_info = json_decode($response, true);
//
//			$extension_total = $response_info['extension_total'];
			
			$json['success']['obj'] = $result;
			
      $result_arr = json_decode($result, true);
			
			$json['success']['id'] = $result_arr['id'];
			
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';
			
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

		}
	
    public function getCustomerList() {
			
			$json = array();
	
	$date = new DateTime();
			
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			
			$url = 'https://api.uds.app/partner/v2/customers?max=30&offset=0';
			$uuid_v4 = $this->uuid();
//			$companyId = $this->config->get('uds_company_id');
//			$apiKey = $this->config->get('uds_api_key');

			// Create a stream

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';
		
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
    
		public function getTransList() {
			
			$json = array();
	
			$date = new DateTime();
			
			$uds_url = 'https://api.uds.app/partner/v2';
			$uds_company_id = $this->config->get('total_uds_company_id');
			$uds_api_key = $this->config->get('total_uds_api_key');
			
			$url = 'https://api.uds.app/partner/v2/operations?max=50&offset=0';
			$uuid_v4 = $this->uuid();

			$opts = array(
					'http' => array(
							'method' => 'GET',
							'header' => "Accept: application/json\r\n" .
													"Accept-Charset: utf-8\r\n" .
													"Authorization: Basic ". base64_encode("$uds_company_id:$uds_api_key")."\r\n" .
													"X-Origin-Request-Id: ".$uuid_v4."\r\n" .
													"X-Timestamp: ".$date->format(DateTime::ATOM)
					)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($url, false, $context);
      $result_arr = json_decode($result, true);

			$json['success']['obj'] = $result;
      $json['success']['arr'] = '<pre>' . print_r(json_decode($result, true), 1) . '</pre>';
		
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));

		}
	
		private function uuid() {
	 $uuid = array(
		'time_low'  => 0,
		'time_mid'  => 0,
		'time_hi'  => 0,
		'clock_seq_hi' => 0,
		'clock_seq_low' => 0,
		'node'   => array()
	 );

	 $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
	 $uuid['time_mid'] = mt_rand(0, 0xffff);
	 $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
	 $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
	 $uuid['clock_seq_low'] = mt_rand(0, 255);

	 for ($i = 0; $i < 6; $i++) {
		$uuid['node'][$i] = mt_rand(0, 255);
	 }

	 $uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
		$uuid['time_low'],
		$uuid['time_mid'],
		$uuid['time_hi'],
		$uuid['clock_seq_hi'],
		$uuid['clock_seq_low'],
		$uuid['node'][0],
		$uuid['node'][1],
		$uuid['node'][2],
		$uuid['node'][3],
		$uuid['node'][4],
		$uuid['node'][5]
	 );

	 return $uuid;
	}
			
}
<?php
class ControllerMarketingPromoProduct extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('marketing/promo_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/promo_product');
		
		$data['user_token'] = $this->session->data['user_token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/promo_product', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['promos'] = array();
		$results = $this->model_marketing_promo_product->getPromos();
		foreach ($results as $result) {
			$data['promos'][] = array(
				'coupon_id'  => $result['coupon_id'],
				'name'       => $result['name'],
				'code'       => $result['code'],
				'type'       => $result['type'],
				'total'			 => $result['total'],
				'discount'   => $result['discount'],
				'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
				'date_end'   => date($this->language->get('date_format_short'), strtotime($result['date_end'])),
				'status'     => $result['status']
			);
		}
		
		$data['categories'] = array();
		$filter_data = array('sort'=>'fullname','order'=>'ASC');
		$results = $this->model_marketing_promo_product->getAllCategories($filter_data);
		foreach ($results as $result) {
			$level = count(explode("&and;", $result['fullname']));
			$data['categories'][] = array(
				'category_id' 	=> $result['category_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
				'level' 		=> $level, 
				'parent_id' 	=> $result['parent_id'], 
				'product_count' => $result['product_count'], 
				'sort_order' 	=> $result['sort_order'], 
				'status' 		=> $result['status'] 
			);
		}
		
		$data['manufacturers'] = array();
		$filter_data = array('sort'=>'name','order'=>'ASC');
		$results = $this->model_marketing_promo_product->getManufacturers($filter_data);
		foreach ($results as $result) {
			$data['manufacturers'][] = array(
				'manufacturer_id' 	=> $result['manufacturer_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['pagination'] = '';
		$data['results'] = '';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/promo_product', $data));
	}

	public function selectPromo() {
		$json = array();
		
		$this->load->model('marketing/promo_product');
		
		$promo_info = $this->model_marketing_promo_product->getPromoById($this->request->post['coupon_id']);
		
		if ($promo_info) {
			
			$promo_categories = $this->model_marketing_promo_product->getPromoCategories($this->request->post['coupon_id']);
			$total_promo_categories = $this->model_marketing_promo_product->getTotalPromoCategories($this->request->post['coupon_id']);
			$total_promo_products = $this->model_marketing_promo_product->getTotalPromoProducts($this->request->post['coupon_id']);
			
			$json['success'] = 'Получены параметры промо-кода ' . $promo_info['code'] . ' ( id=' . $promo_info['coupon_id'] . ' )';
			$json['promo_info'] = $promo_info;
			$json['total_promo_categories'] = $total_promo_categories;
			$json['total_promo_products'] = $total_promo_products;
			$json['promo_categories'] = implode(',', $promo_categories);
			
		} else {
			$json['error'] = 'Не удалось получить параметры промо-кода id=' . $this->request->post['coupon_id'];
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function setCategories() {
		$json = array();
		
		$this->load->model('marketing/promo_product');
		
		$coupon_id = $this->request->post['coupon_id'];
		$coupon_code = $this->request->post['coupon_code'];
		$coupon_categories = explode(',', $this->request->post['coupon_categories']);
		
		$this->model_marketing_promo_product->setPromoCategories($coupon_id, $coupon_categories);
		
		$result = $this->model_marketing_promo_product->getPromoCategories($coupon_id);
		
		if ($result) {
			$json['promo_categories'] = implode(',', $result);
			$json['success'] = 'Установлены категории для промо-кода ' . $coupon_code . ' ( id=' . $coupon_id . ' )';
			$json['style'] = 'success';
		} else {
			$json['success'] = 'Очищен список категорий для промо-кода ' . $coupon_code . ' ( id=' . $coupon_id . ' )';
			$json['style'] = 'warning';
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function setProducts() {
		$json = array();
		
		$arr_products = array();
		
		$this->load->model('marketing/promo_product');
		
		$this->model_marketing_promo_product->setPromoProducts($this->request->post['coupon_id'], $this->request->post['coupon_products'], $this->request->post['mode']);
		
		$json['success'] = 'Обновлен список товаров для промо-кода ' . $this->request->post['coupon_code'] . ' ( id=' . $this->request->post['coupon_id'] . ' )';
		$json['style'] = 'success';
 
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getProducts() {
		$json = array();
		
		$this->load->model('marketing/promo_product');
		
		if ($this->request->post['coupon_id'] == '0') {
			$filter_coupon_id = null;	
		}else{
			$filter_coupon_id = $this->request->post['coupon_id'];	
		}
		
		if ($this->request->post['filter_name'] == '') {
			$filter_name = null;	
		}else{
			$filter_name = $this->request->post['filter_name'];	
		}
		
		if ($this->request->post['filter_category'] == '*') {
			$filter_category = null;	
		}else{
			$filter_category = $this->request->post['filter_category'];	
		}
		
		if ($this->request->post['filter_manufacturer'] == '*') {
			$filter_manufacturer = null;	
		}else{
			$filter_manufacturer = $this->request->post['filter_manufacturer'];	
		}
		
		if ($this->request->post['filter_price_from'] == '') {
			$filter_price_from = null;	
		}else{
			$filter_price_from = $this->request->post['filter_price_from'];	
		}
		
		if ($this->request->post['filter_price_to'] == '') {
			$filter_price_to = null;	
		}else{
			$filter_price_to = $this->request->post['filter_price_to'];	
		}
		
		if ($this->request->post['filter_quantity_from'] == '') {
			$filter_quantity_from = null;	
		}else{
			$filter_quantity_from = $this->request->post['filter_quantity_from'];	
		}
		
		if ($this->request->post['filter_quantity_to'] == '') {
			$filter_quantity_to = null;	
		}else{
			$filter_quantity_to = $this->request->post['filter_quantity_to'];	
		}
		
		if ($this->request->post['filter_status'] == '*') {
			$filter_status = null;	
		}else{
			$filter_status = $this->request->post['filter_status'];	
		}
		
		$page = $this->request->post['page'];
			
		$filter_data = array(
			'filter_coupon_id'			=> $filter_coupon_id,
			'filter_name'	  				=> $filter_name,
			'filter_price_from'	  	=> $filter_price_from,
			'filter_price_to'	  		=> $filter_price_to,
			'filter_quantity_from' 	=> $filter_quantity_from,
			'filter_quantity_to' 		=> $filter_quantity_to,
      'filter_category' 			=> $filter_category,
      'filter_manufacturer' 	=> $filter_manufacturer,
			'filter_status'   			=> $filter_status,
			'sort'            			=> 'pd.name',
			'order'           			=> 'ASC',
			'start'           			=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           			=> $this->config->get('config_limit_admin')
		);
		
		$product_total = $this->model_marketing_promo_product->getTotalProducts($filter_data);
		$page_products = '';	
		
		if ($product_total == 0) {
			
			$json['success']['pagination'] = '';
			$json['success']['results'] = '';
			$json['success']['text'] = 'Запрос выполнен. Пустой результат.';
			
		} else {

			$results = $this->model_marketing_promo_product->getProductArr($filter_data);
			
			foreach ($results as $result) {
	
				$special = false;
	
				$product_specials = $this->model_marketing_promo_product->getProductSpecials($result['product_id']);
				foreach ($product_specials  as $product_special) {
					if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
						$special = $this->currency->format($product_special['price'], $this->config->get('config_currency'));
						break;
					}
				}
				$product_promos = $this->model_marketing_promo_product->getProductPromos($result['product_id']);
				$promos = implode(',', $product_promos);
	
				$product_id   = $result['product_id'];
				$model       	= str_replace ('"', '&quot;', trim($result['model']));
				$name       	= str_replace ('"', '&quot;', trim($result['name']));
				$price      	= $result['price'];
				$special    	= $special;
				$quantity   	= $result['quantity'];
				$status     	= $result['status'];
				
				$page_products .= '{"product_id":"'.$product_id.'","model":"'.$model.'","name":"'.$name.'","promos":"'.$promos.'","price":"'.$price.'","special":"'.$special.'","quantity":"'.$quantity.'","status":"'.$status.'"},';
				
			}
			
			$page_products = trim($page_products, ",");
			
			$paginator = $this->getPaginator($product_total, $page);
			$json['success']['pagination'] = $paginator['paginator'];
			$json['success']['results'] = $paginator['results'];
		
			$json['success']['text'] = 'Запрос выполнен. Загружена страница ' . $page . '.';
		}
		
		$json['success']['page_products'] = '['.$page_products.']';
		$json['success']['count'] = $product_total;
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getPaginator($total, $page) {
		
		$limit = $this->config->get('config_limit_admin');
		$num_links = 8;
		$text_first = '|&lt;';
		$text_last = '&gt;|';
		$text_next = '&gt;';
		$text_prev = '&lt;';
		
		$paginator = array();
		
		if ($page < 1) {
			$page = 1;
		}
		if (!(int)$limit) {
			$limit = 20;
		}
		$num_pages = ceil($total / $limit);
		$output = '<ul class="paginator">';
		if ($page > 1) {
			$output .= '<li id="page-' . '1'. '" class="page">' . $text_first . '</li>';
			if ($page - 1 === 1) {
				$output .= '<li id="page-' . $page . '" class="page">' . $text_prev . '</li>';
			} else {
				$output .= '<li id="page-' . ($page - 1) . '" class="page">' . $text_prev . '</li>';
			}
		}
		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);
				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}
			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<li id="page-' . $i . '" class="active">' . $i . '</li>';
				} else {
					$output .= '<li id="page-' . $i . '" class="page">' . $i . '</li>';
				}
			}
		}
		if ($page < $num_pages) {
			$output .= '<li id="page-' . ($page + 1) . '" class="page">' . $text_next . '</li>';
			$output .= '<li id="page-' . $num_pages . '" class="page">' . $text_last . '</li>';
		}
		$output .= '</ul>';
		
		if ($num_pages > 1) {
			$paginator['paginator'] = $output;
		} else {
			$paginator['paginator'] = '';
		}
		
		$paginator['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit), $total, ceil($total / $limit));
		
		return $paginator;
	}
	
	
//	public function add() {
//		$this->load->language('marketing/promo_product');
//
//		$this->document->setTitle($this->language->get('heading_title'));
//
//		$this->load->model('marketing/promo_product');
//
//		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
//			$this->model_marketing_promo_product->addpromo_product($this->request->post);
//
//			$this->session->data['success'] = $this->language->get('text_success');
//
//			$url = '';
//
//			if (isset($this->request->get['sort'])) {
//				$url .= '&sort=' . $this->request->get['sort'];
//			}
//
//			if (isset($this->request->get['order'])) {
//				$url .= '&order=' . $this->request->get['order'];
//			}
//
//			if (isset($this->request->get['page'])) {
//				$url .= '&page=' . $this->request->get['page'];
//			}
//
//			$this->response->redirect($this->url->link('marketing/promo_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
//		}
//
//		$this->getForm();
//	}
//
//	public function edit() {
//		$this->load->language('marketing/promo_product');
//
//		$this->document->setTitle($this->language->get('heading_title'));
//
//		$this->load->model('marketing/promo_product');
//
//		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
//			$this->model_marketing_promo_product->editpromo_product($this->request->get['promo_product_id'], $this->request->post);
//
//			$this->session->data['success'] = $this->language->get('text_success');
//
//			$url = '';
//
//			if (isset($this->request->get['sort'])) {
//				$url .= '&sort=' . $this->request->get['sort'];
//			}
//
//			if (isset($this->request->get['order'])) {
//				$url .= '&order=' . $this->request->get['order'];
//			}
//
//			if (isset($this->request->get['page'])) {
//				$url .= '&page=' . $this->request->get['page'];
//			}
//
//			$this->response->redirect($this->url->link('marketing/promo_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
//		}
//
//		$this->getForm();
//	}
//
//	public function delete() {
//		$this->load->language('marketing/promo_product');
//
//		$this->document->setTitle($this->language->get('heading_title'));
//
//		$this->load->model('marketing/promo_product');
//
//		if (isset($this->request->post['selected']) && $this->validateDelete()) {
//			foreach ($this->request->post['selected'] as $promo_product_id) {
//				$this->model_marketing_promo_product->deletepromo_product($promo_product_id);
//			}
//
//			$this->session->data['success'] = $this->language->get('text_success');
//
//			$url = '';
//
//			if (isset($this->request->get['sort'])) {
//				$url .= '&sort=' . $this->request->get['sort'];
//			}
//
//			if (isset($this->request->get['order'])) {
//				$url .= '&order=' . $this->request->get['order'];
//			}
//
//			if (isset($this->request->get['page'])) {
//				$url .= '&page=' . $this->request->get['page'];
//			}
//
//			$this->response->redirect($this->url->link('marketing/promo_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
//		}
//
//		$this->getList();
//	}
//
//	protected function getForm() {
//		$data['text_form'] = !isset($this->request->get['promo_product_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
//
//		$data['user_token'] = $this->session->data['user_token'];
//
//		if (isset($this->request->get['promo_product_id'])) {
//			$data['promo_product_id'] = $this->request->get['promo_product_id'];
//		} else {
//			$data['promo_product_id'] = 0;
//		}
//
//		if (isset($this->error['warning'])) {
//			$data['error_warning'] = $this->error['warning'];
//		} else {
//			$data['error_warning'] = '';
//		}
//
//		if (isset($this->error['name'])) {
//			$data['error_name'] = $this->error['name'];
//		} else {
//			$data['error_name'] = '';
//		}
//
//		if (isset($this->error['code'])) {
//			$data['error_code'] = $this->error['code'];
//		} else {
//			$data['error_code'] = '';
//		}
//
//		if (isset($this->error['date_start'])) {
//			$data['error_date_start'] = $this->error['date_start'];
//		} else {
//			$data['error_date_start'] = '';
//		}
//
//		if (isset($this->error['date_end'])) {
//			$data['error_date_end'] = $this->error['date_end'];
//		} else {
//			$data['error_date_end'] = '';
//		}
//
//		$url = '';
//
//		if (isset($this->request->get['page'])) {
//			$url .= '&page=' . $this->request->get['page'];
//		}
//
//		if (isset($this->request->get['sort'])) {
//			$url .= '&sort=' . $this->request->get['sort'];
//		}
//
//		if (isset($this->request->get['order'])) {
//			$url .= '&order=' . $this->request->get['order'];
//		}
//
//		$data['breadcrumbs'] = array();
//
//		$data['breadcrumbs'][] = array(
//			'text' => $this->language->get('text_home'),
//			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
//		);
//
//		$data['breadcrumbs'][] = array(
//			'text' => $this->language->get('heading_title'),
//			'href' => $this->url->link('marketing/promo_product', 'user_token=' . $this->session->data['user_token'] . $url, true)
//		);
//
//		if (!isset($this->request->get['promo_product_id'])) {
//			$data['action'] = $this->url->link('marketing/promo_product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
//		} else {
//			$data['action'] = $this->url->link('marketing/promo_product/edit', 'user_token=' . $this->session->data['user_token'] . '&promo_product_id=' . $this->request->get['promo_product_id'] . $url, true);
//		}
//
//		$data['cancel'] = $this->url->link('marketing/promo_product', 'user_token=' . $this->session->data['user_token'] . $url, true);
//
//		if (isset($this->request->get['promo_product_id']) && (!$this->request->server['REQUEST_METHOD'] != 'POST')) {
//			$promo_product_info = $this->model_marketing_promo_product->getpromo_product($this->request->get['promo_product_id']);
//		}
//
//		if (isset($this->request->post['name'])) {
//			$data['name'] = $this->request->post['name'];
//		} elseif (!empty($promo_product_info)) {
//			$data['name'] = $promo_product_info['name'];
//		} else {
//			$data['name'] = '';
//		}
//
//		if (isset($this->request->post['code'])) {
//			$data['code'] = $this->request->post['code'];
//		} elseif (!empty($promo_product_info)) {
//			$data['code'] = $promo_product_info['code'];
//		} else {
//			$data['code'] = '';
//		}
//
//		if (isset($this->request->post['type'])) {
//			$data['type'] = $this->request->post['type'];
//		} elseif (!empty($promo_product_info)) {
//			$data['type'] = $promo_product_info['type'];
//		} else {
//			$data['type'] = '';
//		}
//
//		if (isset($this->request->post['discount'])) {
//			$data['discount'] = $this->request->post['discount'];
//		} elseif (!empty($promo_product_info)) {
//			$data['discount'] = $promo_product_info['discount'];
//		} else {
//			$data['discount'] = '';
//		}
//
//		if (isset($this->request->post['logged'])) {
//			$data['logged'] = $this->request->post['logged'];
//		} elseif (!empty($promo_product_info)) {
//			$data['logged'] = $promo_product_info['logged'];
//		} else {
//			$data['logged'] = '';
//		}
//
//		if (isset($this->request->post['shipping'])) {
//			$data['shipping'] = $this->request->post['shipping'];
//		} elseif (!empty($promo_product_info)) {
//			$data['shipping'] = $promo_product_info['shipping'];
//		} else {
//			$data['shipping'] = '';
//		}
//
//		if (isset($this->request->post['total'])) {
//			$data['total'] = $this->request->post['total'];
//		} elseif (!empty($promo_product_info)) {
//			$data['total'] = $promo_product_info['total'];
//		} else {
//			$data['total'] = '';
//		}
//
//		if (isset($this->request->post['promo_product_product'])) {
//			$products = $this->request->post['promo_product_product'];
//		} elseif (isset($this->request->get['promo_product_id'])) {
//			$products = $this->model_marketing_promo_product->getpromo_productProducts($this->request->get['promo_product_id']);
//		} else {
//			$products = array();
//		}
//
//		$this->load->model('catalog/product');
//
//		$data['promo_product_product'] = array();
//
//		foreach ($products as $product_id) {
//			$product_info = $this->model_catalog_product->getProduct($product_id);
//
//			if ($product_info) {
//				$data['promo_product_product'][] = array(
//					'product_id' => $product_info['product_id'],
//					'name'       => $product_info['name']
//				);
//			}
//		}
//
//		if (isset($this->request->post['promo_product_category'])) {
//			$categories = $this->request->post['promo_product_category'];
//		} elseif (isset($this->request->get['promo_product_id'])) {
//			$categories = $this->model_marketing_promo_product->getpromo_productCategories($this->request->get['promo_product_id']);
//		} else {
//			$categories = array();
//		}
//
//		$this->load->model('catalog/category');
//
//		$data['promo_product_category'] = array();
//
//		foreach ($categories as $category_id) {
//			$category_info = $this->model_catalog_category->getCategory($category_id);
//
//			if ($category_info) {
//				$data['promo_product_category'][] = array(
//					'category_id' => $category_info['category_id'],
//					'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
//				);
//			}
//		}
//
//		if (isset($this->request->post['date_start'])) {
//			$data['date_start'] = $this->request->post['date_start'];
//		} elseif (!empty($promo_product_info)) {
//			$data['date_start'] = ($promo_product_info['date_start'] != '0000-00-00' ? $promo_product_info['date_start'] : '');
//		} else {
//			$data['date_start'] = date('Y-m-d', time());
//		}
//
//		if (isset($this->request->post['date_end'])) {
//			$data['date_end'] = $this->request->post['date_end'];
//		} elseif (!empty($promo_product_info)) {
//			$data['date_end'] = ($promo_product_info['date_end'] != '0000-00-00' ? $promo_product_info['date_end'] : '');
//		} else {
//			$data['date_end'] = date('Y-m-d', strtotime('+1 month'));
//		}
//
//		if (isset($this->request->post['uses_total'])) {
//			$data['uses_total'] = $this->request->post['uses_total'];
//		} elseif (!empty($promo_product_info)) {
//			$data['uses_total'] = $promo_product_info['uses_total'];
//		} else {
//			$data['uses_total'] = 1;
//		}
//
//		if (isset($this->request->post['uses_customer'])) {
//			$data['uses_customer'] = $this->request->post['uses_customer'];
//		} elseif (!empty($promo_product_info)) {
//			$data['uses_customer'] = $promo_product_info['uses_customer'];
//		} else {
//			$data['uses_customer'] = 1;
//		}
//
//		if (isset($this->request->post['status'])) {
//			$data['status'] = $this->request->post['status'];
//		} elseif (!empty($promo_product_info)) {
//			$data['status'] = $promo_product_info['status'];
//		} else {
//			$data['status'] = true;
//		}
//
//		$data['header'] = $this->load->controller('common/header');
//		$data['column_left'] = $this->load->controller('common/column_left');
//		$data['footer'] = $this->load->controller('common/footer');
//
//		$this->response->setOutput($this->load->view('marketing/promo_product_form', $data));
//	}
//
//	protected function validateForm() {
//		if (!$this->user->hasPermission('modify', 'marketing/promo_product')) {
//			$this->error['warning'] = $this->language->get('error_permission');
//		}
//
//		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 128)) {
//			$this->error['name'] = $this->language->get('error_name');
//		}
//
//		if ((utf8_strlen($this->request->post['code']) < 3) || (utf8_strlen($this->request->post['code']) > 10)) {
//			$this->error['code'] = $this->language->get('error_code');
//		}
//
//		$promo_product_info = $this->model_marketing_promo_product->getpromo_productByCode($this->request->post['code']);
//
//		if ($promo_product_info) {
//			if (!isset($this->request->get['promo_product_id'])) {
//				$this->error['warning'] = $this->language->get('error_exists');
//			} elseif ($promo_product_info['promo_product_id'] != $this->request->get['promo_product_id']) {
//				$this->error['warning'] = $this->language->get('error_exists');
//			}
//		}
//
//		return !$this->error;
//	}
//
//	protected function validateDelete() {
//		if (!$this->user->hasPermission('modify', 'marketing/promo_product')) {
//			$this->error['warning'] = $this->language->get('error_permission');
//		}
//
//		return !$this->error;
//	}

}
<?php
class ControllerMarketingGiftProduct extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('marketing/gift_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/gift_product');

		$this->getList();
	}

	public function add() {
		$this->load->language('marketing/gift_product');

		$this->document->setTitle('Новая настройка');

		$this->load->model('marketing/gift_product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_marketing_gift_product->addSetting($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('marketing/gift_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/gift_product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_marketing_gift_product->editSetting($this->request->get['setting_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('marketing/gift_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/gift_product');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $setting_id) {
				$this->model_marketing_gift_product->deleteSetting($setting_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	public function refresh() {
		
		$this->load->model('marketing/gift_product');
		
		$this->model_marketing_gift_product->refreshOptions();
		
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'setting_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('marketing/gift_product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('marketing/gift_product/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['refresh'] = $this->url->link('marketing/gift_product/refresh', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['settings'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$setting_total = $this->model_marketing_gift_product->getTotalSettings();

		$results = $this->model_marketing_gift_product->getSettings($filter_data);

		foreach ($results as $result) {
			$data['settings'][] = array(
				'setting_id'  				=> $result['setting_id'],
				'setting_name'				=> $result['setting_name'],
				'option_name'       	=> $this->model_marketing_gift_product->getOptionName($result['option_id']),
				'option_value_name'   => $this->model_marketing_gift_product->getOptionValueName($result['option_value_id']),
				'gift_quantity' 			=> $result['gift_quantity'],
				'product_quantity' 		=> $result['product_quantity'],
				'status'     					=> ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'edit'       					=> $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $result['setting_id'] . $url, true)
			);
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_setting_name'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . '&sort=setting_name' . $url, true);
		$data['sort_option_name'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . '&sort=option_name' . $url, true);
		$data['sort_option_value_name'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . '&sort=option_value_name' . $url, true);
		$data['sort_gift_quantity'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . '&sort=gift_quantity' . $url, true);
		$data['sort_product_quantity'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . '&sort=product_quantity' . $url, true);
		$data['sort_status'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $setting_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($setting_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($setting_total - $this->config->get('config_limit_admin'))) ? $setting_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $setting_total, ceil($setting_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/gift_product_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['setting_id']) ? 'Новая настройка' : 'Изменение настройки';

		$data['user_token'] = $this->session->data['user_token'];
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $sort;
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['options'] = array();
		$filter_data = array('sort'=>'name','order'=>'ASC');
		$results = $this->model_marketing_gift_product->getOptions($filter_data);
		foreach ($results as $result) {
			$data['options'][] = array(
				'option_id' 	=> $result['option_id'], 
				'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
		
		$data['optionvalues'] = array();
		$results = $this->model_marketing_gift_product->getOptionValues();
		foreach ($results as $result) {
			$data['optionvalues'][] = array(
				'option_id' 			=> $result['option_id'], 
				'option_value_id'	=> $result['option_value_id'], 
				'name'        		=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
		
		$data['categories'] = array();
		$filter_data = array('sort'=>'fullname','order'=>'ASC');
		$results = $this->model_marketing_gift_product->getAllCategories($filter_data);
		foreach ($results as $result) {
			$level = count(explode("&and;", $result['fullname']));
			$data['categories'][] = array(
				'category_id' 	=> $result['category_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'fullname'      => strip_tags(html_entity_decode($result['fullname'], ENT_QUOTES, 'UTF-8')),
				'level' 		=> $level, 
				'parent_id' 	=> $result['parent_id'], 
				'product_count' => $result['product_count'], 
				'sort_order' 	=> $result['sort_order'], 
				'status' 		=> $result['status'] 
			);
		}
		
		$data['manufacturers'] = array();
		$filter_data = array('sort'=>'name','order'=>'ASC');
		$results = $this->model_marketing_gift_product->getAllManufacturers($filter_data);
		foreach ($results as $result) {
			$data['manufacturers'][] = array(
				'manufacturer_id' 	=> $result['manufacturer_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
		
		$data['statuses'] = array();
		$results = $this->model_marketing_gift_product->getStockStatuses();
		foreach ($results as $result) {
			$data['statuses'][] = array(
				'stock_status_id' 	=> $result['stock_status_id'], 
				'name'        	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
		
		$data['products'] = array();

		if (isset($this->request->get['setting_id'])) {
			$data['setting_id'] = $this->request->get['setting_id'];
		} else {
			$data['setting_id'] = 0;
		}
		
		if ($data['setting_id'] != 0) {
			
			$filter_data = array(
				'setting_id'            => $data['setting_id'],
				'sort'            			=> $sort,
				'order'           			=> $order,
				'start'           			=> ($page - 1) * $this->config->get('config_limit_admin'),
				'limit'           			=> $this->config->get('config_limit_admin')
			);

			$product_total = $this->model_marketing_gift_product->getTotalProducts($filter_data);
			
			$results = $this->model_marketing_gift_product->getProducts($filter_data);
			
			foreach ($results as $result) {
				
				$special = false;
				$product_specials = $this->model_marketing_gift_product->getProductSpecials($result['product_id']);
				foreach ($product_specials  as $product_special) {
					if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
						$special = $this->currency->format($product_special['price'], $this->config->get('config_currency'));
						break;
					}
				}
				
				$status_info = $this->model_marketing_gift_product->getStockStatus($result['stock_status_id']);
				
				$data['products'][] = array(
					'product_id' 			=> $result['product_id'],
					'model'      			=> $result['model'],
					'name'       			=> $result['name'],
					'price'      			=> $this->currency->format($result['price'], $this->config->get('config_currency')),
					'special'    			=> $special,
					'quantity'   			=> $result['quantity'],
					'stock_status_id'	=> $result['stock_status_id'],
					'status'     			=> $status_info['name']
				);
			
			}
			
			$url = '';

			if ($order == 'ASC') {
				$url .= '&order=DESC';
			} else {
				$url .= '&order=ASC';
			}

//			if (isset($this->request->get['page'])) {
//				$url .= '&page=' . $this->request->get['page'];
//			}
			// сброс на первую страницу при смене сортировки
			$url .= '&page=1';

			$data['sort_name'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $data['setting_id'] . '&sort=pd.name' . $url . '#tab-products', true);
			$data['sort_model'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $data['setting_id'] . '&sort=p.model' . $url . '#tab-products', true);
			$data['sort_price'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $data['setting_id'] . '&sort=p.price' . $url . '#tab-products', true);
			$data['sort_quantity'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $data['setting_id'] . '&sort=p.quantity' . $url . '#tab-products', true);
			$data['sort_stock_status_id'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $data['setting_id'] . '&sort=p.stock_status_id' . $url . '#tab-products', true);
			
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
		
			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_limit_admin');
			$pagination->url = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $data['setting_id'] . $url . '&page={page}#tab-products', true);

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

			$data['sort'] = $sort;
			$data['order'] = $order;
		
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['setting_name'])) {
			$data['error_setting_name'] = $this->error['setting_name'];
		} else {
			$data['error_setting_name'] = '';
		}

		if (isset($this->error['option_id'])) {
			$data['error_option_id'] = $this->error['option_id'];
		} else {
			$data['error_option_id'] = '';
		}

		if (isset($this->error['option_value_id'])) {
			$data['error_option_value_id'] = $this->error['option_value_id'];
		} else {
			$data['error_option_value_id'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

//		if (!isset($this->request->get['setting_id'])) {
//			$data['action'] = $this->url->link('marketing/gift_product/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
//		} else {
//			$data['action'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $this->request->get['setting_id'] . $url, true);
//		}

		if (!isset($this->request->get['setting_id'])) {
			$data['action'] = $this->url->link('marketing/gift_product/add', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('marketing/gift_product/edit', 'user_token=' . $this->session->data['user_token'] . '&setting_id=' . $this->request->get['setting_id'], true);
		}

		//$data['cancel'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['cancel'] = $this->url->link('marketing/gift_product', 'user_token=' . $this->session->data['user_token'], true);

		if (isset($this->request->get['setting_id']) && (!$this->request->server['REQUEST_METHOD'] != 'POST')) {
			$setting_info = $this->model_marketing_gift_product->getSetting($this->request->get['setting_id']);
		}

		if (isset($this->request->post['setting_name'])) {
			$data['setting_name'] = $this->request->post['setting_name'];
		} elseif (!empty($setting_info)) {
			$data['setting_name'] = $setting_info['setting_name'];
		} else {
			$data['setting_name'] = '';
		}

		if (isset($this->request->post['option_id'])) {
			$data['option_id'] = $this->request->post['option_id'];
		} elseif (!empty($setting_info)) {
			$data['option_id'] = $setting_info['option_id'];
		} else {
			$data['option_id'] = '';
		}

		if (isset($this->request->post['option_value_id'])) {
			$data['option_value_id'] = $this->request->post['option_value_id'];
		} elseif (!empty($setting_info)) {
			$data['option_value_id'] = $setting_info['option_value_id'];
		} else {
			$data['option_value_id'] = '';
		}

		if (isset($this->request->post['stock_from'])) {
			$data['stock_from'] = $this->request->post['stock_from'];
		} elseif (!empty($setting_info)) {
			$data['stock_from'] = $setting_info['stock_from'];
		} else {
			$data['stock_from'] = '';
		}

		if (isset($this->request->post['stock_to'])) {
			$data['stock_to'] = $this->request->post['stock_to'];
		} elseif (!empty($setting_info)) {
			$data['stock_to'] = $setting_info['stock_to'];
		} else {
			$data['stock_to'] = '';
		}

		if (isset($this->request->post['price_from'])) {
			$data['price_from'] = $this->request->post['price_from'];
		} elseif (!empty($setting_info)) {
			$data['price_from'] = $setting_info['price_from'];
		} else {
			$data['price_from'] = '';
		}

		if (isset($this->request->post['price_to'])) {
			$data['price_to'] = $this->request->post['price_to'];
		} elseif (!empty($setting_info)) {
			$data['price_to'] = $setting_info['price_to'];
		} else {
			$data['price_to'] = '';
		}

		if (isset($this->request->post['gift_quantity'])) {
			$data['gift_quantity'] = $this->request->post['gift_quantity'];
		} elseif (!empty($setting_info)) {
			$data['gift_quantity'] = $setting_info['gift_quantity'];
		} else {
			$data['gift_quantity'] = '';
		}

		if (isset($this->request->post['product_quantity'])) {
			$data['product_quantity'] = $this->request->post['product_quantity'];
		} elseif (!empty($setting_info)) {
			$data['product_quantity'] = $setting_info['product_quantity'];
		} else {
			$data['product_quantity'] = '';
		}

		if (isset($this->request->post['setting_categories'])) {
			$data['setting_categories'] = $this->request->post['setting_categories'];
		} elseif (!empty($setting_info)) {
			$data['setting_categories'] = $setting_info['setting_categories'];
		} else {
			$data['setting_categories'] = '';
		}

		if (isset($this->request->post['setting_manufacturers'])) {
			$data['setting_manufacturers'] = $this->request->post['setting_manufacturers'];
		} elseif (!empty($setting_info)) {
			$data['setting_manufacturers'] = $setting_info['setting_manufacturers'];
		} else {
			$data['setting_manufacturers'] = '';
		}

		if (isset($this->request->post['setting_statuses'])) {
			$data['setting_statuses'] = $this->request->post['setting_statuses'];
		} elseif (!empty($setting_info)) {
			$data['setting_statuses'] = $setting_info['setting_statuses'];
		} else {
			$data['setting_statuses'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($setting_info)) {
			$data['status'] = $setting_info['status'];
		} else {
			$data['status'] = true;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/gift_product_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'marketing/gift_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['setting_name']) < 3) || (utf8_strlen($this->request->post['setting_name']) > 128)) {
			$this->error['setting_name'] = 'Название настройки должно быть от 3 до 128 символов!';
		}

		if ($this->request->post['option_id'] == 0) {
			$this->error['option_id'] = 'Выберите опцию (группу подарков)';
		}

		if ($this->request->post['option_value_id'] == 0) {
			$this->error['option_value_id'] = 'Выберите подарок';
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'marketing/gift_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

}
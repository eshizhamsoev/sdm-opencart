<?php

$envPath = __DIR__ . '/env.php';

if(is_file($envPath)){
    require $envPath;
}

$protocol = getenv('ENV_HTTP') ?: 'https';
$hostname = getenv('ENV_HOSTNAME') ?: $_SERVER['HTTP_HOST'];

$url = $protocol . '://'. $hostname . '/';

// HTTP
define('HTTP_SERVER', $url);

// HTTPS
define('HTTPS_SERVER', $url);

// DIR
define('DIR_PROJECT_ROOT', getenv('ENV_ROOTPATH'));
define('DIR_APPLICATION', getenv('ENV_ROOTPATH') . 'catalog/');
define('DIR_SYSTEM', getenv('ENV_ROOTPATH') . 'system/');
define('DIR_IMAGE', getenv('ENV_IMAGEPATH'));
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', getenv('ENV_LOGSPATH'));
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', getenv('DB_HOST'));
define('DB_USERNAME', getenv('DB_USERNAME'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_DATABASE', getenv('DB_DATABASE'));
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// Redis
//define('CACHE_HOSTNAME', 'redis');
define('CACHE_PORT', '6379');
define('CACHE_PREFIX', 'redis_');

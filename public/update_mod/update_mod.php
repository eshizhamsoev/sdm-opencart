<?php
$is_web = http_response_code() !== false;
if ($is_web) {
    echo 'only cli call supported!';
    exit;
}
$username = isset($argv[1]) ? $argv[1] : 'admin';
$password = isset($argv[1]) ? $argv[2] : 'admin';
$url = isset($argv[1]) ? $argv[3] : 'http://127.0.0.1';
$rootpath = isset($argv[4]) ? $argv[4] : '/home/webdevuser/www/public/';
$cookie = $rootpath . "system/storage/cache/cookie.txt";
$useragent = isset($_SERVER['HTTP_USER_AGENT']) ? strtolower($_SERVER['HTTP_USER_AGENT']) : '';

$array = [
    'username' => $username,
    'password' => $password
];
$_h = curl_init();
//$_cache = curl_init();

echo 'update started ';
curl_setopt($_h, CURLOPT_HEADER, 1);
curl_setopt($_h, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($_h, CURLOPT_HTTPGET, 1);
curl_setopt($_h, CURLOPT_COOKIESESSION, true);
curl_setopt($_h, CURLOPT_POST, true);
curl_setopt($_h, CURLOPT_USERAGENT, $useragent);
curl_setopt($_h, CURLOPT_URL, $url . '/admin/index.php?route=common/login');
//curl_setopt($_h, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
curl_setopt($_h, CURLOPT_POSTFIELDS, $array);
curl_setopt($_h, CURLOPT_DNS_CACHE_TIMEOUT, 2);
curl_setopt($_h, CURLOPT_COOKIEJAR, $cookie);
$html = curl_exec($_h);

$loginResponse = curl_getinfo($_h);
$user_token = strstr($loginResponse['redirect_url'], 'user_token=');
if (!$user_token) {
//    throw new Exception($html);
    echo 'not autorized. login or pass fail ';
} else {
    echo 'aurorized, token = ' . $user_token . ' ';
    curl_setopt($_h, CURLOPT_URL,
        $url . '/admin/index.php?route=extension/theme/oct_ultrastore/cacheDelete&' . $user_token . '&store_id=0');
    curl_setopt($_h, CURLOPT_POSTFIELDS, $array);
    curl_exec($_h);
    echo 'cache deleted ';
    curl_setopt($_h, CURLOPT_URL,
        $url . '/admin/index.php?route=extension/theme/oct_ultrastore/refresh&' . $user_token . '&store_id=0');
    curl_setopt($_h, CURLOPT_POSTFIELDS, $array);
    curl_exec($_h);
    echo 'update modifications ';
    curl_setopt($_h, CURLOPT_URL,
        $url . '/admin/index.php?route=extension/theme/oct_ultrastore&' . $user_token . '&store_id=0');
    curl_setopt($_h, CURLOPT_POSTFIELDS, $array);
    curl_exec($_h);
    echo 'cache deleted ';
    echo 'all updated ';
}

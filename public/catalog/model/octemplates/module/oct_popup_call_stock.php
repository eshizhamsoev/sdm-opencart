<?php
/**************************************************************/
/*	@support	https://wisesol.ru/					  */
/**************************************************************/

class ModelOCTemplatesModuleOctPopupCallStock extends Model {
	public function addRequest($data, $linkgood) {
		$this->db->query("
			INSERT INTO `". DB_PREFIX ."oct_popup_call_stock` 
			SET 
				info = '". $this->db->escape($data['info']) ."', 
				note = '". $this->db->escape($linkgood) ."', 
				date_added = NOW()
		");
	}
}
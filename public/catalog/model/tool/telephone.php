<?php
class ModelToolTelephone extends Model {
    public function parse($telephone) {
        $result = preg_replace('~\D+~', '', $telephone);
        if (strlen($result) === 10) {
            return '+7' . $result;
        } else if (strlen($result) === 11) {
            $result = substr_replace($result, '+7', 0, 1);
        }
        return $result;
    }
}

<?php

class ModelModuleB24Order extends Model
{
    const TABLE_NAME = 'b24_order';
    const MANAGER = 10;
    const CONFIG_CUSTOMER_GROUP_COEF = 'customer_group_coef';
    const RETAIL_CUSTOMER_GROUP = 1;
    const MEASURE_PIECE = 796;
    const WITHOUT_SIZE = 0;

    const FIELD_FULL_ADDRESS = 'UF_CRM_1574612721';
    const FIELD_SHIPPING_METHOD = 'UF_CRM_1624614837146';
    const FIELD_PAYMENT_STATUS = 'UF_CRM_1624616584669';

    public $UF_DEAL_PAYMENT_METHOD = 'UF_CRM_1527844108';
    public $UF_LEAD_PAYMENT_METHOD = 'UF_CRM_1527844072';

    public $UF_DEAL_DISCOUNT_TYPE = 'UF_CRM_1527844360';
    public $UF_LEAD_DISCOUNT_TYPE = 'UF_CRM_1524036962';


    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('setting/setting');

        $b24_setting = $this->model_setting_setting->getSetting('b24_hook_key');
        $this->b24->setFields($b24_setting);
    }

    protected function initEditOrder($order_id)
    {
        $this->load->model('module/b24_customer');
        $this->load->model('checkout/order');

        $order = $this->model_checkout_order->getOrder($order_id);
        $customerName = $order['firstname'];

        $b24Contact = $this->getContactFromDB($order['customer_id']);

        if (empty($b24Contact)) {
            $b24Contact = $this->getContactFromB24($order['email']);
        }

        $managerId = isset($b24Contact['ASSIGNED_BY_ID']) ? $b24Contact['ASSIGNED_BY_ID'] : $this->model_module_b24_customer->getCurrentManagerId();
        $b24ContactId = !empty($b24Contact['ID']) ? $b24Contact['ID'] : 0;

        return [
            'fields' => [
                'CONTACT_ID' => $b24ContactId,
                'ASSIGNED_BY_ID' => $managerId,
                'NAME' => $customerName
            ]
        ];
    }

    public function addToDB($order_id, $b24Id, array $fields = [])
    {
        if (empty($b24Id) || (int) $order_id <= 0) {
            trigger_error('Empty $b24Id or $order_id '
                . ". Order ID : " . print_r($order_id, 1) . ". ID B24: " . print_r($b24Id, 1),
                E_USER_WARNING);
        }

        //$fields = json_encode($fields);
        $fieldsToAdd = ['oc_order_id' => $order_id, 'b24_order_id' => $b24Id];
        $this->insertToDB($fieldsToAdd);
    }


    // Отправка запроса в лиды битрикс
    public function addRequest($data, $title)
    {
        $this->load->model('module/b24_customer');

        /* name, telephone, comment, pid, link, email, enquiry, url_page, time */
        $roistat = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : '';
        $RequestName = isset($data['name']) ? $data['name'] : '';
        $RequestPhone = isset($data['telephone']) ? $data['telephone'] : '';
        $RequestEmail = isset($data['email']) ? $data['email'] : '';
        $RequestUrlProductShop = isset($data['pid']) ? '<b>Ссылка на товар:</b> <a href="' . $data['pid'] . '">Посмотреть на сайте</a><br>' : '';
        $RequestUrlProduct = isset($data['link']) ? '<b>Ссылка на товар:</b> <a href="' . $data['link'] . '">Перейти</a><br>' : '';
        $RequestURL = isset($data['url_page']) ? '<b>C какой страницы отправлен запрос:</b> <a href="' . $data['url_page'] . '">Посмотреть</a><br>' : '';
        $RequestKogda = isset($data['time']) ? '<b>Когда перезвонить:</b> ' . $data['time'] . '<br>' : '';
        $RequestComment = isset($data['comment']) ? '<b>Комментарий:</b> ' . $data['comment'] . '<br>' : '';
        $RequestEnquiry = isset($data['enquiry']) ? '<b>Запрос:</b> ' . $data['enquiry'] . '<br>' : '';
        $RequestCity = isset($data['city']) ? '<b>Город:</b> ' . $data['city'] . '<br>' : '';
        $RequestActivity = isset($data['activity']) ? '<b>Направление деятельности:</b> ' . $data['activity'] . '<br>' : '';
        $RequestSite = isset($data['site']) ? '<b>Сайт:</b> ' . $data['site'] . '<br>' : '';

        $lead_data = [
            'fields' => [
                'TITLE' => $title,
                'NAME' => $RequestName,
                'STATUS_ID' => 'NEW',
                'SOURCE_ID' => 'WEB',
                'OPENED' => 'Y',
                'ASSIGNED_BY_ID' => $this->model_module_b24_customer->getCurrentManagerId(),
                'COMMENTS' => $title . '</br>' . $RequestKogda . '' . $RequestUrlProductShop . '' . $RequestUrlProduct . '' . $RequestURL . '' . $RequestComment . '' . $RequestEnquiry
                    . '' . $RequestCity . '' . $RequestActivity . '' . $RequestSite,
                'PHONE' => [['VALUE' => $RequestPhone, "VALUE_TYPE" => "WORK"]],
                'EMAIL' => [['VALUE' => $RequestEmail, "VALUE_TYPE" => "WORK"]],
                'UTM_SOURCE' => isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : '',
                'UTM_MEDIUM' => isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : '',
                'UTM_CAMPAIGN' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                'UTM_CONTENT' => isset($_COOKIE['utm_content']) ? $_COOKIE['utm_content'] : '',
                'UTM_TERM' => isset($_COOKIE['utm_term']) ? $_COOKIE['utm_term'] : '',
                'UF_CRM_1576410299' => $roistat, // Ройстат лида
                'UF_CRM_AMO_485281' => $roistat, // Ройстат сделки
            ],
        ];

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_add' => 'crm.lead.add?' . http_build_query($lead_data),
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
    }

    public function addOrder($order_id)
    {
        $siteName = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

        $this->load->model('checkout/order');
        $this->load->model('module/b24_customer');

        $order = $this->model_checkout_order->getOrder($order_id);

        if ((int) $order['order_status_id'] <= 0) {
            return;
        }

        $dataToAdd = $this->prepareDataToB24($order_id);

        // Оповещение менеджера о новом клиенте
        $dataToAdd = array_merge($dataToAdd, ['params' => ['REGISTER_SONET_EVENT' => 'Y']]);

        //Product
        $productToAdd = $this->prepareProductToB24($order_id);
        // Product

        // Not Registered user b24ContactId. LEAD
        $extraField = [];
        if (empty($dataToAdd['fields']['CONTACT_ID'])) {
            $typeApi = 'lead';
            $typeApiRu = 'лид';
            $typeApiRu2 = 'лида';
            $typeApiUrl = '/crm/lead/details/';
            $managerId = $this->model_module_b24_customer->getCurrentManagerId();

            $text = 'На сайте ' . $siteName . ' получен новый <a href="{typeApiUrl}{b24Id}/">{typeApiRu}</a> от {dataToAdd[fields][NAME]}'
                . '. Перейдите к просмотру нового {typeApiRu2} <a href="{typeApiUrl}{b24Id}/">{b24Id}</a>';
            //$customerGroupId = $this->config->get('config_customer_group_id');
            //customerGroupId = $this->model_module_b24_customer->getGroupId();
            //$groupName = $this->getCustomerGroupName($customerGroupId);
            //$discountType = $groupName . ' ' . $this->getDiscountCoef($customerGroupId) . '%';
            //$extraField = [
            //$this->UF_LEAD_PAYMENT_METHOD => $order['payment_method'],
            //$this->UF_LEAD_DISCOUNT_TYPE => $discountType,
            //];
        } else {
            $typeApi = 'deal';
            $typeApiRu = 'сделка';
            $typeApiRu2 = 'сделки';
            $typeApiUrl = '/crm/deal/details/';
            $managerId = $dataToAdd['fields']['ASSIGNED_BY_ID'];

            $text = 'На сайте ' . $siteName . ' полученa новая <a href="{typeApiUrl}{b24Id}/">{typeApiRu}</a> от {dataToAdd[fields][NAME]}'
                . '. Перейдите к просмотру новой {typeApiRu2} <a href="{typeApiUrl}{b24Id}/">{b24Id}</a>';

            //$customerGroupId = $this->config->get('config_customer_group_id');
            //$customerGroupId = $this->model_module_b24_customer->getGroupId();
            //$groupName = $this->getCustomerGroupName($customerGroupId);
            //$discountType = $groupName . ' ' . $this->getDiscountCoef($customerGroupId) . '%';
            //$extraField = [
            //$this->UF_DEAL_PAYMENT_METHOD => $order['payment_method'],
            //$this->UF_DEAL_DISCOUNT_TYPE => $discountType,
            //];
        }


        //$dataToAdd['fields'] = array_merge($dataToAdd['fields'], $extraField);
        //$typeApi = 'lead';
        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_add' => 'crm.' . $typeApi . '.add?' . http_build_query($dataToAdd),
                    'product_add' => 'crm.' . $typeApi . '.productrows.set?id=$result[order_add]&' . http_build_query($productToAdd)
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
        $b24Id = $result['result']['result']['order_add'];
        //$b24Fields = $result['result']['result']['product_add'];

        if (!empty($result['result']['result_error'])) {
            trigger_error('Ошибка при добавлении клиента в Б24 ' . print_r($result['result_error'], 1), E_USER_WARNING);
        }

        /**
         * Todo добиться атомарности операции. В Б24 можно добавить одинаковые товары. Если не запишется в БД,
         * то след раз добавятся еще товары
         */

        $this->addToDB($order_id, $b24Id);

        $findSearch = ['{typeApiUrl}', '{b24Id}', '{typeApiRu}', '{dataToAdd[fields][NAME]}', '{typeApiRu2}'];
        $findReplace = [$typeApiUrl, $b24Id, $typeApiRu, $dataToAdd['fields']['NAME'], $typeApiRu2];

        $message = str_replace($findSearch, $findReplace, $text);

        $params2 = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'im_notify' => 'im.notify?' . http_build_query([
                            'to' => $managerId,
                            'message' => $message
                        ]),
                ],
            ],
        ];

        $this->b24->callHook($params2);
    }

    public function editOrderStatus($order_id, $order_status_id)
    {
        $dataToB24 = $this->initEditOrder($order_id);
        $typeApi = ($dataToB24['fields']['CONTACT_ID'] == 0) ? 'lead' : 'deal';

        $b24OrderStatusById = $this->getStatusById($order_status_id);

        if ($typeApi == 'lead') {
            $b24OrderStatusId = !empty($b24OrderStatusById['b24_status_id']) ? $b24OrderStatusById['b24_status_id'] : 'NEW';
            $dataToB24['fields']['STATUS_ID'] = $b24OrderStatusId;
        } elseif ($typeApi == 'deal') {
            $b24OrderStatusId = !empty($b24OrderStatusById['b24_stage_id']) ? $b24OrderStatusById['b24_stage_id'] : 'NEW';
            $dataToB24['fields']['STAGE_ID'] = $b24OrderStatusId;
        }

        $b24OrderById = $this->getById($order_id);
        $b24OrderId = !empty($b24OrderById['b24_order_id']) ? $b24OrderById['b24_order_id'] : 0;

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_update' => 'crm.' . $typeApi . '.update?id=' . $b24OrderId . http_build_query($dataToB24),
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
    }

    public function editOrder($order_id)
    {
        $this->load->model('module/b24_customer');
        $this->load->model('checkout/order');

        $b24OrderById = $this->getById($order_id);
        $b24OrderId = !empty($b24OrderById['b24_order_id']) ? $b24OrderById['b24_order_id'] : 0;

        $order = $this->model_checkout_order->getOrder($order_id);
        $customerName = $order['firstname'];

        $b24Contact = $this->getContactFromDB($order['customer_id']);

        if (empty($b24Contact)) {
            $b24Contact = $this->getContactFromB24($order['email']);
        }

        $managerId = isset($b24Contact['ASSIGNED_BY_ID']) ? $b24Contact['ASSIGNED_BY_ID'] : $this->model_module_b24_customer->getCurrentManagerId();
        $b24ContactId = !empty($b24Contact['ID']) ? $b24Contact['ID'] : 0;

        $typeApi = (0 == $b24ContactId) ? 'lead' : 'deal';

        $orderName = 'Заказ № ' . $order['order_id'] . ' на сайте ' . $order['store_name'];

        $customerGroupId = $this->config->get('config_customer_group_id');
        //$customerGroupId = $this->model_module_b24_customer->getGroupId();
        //$groupName = $this->getCustomerGroupName($customerGroupId);
        //$discountType = $groupName . ' ' . $this->getDiscountCoef($customerGroupId) . '%';
        $extraField = [
            $this->UF_DEAL_PAYMENT_METHOD => $order['payment_method'],
            //$this->UF_DEAL_DISCOUNT_TYPE => $discountType,
        ];

        $dataToB24 = [
            'fields' => [
                //'ID' => $b24OrderId,
                'TITLE' => $orderName . ' ------------- ',

                //'CURRENCY_ID' => 'RUB',
                //'OPPORTUNITY' => 555,        // hard-code  !!!
                'CONTACT_ID' => $b24ContactId,
                //'BEGINDATE' => '2017-10-30', // hard-code  !!!
                //'OPENED' => 'no',
                //'COMMENTS' => 'Comment',
                'ASSIGNED_BY_ID' => $managerId,
                //'DATE_CREATE' => '2017-10-30', // hard-code  !!!

                //'STAGE_ID' => 'new',
                //'ORIGINATOR_ID' => $order['order_id'],
                //'ORIGIN_ID' => $order['order_id'],
                /*'STATUS_ID' => 'NEW',
                'SOURCE_ID' => 'WEB',
                'OPENED' => 'N',
                */

                /*
                //'CREATED_BY_ID' => self::CREATED_BY,
                //'SOURCE_ID' => self::SOURCE_ID_WEB,
                */

                // LEAD Fields
                'NAME' => $customerName,
                /*'LAST_NAME' => $customerLastname,
                'ADDRESS' => $order['payment_address_1'],
                'ADDRESS_COUNTRY' => $order['payment_country'],
                'ADDRESS_PROVINCE' => $order['payment_zone'],
                'ADDRESS_CITY' => $order['payment_city'],
                'ADDRESS_POSTAL_CODE' => $order['payment_postcode'],
                'PHONE' => [['VALUE' => $customerPhone, "VALUE_TYPE" => "WORK"]],
                'EMAIL' => [['VALUE' => $customerEmail, "VALUE_TYPE" => "WORK"]],
                */
            ]
        ];

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_update' => 'crm.' . $typeApi . '.update?id=' . $b24OrderId . http_build_query($dataToB24),
                    //'product_update' => 'crm.' . $typeApi . '.productrows.set?id=$result[order_add]&' . http_build_query($productToAdd)
                ]
            ]
        ];

        $result = $this->b24->callHook($params);


        if (!empty($result['result']['result_error'])) {
            trigger_error('Ошибка при обновлении заказа в Б24 ' . print_r($result['result_error'], 1), E_USER_WARNING);
        }
    }

    public function prepareProductToB24($order_id)
    {
        $this->load->model('checkout/order');
        $this->load->model('catalog/product');

        $productToAdd = [];
        $productRows = $this->model_checkout_order->getOrderProducts($order_id);
        foreach ($productRows as $product) {
            $productId = $product['product_id'];
            $productData = $this->model_catalog_product->getProduct($productId);
            $mpn = !empty($productData['mpn']) ? $productData['mpn'] . ' | ' : '';

            // region Get B24 Product ID
            $orderOption = $this->model_checkout_order->getOrderOptions($order_id, $product['order_product_id']);
            $productOptions = '';
            foreach ($orderOption as $option) {
                $productOptions .= ' | ' . $option['name'] . ': ' . $option['value'];
            }
            //	$size = $orderOption[0]['value'] ? $orderOption[0]['value'] : self::WITHOUT_SIZE;

            //$filter = ['oc_product_id' => $productId, 'size' => $size];
            //$b24Product = $this->model_module_b24_product->getList($filter)[0];
            //endregion Get B24 Product ID

            //$newProduct = $this->recalculatePriceAndDiscount($product['product_id'], $this->customer->getGroupId());
            $taxRate = ($product['tax'] / $product['price']) * 100;
            $price = $product['price'] + $product['tax'];
            $productName = html_entity_decode(trim($product['name'] . $productOptions));
            $productToAdd['rows'][] = [
                //'PRODUCT_ID' => $b24Product['b24_product_id'],
                'PRODUCT_NAME' => $mpn . $productName,
                'PRICE' => $price,
                //'DISCOUNT_RATE' => $newProduct['discount_rate'],
                //'DISCOUNT_SUM' => $newProduct['discount_sum'],
                //'DISCOUNT_TYPE_ID' => 2,
                'TAX_RATE' => $taxRate,
                'TAX_INCLUDED' => 'N',
                'QUANTITY' => $product['quantity'],
                'MEASURE_CODE' => self::MEASURE_PIECE, // piece
            ];
        }

        $productToAdd = $this->addDeliveryCost($order_id, $productToAdd);

        return $productToAdd;
    }

    public function addDeliveryCost($order_id, array $productToAdd)
    {
        $this->load->model('checkout/order');
        $orderTotalList = $this->model_checkout_order->getOrderTotals($order_id);

        foreach ($orderTotalList as $orderTotal) {
            if ($orderTotal['code'] == 'shipping') {
                $productToAdd['rows'][] = [
                    'PRODUCT_ID' => 0,
                    'PRICE' => $orderTotal['value'],
                    'PRODUCT_NAME' => $orderTotal['title'],
                    'QUANTITY' => 1,
                    'MEASURE_CODE' => self::MEASURE_PIECE, // piece
                ];
            }
        }

        return $productToAdd;
    }

    //public function recalculatePriceAndDiscount($productId, $customerGroupId)
    //{
    //	$this->load->model('module/b24_product');
    //
    //	$product = $this->model_module_b24_product->getProduct($productId);
    //	//$groupCoef = $this->getCustomerGroupCoef();
    //	//$customerGroupId = abs($customerGroupId) > 0 ? $customerGroupId : self::RETAIL_CUSTOMER_GROUP;
    //
    //	//$product['discount_rate'] = 100 - $groupCoef[$customerGroupId];
    //	//$product['discount_sum'] = $product['price'] * ($product['discount_rate'] / 100);
    //	//$product['price'] = $product['price'] + (($groupCoef[self::RETAIL_CUSTOMER_GROUP] / 100) * $product['price'])
    //	//	- $product['discount_sum'];
    //
    //	$realPrice = $product['price'] * 2;
    //	$product['discount_rate'] = $this->getDiscountCoef($customerGroupId);
    //	$product['discount_sum'] = $realPrice * ($product['discount_rate'] / 100);
    //	$product['price'] = $realPrice - $product['discount_sum'];
    //
    //	return $product;
    //}

    private function getFullAddress($orderInfo){
        if($orderInfo['payment_address_format'] === 1){
            return '';
        }

        $format =  '{country}, {zone}, {city}, {address_1} {address_2}, {postcode}';

        $find = array(
            '{firstname}',
            '{lastname}',
            '{company}',
            '{address_1}',
            '{address_2}',
            '{city}',
            '{postcode}',
            '{zone}',
            '{zone_code}',
            '{country}'
        );

        $replace = array(
            'firstname' => $orderInfo['payment_firstname'],
            'lastname'  => $orderInfo['payment_lastname'],
            'company'   => $orderInfo['payment_company'],
            'address_1' => $orderInfo['payment_address_1'],
            'address_2' => $orderInfo['payment_address_2'],
            'city'      => $orderInfo['payment_city'],
            'postcode'  => $orderInfo['payment_postcode'],
            'zone'      => $orderInfo['payment_zone'],
            'zone_code' => $orderInfo['payment_zone_code'],
            'country'   => $orderInfo['payment_country']
        );

        return trim(str_replace($find, $replace, $format));
    }

    public function prepareDataToB24($order_id)
    {
        $this->load->model('checkout/order');
        $this->load->model('module/b24_customer');

        // DATA
        $order = $this->model_checkout_order->getOrder($order_id);
        $orderName = html_entity_decode($order['store_name'], ENT_QUOTES, 'UTF-8') . '. Заказ № ' . $order['order_id'];
        $orderComment = $order['comment'];
        $customerLastname = $order['lastname'];
        $customerName = $order['firstname'];
        $customerEmail = $order['email'];
        $customerPhone = preg_replace("/[^0-9]/", '', $order['telephone']);
        $customerId = $order['customer_id'];
        $roistat = isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : '';
        // DATA

        //MPN
        $productRows = $this->model_checkout_order->getOrderProducts($order_id);
        foreach ($productRows as $product) {
            $productData = $this->model_catalog_product->getProduct($product['product_id']);
            if (!empty($productData['mpn']) && $productData['mpn'] == 'Специальная цена') {
                $mpncode[] = 790;
            } elseif (!empty($productData['mpn']) && $productData['mpn'] == 'Подарок') {
                $mpncode[] = 800;
            } elseif (!empty($productData['mpn']) && $productData['mpn'] == 'Акция') {
                $mpncode[] = 788;
            }
        }
        //MPN

        //Contact
        if ($this->customer->isLogged()) {
            $b24Contact = $this->getContactFromDB($order['customer_id']);
            $b24ContactId = $b24Contact['ID'];
        } else {
            $b24ContactByPhone = $this->getContactFromDBByPhone($customerPhone);
            $b24ContactId = isset($b24ContactByPhone['ID']) ? $b24ContactByPhone['ID'] : 0;

            if (!$b24ContactId) {
                $b24Contact = $this->getContactFromB24($order['email']);
                $b24ContactId = 0;
            }
        }

        $managerId = isset($b24Contact['ASSIGNED_BY_ID']) ? $b24Contact['ASSIGNED_BY_ID'] : $this->model_module_b24_customer->getCurrentManagerId();
        //Contact

        //Адрес
        $shipping_country = isset($order['shipping_country']) ? $order['shipping_country'] . ', ' : '';
        $shipping_city = isset($order['shipping_city']) ? $order['shipping_city'] . ', ' : '';
        $shipping_zone = isset($order['shipping_zone']) ? $order['shipping_zone'] . ', ' : '';
        $shipping_postcode = isset($order['shipping_postcode']) ? $order['shipping_postcode'] . ', ' : '';
        $shipping_address_1 = isset($order['shipping_address_1']) ? $order['shipping_address_1'] : '';

        $this->log->write(print_r($_COOKIE, true));

        if ($order['payment_code'] == 'cod') {
            $payment_code = 336;
        } elseif ($order['payment_code'] == 'rbs') {
            $payment_code = 340;
        } elseif ($order['payment_code'] == 'kupivkredit') {
            $payment_code = 342;
        }

        $orderTotalList = $this->model_checkout_order->getOrderTotals($order_id);
        foreach ($orderTotalList as $orderTotal) {
            if ($orderTotal['code'] == 'shipping') {
                $deliveryCost = $orderTotal['value'];
            }
            if ($orderTotal['code'] == 'sub_total') {
                $subTotal = $orderTotal['value'];
            }
        }
        $typeOrderGo = $order['payment_address_format'] == 1 ? 'Заказ в 1 клик' : 'Заказ из корзины';


        //$managerId = $this->model_module_b24_customer->getCurrentManagerId();
        $dataToB24 = [
            'fields' => [
                'TITLE' => $orderName,
                'STATUS_ID' => 'NEW',
                'CURRENCY_ID' => $this->config->get('config_currency'),
                'SOURCE_ID' => 'WEB',
                'OPENED' => 'N',
                'ASSIGNED_BY_ID' => $managerId,
                'CONTACT_ID' => $b24ContactId,
                'COMMENTS' => $orderComment . '<br> <b>Адрес:</b> ' . $shipping_country . '' . $shipping_city . '' . $shipping_zone . '' . $shipping_postcode . '' . $shipping_address_1 . '',
                'UTM_SOURCE' => isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : '',
                'UTM_MEDIUM' => isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : '',
                'UTM_CAMPAIGN' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
                'UTM_CONTENT' => isset($_COOKIE['utm_content']) ? $_COOKIE['utm_content'] : '',
                'UTM_TERM' => isset($_COOKIE['utm_term']) ? $_COOKIE['utm_term'] : '',

                // LEAD Fields
                'NAME' => $customerName,
                'LAST_NAME' => $customerLastname,
                'ADDRESS' => $order['payment_address_1'],
                'ADDRESS_COUNTRY' => $order['payment_country'],
                'ADDRESS_PROVINCE' => $order['payment_zone'],
                'ADDRESS_CITY' => $order['payment_city'],
                'ADDRESS_POSTAL_CODE' => $order['payment_postcode'],
                'PHONE' => [['VALUE' => $customerPhone, "VALUE_TYPE" => "WORK"]],
                'EMAIL' => [['VALUE' => $customerEmail, "VALUE_TYPE" => "WORK"]],
                'UF_CRM_1576410299' => $roistat, // Ройстат лида
                'UF_CRM_AMO_485281' => $roistat, // Ройстат сделки
                'UF_CRM_1574497327' => $subTotal ?? 0, // Лид бюджет
                'UF_CRM_1574611202' => round($deliveryCost ?? 0, 0), // Лид доставка
                'UF_CRM_5DDAB6126FFA7' => round($deliveryCost ?? 0, 0), // Сделка доставка
                'UF_CRM_1574611974' => $payment_code ?? '', // Лид оплата
                'UF_CRM_5DDAB6129F36A' => $payment_code ?? '', // Сделка оплата
                'UF_CRM_5DEE314A8FA15' => $typeOrderGo, // Лид комментарий
                'UF_CRM_1592562806' => $mpncode ?? '', // Промо
                self::FIELD_FULL_ADDRESS => $this->getFullAddress($order),
                self::FIELD_SHIPPING_METHOD => $order['shipping_method'] ?? ''
            ]
        ];
        $this->log->write(print_r($dataToB24, true));

        return $dataToB24;
    }

    public function getContactFromDB($customerId)
    {
        $this->load->model('module/b24_customer');
        if (abs($customerId) <= 0) {
            return [];
        }

        $b24Row = $this->model_module_b24_customer->getById($customerId);
        $b24Contact = json_decode($b24Row['b24_contact_field'] ?? "", 1);

        return $b24Contact;
    }

    public function getContactFromDBByPhone($phone)
    {
        $result = [];

        $query = $this->db->query("SELECT * FROM b24_customer WHERE phone = '" . $phone . "'");

        if ($query->num_rows) {
            $result = json_decode($query->row['b24_contact_field'] ?? "", 1);
        }

        return $result;
    }

    public function getContactFromB24($contactEmail)
    {
        $B24ContactList = $this->getB24ContactList(['EMAIL' => $contactEmail]);
        $b24Contact = isset($B24ContactList[0]) ? $B24ContactList[0] : [];

        return $b24Contact;
    }

    public function getDiscountCoef($customerGroupId)
    {
        if (abs($customerGroupId) <= 0) {
            $customerGroupId = self::RETAIL_CUSTOMER_GROUP;
        }

        $groupCoef = $this->getCustomerGroupCoef();
        $discount = $groupCoef[$customerGroupId];

        return $discount;
    }

    public function getCustomerGroupName($customerGroupId)
    {
        if (abs($customerGroupId) <= 0) {
            $customerGroupId = self::RETAIL_CUSTOMER_GROUP;
        }
        $this->load->model('account/customer_group');

        $groupName = $this->model_account_customer_group->getCustomerGroup($customerGroupId)['name'];

        return $groupName;
    }

    public function getCustomerGroupCoef()
    {
        $sql = "SELECT * FROM b24_order_config WHERE name = '" . self::CONFIG_CUSTOMER_GROUP_COEF . "'";
        $query = $this->db->query($sql);
        print_r($query);

        return json_decode($query->row['value'], 1);
    }

    public function getB24ContactList($filter)
    {
        if (empty($filter)) {
            trigger_error('Empty filter', E_USER_WARNING);
        }

        foreach ($filter as $value) {
            if (empty($value)) {
                return false;
            }
        }

        $params = [
            'type' => 'crm.contact.list',
            'params' => [
                'filter' => $filter
            ]
        ];

        $result = $this->b24->callHook($params);

        return $result['result'];
    }

    public function insertToDB(array $fields)
    {
        $db = $this->db;

        $sql = 'REPLACE INTO ' . self::TABLE_NAME . ' SET ' . $this->prepareFields($fields) . ';';
        $db->query($sql);

        $lastId = $this->db->getLastId();

        return $lastId;
    }

    public function prepareFields(array $fields)
    {
        $sql = '';
        $index = 0;
        foreach ($fields as $columnName => $value) {
            $glue = $index === 0 ? ' ' : ', ';
            $sql .= $glue . "`$columnName`" . ' = "' . $this->db->escape($value) . '"';
            $index++;
        }

        return $sql;
    }

    public function getStatusById($order_status_id)
    {
        if (abs($order_status_id) <= 0) {
            trigger_error('ID must be integer', E_USER_WARNING);
        }

        $db = $this->db;
        $sql = 'SELECT * FROM b24_status_map WHERE oc_status_id = "' . $db->escape($order_status_id) . '"';
        $query = $db->query($sql);

        return $query->row;
    }

    public function getById($order_id)
    {
        if (abs($order_id) <= 0) {
            trigger_error('ID must be integer', E_USER_WARNING);
        }

        $db = $this->db;
        $sql = 'SELECT * FROM ' . self::TABLE_NAME . ' WHERE oc_order_id = "' . $db->escape($order_id) . '"';
        $query = $db->query($sql);

        return $query->row;
    }

    public function getList(array $filter)
    {
        $db = $this->db;
        $where = ' WHERE ';
        $index = 0;
        foreach ($filter as $columnName => $value) {
            $glue = $index === 0 ? ' ' : ' AND ';
            $where .= $glue . $columnName . ' = "' . $db->escape($value) . '"';
            $index++;
        }

        $sql = 'SELECT * FROM ' . self::TABLE_NAME . $where . ';';
        $query = $db->query($sql);

        return $query->rows;
    }
}

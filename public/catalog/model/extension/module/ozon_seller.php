<?php

class ModelExtensionModuleOzonSeller extends Model
{
    public function saveOzonCategory($response)
    {
        $query = $this->db->query("DELETE FROM ".DB_PREFIX."ozon_category");

        foreach ($response as $respons) {
            header('Content-Type: text/html; charset=utf-8');
            foreach ($respons as $respon) {
                if ( ! empty($respon['children'])) {
                    $subcategory = $respon['children'];
                    foreach ($subcategory as $resp) {
                        if ( ! empty($resp['children'])) {
                            $subcategory1 = $resp['children'];
                            foreach ($subcategory1 as $res) {
                                if ( ! empty($res['children'])) {
                                    $subcategory2 = $res['children'];
                                    foreach ($subcategory2 as $re) {
                                        $title            = $respon['title'].' > '.$resp['title'].' > '.$res['title']
                                            .' > '.$re['title'];
                                        $ozon_category_id = $re['category_id'];
                                        $this->db->query("INSERT INTO ".DB_PREFIX
                                            ."ozon_category SET ozon_category_id = '".(int)$ozon_category_id
                                            ."', title = '".$this->db->escape($title)."'");
                                    }
                                } else {
                                    $title            = $respon['title'].' > '.$resp['title'].' > '.$res['title'];
                                    $ozon_category_id = $res['category_id'];
                                    $this->db->query("INSERT INTO ".DB_PREFIX."ozon_category SET ozon_category_id = '"
                                        .(int)$ozon_category_id."', title = '".$this->db->escape($title)."'");
                                }
                            }
                        } else {
                            $title            = $respon['title'].' > '.$resp['title'];
                            $ozon_category_id = $resp['category_id'];
                            $this->db->query("INSERT INTO ".DB_PREFIX."ozon_category SET ozon_category_id = '"
                                .(int)$ozon_category_id."', title = '".$this->db->escape($title)."'");
                        }
                    }
                }
            }
        }
    }

    public function deleteOrder($posting_number)
    {

        $this->db->query("DELETE FROM ".DB_PREFIX."ozon_ms_order_guid WHERE posting_number = '"
            .$this->db->escape($posting_number)."'");
    }

    public function getMyOrder($posting_number)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_ms_order_guid WHERE posting_number = '"
            .$this->db->escape($posting_number)."'");

        return $query->rows;
    }

    public function getMyOrders($postings)
    {

        $postings = implode("','", $postings);

        $query = $this->db->query("SELECT guid FROM ".DB_PREFIX."ozon_ms_order_guid WHERE posting_number IN ('"
            .$postings."')");

        return $query->rows;
    }

    public function saveOrder($posting_number, $shipment_date, $status)
    {

        $query = $this->db->query("INSERT INTO ".DB_PREFIX."ozon_ms_order_guid SET posting_number = '"
            .$this->db->escape($posting_number)."', shipment_date = '".$this->db->escape($shipment_date)."', status = '"
            .$this->db->escape($status)."'");
    }

    public function saveOrderFull($posting_number, $shipment_date, $status, $guid)
    {

        $query = $this->db->query("INSERT INTO ".DB_PREFIX."ozon_ms_order_guid SET posting_number = '"
            .$this->db->escape($posting_number)."', shipment_date = '".$this->db->escape($shipment_date)."', status = '"
            .$this->db->escape($status)."', guid = '".$this->db->escape($guid)."'");
    }

    public function updateShipmentDate($posting_number, $shipment_date)
    {

        $query = $this->db->query("UPDATE ".DB_PREFIX."ozon_ms_order_guid SET shipment_date = '"
            .$this->db->escape($shipment_date)."' WHERE posting_number = '".$this->db->escape($posting_number)."'");
    }

    public function saveOrderGuid($posting_number, $guid)
    {

        $query = $this->db->query("UPDATE ".DB_PREFIX."ozon_ms_order_guid SET guid = '".$this->db->escape($guid)
            ."' WHERE posting_number = '".$this->db->escape($posting_number)."'");
    }

    public function getOrderByStatus($status)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_ms_order_guid WHERE status = '"
            .$this->db->escape($status)."'");

        return $query->rows;
    }

    public function getOrderByStatusMonthOld($data = array())
    {
        $date = new DateTime();
        $date->modify($data['month']);
        $date = $date->format('Y-m-d');

        $sql = "SELECT * FROM ".DB_PREFIX."ozon_ms_order_guid WHERE status = '".$this->db->escape($data['status'])
            ."' AND shipment_date > '".$date."'";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOrder($guid)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_ms_order_guid WHERE guid = '"
            .$this->db->escape($guid)."'");

        return $query->rows;
    }

    public function updateStatusOzon($posting_number, $status_ozon)
    {

        $query = $this->db->query("UPDATE ".DB_PREFIX."ozon_ms_order_guid SET status = '"
            .$this->db->escape($status_ozon)."' WHERE posting_number = '".$this->db->escape($posting_number)."'");
    }

    public function saveAttributeRequired($category_id, $attribute_id)
    {

        $sql = "INSERT IGNORE INTO ".DB_PREFIX."ozon_attribute_required SET category_id = '".(int)$category_id
            ."', attribute_id = '".(int)$attribute_id."'";

        $this->db->query($sql);
    }

    public function saveAttributeDescription(
        $ozon_attribute_id,
        $ozon_attribute_name,
        $ozon_attribute_description,
        $ozon_dictionary_id,
        $required
    ) {

        $sql = "INSERT IGNORE INTO ".DB_PREFIX."ozon_attribute_description SET ozon_attribute_id = '"
            .(int)$ozon_attribute_id."', ozon_attribute_name = '".$this->db->escape($ozon_attribute_name)
            ."', ozon_attribute_description = '".$this->db->escape($ozon_attribute_description)
            ."', ozon_dictionary_id = '".$this->db->escape($ozon_dictionary_id)."', required = '"
            .$this->db->escape($required)."'";

        $this->db->query($sql);
    }

    public function getOzonAttributeDescription()
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_attribute_description");

        return $query->rows;
    }

    public function saveAttribute($ozon_category_id, $ozon_attribute_id)
    {

        $id = $ozon_category_id.$ozon_attribute_id;

        $sql = "INSERT IGNORE INTO ".DB_PREFIX."ozon_attribute SET id = '".(int)$id."', ozon_category_id = '"
            .(int)$ozon_category_id."', ozon_attribute_id = '".(int)$ozon_attribute_id."'";

        $this->db->query($sql);
    }

    public function getOzonAttribute($ozon_category_id)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_attribute WHERE ozon_category_id = '"
            .$ozon_category_id."'");

        return $query->rows;
    }

    public function getDictionaryShoptoOzon()
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_to_shop_dictionary");

        return $query->rows;
    }

    public function getShopDictionary($product_id, $language_id)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."product_attribute WHERE product_id = '".$product_id
            ."' AND language_id = '".$language_id."'");

        return $query->rows;
    }

    public function getLanguage($code)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."language WHERE code = '".$code."'");

        return $query->row['language_id'];
    }

    public function saveDictonary(
        $ozon_dictionary_id,
        $attribute_value_id,
        $ozon_category_id,
        $ozon_attribute_id,
        $text
    ) {

        if ($ozon_attribute_id != 8229 && $ozon_attribute_id != 9461) {
            $ozon_category_id = '';
        }

        $query = $this->db->query("REPLACE INTO ".DB_PREFIX."ozon_dictionary SET dictionary_id = '"
            .(int)$ozon_dictionary_id."', attribute_value_id = '".(int)$attribute_value_id."', category_id = '"
            .(int)$ozon_category_id."', attribute_group_id = '".(int)$ozon_attribute_id."', text = '"
            .$this->db->escape($text)."'");
    }

    public function getDictionaryByCategoryAndAttributeId($category_id, $attribute_value_id)
    {

        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_dictionary WHERE category_id = '"
            .$this->db->escape($category_id)."' AND attribute_value_id = '".$this->db->escape($attribute_value_id)."'");

        return $query->rows[0];
    }

    public function saveExportProduct($export_table = array(), $task_id, $status = 'posting')
    {
        foreach ($export_table as $data) {
            $query = $this->db->query("REPLACE INTO ".DB_PREFIX."ozon_products SET product_id = '"
                .(int)$data['product_id']."', model = '".$this->db->escape($data['model'])."', sku = '"
                .$this->db->escape($data['sku'])."', status = '".$this->db->escape($status)
                ."', date = NOW(), task_id = '".$this->db->escape($task_id)."'");
        }
    }

    public function getExportProduct($ozon_product_id)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_products WHERE ozon_product_id = '"
            .$this->db->escape($ozon_product_id)."'");
        if (empty($query->rows)) {
            $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_products WHERE ozon_sku = '"
                .$this->db->escape($ozon_product_id)."'");
        }

        return $query->rows;
    }

    public function chekTaskId()
    {
        $query = $this->db->query("SELECT DISTINCT task_id FROM ".DB_PREFIX
            ."ozon_products WHERE status != 'processed'");

        return $query->rows;
    }

    public function updateExportProduct($status, $offer_id, $ozon_sku, $ozon_product_id, $error)
    {
        if ($this->config->get('ozon_seller_entry_offer_id')) {
            $query = $this->db->query("UPDATE ".DB_PREFIX."ozon_products SET status = '".$this->db->escape($status)
                ."', ozon_sku = '".(int)$ozon_sku."', ozon_product_id = '".(int)$ozon_product_id."', error = '"
                .$this->db->escape($error)."' WHERE model = '".$this->db->escape($offer_id)."'");
        } else {
            $query = $this->db->query("UPDATE ".DB_PREFIX."ozon_products SET status = '".$this->db->escape($status)
                ."', ozon_sku = '".(int)$ozon_sku."', ozon_product_id = '".(int)$ozon_product_id."', error = '"
                .$this->db->escape($error)."' WHERE sku = '".$this->db->escape($offer_id)."'");
        }
    }

    public function downloadProduct($product_id, $model, $sku, $ozon_product_id)
    {
        $query = $this->db->query("INSERT IGNORE INTO ".DB_PREFIX."ozon_products SET product_id = '".(int)$product_id
            ."', model = '".$this->db->escape($model)."', sku = '".$this->db->escape($sku)
            ."', status = 'processed', date = NOW(), ozon_product_id = '".(int)$ozon_product_id."'");
    }

    public function deletedExportProduct($product_id)
    {
        $query = $this->db->query("DELETE FROM ".DB_PREFIX."ozon_products WHERE product_id = '"
            .$this->db->escape($product_id)."'");
    }

    public function getProductByModel($model)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE model = '".$this->db->escape($model)."'");

        return $query->rows;
    }

    public function getProductBySku($sku)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE sku = '".$this->db->escape($sku)."'");

        return $query->rows;
    }

    public function getProduct($product_id)
    {
        $query
            = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM "
            .DB_PREFIX."product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '"
            .(int)$this->config->get('config_customer_group_id')
            ."' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT wcd.unit FROM "
            .DB_PREFIX
            ."weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '"
            .(int)$this->config->get('config_language_id')."') AS weight_class, (SELECT lcd.unit FROM ".DB_PREFIX
            ."length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '"
            .(int)$this->config->get('config_language_id')."') AS length_class, p.sort_order FROM ".DB_PREFIX
            ."product p LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "
            .DB_PREFIX."product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN ".DB_PREFIX
            ."manufacturer m ON (p.manufacturer_id = m.manufacturer_id) LEFT JOIN ".DB_PREFIX
            ."product_to_category p2c ON (p.product_id = p2c.product_id) WHERE p.product_id = '".(int)$product_id
            ."' AND pd.language_id = '".(int)$this->config->get('config_language_id')
            ."' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '"
            .(int)$this->config->get('config_store_id')."'");

        if ($query->num_rows) {
            return array(
                'product_id'       => $query->row['product_id'],
                'name'             => $query->row['name'],
                'category_id'      => $query->row['category_id'],
                'description'      => $query->row['description'],
                'meta_title'       => $query->row['meta_title'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword'     => $query->row['meta_keyword'],
                'tag'              => $query->row['tag'],
                'model'            => $query->row['model'],
                'sku'              => $query->row['sku'],
                'upc'              => $query->row['upc'],
                'ean'              => $query->row['ean'],
                'jan'              => $query->row['jan'],
                'isbn'             => $query->row['isbn'],
                'mpn'              => $query->row['mpn'],
                'quantity'         => $query->row['quantity'],
                'image'            => $query->row['image'],
                'manufacturer_id'  => $query->row['manufacturer_id'],
                'manufacturer'     => $query->row['manufacturer'],
                'price'            => $query->row['price'],
                'special'          => $query->row['special'],
                'tax_class_id'     => $query->row['tax_class_id'],
                'date_available'   => $query->row['date_available'],
                'weight'           => $query->row['weight'],
                'weight_class_id'  => $query->row['weight_class_id'],
                'length'           => $query->row['length'],
                'width'            => $query->row['width'],
                'height'           => $query->row['height'],
                'length_class_id'  => $query->row['length_class_id'],
                'minimum'          => $query->row['minimum'],
                'sort_order'       => $query->row['sort_order'],
                'status'           => $query->row['status'],
                'viewed'           => $query->row['viewed'],
            );
        } else {
            return false;
        }
    }

    public function getProducts($data = array())
    {
        $filters                  = $this->config->get('ozon_seller_filter');
        $additional_settings      = $this->config->get('ozon_seller_additional_settings');
        $additional_settings_sql  = ' AND p.actual_price = 1';
        $price_type_sql           = [];
        $additional_settings_join = '';

        if (isset($additional_settings) && is_array($additional_settings)) {
            foreach ($additional_settings as $key => $values) {
                switch ($key) {
                    case 'manufacturer':
                        $additional_settings_sql .= " AND p.manufacturer_id > 0";
                        break;
                    case 'sku':
                        $additional_settings_sql .= " AND p.sku != '' AND p.sku IS NOT NULL";
                        break;
                    case 'dimensions':
                        $additional_settings_sql .= " AND p.length > 0 AND p.width > 0 AND p.height > 0";
                        break;
                    case 'weight':
                        $additional_settings_sql .= " AND p.weight > 0";
                        break;
                    case 'spec_price':
                        $price_type_sql[] = "p.price_type = 'Специальная цена'";
                        break;
                    case 'discount_price':
                        $price_type_sql[] = "p.price_type = 'Акция'";
                        break;
                    case 'attributes':
                        foreach ($values as $attribute_id) {
                            $additional_settings_sql  .= " AND attrs{$attribute_id}.attribute_id = '".(int)$attribute_id
                                ."'";
                            $additional_settings_join .= " LEFT JOIN ".DB_PREFIX
                                ."product_attribute attrs{$attribute_id} ON attrs{$attribute_id}.product_id = p.product_id";
                        }
                        break;
                }
            }

            if ($price_type_sql) {
                /** если выбраны несколько типов цены */
                if (count($price_type_sql) > 1) {
                    $additional_settings_sql .= " AND (".implode(' OR ', $price_type_sql).")";
                } else {
                    $additional_settings_sql .= " AND $price_type_sql[0]";
                }
            }
        }

        $diapasons = [];

        if(isset($filters) && is_array($filters)) {
            foreach ($filters as $filter) {
                $diapason = "(p.price >= ".$filter['priceFrom']." AND p.price <= ".$filter['priceTo']
                    ." AND p.quantity >= ".$filter['quantityFrom']." AND p.quantity <= ".$filter['quantityTo']."";

                if (isset($filter['manufacturers'])) {
                    $diapason .= " AND p.manufacturer_id IN (".implode(',', $filter['manufacturers']).")";
                }
                if (isset($filter['suppliers'])) {
                    $diapason .= " AND p.supplier_main IN ('".implode("','", $filter['suppliers'])."')";
                }

                $diapason .= ")";

                $diapasons[] = $diapason;
            }
        }

        $sql = "SELECT p.product_id, p.price, p.sup_price, p.price_type, (SELECT price FROM ".DB_PREFIX
            ."product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '"
            .(int)$this->config->get('config_customer_group_id')
            ."' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

        if ( ! empty($data['filter_category_id'])) {
            if ( ! empty($data['filter_sub_category'])) {
                $sql .= " FROM ".DB_PREFIX."category_path cp LEFT JOIN ".DB_PREFIX
                    ."product_to_category p2c ON (cp.category_id = p2c.category_id)";
            } else {
                $sql .= " FROM ".DB_PREFIX."product_to_category p2c";
            }

            $sql .= " LEFT JOIN ".DB_PREFIX."product p ON (p2c.product_id = p.product_id)";

        } else {
            $sql .= " FROM ".DB_PREFIX."product p";
        }

        if ($this->config->get('ozon_seller_export_stock_null')) {
            $quantity = '>=';
        } else {
            $quantity = '>';
        }

        $sql .= $additional_settings_join;
        $sql .= " LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "
            .DB_PREFIX."product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '"
            .(int)$this->config->get('config_language_id')."' AND p.status = '1' AND p.quantity ".$quantity
            ." 0 AND image IS NOT NULL AND p.date_available <= NOW() AND p2s.store_id = '"
            .(int)$this->config->get('config_store_id')."' AND NOT p.product_id IN (SELECT product_id FROM "
            .DB_PREFIX."ozon_products)";

        if ( ! empty($data['filter_category_id'])) {
            if ( ! empty($data['filter_sub_category'])) {
                $sql .= " AND cp.path_id = '".(int)$data['filter_category_id']."'";
            } else {
                $sql .= " AND p2c.category_id = '".(int)$data['filter_category_id']."'";
            }
        }

        if ( ! empty($data['exclude_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id NOT IN (".$data['exclude_manufacturer_id'].")";
        }

        if($diapasons) {
            $sql .= " AND (".implode(" OR ", $diapasons).")";
        }
        $sql .= $additional_settings_sql;

        $sql .= " GROUP BY p.product_id";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 100;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $product_data = [];

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            /** если включен расчет маржи */
            if (isset($additional_settings['marginOn']) && (int)$additional_settings['marginPercent']
                && (int)$additional_settings['marginSumEnd']
            ) {
                if ($result['price'] >= (int)$additional_settings['marginSumStart']
                    && $result['price'] <= (int)$additional_settings['marginSumEnd']
                    && $result['price_type'] != 'Акция'
                    && $result['price_type'] != 'Специальная цена'
                ) {
                    $margin = ($result['price'] - $result['sup_price']) / $result['price'] * 100;
                    if ($margin < (int)$additional_settings['marginPercent']) {
                        continue;
                    }
                }
            }
            
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }


    public function updateProducts($data = array())
    {

        $sql = "SELECT * FROM ".DB_PREFIX."ozon_products WHERE status IN ('processed', 'imported') GROUP BY product_id";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 1000;
            }

            $sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
        }

        $product_data = array();

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }

    public function saveManufacturer($ozon_id, $value, $picture)
    {
        $query = $this->db->query("INSERT IGNORE INTO ".DB_PREFIX."ozon_manufacturer SET ozon_id = '".(int)$ozon_id
            ."', value = '".$this->db->escape($value)."', picture = '".$this->db->escape($picture)."'");
    }

    public function getManufacturer($shop_id)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."ozon_manufacturer WHERE shop_id = '"
            .$this->db->escape($shop_id)."'");

        return $query->rows;
    }

    public function getOrderOc($posting_number)
    {
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."order WHERE lastname = '"
            .$this->db->escape($posting_number)."'");

        return $query->rows;
    }
}

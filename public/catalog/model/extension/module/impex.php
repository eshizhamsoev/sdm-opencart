<?php

set_time_limit(1200);
ini_set('max_execution_time', 1200);

class ModelExtensionModuleImpex extends Model {
	
	// VARS
	private $STORE_ID					= 0;
	private $LANG_ID					= 0;
	
	private $FULL_IMPORT				= false;
	private $NOW 						= '';
	private $TAB_FIELDS					= array();
	private $ERROR						= 0;
	private $XML_VER					= "";

	// Классификатор
	private $TAXES						= array();
	private	$MANUFACTURERS				= array();
	private	$CATEGORIES					= array();
	private	$ATTRIBUTES					= array();
	private	$ATTRIBUTE_GROUPS			= array();
	private $PRODUCT_CATEGORIES 		= array();

	// Статистика
	private $STAT						= array();
	
	//Settings
	private $SET_USER					= '';
	private $SET_BASE					= '';
	private $SET_MODE					= 'insert';
	private $SET_CATEGORIES				= false;
	private $SET_IMAGE					= false;
	private $SET_FILES					= false;
	private $SET_PRICE					= false;
	private $SET_STOCK					= false;
	private $SET_BATCHES				= 0;
	private $SET_BATCH					= 0;
	private $SET_BATCH_SIZE				= 0;
	private $SET_FILE					= 0;
	

	private function error() {
		$this->log->write("ОШИБКА " . $this->ERROR . ". Смотрите описание ошибки в справке модуля обмена.");
		return $this->ERROR;
	} 

	private function log($message, $level = 1, $line = '') {

		if ($this->config->get('exchange1c_log_debug_line_view') == 1) {
			if (!$line) {
				list ($di) = debug_backtrace();
				$line = sprintf("%04s",$di["line"]);
			}
		} else {
			$line = '';
		}

		if (is_array($message) || is_object($message)) {
			$this->log->write($line . "M:");
			$this->log->write(print_r($message, true));
		} else {
			if (mb_substr($message,0,1) == '~') {
				$this->log->write('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
				$this->log->write($line . "M " . mb_substr($message, 1));
			} else {
				$this->log->write($line . "M " . $message);
			}
		}
	} 

	private function errorLog($error_num, $arg1 = '', $arg2 = '', $arg3 = '') {
		$this->ERROR = $error_num;
		$message = $this->language->get('error_' . $error_num . '_log');
		if (!$message) {
			$this->language->get('error_' . $error_num);
		}
		if ($message && $this->config->get('exchange1c_log_level') > 0) {
			list ($di) = debug_backtrace();
			$debug = "Строка ошибки: " . sprintf("%04s",$di["line"]) . " - ";
			$this->log->write(sprintf($debug . $message, $arg1, $arg2, $arg3));
		}
	} 
	
	public function getLanguageId($lang) {

		if ($this->LANG_ID) {
			return $this->LANG_ID;
		}
		$query = $this->db->query("SELECT `language_id` FROM `" . DB_PREFIX . "language` WHERE `code` = '" . $this->db->escape($lang) . "'");
		$this->LANG_ID = $query->row['language_id'];
		return $this->LANG_ID;

	} 
	
	public function importFile($importFile, $type) {

		$this->log("~НАЧАЛО ЗАГРУЗКИ ДАННЫХ");
		
		$this->NOW = date('Y-m-d H:i:s');
		$this->log("Единое время загрузки: " . $this->NOW, 2);

		$this->getLanguageId($this->config->get('config_language'));
		$this->log("Язык загрузки, id: " . $this->LANG_ID, 2);

		$this->log("~ТИП ЗАГРУЗКИ: " . $type, 2);

		// Читаем XML
		libxml_use_internal_errors(true);
		$path_parts = pathinfo($importFile);
		
		$filename = $path_parts['basename'];
		$this->log("Читаем XML файл: '" . $filename . "'", 2);
		
		$this->SET_FILE = $filename; 

		if (is_file($importFile)) {

			$xml = simplexml_load_file($importFile);
			
			if (!$xml) {
				$this->log(libxml_get_errors(), 2);
				return $this->error();
			}

		} else {
			return $this->error();
		}
		
		if ($xml->settings->base) {
			$this->SET_BASE = $xml->settings->base; 
			$this->log("~База 1С в файле = " . $this->SET_BASE,2);
			$bases = $this->config->get('module_impex_base');
			$base  = '' . $this->SET_BASE . '';
			$pos = strpos($bases, $base);
			if ($pos === false) {
				$this->log("Загрузка отменена - несоответствие базы данных 1С",2);
				return $this->error();
			}
		}
		
		if ($xml->settings->user) {
			$this->SET_USER = $xml->settings->user; 
		}
		
		if ($xml->settings->mode) {
			$this->SET_MODE = $xml->settings->mode; 
		}
		
		if ($xml->settings->categories) {
			$this->SET_CATEGORIES = true; 
		}
		
		if ($xml->settings->image) {
			$this->SET_IMAGE = true; 
		}
		
		if ($xml->settings->files) {
			$this->SET_FILES = true; 
		}
		
		if ($xml->settings->price) {
			$this->SET_PRICE = true; 
		}
		
		if ($xml->settings->stock) {
			$this->SET_STOCK = true; 
		}
		
		if ($type == 'fastcatalogfull' || $type == 'fastcatalogshort') {
			$this->SET_PRICE = false; 
			$this->SET_STOCK = false; 
		}
		
		if ($xml->settings->batches) {
			$this->SET_BATCHES = $xml->settings->batches; 
		}
		
		if ($xml->settings->batches) {
			$this->SET_BATCH = $xml->settings->batch; 
		}
		
		if ($xml->settings->batches) {
			$this->SET_BATCH_SIZE = $xml->settings->batch_size; 
		}
		
		$this->log("~РЕЖИМ ЗАГРУЗКИ = " . $this->SET_MODE,2);
		$this->log("~ПОЛЬЗОВАТЕЛЬ = " . $this->SET_USER,2);
		$this->log("~ЗАГРУЖАТЬ КАТЕГОРИИ = " . $this->SET_CATEGORIES,2);
		$this->log("~ЗАГРУЖАТЬ КАРТИНКИ = " . $this->SET_IMAGE,2);
		$this->log("~ЗАГРУЖАТЬ ФАЙЛЫ = " . $this->SET_FILES,2);
		$this->log("~ЗАГРУЖАТЬ ЦЕНЫ = " . $this->SET_PRICE,2);
		$this->log("~ЗАГРУЖАТЬ ОСТАТКИ = " . $this->SET_STOCK,2);
		$this->log("~КОЛИЧЕСТВО БАТЧЕЙ = " . $this->SET_BATCHES,2);
		$this->log("~РАЗМЕР БАТЧА = " . $this->SET_BATCH_SIZE,2);
		$this->log("~НОМЕР БАТЧА = " . $this->SET_BATCH,2);

		if ($xml->categories) {
			$this->log("~ЗАГРУЗКА КАТЕГОРИЙ---",2);
			$this->importCategories($xml->categories->category, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->categories);
		}

		if ($xml->manufacturers) {
			$this->log("~ЗАГРУЗКА ПРОИЗВОДИТЕЛЕЙ---",2);
			$this->importManufacturers($xml->manufacturers->manufacturer, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->manufacturers);
		}
		
		if ($xml->attributes) {
			$this->log("~ЗАГРУЗКА АТРИБУТОВ---",2);
			$this->importAttributes($xml->attributes->attribute, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->attributes);
		}

		if ($xml->products) {
			$this->log("~ЗАГРУЗКА ТОВАРОВ---",2);
			$this->importProducts($xml->products->product, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->products);
		}

		if ($xml->stockprice) {
			$this->log("~ЗАГРУЗКА ОСТАТКОВ и ЦЕН---",2);
			$this->importStockPrice($xml);
			if ($this->ERROR) return $this->error();
			unset($xml->stockprice);
		}

		if ($xml->gift) {
			$this->log("~ЗАГРУЗКА ПОДАРКОВ---",2);
			$this->importGift($xml);
			if ($this->ERROR) return $this->error();
			unset($xml->gift);
		}

		if ($xml->stockprices) {
			$this->log("~ЗАГРУЗКА ОСТАТКОВ и ЦЕН в фоне с параметрами---",2);
			$this->importStockPriceParam($xml->stockprices->stockprice);
			if ($this->ERROR) return $this->error();
			unset($xml->stockprice);
		}

		if ($xml->content) {
			$this->log("~ЗАГРУЗКА КОНТЕНТА",2);
			$this->importContent($xml);
			if ($this->ERROR) return $this->error();
			unset($xml->content);
		}
		
		if ($xml->category) {
			$this->log("~ЗАГРУЗКА КАТЕГОРИЙ",2);
			$this->importCategories($xml, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->category);
		}
		
		if ($xml->manufacturer) {
			$this->log("~ЗАГРУЗКА ПРОИЗВОДИТЕЛЕЙ",2);
			$this->importManufacturers($xml, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->manufacturer);
		}
		
		if ($xml->attribute) {
			$this->log("~ЗАГРУЗКА АТРИБУТОВ",2);
			$this->importAttributes($xml, 0, $this->LANG_ID);
			if ($this->ERROR) return $this->error();
			unset($xml->attribute);
		}
		
		$this->log("~КОНЕЦ ЗАГРУЗКИ ДАННЫХ");

		return "";
	}
	
	private function importStockPrice($xml) {
		
		foreach ($xml as $product){
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			if ($product_id) {
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET vendor_code = '" . $this->db->escape($product->vendor_code) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				if ($product->delete) {
					$this->log->write("УДАЛИТЬ Товар : " . $product_id . " - " . $product->product_name);
					$this->log->write("---------------------------");
					
					$this->deleteProduct($product_id);
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$product_id . "' AND object = 'product'");	
					
					continue;
				}
				
				$price = $product->price == 0 ? 0 : $product->price;
				$sup_price = $product->sup_price == 0 ? 0 : $product->sup_price;
				$stock = $product->stock == 0 ? 0 : $product->stock;
				$special = $product->special == 0 ? 0 : $product->special; 
				$mpn = '';
				$actual_price = isset($product->actual_price) ? $product->actual_price : 1;
				
				if ($product->specprice == 'Специальная цена') {
					$mpn = 'Специальная цена';
				}
				
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
					
				if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
					
					if ($product->action == 'Акция') {
						$mpn = 'Акция';
					}
					
//					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
				}
				
				
				if ($product->out_of_sale) {
					//$stock_status_id = $this->config->get('config_out_of_sale_stock_status_id');
					$stock_status_id = 9;
					$stock = 0;	
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', price = '" . (float)$price . "', stock_status_id = '" . (int)$stock_status_id . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				} else {
					//$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', price = '" . (float)$price . "', mpn = '" . $this->db->escape($mpn) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', price = '" . (float)$price . "', sup_price = '" . (float)$sup_price . "', actual_price = '" . (int)$actual_price . "', mpn = '" . $this->db->escape($mpn) . "', price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				}
			} else {
				$this->log->write("!!! ТОВАР НЕ НАЙДЕН: " . $product->product_uid);
			}
		}
	}
	
	private function importStockPriceParam($xml) {
		
		$ttl = $this->config->get('module_impex_stat_ttl');
		
		$date = new DateTime();
		$date->setTimestamp(Time() - 60 * 60 * 24 * $ttl);
		$date_added = $date->format(DateTime::ATOM);
		
$this->log->write("date_added " . $date_added);
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "catalog_stats WHERE date_added < '" . $date_added . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "catalog_count WHERE date_added < '" . $date_added . "'");
		
		$this->log->write("---------------------------");
		$this->log->write("SET_STOCK = " . $this->SET_STOCK);
		$this->log->write("SET_PRICE = " . $this->SET_PRICE);
		$this->log->write("---------------------------");
		
		$num = 0;
		foreach ($xml as $product){
			
			$num++;
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			if ($product_id) {
				
				if ($this->config->get('module_impex_stats')) {
				
					$author = $this->SET_USER;
					$delete_now = 0;
					if ($product->delete) $delete_now = 1; 
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_stats SET delete_now = '" . (int)$delete_now . "', file = '" . $this->db->escape($this->SET_FILE) . "', author = '" . $this->db->escape($this->SET_USER) . "', product = '" . $this->db->escape($product->product_name) . "', date_added = NOW()");
					
				}
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET vendor_code = '" . $this->db->escape($product->vendor_code) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				if ($this->SET_STOCK) {
				
					$stock = $product->stock == 0 ? 0 : $product->stock;
					
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$stock . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					
					$this->log->write($num . " - product_id = " . $product_id . " :: установлено количество = " . $stock);
				}
				
				if ($this->SET_PRICE) {
					
					$price = $product->price == 0 ? 0 : $product->price;
					$sup_price = $product->sup_price == 0 ? 0 : $product->sup_price;
					$special = $product->special == 0 ? 0 : $product->special; 
					$mpn = '';
					$actual_price = isset($product->actual_price) ? $product->actual_price : 1;

					if ($product->specprice == 'Специальная цена') {
						$mpn = 'Специальная цена';
						//$special = 0;
					}

					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

					if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {

						if ($product->action == 'Акция') {
							$mpn = 'Акция';
						}

	//					$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
					}
					
					if ($product->out_of_sale) {
						$stock_status_id = 9;	
					} else {
						$stock_status_id = '7';	
					}

					$this->db->query("UPDATE " . DB_PREFIX . "product SET stock_status_id = '" . (int)$stock_status_id . "', price = '" . (float)$price . "', sup_price = '" . (float)$sup_price . "', actual_price = '" . (int)$actual_price . "', mpn = '" . $this->db->escape($mpn) . "', price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					
					$this->log->write($num . " - product_id = " . $product_id . " :: установлен складской статус = " . $stock_status_id);
					$this->log->write($num . " - product_id = " . $product_id . " :: установлена цена = " . $price);
					$this->log->write($num . " - product_id = " . $product_id . " :: установлена закупочная цена = " . $sup_price);
					
					
				}
				
			} else {
				$this->log->write($num . " - !!! ТОВАР НЕ НАЙДЕН: " . $product->product_name);
			}
		}
	}
	
	private function importGift($xml) {
		
		foreach ($xml as $product){
			
			$this->log->write("Опция : " . $product->category_uid . " - " . $product->category_name);
			
			$option_id = $this->getObjectIdByUid($product->category_uid, 'option');
			
			if ($option_id) {
				
				$this->log->write("Опция существует : " . $option_id . " - " . $product->category_name);
				
				$this->db->query("UPDATE " . DB_PREFIX . "option SET type = 'radio', sort_order = '0' WHERE option_id = '" . (int)$option_id . "'");	
				
				$this->db->query("UPDATE " . DB_PREFIX . "option_description SET option_id = '" . $option_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->category_name) . "' WHERE option_id = '" . (int)$option_id . "'");	
				
			} else {
				
				$this->log->write("Новая опция : " . $product->category_name);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = 'radio', sort_order = '0'");
				$option_id = $this->db->getLastId();
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . $option_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->category_name) . "'");
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$option_id . "', `object_uid` = '" . $this->db->escape($product->category_uid) . "', `object` = 'option'");
				
			}
			
			$option_value_id = $this->getObjectIdByUid($product->product_uid, 'option_value');
			
			if ($option_value_id) {
				
$this->log->write('Значение опции существует : ' . $product->product_name . ' : ' . $option_value_id);
				
				$this->db->query("UPDATE " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape($product->image) . "', sort_order = '0' WHERE option_value_id = '" . (int)$option_value_id . "'");	
				
				$this->db->query("UPDATE " . DB_PREFIX . "option_value_description SET option_id = '" . (int)$option_id . "', option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->product_name) . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
				
			} else {
				
$this->log->write('Картинка опции : ' . $product->image . ' : ' . $option_value_id);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "option_value` SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape($product->image) . "', sort_order = '0'");
				$option_value_id = $this->db->getLastId();
$this->log->write('Новое значение опции : ' . $product->product_name . ' создано: ' . $option_value_id);
				$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_id = '" . (int)$option_id . "', option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$this->config->get('config_language_id') . "', name = '" . $this->db->escape($product->product_name) . "'");
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$option_value_id . "', `object_uid` = '" . $this->db->escape($product->product_uid) . "', `object` = 'option_value'");
				
			}
			
			$this->db->query("UPDATE " . DB_PREFIX . "gift_product SET gift_quantity = '" . (int)$product->stock . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
			
			$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = '" . (int)$product->stock . "' WHERE option_value_id = '" . (int)$option_value_id . "'");	
			
			
		}
	}
	
	private function importContent($xml) {
		
		if ($xml->content->product) {
			$this->log('Импорт товара: ' . $xml->content->product->product_uid, 2);
			
			$product = $xml->content->product;
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			if ($product_id) {
				
				//$this->db->query("UPDATE " . DB_PREFIX . "product SET vendor_code = '" . $this->db->escape($product->vendor_code) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				$this->log->write("Товар существует: " . $product_id);
				
				$old_data = $this->getProduct($product_id);
				$old_descriptions = $this->getProductDescriptions($product_id);
				$old_specials = $this->getProductSpecials($product_id);
				$old_related = $this->getProductRelated($product_id);
				$old_filters = $this->getProductFilters($product_id);
				$old_stores = $this->getProductStores($product_id);
				$old_layouts = $this->getProductLayouts($product_id);
				$old_seourls = $this->getProductSeoUrls($product_id);
				$old_categories = $this->getProductCategories($product_id);
				$old_attributes = $this->getProductAttributes($product_id);
				
				$stores			= $this->getStores();	
				
				$new_data = $old_data;
				$new_data['product_special'] = $old_specials;
				$new_data['product_related'] = $old_related;
				$new_data['product_filter'] = $old_filters;
				//$new_data['product_store'] = $old_stores;				
				$new_data['product_store'] = $stores;
				$new_data['product_layout'] = $old_layouts;
				$new_data['product_seo_url'] = $old_seourls;
				$new_data['product_category'] = $old_categories;
				$new_data['product_attribute'] = $old_attributes;
				
				if ($product->images->image) {
					
					$img_count = count($product->images->image);
					if ($img_count > 0) {
						$new_data['image'] = $product->images->image[0];
					} else {
						$new_data['image'] = '';
					}
					if ($img_count > 1) {
						$product_images = array();
						for ($i = 1; $i < $img_count; $i++) {
							$product_images[] = array(
								 'image'         			=> $product->images->image[$i]
								,'sort_order'            		=> $i
							);
						}
						$new_data['product_image'] = $product_images;	
					}
				
				}
				
				$new_description = '';
						
				if ($product->files->file) {
					
					$files_count = count($product->files->file);
					if ($files_count > 0) {
						if ($product->files->file[0]->type == "description") {
							$description_file = $product->files->file[0]->address;
							$this->log("~ФАЙЛ ОПИСАНИЯ",2);
							$this->log(DIR_IMAGE . $description_file[0]);
	
							$new_description = file_get_contents(DIR_IMAGE . $description_file[0]);
							
						}
					}
				}
				
				$new_descriptions = $old_descriptions;
				//if ($new_description != '') {
					$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				//}
				$new_data['product_description'] = $new_descriptions;
				
				
$this->log->write('Обновляем товар : ' . $product_id);
				
$this->log->write('Данные товара : ' . print_r($new_data, 1));
				
				$this->editProduct($product_id, $new_data);
				
			} else {
				
$this->log->write("Новый товар: " . $product->product_uid);
				
			}
			
		}
		
		if ($xml->content->category) {
			$this->log('Импорт категории: ' . $xml->content->category->category_uid, 2);
			
			$category = $xml->content->category;
			
			$category_id = $this->getObjectIdByUid($category->category_uid, 'category');

			if ($category_id) {
				
				$this->log->write("Категория существует: " . $category->category_uid);
				
				$old_data = $this->getCategory($category_id);
				
				$old_descriptions = $this->getCategoryDescriptions($category_id);
				$old_pathes = $this->getCategoryPath($category_id);
				$old_filters = $this->getCategoryFilters($category_id);
				$old_stores = $this->getCategoryStores($category_id);
				$old_layouts = $this->getCategoryLayouts($category_id);
				$old_seourls = $this->getCategorySeoUrls($category_id);
				
				$new_data = $old_data;
				
				if ($category->image) {
					$new_image = (string)$category->image;
				} else {
					$new_image = '';
				}
				
				$new_description = '';
					
				if ($category->description) {
					$description_file = (string)$category->description;
					$new_description = file_get_contents(DIR_IMAGE . $description_file);
				}
		
				$new_descriptions = $old_descriptions;
				//$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				//if ($new_description != '') {
					$new_descriptions[$this->LANG_ID]['description'] = $new_description;
				//}
				
				$stores			= $this->getStores();
				
				$new_data['category_description'] = $new_descriptions;
				$new_data['image'] = $new_image;
				$new_data['category_filter'] = $old_filters;
				$new_data['category_store'] = $stores;
				$new_data['category_layout'] = $old_layouts;
				$new_data['category_seo_url'] = $old_seourls;
				
				$this->editCategory($category_id, $new_data);
		
			} else {
				
				$this->log->write("Новая категория: " . $category->category_uid);
				
			}
				
		}
		
		if ($xml->content->manufacturer) {
			$this->log('Импорт производителя: ' . $xml->content->manufacturer->manufacturer_uid, 2);
			
			$manufacturer = $xml->content->manufacturer;
			
			$manufacturer_id = $this->getObjectIdByUid($manufacturer->manufacturer_uid, 'manufacturer');

			if ($manufacturer_id) {
				
				$this->log->write("Есть такой производитель: " . $manufacturer->manufacturer_uid);
				
				$old_data = $this->getManufacturer($manufacturer_id);
				$old_stores = $this->getManufacturerStores($manufacturer_id);
				$old_seourls = $this->getManufacturerSeoUrls($manufacturer_id);
				
				$new_data = $old_data;
				
				if ($manufacturer->image) {
					$new_image = (string)$manufacturer->image;
				} else {
					$new_image = '';
				}
				
				$stores			= $this->getStores();	
				
				$new_data['image'] = $new_image;
				$new_data['manufacturer_store'] = $stores;
				$new_data['manufacturer_seo_url'] = $old_seourls;
				
				$this->editManufacturer($manufacturer_id, $new_data);
				
			} else {
				
				$this->log->write("Новый производитель: " . $manufacturer->manufacturer_uid);
				
			}

		}
		
	}
	
	private function importProducts($xml, $language_id) {
		
		$ttl = $this->config->get('module_impex_stat_ttl');
		
		$date = new DateTime();
		$date->setTimestamp(Time() - 60 * 60 * 24 * $ttl);
		$date_added = $date->format(DateTime::ATOM);
		
$this->log->write("date_added " . $date_added);
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "catalog_stats WHERE date_added < '" . $date_added . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "catalog_count WHERE date_added < '" . $date_added . "'");
		
		foreach ($xml as $product){
			
			$product_id = $this->getObjectIdByUid($product->product_uid, 'product');
			
			$price = $product->price == 0 ? 0 : $product->price;
			$sup_price = $product->sup_price == 0 ? 0 : $product->sup_price;
			$special = $product->special == 0 ? 0 : $product->special; 
			$mpn = '';
			$actual_price = isset($product->actual_price) ? $product->actual_price : 1;

			if ($product->specprice == 'Специальная цена') {
				$mpn = 'Специальная цена';
				//$special = 0;
			}

			if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
				if ($product->action == 'Акция') {
					$mpn = 'Акция';
				}
			}

			if ($product_id) {
				
				$this->log->write("Товар существует: " . $product->product_name);
				
				if ($this->config->get('module_impex_stats')) {
				
					$author = $this->SET_USER;
					$delete_now = 0;
					if ($product->delete) $delete_now = 1; 
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_stats SET delete_now = '" . (int)$delete_now . "', file = '" . $this->db->escape($this->SET_FILE) . "', author = '" . $this->db->escape($this->SET_USER) . "', product = '" . $this->db->escape($product->product_name) . "', date_added = NOW()");
					
				}
				
				if ($product->delete) {
					$this->log->write("УДАЛИТЬ Товар : " . $product_id . " - " . $product->product_name);
					$this->log->write("---------------------------");
					
					$this->deleteProduct($product_id);
					$this->db->query("DELETE FROM " . DB_PREFIX . "to_impex WHERE object_id = '" . (int)$product_id . "' AND object = 'product'");	
					
					continue;
				}
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET vendor_code = '" . $this->db->escape($product->vendor_code) . "', sup_price = '" . (float)$sup_price . "', actual_price = '" . (int)$actual_price . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				//$this->db->query("UPDATE " . DB_PREFIX . "product SET weight = '" . (float)$product->weight . "', width = '" . (float)$product->width . "', height = '" . (float)$product->height . "', length = '" . (float)$product->length . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				if ($this->SET_MODE == 'update') {
					
					$this->log->write(" Обновляем все данные товара ");
					
					$old_data = $this->getProduct($product_id);
					$old_descriptions = $this->getProductDescriptions($product_id);
					$old_images = $this->getProductImages($product_id);
					$old_specials = $this->getProductSpecials($product_id);
					$old_related = $this->getProductRelated($product_id);
					$old_filters = $this->getProductFilters($product_id);
					$old_stores = $this->getProductStores($product_id);
					$old_layouts = $this->getProductLayouts($product_id);
					$old_seourls = $this->getProductSeoUrls($product_id);
					$old_tags = $this->getProductTags($product_id);
					
					$new_data = $old_data;
					
					$stores			= $this->getStores();	
					
					$new_data['product_special'] = $old_specials;
					$new_data['product_related'] = $old_related;
					$new_data['product_filter'] = $old_filters;
					$new_data['product_store'] = $stores;
					$new_data['product_layout'] = $old_layouts;
					$new_data['product_seo_url'] = $old_seourls;
					$new_data['product_tagn'] = $old_tags;
					
					$new_data['manufacturer_id'] = $this->getObjectIdByUid($product->manufacturer_uid, 'manufacturer');
					
					$new_data['sku'] = $product->product_sku;
					$new_data['model'] = $product->product_model;
					$new_data['mpn'] = $mpn;
					
					$new_data['weight'] = str_replace(',', '.', $product->weight);
					$new_data['width'] = str_replace(',', '.', $product->width);
					$new_data['height'] = str_replace(',', '.', $product->height);
					$new_data['length'] = str_replace(',', '.', $product->length);
					
					if ($product->out_of_sale) {
						//$new_data['stock_status_id'] = $this->config->get('config_out_of_sale_stock_status_id');	
						$new_data['stock_status_id'] = 9;	
					} else {
						$new_data['stock_status_id'] = '7';	
					}
					
					$product_attributes = array();	
					foreach ($product->attributes->attribute as $attribute) {
						
						$attribute_id = $this->getObjectIdByUid($attribute->attribute_uid, 'attribute');
						
						if ($attribute_id) {
			
							$this->log->write("Атрибут существует: " . $attribute->attribute_name);
							
							if ($this->SET_MODE == 'update') {
								
								$this->log->write(" Обновляем атрибут ");
								
								$old_attr_data = $this->getAttribute($attribute_id);
								$old_attr_descriptions = $this->getAttributeDescriptions($attribute_id);
								
								$new_attr_data = $old_attr_data;
								$new_attr_descriptions[$this->LANG_ID]['name'] =	(string)$attribute->attribute_name;
								$new_attr_data['attribute_description'] = $new_attr_descriptions;
								
								$this->editAttribute($attribute_id, $new_attr_data);
								
							} else {
								$this->log->write(" Ничего не делаем ");
							}
							
						} else {
							
							$this->log->write("Новый атрибут: " . $attribute->attribute_name);
							
							$attr_data = $this->getAttributeData($attribute);
							
							$attribute_id = $this->addAttribute($attr_data);
							
							$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$attribute_id . "', `object_uid` = '" . $this->db->escape($attribute->attribute_uid) . "', `object` = 'attribute'");
							
						}
						
						if (trim($attribute->attribute_value) != '' && trim($attribute->attribute_value) != '<>') {
							$product_attributes[] = array(
								 'attribute_id'	=> $this->getObjectIdByUid($attribute->attribute_uid, 'attribute')
								,'product_attribute_description'	=> array(
									$this->LANG_ID => array(
										 'text'	=> $attribute->attribute_value
									)
								)
							);
						}
					}
					$new_data['product_attribute'] = $product_attributes;
					
					
	
					$spec_categories = $this->config->get('module_impex_categories');
					
$this->log->write("Спецкатегории = " . print_r($spec_categories, true));
				
					
					$category_id = $this->getObjectIdByUid($product->category_uid, 'category');
					
$this->log->write("Категория = " . $category_id);
				
					
					$product_categories = array();
					
					if (is_array($spec_categories) && in_array($category_id, $spec_categories)) {
						
						$product_categories[] = $category_id;
						
					} else {
					
						$product_categories[] = $category_id;
						$parent_id = $this->getParentCategory($category_id);
						while ($parent_id != '0') {
							$product_categories[] = $parent_id;
							$parent_id = $this->getParentCategory($parent_id);
						}
						
					}
					$new_data['product_category'] = $product_categories;
					
					if ($this->SET_IMAGE) {
						
						if ($product->images->image) {
						
							$img_count = count($product->images->image);
							if ($img_count > 0) {
								$new_data['image'] = $product->images->image[0];
							} else {
								$new_data['image'] = '';
							}
							if ($img_count > 1) {
								$product_images = array();
								for ($i = 1; $i < $img_count; $i++) {
									$product_images[] = array(
										 'image'         			=> $product->images->image[$i]
										,'sort_order'            		=> $i
									);
								}
								$new_data['product_image'] = $product_images;	
							}
						}
					} else {
						$new_data['product_image'] = $old_images;	
					}

					if ($this->SET_PRICE) $new_data['price'] = $product->price;
					
					if ($this->SET_STOCK) $new_data['quantity'] = $product->quantity == 0 ? 0 : $product->quantity;
					
					if ($product->out_of_sale) {
						$new_data['quantity'] = 0;	
					}
					
					$new_description = '';
					
					if ($this->SET_FILES) {
						
						if ($product->files->file) {
						
							$files_count = count($product->files->file);
							if ($files_count > 0) {
								if ($product->files->file[0]->type == "description") {
									$description_file = $product->files->file[0]->address;
									$this->log("~ФАЙЛ ОПИСАНИЯ",2);
									$this->log(DIR_IMAGE . $description_file[0]);
	
									$new_description = file_get_contents(DIR_IMAGE . $description_file[0]);
									
								}
							}
						}
						
						
						
						
					}
					
					$new_descriptions = $old_descriptions;
					
					if ($new_description != '') {
					
						$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					
					}
					
					$new_descriptions[$this->LANG_ID]['name'] =	$product->product_name;
					if ($old_descriptions[$this->LANG_ID]['meta_title'] == $old_descriptions[$this->LANG_ID]['name']) {
						$new_descriptions[$this->LANG_ID]['meta_title'] =	$product->product_name;
					} else {
						if ($old_descriptions[$this->LANG_ID]['meta_title'] == '') {
							$new_descriptions[$this->LANG_ID]['meta_title'] =	$product->product_name;
						}
					}
					$new_data['product_description'] = $new_descriptions;
					
					$this->editProduct($product_id, $new_data);
					
$this->db->query("UPDATE " . DB_PREFIX . "product SET price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
					
					if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
						
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
					}
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				if ($product->delete) {
					$this->log->write("НОВЫЙ Товар : с маркером delete - " . $product->product_name);
					$this->log->write("Ничего не делаем");
					$this->log->write("---------------------------");
				
					if ($this->config->get('module_impex_stats')) {
					
						$author = $this->SET_USER;
						$delete_now = 0;
						if ($product->delete) $delete_now = 1; 
						
						$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_stats SET delete_now = '" . (int)$delete_now . "', file = '" . $this->db->escape($this->SET_FILE) . "', author = '" . $this->db->escape($this->SET_USER) . "', product = '" . $this->db->escape($product->product_name) . "', date_added = NOW()");
						
					}
					continue;
				}
				
				$this->log->write("Новый Товар: " . $product->product_name);
				
				$data = $this->getProductData($product);
				
				if ($product->out_of_sale) {
					//$new_data['stock_status_id'] = $this->config->get('config_out_of_sale_stock_status_id');	
					$data['stock_status_id'] = 9;	
				}
					
				$product_id = $this->addProduct($data);
				
				if ($this->config->get('module_impex_stats')) {
				
					$author = $this->SET_USER;
					$delete_now = 0;
					if ($product->delete) $delete_now = 1; 
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_stats SET delete_now = '" . (int)$delete_now . "', file = '" . $this->db->escape($this->SET_FILE) . "', author = '" . $this->db->escape($this->SET_USER) . "', product = '" . $this->db->escape($product->product_name) . "', date_added = NOW()");
					
				}
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET vendor_code = '" . $this->db->escape($product->vendor_code) . "', sup_price = '" . (float)$sup_price . "', actual_price = '" . (int)$actual_price . "',  price_type = '" . $this->db->escape($mpn) . "', supplier_main = '" . $this->db->escape($product->supplier_main) . "', supplier_spec = '" . $this->db->escape($product->supplier_spec) . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
				
				if ((float)$special > 0 && (float)$price > 0 && (float)$special < (float)$price) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '" . $this->db->escape($product->special_from) . "', date_end = '" . $this->db->escape($product->special_to) . "'");
				}
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$product_id . "', `object_uid` = '" . $this->db->escape($product->product_uid) . "', `object` = 'product'");
				
			}
		}
	}
	
	private function importCategories($xml, $parent = 0, $language_id) {
		
		foreach ($xml as $category){
			
			$category_id = $this->getObjectIdByUid($category->category_uid, 'category');
			
			if ($category_id) {
				
				$this->log->write("Категория существует: " . $category->category_name);
				
				if ($this->SET_MODE == 'update') {
					
$this->log->write(" Обновляем все данные категории ");
					
					$old_data = $this->getCategory($category_id);
					
$this->log('Старые данные категории',2);
$this->log($old_data,2);
					
					$old_descriptions = $this->getCategoryDescriptions($category_id);
					$old_pathes = $this->getCategoryPath($category_id);
					$old_filters = $this->getCategoryFilters($category_id);
					$old_stores = $this->getCategoryStores($category_id);
					$old_layouts = $this->getCategoryLayouts($category_id);
					$old_seourls = $this->getCategorySeoUrls($category_id);
					
					$new_data = $old_data;
					if ($category->level > 0) {
						$parent_id = $this->getObjectIdByUid($category->parent_uid, 'category');
					} else {
						$parent_id = 0;
					}
					$new_data['top'] = $category->level == 0 ? 1 : 0;
					$new_data['parent_id'] = $parent_id;
					
					if ($this->SET_IMAGE) {
						if ($category->image) {
							$new_image = (string)$category->image;
						} else {
							$new_image = '';
						}
					} else {
						$new_image = '';	
					}
					$new_data['image'] = $new_image;
					
					$new_description = '';
					
					if ($this->SET_FILES) {
						if ($category->description) {
							$description_file = (string)$category->description;
							
//							$this->log("~ФАЙЛ ОПИСАНИЯ",2);
//							$this->log(DIR_IMAGE . $description_file);

							$new_description = file_get_contents(DIR_IMAGE . $description_file);
								
//						} else {
//							$new_description = '';
						}
//					} else {
//						$new_description = '';	
					}
					
					$new_descriptions = $old_descriptions;
					
//					$this->log($this->LANG_ID,2);
//					$this->log($old_descriptions,2);
					
					//$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					//if ($new_description != '') {
						$new_descriptions[$this->LANG_ID]['description'] = $new_description;
					//}
					$new_descriptions[$this->LANG_ID]['name'] =	(string)$category->category_name;
					if ($old_descriptions[$this->LANG_ID]['meta_title'] == $old_descriptions[$this->LANG_ID]['name']) {
						$new_descriptions[$this->LANG_ID]['meta_title'] =	(string)$category->category_name;
					} else {
						if ($old_descriptions[$this->LANG_ID]['meta_title'] == '') {
							$new_descriptions[$this->LANG_ID]['meta_title'] =	(string)$category->category_name;
						}
					}
					$new_data['category_description'] = $new_descriptions;
					$new_data['category_filter'] = $old_filters;
					
					$stores			= $this->getStores();	
					
					$new_data['category_store'] = $stores;
					$new_data['category_layout'] = $old_layouts;
					$new_data['category_seo_url'] = $old_seourls;
					
$this->log('Новые данные категории',2);
$this->log($new_data,2);
					
					$this->editCategory($category_id, $new_data);
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				$this->log->write("Новая категория: " . $category->category_name);
				
				$data = $this->getCategoryData($category);

				$category_id = $this->addCategory($data);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$category_id . "', `object_uid` = '" . $this->db->escape($category->category_uid) . "', `object` = 'category'");
				
			}
		}
	}
	
	private function importAttributes($xml, $language_id) {
		
		foreach ($xml as $attribute){
			
			$attribute_id = $this->getObjectIdByUid($attribute->attribute_uid, 'attribute');
			
			if ($attribute_id) {

				$this->log->write("Атрибут существует: " . $attribute->attribute_name);
				
				if ($this->SET_MODE == 'update') {
					
					$this->log->write(" Обновляем атрибут ");
					
					$old_data = $this->getAttribute($attribute_id);
					$old_descriptions = $this->getAttributeDescriptions($attribute_id);
					
					$new_data = $old_data;
					$new_descriptions[$this->LANG_ID]['name'] =	(string)$attribute->attribute_name;
					$new_data['attribute_description'] = $new_descriptions;
					
					$this->editAttribute($attribute_id, $new_data);
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				$this->log->write("Новый атрибут: " . $attribute->attribute_name);
				
				$data = $this->getAttributeData($attribute);
				
				$attribute_id = $this->addAttribute($data);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$attribute_id . "', `object_uid` = '" . $this->db->escape($attribute->attribute_uid) . "', `object` = 'attribute'");
				
			}
		}
	}
	
	private function importManufacturers($xml, $parent = 0, $language_id) {
		
		foreach ($xml as $manufacturer){
			
			$manufacturer_id = $this->getObjectIdByUid($manufacturer->manufacturer_uid, 'manufacturer');
			
			if ($manufacturer_id) {
				
				$this->log->write("Есть такой производитель: " . $manufacturer->manufacturer_name);
				
				if ($this->SET_MODE == 'update') {
					
					$this->log->write(" Обновляем все данные производителя ");
					
					$old_data = $this->getManufacturer($manufacturer_id);
					$old_stores = $this->getManufacturerStores($manufacturer_id);
					$old_seourls = $this->getManufacturerSeoUrls($manufacturer_id);
					
					$new_data = $old_data;
					
					if ($this->SET_IMAGE) {
						if ($manufacturer->image) {
							$new_image = (string)$manufacturer->image;
						} else {
							$new_image = '';
						}
					} else {
						$new_image = '';	
					}
					
					$new_data['image'] = $new_image;
					$new_data['name'] = $manufacturer->manufacturer_name;
					
					$stores			= $this->getStores();	
					
					$new_data['manufacturer_store'] = $stores;
					$new_data['manufacturer_seo_url'] = $old_seourls;
					
					$this->editManufacturer($manufacturer_id, $new_data);
					
				} else {
					$this->log->write(" Ничего не делаем ");
				}
				
			} else {
				
				$this->log->write("Новый производитель: " . $manufacturer->manufacturer_name);
				
				$data = $this->getManufacturerData($manufacturer);
				
				$manufacturer_id = $this->addManufacturer($data);
				
				$this->db->query("INSERT INTO `" . DB_PREFIX . "to_impex` SET object_id = '" . (int)$manufacturer_id . "', `object_uid` = '" . $this->db->escape($manufacturer->manufacturer_uid) . "', `object` = 'manufacturer'");
				
			}
		}
	}

	private function getObjectIdByUid($object_uid, $object) {
		$query = $this->db->query("SELECT object_id FROM " . DB_PREFIX . "to_impex WHERE object_uid = '" . $object_uid . "' AND object = '" . $object . "'");

		if ($query->num_rows) {
			return $query->row['object_id'];
		}
		else {
			return false;
		}
	}

	private function getObjectKeyByUid($object_uid, $object) {
		$query = $this->db->query("SELECT object_key FROM " . DB_PREFIX . "to_impex WHERE object_uid = '" . $object_uid . "' AND object = '" . $object . "'");

		if ($query->num_rows) {
			return $query->row['object_key'];
		}
		else {
			return false;
		}
	}
	
	private function getObjectUidById($id, $object) {
		
		if ($object == 'total') {
			
			$query = $this->db->query("SELECT object_uid FROM `" . DB_PREFIX . "to_impex` WHERE object_key = '" . $this->db->escape($id) . "' AND object = '" . $this->db->escape($object) . "'");
	
			if ($query->num_rows) {
				return $query->row['object_uid'];
			}
			else {
				return false;
			}
	
		} else {
			
			$query = $this->db->query("SELECT object_uid FROM `" . DB_PREFIX . "to_impex` WHERE `object_id` = '" . (int)$id . "' AND object = '" . $this->db->escape($object) . "'");
	
			if ($query->num_rows) {
				return $query->row['object_uid'];
			} else {
				return false;
			}
	
		}
  }
	
	private function getParentCategory($category_id) {
		$query = $this->db->query("SELECT parent_id FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");

		if ($query->num_rows) {
			return $query->row['parent_id'];
		} else {
			return false;
		}
	}
	
	private function getProductData($product) {

		$category_id = $this->getObjectIdByUid($product->category_uid, 'category');
		$manufacturer_id = $this->getObjectIdByUid($product->manufacturer_uid, 'manufacturer');
		
		$quantity = $product->quantity; 
		$price = $product->price; 
		$special = $product->special; 
		
		if ($quantity) {
			$quantity = (float)$quantity; 
		} else {
			$quantity = 0; 
		}
//		if ($quantity == 0) {
//			$quantity = 555; 
//		}
		if ($product->out_of_sale) {
			$this->log->write("Товар снят с производства");
			$quantity = 0;	
		}
		$this->log->write("quantity = " . $quantity);
		
		if ($price) {
			$price = (float)$price; 
		} else {
			$price = 0; 
		}
		$this->log->write("price = " . $price);
		
		if ($special) {
			$special = (float)$special; 
		} else {
			$special = 0; 
		}
		$this->log->write("price = " . $special);
		
		$mpn = '';

		if ($product->specprice == 'Специальная цена') {
			$mpn = 'Специальная цена';
			//$special = 0;
		}

		if ($special > 0 && $price > 0 && $special < $price) {
			if ($product->action == 'Акция') {
				$mpn = 'Акция';
			}
		}
		
		$data = array(
			 'model'         		=> $product->product_model
			,'sku'            		=> $product->product_sku
			,'upc'            		=> ''
			,'ean'            		=> ''
			,'jan'            		=> ''
			,'isbn'            		=> ''
			,'mpn'            		=> $mpn
			,'location'           	=> ''
			,'quantity'      		=> $quantity
			,'minimum' 				=>  1
			,'subtract'        		=> 0
			,'stock_status_id'		=> 7
			,'date_available'     	=> ''
			,'manufacturer_id'		=> $manufacturer_id
			,'shipping'         	=> 1
			,'price'         		=> $price
			,'points'         		=> 0
			,'weight'         		=> 0
			,'weight_class_id'		=> 1
			,'length'         		=> 0
			,'width'         		=> 0
			,'height'         		=> 0
			,'length_class_id'		=> 1
			,'status'         		=> 1
			,'tax_class_id'			=> 0
			,'sort_order'         	=> 0
			
			,'delivery_option'    	=> ''
			,'sales_note'         	=> ''
			,'yam_status'         	=> 0
		);
		
		if ($product->images->image) {
		
			$img_count = count($product->images->image);
			if ($img_count > 0) {
				$data['image'] = $product->images->image[0];
			}
			
			if ($img_count > 1) {
				$data['product_image'] = array();
				for ($i = 1; $i < $img_count; $i++) {
					$data['product_image'][] = array(
						 'image'         			=> $product->images->image[$i]
						,'sort_order'            		=> $i
					);
				}
			}
	
		}
	
		$data['product_category'] = array();
			
		$data['product_category'][] = $category_id;
		
		$parent_id = $this->getParentCategory($category_id);
		while ($parent_id != '0') {
			$data['product_category'][] = $parent_id;
			$parent_id = $this->getParentCategory($parent_id);
		}
					
		$description = '';
					
		if ($this->SET_FILES) {
			
			if ($product->files->file) {
			
				$files_count = count($product->files->file);
				if ($files_count > 0) {
					if ($product->files->file[0]->type == "description") {
						$description_file = $product->files->file[0]->address;
						$description = file_get_contents(DIR_IMAGE . $description_file[0]);
					}
//				} else {
//					$description = '';
				}
			
			}
			
//		} else {
//			$description = '';	
		}
		
		$data['product_description'] = array(
			$this->LANG_ID => array(
				 'name'             	=> $product->product_name
				 ,'description'				=> $description
				 ,'tag'             	=> ''
				 ,'meta_title'				=> $product->product_name
				 ,'meta_description'	=> ''
				 ,'meta_keyword'			=> ''
			)
		);
	
		$data['product_store'] = array(
			$this->STORE_ID
		);
		
		$data['product_layout'] = array(
			$this->STORE_ID 	=>  0
		);
		
		$data['product_attribute'] = array();	
		
		foreach ($product->attributes->attribute as $attribute) {
			$data['product_attribute'][] = array(
				 'attribute_id'						=> $this->getObjectIdByUid($attribute->attribute_uid, 'attribute')
				,'product_attribute_description'	=> array(
					$this->LANG_ID => array(
						 'text'						=> $attribute->attribute_value
					)
				)
			);
		}
		
		return $data;
	}
	
	private function getCategoryData($category) {
	
		if ($category->level > 0) {
			$parent_id = $this->getObjectIdByUid($category->parent_uid, 'category');
		} else {
			$parent_id = 0;
		}
		
		if ($this->SET_IMAGE) {
			if ($category->image) {
				$image = (string)$category->image;
			} else {
				$image = '';
			}
		} else {
			$image = '';	
		}

		$data = array(
			 'status'         => 1
			,'top'            => $category->level == 0 ? 1 : 0
			,'parent_id'      => $parent_id
			,'category_store' =>  array($this->STORE_ID)
			,'keyword'        => ''
			,'image'          => $image
			,'sort_order'     => 0
			,'column'         => 1
		);
					
		$description = '';
					
		if ($this->SET_FILES) {
			if ($category->description) {
				$description_file = (string)$category->description;
				$description = file_get_contents(DIR_IMAGE . $description_file);
//			} else {
//				$description = '';
			}
//		} else {
//			$description = '';	
		}

		$data['category_description'] = array(
			$this->LANG_ID 				=> array(
				 'name'             => (string)$category->category_name
				,'meta_keyword'     => ''
				,'meta_description'	=> ''
				,'description'		  => $description
				,'meta_title'				=> (string)$category->category_name
				,'meta_h1'					=> ''
				,'tagline'					=> ''
				,'color'						=> ''
			),
		);
	
		return $data;
	}
	
	private function getManufacturerData($manufacturer) {
		
		if ($this->SET_IMAGE) {
			if ($manufacturer->image) {
				$image = (string)$manufacturer->image;
			} else {
				$image = '';
			}
		} else {
			$image = '';	
		}

		$data = array(
			 'status'         		=> 1
			,'name'					=> $manufacturer->manufacturer_name
			,'image'          		=> $image
			,'sort_order'     		=> 0
		);
		
		$data['manufacturer_store'] = array(
			$this->STORE_ID
		);
		
		$data['manufacturer_seo_url'] = array(
			$this->LANG_ID => array(
				 'keyword'					=> ''
			),
		);
		
		return $data;
	}
	
	private function getAttributeData($attribute) {
		
		$attribute_group_id = 1;
		
		$data = array(
			'attribute_group_id'	=> $attribute_group_id
			,'sort_order'			=> 0
		);

		$data['attribute_description'] = array(
			$this->LANG_ID 	=> array(
			 'name'					=> $attribute->attribute_name
			)
		);
		
		return $data;
	}
	
	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row;
	}
	
	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description']
			);
		}

		return $category_description_data;
	}
	
	public function getCategoryPath($category_id) {
		$query = $this->db->query("SELECT category_id, path_id, level FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int)$category_id . "'");

		return $query->rows;
	}
	
	public function getCategoryFilters($category_id) {
		$category_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_filter_data[] = $result['filter_id'];
		}

		return $category_filter_data;
	}

	public function getCategoryStores($category_id) {
		$category_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_store_data[] = $result['store_id'];
		}

		return $category_store_data;
	}
	
	public function getCategorySeoUrls($category_id) {
		$category_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'category_id=" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $category_seo_url_data;
	}
	
	public function getCategoryLayouts($category_id) {
		$category_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $category_layout_data;
	}

	public function editCategory($category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

		if ($query->rows) {
			foreach ($query->rows as $category_path) {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

				$path = array();

				// Get the nodes new parents
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Get whats left of the nodes current path
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Combine the paths with a new level
				$level = 0;

				foreach ($path as $path_id) {
					$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

					$level++;
				}
			}
		} else {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$category_id . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_filter'])) {
			foreach ($data['category_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		// SEO URL
		$this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'category_id=" . (int)$category_id . "'");

		if (isset($data['category_seo_url'])) {
			foreach ($data['category_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "'");

		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('category');
	}

	public function addCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$category_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
		}

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$level = 0;

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

			$level++;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

		if (isset($data['category_filter'])) {
			foreach ($data['category_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['category_store'])) {
			foreach ($data['category_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		if (isset($data['category_seo_url'])) {
			foreach ($data['category_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}
		
		// Set which layout to use with this category
		if (isset($data['category_layout'])) {
			foreach ($data['category_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('category');

		return $category_id;
	}

	public function getAttribute($attribute_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_id = '" . (int)$attribute_id . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getAttributeDescriptions($attribute_id) {
		$attribute_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute_description WHERE attribute_id = '" . (int)$attribute_id . "'");

		foreach ($query->rows as $result) {
			$attribute_data[$result['language_id']] = array('name' => $result['name']);
		}

		return $attribute_data;
	}

	public function addAttribute($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = '" . (int)$data['attribute_group_id'] . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$attribute_id = $this->db->getLastId();

		foreach ($data['attribute_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
		return $attribute_id;
	}

	public function editAttribute($attribute_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "attribute SET attribute_group_id = '" . (int)$data['attribute_group_id'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE attribute_id = '" . (int)$attribute_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "attribute_description WHERE attribute_id = '" . (int)$attribute_id . "'");

		foreach ($data['attribute_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}
	}
	
	public function getProduct($product_id) {
		
		$this->log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
		$this->log->write('getProduct($product_id)' . $product_id);
		$this->log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}
	
	public function getProductDescriptions($product_id) {
		$product_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'tag'              => $result['tag']
			);
		}

		return $product_description_data;
	}

	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}

	public function getProductRelated($product_id) {
		$product_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}

		return $product_related_data;
	}
	
	public function getProductFilters($product_id) {
		$product_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_filter_data[] = $result['filter_id'];
		}

		return $product_filter_data;
	}

	public function getProductStores($product_id) {
		$product_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_store_data[] = $result['store_id'];
		}

		return $product_store_data;
	}

	public function getProductSeoUrls($product_id) {
		$product_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $product_seo_url_data;
	}
	
	public function getProductLayouts($product_id) {
		$product_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $product_layout_data;
	}

	public function getProductTags($product_id) {
		$query = $this->db->query("SELECT *, IF(td.name_short IS NULL or td.name_short = '', td.name, td.name_short) as name,
			(select category_id from " . DB_PREFIX . "tag_to_category where tag_id = t.tag_id) as category_id,
			(select count(*) from " . DB_PREFIX . "product_to_tag where tag_id = pt.tag_id) as kol  
			FROM " . DB_PREFIX . "product_to_tag pt 
			INNER JOIN " . DB_PREFIX . "tag t ON (t.tag_id = pt.tag_id) 
			INNER JOIN " . DB_PREFIX . "tag_description td ON (t.tag_id = td.tag_id) 
			INNER JOIN " . DB_PREFIX . "tag_to_store ts ON (pt.tag_id = ts.tag_id) 
			WHERE td.language_id = '" . (int)$this->config->get('config_language_id') . "' 
				AND ts.store_id = '" . (int)$this->config->get('config_store_id') . "'  
				AND t.status = '1'
				AND pt.product_id = ".(int)$product_id." 
			group by t.tag_id ORDER BY kol desc");

		return $query->rows;
	}

	public function getProductCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}

	public function getProductAttributes($product_id) {
		$product_attribute_data = array();

		$product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();

			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}

			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}

		return $product_attribute_data;
	}

	public function addProduct($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW(), date_modified = NOW()");

		$product_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
		}
		
		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					// Removes duplicates
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");

						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}

		if (isset($data['product_option'])) {
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
				}
			}
		}

		if (isset($data['product_recurring'])) {
			foreach ($data['product_recurring'] as $recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
			}
		}
		
		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
			}
		}

		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			}
		}

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

		if (isset($data['product_filter'])) {
			foreach ($data['product_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
				if ((int)$product_reward['points'] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
				}
			}
		}
		
		// SEO URL
		if (isset($data['product_seo_url'])) {
			foreach ($data['product_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}
		
		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}


		$this->cache->delete('product');

		return $product_id;
	}

	public function editProduct($product_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

		if (!empty($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					// Removes duplicates
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_option'])) {
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
				}
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = " . (int)$product_id);

		if (isset($data['product_recurring'])) {
			foreach ($data['product_recurring'] as $product_recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$product_recurring['customer_group_id'] . ", `recurring_id` = " . (int)$product_recurring['recurring_id']);
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_filter'])) {
			foreach ($data['product_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");

		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $value) {
				if ((int)$value['points'] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
				}
			}
		}
		
		// SEO URL
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
		
		if (isset($data['product_seo_url'])) {
			foreach ($data['product_seo_url']as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		$this->cache->delete('product');
	}

	public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE product_id = '" . (int)$product_id . "'");

		$this->cache->delete('product');
	}

	public function getManufacturer($manufacturer_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row;
	}

	public function getManufacturerStores($manufacturer_id) {
		$manufacturer_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		foreach ($query->rows as $result) {
			$manufacturer_store_data[] = $result['store_id'];
		}

		return $manufacturer_store_data;
	}
	
	public function getManufacturerSeoUrls($manufacturer_id) {
		$manufacturer_seo_url_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		foreach ($query->rows as $result) {
			$manufacturer_seo_url_data[$result['store_id']][$result['language_id']] = $result['keyword'];
		}

		return $manufacturer_seo_url_data;
	}
	
	public function addManufacturer($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$manufacturer_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		}

		if (isset($data['manufacturer_store'])) {
			foreach ($data['manufacturer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
				
		// SEO URL
		if (isset($data['manufacturer_seo_url'])) {
			foreach ($data['manufacturer_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}
		
		$this->cache->delete('manufacturer');

		return $manufacturer_id;
	}

	public function editManufacturer($manufacturer_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		if (isset($data['manufacturer_store'])) {
			foreach ($data['manufacturer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		if (isset($data['manufacturer_seo_url'])) {
			foreach ($data['manufacturer_seo_url'] as $store_id => $language) {
				foreach ($language as $language_id => $keyword) {
					if (!empty($keyword)) {
						$this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($keyword) . "'");
					}
				}
			}
		}

		$this->cache->delete('manufacturer');
	}
	
	public function getStores($data = array()) {
		
		$data['stores'] = array();
		
		$data['stores'][] = 0; // array(		

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY url");
		$stores = $query->rows;

		foreach ($stores as $store) {
			$data['stores'][] = $store['store_id'];
		}
		
		$this->log("~STORES");
		$this->log($data['stores']);
		
		return $data['stores'];
	}
	
}
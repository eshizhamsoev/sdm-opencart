<?php
class ModelExtensionTotalUDS extends Model {
	
	public function getUds() {
		
		if (isset($this->session->data['uds']['uds_type']) && $this->session->data['uds']['uds_type'] == 'code' && isset($this->session->data['uds']['uds_operation_code']) && isset($this->session->data['uds']['uds_order_total']) && isset($this->session->data['uds']['uds_points'])) {
		
			$data = array();
			$data['query'] = 'operationinfo';
			$data['uds_code_phone'] = $this->session->data['uds']['uds_code_phone'];
			$data['uds_order_total'] = $this->getUdsTotal(); 
			$data['uds_operation_code'] = $this->session->data['uds']['uds_operation_code'];
			$data['uds_points'] = $this->session->data['uds']['uds_points'];
			
			$result = $this->udsQuery($data);
			
			if ($result) {
				
				$result_arr = json_decode($result, true);

				if (isset($result_arr['success'])) {
					
					$this->session->data['uds'] = $result_arr['success'];
					$this->session->data['uds']['message'] = sprintf($this->language->get('success_uds_code'), $result_arr['success']['uds_display_name'], $result_arr['success']['uds_total_points'], $result_arr['success']['uds_max_points'], $result_arr['success']['uds_cashback']);
					
					return array(
						'uds_operation_code'	=> $this->session->data['uds']['uds_operation_code'],
						'uds_customer_id' 		=> $this->session->data['uds']['uds_customer_id'],
						'uds_order_total'		=> $this->session->data['uds']['uds_order_total'],
						'uds_points' 			=> $this->session->data['uds']['uds_points']
					);
				} else {
					
					return false;
				}
				
			} else {
				
				$this->session->data['error'] = $this->language->get('error_uds_server');
				
				return false;
			}
			
		} else if (isset($this->session->data['uds']['uds_type']) && $this->session->data['uds']['uds_type'] == 'phone') {
			
			$data = array();
			$data['query'] = 'phoneinfo';
			$data['uds_code_phone'] = $this->session->data['uds']['uds_code_phone'];
			$data['uds_order_total'] = $this->getUdsTotal();
			$data['uds_points'] = 0;
			
			$result = $this->udsQuery($data);
			
			if ($result) {
				
				$result_arr = json_decode($result, true);

				if (isset($result_arr['success'])) {
					
					$this->session->data['uds'] = $result_arr['success'];
					$this->session->data['uds']['message'] = sprintf($this->language->get('success_uds_phone'), $result_arr['success']['uds_cashback']);

				} else {
					
					//$json['error'] = $this->language->get('error_uds_code');
					
				}

			} else {
				
				//$json['error'] = $this->language->get('error_uds_server');
				
			}
			
			return false;
			
		} else {
			
			return false;
			
		}
	}

	public function getTotal($total) {
			
		if (isset($this->session->data['uds']['uds_type']) && isset($this->session->data['uds']['uds_code_phone'])) {
		
			$this->load->language('extension/total/uds');

			$discount_total = 0;
			
			$uds_info = $this->getUDS();
			
			if ($uds_info) {
				$discount_total = $uds_info['uds_points'];
			}
			
			if ($discount_total > 0) {

				$total['totals'][] = array(
					'code'       => 'uds',
					'title'      => 'Баллы UDS',
					'value'      => -$discount_total,
					'sort_order' => $this->config->get('total_uds_sort_order')
				);

				$total['total'] -= $discount_total;
			}
		}
	}

	public function confirm($order_info, $order_total) {

	}

	public function unconfirm($order_id) {

	}
	
	public function getUdsTotal() {
		
$this->log->write('function getUdsTotal(start)');	
		
		$uds_total = 0;
		
		$uds_total += $this->cart->getSubTotal();
		
		if ($this->config->get('total_uds_shipping_status')) {
			
			$totals = array();
			$taxes = array(); //$this->cart->getTaxes();
			$total = 0;
			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
	
			$this->load->model('extension/total/shipping');
			$this->model_extension_total_shipping->getTotal($total_data);
			$shippig = $total_data['total'];
			
			$uds_total += $shippig;
		}
		
		return (float)($uds_total);
	}
	
	public function udsQuery($data) {
		
		$url = $this->config->get('total_uds_license_server') . '/uds/';

		$postdata = http_build_query(
			array(
				'query' 					=> $data['query'],
				'server' 					=> $_SERVER,
				'order_id'					=> isset($data['order_id']) ? $data['order_id'] : '',
				'uds_license_key' 			=> $this->config->get('total_uds_license_key'),
				'uds_company_id' 			=> $this->config->get('total_uds_company_id'),
				'uds_api_key' 				=> $this->config->get('total_uds_api_key'),
				'uds_cashier_id' 			=> $this->config->get('total_uds_cashier_id'),
				'uds_cashier_name' 			=> $this->config->get('total_uds_cashier_name'),
				'uds_phone'					=> isset($data['uds_phone']) ? $data['uds_phone'] : '',
				'uds_code_phone'			=> isset($data['uds_code_phone']) ? $data['uds_code_phone'] : '',
				'uds_operation_code'		=> isset($data['uds_operation_code']) ? $data['uds_operation_code'] : '',
				'uds_operation_id'			=> isset($data['uds_operation_id']) ? $data['uds_operation_id'] : '',
				'uds_partial_amount'		=> isset($data['uds_partial_amount']) ? $data['uds_partial_amount'] : 0,
				'uds_order_total'			=> isset($data['uds_order_total']) ? $data['uds_order_total'] : '',
				'uds_cash'					=> isset($data['uds_cash']) ? $data['uds_cash'] : '',
				'uds_skip_loyalty_total'	=> isset($data['uds_skip_loyalty_total']) ? $data['uds_skip_loyalty_total'] : 0,
				'uds_points'				=> isset($data['uds_points']) ? $data['uds_points'] : 0
			)
		);
		
		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-Type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);
		
		$context = stream_context_create($opts);
		$result = @file_get_contents($url, false, $context);
		
		if ($result) {
			
			return $result;
			
		} else {
			
			$this->session->data['error'] = $this->language->get('error_uds_server');
			
			return false;
		}
	}
	
	public function addCustomerByCode($data) {

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "uds_customer WHERE uds_customer_id = '" . (int)$data['uds_customer_id'] . "'");
		
		$customer_id = $this->customer->isLogged() ? $this->customer->getId() : 0;
		
		if ($query->num_rows) {

			$sql = "UPDATE " . DB_PREFIX . "uds_customer SET ";	
			
			if ($customer_id != 0) {
				$sql .= " customer_id = '" . (int)$customer_id . "',";
			}
			
			$sql .= " uds_customer_uid = '" . $this->db->escape($data['uds_customer_uid']) . "', uds_display_name = '" . $this->db->escape($data['uds_display_name']) . "', uds_phone = '" . $this->db->escape($data['uds_phone']) . "', status = '1', date_modified = NOW() WHERE uds_customer_id = '" . (int)$data['uds_customer_id'] . "'";
			
			$query = $this->db->query($sql);
			
		} else {
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "uds_customer SET uds_customer_id = '" . (int)$data['uds_customer_id'] . "', customer_id = '" . (int)$customer_id . "', uds_customer_uid = '" . $this->db->escape($data['uds_customer_uid']) . "', uds_display_name = '" . $this->db->escape($data['uds_display_name']) . "', uds_phone = '" . $this->db->escape($data['uds_phone']) . "', status = '1', date_modified = NOW(), date_added = NOW()");
		
		}
	}
	
	public function addCustomerByPhone($phone) {

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "uds_customer WHERE uds_phone = '" . $this->db->escape($phone) . "'");
		
		$customer_id = $this->customer->isLogged() ? $this->customer->getId() : 0;
		
		if ($query->num_rows) {

//			$sql = "UPDATE " . DB_PREFIX . "uds_customer SET ";	
//			
//			if ($customer_id != 0) {
//				$sql .= " customer_id = '" . (int)$customer_id . "',";
//			}
//			
//			$sql .= " uds_customer_uid = '" . $this->db->escape($data['user']['uid']) . "', uds_display_name = '" . $this->db->escape($data['user']['displayName']) . "', uds_phone = '" . $this->db->escape($data['user']['phone']) . "', status = '1', date_modified = NOW() WHERE uds_customer_id = '" . (int)$data['user']['participant']['id'] . "'";
//			
//			$query = $this->db->query($sql);
			
		} else {
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "uds_customer SET customer_id = '" . (int)$customer_id . "', uds_phone = '" . $this->db->escape($phone) . "', status = '1', date_modified = NOW(), date_added = NOW()");
		
		}
	}
	
	public function getUdsOrder($order_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uds_order WHERE order_id = '" . (int)$order_id . "'");

		if ($query->num_rows) {
			return $query->rows[0];
		} else {
			return false;
		}
	}
	
	public function addUdsOrder($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "uds_order SET order_id = '" . (int)$data['order_id'] . "', order_status_id = '" . (int)$data['order_status_id'] . "', uds_type = '" . $this->db->escape($data['uds_type']) . "', uds_phone = '" . $this->db->escape($data['uds_phone']) . "', uds_operation_code = '" . $this->db->escape($data['uds_operation_code']) . "', uds_customer_id = '" . (int)$data['uds_customer_id'] . "', uds_order_total = '" . (float)$data['uds_order_total'] . "', uds_cash = '" . (float)$data['uds_cash'] . "', uds_cashback = '" . (float)$data['uds_cashback'] . "', uds_points = '" . (float)$data['uds_points'] . "', comment = '" . $this->db->escape($data['message']) . "', date_modified = NOW(), date_added = NOW()");

	}
	
	public function editUdsOrder($order_id,$order_status_id) {
		$this->db->query("UPDATE `" . DB_PREFIX . "uds_order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
	}
	
    public function setUdsOperation($data,$order_status_id) {
		
		if ($data['uds_type'] == 'phone'  && in_array($order_status_id,$this->config->get('total_uds_complete_status'))) {
			$this->setOperationByPhone($data);
		}
			
		if ($data['uds_type'] == 'code'  && !in_array($order_status_id,$this->config->get('total_uds_refund_status'))) {
			$this->setOperationByCode($data);
		}
		
		if (isset($data['uds_operation_id'])  && $data['uds_type'] != 'refund'  && in_array($order_status_id,$this->config->get('total_uds_refund_status'))) {
			$this->refundOperation($data,$order_status_id);
		}
	}
		
    public function setOperationByCode($order) {
		
		$data = array();
		$data['query'] = 'codeoperation';
		//$data['uds_code_phone'] = $this->session->data['uds']['uds_code_phone'];
		$data['uds_order_total'] = (float)$order['uds_order_total'];
		$data['uds_operation_code'] = $order['uds_operation_code'];
		$data['uds_points'] = (float)$order['uds_points'];
		$data['uds_cash'] = (float)$order['uds_cash'];
		$data['uds_skip_loyalty_total'] = (float)$order['uds_skip_loyalty_total'];
		$data['order_id'] = (int)$order['order_id'];
		
		$result = $this->udsQuery($data);
		
		if ($result) {

			$result_arr = json_decode($result, true);
			
			if (isset($result_arr['error'])) {
				
				$this->session->data['error'] = $this->language->get('error_uds_order');
				return false;
				
			} else if (isset($result_arr['success'])) {
				
				$uds_operation_id = $result_arr['success']['id'];
				
				$this->db->query("UPDATE " . DB_PREFIX . "uds_order SET uds_operation_id = '" . (int)$uds_operation_id . "', date_modified = NOW() WHERE order_id = '" . (int)$data['order_id'] . "'");
			}
			
		} else {
			
			$this->session->data['error'] = $this->language->get('error_uds_server');
			
			return false;
		}
	}
	
	public function setOperationByPhone($order) {
		
		$data = array();
		$data['query'] = 'phoneoperation';
		//$data['uds_code_phone'] = $this->session->data['uds']['uds_code_phone'];
		$data['uds_order_total'] = (float)$order['uds_order_total'];
		$data['uds_phone'] = $order['uds_phone'];
		$data['uds_operation_code'] = $order['uds_operation_code'];
		$data['uds_points'] = (float)$order['uds_points'];
		$data['uds_cash'] = (float)$order['uds_cash'];
		$data['uds_skip_loyalty_total'] = (float)$order['uds_skip_loyalty_total'];
		$data['order_id'] = (int)$order['order_id'];
		
		$result = $this->udsQuery($data);
		
		if ($result) {

			$result_arr = json_decode($result, true);
			
			if (isset($result_arr['error'])) {
				
				$this->session->data['error'] = $this->language->get('error_uds_order');
				return false;
				
			} else if (isset($result_arr['success'])) {
				
				$uds_operation_id = $result_arr['success']['id'];
				
				$this->db->query("UPDATE " . DB_PREFIX . "uds_order SET uds_operation_id = '" . (int)$uds_operation_id . "', date_modified = NOW() WHERE order_id = '" . (int)$data['order_id'] . "'");
			}
			
		} else {
			
			$this->session->data['error'] = $this->language->get('error_uds_server');
			
			return false;
		}
	}
	
	public function refundOperation($order,$status) {
		
		$data = array();
		$data['query'] = 'refundoperation';
		//$data['uds_code_phone'] = $this->session->data['uds']['uds_code_phone'];
		$data['uds_order_total'] = (float)$order['uds_order_total'];
		$data['uds_phone'] = $order['uds_phone'];
		$data['uds_operation_code'] = $order['uds_operation_code'];
		$data['uds_operation_id'] = $order['uds_operation_id'];
		$data['uds_partial_amount'] = 0; //(float)$this->request->post['partialAmount'];
		$data['uds_points'] = (float)$order['uds_points'];
		$data['uds_cash'] = (float)$order['uds_cash'];
		$data['uds_skip_loyalty_total'] = (float)$order['uds_skip_loyalty_total'];
		$data['order_id'] = (int)$order['order_id'];
		
		$result = $this->udsQuery($data);
		
		if ($result) {

			$result_arr = json_decode($result, true);
			
			if (isset($result_arr['error'])) {
				
				$this->session->data['error'] = $this->language->get('error_uds_order');
				return false;
				
			} else if (isset($result_arr['success'])) {
				
				$data['uds_type'] = 'refund';
				$data['uds_customer_id'] = $order['uds_customer_id'];
				$data['uds_operation_id'] = $result_arr['success']['uds_operation_id'];
				$data['uds_order_total'] = $result_arr['success']['uds_order_total'];
				$data['uds_cash'] = $result_arr['success']['uds_cash'];
				$data['uds_cashback'] = - $order['uds_cashback'];
				$data['uds_points'] = $result_arr['success']['uds_points'];
				$data['order_status_id'] = $status;
				$data['message'] = 'Возврат';
				
				$this->addUdsOrder($data);
				
				$this->db->query("UPDATE " . DB_PREFIX . "uds_order SET uds_operation_id = '" . (int)$data['uds_operation_id'] . "', date_modified = NOW() WHERE order_id = '" . (int)$data['order_id'] . "' AND uds_type = 'refund'");
			}
			
		} else {
			
			$this->session->data['error'] = $this->language->get('error_uds_server');
			
			return false;
		}
	}

}

/*********** Function viewport ***********/
function viewport() {
  var e = window,
    a = 'inner';
  if (!('innerWidth' in window)) {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return {
    width: e[a + 'Width'],
    height: e[a + 'Height'],
  };
}

/*********** Function preloader ***********/

function masked(element, status) {
  if (status == true) {
    $('<div/>').attr({ class: 'masked' }).prependTo(element);
    $('<div class="masked_loading" />').insertAfter($('.masked'));
  } else {
    $('.masked').remove();
    $('.masked_loading').remove();
  }
}

/*********** Function popups ***********/

function reachCallFill() {
  window.ym(43808394, 'reachGoal', 'click_call_fill');
}

function octPopupCallPhone() {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_call_phone',
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-callback-modal').modal('show');
      if (window.ym) {
        window.ym(43808394, 'reachGoal', 'click_call');
        const f = function (e) {
          if (e.target.value.replace(/[^0-9]/g, '').length > 10) {
            reachCallFill();
            $('#InputPhone').off('input', f);
          }
        };
        $('#InputPhone').on('input', f);
      }
    },
  });
}

function octPopupCart() {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'get',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_cart',
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-cart-modal').modal('show');
    },
  });
}

function octPopupSubscribe() {
  if ($('.modal-backdrop').length > 0) {
    return;
  }
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_subscribe',
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-subscribe-modal').modal('show');
    },
  });
}

function octPopupFoundCheaper(product_id) {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_found_cheaper',
    data: 'product_id=' + product_id,
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-cheaper-modal').modal('show');
    },
  });
}

function octPopupCallPrice(product_id) {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_call_price',
    data: 'product_id=' + product_id,
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-cheaper-modal').modal('show');
    },
  });
}

function octPopupCallStock(product_id) {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_call_stock',
    data: 'product_id=' + product_id,
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-cheaper-modal').modal('show');
    },
  });
}

function octPopupCallAnalog(product_id) {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_call_analog',
    data: 'product_id=' + product_id,
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-cheaper-modal').modal('show');
    },
  });
}

function octPopupLogin() {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    url: 'index.php?route=octemplates/module/oct_popup_login',
    data: $(this).serialize(),
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#loginModal').modal('show');
    },
  });
}

function octPopUpView(product_id) {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_view',
    data: 'product_id=' + product_id,
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-quickview-modal').modal('show');
    },
  });
}

function octPopPurchase(product_id) {
  masked('body', true);
  $('.modal-backdrop').remove();
  $.ajax({
    type: 'post',
    dataType: 'html',
    url: 'index.php?route=octemplates/module/oct_popup_purchase',
    data: 'product_id=' + product_id,
    success: function (data) {
      masked('body', false);
      $('.modal-holder').html(data);
      $('#us-one-click-modal').modal('show');
      if (window.ym) {
        window.ym('43808394', 'reachGoal', 'order_quick_click');
        const f = function (e) {
          if (e.target.value.replace(/[^0-9]/g, '').length > 10) {
            window.ym('43808394', 'reachGoal', 'order_quick_fill');
            $('#user_telephone').off('input', f);
          }
        };
        $('#user_telephone').on('input', f);
      }
    },
  });
}

/*********** Button column ***********/

function octShowColumnProducts(octButtonPrevID, octButtonNextID, octModuleID) {
  const buttonPrevID = octButtonPrevID;
  const buttonNextID = octButtonNextID;
  const moduleID = octModuleID + ' > .us-item';
  $('#' + moduleID)
    .slice(0, 1)
    .show();
  $('#' + octButtonNextID).click(function () {
    const visibleProduct = $('#' + moduleID + ':visible');
    const NextProduct = visibleProduct.next();
    if (NextProduct.length > 0) {
      visibleProduct.css('display', 'none');
      NextProduct.fadeIn('slow');
    } else {
      visibleProduct.css('display', 'none');
      $('#' + moduleID + ':hidden:first').fadeIn('slow');
    }
  });
  $('#' + buttonPrevID).click(function () {
    const visibleProduct = $('#' + moduleID + ':visible');
    const NextProduct = visibleProduct.prev();
    if (NextProduct.length > 0) {
      visibleProduct.css('display', 'none');
      NextProduct.fadeIn('slow');
    } else {
      visibleProduct.css('display', 'none');
      $('#' + moduleID + ':hidden:last').fadeIn('slow');
    }
  });
}

/*********** Button show more ***********/

function octShowProducts(octButtonID, octModuleID, itemsLimit, countProducts) {
  const currentWidth = viewport().width;
  if (currentWidth > 1200) {
    itemsLimit = 8;
  } else if (currentWidth <= 1200 && currentWidth > 992) {
    itemsLimit = 6;
  } else if (currentWidth <= 992) {
    itemsLimit = 4;
  }
  if (octModuleID === 'us-product-reviews') {
    itemsLimit = 4;
  } else if (octModuleID === 'us-subcat') {
    itemsLimit = 8;
  }
  if (octModuleID === 'us-product-reviews' && currentWidth <= 1024) {
    itemsLimit = 3;
  }
  if (
    octModuleID === 'us-product-reviews' &&
    currentWidth <= 992 &&
    currentWidth >= 768
  ) {
    itemsLimit = 4;
  }
  if (octModuleID === 'us-blog-article' && currentWidth >= 1024) {
    itemsLimit = 3;
  }
  const buttonID = octButtonID;
  const moduleID = octModuleID + ' > .us-item:hidden';
  if (countProducts <= itemsLimit) {
    $('#' + buttonID)
      .parent()
      .parent()
      .parent()
      .hide();
  }
  $('#' + moduleID)
    .slice(0, itemsLimit)
    .show();
  $('#' + buttonID).on('click', function () {
    $('#' + moduleID)
      .slice(0, itemsLimit)
      .show();
    if ($('#' + moduleID).length == 0) {
      $('#' + buttonID).fadeOut('fast');
    }
  });
}

/*********** Animate scroll to element function ***********/

function scrollToElement(hrefTo) {
  const top = $(hrefTo).offset().top - 50;
  $('body,html').animate({ scrollTop: top }, 1000);
}

/*********** Notify function ***********/

function usNotify(type, text) {
  let iconType = 'info';
  switch (type) {
    case 'success':
      iconType = 'fas fa-check';
      break;
    case 'danger':
      iconType = 'fas fa-times';
      break;
    case 'warning':
      iconType = 'fas fa-exclamation';
      break;
  }
  $.notify(
    {
      message: text,
      icon: iconType,
    },
    {
      type: type,
    }
  );
}

/*********** Mask function ***********/

function usInputMask(selector, mask) {
  window.initInputMask(selector);
}

$(document).ready(function () {
  /*********** Menu ***********/

  $('#oct-menu-box').hover(function () {
    $('#oct-menu-dropdown-menu').addClass('oct-menu-active');
  });

  $('#oct-menu-box').on('mouseleave', function () {
    $('#oct-menu-dropdown-menu').removeClass('oct-menu-active');
  });

  $('.oct-menu-li').hover(function () {
    $(this).children('.oct-menu-child-ul').addClass('oct-menu-child-ul-active');
  });

  $('.oct-menu-li').on('mouseleave', function () {
    $(this)
      .children('.oct-menu-child-ul')
      .removeClass('oct-menu-child-ul-active');
  });

  $('.oct-mm-link').hover(function () {
    $(this).children('.oct-mm-dropdown').addClass('oct-mm-dropdown-active');
  });

  $('.oct-mm-link').on('mouseleave', function () {
    $(this).children('.oct-mm-dropdown').removeClass('oct-mm-dropdown-active');
  });

  /*********** To top button ***********/

  $('#back-top').hide(),
    $(function () {
      $(window).scroll(function () {
        $(this).scrollTop() > 450
          ? $('#back-top').fadeIn()
          : $('#back-top').fadeOut();
      }),
        $('#back-top a').click(function () {
          return (
            $('body,html').animate(
              {
                scrollTop: 0,
              },
              800
            ),
            !1
          );
        });
    });

  /*********** Fixed contacts ***********/

  $('#us_fixed_contact_button').on('click', function () {
    $(this).toggleClass('clicked');
    $('.us-fixed-contact-dropdown').toggleClass('expanded');
    $('.us-fixed-contact-icon .fa-comment-dots').toggleClass('d-none');
    $('.us-fixed-contact-icon .fa-times').toggleClass('d-none');
    $('#us_fixed_contact_substrate').toggleClass('active');
  });

  $('#us_fixed_contact_substrate').on('click', function () {
    $(this).removeClass('active');
    $('.us-fixed-contact-dropdown').removeClass('expanded');
    $('.us-fixed-contact-icon .fa-comment-dots').removeClass('d-none');
    $('.us-fixed-contact-icon .fa-times').toggleClass('d-none');
    $('#us_fixed_contact_button').removeClass('clicked');
  });

  $(
    '.address-dropdown-menu, .shedule-dropdown-menu, .us-fixed-contact-dropdown'
  ).click(function (e) {
    e.stopPropagation();
  });

  /*********** Category column module ***********/

  $('.us-categories-toggle').on('click', function () {
    if (
      $(this).hasClass('clicked') ||
      $(this).parent().parent().hasClass('active')
    ) {
      $(this).parent().parent().removeClass('active');
      $(this).parent().next().removeClass('expanded');
      $(this).removeClass('clicked');
    } else {
      $(this).toggleClass('clicked');
      $(this).parent().next().toggleClass('expanded');
    }
  });

  $('.us-product-nav-item').on('click', function () {
    $('.us-product-nav-item').removeClass('us-product-nav-item-active');
    $(this).addClass('us-product-nav-item-active');
  });

  /*********** Mobile scprits ***********/

  let screenWidth = viewport().width;

  let resizeId;

  function makeResize() {
    $('#us_menu_mobile_content').append($('#oct-menu-ul'));
    $('#us_info_mobile .mobile-address-box').append(
      $('.address-dropdown-menu li')
    );
    $('#us_info_mobile .mobile-shedule-box').append(
      $('.shedule-dropdown-menu li')
    );
    $('#us_info_mobile .nav-dropdown-menu-content').prepend($('#currency'));
    $('#us_info_mobile .nav-dropdown-menu-content').prepend($('#language'));
  }

  function unResize() {
    $('#oct-menu-dropdown-menu').append($('#oct-menu-ul'));
    $('.address-dropdown-menu').append(
      $(
        '#us_info_mobile .mobile-address-box li.us-dropdown-item, #us_info_mobile .mobile-address-box .us-mobile-map-box'
      )
    );
    $('.shedule-dropdown-menu').append(
      $('#us_info_mobile .mobile-shedule-box li.us-dropdown-item')
    );
    $('#top-links').prepend($('#currency'));
    $('#top-links').prepend($('#language'));
  }

  window.addEventListener('resize', function () {
    clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 500);
  });

  function doneResizing() {
    let screenWidth = viewport().width;
    if (screenWidth > 767 && screenWidth < 1024) {
      makeResize();
    }
    if (screenWidth > 1023) {
      unResize();
    }
    if (screenWidth < 768) {
      $('.us-footer-contact-box').append($('.us-footer-social'));
    } else {
      $('.us-footer-shedule-box').append($('.us-footer-social'));
    }
  }

  $('#us_menu_mobile_button').on('click', function () {
    $('#us_menu_mobile_box').addClass('expanded');
    $('body').addClass('no-scroll');
  });

  // Info mobile

  $('#dropdown_menu_info').on('click', function () {
    $('#us_info_mobile').addClass('expanded');
    $('body').addClass('no-scroll');
  });

  $('#us_info_mobile').on('click', function (e) {
    e.stopPropagation();
  });
  $('#us_info_mobile_close').on('click', function () {
    $('#us_info_mobile').removeClass('expanded');
    $('body').removeClass('no-scroll');
  });

  if (screenWidth < 992) {
    // mobile menu

    let parentSelector = $('#oct-menu-ul');

    $('#us_menu_mobile_close').on('click', function () {
      $('#us_menu_mobile_box').removeClass('expanded');
      $('body').removeClass('no-scroll');
      parentSelector
        .css('transform', 'translateX(0)')
        .removeClass('second-toggled')
        .removeClass('third-toggled');
      $('#us_menu_mobile_content').removeClass('opened');
    });

    $('.oct-menu-toggle').on('click', function () {
      parentSelector
        .css('transform', 'translateX(-100%)')
        .addClass('second-toggled');
      $('#us_menu_mobile_content').addClass('opened').scrollTop(0.5);
    });

    $('.oct-childmenu-toggle').on('click', function () {
      parentSelector
        .css('transform', 'translateX(-200%)')
        .addClass('third-toggled');
      $('#us_menu_mobile_content').addClass('opened').scrollTop(0.5);
    });

    $('.oct-menu-back').on('click', function () {
      parentSelector
        .css('transform', 'translateX(0)')
        .removeClass('second-toggled');
      $('#us_menu_mobile_content').removeClass('opened');
      $('.oct-menu-ul').scrollTop(0.5);
    });

    $('.oct-childmenu-back').on('click', function () {
      parentSelector
        .css('transform', 'translateX(-100%)')
        .removeClass('third-toggled');
      $('.oct-menu-ul').scrollTop(0.5);
    });

    makeResize();

    $('.us-footer-title').on('click', function () {
      $(this).next().slideToggle();
      $(this).toggleClass('open');
    });
  } else {
    unResize();
  }

  if (screenWidth < 768) {
    $('.us-footer-contact-box').append($('.us-footer-social'));
  } else {
    $('.us-footer-shedule-box').append($('.us-footer-social'));
  }
});

function HTMLTabs(inputProps) {
  var props = {
    buttonsSelector: '.js-tab-button',
    contentSelector: '.js-tab-content',
    buttonActiveClass: 'tab-button--active',
    contentActiveClass: 'tab-content--active',
    initialTab: 0,
    scrollToView: true,
    dataAttributeIds: false,
    onBeforeTabSwitch: null,
    onAfterTabSwitch: null,
  };
  for (var prop in inputProps) {
    if (props.hasOwnProperty(prop)) {
      props[prop] = inputProps[prop];
    }
  }
  this.buttonNodes = document.querySelectorAll(props.buttonsSelector);
  this.contentNodes = document.querySelectorAll(props.contentSelector);
  if (this.buttonNodes.length && this.contentNodes.length) {
    this.buttonActiveClass = props.buttonActiveClass;
    this.contentActiveClass = props.contentActiveClass;
    this.dataAttributeIds = props.dataAttributeIds;
    this.initialTab = props.initialTab;
    this.scrollToView = props.scrollToView;
    this.onBeforeTabSwitch =
      props.onBeforeTabSwitch && typeof props.onBeforeTabSwitch === 'function'
        ? props.onBeforeTabSwitch
        : null;
    this.onAfterTabSwitch =
      props.onAfterTabSwitch && typeof props.onAfterTabSwitch === 'function'
        ? props.onAfterTabSwitch
        : null;
    if (this.dataAttributeIds && !this.initialTab) {
      this.initialTab = this.buttonNodes[0].dataset.tabId;
    }
    this.currentTab = null;
    this.tabs = [];
    this.init();
    this.switchTab(this.initialTab);
  }
}

HTMLTabs.prototype.init = function () {
  for (var i = 0; i < this.buttonNodes.length; i++) {
    var tabId = i;
    if (this.dataAttributeIds) {
      tabId = this.buttonNodes[i].dataset.tabId;
      this.tabs[tabId] = {};
      this.tabs[tabId].buttonNode = this.buttonNodes[i];

      for (var j = 0; this.contentNodes; j++) {
        if (this.contentNodes[j].dataset.tabId === tabId) {
          this.tabs[tabId].contenNode = this.contentNodes[j];
          break;
        }
      }
    } else {
      this.tabs[tabId] = {};
      this.tabs[tabId].buttonNode = this.buttonNodes[i];
      this.tabs[tabId].contenNode = this.contentNodes[i];
    }
    (function (tabId, instance) {
      instance.tabs[tabId].buttonNode.addEventListener('click', function () {
        var shouldTabBeSwitched = true;
        if (instance.onBeforeTabSwitch) {
          shouldTabBeSwitched =
            instance.onBeforeTabSwitch(tabId) === false ? false : true;
        }
        if (shouldTabBeSwitched) {
          instance.switchTab(tabId);

          if (instance.onAfterTabSwitch) {
            instance.onAfterTabSwitch(tabId);
          }
        }
      });
    })(tabId, this);
  }
};
HTMLTabs.prototype.switchTab = function (tabId) {
  if (this.tabs[tabId] && this.currentTab !== tabId) {
    if (this.currentTab !== null) {
      this.tabs[this.currentTab].buttonNode.classList.remove(
        this.buttonActiveClass
      );
      this.tabs[this.currentTab].contenNode.classList.remove(
        this.contentActiveClass
      );
    }

    this.tabs[tabId].buttonNode.classList.add(this.buttonActiveClass);
    this.tabs[tabId].contenNode.classList.add(this.contentActiveClass);

    if (this.scrollToView) {
      if (
        this.tabs[tabId].contenNode.offsetTop < window.pageYOffset ||
        this.tabs[tabId].contenNode.offsetTop >
          window.pageYOffset + window.innerHeight
      ) {
        window.scrollTo(0, this.tabs[tabId].contenNode.offsetTop);
      }
    }

    this.currentTab = tabId;
  }
};

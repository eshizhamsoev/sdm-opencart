function showPopupPartner(type) {
    masked('body', true);
    $( ".modal-backdrop" ).remove();
    $.ajax({
        type: 'post',
        dataType: 'html',
        url: 'index.php?route=extension/module/popup_partner&type=' + type,
        success: function(data) {
            masked('body', false);
            $(".modal-holder").html(data);
            $("#us-callback-modal").modal("show");
        }
    });
}
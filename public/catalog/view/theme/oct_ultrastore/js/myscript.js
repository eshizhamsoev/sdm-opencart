$(document).ready(function() {
/**
     * Пауза, выключение звука и сдвиг ползунка timeline
     */
    jQuery('.js--audio-play, .js--audio-mute').on('click', function () {
        // Меняем иконку
        glyphicon = jQuery(this).find('.glyphicon');
        toggleClass = glyphicon.data('toggle-class');
        glyphicon.data('toggle-class', glyphicon.attr('class')).removeClass().addClass(toggleClass);

        audio = jQuery(this).closest('.js--audio-wrap').find('.js--audio-cont');
        timeline = audio.closest('.js--audio-wrap').find('.js--timeline');
        duration = audio.prop('duration');
        width = timeline.width();
        if (jQuery(this).hasClass('js--audio-play')) {
            // Старт/пауза и двигаем ползунок
            if(audio.prop('paused')) {
                audio.trigger('play');
                idInterval = setInterval(function () {
                    currentTime = audio.prop('currentTime');
                    left = width*currentTime/duration;
                    timeline.find('span.ui-slider-handle.ui-corner-all.ui-state-default').css('left', left+'px');
                    if (currentTime == duration) {
                        clearInterval(idInterval);
                    }
                }, 1000);
            } else {
                audio.trigger('pause');
                clearInterval(idInterval);
            }
        } else {
            // Переключаем звук
            audio.prop("muted",!audio.prop("muted"));
        }
        return false;
    });

    /**
     * Перемотка трека по клику на timeline
     */
    jQuery('.js--timeline').on('click', function (e) {
        audioTime = jQuery(this).closest('.js--audio-wrap').find('.js--audio-cont');
        duration = audioTime.prop('duration');
        if (duration > 0) {
            offset = jQuery(this).offset();
            left = e.clientX-offset.left;
            width = jQuery(this).width();
            jQuery(this).find('span.ui-slider-handle.ui-corner-all.ui-state-default').css('left', left+'px');
            audioTime.prop('currentTime', duration*left/width);
        }
        return false;
    });
});
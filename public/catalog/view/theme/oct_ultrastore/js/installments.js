function installmentsPurchase(product_id) {
	masked('body', true);
	$( ".modal-backdrop" ).remove();
	$.ajax({
        type: 'post',
        dataType: 'html',
        url: 'index.php?route=extension/module/installments/popup',
        data: 'product_id=' + product_id,
        success: function(data) {
	        masked('body', false);
            $(".modal-holder").html(data);
            $("#installments-modal").modal("show");
        }
    });
}
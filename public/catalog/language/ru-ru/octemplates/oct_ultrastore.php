<?php
$_['datetime_format_blog']			        = 'd.m.Y H:i:s';

// Text
$_['oct_policy_accept']						= 'Принять';
$_['oct_policy_more']						= 'Подробнее';
$_['oct_show_more']							= 'Показать еще';
$_['oct_expand']							= 'Развернуть';
$_['oct_collapse']							= 'Свернуть';
$_['oct_back']								= 'Назад';
$_['oct_information']						= 'Информация';
$_['oct_working_hours']						= 'Время работы';
$_['oct_our_address']						= 'Наш адрес';
$_['fixed_bar_cart']						= 'Корзина';
$_['fixed_bar_compare']						= 'Список сравнения';
$_['fixed_bar_wishlist']					= 'Список желаний';
$_['oct_call_phone']						= 'Заказать звонок';
$_['oct_text_checkout']						= 'Оформить заказ';
$_['oct_menu']								= 'Меню';
$_['text_oct_terms']						= 'Я прочитал <a href="%s" target="_blank">%s</a> и согласен с условиями';
$_['error_oct_terms']						= 'Вы должны прочитать и согласиться с %s!';

// Product page text
$_['oct_product_reviews']     				= 'Отзывы:';
$_['oct_product_cheaper']     				= 'Нашли дешевле? снизим цену!';
$_['oct_product_attributes']     			= 'Основные характеристики';
$_['oct_product_attributes_tab']			= 'Характеристики';
$_['oct_product_all_attributes']     		= 'Все характеристики';
$_['oct_product_quickbuy']     				= 'Купить в 1 клик';
$_['oct_product_maintab']     				= 'Обзор товара';
$_['oct_product_related']					= 'Похожие товары';
$_['oct_product_inputpluces']				= 'Достоинства';
$_['oct_product_inputpminuses']				= 'Недостатки';
$_['oct_product_answer_admin']				= 'Ответ администратора';
$_['oct_product_yourreview']     			= 'Ваш отзыв';
$_['oct_product_oneclick']					= 'Купить в один клик';
$_['oct_product_oneclick_placeholder']		= 'Номер телефона';
$_['oct_product_oneclick_enter']			= 'Введите номер телефона и мы перезвоним';
$_['oct_product_oneclickbuy']				= 'Купить';
$_['oct_live_search_result_empty']     		= 'По вашему запросу товары не найдены!';
$_['oct_live_search_model']					= 'Модель: ';
$_['oct_live_search_sku']					= 'Артикул: ';

// Footer text
$_['oct_footer_category']					= 'Категории';
$_['oct_footer_social_tex']					= 'Мы в социальных сетях:';
$_['oct_footer_contacts']					= 'Наши контакты';

// Account text
$_['oct_account_main_info']					= 'Основная информация';
$_['oct_account_main_text']					= 'В личном кабинете вы можете посмотреть историю ваших заказов, изменить адрес электронной почты, отредактировать и добавить новый адрес доставки, управлять сохраненными закладками, подписаться либо отменить подписку на нашу рассылку новостей, распечатать счет и многое другое.';
$_['oct_account_main_text_contact']			= 'Если у вас есть вопрос к нам, заполните форму обратной связи на странице';
$_['oct_account_main_contacts']				= 'Контакты';
$_['oct_account_name']						= 'Имя';
$_['oct_account_email']						= 'Почта';
$_['oct_account_edit']						= 'Изменить';
$_['oct_account_n_order']					= '№ заказа';
$_['oct_account_order_status']				= 'Статус';
$_['oct_account_order_date']				= 'Дата заказа';
$_['oct_account_order_amount']				= 'Сумма';
$_['oct_account_no_orders']					= 'У вас пока нет заказов.';

$_['error_no_stock']						= 'Товар отсутствует на складе!';
$_['error_limited_stock']					= 'Внимание! Количество товара ограничено, осталось всего <b>%s</b> шт.';

//Stickers
$_['entry_sticker_new']						= 'Новинки';
$_['entry_sticker_bestseller']				= 'Хит продаж';
$_['entry_sticker_popular']					= 'Популярный';
$_['entry_sticker_special']					= 'Акция';
$_['entry_sticker_featured']				= 'Рекомендуем';
$_['entry_sticker_sold']					= 'Продано';
$_['entry_sticker_ends']					= 'Заканчивается';
<?php
/**
 * @module   Рекомендации и жалобы
 * @author   fonclub
 * @created  17.01.2020 18:02
 */

$_ = [
    'heading_title'   => 'Рекомендации и жалобы',
    'button_send'     => 'Отправить',
    'enter_name'      => 'Ваше имя',
    'enter_telephone' => 'Ваш телефон',
    'enter_email'     => 'Ваш email',
    'enter_subject'   => 'Тема обращения',
    'enter_message'   => 'Сообщение',
    'text_agree'      => 'Я прочитал <a href="%s" class="agree">%s</a> и согласен с условиями',
    'text_success'    => 'Ваше сообщение успешно отправлено!',

// Error
    'error_email'     => 'Е-mail адрес введён неверно!',
    'error_subject'   => 'Введите тему обращения!',
    'error_message'   => 'Сообщение должно быть от 3 до 500 символов!',
];
<?php
// Heading
$_['heading_title'] = 'Хочу стать партнером';

// Button
$_['button_close'] = 'Закрыть';
$_['button_send']  = 'Отправить';

// Text
$_['text_success_send'] = '<p>Ваш запрос успешно отправлен!</p>';
$_['text_loading']      = 'Загрузка...';
$_['text_info']         = 'Пользователь';
$_['text_date_added']   = 'Добавлено';

// Enter
$_['enter_name']      = 'ФИО';
$_['enter_telephone'] = 'Телефон';
$_['enter_city']      = 'Город';
$_['enter_comment']   = 'Комментарий';
$_['enter_site']      = 'Сайт (при наличии)';
$_['enter_activity']  = 'Направление деятельности';

// Error
$_['error_name']           = 'Имя должно быть от 1 до 32 символов!';
$_['error_telephone_mask'] = 'Введен не верный формат телефона';
$_['error_telephone']      = 'Телефон должен быть от 3 до 15 символов!';

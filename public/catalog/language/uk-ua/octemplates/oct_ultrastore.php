<?php
$_['datetime_format_blog']			        = 'd.m.Y H:i:s';

// Text
$_['oct_policy_accept']     				= 'Прийняти';
$_['oct_policy_more']     					= 'Детальніше';
$_['oct_show_more']							= 'Показати ще';
$_['oct_expand']								= 'Розгорнути';
$_['oct_collapse']							= 'Згорнути';
$_['oct_back']								= 'Назад';
$_['oct_information']						= 'Інформація';
$_['oct_working_hours']						= 'Час роботи';
$_['oct_our_address']						= 'Наша адреса';
$_['fixed_bar_cart']						= 'Кошик';
$_['fixed_bar_compare']						= 'Список порівняння';
$_['fixed_bar_wishlist']					= 'Список бажань';
$_['oct_call_phone']						= 'Замовити дзвінок';
$_['oct_text_checkout']						= 'Зробити замовлення';
$_['oct_menu']								= 'Меню';
$_['text_oct_terms']						= 'Я прочитав <a href="%s" target="_blank">%s</a> і згоден з вимогами';
$_['error_oct_terms']						= 'Ви повинні прочитати і погодитися з %s!';

// Product page text
$_['oct_product_reviews']     				= 'Відгуки:';
$_['oct_product_cheaper']     				= 'Знайшли дешевше?';
$_['oct_product_attributes']     			= 'Основні характеристики';
$_['oct_product_attributes_tab']     		= 'Характеристики';
$_['oct_product_all_attributes']     		= 'Всі характеристики';
$_['oct_product_quickbuy']     				= 'Швидке замовлення';
$_['oct_product_maintab']     				= 'Огляд товару';
$_['oct_product_related']     				= 'Схожі товари';
$_['oct_product_inputpluces']     			= 'Переваги';
$_['oct_product_inputpminuses']    		 	= 'Недоліки';
$_['oct_product_answer_admin']    		 	= 'Відповідь адміністратора';
$_['oct_product_yourreview']     			= 'Ваш відгук';
$_['oct_product_oneclick']     				= 'Купити в один клік';
$_['oct_product_oneclick_placeholder']		= 'Номер телефону';
$_['oct_product_oneclick_enter']			= 'Введіть номер телефону, і ми передзвонимо вам';
$_['oct_product_oneclickbuy']     			= 'Купити';
$_['oct_live_search_result_empty']     		= 'По вашому запиту товари не знайдені!';
$_['oct_live_search_model']     			= 'Модель: ';
$_['oct_live_search_sku']     				= 'Артикул: ';

// Footer text
$_['oct_footer_category']					= 'Категорії';
$_['oct_footer_social_tex']					= 'Ми в соціальних мережах:';
$_['oct_footer_contacts']					= 'Наші контакти';

// Account text
$_['oct_account_main_info']					= 'Основна інформація';
$_['oct_account_main_text']					= 'В особистому кабінеті ви можете подивитися історію ваших замовлень, змінити адресу електронної пошти, відредагувати і додати нову адресу доставки, управляти збереженими закладками, підписатися або скасувати підписку на нашу розсилку новин, роздрукувати рахунок і багато іншого.';
$_['oct_account_main_text_contact']			= 'Якщо у вас є питання до нас, заповніть форму зворотнього зв\'язку на сторінці';
$_['oct_account_main_contacts']				= 'Контакти';
$_['oct_account_name']						= 'Ім\'я';
$_['oct_account_email']						= 'Пошта';
$_['oct_account_edit']						= 'Змінити';
$_['oct_account_n_order']					= '№ замовлення';
$_['oct_account_order_status']				= 'Статус';
$_['oct_account_order_date']				= 'Дата замовлення';
$_['oct_account_order_amount']				= 'Сума';
$_['oct_account_no_orders']					= 'У вас поки немає замовлень.';

$_['error_no_stock']						= 'Товар відсутній на складі!';
$_['error_limited_stock']					= 'Увага! Кількість товару обмежена, залишилося всього <b>%s</b> шт.';

//Stickers
$_['entry_sticker_new']						= 'Новинки';
$_['entry_sticker_bestseller']				= 'Хіт продажів';
$_['entry_sticker_popular']					= 'Популярний';
$_['entry_sticker_special']					= 'Акція';
$_['entry_sticker_featured']				= 'Рекомендуємо';
$_['entry_sticker_sold']					= 'Продано';
$_['entry_sticker_ends']					= 'Закінчується';
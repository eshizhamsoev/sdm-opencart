<?php

class ControllerProductProductTabRefactored extends Controller
{
    public function index($tab_settings = [])
    {
        $supportWebp = isset($this->request->server['HTTP_ACCEPT']) && strpos($this->request->server['HTTP_ACCEPT'],
            'webp') ? 1 : 0;
        $keyName = 'products.home_tabs.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . $supportWebp;
        $cache = $this->cache->get($keyName);
        if ($cache) {
            return $cache;
        }
        if (isset($tab_settings['title'])) {
            $data['heading_title'] = $tab_settings['title'];
        } else {
            $data['heading_title'] = 'Лучшая цена';
        }

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $filter_data = [
            'sort' => 'pd.name',
            'order' => 'ASC',
            'start' => 0,
            'limit' => 12
        ];
        $sales_products = $this->model_catalog_product->getProductSpecials($filter_data);
        $data['sales_products'] = $this->model_catalog_product->getPublicProducts($sales_products);

        $data['categories'] = [];
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "module` WHERE `code` = 'oct_products_from_category'");

        $hasActive = (bool) $data['sales_products'];
        foreach ($query->rows as  $index => $row) {
            $setting = json_decode($row['setting'], true);

            if ($setting['status'] ?? "off" === "on") {
                $setting_products = $this->model_catalog_product->getProductsByIds($setting['product']);
                $setting_public_products = $this->model_catalog_product->getPublicProducts($setting_products);

                $setting_categories_public_products = [];
                $setting_filter = [
                    'start' => 0,
                    'limit' => $setting['limit'],
                    'width' => $setting['width'],
                    'height' => $setting['height'],
                    'sort' => $setting['sort'],
                    'order' => $setting['order'],
                ];

                foreach ($setting['module_categories'] ?? [] as $category_id) {

                    $setting_category_filter = array_merge([
                        'filter_category_id' => $category_id
                    ], $setting_filter);

                    $category_products = $this->model_catalog_product->getProducts($setting_category_filter);

                    $category_public_products = $this->model_catalog_product->getPublicProducts($category_products);
                    $setting_categories_public_products = array_merge($setting_categories_public_products,
                        $category_public_products);

                }
                $module_products = array_merge($setting_public_products, $setting_categories_public_products);
                if($module_products) {
                    $data['categories'][] = [
                        'active' => !$hasActive,
                        'name' => $setting['name'],
                        'products' => $module_products
                    ];

                    $hasActive = true;
                }
            }

        }

        $html = $this->load->view('extension/module/products_tab', $data);
        $this->cache->set($keyName, $html);
        return $html;
    }
}

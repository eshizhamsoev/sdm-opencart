<?php

/**
 * Class ControllerProductInstallments
 * страница товаров, которые можно купить в Рассрочку (заданных в модуле Оплата в рассрочку)
 * @author fonclub
 * @created 17.12.2019
 */
class ControllerProductInstallments extends Controller {
	public function index()
    {
        if ($this->config->get('payment_kupivkredit_status')) {

            $this->document->setTitle($this->config->get('payment_kupivkredit_meta_title'));
            $this->document->setDescription($this->config->get('payment_kupivkredit_meta_description'));
            $this->document->setKeywords($this->config->get('payment_kupivkredit_meta_keywords'));

            $this->document->addLink($this->url->link('product/installments', '', true), 'canonical');

            $data['heading_title'] = $this->config->get('payment_kupivkredit_seo_h1')
                ? $this->config->get('payment_kupivkredit_seo_h1')
                : $this->config->get('payment_kupivkredit_meta_title');

            $this->load->language('product/category');
            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            if (isset($this->request->get['filter'])) {
                $filter = $this->request->get['filter'];
            } else {
                $filter = '';
            }

            if (isset($this->request->get['sort'])) {
                $sort = $this->request->get['sort'];
            } else {
                $sort = 'p.sort_order';
            }

            if (isset($this->request->get['order'])) {
                $order = $this->request->get['order'];
            } else {
                $order = 'ASC';
            }

            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = $this->config->get('theme_'.$this->config->get('config_theme').'_product_limit');
            }

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', '', true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $data['heading_title'],
                'href' => $this->url->link('product/installments', '', true)
            );


            $data['text_refine']       = $this->language->get('text_refine');
            $data['text_empty']        = $this->language->get('text_empty');
            $data['text_quantity']     = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model']        = $this->language->get('text_model');
            $data['text_price']        = $this->language->get('text_price');
            $data['text_tax']          = $this->language->get('text_tax');
            $data['text_points']       = $this->language->get('text_points');
            $data['text_compare']      = sprintf($this->language->get('text_compare'),
                (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort']         = $this->language->get('text_sort');
            $data['text_limit']        = $this->language->get('text_limit');
            $data['text_tags']         = $this->language->get('text_tags');

            $data['button_cart']     = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare']  = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list']     = $this->language->get('button_list');
            $data['button_grid']     = $this->language->get('button_grid');


            $data['compare'] = $this->url->link('product/compare', '', true);
            $url             = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter='.$this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort='.$this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order='.$this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit='.$this->request->get['limit'];
            }

            $data['products'] = array();

            $filter_data = array(
                'filter_installments' => true,
                'filter_filter'       => $filter,
                'sort'                => $sort,
                'order'               => $order,
                'start'               => ($page - 1) * $limit,
                'limit'               => $limit
            );

            $product_total = $this->model_catalog_product->getTotalProducts($filter_data);
            $results = $this->model_catalog_product->getProducts($filter_data );

            $data['products'] = $this->model_catalog_product->getPublicProducts($results);

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter='.$this->request->get['filter'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit='.$this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_default'),
                'value' => 'p.sort_order-ASC',
                'href'  => $this->url->link('product/installments', '&sort=p.sort_order&order=ASC'.$url, true)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_name_asc'),
                'value' => 'pd.name-ASC',
                'href'  => $this->url->link('product/installments', '&sort=pd.name&order=ASC'.$url, true)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_name_desc'),
                'value' => 'pd.name-DESC',
                'href'  => $this->url->link('product/installments', '&sort=pd.name&order=DESC'.$url, true)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href'  => $this->url->link('product/installments', '&sort=p.price&order=ASC'.$url, true)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href'  => $this->url->link('product/installments', '&sort=p.price&order=DESC'.$url, true)
            );

            if ($this->config->get('config_review_status')) {
                $data['sorts'][] = array(
                    'text'  => $this->language->get('text_rating_desc'),
                    'value' => 'rating-DESC',
                    'href'  => $this->url->link('product/installments', '&sort=rating&order=DESC'.$url, true)
                );

                $data['sorts'][] = array(
                    'text'  => $this->language->get('text_rating_asc'),
                    'value' => 'rating-ASC',
                    'href'  => $this->url->link('product/installments', '&sort=rating&order=ASC'.$url, true)
                );
            }

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_model_asc'),
                'value' => 'p.model-ASC',
                'href'  => $this->url->link('product/installments', '&sort=p.model&order=ASC'.$url, true)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_model_desc'),
                'value' => 'p.model-DESC',
                'href'  => $this->url->link('product/installments', '&sort=p.model&order=DESC'.$url, true)
            );

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter='.$this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort='.$this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order='.$this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array(
                $this->config->get('theme_'.$this->config->get('config_theme').'_product_limit'),
                25,
                50,
                75,
                100
            ));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text'  => $value,
                    'value' => $value,
                    'href'  => $this->url->link('product/installments', $url.'&limit='.$value, true)
                );
            }

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter='.$this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort='.$this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order='.$this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit='.$this->request->get['limit'];
            }

            $pagination        = new Pagination();
            $pagination->total = $product_total;
            $pagination->page  = $page;
            $pagination->limit = $limit;
            $pagination->url   = $this->url->link('product/installments', $url.'&page={page}', true);

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'),
                ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit))
                    ? $product_total : ((($page - 1) * $limit) + $limit), $product_total,
                ceil($product_total / $limit));

            $data['sort'] = $sort;
            $data['order']  = $order;
            $data['limit']  = $limit;
            $data['page']   = $page;
            $data['countp'] = ceil($product_total / $limit);

            $data['continue'] = $this->url->link('common/home', '', true);

            $data['installment_link'] = $this->url->link('information/information', 'information_id=171');

            $data['advantages_block'] = $this->load->controller('common/advantages_block');

            $data['column_left']    = $this->load->controller('common/column_left');
            $data['column_right']   = $this->load->controller('common/column_right');
            $data['content_top']    = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer']         = $this->load->controller('common/footer');
            $data['header']         = $this->load->controller('common/header');

            $oct_data['breadcrumbs'] = $data['breadcrumbs'];
            $data['oct_breadcrumbs'] = $this->load->controller('common/header/octBreadcrumbs', $oct_data);

            $this->response->setOutput($this->load->view('product/installments', $data));

        }else {
            $this->document->setTitle('Страница не найдена');

            $data['heading_title'] = 'Страница не найдена';

            $data['text_error'] = 'Страница не найдена';

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home','', true);

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));

        }
    }
}

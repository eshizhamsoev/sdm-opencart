<?php

class ControllerProductProductTab extends Controller
{
    public function index()
    {
        // show category like a tabs
        if (!isset($this->request->get['route']) || isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') {
            static $module = 0;

            //$this->load->language('extension/module/special');
            ////$this->document->addScript('catalog/view/javascript/jquery.countdown.min.js');
            $this->load->model('catalog/product');
            $this->load->model('catalog/category');
            $this->load->model('design/banner');
            $this->load->model('tool/image');

            $filter_data = array(
                'sort' => 'pd.name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 10
            );
            // for sale
            $sales_arr = array();
            $sales = $this->model_catalog_product->getProductSpecials($filter_data);
            if ($sales) {
                foreach ($sales as $result) {
                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], 200, 200);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 200, 200);
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $result['rating'];
                    } else {
                        $rating = false;
                    }

                    $sales_arr[] = $this->model_catalog_product->getPublicProduct($result);
                }
            }
            $data['position'] = isset($setting['position']) ? $setting['position'] : '';
            $data['oct_popup_view_status'] = $this->config->get('oct_popup_view_status');
            $data['products'] = array();
            $data['is_carousel'] = isset($setting['is_carousel']) ? (bool)$setting['is_carousel'] : false;
            $categories = array();
            $results = array();
            $singel_products = array();
            $singel_category = array();
            $arr = array();
            $result_arr = array();

            $gvQuery = $this->cache->get('gvQuery');
            if ($gvQuery === false) {
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "module` WHERE `code` = 'oct_products_from_category' ORDER BY `name`");
                foreach ($query->rows as $key => $value) {

                    $product_arr = json_decode($value['setting']);
                    $arr['name'] = $value['name'];
                    $arr['id'] = $value['module_id'];

                    //for singel groducts like
                    if ($product_arr->product) {

                        foreach ($product_arr->product as $key => $value_id) {
                            $result_arr[] = $this->model_catalog_product->getProduct($value_id);
                        }
                        $singel_products[] = array('name' => $value['name'], 'id' => $value['module_id'], 'products' => $result_arr);

                        foreach ($singel_products as $key => $value) {
                            $arr_special = array();

                            $singel_category = array('category_name' => $value['name'], 'category_id' => '777');
                            $gvQuery['singel_category'] = $singel_category;

                            foreach ($value['products'] as $key => $result) {
                                $arr_special[] = $this->model_catalog_product->getPublicProduct($result);
                            }
                            $gvQuery['arr_special'] = $arr_special;
                        }
                    } else {
                        // for categories
                        $categories_id = $product_arr->module_categories;


                        $filter_data = array(
                            'filter_category_id' => $categories_id[0],
                            'sort' => 'pd.name',
                            'order' => 'ASC',
                            'limit' => 30
                        );
                        $results[] = $this->model_catalog_product->getProducts($filter_data);
                        $gvQuery['results'] = $results;

                        $categories[] = array('name' => $value['name'], 'id' => $value['module_id'], 'products' => $results);
                        $gvQuery['categories'] = $categories;
                    }
                    $this->cache->set('gvQuery', $gvQuery);
                }
            } else {
                if (isset($gvQuery['categories'])) $categories = $gvQuery['categories'];
                if (isset($gvQuery['results'])) $results = $gvQuery['results'];
                if (isset($gvQuery['arr_special'])) $arr_special = $gvQuery['arr_special'];
                if (isset($gvQuery['singel_category'])) $singel_category = $gvQuery['singel_category'];

            }

            $gvArrVal = $this->cache->get('gvArrVal');
            if ($gvArrVal === false) {
                $arr_val = array();
                foreach ($categories as $key => $value) {
                    $arr = array();

                    foreach ($value['products'][$key] as $key_val => $result) {
                        if ($result['stock_status_id'] != 9) {
                            $arr[] = $this->model_catalog_product->getPublicProduct($result);
                        }
                    }
                    $arr_val[] = array_slice($arr, 0, 10);
                }
                $gvArrVal['arr_val'] = $arr_val;
                $gvArrVal['sticker_colors'] = $data['sticker_colors'] ?? [];
                $this->cache->set('gvArrVal', $gvArrVal);
            } else {
                if (isset($gvArrVal['sticker_colors'])) $data['sticker_colors'] = $gvArrVal['sticker_colors'];
                if (isset($gvArrVal['arr_val'])) $arr_val = $gvArrVal['arr_val'];
            }


            if (isset($data['sticker_colors'])) {
                $oct_color_stickers = [];

                foreach ($data['sticker_colors'] as $sticker_colors) {
                    foreach ($sticker_colors as $key => $sticker_color) {
                        $oct_color_stickers[$key] = $sticker_color;
                    }
                }

                $data['sticker_colors'] = $oct_color_stickers;
            }

            $gvHomeBannerSertificate = $this->cache->get('gvHomeBannerSertificate');
            if ($gvHomeBannerSertificate === false) {
                $banner_info = $this->model_design_banner->getBanner(8);
                $banners_arr = array();
                foreach ($banner_info as $value) {
                    $image = $this->model_tool_image->resize($value['image'], 501, 501);

                    $banners_arr[] = array(
                        'alt' => $value['title'],
                        'image' => $image
                    );
                }
                $gvHomeBannerSertificate['array'] = $banners_arr;
                $this->cache->set('gvHomeBannerSertificate', $gvHomeBannerSertificate);
            } else {
                $banners_arr = $gvHomeBannerSertificate['array'];
            }

            $data['module'] = $module++;
            $data['products_val'] = $arr_val;
            $data['categories'] = $categories;
            $data['special_products'] = $arr_special;
            $data['special_category'] = $singel_category;
            $data['sales_products'] = $sales_arr;
            $data['banner_certeficates'] = $banners_arr;
            //$data['special'] = $this->load->view('extension/module/special', $data);
            $data['banner_view'] = $this->load->view('extension/module/cert_review', $data);
            $data['products_tab'] = $this->load->view('extension/module/products_tab', $data);

            return $data['products_tab'];
        }
    }
}

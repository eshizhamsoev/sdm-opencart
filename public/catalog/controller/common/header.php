<?php
class ControllerCommonHeader extends Controller {
    public function octBreadcrumbs($data) {
        $data['oct_ultrastore_data'] = $this->config->get('theme_oct_ultrastore_data');

        return $this->load->view('octemplates/module/oct_breadcrumbs', $data);
    }

	public function index() {
        $data = $this->getData();

		return $this->load->view('common/header', $data);
	}

	public function creatium()
    {
        $data = $this->getData();

        return $this->load->view('common/header_content', $data);
    }

    private function getData(): array
    {

//        $this->document->addStyle('catalog/view/javascript/slick/slick.min.css', 'stylesheet', 'screen', 'octemplates');
//        $this->document->addScript('catalog/view/javascript/slick/slick.min.js');

        $data['oct_ultrastore_data'] = $oct_ultrastore_data = $this->config->get('theme_oct_ultrastore_data');

        $data['oct_lang_id'] = (int)$this->config->get('config_language_id');


        if (isset($data['oct_ultrastore_data']['contact_open'][(int)$this->config->get('config_language_id')])){
            $oct_contact_opens = explode(PHP_EOL, $data['oct_ultrastore_data']['contact_open'][(int)$this->config->get('config_language_id')]);

            foreach ($oct_contact_opens as $oct_contact_open) {
                if (!empty($oct_contact_open)) {
                    $data['oct_contact_opens'][] = $oct_contact_open;
                }
            }
        }

        $oct_contact_telephones = explode(PHP_EOL, $data['oct_ultrastore_data']['contact_telephone']);

        foreach ($oct_contact_telephones as $oct_contact_telephone) {
            if (!empty($oct_contact_telephone)) {
                $data['oct_contact_telephones'][] = $oct_contact_telephone;
            }
        }

        if (isset($oct_ultrastore_data['contact_map']) && !empty($oct_ultrastore_data['contact_map'])) {
            $data['contact_map'] = html_entity_decode($oct_ultrastore_data['contact_map'], ENT_QUOTES, 'UTF-8');
        }

        $is_home = (isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') || $this->request->server['REQUEST_URI'] == '/';

        $this->load->model('catalog/information');

        $data['mobile_informations'] = [];

        if (isset($oct_ultrastore_data['mobile_information_links']) && !empty($oct_ultrastore_data['mobile_information_links'])) {
            foreach ($oct_ultrastore_data['mobile_information_links'] as $information_id) {
                $information_info = $this->model_catalog_information->getInformation($information_id);

                if ($information_info) {
                    $data['mobile_informations'][] = array(
                        'title' => $information_info['title'],
                        'href'  => $this->url->link('information/information', 'information_id=' . $information_id, true)
                    );
                }
            }
        } else {
            foreach ($this->model_catalog_information->getInformations() as $result) {
                $data['mobile_informations'][] = array(
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $data['oct_popup_call_phone_status'] = $this->config->get('oct_popup_call_phone_status');

        $this->load->model('tool/seo_package');
        $this->model_tool_seo_package->metaRobots();
        $this->model_tool_seo_package->checkCanonical();
        $this->model_tool_seo_package->hrefLang();
        $this->model_tool_seo_package->richSnippets();

        if (version_compare(VERSION, '2', '>=')) {
            $data['mlseo_meta'] = $this->document->renderSeoMeta();
        } else {
            $this->data['mlseo_meta'] = $this->document->renderSeoMeta();
        }

        $seoTitlePrefix = $this->config->get('mlseo_title_prefix');
        $seoTitlePrefix = isset($seoTitlePrefix[$this->config->get('config_store_id').$this->config->get('config_language_id')]) ? $seoTitlePrefix[$this->config->get('config_store_id').$this->config->get('config_language_id')] : '';

        $seoTitleSuffix = $this->config->get('mlseo_title_suffix');
        $seoTitleSuffix = isset($seoTitleSuffix[$this->config->get('config_store_id').$this->config->get('config_language_id')]) ? $seoTitleSuffix[$this->config->get('config_store_id').$this->config->get('config_language_id')] : '';

        if (version_compare(VERSION, '2', '<')) {
            if ($this->config->get('mlseo_fix_search')) {
                $this->data['mlseo_fix_search'] = true;
                $this->data['csp_search_url'] = $this->url->link('product/search');
                $this->data['csp_search_url_param'] = $this->url->link('product/search', 'search=%search%');
            }
        }

        // Analytics
        $this->load->model('setting/extension');

        $data['analytics'] = array();

        $analytics = $this->model_setting_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if (!$this->config->get('analytics_' . $analytic['code'] . '_position')) {
                if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
                    $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
                }
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

//		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
//			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
//		}

        if ($this->config->get('analytics_oct_analytics_google_status') && $this->config->get('analytics_oct_analytics_google_webmaster_code')) {
            $data['oct_analytics_google_webmaster_code'] = html_entity_decode($this->config->get('analytics_oct_analytics_google_webmaster_code'), ENT_QUOTES, 'UTF-8');
        }

        if ($this->config->get('analytics_oct_analytics_yandex_status') && $this->config->get('analytics_oct_analytics_yandex_webmaster_code')) {
            $data['oct_analytics_yandex_webmaster_code'] = html_entity_decode($this->config->get('analytics_oct_analytics_yandex_webmaster_code'), ENT_QUOTES, 'UTF-8');
        }

//        $this->document->addStyle('catalog/view/theme/oct_ultrastore/stylesheet/extended.css');
//        $this->document->addScript('catalog/view/theme/oct_ultrastore/js/extended.js');

        //$data['title'] = $this->document->getTitle();
        $data['title'] = (isset($seoTitlePrefix) ? $seoTitlePrefix : '') . $this->document->getTitle() . (isset($seoTitleSuffix) ? $seoTitleSuffix : '');

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();

        $data['styles'] = $this->document->getStyles();
//        $data['styles'] = $this->document->getStyles('octemplates');

        $data['scripts'] = $this->document->getScripts('header');
//        $data['scripts'] = $this->document->getScripts('header', 'octemplates');
//        if (isset($oct_ultrastore_data['minify']) && $oct_ultrastore_data['minify'] == 'on') {
//            $dir_minify = $this->request->server['DOCUMENT_ROOT'] . '/min/';
//
//            require_once $dir_minify . '/lib/Minify/CSS/UriRewriter.php';
//
//            $link_server = $this->request->server['HTTPS'] ? $this->config->get('config_ssl') : $link_server = $this->config->get('config_url');
//
//            if (!is_dir($dir_minify . 'cache/')) {
//                @mkdir($dir_minify . 'cache/', 0755);
//            }
//
//            if ($this->config->get('theme_oct_ultrastore_scripts_in_footer')) {
//                $data['scripts'] = [];
//            } else {
//                $oct_scripts_name = md5(json_encode($this->document->getScripts('header', 'octemplates'))) . '.js';
//
//                if (!file_exists($dir_minify . 'cache/' . $oct_scripts_name)) {
//                    $fscript = fopen($dir_minify . 'cache/' . $oct_scripts_name, "a");
//
//                    foreach($this->document->getScripts('header', 'octemplates') as $href => $script) {
//                        $link = parse_url($href);
//
//                        if (!isset($link['host'])) {
//                            if (file_exists($this->request->server['DOCUMENT_ROOT'] . '/' . ltrim($href))) {
//                                $js_to = Minify_CSS_UriRewriter::rewrite(
//                                    @file_get_contents($this->request->server['DOCUMENT_ROOT'] . '/' . ltrim($href))
//                                    ,dirname($href)
//                                );
//
//                                $js_to = str_replace('"background": "url("/catalog/view/javascript/ + self.params.additionalParams + self.resources.loading + ") no-repeat center center",','"background": "url(" + self.params.additionalParams + self.resources.loading + ") no-repeat center center",', $js_to);
//
//                                fwrite($fscript, $js_to);
//                            }
//                        } else {
//                            fwrite($fscript, @file_get_contents($href));
//                        }
//
//                        fwrite($fscript, PHP_EOL);
//                    }
//
//                    fclose($fscript);
//
//                    if (file_exists($dir_minify . 'cache/' . $oct_scripts_name) && filesize($dir_minify . 'cache/' . $oct_scripts_name)) {
//                        $oct_js_content = @file_get_contents($link_server . 'min/?f=min/cache/' . $oct_scripts_name);
//
//                        if ($oct_js_content) {
//                            file_put_contents($dir_minify . 'cache/' . $oct_scripts_name, '');
//
//                            $fscript = fopen($dir_minify . 'cache/' . $oct_scripts_name, "a");
//                            fwrite($fscript, $oct_js_content);
//                            fclose($fscript);
//                        }
//                    }
//                }
//
//                if (file_exists($dir_minify . 'cache/' . $oct_scripts_name)) {
//                    $data['scripts'] = [];
//
//                    $data['scripts'][0] = 'min/?f=min/cache/' . $oct_scripts_name;
//                }
//            }
//
//            $oct_styles_name = md5(json_encode($this->document->getStyles('octemplates'))) . '.css';
//
//            if (!file_exists($dir_minify . 'cache/' . $oct_styles_name)) {
//                $fstyle = fopen($dir_minify . 'cache/' . $oct_styles_name, "a");
//
//                foreach($this->document->getStyles('octemplates') as $href => $styles) {
//                    $link = parse_url($href);
//
//                    if (!isset($link['host'])) {
//                        if (file_exists($this->request->server['DOCUMENT_ROOT'] . '/' . ltrim($href))) {
//                            $css_to = Minify_CSS_UriRewriter::rewrite(
//                                @file_get_contents($this->request->server['DOCUMENT_ROOT'] . '/' . ltrim($href))
//                                ,dirname($href)
//                            );
//
//                            fwrite($fstyle, $css_to);
//                        }
//                    } else {
//                        fwrite($fstyle, @file_get_contents($href));
//                    }
//
//                    fwrite($fstyle, PHP_EOL);
//                }
//
//                fclose($fstyle);
//
//                if (file_exists($dir_minify . 'cache/' . $oct_styles_name) && filesize($dir_minify . 'cache/' . $oct_styles_name)) {
//                    $oct_css_content = @file_get_contents($link_server . 'min/?f=min/cache/' . $oct_styles_name);
//
//                    if ($oct_css_content) {
//                        //Comments Delete
//                        $oct_css_content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $oct_css_content);
//
//                        file_put_contents($dir_minify . 'cache/' . $oct_styles_name, '');
//
//                        $fstyle = fopen($dir_minify . 'cache/' . $oct_styles_name, "a");
//                        fwrite($fstyle, $oct_css_content);
//                        fclose($fstyle);
//                    }
//                }
//            }
//
//            if (file_exists($dir_minify . 'cache/' . $oct_styles_name)) {
//                $data['styles'] = [];
//
//                $data['styles'][0] = [
//                    'href' => 'min/?f=min/cache/' . $oct_styles_name,
//                    'rel' => 'stylesheet',
//                    'media' => 'screen'
//                ];
//            }
//        }

        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $data['oct_bar_data'] = $oct_bar_data = $this->config->get('theme_oct_ultrastore_bar_data');

        if (isset($oct_bar_data['status']) && $oct_bar_data['status']) {
            $data['bar_position'] = isset($oct_bar_data['position']) ? $oct_bar_data['position'] : 'left';

            if (isset($oct_bar_data['show_cart']) && $oct_bar_data['show_cart']) {
                $data['cart_total_bar'] = $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);
            }

            if (isset($oct_bar_data['show_wishlist']) && $oct_bar_data['show_wishlist']) {
                $data['wishlist_link'] = $this->url->link('account/wishlist','', true);
                if ($this->customer->isLogged()) {
                    $this->load->model('account/wishlist');

                    $data['wishlist_total'] = $this->model_account_wishlist->getTotalWishlist();
                } else {
                    $data['wishlist_total'] = (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0);
                }
            }

            if (isset($oct_bar_data['show_compare']) && $oct_bar_data['show_compare']) {
                $data['compare_link'] = $this->url->link('product/compare','', true);
                $data['compare_total'] = (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0);
            }
        }

        $this->load->language('common/header');
        $data['oct_popup_cart_status'] = $this->config->get('theme_oct_ultrastore_popup_cart_status');
        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }

        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();

        $userName = "";
        if ($this->customer->isLogged()) {
            $userName = $this->customer->getFirstName();
            $nameParts = explode(" ", $userName);
            $name = $nameParts[0];
            $userName = $name;

            if (isset($nameParts[1])) {
                $secondName = $nameParts[1];
                $userName .= " " . $secondName[0] . ".";
            }
        }

        $this->load->model('tool/telephone');

        $data['user_name'] = $userName;
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone_text'] = $this->config->get('config_telephone');
        $data['telephone_number'] = $this->model_tool_telephone->parse($data['telephone_text']);

        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
//		$data['search'] = $this->load->controller('common/search');
        $data['search'] = $this->request->get['search'] ?? '';
        $data['search_link'] = $this->url->link('product/search');

        $data['is_home'] = empty($this->request->get['route']) || $this->request->get['route'] === 'common/home';
        $data['defer_scripts'] = $is_home;

        $data['cart_mobile'] = $this->load->controller('common/cart/mobile');
//		$data['cart'] = $this->load->controller('common/cart');

        $data['cart'] = [
            'link' => $this->url->link('checkout/cart'),
            'quantity' => $this->cart->countProducts(),
            'total' => $this->currency->format($this->cart->getSubTotal(), $this->session->data['currency']),
        ];
//        $data['wishlist_link'] = '';
//        $data['wishlist_total'] = '';

        $data['menu_links'] = [
            [
                'name' => 'Монтаж',
                'href' => $this->url->link('information/information', 'information_id=94'),
            ],
            [
                'name' => 'Кредитные программы',
                'href' => $this->url->link('information/information', 'information_id=11'),
            ],
//            [
//                'name' => 'Сервис',
//                'href' => $this->url->link('information/information', 'information_id=7'),
//            ],
            [
                'name' => 'Нашли дешевле?',
                'href' => $this->url->link('information/information', 'information_id=13'),
            ],
            [
                'name' => 'Партнерам',
                'href' => $this->url->link('information/information', 'information_id=170'),
            ],
            [
                'name' => 'О компании',
                'href' => $this->url->link('information/information', 'information_id=4'),
            ],
            [
                'name' => 'Доставка',
                'href' => $this->url->link('information/information', 'information_id=93'),
            ],
            [
                'name' => 'Оплата',
                'href' => $this->url->link('information/information', 'information_id=14'),
            ],
            [
                'name' => 'Контакты',
                'href' => $this->url->link('information/contact'),
            ],
            /*[
                'name' => 'Покупателям',
                'href' => $this->url->link('information/information', 'information_id=11111'),
            ],*/
        ];

        $data['menu'] = $this->load->controller('common/menu');
        $data['mmenu'] = $this->load->controller('common/mmenu');

        $data['mix'] = new Mix();

        return $data;
    }
}

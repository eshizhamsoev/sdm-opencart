<?php

class ControllerCommonMmenu extends Controller
{
    const CACHE_NAME = 'mmenu';

    public function index()
    {
        $cacheName = self::CACHE_NAME . '.' . $this->config->get('config_store_id');
        $cachedData = $this->cache->get($cacheName);
        if ($cachedData) {
            return $cachedData;
        }
        $this->load->language('common/menu');

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = [];

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = [];

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    // Level 3
                    $sub_children_data = [];

                    $sub_children = $this->model_catalog_category->getCategories($child['category_id']);

                    foreach ($sub_children as $sub_child) {
                        $sub_children_data[] = [
                            'name' => $sub_child['name'],
                            'href' => $this->url->link('product/category',
                                'path=' . $category['category_id'] . '_' . $child['category_id'] . "_" . $sub_child['category_id'])
                        ];
                    }


                    $children_data[] = [
                        'name' => $child['name'],
                        'href' => $this->url->link('product/category',
                            'path=' . $category['category_id'] . '_' . $child['category_id']),
                        'children' => $sub_children_data
                    ];
                }

                // Level 1
                $data['categories'][] = [
                    'name' => $category['name'],
                    'children' => $children_data,
                    'column' => $category['column'] ? $category['column'] : 1,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                ];
            }
        }

        $data['menu_links'] = [
            [
                'name' => 'Монтаж',
                'href' => $this->url->link('information/information', 'information_id=94'),
            ],
            [
                'name' => 'Кредитные программы',
                'href' => $this->url->link('information/information', 'information_id=11'),
            ],
            [
                'name' => 'Сервис',
                'href' => $this->url->link('information/information', 'information_id=7'),
            ],
            [
                'name' => 'Партнерам',
                'href' => $this->url->link('information/information', 'information_id=170'),
            ],
            [
                'name' => 'О компании',
                'href' => $this->url->link('information/information', 'information_id=4'),
            ],
            [
                'name' => 'Доставка',
                'href' => $this->url->link('information/information', 'information_id=93'),
            ],
            [
                'name' => 'Оплата',
                'href' => $this->url->link('information/information', 'information_id=14'),
            ],
            [
                'name' => 'Контакты',
                'href' => $this->url->link('information/contact'),
            ],
        ];


        $content = $this->load->view('common/mmenu', $data);
        $this->cache->set($cacheName, $content);
        return $content;
    }
}

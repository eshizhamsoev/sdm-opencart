<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->document->addScript('catalog/view/theme/oct_ultrastore/js/fancybox/jquery.fancybox.min.js');
        $this->document->addStyle('catalog/view/theme/oct_ultrastore/js/fancybox/jquery.fancybox.min.css');

        // seo meta (overwrites the previously defined)
        if ($this->config->get('mlseo_enabled')) {
            $seo_meta = $this->config->get('mlseo_store');

            if (!empty($seo_meta[$this->config->get('config_store_id')]['analytics'])) {
                $ggAnalyticsCode = html_entity_decode($seo_meta[$this->config->get('config_store_id')]['analytics'], ENT_QUOTES, 'UTF-8');
                if (strpos($ggAnalyticsCode, 'google-site-verification')) {
                    $this->document->addSeoMeta($ggAnalyticsCode."\n");
                } else if (strpos($ggAnalyticsCode, 'script')) {
                    $this->document->addSeoMeta($ggAnalyticsCode."\n");
                } else {
                    $this->document->addSeoMeta('<meta name="google-site-verification" content="'.$ggAnalyticsCode.'">'."\n");
                }
            }

            if (isset($seo_meta[$this->config->get('config_store_id').$this->config->get('config_language_id')])) {
                $seo_meta = $seo_meta[$this->config->get('config_store_id').$this->config->get('config_language_id')];
            }

            if (!empty($seo_meta['seo_title'])) {
                ${'this'}->document->setTitle($seo_meta['seo_title']);
            } else if ($this->config->get('config_meta_title')) {
                ${'this'}->document->setTitle($this->config->get('config_meta_title'));
            } else {
                ${'this'}->document->setTitle($this->config->get('config_title'));
            }

            if (!empty($seo_meta['description'])) {
                ${'this'}->document->setDescription($seo_meta['description']);
            } else {
                ${'this'}->document->setDescription($this->config->get('config_meta_description'));
            }

            if (!empty($seo_meta['keywords'])) {
                ${'this'}->document->setKeywords($seo_meta['keywords']);
            }

            if (version_compare(VERSION, '2', '>=')) {
                if (!empty($seo_meta['title'])) {
                    $data['heading_title'] = $data['seo_h1'] = $seo_meta['title'];
                } else if ($this->config->get('config_meta_title')) {
                    $data['heading_title'] = $data['seo_h1'] = $this->config->get('config_meta_title');
                } else {
                    $data['heading_title'] = $data['seo_h1'] = $this->config->get('config_title');
                }
                $data['heading_title'] = $data['seo_h1'] = !empty($seo_meta['title']) ? $seo_meta['title'] : $this->config->get('config_title');
                $data['seo_h2'] = !empty($seo_meta['h2']) ? $seo_meta['h2'] : '';
                $data['seo_h3'] = !empty($seo_meta['h3']) ? $seo_meta['h3'] : '';
            } else {
                $this->data['heading_title'] = $this->data['seo_h1'] = !empty($seo_meta['title']) ? $seo_meta['title'] : $this->config->get('config_title');
                $this->data['seo_h2'] = !empty($seo_meta['h2']) ? $seo_meta['h2'] : '';
                $this->data['seo_h3'] = !empty($seo_meta['h3']) ? $seo_meta['h3'] : '';
            }
        }
        /* now defined in header ctrl
        $this->load->model('tool/seo_package');

        if ($this->config->get('mlseo_opengraph')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('opengraph', 'home'));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('opengraph', 'home'));
          }
        }

        if ($this->config->get('mlseo_tcard')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('tcard', 'home'));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('tcard', 'home'));
          }
        }

        if ($this->config->get('mlseo_gpublisher')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('gpublisher', 'home'));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('gpublisher', 'home'));
          }
        }
        */
        // end - seo meta

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

        $this->load->model('tool/image');
		$this->load->model('design/banner');

		$home_banners = $this->model_design_banner->getBanner(9);
		$data['home_banners'] = [];
        foreach ($home_banners as $home_banner) {
            $data['home_banners'][] = [
                'title' => $home_banner['title'],
                'href' => $home_banner['link'],
                'image' => $this->model_tool_image->resize($home_banner['image'], 1118, 535),
            ];
		}

        $manufacturers_banners = $this->model_design_banner->getBanner(6);
        $data['manufacturers_banners'] = [];
        foreach ($manufacturers_banners as $manufacturers_banner) {
            $data['manufacturers_banners'][] = [
                'href' => $manufacturers_banner['link'],
                'image' => $this->model_tool_image->resize($manufacturers_banner['image'], 180, 50),
            ];
        }

        $documents_banners = $this->model_design_banner->getBanner(8);
        $data['documents_banners'] = [];
        foreach ($documents_banners as $document_banner) {
            $data['documents_banners'][] = [
                'href' => $document_banner['link'],
                'image' => $this->model_tool_image->resize($document_banner['image'], 247, 355),
                'original' => '/image/' . $document_banner['image'],
            ];
        }
        $data['documents_banners'] = array_slice($data['documents_banners'], 0, 12);

        $this->load->model('catalog/product');
        $product_specials = $this->model_catalog_product->getProductSpecials([
            'start' => 0,
            'limit' => 12
        ]);
        $data['product_specials'] = $this->model_catalog_product->getPublicProducts($product_specials, [
            'image' => [
                'x' => 212,
                'y' => 185,
            ],
        ]);

        $data['categories_block'] = $this->load->controller('extension/module/category_grid', [
            'class' => 'frontpage__categories'
        ]);

        $data['products_tab'] = $this->load->controller('product/product_tab_refactored');
        $data['advantages_block'] = $this->load->controller('common/advantages_block');

        $data['discounted_link'] = $this->url->link('product/special');
        $data['installment_link'] = $this->url->link('information/information', 'information_id=171');
        $data['delivery_link'] = $this->url->link('information/information', 'information_id=93');
        $data['installation_link'] = $this->url->link('information/information', 'information_id=94');


		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}

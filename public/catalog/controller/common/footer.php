<?php
class ControllerCommonFooter extends Controller {
	public function index() {
	    $data = $this->getData();

		return $this->load->view('common/footer', $data);
	}

    public function creatium()
    {
        $data = $this->getData();

        return $this->load->view('common/footer_content', $data);
    }

    private function getData()
    {

        $data['oct_ultrastore_data'] = $oct_ultrastore_data = $this->config->get('theme_oct_ultrastore_data');

        $data['oct_lang_id'] = (int)$this->config->get('config_language_id');

        $data['oct_jscode'] = html_entity_decode($this->config->get('theme_oct_ultrastore_js_code'), ENT_QUOTES, 'UTF-8');

        $this->load->model('tool/image');

        $data['oct_customer_paymets'] = [];

        if (isset($oct_ultrastore_data['payments']['customers']) && !empty($oct_ultrastore_data['payments']['customers'])) {
            foreach ($oct_ultrastore_data['payments']['customers'] as $oct_c_payment) {
                if ((isset($oct_c_payment['status']) && $oct_c_payment['status'] == 'on') && isset($oct_c_payment['image']) && !empty($oct_c_payment['image']) && file_exists(DIR_IMAGE.$oct_c_payment['image'])) {
                    $data['oct_customer_paymets'][] = $this->model_tool_image->resize($oct_c_payment['image'], 52, 32);
                }
            }
        }

        $this->load->language('common/footer');

        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][] = array(
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $this->load->model('tool/telephone');

        $data['contact'] = $this->url->link('information/contact');
        $data['return'] = $this->url->link('account/return/add', '', true);
        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['tracking'] = $this->url->link('information/tracking');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
        $data['voucher'] = $this->url->link('account/voucher', '', true);
        $data['affiliate'] = $this->url->link('affiliate/login', '', true);
        $data['special'] = $this->url->link('product/special');
        $data['account'] = $this->url->link('account/account', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['newsletter'] = $this->url->link('account/newsletter', '', true);
        $data['telephone_text'] = $this->config->get('config_telephone');
        $data['telephone_number'] = $this->model_tool_telephone->parse($data['telephone_text']);
        if (isset($data['oct_ultrastore_data']['footer_link_contact']) && $data['oct_ultrastore_data']['footer_link_contact'] == 'on') {
            $data['informations'][] = array(
                'title' => $this->language->get('text_contact'),
                'href'  => $this->url->link('information/contact')
            );
        }

        if (isset($data['oct_ultrastore_data']['footer_link_return']) && $data['oct_ultrastore_data']['footer_link_return'] == 'on') {
            $data['informations'][] = array(
                'title' => $this->language->get('text_return'),
                'href'  => $this->url->link('account/return/add', '', true)
            );
        }

        if (isset($data['oct_ultrastore_data']['footer_link_sitemap']) && $data['oct_ultrastore_data']['footer_link_sitemap'] == 'on') {
            $data['informations'][] = array(
                'title' => $this->language->get('text_sitemap'),
                'href'  => $this->url->link('information/sitemap')
            );
        }

        if (isset($data['oct_ultrastore_data']['footer_link_man']) && $data['oct_ultrastore_data']['footer_link_man'] == 'on') {
            $data['informations'][] = array(
                'title' => $this->language->get('text_manufacturer'),
                'href'  => $this->url->link('product/manufacturer')
            );
        }

        if (isset($data['oct_ultrastore_data']['footer_link_cert']) && $data['oct_ultrastore_data']['footer_link_cert'] == 'on') {
            $data['informations'][] = array(
                'title' => $this->language->get('text_voucher'),
                'href'  => $this->url->link('account/voucher', '', true)
            );
        }

        if (isset($data['oct_ultrastore_data']['footer_link_specials']) && $data['oct_ultrastore_data']['footer_link_specials'] == 'on') {
            $data['informations'][] = array(
                'title' => $this->language->get('text_special'),
                'href'  => $this->url->link('product/special')
            );
        }

        if (isset($data['oct_ultrastore_data']['footer_category_links']) && !empty($data['oct_ultrastore_data']['footer_category_links'])) {
            $this->load->model('catalog/category');

            foreach ($data['oct_ultrastore_data']['footer_category_links'] as $category_id) {
                $category_info = $this->model_catalog_category->getOCTCategory($category_id);

                if ($category_info) {
                    $path = ($category_info['path']) ? $category_info['path'] . '_' . $category_info['category_id'] : $category_info['category_id'];

                    $data['categories'][] = array(
                        'name' => $category_info['name'],
                        'href'  => $this->url->link('product/category', 'path=' . $path, true)
                    );
                }
            }
        }

        if (isset($data['oct_ultrastore_data']['contact_open'][(int)$this->config->get('config_language_id')])){
            $oct_contact_opens = explode(PHP_EOL, $data['oct_ultrastore_data']['contact_open'][(int)$this->config->get('config_language_id')]);

            foreach ($oct_contact_opens as $oct_contact_open) {
                if (!empty($oct_contact_open)) {
                    $data['oct_contact_opens'][] = $oct_contact_open;
                }
            }
        }

        $oct_contact_telephones = explode(PHP_EOL, $data['oct_ultrastore_data']['contact_telephone']);

        foreach ($oct_contact_telephones as $oct_contact_telephone) {
            if (!empty($oct_contact_telephone)) {
                $data['oct_contact_telephones'][] = $oct_contact_telephone;
            }
        }

        $data['social'] = [
            "instagram" => "https://www.instagram.com/sdmclimate/?roistat_visit=470229",
            "youtube" => "https://www.youtube.com/channel/UCqV1Wso-dNFsRuCRSOR-jEw",
            "vk" => "https://vk.com/sdmclimate?roistat_visit=470229",
            "fb" => "https://www.facebook.com/sdmclimate1/?ref=page_internal&roistat_visit=470229",
        ];

        $data['menu_links'] = [
            [
                [
                    'name' => 'Бренды',
                    'href' => $this->url->link('product/manufacturer')
                ],
                [
                    'name' => 'Оформление заказа',
                    'href' => $this->url->link('checkout/cart')
                ],
                [
                    'name' => 'Оплата',
                    'href' => $this->url->link('information/information', 'information_id=14')
                ],
                [
                    'name' => 'Информация о доставке',
                    'href' => $this->url->link('information/information', 'information_id=93')
                ],
                [
                    'name' => 'Покупка в кредит',
                    'href' => $this->url->link('information/information', 'information_id=11')
                ],
                [
                    'name' => 'Рассрочка на товары',
                    'href' => $this->url->link('product/installments')
                ],
            ],

            [
                [
                    'name' => 'Гарантия и возврат',
                    'href' => $this->url->link('information/information', 'information_id=10')
                ],
                [
                    'name' => 'Сервисный центр',
                    'href' => $this->url->link('information/information', 'information_id=7')
                ],
                [
                    'name' => 'Монтаж',
                    'href' => $this->url->link('information/information', 'information_id=94')
                ],
                [
                    'name' => 'Полезная информация',
                    'href' => 'https://sdmclimate.ru/blog'
                ],
                [
                    'name' => 'Программа лояльности',
                    'href' => $this->url->link('information/information', 'information_id=173')
                ],
            ],

            [
                [
                    'name' => 'Кто мы',
                    'href' => $this->url->link('information/information', 'information_id=4')
                ],
                [
                    'name' => 'Партнерам',
                    'href' => $this->url->link('information/information', 'information_id=170')
                ],
                [
                    'name' => 'Контакты',
                    'href' => $this->url->link('information/contact')
                ],
                [
                    'name' => 'Договор-оферта',
                    'href' => $this->url->link('information/information', 'information_id=74')
                ],
                [
                    'name' => 'Политика обработки персональных данных',
                    'href' => $this->url->link('information/information', 'information_id=3')
                ],
            ],
        ];

        $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        // Whos Online
        if ($this->config->get('config_customer_online')) {
            $this->load->model('tool/online');

            if (isset($this->request->server['REMOTE_ADDR'])) {
                $ip = $this->request->server['REMOTE_ADDR'];
            } else {
                $ip = '';
            }

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
        }

        $data['home'] = $this->url->link('common/home');

        $data['scripts'] = $this->document->getScripts('footer');

        if ($this->config->get('analytics_oct_analytics_status') && $this->config->get('analytics_oct_analytics_position') == 1) {
            $data['analytics'] = $this->load->controller('extension/analytics/oct_analytics', $this->config->get('analytics_oct_analytics_status'));
        }

        $data['mix'] = new Mix();

        return $data;
    }
}

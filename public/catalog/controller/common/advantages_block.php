<?php
class ControllerCommonAdvantagesBlock extends Controller {
    public function index() {
        $data['certificate_link'] = $this->url->link('information/information', 'information_id=9');
        $data['installation_link'] = $this->url->link('information/information', 'information_id=94');
        $data['credit_program_link'] = $this->url->link('information/information', 'information_id=11');
        $data['warranty_link'] = $this->url->link('information/information', 'information_id=10');

        return $this->load->view('common/advantages_block', $data);
    }
}

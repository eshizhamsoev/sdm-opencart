<?php
class ControllerApiB24 extends Controller{
	public function __construct( $registry ){
		parent::__construct($registry);
        $this->load->model('setting/setting');
        $b24_setting = $this->model_setting_setting->getSetting('b24_hook_key');
		$this->b24->setFields($b24_setting);
	}
	public function index(){
        $this->load->model('setting/setting');
        $setting = $this->model_setting_setting->getSetting('b24_out_hooks');
	    if (isset($this->request->server['REQUEST_METHOD']) && 'POST' == $this->request->server['REQUEST_METHOD']) {
			$post = $this->request->post;
			$event = !empty($post['event']) ? $post['event'] : '';
			$token = !empty($post['auth']['application_token']) ? $post['auth']['application_token'] : '';
			/*���� ��������� ����� � ���� ��� ��� ������ � �� ��������� � ���������*/		
			if (!empty($setting) && isset($setting['b24_out_hooks_' . $event]) && ($setting['b24_out_hooks_' . $event] == $token)) {
				$this->$event($post['data']);
			}
		}	
	}
	/*�������� �����*/
	protected function ONCRMINVOICEADD($data){
	}
	
	/*���������� �����*/
	protected function ONCRMINVOICEUPDATE($data){
	}
	/*�������� �����*/
	protected function ONCRMINVOICEDELETE($data){
	}
	/*���������� ������� �����*/
	protected function ONCRMINVOICESETSTATUS($data){
	}
	/*�������� ����*/
	protected function ONCRMLEADADD($data){
	}
	/*���������� ����*/
	protected function ONCRMLEADUPDATE($data){	
		$this->load->model('checkout/order');
		$b24_order_id = $data['FIELDS']['ID'];		
		$oc_order_id = $this->getOrderById($b24_order_id);
		$oc_order = $this->model_checkout_order->getOrder($oc_order_id);
        $params = [
            'type' => 'crm.lead.list',
            'params' => [
				'filter' => ["ID" => $b24_order_id]
            ]
        ];		
		$result = $this->b24->callHook($params);			
		$b24_order = $result['result'][0];
		if ($b24_order['STATUS_ID'] != 'CONVERTED'){
			$order_status_id = $this->getOrderStatusById($b24_order['STATUS_ID']);	
		}
		else{
			$order_status_id = $this->getOrderStatusById('NEW');
			$params = [
				'type' => 'crm.deal.list',
				'params' => [
					'filter' =>  ["LEAD_ID" => $b24_order_id] 
				]
			];		
			$result_new = $this->b24->callHook($params);
			$new_deal_id = $result_new['result'][0]['ID'];
			$this->updateIdLeadToDeal($b24_order_id, $new_deal_id);
		}
		if ($oc_order['order_status_id'] != $order_status_id){
			$this->model_checkout_order->addOrderHistory($oc_order_id, $order_status_id);
		}			
	}
	/*�������� ����*/
	protected function ONCRMLEADDELETE($data){
	}
	/*�������� ������*/
	protected function ONCRMDEALADD($data){
		add2Log(array('ONCRMDEALADD'=>true));
		add2Log($data);
	}
	
	/*���������� ������*/
	protected function ONCRMDEALUPDATE($data){
		$this->load->model('checkout/order');
		$b24_order_id = $data['FIELDS']['ID'];
		$oc_order_id = $this->getOrderById($b24_order_id);
		$oc_order = $this->model_checkout_order->getOrder($oc_order_id);
	    $params = [
            'type' => 'crm.deal.get',
            'params' => [
				'id' => $b24_order_id
            ]
        ];	
		$result = $this->b24->callHook($params);		
		$b24_order = $result['result'];
		$order_status_id = $this->getOrderStatusByStage($b24_order['STAGE_ID']);
		if ($oc_order['order_status_id'] != $order_status_id){
			$this->model_checkout_order->addOrderHistory($oc_order_id, $order_status_id);
		}
	}
	/*�������� ������*/
	protected function ONCRMDEALDELETE($data){
		add2Log(array('ONCRMDEALDELETE'=>true));
		add2Log($data);
	}
	/*�������� ��������*/
	protected function ONCRMCOMPANYADD($data){
	}
	/*���������� ��������*/
	protected function ONCRMCOMPANYUPDATE($data){
	}
	
	/*�������� ��������*/
	protected function ONCRMCOMPANYDELETE($data){
	}
	/*�������� ��������*/
	protected function ONCRMCONTACTADD($data){
		$params = [
			'type' => 'crm.contact.get',
			'params' => [
				'id' => $data['FIELDS']['ID']
			]
		];
		$result = $this->b24->callHook($params);
		$contact = $result['result'];
		//$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		//$password = sha1($salt . sha1($salt . sha1($result['result']['UF_CRM_1517393208530'])));
		if (isset($contact['PHONE'])){
			$phone = preg_replace("/[^0-9]/", '', $contact['PHONE'][0]['VALUE']);
			$oc_customer_id	= $this->getCustomerByPhone($phone);
			if ($oc_customer_id){
				$this->db->query("UPDATE `b24_customer` SET `b24_contact_id` = '" . (int)$data['FIELDS']['ID'] . "', `phone` = '" . $phone . "', `b24_contact_field` = '" . json_encode($contact) . "' WHERE `oc_customer_id` = '" . (int)$oc_customer_id . "'");
				
				$this->db->query("UPDATE `oc_customer` SET `firstname`='" . $contact['NAME'] . " " . $contact['SECOND_NAME'] . "', `lastname`='".$contact['LAST_NAME']."', `email`='".$contact['EMAIL'][0]['VALUE']."', `phone` = '" . $phone . "' WHERE `customer_id` = '" . $oc_customer_id . "'");
			} else {
				$this->db->query("INSERT INTO ".DB_PREFIX."customer (`customer_group_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `status`, `approved`, `safe`, `date_added`) VALUES ('1', '1', '".$result['result']['NAME']." ".$result['result']['SECOND_NAME']."', '".$result['result']['LAST_NAME']."', '".$result['result']['EMAIL'][0]['VALUE']."', '".$phone."', '1', '1', '0', '".$result['result']['DATE_CREATE']."');");
				$get_last_customer_id = $this->db->getLastId();
				$this->db->query("INSERT INTO `b24_customer`(`oc_customer_id`, `b24_contact_id`, `phone`, `b24_contact_field`) VALUES ('".(int)$get_last_customer_id."', '" . (int)$data['FIELDS']['ID']. "', '" . $phone . "', '" . json_encode($contact) . "')");
			}
			} else{
				$this->db->query("INSERT INTO ".DB_PREFIX."customer (`customer_group_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `status`, `approved`, `safe`, `date_added`) VALUES ('1', '1', '".$result['result']['NAME']." ".$result['result']['SECOND_NAME']."', '".$result['result']['LAST_NAME']."', '".$result['result']['EMAIL'][0]['VALUE']."', '', '1', '1', '0', '".$result['result']['DATE_CREATE']."');");
				$get_last_customer_id = $this->db->getLastId();
				$this->db->query("INSERT INTO `b24_customer`(`oc_customer_id`, `b24_contact_id`, `phone`, `b24_contact_field`) VALUES ('".(int)$get_last_customer_id."', '" . (int)$data['FIELDS']['ID']. "', '', '" . json_encode($contact) . "')");
		}
	}
	/*���������� ��������*/
	protected function ONCRMCONTACTUPDATE($data){
		$params = [
			'type' => 'crm.contact.get',
			'params' => [
				'id' => $data['FIELDS']['ID']
			]
		];
		$result = $this->b24->callHook($params);
		$contact = $result['result'];
		//$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		//$password = sha1($salt . sha1($salt . sha1('pasword')));
		if (isset($contact['PHONE'])){
			$phone = preg_replace("/[^0-9]/", '', $contact['PHONE'][0]['VALUE']);
			$this->db->query("UPDATE `b24_customer` SET `phone` = '" . $phone . "', `b24_contact_field` = '" . json_encode($contact) . "' WHERE `b24_contact_id` = '" . (int)$contact['ID'] . "'");
			$query = $this->db->query("SELECT * FROM `b24_customer` WHERE b24_contact_id = '" . (int)$contact['ID'] . "'");
			foreach ($query->rows as $row) {
				$oc_customer_id = $row['oc_customer_id'];
			}
			$this->db->query("UPDATE ".DB_PREFIX."customer SET `firstname`='".$result['result']['NAME']." ".$result['result']['SECOND_NAME']."', `lastname`='".$result['result']['LAST_NAME']."', `email`='".$result['result']['EMAIL'][0]['VALUE']."',`fax`='', `telephone` = '" . $phone . "' WHERE `customer_id` = '".(int)$oc_customer_id."'");
		}
	}
	/*�������� ��������*/
	protected function ONCRMCONTACTDELETE($data){
		$query = $this->db->query("SELECT * FROM `b24_customer` WHERE b24_contact_id = '" . (int)$data['FIELDS']['ID'] . "'");
		foreach ($query->rows as $row) {
			$oc_customer_id = $row['oc_customer_id'];
		}
		$this->db->query("DELETE FROM `b24_customer` WHERE 	`b24_contact_id` = '" . (int)$data['FIELDS']['ID'] . "'");
		$this->db->query("DELETE FROM ".DB_PREFIX."customer WHERE 	`customer_id` = '" . (int)$oc_customer_id . "'");
	}
	/*�������� ������*/
	protected function ONCRMCURRENCYADD($data){
	}
	
	/*���������� ������*/
	protected function ONCRMCURRENCYUPDATE($data){
	}
	/*�������� ������*/
	protected function ONCRMCURRENCYDELETE($data){
	}
	/*�������� ������*/
	protected function ONCRMPRODUCTADD($data){
	}
	/*���������� ������*/
	protected function ONCRMPRODUCTUPDATE($data){
	}
	/*�������� ������*/
	protected function ONCRMPRODUCTDELETE($data){
	}
	/*�������� ����*/
	protected function ONCRMACTIVITYADD($data){
	}
	/*���������� ����*/
	protected function ONCRMACTIVITYUPDATE($data){
	}
	/*�������� ����*/
	protected function ONCRMACTIVITYDELETE($data){
	}
	
	private function getOrderById($order_id) {
		if (abs($order_id ) <= 0) {
		    trigger_error('ID must be integer', E_USER_WARNING);
		}
		$query = $this->db->query("SELECT oc_order_id FROM b24_order WHERE b24_order_id = '" . (int)($order_id ) . "'");
		return $query->row['oc_order_id'];
	}
	private function getOrderStatusById($order_status_id) {
		$query = $this->db->query("SELECT oc_status_id FROM b24_status_map WHERE b24_status_id = '" . $order_status_id . "'");
		return $query->row['oc_status_id'];
	}
	private function getOrderStatusByStage($order_status_id) {
		$query = $this->db->query("SELECT oc_status_id FROM b24_status_map WHERE b24_stage_id = '" . $order_status_id . "'");
		return $query->row['oc_status_id'];
	}
	private function getCustomerByPhone($phone){
		$query = $this->db->query("SELECT * FROM b24_customer WHERE phone = '" . $phone . "'");
		return ($query->num_rows) ? $query->row['oc_customer_id'] : false;
	}
	
	// ����� � ������� 24 ������� ������ � �� � ������ OLD
	public function delete()
    {
        $this->load->model('setting/setting');
	    if (isset($this->request->server['REQUEST_METHOD']) && 'POST' == $this->request->server['REQUEST_METHOD']) {
	        $postFields = $this->request->post;
            if (isset($postFields['auth'])) {
                $application_token = !empty($postFields['auth']['application_token']) ? $postFields['auth']['application_token'] : '';
                $b24_hook_key_delete = '';
                $b24_setting = $this->model_setting_setting->getSetting('b24_hook_key');
                if (!empty($b24_setting) && isset($b24_setting['b24_hook_key_delete'])) {
                    $b24_hook_key_delete = $b24_setting['b24_hook_key_delete'];
                }
                // hard-code
                if ($b24_hook_key_delete == $application_token) {
                    if (isset($postFields['data']['FIELDS']['ID'])) {
                        $b24_product_id = !empty($postFields['data']['FIELDS']['ID']) ? (int)$postFields['data']['FIELDS']['ID'] : 0;
                        $findCountQuery = 'SELECT COUNT(`p1`.`id`) `findcount`'
                                        . ' FROM `b24_product` `p1`, `b24_product` `p2`'
                                        . ' WHERE `p1`.`oc_product_id` = `p2`.`oc_product_id`'
                                        . ' AND `p2`.`b24_product_id` = ' . $b24_product_id;
                        $findCount = $this->db->query($findCountQuery);
                        if (0 < $findCount->num_rows) {
                            if (1 < $findCount->row['findcount']) {
                                $sql = 'DELETE FROM `b24_product` WHERE `b24_product_id` = ' . $b24_product_id;
                                $this->db->query($sql);
                            } else {
                                $sql = 'UPDATE `' . DB_PREFIX . 'product` `p`'
                                     . ' INNER JOIN `b24_product` ON `p`.`product_id` = `b24_product`.`oc_product_id`'
                                     . ' SET `p`.`status` = 0'
                                     . ' WHERE `b24_product`.`b24_product_id` = ' . $b24_product_id;
                                $this->db->query($sql);
                                $sql = 'DELETE FROM `b24_product` WHERE `b24_product_id` = ' . $b24_product_id;
                                $this->db->query($sql);
                            }
                        } else {
                            $sql = 'UPDATE `' . DB_PREFIX . 'product` `p`'
                                 . ' INNER JOIN `b24_product` ON `p`.`product_id` = `b24_product`.`oc_product_id`'
                                 . ' SET `p`.`status` = 0'
                                 . ' WHERE `b24_product`.`b24_product_id` = ' . $b24_product_id;
                            $this->db->query($sql);
                            $sql = 'DELETE FROM `b24_product` WHERE `b24_product_id` = ' . $b24_product_id;
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
	}
	
	private function updateIdLeadToDeal($b24_order_id, $new_deal_id){
		$query = $this->db->query("UPDATE b24_order set b24_order_id = '" . $new_deal_id . "' WHERE b24_order_id = '" . $b24_order_id . "'");
	}
}
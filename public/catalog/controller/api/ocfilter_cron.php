<?php

/**
 * Class ControllerApiOcfilter
 * @author fonclub
 * @created 13.02.2020
 */
class ControllerApiOcfilterCron extends Controller {
    protected $api_token = 'dgf34tg@45dfg45tyww';

    /**
     * метод для копирования фильтров по крону
     */
    public function copy_filter() {
        /** проверка доступа */
        if(!isset($this->request->get['api_token']) or $this->request->get['api_token'] !== $this->api_token){
            exit('access denied');
        }

        /** настройки импорта */
        $options = [
            'copy_type' => 'checkbox', // тип скопированных фильтров
            'copy_status' => -1, // статус скопированных атрибутов (-1 - автоматически)
            'copy_attribute' => 1, // копировать атрибуты
            'attribute_separator' => '', // разделитель атрибутов
            'copy_filter' => 0, // копировать стандартные фильтры
            'copy_option' => 1, // копировать опции
            'copy_truncate' => 1, // очистить существующие фильтры OCFilter
            'copy_category' => 0, // привязать фильтры к категориям
        ];

        /** используем ocmod, если есть */
        if(file_exists(DIR_MODIFICATION . '/admin/model/extension/ocfilter.php'))
            require_once DIR_MODIFICATION . '/admin/model/extension/ocfilter.php';
        else
            require_once DIR_CATALOG . '/../admin/model/extension/ocfilter.php';

        $model = new ModelExtensionOCFilter($this->registry);
        $model->copyFilters($options);
        $this->cache->delete('*');

        echo 'operation end';
    }
}

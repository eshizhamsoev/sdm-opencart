<?php

class ControllerCreatiumCreatium extends Controller
{
    public function index(int $information_id): bool
    {
        $link = $this->getLink($information_id);

        if (empty($link)) {
            return false;
        }

        $html = $this->creatiumEmbed($link);

        $html = preg_replace_callback('/<body (.*?)>/', function($data) {
            return $data[0] . $this->load->controller('common/header/creatium');
        }, $html);
//
        $html = str_replace('</body>', $this->load->controller('common/footer/creatium') . '</body>', $html);

        $this->response->setOutput($html);

        return true;
    }

    private function getLink(int $information_id): string
    {
        switch ($information_id) {
            case 94:
                $link = "https://b382ae.creatium.site/";
                break;
            default:
                $link = "";
        }

        return $link;
    }

    public function creatiumEmbed($url) {
        $ch = curl_init($url);

        $embed = '1';
        if (isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI'])) {
            $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';
            $embed = "$protocol://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }

        $headers = ["X-Creatium-Embed: $embed"];

        // Передаем Cookie посетителя
        $cookies = [];
        foreach ($_COOKIE as $key => $value) {
            $cookies[] = $key . '=' . urlencode($value);
        }
        $headers[] = 'Cookie: ' . implode(';', $cookies);

        // Передаем IP адрес посетителя
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $headers[] = "X-Forwarded-For: $_SERVER[REMOTE_ADDR]";
        }

        foreach ($_SERVER as $key => $value) {
            if ($key === 'HTTP_HOST' || $key === 'HTTP_COOKIE') continue;
            else if (strpos($key, 'HTTP_') === 0) {

                // Преобразование строк типа HTTP_KEY_EXAMPLE в Key-Example
                $header = implode('-', array_map(
                    'ucfirst', array_slice(explode('_', strtolower($key)), 1)
                ));

                $headers[] = "$header: $value";
            }
        }

//        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($ch, $header) {
//            $length = strlen($header);
//
//            if (strpos($header, ': ') > 0) {
//                $pass = true;
//
//                // Если кодирование отличается, ошибка возникает
//                if (strpos(strtolower($header), 'transfer-encoding') === 0) $pass = false;
//
//                if ($pass) header($header);
//            }
//
//            return $length;
//        });

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            print curl_error($ch);
        }

        curl_close($ch);

        return gzdecode($response);
    }
}

<?php
class ControllerExtensionPaymentKupivkredit1 extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_back'] = $this->language->get('button_back');
		$data['action'] =  $this->url->link('extension/payment/kupivkredit1/success');
		$data['continue'] = $this->url->link('checkout/success');
		$this->load->language('extension/payment/kupivkredit1');
		if($this->config->get('payment_kupivkredit1_server')) {
		    $data['text_server'] = 'https://loans.tinkoff.ru/api/partners/v1/lightweight/create';
        } else {
		    $data['text_server'] = 'https://loans-qa.tcsbank.ru/api/partners/v1/lightweight/create';
        }


		$data['text_title'] = $this->language->get('text_title');
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$items = array();
        if (isset($this->session->data['shipping_method']) && $this->session->data['shipping_method']['cost'] != 0) {
            $i = 1;
            $items[] = array(
                'itemName_0' => $this->session->data['shipping_method']['title'],
                'itemQuantity_0' => 1,
                'itemCategory_0' => 'Доставка',
                'itemPrice_0' => (int)$this->session->data['shipping_method']['cost']
            );
        } else {
        	$i = 0;
		}
		foreach ($this->cart->getProducts() as $product) {
			$option_data = '';
			foreach ($product['option'] as $option) {
				$option_data .= " ".$option['name'].": ".$option['value'];
			}
			if($option_data) $option_data = " (".$option_data." )";
			$items[] = array(
				'itemName' . '_' . $i => htmlspecialchars_decode($product['name'], ENT_QUOTES).$option_data,
				'itemCategory' . '_' . $i => $this->getCategoryByProductId($product['product_id']),
				'itemQuantity' . '_' . $i => $product['quantity'],
				'itemPrice' . '_' . $i => $this->currency->format($product['price'], $order_info['currency_code'], false, false),
			);
			$i++;
		}
		$data['items'] = $items;
		if ($this->customer->isLogged()) {
			$firstname=str_replace(",", "", $this->customer->getFirstName());
			$names=explode(" ", $firstname);
			if(isset($names[0]))$firstname=$names[0];
			$details =   array (
				'customerEmail' => $this->customer->getEmail(),
				'customerPhone' => $this->customer->getTelephone(),
			);
		} else {
			$firstname=str_replace(",", "", $this->session->data['guest']['firstname']);
			$names=explode(" ", $firstname);
			if(isset($names[0]))$firstname=$names[0];
			$details =   array (
				'customerEmail' => $this->session->data['guest']['email'],
				'customerPhone' => $this->session->data['guest']['telephone'],
			);
		}
		if ($data['text_server'] == 'https://loans.tinkoff.ru/api/partners/v1/lightweight/create') {
			$order = array (
				'details'        => $details,
				'shopId'      => $this->config->get('payment_kupivkredit1_merch_z'),
				'orderNumber' => $this->session->data['order_id']
			);
		} else {
			$order = array (
				'details'        => $details,
				'shopId'      => $this->config->get('payment_kupivkredit1_merch_z'),
				'orderNumber' => 'test_order_'.uniqid()
			);
		}
		$data['order'] = $order;
		$data['sum'] = (int)$order_info['total'];
		$data['promoCode'] = $this->config->get('payment_kupivkredit1_promo_сode');
        
        return $this->load->view('extension/payment/kupivkredit1', $data);
	}
	
	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'kupivkredit1') {
			$this->load->language('extension/payment/kupivkredit1');
			$this->load->model('checkout/order');
			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_kupivkredit1_order_status_id'));
		}
	}
	
	private function getCategoryByProductId($product_id) {
		$query = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category p LEFT JOIN " . DB_PREFIX . "category c ON (p.category_id = c.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE p.product_id = '".(int)$product_id."' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		return $query->row['name'];
	}
}
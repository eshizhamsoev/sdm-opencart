<?php
class ControllerExtensionPaymentKupilegko extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_back'] = $this->language->get('button_back');

		$data['action'] =  $this->url->link('extension/payment/kupilegko/success');
		$data['continue'] = $this->url->link('checkout/success');

		$this->load->language('extension/payment/kupilegko');

		$data['text_server'] = 'https://anketa.alfabank.ru/alfaform-pos/endpoint';


		$data['text_title'] = $this->language->get('text_title');

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$items = array();

        if (isset($this->session->data['shipping_method']) && $this->session->data['shipping_method']['cost'] != 0) {
            $i = 1;

            $items[] = array(
                'itemName_0' => $this->session->data['shipping_method']['title'],
                'itemQuantity_0' => 1,
                'itemModel_0' => 'Delivery',
                'itemCategory_0' => 'Доставка',
                'itemPrice_0' => (int)$this->session->data['shipping_method']['cost']
            );
        } else {
        	$i = 0;
		}
		foreach ($this->cart->getProducts() as $product) {
			$option_data = '';

			foreach ($product['option'] as $option) {
				$option_data .= " ". $option['name'] . ": " . $option['value'];
			}

			if($option_data) { 
				$option_data = " (" . $option_data . " )";
			}

			$items[] = array(
				'itemName' . '_' . $i => htmlspecialchars_decode($product['name'], ENT_QUOTES) . $option_data,
				'itemModel' . '_' . $i => $product['model'] . '_' . $product['product_id'],
				'itemCategory' . '_' . $i => $this->getCategoryByProductId($product['product_id']),
				'itemQuantity' . '_' . $i => $product['quantity'],
				'itemPrice' . '_' . $i => $this->currency->format($product['price'], $order_info['currency_code'], false, false),
			);

			$i++;
		}

		$data['items'] = $items;

		if ($this->customer->isLogged()) {
			$firstname = str_replace(",", "", $this->customer->getFirstName());
			$lastname = str_replace(",", "", $this->customer->getLastName());
			$names = explode(" ", $firstname);

			if(isset($names[0])) {
				$firstname = $names[0];
			}

			$details =   array (
				'customerFirstname' => $firstname,
				'customerLastname' => $lastname,
				'customerEmail' => $this->customer->getEmail(),
				'customerPhone' => $this->phone($this->customer->getTelephone()),
			);
		} else {
			$firstname = str_replace(",", "", $this->session->data['guest']['firstname']);

			$names = explode(" ", $firstname);

			if(isset($names[0])) { 
				$firstname = $names[0];
			}

			if(!empty($this->session->data['guest']['lastname'])){

			
				$lastname = str_replace(",", "", $this->session->data['guest']['lastname']);

				$names = explode(" ", $lastname);

				if(isset($names[0])) { 
					$lastname = $names[0];
				}
			}else{
				$lastname = '';
			}

			$details =   array (
				'customerFirstname' => $firstname,
				'customerLastname' => $lastname,
				'customerEmail' => $this->session->data['guest']['email'],
				'customerPhone' => $this->phone($this->session->data['guest']['telephone']),
			);
		}
		
		$order = array (
			'details'        => $details,
			'shopId'      => $this->config->get('payment_kupilegko_merch_z'),
			'orderNumber' => $this->session->data['order_id']
		);


		$data['order'] = $order;
		$data['sum'] = (int)$order_info['total'];
        
        return $this->load->view('extension/payment/kupilegko', $data);
	}
	
	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'kupilegko') {
			$log = new Log('payment.log');

			$log->write('ALFA NEW ORDER: id => ' . $this->session->data['order_id']);

			$this->load->language('extension/payment/kupilegko');

			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_kupilegko_order_status_id'));
		}
	}
	
	private function getCategoryByProductId($product_id) {
		$list = [
			1 => 'AIR_CONDITIONER',
			2 => 'AUDIO_TECHNICS',
			3 => 'BIG_INTERIOR_FACILITY',
			4 => 'BUILDING_IMPLEMENTS',
			5 => 'BUILDING_MATERIALS',
			6 => 'CAR_ACCESSORY',
			7 => 'COMPUTERS_ACCESSORIES_AND_SUPPLIES',
			8 => 'COTTAGE_TECHNICS',
			9 => 'ELECTRIC_GOODS',
			10 => 'FUR_LATHER_CLOTHES',
			11 => 'FURNITURE',
			12 => 'OTHER_CLOTHES',
			13 => 'PHOTO_AND_VIDEO_GOODS',
			14 => 'SANITARY_ENGINEERIN',
			15 => 'SERVICES',
			16 => 'SMALL_INTERIOR_FACILITY',
			17 => 'SPORTING_GOODS',
			18 => 'TELEPHONES_AND_ACCESSORIES',
			19 => 'TV_AND_VIDEO',
			20 =>'WINDOWS_AND_DOORS',
			21 =>'NOTEBOOK',
			22 =>'PLASMA_TV',
			23 =>'CARDS',
			24 =>'TOYS',
			25 =>'MISCELLANEOUS',
			26 =>'JEWELRY',
			27 =>'COTTAGES',
			28 =>'HUNTING_AND_FISHING',
			29 =>'EDUCATION',
			30 => 'Medicine',
			31 => 'Cosmetics',
			32 => 'motor_products',
			33 => 'Tourism'
		];
		$query = $this->db->query("
			SELECT 
				cd.name,
				c.category_id
			FROM 
				" . DB_PREFIX . "product_to_category p 
			LEFT JOIN 
				" . DB_PREFIX . "category c ON (p.category_id = c.category_id) 
			LEFT JOIN 
				" . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
			LEFT JOIN 
				" . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
			WHERE 
				p.product_id = '".(int)$product_id."' 
			AND 
				cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
			AND 
				c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' 
			AND 
				c.status = '1'");

		foreach($query->rows as $row){
			if(!empty($this->config->get('payment_kupilegko_category')[$row['category_id']])){
				return $list[$this->config->get('payment_kupilegko_category')[$row['category_id']]];
			}
			break;
		}

		return 'UNDEFINED';
	
	}

	private function phone($phone) {
    	$phone = preg_replace("/[^0-9]/", "", $phone);

	    if (strlen($phone) === 11) {
	      $phone = preg_replace("/^7/", "", $phone);
	    }

    	return $phone;
  	}
  
  	public function response(){
  		$log = new Log('payment.log');

  		$this->response->addHeader('Content-Type: application/json');

  		// VVV Дичь какая-то? УБрать бы это VVV
  		global $light_page_cache;
		$light_page_cache->status = 0;

  		$error = array();

  		if(isset($this->request->server['CONTENT_TYPE']) && $this->request->server['CONTENT_TYPE'] == 'application/json'){
  			$body = file_get_contents('php://input');
			$data = json_decode($body, true);

			if(isset($data['appId'])){
				if(isset($data['currentStatus'])){ 
					/*
					switch($data['currentStatus']){
						case 'appDraft': {
							$status = 'Черновик заявки. Клиент прервал заполнение анкеты, сессия истекла.';
							break;
						},
						case 'appCallCenter': {
							$status = 'Короткая заявка. Заказан звонок, с клиентом свяжется сотрудник банка для дооформления.';
							break;
						},
						case 'appPrelimAccept': {
							$status = 'Заявка предварительна одобрена';
							break;
						},
						case 'appDecline': {
							$status = 'Заявка отказана скорингом.';
							break;
						},
						case 'appSmsSignFail': {
							$status = 'СМС-кредит. Неудачное подписание кредитных документов по смс(клиент ввел неверный код подписания кредитных документов из смс).';
							break;
						},
						case 'appContractSignedBySms': {
							$status = 'СМС-кредит. Кредитные документы подписаны. Покупка подтверждена.';
							break;
						},
						case 'appContractRejectedByClient': {
							$status = 'СМС-кредит. Клиент отказался от подписания кредитных документов.';
							break;
						},
						case 'appFinalComplete': {
							$status = 'Предоставлено. Денежные средства перечислены в интернет-магазин.';
							break;
						},
						case 'appOperatorReject': {
							$status = 'Заявка отказана автоматическим процессом или оператором на точке подписания.';
							break;
						},
						case 'appComplete': {
							$status = 'Подтверждение покупки. Кредитные документы подписаны клиентом.';
							break;
						},
						case 'appOverdue': {
							$status = 'Заявка просрочена. Истек срок жизни заявки.';
							break;
						},
						case 'appTechAutomaticProcess': {
							$status = 'Автоматические процессы –срок жизни заявки не истек.';
							break;
						},
						case 'appScoring': {
							$status = 'Завершен ввод полной заявки в интернет-анкете. Заявка направлена на предварительный скоринг.';
							break;
						},
						case 'appSmsCreditEnabled': {
							$status = 'СМС-кредит. СМС-кредит доступен для клиента по данной заявке. Заявка заполняется.';
							break;
						}
					}
					*/

				}

				$this->model_checkout_order->addOrderHistory((int)$data['reference'], $this->config->get('payment_kupilegko_order_status_id'), $data['currentStatusDescription']);

				$response = array('appId' => $data['appId']);
				$log->write('ALFA RESPONSE: Success');
			}else{
				$log->write('ALFA ERROR: no appId in array');
				$error[] = 'no appId in array';
			}
			$log->write($data);
  		}else{
  			$log->write('ALFA ERROR: no \'application/json\' content-type');
  			$error[] = 'no \'application/json\' content-type';
  		}

  		if(!$error){
			$this->response->setOutput(json_encode($response));
  		}else{
  			$this->response->setOutput(json_encode($error));
  		}
  	}
  	
}
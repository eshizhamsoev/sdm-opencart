<?php
/**
 * Class ControllerExtensionModuleInstallments
 *
 * @module   Товары в рассрочку
 * @author   fonclub
 * @created  19.12.2019 11:52
 */

class ControllerExtensionModuleInstallments extends Controller
{
    protected $error;
    public function index($setting)
    {
        static $module = 0;

        if ($setting['status'] == 'on') {
            // Load model
            $this->load->model('extension/module/installments');
            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            $data['heading_title'] = (isset($setting['heading'][(int)$this->config->get('config_language_id')])
                && ! empty($setting['heading'][(int)$this->config->get('config_language_id')]))
                ? $setting['heading'][(int)$this->config->get('config_language_id')]
                : $this->language->get('heading_title');


            $data['position'] = isset($setting['position']) ? $setting['position'] : '';
            $data['is_carousel'] = isset($setting['is_carousel']) ? (boolean) $setting['is_carousel'] : false;


            if (empty($setting['limit'])) {
                $setting['limit'] = 25;
            }

            $filter_data = [
                'sort'               	=> (isset($setting['sort']) && !empty($setting['sort'])) ? $setting['sort'] : 'pd.name',
                'order'              	=> (isset($setting['order']) && !empty($setting['order'])) ? $setting['order'] : 'ASC',
                'quantity_view'         => (isset($setting['quantity_view']) && !empty($setting['quantity_view'])) ? false : true,
                'start'					=> 0,
                'limit'					=> (int)$setting['limit']
            ];

            $products = $this->model_extension_module_installments->getProducts($filter_data);

            $data['products'] = [];

            if (!empty($products)) {
                
                $oct_product_stickers = [];
                $data['sticker_colors'] = [];

                if ($this->config->get('oct_stickers_status')) {
                    $oct_stickers = $this->config->get('oct_stickers_data');

                    $data['oct_sticker_you_save'] = false;

                    if ($oct_stickers) {
                        $data['oct_sticker_you_save'] = isset($oct_stickers['stickers']['special']['persent']) ? true : false;
                    }

                    $this->load->model('octemplates/stickers/oct_stickers');
                }
                
                foreach ($products as $product) {
                    $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                    if ($product_info) {
                        if (isset($oct_stickers)) {
                            $oct_stickers_data = $this->model_octemplates_stickers_oct_stickers->getOCTStickers($product_info);

                            $oct_product_stickers = [];

                            if ($oct_stickers_data) {
                                $oct_product_stickers = $oct_stickers_data['stickers'];
                                $data['sticker_colors'][] = $oct_stickers_data['sticker_colors'];
                            }
                        }
                        
                        $width = (isset($setting['width']) && !empty($setting['width'])) ? $setting['width'] : $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width');
                        $height = (isset($setting['height']) && !empty($setting['height'])) ? $setting['height'] : $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height');

                        if ($product_info['image'] && file_exists(DIR_IMAGE.$product_info['image'])) {
                            $image = $this->model_tool_image->resize($product_info['image'], $width, $height);
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', $width, $height);
                        }

					$deviant = false;
					if ($product_info['quantity'] < 1) $deviant = 'call_stock';

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						if ($product_info['price'] > 0) {
							$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						} else {
							$price = false;
							$deviant = 'call_price';
						}
					} else {
						$price = false;
					}

					if ($product_info['stock_status_id'] == 9) $deviant = 'call_analog';

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

                        if ($this->config->get('config_tax')) {
                            $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                        } else {
                            $tax = false;
                        }

                        if ($this->config->get('config_review_status')) {
                            $rating = (int)$product_info['rating'];
                        } else {
                            $rating = false;
                        }

					$data['products'][] = [
						'deviant'  => $deviant,
						'product_id'  => $product_info['product_id'],
                        'oct_stickers'  => $oct_product_stickers,
                        'you_save'  	=> $product_info['you_save'],			
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(trim(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
						//villarasa
						'options' 		=> $this->model_catalog_product->getProductOptions($product_info['product_id']),
						//villarasa.
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
						'rating'      => $rating,
			            'reviews'	  => $product_info['reviews'],			
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					];
                }
            }
        }

            $data['module'] = $module++;

            if ($data['products']) {
                if ($data['sticker_colors']) {
                    $oct_color_stickers = [];

                    foreach ($data['sticker_colors'] as $sticker_colors) {
                        foreach ($sticker_colors as $key=>$sticker_color) {
                            $oct_color_stickers[$key] = $sticker_color;
                        }
                    }

                    $data['sticker_colors'] = $oct_color_stickers;
                }
                
                return $this->load->view('extension/module/installments', $data);
            }
        }

        return false;
    }

    /**
     * метод для показа popup окна оформления рассрочки
     */
    public function popup()
    {
        if ($this->config->get('payment_kupivkredit_status') && $this->config->get('config_checkout_guest')
            && isset($this->request->server['HTTP_X_REQUESTED_WITH'])
            && ! empty($this->request->server['HTTP_X_REQUESTED_WITH'])
            && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {

            $this->load->model('catalog/product');
            $this->load->language('extension/module/installments_purchase');

            if (isset($this->request->post['product_id']) && ! empty($this->request->post['product_id'])) {
                $product_id = (int)$this->request->post['product_id'];
            } else {
                $product_id = 0;
            }

            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                $this->load->model('account/address');
                $data['payment_kupivkredit_purchase_data'] = $payment_kupivkredit_purchase_data = $this->config->get('payment_kupivkredit_purchase_data');

                $data['user_name'] = $this->customer->isLogged() ? $this->customer->getFirstName() ." ". $this->customer->getLastName() : '';
                $data['user_email'] = $this->customer->isLogged() ? $this->customer->getEmail() : '';
                $data['user_telephone'] = $this->customer->isLogged() ? $this->customer->getTelephone() : '';
                $data['user_address'] = $this->customer->isLogged() ? $this->model_account_address->getAddress($this->customer->getAddressId()) : '';

                $data['product_id']   = (int)$product_id;
                $data['product_name'] = $product_info['name'];
                $data['model']        = $product_info['model'];
                $data['sku']          = $product_info['sku'];
                
                $data['promocodes'] = $allPromoCodes = [];
                /** Варианты рассрочки в модуле */
                $configPromoCodes = $this->config->get('payment_kupivkredit_promo_code');
                if($configPromoCodes){
                    foreach ($configPromoCodes as $item){
                        $allPromoCodes[$item['code']] = $item;
                    }
                }
                /** Варианты рассрочки, указанные у товара */
                $productPromoCodes = $this->getProductInstallmentTypes($product_id);
                foreach ($productPromoCodes as $code){
                    if(isset($allPromoCodes[$code]))
                        $data['promocodes'][] = $allPromoCodes[$code];
                }

                if ($product_info['minimum']) {
                    $data['minimum'] = $product_info['minimum'];
                } else {
                    $data['minimum'] = 1;
                }

                $data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);

                if ($product_info['quantity'] <= 0 && isset($payment_kupivkredit_purchase_data['stock_check'])) {
                    $data['error_stock_check'] = $this->language->get('error_quantity_stock');
                }

                if (isset($payment_kupivkredit_purchase_data['stock_check'])) {
                    $data['max_quantity'] = $product_info['quantity'];
                }

                if (isset($payment_kupivkredit_purchase_data['image']) && $payment_kupivkredit_purchase_data['image']) {
                    $this->load->model('tool/image');

                    $image_width  = (isset($payment_kupivkredit_purchase_data['image_width']) && !empty($payment_kupivkredit_purchase_data['image_width'])) ? $payment_kupivkredit_purchase_data['image_width'] : $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width');
                    $image_height = (isset($payment_kupivkredit_purchase_data['image_height']) && !empty($payment_kupivkredit_purchase_data['image_height'])) ? $payment_kupivkredit_purchase_data['image_height'] : $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height');

                    if ($product_info['image']) {
                        $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $image_width, $image_height);
                    } else {
                        $data['thumb'] = $this->model_tool_image->resize("placeholder.png", $image_width, $image_height);
                    }

                    $results = $this->model_catalog_product->getProductImages((int)$product_id);

                    if ($data['thumb'] && !empty($results)) {
                        $data['images'][0] = [
                            'popup' => $data['thumb'],
                            'thumb' => $data['thumb']
                        ];
                    }

                    foreach ($results as $result) {
                        $data['images'][] = [
                            'popup' => $this->model_tool_image->resize($result['image'], $image_width, $image_height),
                            'thumb' => $this->model_tool_image->resize($result['image'], $image_width, $image_height)
                        ];
                    }
                }

                if ($this->customer->isLogged() || ! $this->config->get('config_customer_price')) {
                    $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'],
                        $product_info['tax_class_id'], $this->config->get('config_tax')),
                        $this->session->data['currency']);
                } else {
                    $data['price'] = false;
                }

                if ((float)$product_info['special']) {
                    $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'],
                        $product_info['tax_class_id'], $this->config->get('config_tax')),
                        $this->session->data['currency']);
                } else {
                    $data['special'] = false;
                }



                if($this->config->get('payment_kupivkredit_server')) {
                    $data['text_server'] = 'https://loans.tinkoff.ru/api/partners/v1/lightweight/create';
                } else {
                    $data['text_server'] = 'https://loans-qa.tcsbank.ru/api/partners/v1/lightweight/create';
                }

                $data['productCategory'] = $this->getCategoryByProductId($product_info['product_id']);
                $data['productPrice'] = (float)$product_info['special'] ? 
                    round($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'))) 
                    : round($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                $data['shopId']     = $this->config->get('payment_kupivkredit_merch_z');

                $data['options'] = [];

                foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
                    $product_option_value_data = [];

                    //if ((isset($payment_kupivkredit_purchase_data['allowed_options']) && !empty($payment_kupivkredit_purchase_data['allowed_options'])) && (in_array($option['option_id'], $payment_kupivkredit_purchase_data['allowed_options']))) {
                        
											foreach ($option['product_option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $price = false;
                                }

                                $product_option_value_data[] = [
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id'         => $option_value['option_value_id'],
                                    'name'                    => $option_value['name'],
                                    'image'                   => $this->model_tool_image->resize($option_value['image'], 100, 100),
                                    'price'                   => $price,
                                    'price_prefix'            => $option_value['price_prefix']
                                ];
                            }
                        }

                        $data['options'][] = [
                            'product_option_id'    => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id'            => $option['option_id'],
                            'name'                 => $option['name'],
                            'type'                 => $option['type'],
                            'value'                => $option['value'],
                            'required'             => $option['required']
                        ];
                    }
               // }

                // Captcha
                if ($this->config->get('captcha_'.$this->config->get('config_captcha').'_status')
                    && in_array('oct_popup_purchase', (array)$this->config->get('config_captcha_page'))
                ) {
                    $data['captcha'] = $this->load->controller('extension/captcha/'
                        .$this->config->get('config_captcha'));
                } else {
                    $data['captcha'] = '';
                }

                if ($this->config->get('config_checkout_id')) {
                    $this->load->model('catalog/information');

                    $information_info
                        = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

                    if ($information_info) {
                        $data['text_agree'] = sprintf($this->language->get('text_agree'),
                            $this->url->link('information/information/agree',
                                'information_id='.$this->config->get('config_checkout_id'), true),
                            $information_info['title'], $information_info['title']);
                    } else {
                        $data['text_agree'] = false;
                    }
                } else {
                    $data['text_agree'] = false;
                }

                $data['product_link'] = $this->url->link('product/product', 'product_id='.$product_id, true);
            } else {
                $this->response->redirect($this->url->link('error/not_found', '', true));
            }

            $this->response->setOutput($this->load->view('extension/module/installments_purchase', $data));
        } else {
            $this->response->redirect($this->url->link('error/not_found', '', true));
        }
    }

    /**
     * метод для оформления заказа в рассрочку
     */
    public function makeOrder()
    {
        if ($this->config->get('oct_popup_purchase_status') && $this->config->get('config_checkout_guest')
            && isset($this->request->server['HTTP_X_REQUESTED_WITH'])
            && ! empty($this->request->server['HTTP_X_REQUESTED_WITH'])
            && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
            && (isset($this->request->post['product_id']) && ! empty($this->request->post['product_id']))
        ) {
            $this->load->language('octemplates/module/oct_popup_purchase');

            $json = [];

            if ($this->validate()) {
                $payment_kupivkredit_purchase_data = $this->config->get('payment_kupivkredit_purchase_data');

                $old_cart = $this->cart->getProducts();
                $this->cart->clear();

                $product_id = (int)$this->request->post['product_id'];

                if (isset($this->request->post['quantity'])) {
                    $quantity = (int)$this->request->post['quantity'];
                } else {
                    $quantity = 1;
                }

                if (isset($this->request->post['option'])) {
                    $option = array_filter($this->request->post['option']);
                } else {
                    $option = [];
                }

                $this->cart->add($product_id, $quantity, $option);

                $order_data = [];

                $totals = [];
                $taxes  = $this->cart->getTaxes();
                $total  = 0;

                // Because __call can not keep var references so we put them into an array.
                $total_data = [
                    'totals' => &$totals,
                    'taxes'  => &$taxes,
                    'total'  => &$total,
                ];

                $this->load->model('setting/extension');

                $sort_order = [];

                $results = $this->model_setting_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get('total_'.$value['code'].'_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get('total_'.$result['code'].'_status')) {
                        $this->load->model('extension/total/'.$result['code']);

                        // We have to put the totals in an array so that they pass by reference.
                        $this->{'model_extension_total_'.$result['code']}->getTotal($total_data);
                    }
                }

                $sort_order = [];

                foreach ($totals as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $totals);

                $order_data['totals'] = $totals;

                $this->load->language('checkout/checkout');

                $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
                $order_data['store_id']       = $this->config->get('config_store_id');
                $order_data['store_name']     = $this->config->get('config_name');

                if ($order_data['store_id']) {
                    $order_data['store_url'] = $this->config->get('config_url');
                } else {
                    if ($this->request->server['HTTPS']) {
                        $order_data['store_url'] = HTTPS_SERVER;
                    } else {
                        $order_data['store_url'] = HTTP_SERVER;
                    }
                }

                $this->load->model('account/customer');

                if ($this->customer->isLogged()) {
                    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

                    $order_data['customer_id']       = $this->customer->getId();
                    $order_data['customer_group_id'] = $customer_info['customer_group_id'];
                    $order_data['firstname']
                                                     = $order_data['shipping_firstname']
                        = $order_data['payment_firstname'] = isset($this->request->post['name'])
                        ? $this->request->post['name'] : $customer_info['firstname'];
                    $order_data['lastname']
                                                     =
                    $order_data['payment_lastname'] = $order_data['shipping_lastname'] = $customer_info['lastname'];
                    $order_data['email']             = isset($this->request->post['email'])
                        ? $this->request->post['email'] : $customer_info['email'];
                    $order_data['telephone']         = isset($this->request->post['telephone'])
                        ? $this->request->post['telephone'] : $customer_info['telephone'];
                } else {
                    $order_data['customer_id']       = 0;
                    $order_data['customer_group_id'] = $this->customer->getGroupId();
                    $order_data['firstname']
                                                     = $order_data['shipping_firstname']
                        = $order_data['payment_firstname'] = isset($this->request->post['name'])
                        ? $this->request->post['name'] : '';
                    $order_data['lastname']
                                                     =
                    $order_data['payment_lastname'] = $order_data['shipping_lastname'] = '';
                    $order_data['email']             = (isset($this->request->post['email'])
                        && ! empty($this->request->post['email'])) ? $this->request->post['email']
                        : $payment_kupivkredit_purchase_data['notify_email'];
                    $order_data['telephone']         = isset($this->request->post['telephone'])
                        ? $this->request->post['telephone'] : '';

                }
                
                $user_address = isset($this->request->post['address']) ? $this->request->post['address'] : '';

                $order_data['custom_field'] = [];

                $order_data['payment_method']         = 'Оплата в рассрочку';
                $order_data['payment_code']           = 'kupivkredit';
                $order_data['payment_company']        = '';
                $order_data['payment_address_1']      = $user_address;
                $order_data['payment_address_2']      = '';
                $order_data['payment_city']           = '';
                $order_data['payment_postcode']       = '';
                $order_data['payment_zone']           = '';
                $order_data['payment_zone_id']        = '';
                $order_data['payment_country']        = '';
                $order_data['payment_country_id']     = '';
                $order_data['payment_address_format'] = '';
                $order_data['payment_custom_field']   = [];

                $order_data['shipping_company']        = '';
                $order_data['shipping_address_1']      = $user_address;
                $order_data['shipping_address_2']      = '';
                $order_data['shipping_city']           = '';
                $order_data['shipping_postcode']       = '';
                $order_data['shipping_zone']           = '';
                $order_data['shipping_zone_id']        = '';
                $order_data['shipping_country']        = '';
                $order_data['shipping_country_id']     = '';
                $order_data['shipping_address_format'] = '';
                $order_data['shipping_custom_field']   = [];
                $order_data['shipping_method']         = '';
                $order_data['shipping_code']           = '';

                $order_data['products'] = [];

                foreach ($this->cart->getProducts() as $product) {
                    $option_data = [];

                    foreach ($product['option'] as $option) {
                        $option_data[] = [
                            'product_option_id'       => $option['product_option_id'],
                            'product_option_value_id' => $option['product_option_value_id'],
                            'option_id'               => $option['option_id'],
                            'option_value_id'         => $option['option_value_id'],
                            'name'                    => $option['name'],
                            'value'                   => $option['value'],
                            'type'                    => $option['type'],
                        ];
                    }

                    $order_data['products'][] = [
                        'product_id' => $product['product_id'],
                        'name'       => $product['name'],
                        'model'      => $product['model'],
                        'option'     => $option_data,
                        'download'   => $product['download'],
                        'quantity'   => $product['quantity'],
                        'subtract'   => $product['subtract'],
                        'price'      => $product['price'],
                        'total'      => $product['total'],
                        'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
                        'reward'     => $product['reward'],
                    ];
                }

                // Gift Voucher
                $order_data['vouchers'] = [];

                $order_data['comment'] = isset($this->request->post['comment']) ? $this->request->post['comment'] : '';
                $order_data['comment'] .= '
                -----
                рассрочка '.$this->request->post['type']; 
                $order_data['total']   = $total_data['total'];

                if (isset($this->request->cookie['tracking'])) {
                    $order_data['tracking'] = $this->request->cookie['tracking'];

                    $subtotal = $this->cart->getSubTotal();

                    // Affiliate
                    $affiliate_info
                        = $this->model_account_customer->getAffiliateByTracking($this->request->cookie['tracking']);

                    if ($affiliate_info) {
                        $order_data['affiliate_id'] = $affiliate_info['customer_id'];
                        $order_data['commission']   = ($subtotal / 100) * $affiliate_info['commission'];
                    } else {
                        $order_data['affiliate_id'] = 0;
                        $order_data['commission']   = 0;
                    }

                    // Marketing
                    $this->load->model('checkout/marketing');

                    $marketing_info
                        = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

                    if ($marketing_info) {
                        $order_data['marketing_id'] = $marketing_info['marketing_id'];
                    } else {
                        $order_data['marketing_id'] = 0;
                    }
                } else {
                    $order_data['affiliate_id'] = 0;
                    $order_data['commission']   = 0;
                    $order_data['marketing_id'] = 0;
                    $order_data['tracking']     = '';
                }

                $order_data['language_id']    = $this->config->get('config_language_id');
                $order_data['currency_id']    = $this->currency->getId($this->session->data['currency']);
                $order_data['currency_code']  = $this->session->data['currency'];
                $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
                $order_data['ip']             = $this->request->server['REMOTE_ADDR'];

                if ( ! empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                    $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
                } elseif ( ! empty($this->request->server['HTTP_CLIENT_IP'])) {
                    $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
                } else {
                    $order_data['forwarded_ip'] = '';
                }

                if (isset($this->request->server['HTTP_USER_AGENT'])) {
                    $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
                } else {
                    $order_data['user_agent'] = '';
                }

                if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                    $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
                } else {
                    $order_data['accept_language'] = '';
                }

                $this->load->model('checkout/order');

                $order_id = $this->model_checkout_order->addOrder($order_data);

                $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_kupivkredit_order_status_id'));

                $this->cart->clear();

                if ($old_cart) {
                    foreach ($old_cart as $value) {
                        $this->cart->add($value['product_id'], $value['quantity'], $value['option']);
                    }
                }

                $json['success'] = sprintf($this->language->get('text_success_order'), $order_id);
                $json['order_id'] = $order_id;
                $json['sum'] = $order_data['total'];
            } else {
                $json['error'] = $this->error;
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } else {
            $this->response->redirect($this->url->link('error/not_found', '', true));
        }
    }

    protected function validate()
    {
        $this->load->language('extension/module/installments_purchase');
        $payment_kupivkredit_purchase_data = $this->config->get('payment_kupivkredit_purchase_data');
        $promocodes = $this->config->get('payment_kupivkredit_promo_code');

        if (isset($this->request->post['name'])
            && (isset($payment_kupivkredit_purchase_data['firstname'])
                && $payment_kupivkredit_purchase_data['firstname'] == 2)
            && (utf8_strlen(trim($this->request->post['name'])) < 1
            || utf8_strlen(trim($this->request->post['name'])) > 32)
        ) {
            $this->error['name'] = $this->language->get('error_firstname');
        } 
        
        if (isset($this->request->post['address'])
            && (isset($payment_kupivkredit_purchase_data['address'])
                && $payment_kupivkredit_purchase_data['address'] == 2)
            && (utf8_strlen(trim($this->request->post['address'])) < 3
            || utf8_strlen(trim($this->request->post['address'])) > 128)
        ) {
            $this->error['address'] = $this->language->get('error_address');
        }
        
        if (isset($this->request->post['type'])
            && utf8_strlen(trim($this->request->post['type'])) == 0 
            && count($promocodes) > 0
        ) {
            $this->error['type'] = $this->language->get('error_type');
        }
        
        if (isset($this->request->post['email'])
            && (isset($payment_kupivkredit_purchase_data['email'])
                && $payment_kupivkredit_purchase_data['email'] == 2)
            && (utf8_strlen($this->request->post['email']) > 96
                || ! preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email']))
        ) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (isset($this->request->post['telephone']) && ! empty($payment_kupivkredit_purchase_data['mask'])) {
            $phone_count = utf8_strlen(str_replace(['_', '-', '(', ')', '+', ' '], "",
                $payment_kupivkredit_purchase_data['mask']));

            if ((isset($payment_kupivkredit_purchase_data['telephone']) && $payment_kupivkredit_purchase_data['telephone'] == 2)
                && utf8_strlen(str_replace(['_', '-', '(', ')', '+', ' '], "", $this->request->post['telephone']))
                < $phone_count
            ) {
                $this->error['telephone'] = $this->language->get('error_telephone_mask');
            }
        } elseif (isset($this->request->post['telephone'])
            && ((isset($payment_kupivkredit_purchase_data['telephone'])
                    && $payment_kupivkredit_purchase_data['telephone'] == 2)
                && (utf8_strlen(str_replace(['_', '-', '(', ')', '+', ' '], "", $this->request->post['telephone'])) > 15
                    || utf8_strlen(str_replace(['_', '-', '(', ')', '+', ' '], "", $this->request->post['telephone']))
                    < 3))
        ) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        // Captcha
        if ($this->config->get('captcha_'.$this->config->get('config_captcha').'_status')
            && in_array('oct_popup_purchase', (array)$this->config->get('config_captcha_page'))
        ) {
            $captcha = $this->load->controller('extension/captcha/'.$this->config->get('config_captcha')
                .'/validate');

            if ($captcha) {
                $this->error['captcha'] = $captcha;
            }
        }

        if ( ($this->config->get('config_checkout_id')
                && ! isset($this->request->post['agree']))
        ) {
            $this->load->model('catalog/information');

            $information_info
                = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

            if (isset($information_info) && ! empty($information_info)) {
                $this->error['agree'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }
        

        return ! $this->error;
    }

    private function getCategoryByProductId($product_id) {
        $query = $this->db->query("SELECT cd.name FROM " . DB_PREFIX . "product_to_category p LEFT JOIN " . DB_PREFIX . "category c ON (p.category_id = c.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE p.product_id = '".(int)$product_id."' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
        return $query->row['name'];
    }

    protected function getProductInstallmentTypes($product_id) {
        $query = $this->db->query("SELECT types FROM " . DB_PREFIX . "kupivkredit_product WHERE product_id = '". (int) $product_id ."'");

        return $query->num_rows ? json_decode($query->row['types']) : [];
    }
}
<?php
class ControllerExtensionModuleBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['banners'] = array();
        $this->document->addScript('catalog/view/theme/oct_ultrastore/js/fancybox/jquery.fancybox.min.js');
        $this->document->addStyle('catalog/view/theme/oct_ultrastore/js/fancybox/jquery.fancybox.min.css');
		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
//			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']),
					'large_image' => $this->model_tool_image->resize($result['image'])
				);
//			}
		}

		$data['module'] = $module++;

		return $this->load->view('extension/module/banner', $data);
	}
}

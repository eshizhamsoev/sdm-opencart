<?php
class ControllerExtensionModuleTags extends Controller {
	public function index() {
		$this->load->language('extension/module/tags');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = false;
		}

		$data = array();

		if ($parts){
			$data['category_id'] = $parts[count($parts)-1];
		}

		$this->load->model('catalog/tags');

		$this->load->model('catalog/category');

		$data['tags'] = array();

		$tags = $this->model_catalog_tags->getCloudTags($data);
		
		foreach ($tags as $tag) {
			$href = $this->url->link('product/tags', 'tag_id=' . $tag['tag_id']);
        	if ($tag['category_id']){
        		$path = $tag['category_id'];
                $flag = false;
                $tid = $tag['category_id'];
                while (!$flag) {
                    $c = $this->model_catalog_category->getCategory($tid);
                    if ($c['parent_id']){
                        $path = $c['parent_id']."_".$path;
                        $tid = $c['parent_id'];
                    }
                    else{
                        $flag = true;
                    }
                }
                $href = $this->url->link('product/tags', 'path='.$path.'&tag_id=' . $tag['tag_id']);
        	}
			$data['tags'][] = array(
				'tag_id' 	  => $tag['tag_id'],
				'name'        => $tag['name'],
				'href'        => $href
			);
		}

		
		return $this->load->view('extension/module/tags', $data);
		
	}
}
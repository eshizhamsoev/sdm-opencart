<?php

class ControllerExtensionModuleOzonSeller extends Controller
{
    /* Получение категорий маркетплейса и перезапись их в БД */
    public function getCategoryOzon()
    {

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {
            $this->log_process('Начинаю загрузку...');

            $this->load->model('extension/module/ozon_seller');
            $url      = 'https://api-seller.ozon.ru/v1/categories/tree';
            $response = $this->makeRequest($url, 'GET');

            if ( ! empty($response['result'])) {
                $this->model_extension_module_ozon_seller->saveOzonCategory($response);
                $this->log_process('Категории загружены!');
                $this->log('Категории загружены');
            } else {
                $this->log($response['error']['message']);
            }
        }
    }

    /* Получение списка не обработанных заказов */
    public function getNewOrdersOzon()
    {

        header('Content-Type: text/html; charset=utf-8');
        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {
            $this->load->model('extension/module/ozon_seller');
            $response = $this->getOrdersOzonByStatus('awaiting_packaging');

            if ( ! empty($response)) {
                foreach ($response as $info) {
                    if (empty($info['products']) || empty($info['products'][0]['offer_id'])) {
                        continue;
                    }
                    $posting_number = $info['posting_number'];
                    $shipment_date  = date('Y-m-d H:i:s', strtotime($info['shipment_date']));
                    $products       = $info['products'];
                    $status         = htmlspecialchars($info['status']);
                    $barcodes       = $info['barcodes'];
                    $analytics_data = $info['analytics_data'];

                    $order = $this->model_extension_module_ozon_seller->getMyOrder($posting_number);

                    if (empty($order)) {
                        if ($this->config->get('ozon_seller_status_order_oc')) {
                            $this->createOrderOc($info);
                        }
                        $this->createOrderInDb($posting_number, $shipment_date, $status);
                    } else {
                        // ozon может изменить дату доставки, значит меняется дата отгрузки
                        if ($order[0]['shipment_date'] != $shipment_date) {
                            $this->model_extension_module_ozon_seller->updateShipmentDate($posting_number,
                                $shipment_date);
                            $this->log($posting_number.' дата отгрузки изменена на '.$shipment_date, 0);
                        }
                        echo 'Отправление '.$posting_number.' пропущено, т.к уже создано <br />';
                    }
                }
            } else {
                echo 'Нет новых заказов';
            }
            //трекинг закзов порядок выполнения важен
            $this->statusAwaitingDeliver();
            $this->checkCancelledReturn();
            $this->finalCheckReturnsOzon();
            $this->statusCancelled();
            $this->getReturnsOzon();
            $this->statusDelivering();
            //акт и ттн
            if ($this->config->get('ozon_seller_act')) {
                $this->getActId();
            }
        }
    }

    //Создать заказ
    private function createOrderInDb($posting_number, $shipment_date, $status)
    {
        $this->load->model('extension/module/ozon_seller');
        $this->model_extension_module_ozon_seller->saveOrder($posting_number, $shipment_date, $status);
        echo $posting_number.' успешно создан<br />';
    }


    /* Обработаем заказы, что в нашей БД еще новые, а в Ozon уже собраны */
    private function statusAwaitingDeliver()
    {
        $this->load->model('extension/module/ozon_seller');
        $status_in_shop = 'awaiting_packaging';
        $orders         = $this->model_extension_module_ozon_seller->getOrderByStatus($status_in_shop);

        if ( ! empty($orders)) {
            $status_ozon = 'awaiting_deliver';
            $date        = new DateTime();
            $date->modify('-10 day');
            $offset = 0;
            $url    = 'https://api-seller.ozon.ru/v2/posting/fbs/list';
            do {
                $data     = array(
                    'dir'     => 'asc',
                    'filter'  => array(
                        'since'  => $date->format(DATE_ATOM),
                        'status' => $status_ozon,
                        'to'     => date(DATE_ATOM),
                    ),
                    'sort_by' => 'order_created_at',
                    'limit'   => 50,
                    'offset'  => $offset,
                );
                $response = $this->makeRequest($url, 'POST', $data);

                if ( ! empty($response['result'])) {
                    foreach ($response['result'] as $info) {
                        $posting_number = $info['posting_number'];
                        foreach ($orders as $order) {

                            if ($posting_number == $order['posting_number'] && $order['status'] != $status_ozon) {
                                $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number,
                                    $status_ozon);

                                if ($this->config->get('ozon_seller_status_order_oc')) {
                                    $order_oc = $this->model_extension_module_ozon_seller->getOrderOc($posting_number);
                                    if ( ! empty($order_oc[0]['order_id'])) {
                                        $this->changeOrderStatusOC($order_oc[0]['order_id'],
                                            $this->config->get('ozon_seller_status_deliver'));
                                    }
                                }
                            }
                        }
                    }
                }
                $offset += 50;
            } while (count($response['result']) == 50);
        }
    }

    /* Обработаем заказы, которые мы отвезли в Ozon */
    private function statusDelivering()
    {
        $this->load->model('extension/module/ozon_seller');
        $status_in_shop = 'awaiting_deliver';
        $orders         = $this->model_extension_module_ozon_seller->getOrderByStatus($status_in_shop);

        if ( ! empty($orders)) {
            $status_ozon = 'delivering';
            $date        = new DateTime();
            $date->modify('-10 day');
            $offset = 0;
            $url    = 'https://api-seller.ozon.ru/v2/posting/fbs/list';
            do {
                $data     = array(
                    'dir'     => 'desc',
                    'filter'  => array(
                        'since'  => $date->format(DATE_ATOM),
                        'status' => $status_ozon,
                        'to'     => date(DATE_ATOM),
                    ),
                    'sort_by' => 'updated_at',
                    'limit'   => 50,
                    'offset'  => $offset,
                );
                $response = $this->makeRequest($url, 'POST', $data);

                if ( ! empty($response['result'])) {
                    foreach ($orders as $order) {
                        foreach ($response['result'] as $info) {
                            $posting_number = $info['posting_number'];

                            if ($posting_number == $order['posting_number'] && $order['status'] != $status_ozon) {
                                $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number,
                                    $status_ozon);

                                if ($this->config->get('ozon_seller_status_order_oc')) {
                                    $order_oc = $this->model_extension_module_ozon_seller->getOrderOc($posting_number);
                                    if ( ! empty($order_oc[0]['order_id'])) {
                                        $this->changeOrderStatusOC($order_oc[0]['order_id'],
                                            $this->config->get('ozon_seller_status_shipping'));
                                    }
                                }
                            }
                        }
                    }
                }
                $offset += 50;
            } while (count($response['result']) == 50);
        }
    }

    /* Обработаем отмененные заказы из Ozon */
    private function statusCancelled()
    {
        $this->load->model('extension/module/ozon_seller');
        $order_cancelled = [];
        $status_ozon     = 'cancelled';
        $date            = new DateTime();
        $date->modify('-2 month'); //для премиальных клиентов Ozon дает гарантию и возврат на 60 дней
        $offset = 0;
        $url    = 'https://api-seller.ozon.ru/v2/posting/fbs/list';
        do {
            $data     = array(
                'dir'     => 'desc',
                'filter'  => array(
                    'since'  => $date->format(DATE_ATOM),
                    'status' => $status_ozon,
                    'to'     => date(DATE_ATOM),
                ),
                'sort_by' => 'updated_at',
                'limit'   => 50,
                'offset'  => $offset,
            );
            $response = $this->makeRequest($url, 'POST', $data);

            if ( ! empty($response['result'])) {
                foreach ($response['result'] as $info) {
                    $posting_number = $info['posting_number'];
                    $order          = $this->model_extension_module_ozon_seller->getMyOrder($posting_number);

                    if ( ! empty($order)) {
                        if ($order[0]['status'] != $status_ozon && $order[0]['status'] != 'returned'
                            && $order[0]['status'] != 'return_fbs'
                        ) {
                            $guid              = $order[0]['guid'];
                            $order_cancelled[] = array('guid' => $guid, 'posting_number' => $posting_number);
                        }
                    }
                }
            }
            $offset += 50;
        } while (count($response['result']) == 50);

        if ( ! empty($order_cancelled)) {
            foreach ($order_cancelled as $order_cancel) {
                $guid           = $order_cancel['guid'];
                $posting_number = $order_cancel['posting_number'];

                $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number, $status_ozon);

                if ($this->config->get('ozon_seller_status_order_oc')) {
                    $order_oc = $this->model_extension_module_ozon_seller->getOrderOc($posting_number);
                    if ( ! empty($order_oc[0]['order_id'])) {
                        $this->changeOrderStatusOC($order_oc[0]['order_id'],
                            $this->config->get('ozon_seller_status_cancel'));
                    }
                }
            }
        }
    }

    //получим номера отправлений с возвратами в финансовом отчете
    private function getReturnsOzon()
    {
        $this->load->model('extension/module/ozon_seller');
        $url  = 'https://api-seller.ozon.ru/v3/finance/transaction/list';
        $date = new DateTime();
        $date->modify('-2 day');
        $data     = array(
            'filter'    => array(
                'date'             => array(
                    'from' => $date->format(DATE_ATOM),
                    'to'   => date(DATE_ATOM),
                ),
                'transaction_type' => 'returns',
            ),
            'page'      => 1,
            'page_size' => 1000,
        );
        $response = $this->makeRequest($url, 'POST', $data);

        $postings = array();
        foreach ($response['result']['operations'] as $posting) {
            $postings[] = $posting['posting']['posting_number'];
        }
        //получим информацию по возвратам
        $returns = $this->checkReturnsOzon($postings);

        if ( ! empty($returns['result'])) {
            foreach ($returns['result']['returns'] as $return) {
                if ($return['status'] != 'returned_to_seller') {
                    $posting_number = htmlspecialchars($return['posting_number']);
                    $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number,
                        $status = 'return_fbs');
                }
            }
        }
    }

    //информация по возвратам FBS
    private function checkReturnsOzon($postings)
    {
        $url      = 'https://api-seller.ozon.ru/v2/returns/company/fbs';
        $data     = array(
            'filter' => array('posting_number' => $postings),
            'limit'  => 1000,
        );
        $response = $this->makeRequest($url, 'POST', $data);

        return $response;
    }

    //проверим выдан ли возврат продавцу
    private function finalCheckReturnsOzon()
    {
        $this->load->model('extension/module/ozon_seller');
        $returns_db = $this->model_extension_module_ozon_seller->getOrderByStatus($status = 'return_fbs');
        foreach ($returns_db as $return) {
            $posting_number = $return['posting_number'];
            //получим информацию по возвратам
            $returns_ozon = $this->checkReturnsOzon($postings = array($posting_number));

            foreach ($returns_ozon['result']['returns'] as $return_ozon) {
                if ($return_ozon['status'] != 'returned_to_seller') {
                    continue;
                }
                echo $posting_number.' возврат успешно обработан<br />';
                $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number, $status = 'returned');

                if ($this->config->get('ozon_seller_status_order_oc')) {
                    $order_oc = $this->model_extension_module_ozon_seller->getOrderOc($posting_number);
                    if ( ! empty($order_oc[0]['order_id'])) {
                        $this->changeOrderStatusOC($order_oc[0]['order_id'],
                            $this->config->get('ozon_seller_status_return'));
                    }
                }
            }
        }
    }

    //получим отмененные заказы за 2 месяца и проверим есть ли по ним возврат в Озон
    private function checkCancelledReturn()
    {
        $this->load->model('extension/module/ozon_seller');
        $start = 0;
        $limit = 900;
        do {
            $filter_data = array(
                'status' => 'cancelled',
                'month'  => '-2 month',
                'start'  => $start,
                'limit'  => $limit,
            );
            $orders_db   = $this->model_extension_module_ozon_seller->getOrderByStatusMonthOld($filter_data);
            $posting     = array();
            foreach ($orders_db as $order_db) {
                $posting[] = $order_db['posting_number'];
            }
            $start   += $limit;
            $returns = $this->checkReturnsOzon($posting);
            if ( ! empty($returns['result']['returns'])) {
                foreach ($returns['result']['returns'] as $return) {
                    if (isset($return['id'])) {
                        $posting_number = htmlspecialchars($return['posting_number']);
                        $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number,
                            $status = 'return_fbs');
                    }
                }
            }
        } while (count($orders_db) == $limit);
    }

    /* Обработаем доставленные заказы */
    public function statusDelivered()
    {
        header('Content-Type: text/html; charset=utf-8');

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {
            $this->load->model('extension/module/ozon_seller');
            $status_in_shop = 'delivering';
            $orders         = $this->model_extension_module_ozon_seller->getOrderByStatus($status_in_shop);

            if ( ! empty($orders)) {
                $status_ozon = 'delivered';
                $date        = new DateTime();
                $date->modify('-1 month');
                $offset = 0;
                do {
                    $data     = array(
                        'dir'     => 'asc',
                        'filter'  => array(
                            'since'  => $date->format(DATE_ATOM),
                            'status' => $status_ozon,
                            'to'     => date(DATE_ATOM),
                        ),
                        'sort_by' => 'updated_at',
                        'limit'   => 50,
                        'offset'  => $offset,
                        'with'    => array(
                            'analytics_data' => true,
                        ),
                    );
                    $url      = 'https://api-seller.ozon.ru/v2/posting/fbs/list';
                    $response = $this->makeRequest($url, 'POST', $data);

                    if ( ! empty($response['result'])) {
                        foreach ($response['result'] as $info) {
                            $posting_number = $info['posting_number'];
                            foreach ($orders as $order) {

                                if ($order['posting_number'] == $posting_number) {
                                    echo $posting_number.' доставлен <br />';
                                    $guid = $order['guid'];
                                    $this->model_extension_module_ozon_seller->updateStatusOzon($posting_number,
                                        $status_ozon);
                                    if ($this->config->get('ozon_seller_status_order_oc')) {
                                        $order_oc
                                            = $this->model_extension_module_ozon_seller->getOrderOc($posting_number);
                                        if ( ! empty($order_oc[0]['order_id'])) {
                                            $this->changeOrderStatusOC($order_oc[0]['order_id'],
                                                $this->config->get('ozon_seller_status_delevered'));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $offset += 50;
                } while (count($response['result']) == 50);
            }
        }
    }

    /* Получить список заказов в Ozon по статусу */

    private function getOrdersOzonByStatus($status)
    {
        $url  = 'https://api-seller.ozon.ru/v3/posting/fbs/list';
        $date = new DateTime();
        $date->modify('-10 day');
        $offset   = 0;
        $packings = array();
        do {
            $data     = array(
                'dir'     => 'asc',
                'filter'  => array(
                    'since'  => $date->format(DATE_ATOM),
                    'status' => $status,
                    'to'     => date(DATE_ATOM),
                ),
                'sort_by' => 'order_created_at',
                'limit'   => 50,
                'offset'  => $offset,
                'with'    => array(
                    'barcodes'       => true,
                    'analytics_data' => true,
                ),
            );
            $response = $this->makeRequest($url, 'POST', $data);

            if ( ! empty($response['result']['postings'])) {
                foreach ($response['result']['postings'] as $packing) {
                    $packings[] = $packing;
                }
            }
            $offset += 50;
        } while ($response['result']['has_next'] == 'true');

        return $packings;
    }


    /* Получить информацию о товаре продавца в Ozon по артикулу продавца */
    private function getOzonProduct($offer_id)
    {

        $url      = 'https://api-seller.ozon.ru/v2/product/info';
        $data     = array('offer_id' => $offer_id);
        $response = $this->makeRequest($url, 'POST', $data);

        return $response;

    }

    /* Получить информацию о товаре продавца в Ozon по sku маркетплейса */
    private function getOzonProductId($sku)
    {

        $url = 'https://api-seller.ozon.ru/v2/product/info';

        $data = array('sku' => $sku);

        $response = $this->makeRequest($url, 'POST', $data);

        return $response;

    }

    /* Собрать заказ в Ozon */
    private function packOrder($guid)
    {

        $this->load->model('extension/module/ozon_seller');

        $packings = $this->model_extension_module_ozon_seller->getOrder($guid);

        if ( ! empty($packings['0']) && $packings['0']['status'] != 'delivered') {

            $posting_number = $packings['0']['posting_number'];

            $response = $this->getOrderOzon($posting_number);

            foreach ($response as $info) {

                $items = array();//кладем товары в массив

                $products = $info['products'];

                foreach ($products as $product) {
                    $items[] = array(
                        'quantity' => $product['quantity'],
                        'sku'      => $product['sku'],
                    );
                }

                $url = 'https://api-seller.ozon.ru/v2/posting/fbs/ship';

                $data = array(
                    'packages'       => array(
                        array('items' => $items),
                    ),
                    'posting_number' => $posting_number,
                );

                $respon = $this->makeRequest($url, 'POST', $data);

                if (isset($respon['error']) && $respon['error']['message'] != 'POSTING_ALREADY_SHIPPED') {

                    $this->log($posting_number.' ошибка при сборке: '.json_encode($respon, JSON_UNESCAPED_UNICODE), 0);

                    return 'error';

                } else {

                    return 'OK';
                }
            }

        } else {

            return 'OK';
        }
    }

    // Получить заказ из админки
    public function getOrderOzonAdmin()
    {
        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {

            if (isset($this->request->get['posting'])) {
                $posting = $this->getOrderOzon($this->request->get['posting']);

                echo json_encode($posting, JSON_UNESCAPED_UNICODE);
            }
        }
    }

    // Собрать заказ в Ozon из админки
    public function packOrderAdmin()
    {

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {

            if (isset($this->request->get['posting'])) {

                $response = $this->getOrderOzon($this->request->get['posting']);

                $info = $response['result'];

                $items = array();//кладем товары в массив

                $products = $info['products'];

                foreach ($products as $product) {
                    $items[] = array(
                        'quantity' => $product['quantity'],
                        'sku'      => $product['sku'],
                    );
                }

                $url = 'https://api-seller.ozon.ru/v2/posting/fbs/ship';

                $data = array(
                    'packages'       => array(
                        array('items' => $items),
                    ),
                    'posting_number' => $info['posting_number'],
                );

                $respon = $this->makeRequest($url, 'POST', $data);

                if (isset($respon['error']) && $respon['error']['message'] != 'POSTING_ALREADY_SHIPPED') {

                    echo $info['posting_number'].' ошибка при сборке: '.json_encode($respon, JSON_UNESCAPED_UNICODE);

                } elseif (isset($respon['error']) && $respon['error']['message'] == 'POSTING_ALREADY_SHIPPED') {

                    echo 'Отправление уже было собрано';

                } else {

                    echo 'OK';
                }
            }
        }
    }

    // Удалить заказ в БД
    public function deleteOrder()
    {

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
            && isset($this->request->get['posting'])
        ) {

            $posting_number = htmlspecialchars($this->request->get['posting']);

            $this->load->model('extension/module/ozon_seller');

            $this->model_extension_module_ozon_seller->deleteOrder($posting_number);
        }
    }

    /* Получить заказ в Ozon */
    private function getOrderOzon($posting_number)
    {

        $url = 'https://api-seller.ozon.ru/v2/posting/fbs/get';

        $data = array('posting_number' => $posting_number, 'with' => array('analytics_data' => true));

        $response = $this->makeRequest($url, 'POST', $data);

        return $response;

    }


    /* Напечатать этикетку Ozon */
    public function printSticker()
    {

        $posting_number = $this->request->get['post'];
        $url            = 'https://api-seller.ozon.ru/v2/posting/fbs/package-label';
        $data           = array('posting_number' => array($posting_number));

        $response = $this->request2($url, $data);
        $error    = json_decode($response, true);

        if (isset($error['error'])) {
            echo $error['error']['message'];
        } else {
            header('Content-Type: application/pdf');
            echo $response;
        }
    }


    public function loadAttributes()
    {

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {

            $this->load->model('extension/module/ozon_seller');

            $pass_id = [
                85,
                88,
                95,
                121,
                4194,
                4074,
                4080,
                4159,
                4180,
                4191,
                4195,
                4497,
                8789,
                8790,
                8863,
                9054,
                9461,
                10097,
                10289,
                11254,
                44283970,
            ];

            $categories = $this->config->get('ozon_seller_category');

            foreach ($categories as $category) {

                $chek_load = $this->model_extension_module_ozon_seller->getOzonAttribute($category['ozon']);

                if (empty($chek_load)) {

                    $this->log_process('Начинаю загрузку...');

                    $attributes = $this->getAttribute($category['ozon']);

                    $count_attributes = count($attributes['result']);

                    $i = 0;

                    foreach ($attributes['result'][0]['attributes'] as $attribute) {

                        if ($attribute['id'] != in_array($attribute['id'], $pass_id, true)) {

                            $ozon_attribute_id          = htmlspecialchars($attribute['id']);
                            $ozon_attribute_name        = htmlspecialchars($attribute['name']);
                            $ozon_attribute_description = htmlspecialchars($attribute['description']);
                            $ozon_dictionary_id         = htmlspecialchars($attribute['dictionary_id']);
                            $required                   = htmlspecialchars($attribute['is_required']);

                            $this->model_extension_module_ozon_seller->saveAttributeDescription($ozon_attribute_id,
                                $ozon_attribute_name, $ozon_attribute_description, $ozon_dictionary_id, $required);

                            if ($required) {
                                $this->model_extension_module_ozon_seller->saveAttributeRequired($category['ozon'],
                                    $ozon_attribute_id);
                            }

                            $this->model_extension_module_ozon_seller->saveAttribute($category['ozon'],
                                $ozon_attribute_id);

                            if ($ozon_dictionary_id) {

                                $this->log_process('['.$i.'/'.$count_attributes
                                    .'] Загружаю справочник характеристик для атрибута "'.$ozon_attribute_name
                                    .'", категория '.$category['ozon'].'. Ожидайте... ');

                                $this->getValueAttribute($category['ozon'], $ozon_attribute_id, $ozon_dictionary_id);
                            }
                        }
                        $i++;
                    }
                }
            }
            $this->log_process('Готово. Перезагрузите страницу!');
        }
    }

    /* Получение списка атрибутов для категории Ozon */
    private function getAttribute($ozon_category_id)
    {

        $url      = 'https://api-seller.ozon.ru/v3/category/attribute';
        $data     = array(
            'attribute_type' => 'ALL',
            'category_id'    => array($ozon_category_id),
            'language'       => 'DEFAULT',
        );
        $response = $this->makeRequest($url, 'POST', $data);

        return $response;
    }

    /* Загружаем справочник атрибутов и его значения */
    private function getValueAttribute($ozon_category_id, $ozon_attribute_id, $ozon_dictionary_id)
    {

        $url = 'https://api-seller.ozon.ru/v2/category/attribute/values';

        $data = array('category_id' => $ozon_category_id, 'attribute_id' => $ozon_attribute_id, 'limit' => 5000);

        $response = $this->makeRequest($url, 'POST', $data);

        foreach ($response['result'] as $result) {
            $attribute_value_id = htmlspecialchars($result['id']);
            $text               = htmlspecialchars($result['value']);

            $this->model_extension_module_ozon_seller->saveDictonary($ozon_dictionary_id, $attribute_value_id,
                $ozon_category_id, $ozon_attribute_id, $text);
        }

        if ($response['has_next']) {

            do {

                $last_key      = end($response['result']); //php < 7.3
                $last_value_id = $last_key['id']; //php < 7.3

                $data = array(
                    'category_id'   => $ozon_category_id,
                    'attribute_id'  => $ozon_attribute_id,
                    'limit'         => 5000,
                    'last_value_id' => $last_value_id,
                );

                $response = $this->makeRequest($url, 'POST', $data);

                foreach ($response['result'] as $result) {
                    $attribute_value_id = $result['id'];
                    $text               = htmlspecialchars($result['value']);

                    $this->model_extension_module_ozon_seller->saveDictonary($ozon_dictionary_id, $attribute_value_id,
                        $ozon_category_id, $ozon_attribute_id, $text);
                }

            } while ($response['has_next']);
        }
    }

    /* Импорт и обновление товаров в Ozon */
    public function importProduct()
    {

        header('Content-Type: text/html; charset=utf-8');
        $status_module = $this->config->get('ozon_seller_status');

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
            && $status_module
        ) {

            if ($this->config->get('ozon_seller_category')) {
                $this->load->model('catalog/product');
                $this->load->model('tool/image');
                $this->load->model('extension/module/ozon_seller');

                $ozon_seller_category  = $this->config->get('ozon_seller_category');
                $ozon_seller_attribute = $this->config->get('ozon_seller_attribute');
                // сопоставленные значения справочника
                $dictionary_shop_to_ozon    = $this->model_extension_module_ozon_seller->getDictionaryShoptoOzon();
                $ozon_attribute_description = $this->model_extension_module_ozon_seller->getOzonAttributeDescription();

                if ( ! empty($this->config->get('ozon_seller_manufacturer_stop'))) {
                    $stop_manufacturer = implode(",", $this->config->get('ozon_seller_manufacturer_stop'));
                } else {
                    $stop_manufacturer = 0;
                }

                foreach ($ozon_seller_category as $category_import) {
                    if (empty($category_import['type'])) {
                        continue;
                    }

                    if (isset($category_import['stop'])) {
                        continue;
                    }

                    $filter_data = array(
                        'filter_category_id'      => $category_import['shop'],
                        'exclude_manufacturer_id' => $stop_manufacturer,
                        'filter_sub_category'     => false,
                        'start'                   => 0,
                        'limit'                   => $this->config->get('ozon_seller_limit'),
                    );

                    $products = $this->model_extension_module_ozon_seller->getProducts($filter_data);

                    if ($products) {
                        $data['items'] = array(); //кладем товары в массив
                        $export_table  = array();
                        foreach ($products as $product) {
                            //черный список
                            if ($this->config->get('ozon_seller_product_blacklist')) {
                                $blacklist = $this->config->get('ozon_seller_product_blacklist');
                                if (in_array($product['product_id'], $blacklist)) {
                                    continue;
                                }
                            }

                            if (empty($product['image'])) {
                                continue;
                            }

                            //$offer_id
                            if ($this->config->get('ozon_seller_entry_offer_id')) {
                                $offer_id = $product['model'];
                                if (empty($offer_id)) {
                                    $this->log('Выгрузка товаров: не заполнен код товара (модель) у товара с артикулом '
                                        .$product['sku'], 0);
                                    continue;
                                }
                            } else {
                                $offer_id = $product['sku'];
                                if (empty($offer_id)) {
                                    $this->log('Выгрузка товаров: не заполнен артикул у товара с кодом (модель) '
                                        .$product['model'], 0);
                                    continue;
                                }
                            }

                            // Размеры
                            if ($product['weight_class_id'] == $this->config->get('ozon_seller_weight')) {
                                if ($product['weight'] == 0) {
                                    $weight = $category_import['weight'];
                                } else {
                                    $weight = $product['weight'];
                                }
                            } else {
                                if ($product['weight'] == 0) {
                                    $weight = $category_import['weight'];
                                } else {
                                    $weight = $product['weight'] * 1000;
                                }
                            }

                            if ($product['length_class_id'] == $this->config->get('ozon_seller_length')) {

                                if ($product['length'] == 0) {
                                    $length = $category_import['length'];
                                } else {
                                    $length = $product['length'];
                                }

                                if ($product['width'] == 0) {
                                    $width = $category_import['width'];
                                } else {
                                    $width = $product['width'];
                                }

                                if ($product['height'] == 0) {
                                    $height = $category_import['height'];
                                } else {
                                    $height = $product['height'];
                                }

                            } else {

                                if ($product['length'] == 0) {
                                    $length = $category_import['length'];
                                } else {
                                    $length = $product['length'] / 10;
                                }

                                if ($product['width'] == 0) {
                                    $width = $category_import['width'];
                                } else {
                                    $width = $product['width'] / 10;
                                }

                                if ($product['height'] == 0) {
                                    $height = $category_import['height'];
                                } else {
                                    $height = $product['height'] / 10;
                                }
                            }

                            // Цена
                            if ($product['special']) {
                                $price = $product['special'];
                            } else {
                                $price = $product['price'];
                            }

                            if ($this->config->get('ozon_seller_percent')) {
                                $percent  = $this->config->get('ozon_seller_percent');
                                $price_up = $price * $percent / 100;
                                $price    += round($price_up);
                            }

                            if ($this->config->get('ozon_seller_ruble')) {
                                $price += $this->config->get('ozon_seller_ruble');
                            }

                            // Изображения товара
                            $top_image = $this->request->server['HTTPS']
                                ? $this->config->get('config_ssl').'image/'.$product['image']
                                : $this->config->get('config_url').'image/'.$product['image'];
                            
                            $images     = array();
                            $images[]   = $top_image;
                            $get_images = $this->model_catalog_product->getProductImages($product['product_id']);

                            if ( ! empty($get_images)) {
                                foreach ($get_images as $get_image) {
                                    $images[] = $this->request->server['HTTPS']
                                        ? $this->config->get('config_ssl').'image/'.$get_image['image']
                                        : $this->config->get('config_url').'image/'.$get_image['image'];
                                }
                            }

                            // Rich-контент
                            $rich_content['content'] = array();

                            if ($this->config->get('ozon_seller_description') == 'on') {
                                $description = strip_tags(html_entity_decode($product['description'], ENT_NOQUOTES),
                                    '</p> <br> <style> </style>');
                                $description = preg_replace('/\s?<style>*?>.*?<\/style>\s?/', ' ', $description);
                                $description = str_replace('&quot;', '\'', $description);
                                $description = str_replace(array('&nbsp;', '&bull;'), ' ', $description);
                                $description = preg_replace('/([\r\n]){2,}/s', '\1', $description);
                                $description = preg_replace('/[\t]+/', '', $description);
                                $description = preg_replace('/[\n]+/', '^', $description);
                                $description = str_replace(array('<br/>', '<br />', '<br>', '</p>'), '^', $description);
                                $description = explode('^', $description);

                            } else {
                                $description = [' '];
                            }

                            $rich_content['content'][] = array(
                                'widgetName' => 'raTextBlock',
                                'text'       => array(
                                    'theme'   => 'default',
                                    'content' => $description,
                                ),
                            );

                            if ($this->config->get('ozon_seller_attribute_description') == 'on') {

                                $attributes_product
                                    = $this->model_catalog_product->getProductAttributes($product['product_id']); //массив с атрибутами для вставки в описание

                                if ($attributes_product) {

                                    $table_attr = array();

                                    foreach ($attributes_product as $attribute_product) {

                                        foreach ($attribute_product['attribute'] as $attribute) {

                                            $attribute_name = html_entity_decode($attribute['name'], ENT_NOQUOTES);
                                            $attribute_text = html_entity_decode($attribute['text'], ENT_NOQUOTES);

                                            $table_attr[] = array(
                                                str_replace('&quot;', '\'', $attribute_name),
                                                str_replace('&quot;', '\'', $attribute_text),
                                            );
                                        }
                                    }

                                    $rich_content['content'][] = array(
                                        'widgetName' => 'raTable',
                                        'title'      => 'Характеристики товара от продавца',
                                        'table'      => array(
                                            'header' => array('Наименование', 'Значение'),
                                            'body'   => $table_attr,
                                        ),
                                    );
                                }
                            }

                            $version_rich_content = array('version' => round(0.2, 1));

                            $rich_content = $rich_content + $version_rich_content;

                            ini_set('serialize_precision', -1); //патч бага php 7.3

                            $attribute_export = array(
                                /* Вес с упаковкой в гр. */
                                array(
                                    'complex_id' => 0,
                                    'id'         => 4497,
                                    'values'     => array(
                                        array(
                                            'dictionary_value_id' => 0,
                                            'value'               => (string)$weight,
                                        ),
                                    ),
                                ),
                                /* Rich контент */
                                array(
                                    'complex_id' => 0,
                                    'id'         => 11254,
                                    'values'     => array(
                                        array(
                                            'dictionary_value_id' => 0,
                                            'value'               => json_encode($rich_content, JSON_UNESCAPED_UNICODE),
                                        ),
                                    ),
                                ),
                                /* Главное фото товара */
                                array(
                                    'complex_id' => 0,
                                    'id'         => 4194,
                                    'values'     => array(
                                        array(
                                            'dictionary_value_id' => 0,
                                            'value'               => $top_image,
                                        ),
                                    ),
                                ),
                            );

                            // Бренд

                            $ozon_manufacturer
                                = $this->model_extension_module_ozon_seller->getManufacturer($product['manufacturer_id']);

                            if ( ! empty($ozon_manufacturer)) {

                                $attribute_export[] = array(
                                    'complex_id' => 0,
                                    'id'         => 85,
                                    'values'     => array(
                                        array(
                                            'dictionary_value_id' => (int)$ozon_manufacturer[0]['ozon_id'],
                                            'value'               => (string)$ozon_manufacturer[0]['value'],
                                        ),
                                    ),
                                );

                            } else {

                                $attribute_export[] = array(
                                    'complex_id' => 0,
                                    'id'         => 85,
                                    'values'     => array(
                                        array(
                                            'dictionary_value_id' => 0,
                                            'value'               => (string)$product['manufacturer'],
                                        ),
                                    ),
                                );
                            }

                            // Тип товара соответствует всей категории

                            if ($category_import['type'] != 'attr') {

                                $value
                                    = $this->model_extension_module_ozon_seller->getDictionaryByCategoryAndAttributeId($category_import['ozon'],
                                    $category_import['type']);

                                $attribute_export[] = array(
                                    'complex_id' => 0,
                                    'id'         => 8229,
                                    'values'     => array(
                                        array(
                                            'dictionary_value_id' => (int)$category_import['type'],
                                            'value'               => (string)$value['text'],
                                        ),
                                    ),
                                );
                            }

                            // группы атрибутов категории Ozon (oc_ozon_attribute)
                            $attribute_ozon
                                = $this->model_extension_module_ozon_seller->getOzonAttribute($category_import['ozon']);

                            // язык по-умолчанию
                            $this->load->model('setting/setting');
                            $language_config = $this->model_setting_setting->getSetting('config',
                                $this->config->get('config_store_id'));
                            $language        = $language_config['config_language'];
                            $language_id     = $this->model_extension_module_ozon_seller->getLanguage($language);

                            // значения атрибутов в магазине этого товара
                            $dictionary_shop
                                = $this->model_extension_module_ozon_seller->getShopDictionary($product['product_id'],
                                $language_id);

                            if ($dictionary_shop) {
                                unset($type_stop);
                                foreach ($dictionary_shop as $dictionar_shop) {
                                    foreach ($dictionary_shop_to_ozon as $dictionar_shop_to_ozon) {

                                        // Тип товара соответствует атрибутам
                                        if ($dictionar_shop_to_ozon['ozon_attribute_id'] == 8229
                                            && $category_import['type'] == 'attr'
                                            && ! isset($type_stop)
                                        ) {

                                            $text_shop_attr = $dictionar_shop_to_ozon['text_shop_attribute'];
                                            $delimiter      = '++';
                                            $find_delimiter = strpos($text_shop_attr, $delimiter);

                                            if ($find_delimiter === false) {
                                                $text_shop_attr = array($text_shop_attr);
                                            } else {
                                                $text_shop_attr = explode($delimiter, $text_shop_attr);
                                            }
                                        }

                                        if ($dictionar_shop_to_ozon['ozon_attribute_id'] == 8229
                                            && $category_import['type'] != 'attr'
                                        ) {
                                            continue;
                                        } elseif ($dictionar_shop_to_ozon['ozon_attribute_id'] == 8229
                                            && $category_import['type'] == 'attr'
                                            && in_array($dictionar_shop['text'], $text_shop_attr)
                                            && ! isset($type_stop)
                                        ) {

                                            $attribute_export[] = array(
                                                'complex_id' => 0,
                                                'id'         => 8229,
                                                'values'     => array(
                                                    array(
                                                        'dictionary_value_id' => (int)$dictionar_shop_to_ozon['dictionary_value_id'],
                                                        'value'               => (string)$dictionar_shop_to_ozon['value'],
                                                    ),
                                                ),
                                            );
                                            $type_stop          = true;
                                        }

                                        // Добавляем в выгрузку сопоставленные атрибуты магазина со справочником Ozon из таблицы ozon_to_shop_dictionary
                                        if ($dictionar_shop['attribute_id']
                                            == $dictionar_shop_to_ozon['shop_attribute_id']
                                        ) {
                                            $translit_shop = $this->translit($dictionar_shop['text']);
                                            $translit_ozon
                                                           = $this->translit($dictionar_shop_to_ozon['text_shop_attribute']);
                                            if (strcasecmp($translit_shop, $translit_ozon) == 0) {
                                                $dictionar_value_id = explode('^',
                                                    $dictionar_shop_to_ozon['dictionary_value_id']);
                                                $value_attr         = explode('^', $dictionar_shop_to_ozon['value']);
                                                $output_attr        = array_combine($dictionar_value_id, $value_attr);
                                                $values             = array();
                                                foreach ($output_attr as $key => $out_attr) {
                                                    $values[] = array(
                                                        'dictionary_value_id' => $key,
                                                        'value'               => $out_attr,
                                                    );
                                                }
                                                $attribute_export[] = array(
                                                    'complex_id' => 0,
                                                    'id'         => (int)$dictionar_shop_to_ozon['ozon_attribute_id'],
                                                    'values'     => $values,
                                                );
                                            }
                                        }
                                    }

                                    $attributes_ozon_to_shop = array_keys($ozon_seller_attribute,
                                        $dictionar_shop['attribute_id']);

                                    foreach ($ozon_attribute_description as $ozon_attr_description) {

                                        // Добавляем в выгрузку сопоставленные атрибуты магазина без справочника
                                        if (empty($ozon_attr_description['ozon_dictionary_id'])) {
                                            if (in_array($ozon_attr_description['ozon_attribute_id'],
                                                $attributes_ozon_to_shop)
                                            ) {
                                                $replace = array('mm', 'мм', 'см', 'кг');
                                                $value   = str_replace($replace, '', $dictionar_shop['text']);

                                                $attribute_export[] = array(
                                                    "complex_id" => (int)0,
                                                    'id'         => (int)$ozon_attr_description['ozon_attribute_id'],
                                                    'values'     => array(
                                                        array(
                                                            'dictionary_value_id' => (int)0,
                                                            'value'               => (string)$value,
                                                        ),
                                                    ),
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                            //если в админке модуля присвоили атрибутам поля OpenCart
                            $oc_input = array('sku', 'model', 'mpn', 'isbn', 'ean', 'jan', 'upc', 'product_dimensions', 'product_weight', 'product_length', 'product_width', 'product_height');
                            foreach ($ozon_seller_attribute as $k => $ozon_attr_seller) {
                                if (in_array($ozon_attr_seller, $oc_input)) {
                                    foreach ($attribute_ozon as $attr_ozon) {
                                        if ($attr_ozon['ozon_attribute_id'] == $k) {
                                            
                                            switch ($ozon_attr_seller){
                                                case 'product_dimensions':
                                                    $value = sprintf("%sx%sx%s", round($length*10), round($width*10), round($height*10));
                                                    break;
                                                case 'product_weight':
                                                    $value = $weight;
                                                    break;
                                                case 'product_length':
                                                    $value = round($length*10);
                                                    break;
                                                case 'product_width':
                                                    $value = round($width*10);
                                                    break;
                                                case 'product_height':
                                                    $value = round($height*10);
                                                    break;
                                                default:
                                                    $value = $product[$ozon_attr_seller];
                                                    break;
                                            }

                                            $attribute_export[] = array(
                                                "complex_id" => (int)0,
                                                'id'         => (int)$k,
                                                'values'     => array(
                                                    array(
                                                        'dictionary_value_id' => (int)0,
                                                        'value'               => (string)$value,
                                                    ),
                                                ),
                                            );
                                        }
                                    }
                                }
                            }

                            $laquo = array('«', '»', '"', '°', '\'');
                            $name  = str_replace($laquo, '', $product['name']);
                            $name  = htmlspecialchars_decode($name);

                            // штрих-код
                            //$barcode = mt_rand(1000000000000, 9999999999999);
                            $barcode = md5($product['product_id']);

                            $data['items'][] = array(
                                'name'           => $name,
                                'category_id'    => (int)$category_import['ozon'],
                                'offer_id'       => $offer_id,
                                'price'          => (string)$price,
                                'vat'            => $this->config->get('ozon_seller_nds'),
                                'barcode'        => (string)$barcode,
                                'weight'         => (int)$weight,
                                'weight_unit'    => 'g',
                                'dimension_unit' => 'cm',
                                'depth'          => (int)ceil($length),
                                'width'          => (int)ceil($width),
                                'height'         => (int)ceil($height),
                                'image_group_id' => $product['model'],
                                'primary_image'  => $top_image,
                                'images'         => $images,
                                'attributes'     => $attribute_export,
                            );

                            $export_table[] = array(
                                'product_id' => $product['product_id'],
                                'model'      => $product['model'],
                                'sku'        => $product['sku'],
                            );
                        }

                        if ($this->config->get('ozon_seller_test_export') && ! empty($data['items'])) {
                            echo '<pre>';
                            echo '=== '.$this->config->get('ozon_seller_version').' ===<br />';
                            echo '=== Включен тест экспорта товаров ===<br />';
                            echo '=== Запрос не будет отправлен в Ozon ===<br />';
                            echo '=== Чтобы выгрузить товары отключите тест ===<br />';
                            ini_set('serialize_precision', -1); //патч бага php 7.3
                            print_r(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
                        } elseif ( ! empty($data['items'])) {
                            $url      = 'https://api-seller.ozon.ru/v2/product/import';
                            $response = $this->makeRequest($url, 'POST', $data);

                            if (isset($response['error']) || isset($response['message'])) {
                                $this->log('Выгрузка товаров категория ID '.$category_import['shop'].' '
                                    .json_encode($response, JSON_UNESCAPED_UNICODE), 0);
                                echo 'Категория ID '.$category_import['shop'].': '.json_encode($response,
                                        JSON_UNESCAPED_UNICODE).'<br />';
                            }

                            if ( ! empty($response['result']['task_id'])) {
                                $task_id = $response['result']['task_id'];
                                $this->model_extension_module_ozon_seller->saveExportProduct($export_table, $task_id);
                                echo 'Категория ID '.$category_import['shop'].': товары успешно выгружены!<br />';
                            }
                        }

                    } else {
                        echo 'Категория ID '.$category_import['shop'].': нет товаров для экспорта<br />';
                    }
                }
            }
        }
    }

    // Обновление остатков, цен
    public function updateOzonProduct()
    {
        header('Content-Type: text/html; charset=utf-8');
        $status_module = $this->config->get('ozon_seller_status');
        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
            && $status_module
        ) {
            $this->updatePriceAndStocks();
            echo 'Готово';
        }
    }

    /* Обновить цену товара и остатки */
    private function updatePriceAndStocks()
    {
        $this->load->model('extension/module/ozon_seller');
        $ozon_seller_category = $this->config->get('ozon_seller_category');
        $start                = 0;
        $limit                = 100;
        do {
            $filter_data = array(
                'start' => $start,
                'limit' => $limit,
            );
            $products    = $this->model_extension_module_ozon_seller->updateProducts($filter_data);
            $start       += $limit;
            $prices      = array();
            $stocks      = array();

            foreach ($products as $product) {
                if (empty($product)) {
                    continue;
                }
                if ( ! empty($this->config->get('ozon_seller_highway'))) {
                    foreach ($ozon_seller_category as $seller_category) {
                        if ($product['category_id'] == $seller_category['shop']) {
                            $default_weight = $seller_category['weight'] / 1000;
                            $default_length = $seller_category['length'];
                            $default_width  = $seller_category['width'];
                            $default_height = $seller_category['height'];
                        }
                    }

                    if (empty($product['weight']) || empty($product['length']) || empty($product['width'])
                        || empty($product['height'])
                    ) {

                        if ( ! isset($default_height)) {
                            $this->log('Цены/остатки. Модель: '.$product['model'].' Артикул: '.$product['sku']
                                .' ошибка в размере по умолчанию. Скорее всего у товара в Opencart назначена главная категория, которая не сопоставлена в модуле с категорие Ozon. Чтобы ошибка не повторялась заполните размеры у товара или сопоставьте категории в модуле.',
                                0);

                            echo 'Цены/остатки. Модель: '.$product['model'].' Артикул: '.$product['sku']
                                .' ошибка в размере по умолчанию. Скорее всего у товара в Opencart назначена главная категория, которая не сопоставлена в модуле с категорие Ozon. Чтобы ошибка не повторялась заполните размеры у товара или сопоставьте категории в модуле.<br />';

                            continue;
                        }
                    }

                    if ($product['weight_class_id'] == $this->config->get('ozon_seller_weight')) {
                        if ($product['weight'] == 0) {
                            $weight = $default_weight;
                        } else {
                            $weight = $product['weight'] / 1000;
                        }
                    } else {
                        if ($product['weight'] == 0) {
                            $weight = $default_weight;
                        } else {
                            $weight = $product['weight'];
                        }
                    }

                    if ($product['length_class_id'] == $this->config->get('ozon_seller_length')) {

                        if ($product['length'] == 0) {
                            $length = $default_length;
                        } else {
                            $length = $product['length'];
                        }

                        if ($product['width'] == 0) {
                            $width = $default_width;
                        } else {
                            $width = $product['width'];
                        }

                        if ($product['height'] == 0) {
                            $height = $default_height;
                        } else {
                            $height = $product['height'];
                        }

                    } else {

                        if ($product['length'] == 0) {
                            $length = $default_length;
                        } else {
                            $length = $product['length'] / 10;
                        }

                        if ($product['width'] == 0) {
                            $width = $default_width;
                        } else {
                            $width = $product['width'] / 10;
                        }

                        if ($product['height'] == 0) {
                            $height = $default_height;
                        } else {
                            $height = $product['height'] / 10;
                        }
                    }

                    $volume_weight = (int)ceil($length) * (int)ceil($width) * (int)ceil($height) / 5000;

                    if ($weight > $volume_weight) {
                        $volume_weight = $weight;
                    }

                    $highway = $volume_weight * $this->config->get('ozon_seller_highway'); //магистраль

                    if ($highway < 5) {
                        $highway = 5;
                    } else {
                        if ($highway > 500) {
                            $highway = 500;
                        }
                    }

                } else {
                    $highway = 0;
                }

                if ($product['special']) {
                    $price = $product['special'] + $highway;
                    $price = round($price);
                } else {
                    $price = $product['price'] + $highway;
                    $price = round($price);
                }

                if ($this->config->get('ozon_seller_percent')) {
                    $percent  = $this->config->get('ozon_seller_percent');
                    $price_up = $price * $percent / 100;
                    $price    += round($price_up);
                }

                if ($this->config->get('ozon_seller_ruble')) {
                    $price += $this->config->get('ozon_seller_ruble');
                }

                if ($this->config->get('ozon_seller_last_mile')) {

                    $last_mile = $price * 4.4 / 100;

                    if ($last_mile < 50) {
                        $last_mile = 50;
                    } else {
                        if ($last_mile > 200) {
                            $last_mile = 200;
                        }
                    }

                    if ($this->config->get('ozon_seller_min_last_mile')
                        && $price < $this->config->get('ozon_seller_min_last_mile')
                    ) {
                        $price += $last_mile;
                    }

                    if (empty($this->config->get('ozon_seller_min_last_mile'))) {
                        $price += $last_mile;
                    }
                }

                //$offer_id
                if ($this->config->get('ozon_seller_entry_offer_id')) {

                    if (empty($product['model'])) {
                        continue;
                    }
                    $offer_id = $product['model'];

                } else {

                    if (empty($product['sku'])) {
                        continue;
                    }
                    $offer_id = $product['sku'];
                }

                //округление цен
                switch ($this->config->get('ozon_seller_price_round')) {
                    case 'st':
                        $price = round($price, -1);
                        break;
                    case 'ten':
                        $price = ceil($price / 10) * 10;
                        break;
                    case 'st_so':
                        $price = round($price, -2);
                        break;
                    case 'so':
                        $price = ceil($price / 100) * 100;
                        break;
                    case 'fifty':
                        $price = ceil($price / 50) * 50;
                        break;
                    default:
                        $price;
                        break;
                }

                //фиктивная акция
                if ( ! empty($this->config->get('ozon_seller_fictitious_price'))) {
                    $old_price          = ($price / 100 * $this->config->get('ozon_seller_fictitious_price')) + $price;
                    $prices['prices'][] = array(
                        'offer_id'  => $offer_id,
                        'old_price' => (string)ceil($old_price),
                        'price'     => (string)ceil($price),
                    );
                } else {
                    $prices['prices'][] = array(
                        'offer_id' => $offer_id,
                        'price'    => (string)ceil($price),
                    );
                }

                if ($this->config->get('ozon_seller_stocks_null')) {
                    $product_stock = 0;
                } elseif ($product['quantity'] < 0) {
                    $product_stock = 0;
                } else {
                    $product_stock = $product['quantity'];
                }

                //черный список
                if ($this->config->get('ozon_seller_product_blacklist')) {
                    $blacklist = $this->config->get('ozon_seller_product_blacklist');
                    if (in_array($product['product_id'], $blacklist)) {
                        $product_stock = 0;
                    }
                }

                $stocks['stocks'][] = array(
                    'offer_id'     => $offer_id,
                    'stock'        => (int)$product_stock,
                    'warehouse_id' => (int)$this->config->get('ozon_seller_warehouse_ozon'),
                );
            }

            $url      = 'https://api-seller.ozon.ru/v1/product/import/prices';
            $data     = $prices;
            $response = $this->makeRequest($url, 'POST', $data);

            if (isset($response['result'])) {

                foreach ($response['result'] as $respons) {
                    if ($respons['errors']) {
                        $this->log($respons['offer_id'].' ошибка обновления цены: '.json_encode($respons['errors'],
                                JSON_UNESCAPED_UNICODE), 0);
                    }
                }
            }

            $url    = 'https://api-seller.ozon.ru/v2/products/stocks';
            $data   = $stocks;
            $respon = $this->makeRequest($url, 'POST', $data);

            if ($this->config->get('ozon_seller_test_export')) {
                echo '<pre>';
                echo $this->config->get('ozon_seller_version');
                echo '<br />==== Остатки запрос ====<br />';
                print_r($stocks);
                echo '<br />==== Остатки ответ ====<br />';
                print_r($respon);
                echo '<br />==== Цены запрос ====<br />';
                print_r($prices);
                echo '<br />==== Цены ответ ====<br />';
                print_r($response);
            }

        } while (count($products) == $limit);
    }

    // Проверить статус добавления товара
    public function updateNewOzonProduct()
    {
        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {
            $this->load->model('extension/module/ozon_seller');
            $ozon_task_id = $this->model_extension_module_ozon_seller->chekTaskId();
            $i            = 0;

            if ( ! empty($ozon_task_id)) {
                $this->log_process('Начинаю проверку...');
                foreach ($ozon_task_id as $task_id) {
                    $products_export = $this->checkImportProduct($task_id['task_id']);

                    if ( ! empty($products_export['result']['items'])) {
                        foreach ($products_export['result']['items'] as $product_export) {
                            $status          = htmlspecialchars($product_export['status']);
                            $offer_id        = htmlspecialchars($product_export['offer_id']);
                            $ozon_product_id = htmlspecialchars($product_export['product_id']);
                            $my_product
                                             = $this->model_extension_module_ozon_seller->getExportProduct($ozon_product_id);
                            if ($my_product[0]['status'] == 'processed') {
                                continue;
                            }
                            $ozon_sku = 0;
                            $error    = '';

                            if ($status == 'imported') {
                                $chek_product = $this->getOzonProduct($offer_id);
                                if ( ! empty($chek_product['result'])) {
                                    $status = htmlspecialchars($chek_product['result']['status']['validation_state']);
                                    if ($status == 'success') {
                                        $status = 'processed';
                                    }
                                    foreach ($chek_product['result']['sources'] as $ozon_prod) {
                                        if ($ozon_prod['source'] == 'fbs') {
                                            $ozon_sku = htmlspecialchars($ozon_prod['sku']);
                                            break;
                                        }
                                    }
                                    if (empty($ozon_sku) && $status == 'processed') {
                                        $status = 'imported';
                                    }
                                }
                                if ($status != 'processed') {
                                    $error .= htmlspecialchars($chek_product['result']['status']['state_description'])
                                        .'<br />';
                                    if ( ! empty($product_export['errors'])) {
                                        $product_errors = $product_export['errors'];
                                        foreach ($product_errors as $product_error) {
                                            $error .= htmlspecialchars($product_error['attribute_name']).'<br />'
                                                .htmlspecialchars($product_error['description']).'<br />';
                                        }
                                    }
                                }
                            }
                            $i++;
                            $this->log_process('Проверяю товары со статусом не processed. Проверено: '.$i);
                            $this->model_extension_module_ozon_seller->updateExportProduct($status, $offer_id,
                                $ozon_sku, $ozon_product_id, $error);
                        }
                    }
                }
                $this->log_process('Готово. Проверено '.$i.'. Перезагрузите страницу');
            }
        }
    }

    /* Обновить данные товара */
    public function changeOzonProducts()
    {

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {

            if (isset($this->request->get['product_shop_id'])) {

                $this->load->model('extension/module/ozon_seller');

                $this->model_extension_module_ozon_seller->deletedExportProduct($this->request->get['product_shop_id']);
            }

        }
    }

    /* Метод - проверить статус товара после иморта */
    private function checkImportProduct($task_id)
    {

        $url = 'https://api-seller.ozon.ru/v1/product/import/info';

        $data = array('task_id' => $task_id);

        $response = $this->makeRequest($url, 'POST', $data);

        return $response;

    }

    /* Перенести товар в архив и удалить */
    public function archive()
    {

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {

            if (isset($this->request->get['product_id'])) {

                $this->load->model('extension/module/ozon_seller');

                $url = 'https://api-seller.ozon.ru/v1/product/archive';

                $data = array('product_id' => array($this->request->get['product_id']));

                $response = $this->makeRequest($url, 'POST', $data);

                if ($response['result']) {

                    $ozon_product
                        = $this->model_extension_module_ozon_seller->getExportProduct($this->request->get['product_id']);

                    if ( ! empty($ozon_product)) {

                        if ($this->config->get('ozon_seller_entry_offer_id')) {

                            $offer_id = $ozon_product[0]['model'];

                        } else {

                            $offer_id = $ozon_product[0]['sku'];
                        }

                        $url = 'https://api-seller.ozon.ru/v2/products/delete';

                        $data = array('products' => array(array('offer_id' => $offer_id)));

                        $respon = $this->makeRequest($url, 'POST', $data);

                        if ($respon['status'][0]['is_deleted']) {

                            $this->model_extension_module_ozon_seller->deletedExportProduct($ozon_product['product_id']);

                            echo "товар $offer_id удален";

                        } else {
                            echo 'Ошибка при добавлении товара в архив: '.json_encode($respon);
                        }
                    }

                } else {
                    echo $response['message'];
                }
            }
        }
    }

    /* Скачать производителей с Ozon */
    public function manufacturerDownload()
    {

        header('Content-Type: text/html; charset=utf-8');

        $manufacturerAttributeId = 85;

        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {

            $this->load->model('extension/module/ozon_seller');

            $url = 'https://api-seller.ozon.ru/v2/category/attribute/values';

            $category = $this->request->get['category'];

            $count_response = 5000;

            $this->log_process('Загружаю производителей для категории '.$category.'. Загружено: '.$count_response);

            $data = array('category_id' => $category, 'attribute_id' => $manufacturerAttributeId, 'limit' => 5000);

            $response = $this->makeRequest($url, 'POST', $data);

            foreach ($response['result'] as $result) {
                $ozon_id = htmlspecialchars($result['id']);
                $value   = htmlspecialchars($result['value']);

                if ( ! empty($result['picture'])) {
                    $picture = $result['picture'];
                } else {
                    $picture = '';
                }

                $this->model_extension_module_ozon_seller->saveManufacturer($ozon_id, $value, $picture);
            }

            if ($response['has_next']) {
                do {
                    $last_key      = array_key_last($response['result']);
                    $last_value_id = $response['result'][$last_key]['id'];
                    $data          = array(
                        'category_id'   => $category,
                        'attribute_id'  => $manufacturerAttributeId,
                        'limit'         => 5000,
                        'last_value_id' => $last_value_id,
                    );

                    $response = $this->makeRequest($url, 'POST', $data);

                    foreach ($response['result'] as $result) {
                        $ozon_id = htmlspecialchars($result['id']);
                        $value   = htmlspecialchars($result['value']);

                        if ( ! empty($result['picture'])) {
                            $picture = $result['picture'];
                        } else {
                            $picture = '';
                        }

                        $this->model_extension_module_ozon_seller->saveManufacturer($ozon_id, $value, $picture);
                    }

                    $count_response += count($response['result']);

                    $this->log_process('Загружаю производителей для категории '.$category.'. Загружено: '
                        .$count_response);

                } while ($response['has_next']);
            }

            $this->log_process('Производители успешно загружены!');
        }
    }

    //отправка письма на почту
    private function sendMail($theme, $message)
    {
        $mail                = new Mail();
        $mail->protocol      = $this->config->get('config_mail_protocol');
        $mail->parameter     = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port     = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout');
        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject(html_entity_decode($theme, ENT_QUOTES, 'UTF-8'));
        $sended_message = html_entity_decode($message, ENT_QUOTES, 'UTF-8');
        $mail->setHtml($sended_message);
        $mail->send();
    }

    private function translit($str)
    {
        $rus = array(
            'А',
            'Б',
            'В',
            'Г',
            'Д',
            'Е',
            'Ё',
            'Ж',
            'З',
            'И',
            'Й',
            'К',
            'Л',
            'М',
            'Н',
            'О',
            'П',
            'Р',
            'С',
            'Т',
            'У',
            'Ф',
            'Х',
            'Ц',
            'Ч',
            'Ш',
            'Щ',
            'Ъ',
            'Ы',
            'Ь',
            'Э',
            'Ю',
            'Я',
            'а',
            'б',
            'в',
            'г',
            'д',
            'е',
            'ё',
            'ж',
            'з',
            'и',
            'й',
            'к',
            'л',
            'м',
            'н',
            'о',
            'п',
            'р',
            'с',
            'т',
            'у',
            'ф',
            'х',
            'ц',
            'ч',
            'ш',
            'щ',
            'ъ',
            'ы',
            'ь',
            'э',
            'ю',
            'я',
        );
        $lat = array(
            'A',
            'B',
            'V',
            'G',
            'D',
            'E',
            'E',
            'Gh',
            'Z',
            'I',
            'Y',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'R',
            'S',
            'T',
            'U',
            'F',
            'H',
            'C',
            'Ch',
            'Sh',
            'Sch',
            'Y',
            'Y',
            'Y',
            'E',
            'Yu',
            'Ya',
            'a',
            'b',
            'v',
            'g',
            'd',
            'e',
            'e',
            'gh',
            'z',
            'i',
            'y',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'r',
            's',
            't',
            'u',
            'f',
            'h',
            'c',
            'ch',
            'sh',
            'sch',
            'y',
            'y',
            'y',
            'e',
            'yu',
            'ya',
        );

        return str_replace($rus, $lat, $str);
    }

    private function createOrderOc($info)
    {
        $posting_number = $info['posting_number'];
        $shipment_date  = date('Y-m-d H:i:s', strtotime($info['shipment_date']));
        $products       = $info['products'];
        $status         = htmlspecialchars($info['status']);
        $barcodes       = $info['barcodes'];
        $analytics_data = $info['analytics_data'];

        $this->load->model('extension/module/ozon_seller');
        $this->load->model('checkout/order');

        $stop                   = '';
        $subtotal               = 0;
        $total                  = 0;
        $totals                 = array();
        $order_data['products'] = array();

        foreach ($products as $product) {
            $get_product_id = $this->model_extension_module_ozon_seller->getExportProduct($product['sku']);

            if (empty($get_product_id)) {
                if ($this->config->get('ozon_seller_entry_offer_id')) {
                    $get_product_id
                        = $this->model_extension_module_ozon_seller->getProductByModel($product['offer_id']);
                }
                if (empty($this->config->get('ozon_seller_entry_offer_id'))) {
                    $get_product_id = $this->model_extension_module_ozon_seller->getProductBySku($product['offer_id']);
                }
            }
            $product_id = $get_product_id[0]['product_id'];

            if (empty($product_id)) {
                $this->log($posting_number.' ошибка: товар или комплект '.$product['offer_id']
                    .' не найден в товарах Opencart. Заказ будет перевыгружен.');
                $stop = 'stop';
                break;
            }

            $product_info = $this->model_extension_module_ozon_seller->getProduct($product_id);

            if ($this->config->get('ozon_seller_product_price_oc')) {
                if ($product_info['special']) {
                    $product_price = $product_info['special'];
                } else {
                    $product_price = $product_info['price'];
                }
            } else {
                $product_price = $product['price'];
            }

            $order_data['products'][] = array(
                'product_id' => $product_info['product_id'],
                'name'       => $product_info['name'],
                'model'      => $product_info['model'],
                'option'     => array(),
                'download'   => array(),
                'quantity'   => $product['quantity'],
                //'subtract'   => $product['subtract'],
                'price'      => $product_price,
                'total'      => $product_price * $product['quantity'],
                'tax'        => 0,
                'reward'     => 0,
            );
            $subtotal                 += $product_price * $product['quantity'];
        }

        $total          = $subtotal;
        $id_ozon_client = substr($posting_number, 0, 8);
        $currency       = 'RUB';

        if (empty($info['customer'])) {
            $firstname       = 'Ozon FBS';
            $lastname        = $posting_number;
            $telephone       = $id_ozon_client;
            $zip_code        = '';
            $adress          = $analytics_data['delivery_type'];
            $delivery_method = $analytics_data['delivery_type'];
        } else {
            $fio             = explode(' ', $info['customer']['name']);
            $firstname       = $fio[0];
            $lastname        = $fio[1];
            $telephone       = '+'.$info['customer']['phone'];
            $zip_code        = $info['customer']['address']['zip_code'];
            $adress          = $info['customer']['address']['address_tail'];
            $delivery_method = $info['delivery_method']['name'];
        }

        $order_data['invoice_prefix']    = $this->config->get('config_invoice_prefix');
        $order_data['store_id']          = $this->config->get('config_store_id');
        $order_data['store_name']        = $this->config->get('config_name');
        $order_data['store_url']         = ($this->config->get('config_store_id') ? $this->config->get('config_url')
            : HTTP_SERVER);
        $order_data['customer_id']       = 0;
        $order_data['customer_group_id'] = 1;
        $order_data['firstname']         = $firstname;
        $order_data['lastname']          = $lastname;
        $order_data['email']             = $id_ozon_client.'@ozon.ru';
        $order_data['telephone']         = $telephone;
        $order_data['fax']               = '';

        $order_data['payment_firstname']      = $firstname;
        $order_data['payment_lastname']       = $lastname;
        $order_data['payment_company']        = '';
        $order_data['payment_company_id']     = '';
        $order_data['payment_tax_id']         = '';
        $order_data['payment_address_1']      = $adress;
        $order_data['payment_address_2']      = '';
        $order_data['payment_city']           = $analytics_data['city'];
        $order_data['payment_postcode']       = $zip_code;
        $order_data['payment_country']        = 'Российская Федерация';
        $order_data['payment_country_id']     = 176;
        $order_data['payment_zone']           = '';
        $order_data['payment_zone_id']        = '';
        $order_data['payment_address_format'] = '';
        $order_data['payment_method']         = $analytics_data['payment_type_group_name'];
        $order_data['payment_code']           = 'cod';

        $order_data['shipping_firstname']      = $firstname;
        $order_data['shipping_lastname']       = $lastname;
        $order_data['shipping_company']        = '';
        $order_data['shipping_address_1']      = $adress;
        $order_data['shipping_address_2']      = '';
        $order_data['shipping_city']           = $analytics_data['city'];
        $order_data['shipping_postcode']       = $zip_code;
        $order_data['shipping_country']        = 'Российская Федерация';
        $order_data['shipping_country_id']     = 176;
        $order_data['shipping_zone']           = '';
        $order_data['shipping_zone_id']        = '';
        $order_data['shipping_address_format'] = '';
        $order_data['shipping_method']         = $delivery_method;
        $order_data['shipping_code']           = '';

        $order_data['comment'] = 'Дата отгрузки: '.$shipment_date.'<br />';
        if ( ! empty($info['customer'])) {
            $order_data['comment'] .= $posting_number.'<br />';
        }
        if ( ! empty($barcodes)) {
            $order_data['comment'] .= $barcodes['upper_barcode'].'<br />'.$barcodes['lower_barcode'].'<br />';
        }
        if ( ! empty($info['customer']['address']['comment'])) {
            $order_data['comment'] .= $info['customer']['address']['comment'].'<br />';
        }

        $order_data['total']           = $total;
        $order_data['order_status_id'] = 0;
        // $order_data['order_status'] = 'Ошибочные заказы';
        $order_data['affiliate_id']    = 0;
        $order_data['commission']      = 0;
        $order_data['language_id']     = $this->config->get('config_language_id');
        $order_data['currency_id']     = $this->currency->getId($currency);
        $order_data['currency_code']   = $currency;
        $order_data['currency_value']  = $this->currency->getValue($currency);
        $order_data['ip']              = $this->request->server['REMOTE_ADDR'];
        $order_data['forwarded_ip']    = (isset($this->request->server['HTTP_X_FORWARDED_FOR'])
            ? $this->request->server['HTTP_X_FORWARDED_FOR'] : $this->request->server['REMOTE_ADDR']);
        $order_data['user_agent']      = 'Yandex Robot';
        $order_data['accept_language'] = (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])
            ? $this->request->server['HTTP_ACCEPT_LANGUAGE'] : '');
        $order_data['vouchers']        = array();
        $order_data['marketing_id']    = 0;
        $order_data['tracking']        = '';

        $order_data['payment_custom_field'] = array(
            'name'       => 'Способ оплаты',
            'value'      => $analytics_data['payment_type_group_name'],
            'sort_order' => 1,
        );

        $order_data['totals'][] = array(
            'code'       => 'sub_total',
            'title'      => 'Сумма',
            'text'       => $this->currency->format($subtotal, $currency),
            'value'      => $subtotal,
            'sort_order' => 1,
        );
        //доставка бесплатная
        $shipping_price         = 0;
        $total                  += $shipping_price;
        $order_data['totals'][] = array(
            'code'       => 'shipping',
            'title'      => 'Доставка',
            'text'       => $this->currency->format($shipping_price, $currency),
            'value'      => 0,
            'sort_order' => 2,
        );
        $order_data['totals'][] = array(
            'code'       => 'total',
            'title'      => 'Итого',
            'text'       => $this->currency->format($total, $currency),
            'value'      => $total,
            'sort_order' => 3,
        );

        if (empty($stop)) {
            $order_id = $this->model_checkout_order->addOrder($order_data);
            $this->changeOrderStatusOC($order_id, $this->config->get('ozon_seller_status_new'));
        }
    }

    private function changeOrderStatusOC($order_id, $order_status_id)
    {
        $this->load->model('checkout/order');
        $this->model_checkout_order->addOrderHistory($order_id, $order_status_id, '', false);
    }

    // Получить склады в Озон
    public function warehouseOzon()
    {
        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {
            $url      = 'https://api-seller.ozon.ru/v1/warehouse/list';
            $response = $this->makeRequest($url, 'POST');
            if ( ! empty($response['result'])) {
                echo json_encode($response['result']);
            } else {
                echo 'error';
            }
        }
    }

    // Связать товары из Озона с товарами в магазине
    public function downloadProduct()
    {
        if (isset($this->request->get['cron_pass'])
            && $this->request->get['cron_pass'] == $this->config->get('ozon_seller_cron_pass')
        ) {
            $url       = 'http://api-seller.ozon.ru/v1/product/list';
            $page_size = 500;
            $page      = 1;
            $products  = array();
            do {
                $data          = array(
                    'page_size' => $page_size,
                    'page'      => $page,
                );
                $response      = $this->makeRequest($url, 'POST', $data);
                $products      = array_merge($products, $response['result']['items']);
                $page          += 1;
                $count_product = count($products) + 1;
            } while ($count_product < $response['result']['total']);

            $i = 0;
            if ( ! empty($products)) {
                $this->load->model('extension/module/ozon_seller');
                $skip = '';
                $add  = '';
                foreach ($products as $product) {
                    $ozon_product_id = $product['product_id'];
                    $check           = $this->model_extension_module_ozon_seller->getExportProduct($ozon_product_id);
                    if (empty($check[0])) {
                        if ($this->config->get('ozon_seller_entry_offer_id')) {
                            $product_info
                                = $this->model_extension_module_ozon_seller->getProductByModel($product['offer_id']);
                        } else {
                            $product_info
                                = $this->model_extension_module_ozon_seller->getProductBySku($product['offer_id']);
                        }
                        if ( ! empty($product_info[0])) {
                            $product_id = $product_info[0]['product_id'];
                            $model      = $product_info[0]['model'];
                            $sku        = $product_info[0]['sku'];
                            $this->model_extension_module_ozon_seller->downloadProduct($product_id, $model, $sku,
                                $ozon_product_id);
                            $add .= $model.', ';
                            $i++;
                        } else {
                            $skip .= '     '.$product['offer_id'].', ';
                        }
                    }
                }
            }
            if ( ! empty($skip)) {
                $skip_alert = "\nПропущенные смотрите в логе модуля.";
                $this->log('Связка товаров. Не найден в OC: '.$skip, 0);
            } else {
                $skip_alert = '';
            }

            if ( ! empty($add)) {
                $this->log('Связка товаров. Связаны: '.$add, 0);
            }
            echo 'Товаров связано: '.$i.'.'.$skip_alert;
        }
    }

    // Создать акт приема передачи и накладной
    private function getActId()
    {
        $date     = date('Y-m-d');
        $inf_file = 'act_ozon_seller.txt';

        if (file_exists(DIR_UPLOAD.$inf_file)) {
            $act_data = unserialize(file_get_contents(DIR_UPLOAD.$inf_file));

            if ($act_data['ozon_seller']['act']['date'] != $date) {
                unset($act_data['ozon_seller']['act']);
                $act_data['ozon_seller']['act']['date'] = $date;
            }
        } else {
            $act_data['ozon_seller']['act']['date'] = $date;
        }

        $check_download_act = glob(DIR_UPLOAD.$date.'_act_ozon_seller*.pdf');

        if (empty($check_download_act) && isset($act_data['ozon_seller']['act']['id'])) {
            $this->getAct($act_data['ozon_seller']['act']['id']);
        }

        if ( ! empty($check_download_act)) {
            $act_data['ozon_seller']['act']['print'] = 1;
        } else {
            if (empty($check_download_act) && empty($act_data['ozon_seller']['act']['id'])) {
                $url                = 'https://api-seller.ozon.ru/v3/posting/fbs/unfulfilled/list';
                $delivery_method_id = array();
                $stop               = 0;
                $offset             = 0;
                do {
                    $data     = array(
                        'dir'    => 'asc',
                        'filter' => array(
                            'cutoff_from'  => $date.'T00:00:01Z',
                            'cutoff_to'    => $date.'T23:59:59Z',
                            'warehouse_id' => array((int)$this->config->get('ozon_seller_warehouse_ozon')),
                        ),
                        'limit'  => '50',
                        'offset' => $offset,
                    );
                    $response = $this->makeRequest($url, 'POST', $data);

                    if ( ! empty($response['result']['postings'])) {
                        foreach ($response['result']['postings'] as $posting) {
                            if ($posting['status'] == 'awaiting_packaging') {
                                $act_data['ozon_seller']['act']['error'] = 'Не все заказы собраны';
                                $stop                                    = 1;
                                break 2;
                            } elseif ($posting['status'] == 'delivering') {
                                $act_data['ozon_seller']['act']['error'] = 'Заказы уже сданы';
                                $stop                                    = 1;
                                break 2;
                            } else {
                                if ( ! in_array($posting['delivery_method']['id'], $delivery_method_id)) {
                                    $delivery_method_id[] = $posting['delivery_method']['id'];
                                }
                            }
                        }
                    } else {
                        $stop                                    = 1;
                        $act_data['ozon_seller']['act']['error'] = 'На сегодня нет заказов';
                    }
                    $offset += 50;
                } while ($response['result']['count'] == 50);

                if ($stop == 0) {
                    $url = 'https://api-seller.ozon.ru/v2/posting/fbs/act/create';
                    $id  = array();
                    foreach ($delivery_method_id as $method_id) {
                        $data     = array('delivery_method_id' => (int)$method_id);
                        $response = $this->makeRequest($url, 'POST', $data);

                        if (isset($response['result']['id'])) {
                            $id[] = $response['result']['id'];
                        }
                    }

                    if ( ! empty($id)) {
                        $act_data['ozon_seller']['act']['id'] = $id;
                        unset($act_data['ozon_seller']['act']['error']);
                    }
                }
            }
        }
        $act_data = serialize($act_data);
        file_put_contents(DIR_UPLOAD.$inf_file, $act_data);
    }

    private function getAct($ids)
    {
        $date = date('Y-m-d');
        foreach ($ids as $id) {
            $url      = 'https://api-seller.ozon.ru/v2/posting/fbs/act/check-status';
            $data     = array('id' => (int)$id);
            $response = $this->makeRequest($url, 'POST', $data);

            if ( ! empty($response['result']['status'])) {
                if ($response['result']['status'] == 'ready') {
                    $url    = 'https://api-seller.ozon.ru/v2/posting/fbs/act/get-pdf';
                    $respon = $this->request2($url, $data);
                    file_put_contents(DIR_UPLOAD.$date.'_act_ozon_seller'.$id.'.pdf', $respon);
                }

                if ($response['result']['status'] == 'error'
                    || $response['result']['status'] == "The next postings aren't ready"
                ) {
                    $this->log('Акт и ТТН: в Озоне произошла ошибка при формировании документов');
                }
            }
        }
    }

    private function makeRequest($url, $request, $data = [])
    {

        ini_set('serialize_precision', -1); //патч бага php7.3
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Host: api-seller.ozon.ru',
            'Client-Id: '.$this->config->get('ozon_seller_client_id'),
            'Api-Key: '.$this->config->get('ozon_seller_api_key'),
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        switch (mb_strtoupper($request)) {
            case "GET":
                break;
            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                $data = json_encode($data);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case "POST":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                if ( ! empty($data)) {
                    $data = json_encode($data);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "DELETE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }
        $response = curl_exec($ch);
        curl_close($ch);
        $response = @json_decode($response, true);

        return $response;
    }

    /* Этикетка */
    private function request2($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: api-seller.ozon.ru',
            'Client-Id: '.$this->config->get('ozon_seller_client_id'),
            'Api-Key: '.$this->config->get('ozon_seller_api_key'),
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $data = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * Писать в журнал ошибки и сообщения
     *
     * @param string $msg   запись
     * @param int    $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
     **/
    private function log($msg, $level = 0)
    {
        if ($level > $this->LOG_LEVEL) {
            return;
        }
        $fp = fopen(DIR_LOGS.'ozon_seller.log', 'a');
        fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
        if ($this->ECHO) {
            echo nl2br(htmlspecialchars($msg))."<br/>\n";
        }
        fclose($fp);
    }

    private function log_process($msg)
    {
        $fp = fopen(DIR_SYSTEM.'ozon_seller_process.txt', 'w+');
        fwrite($fp, str_replace("\n", '', $msg)."\n");
        if ($this->ECHO) {
            echo nl2br(htmlspecialchars($msg))."<br/>\n";
        }
        fclose($fp);
    }

}

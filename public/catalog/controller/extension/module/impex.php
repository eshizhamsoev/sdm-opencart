<?php

set_time_limit(1200);
ini_set('max_execution_time', 1200);

class ControllerExtensionModuleImpex extends Controller {
	private $error = array();

	public function index() {
		
		$this->log("Запущен импорт по крону", 2);
		
		$this->log("impex.v63 (20211224)", 2);
		
		$this->cronImport();
		
		exit();
			
	}
	
	public function cronImport() {

		$error = '';
		
		$cache = str_replace('catalog/', 'admin/', DIR_APPLICATION) . 'impex/cron/';
		
		if (file_exists($cache . 'impex_busy.txt')) {
			
			if (time() - filemtime($cache . 'impex_busy.txt') > 30 * 60) {
				unlink($cache . 'impex_busy.txt');
				$this->log('Просроченный стопер ' . $cache . 'impex_busy.txt' . ' удален', 2);
			} else {
				$this->log("cronImport() загружает файл. Будем ждать.", 2);
				echo 'cronImport() загружает файл. Будем ждать. ' . date('Y-m-d H:i:s') . '<br>';
				exit();
			}
		}
		
		$list = scandir($cache, 0);	
		$files = array();	
		foreach ($list as $file) {
			if (strpos($file, '.zip') > 0 || strpos($file, '.xml') > 0) {
        		$files[$file] = filemtime($cache . '/' . $file);
			}
		}
		
		asort($files);
		$files = array_keys($files);
		
		$this->log("count = " . count($files), 2);
		$this->log("cache = " . $cache, 2);
		$this->log(print_r($files, 1), 2);
		
		if (count($files) > 0) {
			
			$fp = fopen($cache . 'impex_busy.txt', 'w');
			fwrite($fp, '');
			fclose($fp); 
			
			$file = $files[0];
			
			$this->log('Импорт файла ' . $file . ' начат', 2);
			echo 'Импорт файла ' . $file . ' начат ' . date('Y-m-d H:i:s') . '<br>';
			
		} else {
			
			if (file_exists($cache . 'impex_busy.txt')) {
				
				unlink($cache . 'impex_busy.txt');
				
			}
			
			$this->log("cronImport() - нет файлов для импорта по крону", 2);
			echo 'cronImport() - нет файлов для импорта по крону ' . date('Y-m-d H:i:s') . '<br>';
			exit();
		}
		
		$path_info = pathinfo($file);
		$filename = $path_info['filename'];
		$extension = $path_info['extension'];
		
		//$this->log($path_info, 2);
		$this->log($filename, 2);
		$this->log($extension, 2);
		
		$zip_support = class_exists('ZipArchive') ? true : false;
		
		if ($extension == 'zip') {

$this->log("-------zip-----", 2);
		
			if (!$zip_support) {
				$this->log("Ваш PHP не поддерживает ZIP архивы, отсутствует класс ZipArchive", 2);
				return;
			}

			$xmlFiles = $this->extractZip($cache . $file, $error);

			if ($error) {
				$this->log($error, 2);
				return;
			}

//$this->log('========================', 2);
$this->log($xmlFiles, 2);
//$this->log('========================', 2);

			if (!count($xmlFiles)) {
				$this->log("Архив пустой или запароленный", 2);
				return;
			}
			
//exit();			
			
			foreach ($xmlFiles as $xmlFile) {
				$this->log('Обрабатывается файл: ' . $xmlFile, 2);
				$this->log('Полный путь к файлу: ' . $cache . $xmlFile, 2);
				
				if ($this->config->get('module_impex_stats')) {
				
					$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product");
					$total = $query->row['total'];
					$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_count SET count = '" . (int)$total . "', file = '" . $this->db->escape($xmlFile) . "', date_added = NOW()");
				
				}
				
				$error = $this->modeImport($cache . $xmlFile);
				
				if ($this->config->get('module_impex_stats')) {
				
					$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product");
					$total = $query->row['total'];
					$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_count SET count = '" . (int)$total . "', file = '" . $this->db->escape($xmlFile) . "', date_added = NOW()");
				
				}
				
				if ($error) {
					return;
				} else {
					
					unlink($cache . $xmlFile);
					$this->log("Удален файл: " . $cache . $xmlFile, 2);
					
				}
			}

		} elseif ($extension == 'xml') {
			
		$this->log("-------xml-----", 2);
		
			if ($this->config->get('module_impex_stats')) {
			
				$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product");
				$total = $query->row['total'];
				$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_count SET count = '" . (int)$total . "', file = '" . $this->db->escape($file) . "', date_added = NOW()");
			
			}
				
			$error = $this->modeImport($cache . $file);
				
			if ($this->config->get('module_impex_stats')) {
			
				$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product");
				$total = $query->row['total'];
				$this->db->query("INSERT INTO " . DB_PREFIX . "catalog_count SET count = '" . (int)$total . "', file = '" . $this->db->escape($file) . "', date_added = NOW()");
			
			}
				
			if ($error) {
				return;
			}

		} else {
			return;
		}
		
		$done = $cache . 'done/';
		if(!is_dir($done)) mkdir($done,0755,TRUE);
		
		$ttl = $this->config->get('module_impex_temp_ttl');
		$files = glob($done.'*.*');
		if ($files) {
			foreach ($files as $oldfile) {
				if (filemtime($oldfile) < time() - $ttl * 60) {
					unlink($oldfile);
$this->log($oldfile . ' удален', 2);
				}
			}
		}
		
		$newfile = $done . $file;
		
		if (!copy($cache . $file, $newfile)) {
			$this->log('не удалось скопировать файл "' . $cache . $file, 2);
			return;
		}
		
		if (!unlink($cache . $file)) {
			$this->log('не удалось удалить файл "' . $cache . $file, 2);
			return;
		}
		
		$this->log( "[i] Обмен прошел без ошибок", 2);
		$this->log('Удален файл "' . $cache . $file . '". Копия сохранена в ' . $done, 2);
		
		unlink($cache . 'impex_busy.txt');
		
		$this->cronImport();
		
		//exit();
		
	} 
	
	private function extractZip($zipFile, &$error) {

		$xmlFiles = array();
		$imgFiles = 0;
		//$cache = DIR_CACHE . 'impex/';
		$cache = str_replace('catalog/', 'admin/', DIR_APPLICATION) . 'impex/cron/';
		
//		$ttl = 180;
//		$files = glob($cache.'*.*');
//		if ($files) {
//			foreach ($files as $file) {
//				if (filemtime($file) < time() - $ttl * 60) {
//					unlink($file);
//$this->log($file . ' удален', 2);
//				}
//			}
//		}

		$zipArc = zip_open($zipFile);
		if (is_resource($zipArc)) {
			$this->log("Начата распаковка архива: " . $zipFile);

			while ($zip_entry = zip_read($zipArc)) {
				$name = zip_entry_name($zip_entry);
				//$this->log("Имя файла: " . $name, 2);
				$pos = stripos($name, 'impex_files');

				if ($pos !== false) {
					
//$this->log("Здесь распаковываем картинки ==================== ", 2);
					
					$error = $this->extractImage($zipArc, $zip_entry, substr($name, $pos));
					if ($error) return $xmlFiles;
					$imgFiles ++;
				} else {
					$error = $this->extractXML($zipArc, $zip_entry, $name, $xmlFiles);
					if ($error) return $xmlFiles;
				}
			}

			if ($imgFiles) {
				$this->log('Распаковано картинок: ' . $imgFiles);
			}
			
			//$this->log("Завершена распаковка архива " . $zipFile, 2);

		} else {
			return $xmlFiles;
		}

		zip_close($zipArc);
		$this->log("Получены XML файлы:", 2);
		$this->log($xmlFiles, 2);
		return $xmlFiles;

	}

	private function extractImage($zipArc, $zip_entry, $name) {
		
		//$this->log("Распаковка картинок,  name = " . $name, 2);

		$error = "";

		if (substr($name, -1) == "/") {

			// проверим каталог
			if (is_dir(DIR_IMAGE . $name)) {
				//$this->log('[zip] directory exist: '.$name, 2);

			} else {
				//$this->log('[zip] create directory: '.$name, 2);
				@mkdir(DIR_IMAGE . $name, 0775) or die ($error = "Ошибка создания директории '" . DIR_IMAGE . $name . "'");
				if ($error) return $error;
				$this->log("Создана директория: '" . $name . "'", 2);
			}

		} elseif (zip_entry_open($zipArc, $zip_entry, "r")) {

			$dump = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			//$this->log("Распаковка картинки: '" . $name . "'", 2);

			$error = $this->checkDirectories($name);
			if ($error) return $error;

			if (is_file(DIR_IMAGE . $name)) {
				// Если файл существует
				$size_dump = strlen($dump);
				$size_file = filesize(DIR_IMAGE . $name);

			 	if ($size_dump != $size_file) {
			 		// файл был изменен, нужно заменить
			 		//$this->log("Файл '" . $name . "' изменен, старый размер " . $size_file . ", новый " . $size_dump);

					$fd = @fopen(DIR_IMAGE . $name, "wb");
					if ($fd === false) {
						return "Ошибка записи файла: '" . DIR_IMAGE . $name . "'";
					}
					fwrite($fd, $dump);
					fclose($fd);

			 	}

			} else {

				// для безопасности проверим, не является ли этот файл php
				$pos = strpos($dump, "<?php");

				if ($pos !== false) {
					$this->log("[!] ВНИМАНИЕ Файл '" . $name . "' является PHP скриптом и не будет записан!");

				} else {

					$fd = @fopen(DIR_IMAGE . $name, "wb");
					if ($fd === false) {
						return "Ошибка создания файла: " . DIR_IMAGE . $name . ", проверьте права доступа!";
					}
					fwrite($fd, $dump);
					//$this->log("Создан файл: " . $name);
					fclose($fd);

					// для безопасности проверим, является ли этот файл картинкой
//					$image_info = getimagesize(DIR_IMAGE.$name);
//					if ($image_info == NULL) {
//						$this->log("[!] ВНИМАНИЕ Файл '" . $name . "' не является картинкой, и будет удален!");
//						unlink(DIR_IMAGE.$name);
//					}
				}
			}
			zip_entry_close($zip_entry);
		}

		//$this->log("Завершена распаковка картинки", 2);
		return $error;

	} 

	private function extractXML($zipArc, $zip_entry, $name, &$xmlFiles) {

		$error = "";
		
		$this->log("Распаковка XML,  name = " . $name, 2);

		$cache = str_replace('catalog/', 'admin/', DIR_APPLICATION) . 'impex/cron/';
		
//		$ttl = 180;
//		$files = glob($cache.'*.*');
//		if ($files) {
//			foreach ($files as $file) {
//				if (filemtime($file) < time() - $ttl * 60) {
//					unlink($file);
//$this->log($file . ' удален', 2);
//				}
//			}
//		}

		if (substr($name, -1) == "/") {
			// это директория
			if (is_dir($cache . $name)) {
				//$this->log("Каталог существует: " . $name, 2);
			} else {
				//$this->log("Создание каталога: " . $name, 2);
				@mkdir($cache . $name, 0775) or die ($error = "Ошибка создания директории '" . $cache . $name . "'");
				if ($error) return $error;
			}

		} elseif (zip_entry_open($zipArc, $zip_entry, "r")) {

			//$this->log($cache . $name, 2);
			$dump = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
			
//$this->log("$dump: " . $dump, 2);

			// Удалим существующий файл
//			if (file_exists($cache . $name)) {
//				unlink($cache . $name);
//				//$this->log("Удален старый файл: " . $cache . $name, 2);
//			}

			// для безопасности проверим, является ли этот файл XML
			$str_xml = substr($dump, 1, 35);
			//$this->log($str_xml, 2);

			$path_obj = explode('/', $name);
			$filename = array_pop($path_obj);
			$path = "";

			// Если есть каталоги, нужно их создать
			foreach ($path_obj as $dir_name) {
				if (!is_dir($cache . $path . $dir_name)) {
					@mkdir($cache . $path . $dir_name, 0775) or die ($error = "Ошибка создания директории '" . $cache . $path . $dir_name . "'");
					$this->log("Создание папки: " . $dir_name, 2);
					$this->log("Путь к файлу в кэше: " . $path, 2);
				} else {
					//$this->log("Папка уже существует: " . $path . $dir_name, 2);
				}
				$path .= $dir_name . "/";
			}

			if ($fd = @fopen($cache . $path . $filename, "wb")) {

				$xmlFiles[] = $path . $filename;
				//$this->log("Создан файл: " . $path . $filename, 2);
				fwrite($fd, $dump);
				fclose($fd);
				
		$this->log(' ------------------- ', 2);
		$this->log($xmlFiles, 2);

			} else {

				$this->log("Ошибка создания файла и открытие на запись: " . $path . $filename);

			}

			zip_entry_close($zip_entry);
		}

		$this->log("Завершена распаковка XML", 2);
		
		return "";

	} 
	
	private function checkDirectories($name) {

		$path = DIR_IMAGE;
		$dir = explode("/", $name);
		for ($i = 0; $i < count($dir)-1; $i++) {
			$path .= $dir[$i] . "/";
			if (!is_dir($path)) {
				$error = "";
				@mkdir($path, 0755) or die ($error = "Ошибка создания директории '" . $path . "'");
				if ($error)
					return $error;
				//$this->log("Создана директория: " . $path, 2);
			}
		}
		return "";
	} 
	
	public function modeImport($importFile) {

		$this->log("Импорт данных из файла: " . $importFile, 2);
		
		$this->load->model('extension/module/impex');

		$this->log("Тип импорта: " . $this->detectFileType($importFile), 2);
		
		$error = $this->model_extension_module_impex->importFile($importFile, $this->detectFileType($importFile));

		if ($error) {

			$this->log("Ошибка при загрузке файла: " . $importFile);

			return $error;

		} else {
			
			$this->log("Успешно обработан файл: " . $importFile);
			
//			@unlink($importFile);
//			
//			$this->log("Удален файл: " . $importFile, 2);
			
		}

		return "";

	} 
	
	public function detectFileType($fileName) {

		$types = array('import', 'category', 'manufacturer', 'attribute', 'reference', 'fastcatalogfull', 'fastcatalogshort', 'catalog', 'faststockprice', 'stockprice', 'content');
		foreach ($types as $type) {
			$pos = stripos($fileName, $type);
			if ($pos !== false)
				return $type;
		}
		return '';
	} 

	private function log($message, $level=1) {
		
		$this->log->write(print_r($message,true));
		
	}
	
	private function echo_message($ok, $message="") {
		if ($ok) {
			echo "success\n";
			$this->log("success",2);
			if ($message) {
				echo $message;
				$this->log($message,2);
			}
		} else {
			echo "failure\n";
			$this->log("failure",2);
			if ($message) {
				echo $message;
				$this->log($message,2);
			}
		};
	}
	
	





	













	
	
	
	
	
	// удалить
	
	private function manualImportFile() {

		$error = "";
		
		$cache = DIR_CACHE . 'impex/';
		if (!is_dir($cache)) {
			mkdir($cache,0755,TRUE);
		}

		$uploaded_file = $this->request->files['file']['tmp_name'];
		$this->log("uploaded_file: " . $uploaded_file);
		if (!is_file($uploaded_file)) {
			return "ERROR: 1004";
		}

		$filename_decode = html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8');
		if (!@move_uploaded_file($uploaded_file, $cache . $filename_decode)) {
			$this->log("Загруженый файл не удалось переместить в: " . $cache . $filename_decode);
			return "ERROR: 1006";
		}

		// Перемещаем файл из временной папки PHP в папку кэша модуля
		$import_file = $cache . $filename_decode;
		$this->log($filename_decode, 2);
		$this->log($this->request->files['file'], 2);

		if (is_file($import_file)) {

			$path_info = pathinfo($filename_decode);
$this->log($path_info, 2);
			$filename = $path_info['filename'];
			$extension = $path_info['extension'];

			$result = $this->modeInit();
$this->log($result, 2);

			if (count($result) < 2) {
				$this->error['warning'] = "Ошибка инициализации перед загрузкой файла: " . $filename;
				return false;
			}

			$zip_support = class_exists('ZipArchive') ? true : false;

			if ($extension == 'zip') {

				if (!$zip_support) {
					$this->error['warning'] = "Ваш PHP не поддерживает ZIP архивы, отсутствует класс ZipArchive";
					return false;
				}

				$xmlFiles = $this->extractZip($import_file, $error);

				//if ($this->config->get('exchange1c_not_delete_files_after_import') != 1) {
					$this->log("Удален файл: " . $import_file);
					unlink($import_file);
				//}

				if ($error) return $error;

				$this->log($xmlFiles, 2);

				if (!count($xmlFiles)) {
					$this->error['warning'] = "Архив пустой или запароленный";
					return false;
				}
				
				
				return "";
				
				
				

				$goods = array();
				$properties = array();
				foreach ($xmlFiles as $key => $file) {
					$pos = strripos($file, "/goods/");
					if ($pos !== false) {
						$goods[] = $file;
						unset($xmlFiles[$key]);
					}
					$pos = strripos($file, "/properties/");
					if ($pos !== false) {
					$properties[] = $file;
						unset($xmlFiles[$key]);
					}
				}

				// Порядок обработки файлов
				sort($goods);
				sort($properties);
				sort($xmlFiles);

				foreach ($xmlFiles as $file) {
					$this->log('Обрабатывается файл основной: ' . $file, 2);
					$error = $this->modeImport($cache . $file);
					if ($error) return $error;
				}
				foreach ($properties as $file) {
					$this->log('Обрабатывается файл свойств: ' . $file, 2);
					$error = $this->modeImport($cache . $file);
					if ($error) return $error;
				}
				foreach ($goods as $file) {
					$this->log('Обрабатывается файл товаров: ' . $file, 2);
					$error = $this->modeImport($cache . $file);
					if ($error) return $error;
				}

			} elseif ($extension == 'xml') {
				// Это не архив
				$error = $this->modeImport($import_file);
				if ($error) return $error;

			} else {
				return "ERROR: 1007";
			}

		} // if (!empty($this->request->files['file']['name']) && is_file($import_file))

		return "";

	} // manualImportFile()
	public function modeImportManual($importFile) {

		$this->log('modeImportManual', 2);

		if ($manual) $this->log("Ручная загрузка данных");

		$this->log($importFile, 2);
		$this->log("------------", 2);

		$this->load->model('extension/module/impex');

		$this->log("Тип импорта: " . $this->detectFileType($importFile), 2);
		$this->log("------------", 2);
		
		// Загружаем файл
		$error = $this->model_extension_module_impex->importFile($importFile, $this->detectFileType($importFile));
		
		$this->log("Импорт файла: " . $importFile, 2);

//		if ($this->config->get('exchange1c_not_delete_files_after_import') != 1) {
//			@unlink($importFile);
//			$this->log("Удален файл: " . $importFile, 2);
//		}


		if ($error) {

			if (!$manual) {
				$this->echo_message(0, 'ERROR: ' . $error);
			}

			$this->log("Ошибка при загрузке файла: " . $importFile);

			return $error;

		} else {
			if (!$manual) {
				$this->echo_message(1, "Successfully processed file: " . $importFile);
			}
		}

		//$this->cache->delete('product');
		return "";

	} 
	public function manualImport() {
		$json = array();
		$error = '';
		
		$file = $this->request->post['file'];
		
		$path_info = pathinfo($file);
		$filename = $path_info['filename'];
		$extension = $path_info['extension'];
		
		$this->log($path_info, 2);
		$this->log($filename, 2);
		$this->log($extension, 2);
		
		$this->log($file, 2);

		$zip_support = class_exists('ZipArchive') ? true : false;
		
		$cache = DIR_CACHE . 'impex/';
//		if (!file_exists($cache)) {
//			if (@mkdir($cache, 0755) === false) {
//				$this->log("Не удалось создать директорию кэша " . $cache, 2);
//				$json['error'] = "Не удалось создать директорию кэша " . $cache;
//				$this->response->setOutput(json_encode($json));
//				return;
//			}
//		}

		if ($extension == 'zip') {

$this->log("-------zip-----", 2);
		
			if (!$zip_support) {
				$this->log("Ваш PHP не поддерживает ZIP архивы, отсутствует класс ZipArchive", 2);
				$json['error'] = "Ваш PHP не поддерживает ZIP архивы, отсутствует класс ZipArchive";
				$this->response->setOutput(json_encode($json));
				return;
			}

			$xmlFiles = $this->extractZip($file, $error);

			if ($error) {
				$this->log($error, 2);
				$json['error'] = $error;
				$this->response->setOutput(json_encode($json));
				return;
			}

$this->log('========================', 2);
$this->log($xmlFiles, 2);
$this->log('========================', 2);

			if (!count($xmlFiles)) {
				$this->log("Архив пустой или запароленный", 2);
				$json['error'] = "Архив пустой или запароленный";
				$this->response->setOutput(json_encode($json));
				return;
			}
			
			foreach ($xmlFiles as $xmlFile) {
				$this->log('Обрабатывается файл: ' . $xmlFile, 2);
				$this->log('Полный путь к файлу: ' . $cache . $xmlFile, 2);
				$error = $this->modeImport($cache . $xmlFile);
				if ($error) {
					$json['error'] = $error;
					$this->response->setOutput(json_encode($json));
					return;
				}
			}

		} elseif ($extension == 'xml') {
			
		$this->log("-------xml-----", 2);
			
			$error = $this->modeImport($file);
			if ($error) {
				$json['error'] = $error;
				$this->response->setOutput(json_encode($json));
				return;
			}

		} else {
			$json['error'] = 'Файлы типа :: ' . $extension . ' не поддерживаются';
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		$json['success'] = 'Ручной обмен прошел без ошибок';
		$this->log( "[i] Ручной обмен прошел без ошибок", 2);
		
		$this->response->setOutput(json_encode($json));
		
	} // manualImport()
	public function deleteFile() {
		$json = array();
		$error = '';
		
		$file = $this->request->post['file'];
		
		//$done = str_replace('admin/', '', DIR_APPLICATION) . 'dataimport/done/';
		$done = str_replace('catalog/', '', DIR_CATALOG) . 'dataimport/done/';

		if(!is_dir($done)) mkdir($done,0755,TRUE);
		
		$newfile = str_replace('dataimport/', 'dataimport/done/', $file);
		
		if (!copy($file, $newfile)) {
    	$json['error'] = "не удалось скопировать $file...\n";
			$this->log('не удалось скопировать файл "' . $file, 2);
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		if (!unlink($file)) {
    	$json['error'] = "не удалось удалить $file...\n";
			$this->log('не удалось удалось файл "' . $file, 2);
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		$json['success'] = 'Удален файл "' . $file . '". Копия сохранена в ' . $done;
		$this->log('Удален файл "' . $file . '". Копия сохранена в ' . $done, 2);
		
		$this->response->setOutput(json_encode($json));
		
	}
	public function serverImport() {
		$json = array();
		$error = '';
		
		$file = $this->request->post['file'];
		
		$path_info = pathinfo($file);
		$filename = $path_info['filename'];
		$extension = $path_info['extension'];
		
		$this->log("Импорт файла на сервере", 2);
		
		$this->log($path_info, 2);
		$this->log($filename, 2);
		$this->log($extension, 2);
		
		$this->log($file, 2);

		$zip_support = class_exists('ZipArchive') ? true : false;
		
		$cache = DIR_CACHE . 'impex/';
		if (!is_dir($cache)) {
			mkdir($cache,0755,TRUE);
		}

		if ($extension == 'zip') {

$this->log("-------zip-----", 2);
		
			if (!$zip_support) {
				$this->log("Ваш PHP не поддерживает ZIP архивы, отсутствует класс ZipArchive", 2);
				$json['error'] = "Ваш PHP не поддерживает ZIP архивы, отсутствует класс ZipArchive";
				$this->response->setOutput(json_encode($json));
				return;
			}

			$xmlFiles = $this->extractZip($file, $error);

			if ($error) {
				$this->log($error, 2);
				$json['error'] = $error;
				$this->response->setOutput(json_encode($json));
				return;
			}

$this->log('========================', 2);
$this->log($xmlFiles, 2);
$this->log('========================', 2);

			if (!count($xmlFiles)) {
				$this->log("Архив пустой или запароленный", 2);
				$json['error'] = "Архив пустой или запароленный";
				$this->response->setOutput(json_encode($json));
				return;
			}
			
			foreach ($xmlFiles as $xmlFile) {
				$this->log('Обрабатывается файл: ' . $xmlFile, 2);
				$this->log('Полный путь к файлу: ' . $cache . $xmlFile, 2);
				$error = $this->modeImport($cache . $xmlFile);
				if ($error) {
					$json['error'] = $error;
					$this->response->setOutput(json_encode($json));
					return;
				}
			}

		} elseif ($extension == 'xml') {
			
		$this->log("-------xml-----", 2);
			
			$error = $this->modeImport($file);
			if ($error) {
				$json['error'] = $error;
				$this->response->setOutput(json_encode($json));
				return;
			}

		} else {
			$json['error'] = 'Файлы типа :: ' . $extension . ' не поддерживаются';
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		//$done = str_replace('admin/', '', DIR_APPLICATION) . 'dataimport/done/';
		$done = str_replace('catalog/', '', DIR_CATALOG) . 'dataimport/done/';
		if(!is_dir($done)) mkdir($done,0755,TRUE);
		
		$newfile = str_replace('dataimport/', 'dataimport/done/', $file);
		
		if (!copy($file, $newfile)) {
    	$json['error'] = "не удалось скопировать $file...\n";
			$this->log('не удалось скопировать файл "' . $file, 2);
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		if (!unlink($file)) {
    	$json['error'] = "не удалось удалить $file...\n";
			$this->log('не удалось удалось файл "' . $file, 2);
			$this->response->setOutput(json_encode($json));
			return;
		}
		
		$json['success'] = 'Ручной обмен прошел без ошибок. Удален файл "' . $file . '". Копия сохранена в ' . $done;
		$this->log( "[i] Ручной обмен прошел без ошибок", 2);
		$this->log('Удален файл "' . $file . '". Копия сохранена в ' . $done, 2);
		
		$this->response->setOutput(json_encode($json));
		
	} 
	public function modeFileCatalog() {

		$error = '';
		
		$this->log('файл импорта: ' . $this->request->get['filename'], 2);
		
		$path_parts = pathinfo($this->request->get['filename']);
		$filename = $path_parts['filename'];
		$this->log('$filename = ' . $filename, 2);
		
		$this->modeFile($filename, $error);
		
		if ($error) {
			$this->echo_message(0, $error);
		} else {
			$this->echo_message(1, "Successfully import catalog ");
		}

	} // modeFileCatalog()
	private function modeFile($mode, &$error) {
		
//	$files = glob(DIR_TEMP.'*.xml');
//	if ($files) {
//		foreach ($files as $file) {
//			if (filemtime($file) < time() - $ttl * 60) {
//				unlink($file);
//			}
//		}
//	}
		
		
		

		$xmlfiles = array();

		if (!$this->checkAuthKey()) exit;
		//$cache = DIR_CACHE . 'impex/';
		$cache = DIR_APPLICATION . 'impex/';

		// Проверяем на наличие каталога
		if(!is_dir($cache)) mkdir($cache);

		// Проверяем на наличие имени файла
		if (isset($this->request->get['filename'])) {
			$upload_file = $cache . $this->request->get['filename'];
		} else {
			$error = "modeFile(): No file name variable";
			return false;
		}

		// Проверяем XML или изображения
		if (strpos($this->request->get['filename'], 'impex_files') !== false) {
			$cache = DIR_IMAGE;
			$upload_file = $cache . $this->request->get['filename'];
			$this->checkUploadFileTree(dirname($this->request->get['filename']) , $cache);
		}

		// Проверка на запись файлов в кэш
		if (!is_writable($cache)) {
			$error = "modeFile(): The folder " . $cache . " is not writable!";
			return false;
		}

		$this->log("upload file: " . $upload_file,2);
		
		if (file_exists($upload_file)) {
				
			$this->log("удаляю предыдущую версию: " . $upload_file,2);
			
			unlink($upload_file);

		} else {
			$this->log("нет старой версии файла: " . $upload_file,2);
		}	

		// Получаем данные
		$data = file_get_contents("php://input");
		if ($data !== false) {

			// Записываем в файл
			$filesize = file_put_contents($upload_file, $data, FILE_APPEND | LOCK_EX);
			$this->log("file size: " . $filesize, 2);

			if ($filesize) {
				chmod($upload_file , 0664);

				$xmlfiles = $this->extractZip($upload_file, $error);
				if ($error) {
					$this->echo_message(0, "modeFile(): Error extract file: " . $upload_file);

//					if ($this->config->get('exchange1c_not_delete_files_after_import') != 1) {
//						$this->log("Удален файл: " . $upload_file);
//						unlink($upload_file);
//					}

					return false;
				};
			} else {
				$this->echo_message(0, "modeFile(): Error create file");
			}
		}
		else {
			$this->echo_message(0, "modeFile(): Data empty");
		}

		return $xmlfiles;

	} 
	public function upload() {
		$json = array();

		if (!empty($_FILES['uploadfile']['name'])){
			
			$uploaddir = DIR_CACHE . 'impex/';
			if(!is_dir($uploaddir)) mkdir($uploaddir,0755,TRUE);
			//deleteAllFiles($uploaddir);
			
			//$this->log($_FILES, 2);
			
			$datimg = pathinfo($_FILES['uploadfile']['name']);
			$ext = strtolower($datimg['extension']);				
			$filetypes = array('xml','zip');
			
			if (in_array($ext, $filetypes)){
				
				$file = $uploaddir . basename($_FILES['uploadfile']['name']); 
				
				if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) { 
					$json['success']['file'] = $file;
					$json['success']['text'] = 'Успешно загружен файл ' . $_FILES['uploadfile']['name'];
				} else {
					$json['error']['text'] = 'Не удалось загрузить файл ' . $_FILES['uploadfile']['name'];
				}
				
			} else {
				$json['error']['text'] = 'Недопустимый формат файла :: ' . $ext;
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	private function checkAccess($echo = false) {

		// Модуль включен?
		if ($this->config->get('module_impex_status') == '0') { //empty()
			//if ($echo) $this->echo_message(0, "Модуль обмена отключен на стороне сайта");
			echo "failure\n";
			echo "Модуль обмена отключен на стороне сайта.\n";
			$this->log->write('Модуль обмена отключен на стороне сайта.');
			return false;
		}
		// Модуль установлен?
		if (!$this->config->get('module_impex_status')) {
			//if ($echo) $this->echo_message(0, "Модуль обмена не установлен на сайте");
			echo "failure\n";
			echo "Модуль обмена не установлен на сайте.\n";
			$this->log->write('Модуль обмена не установлен на сайте.');
			return false;
		}
		
		return true;
	}
	private function checkAuthKey($echo = false) {

		if (!isset($this->request->cookie['key'])) {
			echo "failure";
			if ($echo) $this->echo_message(0, "no cookie key\n");
			return false;
		}
		if ($this->request->cookie['key'] != md5($this->config->get('module_impex_password'))) {
			echo "failure";
			if ($echo) $this->echo_message(0, "Session error\n");
			return false;
		}
		return true;
	}
	public function modeCheckauth() {
		
		if (!$this->checkAccess(true))
			exit;

		$auth_user = "";
		$auth_pw = "";

		// Определение авторизации на сервере
		if (!isset($_SERVER['PHP_AUTH_USER'])) {

			// Определяем пользователя
			if (isset($_SERVER["REMOTE_USER"])) {
				$remote_user = $_SERVER["REMOTE_USER"];
				if (isset($_SERVER["REDIRECT_REMOTE_USER"])) {
					$remote_user = $_SERVER["REMOTE_USER"] ? $_SERVER["REMOTE_USER"]: $_SERVER["REDIRECT_REMOTE_USER"];
				}
			} elseif (isset($_SERVER["REDIRECT_HTTP_AUTHORIZATION"])) {
				$remote_user = $_SERVER["REDIRECT_HTTP_AUTHORIZATION"];
			}

			// Если удалось установить пользователя, тогда раскодируем
			if (isset($remote_user)) {
				$strTmp = base64_decode(substr($remote_user,6));
				if($strTmp)
					list($auth_user, $auth_pw) = explode(':', $strTmp);
			} else {
				$this->echo_message(0, "ERROR: 1009");
				$this->log("Проверьте наличие записи в файле .htaccess в корне файла после RewriteEngine On:\nRewriteCond %{HTTP:Authorization} ^(.*)\nRewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]");
				$this->log($_SERVER, 2);
				exit;
			}

		} else {
			$auth_user = $_SERVER['PHP_AUTH_USER'];
			$auth_pw = $_SERVER['PHP_AUTH_PW'];
		}

		// Авторизуем
		if (($this->config->get('module_impex_login') != '') && ($auth_user != $this->config->get('module_impex_login'))) {
			//$this->echo_message(0, "ERROR: 1010");
			echo "failure\n";
			echo "Авторизация не выполнена. Проверьте логин и пароль.\n";
			$this->log->write('Кто-то ломится с неверными учетными данными - L: '.$auth_user.' P: '.$auth_pw);
			exit;
		}
		if (($this->config->get('module_impex_password') != '') && ($auth_pw != $this->config->get('module_impex_password'))) {
			//$this->echo_message(0, "ERROR: 1011");
			echo "failure\n";
			echo "Авторизация не выполнена. Проверьте логин и пароль.\n";
			$this->log->write('Кто-то ломится с неверными учетными данными - L: '.$auth_user.' P: '.$auth_pw);
			exit;
		}
		echo "success\n";
		echo "Авторизация пользователя выполнена.\n";
		echo "key\n";
		echo md5($this->config->get('module_impex_password')) . "\n";
		
		$this->log->write('Успешная авторизация L: '.$auth_user.' P: '.$auth_pw);
		$this->log->write('modeCheckauth');

	}
	public function modeCatalogInit() {

		if (!$this->checkAuthKey(true)) {
			//echo "failure";
			exit;
		}

		$result = $this->modeInit();
		echo $result[0] . "\n";
		echo $result[1] . "\n";
//$this->log($result, 2);
		$this->log("Mode Catalog Init", 2);
//$this->log($result, 2);
		//echo "sessid=" . md5($this->config->get('impex_password')) . "\n";

	} 
	public function modeInit() {

		$result = array();

		$zip_support = class_exists('ZipArchive') ? true : false;
		if ($zip_support) {
			//$result[0] = $this->config->get('exchange1c_file_zip') == 1 ?  "zip=yes" : "zip=no";
			$result[0] = "zip=yes";
		} else {
			$result[0] = "zip=no";
		}
//		$manual_size = $this->formatSize($this->config->get('exchange1c_file_max_size'));
		$post_max_size = $this->getPostMaxFileSize();
//		if ($this->config->get('exchange1c_file_max_size') && $manual_size <= $post_max_size) {
//			$result[1] = "file_limit=" . $manual_size;
//		} else {
//			$result[1] = "file_limit=" . $post_max_size;
//		}
		$result[1] = "file_limit=" . $post_max_size;
		
		$this->log('PHP Version: ' . PHP_VERSION_ID, 2);

//		// Проверка наличия папки exchange1c в кэше
//		//$cache = DIR_CACHE . 'impex/';
//		$cache = DIR_APPLICATION . 'impex/';
//		$result['error'] = "";
//		if (!file_exists($cache)) {
//			if (@mkdir($cache, 0755) === false) {
//				$result['error'] = "1005";
//			}
//		}

		return $result;

	}
	private function getPostMaxFileSize() {

		$size = $this->formatSize(ini_get('post_max_size'));
		$this->log("POST_MAX_SIZE: " . $size, 2);

//		$size_max_manual = $this->formatSize($this->config->get('exchange1c_file_max_size'));
//		if ($size_max_manual) {
//			$this->log("POST_MAX_SIZE (переопределен в настройках): " . $size_max_manual, 2);
//			if ($size_max_manual < $size) {
//				$size = $size_max_manual;
//			}
//		}

		return $size;
	}
	private function formatSize($size) {
		if (empty($size)) {
			return 0;
		}
		$type = $size{strlen($size)-1};
		if (!is_numeric($type)) {
			$size = (integer)$size;
			switch ($type) {
				case 'K': $size = $size*1024;
					break;
				case 'M': $size = $size*1024*1024;
					break;
				case 'G': $size = $size*1024*1024*1024;
					break;
			}
			return $size;
		}
		return (int)$size;
	} 
	private function checkUploadFileTree($path, $curDir = null) {
		//if (!$curDir) $curDir = DIR_CACHE . 'impex/';
		if (!$curDir) $curDir = DIR_APPLICATION . 'impex/';
		foreach (explode('/', $path) as $name) {
			if (!$name) continue;
			if (file_exists($curDir . $name)) {
				if (is_dir( $curDir . $name)) {
					$curDir = $curDir . $name . '/';
					continue;
				}
				unlink ($curDir . $name);
			}
			mkdir ($curDir . $name );
			$curDir = $curDir . $name . '/';
		}
	} 

}
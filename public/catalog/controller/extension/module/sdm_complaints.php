<?php
/**
 * Class ControllerExtensionModuleSdmComplaints
 *
 * @module   Рекомендации и жалобы
 * @author   fonclub
 * @created  17.01.2020 18:02
 */

class ControllerExtensionModuleSdmComplaints extends Controller
{

    protected $error;

    public function index()
    {
        $status = $this->config->get('module_sdm_complaints_status');

        if ($status == 1) {
            return $this->load->view('extension/module/sdm_complaints');
        }

        return false;
    }

    /**
     * показ popup формы
     * @return mixed
     */
    public function popup()
    {
        $status = $this->config->get('module_sdm_complaints_status');

        if ($status == 1 && isset($this->request->server['HTTP_X_REQUESTED_WITH'])
            && ! empty($this->request->server['HTTP_X_REQUESTED_WITH'])
            && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            $this->load->language('extension/module/sdm_complaints');
            if ($this->config->get('config_account_id')) {
                $this->load->model('catalog/information');

                $information_info
                    = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

                if ($information_info) {
                    $data['text_agree'] = sprintf($this->language->get('text_agree'),
                        $this->url->link('information/information/agree',
                            'information_id='.$this->config->get('config_account_id'), true),
                        $information_info['title'], $information_info['title']);
                } else {
                    $data['text_agree'] = false;
                }
            } else {
                $data['text_agree'] = false;
            }

            return $this->response->setOutput($this->load->view('extension/module/sdm_complaints_popup', $data));
        }

        $this->response->redirect($this->url->link('error/not_found', '', true));
    }

    /**
     * отправка сообщения
     */
    public function send()
    {
        $status = $this->config->get('module_sdm_complaints_status');

        if ($status == 1 && isset($this->request->server['HTTP_X_REQUESTED_WITH'])
            && ! empty($this->request->server['HTTP_X_REQUESTED_WITH'])
            && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            $this->load->language('extension/module/sdm_complaints');
            $json = [];

            if ($this->validate()) {
                $data = [];
                $html_data = [];

                if (isset($this->request->post['name'])) {
                    $data[] = [
                        'name' => $this->language->get('enter_name'),
                        'value' => $this->request->post['name']
                    ];
                }

                if (isset($this->request->post['telephone'])) {
                    $data[] = [
                        'name' => $this->language->get('enter_telephone'),
                        'value' => $this->request->post['telephone']
                    ];
                }
                
                if (isset($this->request->post['email'])) {
                    $data[] = [
                        'name' => $this->language->get('enter_email'),
                        'value' => $this->request->post['email']
                    ];
                }

                if (isset($this->request->post['subject'])) {
                    $data[] = [
                        'name' => $this->language->get('enter_subject'),
                        'value' => $this->request->post['subject']
                    ];
                }

                if (isset($this->request->post['message'])) {
                    $data[] = [
                        'name' => $this->language->get('enter_message'),
                        'value' => $this->request->post['message']
                    ];
                }

                
                $html_data['date_added']      = date('d.m.Y H:i:s', time());
                $html_data['logo']            = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
                $html_data['store_name']      = $this->config->get('config_name');
                $html_data['store_url']       = $this->config->get('config_url');
                $html_data['text_info']       = $this->language->get('text_info');
                $html_data['text_date_added'] = $this->language->get('text_date_added');
                $html_data['data_info']       = $data;

                $html = $this->load->view('extension/module/sdm_complaints_mail', $html_data);
                $mail = new Mail($this->config->get('config_mail_engine'));
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mail->smtp_port     = $this->config->get('config_mail_smtp_port');
                $mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout');
                $mail->setFrom($this->config->get('config_email'));
                $mail->setSender($this->config->get('config_name'));
                $mail->setSubject($this->language->get('heading_title') . " -- " . $html_data['date_added']);
                $mail->setHtml($html);
                $mail->setTo($this->config->get('module_sdm_complaints_email'));
                try {
                    $mail->send();
                } catch (Exception $e) {
                }

                $json['success'] = $this->language->get('text_success');
            } else {
                $json['error'] = $this->error;
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } else {
            $this->response->redirect($this->url->link('error/not_found', '', true));
        }
    }

    /**
     * валидация
     * @return bool
     */
    protected function validate()
    {
        if (isset($this->request->post['subject'])
            && utf8_strlen(trim($this->request->post['subject'])) < 1) {
            $this->error['subject'] = $this->language->get('error_subject');
        }

        if (isset($this->request->post['email'])
            && (utf8_strlen($this->request->post['email']) > 96
                || ! preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email']))
        ) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (isset($this->request->post['message'])
            && (utf8_strlen(trim($this->request->post['message'])) < 1
            || utf8_strlen(trim($this->request->post['message'])) > 500)
        ) {
            $this->error['message'] = $this->language->get('error_message');
        }

        return !$this->error;
    }

}
<?php
class ControllerExtensionModuleWslider extends Controller {
	public function index($setting) {
		static $module = 0;		

		$this->document->addStyle('catalog/view/javascript/tiny-slider/tiny-slider.css');
		$this->document->addStyle('catalog/view/javascript/tiny-slider/sdm-tiny-styles.css');
		$this->document->addScript('catalog/view/javascript/tiny-slider/tiny-slider.js');
		
		$data['module'] = $module++;
		$data['slider_setting'] = $setting['slider_setting'];
		$data['breakpoints'] = $setting['breakpoints'];
		$data['frames'] = $setting['frames'];
		$data['style'] = $setting['style'];
		
		return $this->load->view('extension/module/wslider', $data);
	}
}
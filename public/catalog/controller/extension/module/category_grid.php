<?php

class ControllerExtensionModuleCategoryGrid extends Controller
{
    public function index($setting)
    {
        $data = [];

        $this->load->model('catalog/category');

        $data['class'] = $setting['class'] ?? null;

        $categories = $this->model_catalog_category->getCategories();
        $data['categories'] = $this->model_catalog_category->getPublicCategories($categories, [
            'image' => [
                'x' => 240,
                'y' => 220
            ]
        ]);

        if ($data['categories']) {
            return $this->load->view('extension/module/category_grid', $data);
        }
    }
}

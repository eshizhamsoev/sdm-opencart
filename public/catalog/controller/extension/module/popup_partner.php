<?php

/**
 * Class ControllerExtensionModulePopupPartner
 * @author fonclub
 * @created 07.04.2020
 */

class ControllerExtensionModulePopupPartner extends Controller {
	private $error = [];
	private $types = [
	    1 => 'Хочу стать партнером',
	    2 => 'Разместить первый запрос',
        3 => 'Помогаем партнёрам строить бизнес'
    ];

    public function index() {
	    if ($this->config->get('popup_partner_status') && isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	        $this->load->language('extension/module/popup_partner');

	        $data['popup_partner_data'] = $popup_partner_data = $this->config->get('popup_partner_data');

	        $data['name']      = $this->customer->isLogged() ? $this->customer->getFirstName() : '';
	        $data['telephone'] = $this->customer->isLogged() ? $this->customer->getTelephone() : '';

	        $data['comment'] = '';
	        $data['mask']    = '8 (999) 999-99-99';
	        $data['type']    = $this->request->get['type'] ?? 1;
	        $data['heading_title']    = $this->types[$data['type']];

	        $this->response->setOutput($this->load->view('extension/module/popup_partner', $data));
        } else {
	        $this->response->redirect($this->url->link('error/not_found', '', true));
        }
    }

    public function send() {
	    if ($this->config->get('popup_partner_status') && isset($this->request->server['HTTP_X_REQUESTED_WITH']) && !empty($this->request->server['HTTP_X_REQUESTED_WITH']) && strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	        $json = [];

	        $this->language->load('extension/module/popup_partner');

	        if ($this->validate()) {
		        $popup_partner_data = $this->config->get('popup_partner_data');

		        $data = [];

	            if (isset($this->request->post['name'])) {
	                $data[] = [
	                    'name' => $this->language->get('enter_name'),
	                    'value' => $this->request->post['name']
	                ];
	            }

	            if (isset($this->request->post['telephone'])) {
	                $data[] = [
	                    'name' => $this->language->get('enter_telephone'),
	                    'value' => $this->request->post['telephone']
	                ];
	            }

	            if (isset($this->request->post['city'])) {
	                $data[] = [
	                    'name' => $this->language->get('enter_city'),
	                    'value' => $this->request->post['city']
	                ];
	            }

	            if (isset($this->request->post['site'])) {
	                $data[] = [
	                    'name' => $this->language->get('enter_site'),
	                    'value' => $this->request->post['site']
	                ];
	            }

	            if (isset($this->request->post['activity'])) {
	                $data[] = [
	                    'name' => $this->language->get('enter_activity'),
	                    'value' => $this->request->post['activity']
	                ];
	            }

	            if (isset($this->request->post['comment'])) {
	                $data[] = [
	                    'name' => $this->language->get('enter_comment'),
	                    'value' => $this->request->post['comment']
	                ];
	            }

                $type = $this->request->post['type'] ?? 1;

	            if ($popup_partner_data['notify_status']) {
	                $html_data['date_added']      = date('d.m.Y H:i:s', time());
	                $html_data['logo']            = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
	                $html_data['store_name']      = $this->config->get('config_name');
	                $html_data['store_url']       = $this->config->get('config_url');
	                $html_data['text_info']       = $this->language->get('text_info');
	                $html_data['text_date_added'] = $this->language->get('text_date_added');
	                $html_data['data_info']       = $data;

	                $html = $this->load->view('mail/popup_partner', $html_data);

					$mail = new Mail($this->config->get('config_mail_engine'));
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port     = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout');
	                $mail->setFrom($this->config->get('config_email'));
	                $mail->setSender($this->config->get('config_name'));
	                $mail->setSubject($this->types[$type] . " -- " . $html_data['date_added']);
	                $mail->setHtml($html);

	                $emails = explode(',', $popup_partner_data['notify_email']);

	                foreach ($emails as $email) {
	                    if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
	                        $mail->setTo($email);
                            try {
                                $mail->send();
                            } catch (Exception $e) {
                            }
                        }
	                }

                    //Отправка запроса в Битрикс 24
                    $this->load->model('module/b24_order');
                    $this->model_module_b24_order->addRequest($this->request->post, $this->types[$type]);
                    // Отправка запроса в Битрикс 24
	            }

	            $json['output'] = $this->language->get('text_success_send');
	        } else {
		        $json['error'] = $this->error;
	        }

	        $this->response->addHeader('Content-Type: application/json');
	        $this->response->setOutput(json_encode($json));
        } else {
	        $this->response->redirect($this->url->link('error/not_found', '', true));
        }
    }

    protected function validate() {

        if (isset($this->request->post['name']) && (utf8_strlen(trim($this->request->post['name'])) < 1) || (utf8_strlen(trim($this->request->post['name'])) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}

        if (isset($this->request->post['telephone'])) {
            $phone_count = utf8_strlen(str_replace(['_','-','(',')','+',' '], "", '8 (999) 999-99-99'));

            if (utf8_strlen(str_replace(['_','-','(',')','+',' '], "", $this->request->post['telephone'])) < $phone_count) {
                $this->error['telephone'] = $this->language->get('error_telephone_mask');
            }
        } elseif (isset($this->request->post['telephone']) && (utf8_strlen(str_replace(['_','-','(',')','+',' '], "", $this->request->post['telephone'])) > 15 || utf8_strlen(str_replace(['_','-','(',')','+',' '], "", $this->request->post['telephone'])) < 3)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

	    return !$this->error;
    }
}

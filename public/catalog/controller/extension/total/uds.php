<?php
class ControllerExtensionTotalUDS extends Controller {
	
	public function index() {
		
		$data['total_uds_block_html'] = html_entity_decode($this->config->get('total_uds_block_html'));
		$data['total_uds_block_css'] = $this->config->get('total_uds_block_css');
		$data['uds_app_link'] = $this->config->get('total_uds_app_link');
		
	}

	public function udsCodePhone() {
		$json = array();
		
		$this->load->language('extension/total/uds');
		$this->load->model('extension/total/uds');
		$this->load->model('setting/extension');
		
		$this->clearUdsSession(); 
		
		if (isset($this->request->post['uds_code_phone'])) {
			$uds_code_phone = $this->request->post['uds_code_phone'];
		} else {
			$uds_code_phone = '';
		}
		
		if (empty($uds_code_phone)) {
			
			$json['error'] = $this->language->get('error_empty_code_phone');
			
		} else {
			
			$uds_type = $this->validate($uds_code_phone);

			unset($this->session->data['error']); 
			
			if ($uds_type == 'code') {
				
				$uds_total = $this->model_extension_total_uds->getUdsTotal();
				
				$data = array();
				$data['query'] = 'codeinfo';
				$data['uds_code_phone'] = $this->request->post['uds_code_phone'];
				$data['uds_order_total'] = $uds_total;
				$data['uds_points'] = 0;
				
				$result = $this->model_extension_total_uds->udsQuery($data);

				if ($result) {
					
					$result_arr = json_decode($result, true);

					if (isset($result_arr['success'])) {
						
						$json['success'] = $result_arr['success']; 
						$json['success']['message'] = sprintf($this->language->get('success_uds_code'), $json['success']['uds_display_name'], $json['success']['uds_total_points'], $json['success']['uds_max_points'], $json['success']['uds_cashback']);
						$this->session->data['uds'] = $result_arr['success'];
						$this->session->data['uds']['message'] = sprintf($this->language->get('success_uds_code'), $json['success']['uds_display_name'], $json['success']['uds_total_points'], $json['success']['uds_max_points'], $json['success']['uds_cashback']);
						
						$this->model_extension_total_uds->addCustomerByCode($result_arr['success']);
						
						$this->session->data['success'] = $this->language->get('success_uds_applied');
					} else {
						
						$json['error'] = $this->language->get('error_uds_code');
						
					}

				} else {
					
					$json['error'] = $this->language->get('error_uds_server');
					
				}

			} elseif ($uds_type == 'phone') {
				
				$uds_total = $this->model_extension_total_uds->getUdsTotal();
				
				$data = array();
				$data['query'] = 'phoneinfo';
				$data['uds_code_phone'] = $this->request->post['uds_code_phone'];
				$data['uds_order_total'] = $uds_total;
				$data['uds_points'] = 0;
				
				$result = $this->model_extension_total_uds->udsQuery($data);
				
				if ($result) {
					
					$result_arr = json_decode($result, true);

					if (isset($result_arr['success'])) {
						
						$json['success'] = $result_arr['success']; 
						$json['success']['message'] = sprintf($this->language->get('success_uds_phone'), $json['success']['uds_cashback']);
						$this->session->data['uds'] = $result_arr['success'];
						$this->session->data['uds']['message'] = sprintf($this->language->get('success_uds_phone'), $json['success']['uds_cashback']);
						
						$this->model_extension_total_uds->addCustomerByPhone($uds_code_phone);
						
						$this->session->data['success'] = $this->language->get('success_uds_applied');
					} else {
						
						$json['error'] = $this->language->get('error_uds_code');
						
					}

				} else {
					
					$json['error'] = $this->language->get('error_uds_server');
					
				}
				
			} else {
				
				$json['error'] = $this->language->get('error_wrong_code_phone');
				
			}
			
		}
			
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function uds() {
		$json = array();

		$this->load->language('extension/total/uds');
		$this->load->model('extension/total/uds');

		$this->session->data['uds']['uds_order_total'] = $this->request->post['uds_order_total'];
		$this->session->data['uds']['uds_points'] = $this->request->post['uds_points'];
		$this->session->data['uds']['uds_customer_id'] = $this->request->post['uds_customer_id'];
		$this->session->data['uds']['uds_operation_code'] = $this->request->post['uds_operation_code'];
		
		$uds_info = $this->model_extension_total_uds->getUds();
	
		if ($uds_info) {

			$this->session->data['success'] = $this->language->get('success_uds_updated');

			$json['redirect'] = $this->url->link('checkout/cart');
		} else {
			$json['error'] = '2 - ' . $this->language->get('error_uds');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function removeUDS() {
		$json = array();
		
		$this->load->model('extension/total/uds');
		
		$this->session->data['uds']['uds_order_total'] = 0;
		$this->session->data['uds']['uds_points'] = 0;
		
		$this->model_extension_total_uds->getUds();
		
		$this->clearUdsSession();
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	private function validate($uds_code_phone) {
		$type = 'error';
		
		if (is_numeric($uds_code_phone) && strlen($uds_code_phone.'') == 6) {
			$type = 'code';
		} elseif (is_string($uds_code_phone) && strlen($uds_code_phone.'') == 12 && $uds_code_phone[0] == '+') {
			$type = 'phone';
		}
		
		return $type;
	}
	
	private function clearUdsSession() {
		
		if (isset($this->session->data['uds'])) {
			unset($this->session->data['uds']);
		}
	}
	
	public function eventAddOrderHistoryBefore($route, $args) {
			
$this->log->write('<pre>' . print_r($this->session->data, 1) . '</pre>');			

		$this->load->model('extension/total/uds');
		
		if (isset($this->session->data['uds']['uds_order_total']) && $this->session->data['uds']['uds_order_total'] != '') { 
		
			$data = $this->session->data['uds'];
			
$this->log->write('<pre>' . print_r($data, 1) . '</pre>');			
			
			$this->clearUdsSession();
			$data['order_id'] =  $args[0];
			$data['order_status_id'] =  $args[1];
			$uds_order_info = $this->model_extension_total_uds->addUdsOrder($data);
			
			$this->model_extension_total_uds->setUdsOperation($uds_order_info, $args[1]);	
		}
	}
	
	public function eventAddOrderHistoryAfter($route, $args) {

		if (isset($args[0]) && !empty($args[0])) {

			$this->load->model('extension/total/uds');
			
			if (isset($this->session->data['uds'])) { 
				$this->log->write($this->session->data['uds']);
			} else {
				$uds_order_info = $this->model_extension_total_uds->getUdsOrder($args[0]);
				if ($uds_order_info) {
					
					if (!in_array($args[1],$this->config->get('total_uds_refund_status'))) {
						$this->model_extension_total_uds->editUdsOrder($args[0],$args[1]);
					}
					$uds_order_info['order_status_id'] = $args[1];	
					$this->model_extension_total_uds->setUdsOperation($uds_order_info, $args[1]);	
				}
			}
		}
	}
	
}

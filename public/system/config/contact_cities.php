<?php
/**
 * @author : fonclub <sfonclub@gmail.com>
 * @created: 15.01.2020
 */

return [
    1 => [ /** INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES (NULL, '0', '1', 'contact_city_id=1', 'contact-us/sankt-peterburg'); */
        'name' => 'Санкт-Петербург',
        'html' => '
                        <div class="us-content-contact-title">Контактная информация</div>
						<div class="us-content-contact-offer-block">
							<div class="us-content-contact-text us-content-contact-text-bold">У вас общий вопрос?</div>
							<div class="us-content-contact-text">Напишите или позвоните нам — всё расскажем.</div>
							<div class="us-content-contact-text">E-mail: <a href="mailto:info@sdmclimate.ru" class="us-acc-info-link">info@sdmclimate.ru</a></div>
							<div class="us-content-contact-text roistat-phone-1">Телефон: <a href="tel:88007076860" class="us-acc-info-link phone-click roistat-phone-1">8 (800) 707-68-60</a></div>
							<div class="us-content-contact-text">Служба клиентской поддержки работает с 9:00 до 22:00, без выходных.</div>
							<div class="us-content-contact-text us-content-contact-text-bold">Есть замечания или предложения как нам работать лучше?</div>
							<div class="us-content-contact-text">E-mail: <a href="mailto:info@sdmclimate.ru" class="us-acc-info-link">info@sdmclimate.ru</a></div>
						</div>
						<div class="us-content-contact-title">Наш адрес</div>
						<div class="us-content-contact-text">192007, г. Санкт-Петербург, ул. Тосина, 9, офис 1</div>
        ',
        'seo_title' => 'Обратная связь - Санкт-Петербург',
        'seo_description' => 'Обратная связь - Санкт-Петербург',
        'seo_keys' => 'обратная связь, Санкт-Петербург',
        'seo_url' => 'contact-us/sankt-peterburg',
        'sort_order' => '1',
        'map' => '<iframe width="100%" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=%D0%B3.%20%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3,%20%D1%83%D0%BB.%20%D0%A2%D0%BE%D1%81%D0%B8%D0%BD%D0%B0,%209,%20%D0%BE%D1%84%D0%B8%D1%81%201&amp;key=AIzaSyAmoPXXHiQXth24wmNdPNsmpiSvUZYR1v8&amp;zoom=14" allowfullscreen=""></iframe>'
    ],
    2 => [ /** INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES (NULL, '0', '1', 'contact_city_id=2', 'contact-us/novosibirsk'); */
        'name' => 'Новосибирск',
        'html' => '
                        <div class="us-content-contact-title">Контактная информация</div>
						<div class="us-content-contact-offer-block">
							<div class="us-content-contact-text us-content-contact-text-bold">У вас общий вопрос?</div>
							<div class="us-content-contact-text">Напишите или позвоните нам — всё расскажем.</div>
							<div class="us-content-contact-text">E-mail: <a href="mailto:info@sdmclimate.ru" class="us-acc-info-link">info@sdmclimate.ru</a></div>
							<div class="us-content-contact-text roistat-phone-1">Телефон: <a href="tel:88007076860" class="us-acc-info-link phone-click roistat-phone-1">8 (800) 707-68-60</a></div>
							<div class="us-content-contact-text">Служба клиентской поддержки работает с 9:00 до 22:00, без выходных.</div>
							<div class="us-content-contact-text us-content-contact-text-bold">Есть замечания или предложения как нам работать лучше?</div>
							<div class="us-content-contact-text">E-mail: <a href="mailto:info@sdmclimate.ru" class="us-acc-info-link">info@sdmclimate.ru</a></div>
						</div>
						<div class="us-content-contact-title">Наш адрес</div>
						<div class="us-content-contact-text">630032, г. Новосибирск, ул. Большая, д. 280, офис 15</div>
        ',
        'seo_title' => 'Обратная связь - Новосибирск',
        'seo_description' => 'Обратная связь - Новосибирск',
        'seo_keys' => 'обратная связь, Новосибирск',
        'seo_url' => 'contact-us/novosibirsk',
        'sort_order' => '2',
        'map' => '<iframe width="100%" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=%D0%B3.%20%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D0%B8%D0%B1%D0%B8%D1%80%D1%81%D0%BA,%20%D1%83%D0%BB.%20%D0%91%D0%BE%D0%BB%D1%8C%D1%88%D0%B0%D1%8F,%20%D0%B4.%20280,%20%D0%BE%D1%84%D0%B8%D1%81%2015&amp;key=AIzaSyAmoPXXHiQXth24wmNdPNsmpiSvUZYR1v8&amp;zoom=14" allowfullscreen=""></iframe>'
    ],
    3 => [ /** INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES (NULL, '0', '1', 'contact_city_id=3', 'contact-us/krasnodar'); */
        'name' => 'Краснодар',
        'html' => '
                        <div class="us-content-contact-title">Контактная информация</div>
						<div class="us-content-contact-offer-block">
							<div class="us-content-contact-text us-content-contact-text-bold">У вас общий вопрос?</div>
							<div class="us-content-contact-text">Напишите или позвоните нам — всё расскажем.</div>
							<div class="us-content-contact-text">E-mail: <a href="mailto:info@sdmclimate.ru" class="us-acc-info-link">info@sdmclimate.ru</a></div>
							<div class="us-content-contact-text roistat-phone-1">Телефон: <a href="tel:88007076860" class="us-acc-info-link phone-click roistat-phone-1">8 (800) 707-68-60</a></div>
							<div class="us-content-contact-text">Служба клиентской поддержки работает с 9:00 до 22:00, без выходных.</div>
							<div class="us-content-contact-text us-content-contact-text-bold">Есть замечания или предложения как нам работать лучше?</div>
							<div class="us-content-contact-text">E-mail: <a href="mailto:info@sdmclimate.ru" class="us-acc-info-link">info@sdmclimate.ru</a></div>
						</div>
						<div class="us-content-contact-title">Наш адрес</div>
						<div class="us-content-contact-text">350072, г. Краснодар, ул. Автомобильная, д. 3, офис 14</div>
        ',
        'seo_title' => 'Обратная связь - Краснодар',
        'seo_description' => 'Обратная связь - Краснодар',
        'seo_keys' => 'обратная связь, Краснодар',
        'seo_url' => 'contact-us/krasnodar',
        'sort_order' => '3',
        'map' => '<iframe width="100%" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=%D0%B3.%20%D0%9A%D1%80%D0%B0%D1%81%D0%BD%D0%BE%D0%B4%D0%B0%D1%80,%20%D1%83%D0%BB.%20%D0%90%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F,%20%D0%B4.%203,%20%D0%BE%D1%84%D0%B8%D1%81%2014&amp;key=AIzaSyAmoPXXHiQXth24wmNdPNsmpiSvUZYR1v8&amp;zoom=14" allowfullscreen=""></iframe>'
    ]
];

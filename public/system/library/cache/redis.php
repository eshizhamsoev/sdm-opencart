<?php
namespace Cache;
class Redis {
    private $expire;
    private $cache;
    private $prefix;

    public function __construct($expire) {
        $this->expire = $expire;

        $this->cache = new \Redis();
        if (!$this->cache->pconnect(CACHE_HOSTNAME, CACHE_PORT)) {
            throw new \Exception("Permissions denied session storage");
        }
        $this->prefix = CACHE_PREFIX;
    }

   public function get($key) {
      if ($this->cache->exists($this->prefix . $key)) {
        $data = $this->cache->get($this->prefix . $key);

        return json_decode($data, true);
      }

      return false;
    }

    public function set($key,$value, $expire = null) {
        if (is_null($expire)) {
            $expire = $this->expire;
        }

        return $this->cache->setex($this->prefix . $key, $expire, json_encode($value));
    }

    public function delete($key) {
        if($key === '*')
            $this->flushDb();
        else
            $this->cache->del($this->prefix . $key);
    }

    protected function flushDb() {
        $this->cache->flushDb();
    }
}

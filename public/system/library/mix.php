<?php

class Mix
{
    protected $map = [];

    public function __construct()
    {
        try {
            $path = DIR_PROJECT_ROOT . '/assets/mix-manifest.json';
            if (is_file($path)) {
                $f = file_get_contents($path);
                $this->map = json_decode($f, 1);
            }
        } catch (Exception $e) {
            // Just ignore
        }
    }

    public function getBuildAsset($path): string
    {
        return '/assets' . ($this->map[$path] ?? $path);
    }
}

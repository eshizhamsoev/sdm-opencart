<?php
/**
 * Author: Shabalin Pavel
 */
 
namespace Session;
 
 
final class Redis
{
    public $expire = '';
    protected $prefix = 'sess_';
 
    public function __construct($registry)
    {
        $this->redis = new \Redis();
 
        if (!$this->redis->pconnect(CACHE_HOSTNAME, CACHE_PORT)) {
            throw new \Exception("Permissions denied session storage");
        }
 
        $this->expire = $registry->get('config')->get('session_expire');;
    }
 
    public function read($session_id)
    {
        if ($this->redis->exists($this->prefix . $session_id)) {
            return json_decode( $this->redis->get($this->prefix . $session_id), true );
        }
 
        return false;
    }
 
    public function write($session_id, $data)
    {
        if ($session_id) {
            $this->redis->setex($this->prefix . $session_id, $this->expire, json_encode($data));
        }
 
        return true;
    }
 
    public function destroy($session_id)
    {
        if ($this->redis->exists($this->prefix . $session_id)) {
            $this->redis->del($this->prefix . $session_id);
        }
 
        return true;
    }
 
    public function gc($expire) {
        return true;
    }
 
}
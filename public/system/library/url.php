<?php
/**
 * @package        OpenCart
 * @author        Daniel Kerr
 * @copyright    Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license        https://opensource.org/licenses/GPL-3.0
 * @link        https://www.opencart.com
 */

/**
 * URL class
 */
class Url
{
    const URL_CACHE_PREFIX = 'url.';
    private $url;
    private $ssl;
    private $rewrite = [];
    private $cache;

    /**
     * Constructor
     *
     * @param  string  $url
     * @param  string  $ssl
     *
     */
    public function __construct($url, $ssl = '', $cache = null)
    {
        $this->url = $url;
        $this->ssl = $ssl;
//        $this->cache = $cache;
        $this->cache = null;
    }

    /**
     *
     *
     * @param  object  $rewrite
     */
    public function addRewrite($rewrite)
    {
        $this->rewrite[] = $rewrite;
    }

    /**
     *
     *
     * @param  string  $route
     * @param  mixed  $args
     * @param  bool  $secure
     *
     * @return    string
     */
    public function link($route, $args = '', $secure = false)
    {
        if ($this->ssl && $secure) {
            $url = $this->ssl . 'index.php?route=' . $route;
        } else {
            $url = $this->url . 'index.php?route=' . $route;
        }

        if ($args) {
            if (is_array($args)) {
                $url .= '&amp;' . http_build_query($args);
            } else {
                $url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
            }
        }

        $key = self::URL_CACHE_PREFIX . $url;
        if ($this->cache) {
            $cachedValue = $this->cache->get($key);
            if ($cachedValue) {
                return $cachedValue;
            }
        }

        foreach ($this->rewrite as $rewrite) {
            $url = $rewrite->rewrite($url);
        }

        if($this->cache) {
            $this->cache->set($key, $url);
        }

        return $url;
    }
}

<?php  

class deliveryOutlets extends yandex_beru implements exchange {
	protected $method = '/outlets.json';
	private $data;	
    public $type = 'GET';
    public function setData($data) {
		$this->data = $data;
    }
   
	
	public function getData(){
		return $this->data;
    }

    public function getMethod(){
		return "/outlets.json";
	}
	
	private function getErrorText($error){
		$errors = [
			'BAD_REQUEST' => '(BAD_REQUEST) Ошибка запроса. Проверьте настройки подключения.',];
		if(isset($errors[$error['code']])){
			return $errors[$error['code']];
		}else{
			return $error['message'];
		}
    }
}
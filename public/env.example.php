<?php

putenv('ENV_HOSTNAME=sdm.test');
putenv('ENV_HTTP=http');
putenv('ENV_ROOTPATH=/var/www/sdm/public/');
putenv('ENV_IMAGEPATH=/var/www/sdm/public/image/');
putenv('ENV_LOGSPATH=/var/www/sdm/public/system/storage/logs');
putenv('DB_HOST=mysql');
putenv('DB_USERNAME=root');
putenv('DB_PASSWORD=root');
putenv('DB_DATABASE=sdm');

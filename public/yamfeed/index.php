<?php
	//error_reporting(E_ALL ^ E_DEPRECATED);
	//ini_set('max_execution_time', MAX_EXECUTION_TIME);
	require_once('config.php');

	$eol = "\n";
	
	$yml  = '<?xml version="1.0" encoding="UTF-8"?>' . $eol;
	
	if ( !file_exists(DIR_TEMP) ) mkdir(DIR_TEMP, 0755, true);
	if ( !file_exists(DIR_LOGS) ) mkdir(DIR_LOGS, 0755, true);
	if ( !file_exists(DIR_LOGS . LOG) ) {
		$log_file = fopen(DIR_LOGS.LOG, 'w');
		fwrite($log_file, date('Y-m-d H:i') . ' (:!:) лог файл создан' . PHP_EOL . PHP_EOL);
		fclose($log_file);
	}
	
	if (isset($_GET['mode']) && $_GET['mode'] == 'debug') {
		$debug = true;
	} else {
		$debug = false;
	}
	
	if (isset($_GET['help'])) {
		
		$help = '';
		$help .= 'Как бы API ;)<br><br>';
		$help .= 'Структура запроса:<br><br>';
		$help .= 'HTTPS_SERVER/yamfeed/index.php[?[help][token=TOKEN][mode=debug][action=(config|test|log)]]<br><br>';
		$help .= 'Просмотр конфигурации:<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug&action=config" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug&action=config' . '</a><br><br>';
		$help .= 'Тест окружения:<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug&action=test" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug&action=test' . '</a><br><br>';
		$help .= 'Просмотр логов:<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug&action=log" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug&action=log' . '</a><br><br>';
		
		$help .= 'За вывод в браузер отвечает [mode=debug]:<br><br>';
		
		$help .= 'Генерация фида и вывод в браузер (если USE_TOKEN == true):<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '&mode=debug</a><br><br>';
		$help .= 'Генерация фида и вывод в браузер (если USE_TOKEN == false):<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php?mode=debug" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php?mode=debug</a><br><br>';
		
		$help .= 'Генерация фида без вывода (если USE_TOKEN == true):<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php?token=' . TOKEN . '</a><br><br>';
		$help .= 'Генерация фида без вывода (если USE_TOKEN == false):<br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/index.php" target="_blank">' . HTTPS_SERVER . 'yamfeed/index.php</a><br><br>';
		$help .= 'Обратите внимание на два параметра конфигурации:<br>';
		$help .= 'MAX_EXECUTION_TIME - форсирует установки сервера (назначить по результатам тестирования)<br>';
		$help .= 'BATCH - количество товаров в одной транзакции (назначить по результатам тестирования)<br>';
		
		echo $help;
			
		exit();
	}

	if (USE_TOKEN) {
		
		if (!isset($_GET['token'])) {
			
			$yml .= '<msgs date="' . date('Y-m-d H:i') . '">' . $eol;
			$yml .= '<msg>fastfeed для sdmclimate.ru</msg>' . $eol;
			$yml .= '<msg>Токен не указан</msg>' . $eol;
			$yml .= '<msg>Создание фида для Яндекс Маркета не будет выполнено</msg>' . $eol;
			$yml .= '</msgs>' . $eol;
			
			_log('(:e:) попытка запуска без токена');
			
			if ($debug) {
				header("Content-type:application/xml");
				print($yml);
			}
			exit();
		} else if ($_GET['token'] != TOKEN) {
			
			$yml .= '<msgs date="' . date('Y-m-d H:i') . '">' . $eol;
			$yml .= '<msg>fastfeed для sdmclimate.ru</msg>' . $eol;
			$yml .= '<msg>Недействительный токен</msg>' . $eol;
			$yml .= '<msg>Создание фида для Яндекс Маркета не будет выполнено</msg>' . $eol;
			$yml .= '</msgs>' . $eol;
			
			_log('(:e:) попытка запуска с недействительным токеном (' . $_GET['token'] . ')');
			
			if ($debug) {
				header("Content-type:application/xml");
				print($yml);
			}
			exit();
		}
	}
	
	if (isset($_GET['action'])) {
		
		if ($_GET['action'] == 'config') {
			
			$yml .= '<config date="' . date('Y-m-d H:i') . '">' . $eol;
			$yml .= '<environment___________________________________________/>' . $eol;
			$yml .= '<max_execution_time>' 					. MAX_EXECUTION_TIME . '</max_execution_time>' . $eol;
			$yml .= '<batch>' 											. BATCH . '</batch>' . $eol;
			$yml .= '<language_id>' 								. LANGUAGE_ID . '</language_id>' . $eol;
			$yml .= '<out_of_sale_stock_status_id>'	. OUT_OF_SALE_STOCK_STATUS_ID . '</out_of_sale_stock_status_id>' . $eol;
			$yml .= '<default_delivery_option>'			. DEFAULT_DELIVERY_OPTION . '</default_delivery_option>' . $eol;
			$yml .= '<temp_feed_ttl>'								. TEMP_FEED_TTL . '</temp_feed_ttl>' . $eol;
			$yml .= '<authentication________________________________________/>' . $eol;
			$yml .= '<use_token>' 					. USE_TOKEN . '</use_token>' . $eol;
			$yml .= '<token>' 							. TOKEN . '</token>' . $eol;
			$yml .= '<host__________________________________________________/>' . $eol;
			$yml .= '<https_server>' 				. HTTPS_SERVER . '</https_server>' . $eol;
			$yml .= '<data_base_____________________________________________/>' . $eol;
			$yml .= '<driver>' 							. DB_DRIVER . '</driver>' . $eol;
			$yml .= '<hostname>' 						. DB_HOSTNAME . '</hostname>' . $eol;
			$yml .= '<username>' 						. DB_USERNAME . '</username>' . $eol;
			$yml .= '<password>' 						. DB_PASSWORD . '</password>' . $eol;
			$yml .= '<database>' 						. DB_DATABASE . '</database>' . $eol;
			$yml .= '<port>' 								. DB_PORT . '</port>' . $eol;
			$yml .= '<prefix>' 							. DB_PREFIX . '</prefix>' . $eol;
			$yml .= '<dir_file______________________________________________/>' . $eol;
			$yml .= '<root>' 								. DIR_ROOT . '</root>' . $eol;
			$yml .= '<temp>' 								. DIR_TEMP . '</temp>' . $eol;
			$yml .= '<logs>' 								. DIR_LOGS . '</logs>' . $eol;
			$yml .= '<feed>' 								. FEED . '</feed>' . $eol;
			$yml .= '<log>' 								. LOG . '</log>' . $eol;
			$yml .= '</config>' . $eol;
			
			_log('(:c:) запуск в режиме action=config');
			
			if ($debug) {
				header("Content-type:application/xml");
				print($yml);
			}
			exit();
		}
	
		if ($_GET['action'] == 'test') {
			
			$yml .= '<test date="' . date('Y-m-d H:i') . '">' . $eol;
			
			if ( file_exists(DIR_TEMP) ) {
				$yml .= '<tst>Директория временных файлов ' . DIR_TEMP . ' существует</tst>' . $eol;
				if (is_writable(DIR_TEMP)) {
					$yml .= '<tst>Директория временных файлов ' . DIR_TEMP . ' доступна для записи</tst>' . $eol;
				} else {
					$yml .= '<tst>Директория временных файлов ' . DIR_TEMP . ' НЕдоступна для записи</tst>' . $eol;
				}
			} else {
				$yml .= '<tst>Директория временных файлов ' . DIR_TEMP . ' НЕ существует</tst>' . $eol;
			}
			
			if ( file_exists(DIR_LOGS) ) {
				$yml .= '<tst>Директория логов ' . DIR_LOGS . ' существует</tst>' . $eol;
				if (is_writable(DIR_LOGS)) {
					$yml .= '<tst>Директория логов ' . DIR_LOGS . ' доступна для записи</tst>' . $eol;
				} else {
					$yml .= '<tst>Директория логов ' . DIR_LOGS . ' НЕдоступна для записи</tst>' . $eol;
				}
			} else {
				$yml .= '<tst>Директория логов ' . DIR_LOGS . ' НЕ существует</tst>' . $eol;
			}
			
			$yml .= '<tst>Имя файла фида ' . DIR_ROOT.FEED . '</tst>' . $eol;
			
			if (is_writable(DIR_ROOT)) {
				$yml .= '<tst>Директория фида ' . DIR_ROOT . ' доступна для записи</tst>' . $eol;
			} else {
				$yml .= '<tst>Директория фида ' . DIR_ROOT . ' НЕдоступна для записи</tst>' . $eol;
			}
			
			$yml .= '</test>' . $eol;
			
			_log('(:t:) запуск в режиме action=test');
			
			if ($debug) {
				header("Content-type:application/xml");
				print($yml);
			}
			exit();
		}
	
		if ($_GET['action'] == 'log') {
			
			_log('(:l:) запуск в режиме action=log');
			
			if ($debug) {
				//$log = str_replace(PHP_EOL, '<br>', file_get_contents(DIR_LOGS.LOG));
                $log = str_replace(PHP_EOL, '<br>', file_get_contents(DIR_LOGS));
				echo $log;
			}
			exit();
		}
	}
	
	$start_time = time();
	_log('(:s:) начало генерации фида');
	
	$handle = FALSE;
	$handle = fopen(DIR_TEMP.FEED, "w");
	
	$link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
	$db = mysqli_select_db ($link, DB_DATABASE);
	
	$categories = array();
	$manufacturers = array();
	$attributes = array();
	$products = array();
	$delivery_options = array();
	$promos = array();
	
	$bad_products = array();
	
	$sql = "SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE cd.language_id = '" . LANGUAGE_ID . "' ORDER BY c.category_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$categories[] = array(
				'category_id' 	=> $row['category_id'], 
				'parent_id' 	=> $row['parent_id'], 
				'name'        	=> strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'))
			);
		}
	}
	_log('(:i:) список категорий  (' . count($categories) . ' поз. )');
	//echo '<pre>' . print_r($categories, 1) . '</pre>';
	
	$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer ORDER BY manufacturer_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$manufacturers[$row['manufacturer_id']] = strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'));
		}
	}
	_log('(:i:) список производителей  (' . count($manufacturers) . ' поз. )');
	//echo '<pre>' . print_r($manufacturers, 1) . '</pre>';
	
	$sql = "SELECT DISTINCT delivery_option FROM " . DB_PREFIX . "product p WHERE p.status = 1 AND p.price > 0 AND p.yam_status = 1 ORDER BY p.delivery_option LIMIT 5";	
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$delivery_options[] = $row['delivery_option'];
		}
	}
	_log('(:i:) список delivery_options  (' . count($delivery_options) . ' поз. )');
	//echo '<pre>' . print_r($delivery_options, 1) . '</pre>';
	
	$sql = "SELECT * FROM " . DB_PREFIX . "attribute_description WHERE language_id = '" . LANGUAGE_ID . "' ORDER BY attribute_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$attributes[$row['attribute_id']] = strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'));
		}
	}
	_log('(:i:) список атрибутов  (' . count($attributes) . ' поз. )');
	//echo '<pre>' . print_r($attributes, 1) . '</pre>';
	
	$sql = "SELECT * FROM " . DB_PREFIX . "coupon WHERE ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1' ORDER BY code ASC";
	$res = mysqli_query ($link, $sql);
	if($res){
		while($row=mysqli_fetch_array($res)){
			$promo = array();
			$promo['coupon_id'] = $row['coupon_id'];
			$promo['name'] = $row['name'];
			$promo['code'] = $row['code'];
			$promo['type'] = $row['type'];
			$promo['total'] = $row['total'];
			$promo['discount'] = $row['discount'];
			$promo['date_start'] = date('Y-m-d', strtotime($row['date_start']));
			$promo['date_end'] = date('Y-m-d', strtotime($row['date_end']));
			$promo['status'] = $row['status'];
			$promos[] = $promo;
		}
	}
	_log('(:i:) список промо кодов  (' . count($promos) . ' поз. )');
	
		$sql = "SELECT *, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . LANGUAGE_ID . "' AND p.status = 1 AND p.price > 0 AND p.yam_status = 1 AND p.stock_status_id != " . OUT_OF_SALE_STOCK_STATUS_ID . " ORDER BY p.product_id";
	$res = mysqli_query ($link, $sql);
	if($res){
		$num = 0;
		while($row=mysqli_fetch_array($res)){
			
			$product = array();
			$product['product_id'] = $row['product_id'];
			$product['name'] = $row['name'];
			$product['description'] = $row['description'];
			$product['model'] = $row['model'];
			$product['sku'] = $row['sku'];
			$product['price'] = $row['price'];
			$product['special'] = $row['special'];
			$product['quantity'] = $row['quantity'];
			$product['delivery_option'] = $row['delivery_option'];
			$product['sales_note'] = $row['sales_note'];
			$product['image'] = $row['image'];
			$product['manufacturer'] = $manufacturers[$row['manufacturer_id']];
			
			$products[] = $product;
		}
	}
	_log('(:i:) список товаров  (' . count($products) . ' поз. )');
	//echo '<pre>' . print_r($products, 1) . '</pre>';
	
	$yml .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' . $eol;
	$yml .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . $eol;
	$yml .= '<shop>' . $eol;

	$yml .= '<name>SDM-climate</name>' . $eol;
	$yml .= '<company>SDM-climate</company>' . $eol;
	$yml .= '<url>' . HTTPS_SERVER . '</url>' . $eol;
	$yml .= '<version>fastfeed.v1</version>' . $eol;

	$yml .= '<currencies>' . $eol;
	$yml .= '<currency id="RUB" rate="1" />' . $eol;
	$yml .= '</currencies>' . $eol;

	$yml .= '<categories>' . $eol;
	
	foreach ($categories as $category) {
		$category_id = $category['category_id']; 
		$parent_id = $category['parent_id']; 
		$name = $category['name'];
		if ($parent_id == 0) {
			$yml .= '<category id="' . $category_id . '" >' . $name . '</category>' . $eol;
		} else {
			$yml .= '<category id="' . $category_id . '" parentId="' . $parent_id . '" >' . $name . '</category>' . $eol;
		}
	}
	_log('(:i:) категории выведены во временный файл');
	
	$yml .= '</categories>' . $eol;
	
	$yml .= '<delivery-options>' . $eol;
	
	foreach ($delivery_options as $delivery_option) {
		if ($delivery_option != '') {
			$yml .= html_entity_decode($delivery_option) . $eol;
		}
	}
	
	$yml .= '</delivery-options>' . $eol;
	
	$yml .= '<offers>' . $eol;
	
	if($handle){
		fwrite($handle, $yml);
		fclose($handle);
	}
	
	$num = 0;
	$yml = '';
	foreach ($products as $product) {
		$num++;
		
		$product_categories = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			while($row=mysqli_fetch_array($res)){
				$product_categories[] = $row['category_id'];
			}
		}
		
		$product_attributes = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "product_attribute WHERE language_id = '" . LANGUAGE_ID . "' AND product_id = '" . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			while($row=mysqli_fetch_array($res)){
				$product_attributes[] = array(
					'attribute_id' 	=> $row['attribute_id'], 
					'text' 		=> $row['text']
				);
			}
		}
		
		$alias = false;
		$sql = "SELECT DISTINCT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'product_id=" . $product['product_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			$row = mysqli_fetch_array($res, MYSQLI_NUM);
			$alias = $row[0];
		}
		$url = $alias ? HTTPS_SERVER . $alias : HTTPS_SERVER . 'index.php?route=product/product&amp;product_id=' . $product['product_id'];
		
		$price = number_format($product['price'], 2, '.', '');
		$special = $product['special'] ? number_format($product['special'], 2, '.', '') : false;

		$picture = HTTPS_SERVER . 'image/' . $product['image'];
		
		$productId = $product['product_id'];
		$quantity = $product['quantity'];
		$name = strip_tags($product['name']);
		$model = strip_tags($product['model']);
		
		if ($product['image'] == '' || count($product_attributes) == 0) {
			$bad_products[$product['product_id']] = array(
				'name' 	=> $name, 
				'image' 		=> $picture,
				'attributes' 		=> count($product_attributes)
			);	
		}
		
		
		
		$description = '<![CDATA[' . html_entity_decode($product['description']) . ']]>';
		
		$sales_note = strip_tags($product['sales_note']);
		
		$delivery_option = html_entity_decode($product['delivery_option']);
		if ($delivery_option == '') {
			$delivery_option = DEFAULT_DELIVERY_OPTION;	
		}
		
		$vendor = $product['manufacturer'];
	
		if ($quantity >= 500 && $price >= 5000) {
			$available = 'false';
		} else {
			$available = 'true';
		}
		if ($quantity < 1 || $quantity == '') {
			$available = 'false';
		}
		
		$yml .= '<offer id="' . $productId . '" available="' . $available . '" >' . $eol;
		$yml .= '<url>' . $url . '</url>' . $eol;
		if ($special) {
			$yml .= '<price>' . $special . '</price>' . $eol;
			$yml .= '<oldprice>' . $price . '</oldprice>' . $eol;
		} else {
			$yml .= '<price>' . $price . '</price>' . $eol;
		}
		$yml .= '<currencyId>RUB</currencyId>' . $eol;
		foreach ($product_categories as $product_category) {
			$yml .= '<categoryId>' . $product_category . '</categoryId>' . $eol;
		}
		$yml .= '<picture>' . $picture . '</picture>' . $eol;
		$yml .= '<store>false</store>' . $eol;
		$yml .= '<pickup>false</pickup>' . $eol;
		$yml .= '<delivery>true</delivery>' . $eol;
		$yml .= '<delivery-options>' . $eol;
		$yml .= '' . $delivery_option . $eol;
		$yml .= '</delivery-options>' . $eol;
		$yml .= '<name>' . $name . '</name>' . $eol;
		$yml .= '<vendor>' . $vendor . '</vendor>' . $eol;
		$yml .= '<model>' . $model . '</model>' . $eol;
		$yml .= '<description>' . $description . '</description>' . $eol;
		$yml .= '<sales_notes>' . $sales_note . '</sales_notes>' . $eol;
		$yml .= '<manufacturer_warranty>true</manufacturer_warranty>' . $eol;
	
		foreach ($product_attributes as $product_attribute) {
			$yml .= '<param name="' . str_replace('"','&quot;',$attributes[$product_attribute['attribute_id']]) . '">' . str_replace('"','&quot;',$product_attribute['text']) . '</param>' . $eol;
		}
	
		$yml .= '</offer>' . $eol;
		
		if ($num == BATCH) {
		
			$handle = FALSE;
			$handle = fopen(DIR_TEMP.FEED, "a");
			
			if($handle){
				fwrite($handle, $yml);
				fclose($handle);
			}
			
			$num = 0;
			$yml = '';
		}
	}
	_log('(:i:) товары выведены во временный файл');
	
	$yml .= '</offers>' . $eol;
	$yml .= '<promos>' . $eol;
	
	foreach ($promos as $promo) {
		
		$promo_products = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "coupon_product WHERE coupon_id = '" . (int)$promo['coupon_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			while($row=mysqli_fetch_array($res)){
				$promo_products[] = $row['product_id'];
			}
		}

		$promo_categories = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "coupon_category WHERE coupon_id = '" . (int)$promo['coupon_id'] . "'";
		$res = mysqli_query ($link, $sql);
		if($res){
			while($row=mysqli_fetch_array($res)){
				$promo_categories[] = $row['category_id'];
			}
		}
		
		foreach ($promo_categories as $key => $value) {
			$sql = "SELECT * FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int)$value . "'";
			$res = mysqli_query ($link, $sql);
			if($res){
				while($row=mysqli_fetch_array($res)){
					$promo_products[] = $row['product_id'];
				}
			}
		}
		
		$promo_products = array_unique($promo_products);
		asort($promo_products);
		
		$purchase = array();
		foreach ($promo_products as $key => $value) {
			$k = array_search($value, array_column($products, 'product_id'));
			if (strlen($k) > 0) {
				if (!$products[$k]['special'] && $products[$k]['price'] > $promo['total']) $purchase[] = $value;
			}
		}
		
		if ($purchase) {
			$yml .= '<promo id="' . $promo['coupon_id'] . '" type="promo code">' . $eol;
			$yml .= '<start-date>' . $promo['date_start'] . '</start-date>' . $eol;
			$yml .= '<end-date>' . $promo['date_end'] . '</end-date>' . $eol;
			$yml .= '<description>' . $promo['name'] . '</description>' . $eol;
//			$yml .= '<url>' . 'http://инфо.страница.акции' . '</url>' . $eol;
			$yml .= '<promo-code>' . $promo['code'] . '</promo-code>' . $eol;
			if ($promo['type'] == 'F') {
				$yml .= '<discount unit="currency" currency="RUR">' . number_format($promo['discount'], 2, '.', '') . '</discount>' . $eol;
			} else if ($promo['type'] == 'P') {
				$yml .= '<discount unit="percent">' . number_format($promo['discount'], 2, '.', '') . '</discount>' . $eol;
			}
			$yml .= '<purchase>' . $eol;
			foreach ($purchase as $key => $value) {
				$yml .= '<product offer-id="' . $value . '"/>' . $eol;
			}
			$yml .= '</purchase>' . $eol;
			$yml .= '</promo>' . $eol;
		}
	}
	
	$yml .= '</promos>' . $eol;
	$yml .= '</shop>' . $eol;
	$yml .= '</yml_catalog>';
	
	$handle = FALSE;
	$handle = fopen(DIR_TEMP.FEED, "a");
	
	if($handle){
		fwrite($handle, $yml);
		fclose($handle);
	}
	
	copy(DIR_TEMP.FEED, DIR_TEMP.'temp-feed-'.time().'.xml');
	copy(DIR_TEMP.FEED, DIR_ROOT.FEED);
	unlink(DIR_TEMP.FEED);
	
	$files = glob(DIR_TEMP.'*.xml');
	if ($files) {
		foreach ($files as $file) {
			if (filemtime($file) < time() - TEMP_FEED_TTL * 60) {
				unlink($file);
			}
		}
	}
	
	_log('(:i:) фид записан в файл ' . DIR_ROOT.FEED);
	
	$finish_time = time();
	
	$time = $finish_time - $start_time;
	
	_log('(:!:) всего времени ушло ' . $time . '  сек.');
	
	if($debug){
		
		$help = '';
		$help .= date('Y-m-d H:i') . '<br><br>';
		$help .= 'fastfeed для sdmclimate.ru<br><br>';
		$help .= 'Фид для Яндекс Маркета сформирован:<br><br>';
		$help .= '<a href="' . HTTPS_SERVER . 'yamfeed/' . FEED . '" target="_blank">' . HTTPS_SERVER . 'yamfeed/' . FEED . '</a><br><br>';
		$help .= 'режим отладки ;)<br><br>';
		
		$help .= '"плохие" товары :<br>';
		$help .= '<pre>' . print_r($bad_products,1) . '</pre>';
		
		print($help);
	}
	
	function _log($string) {
//		file_put_contents(DIR_LOGS.LOG, date('Y-m-d H:i') . ' ' . $string . PHP_EOL, FILE_APPEND);
        file_put_contents(DIR_LOGS, date('Y-m-d H:i') . ' ' . $string . PHP_EOL, FILE_APPEND);
	}

?>
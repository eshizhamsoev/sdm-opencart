import {
  initButtonConfirmListener,
  reachPaymentGoal,
} from '@/js/checkout/payment-goal';
import { init } from '@/js/checkout/simple-on-init-trash';

window.reachPaymentGoal = reachPaymentGoal;
window.callbackUpdateSimpleCheckoutPage = () => {
  $(document).ready(() => {
    init();
    initButtonConfirmListener();
  });
};

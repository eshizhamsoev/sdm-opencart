import { Debounce } from '@/js/libs/debounced';

class Cart {
  constructor() {
    this.apiUrl = '/index.php?route=checkout/cart';

    this.cartEl = document.querySelector('.js-cart');

    if (!this.cartEl) {
      return null;
    }

    this.setQuantities();
    this.setDeletes();
    this.debounced = new Debounce();
  }

  update() {
    this.debounced.exec(() => {
      this.cartEl.submit();
    });
  }

  delete(cartId) {
    this.fetch(`/remove&cart_id=${  cartId}`).then(() => {
      location.reload();
    });
  }

  setQuantities() {
    const quantityEls = this.cartEl.querySelectorAll('.js-cart-quantity');

    quantityEls.forEach((quantityEl) => {
      const quantityInputEl = quantityEl.querySelector(
        '.js-cart-quantity-input'
      );
      const increaseButtonEl = quantityEl.querySelector(
        '.js-cart-quantity-increase'
      );
      const degreaseButtonEl = quantityEl.querySelector(
        '.js-cart-quantity-decrease'
      );

      increaseButtonEl.addEventListener('click', () => {
        quantityInputEl.stepUp();
        this.update();
      });
      degreaseButtonEl.addEventListener('click', () => {
        quantityInputEl.stepDown();
        this.update();
      });
      quantityEl.addEventListener('change', () => {
        this.update();
      });
    });
  }

  setDeletes() {
    const deleteBtns = this.cartEl.querySelectorAll('.js-cart-delete');

    deleteBtns.forEach((deleteBtn) => {
      deleteBtn.addEventListener('click', () => {
        this.delete(deleteBtn.dataset.cartId);
      });
    });
  }

  fetch(command) {
    return fetch(this.apiUrl + command).then((response) => {
      if (response.ok) return response.json();
    });
  }
}

export function initCart() {
  return new Cart();
}

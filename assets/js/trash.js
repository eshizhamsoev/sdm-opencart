import Flickity from 'flickity';
import FlickityAsNav from 'flickity-as-nav-for';

document.querySelectorAll('.subcategories__list').forEach((el) => {
  // eslint-disable-next-line no-new
  new Flickity(el, {
    pageDots: false,
    cellAlign: 'left',
  });
});

jQuery(() => {
  // if (window.matchMedia('(max-width: 991px)').matches) {
  //   $('#page').animate({
  //     paddingTop: $('.header').height()
  //   })
  // }

  $('.city-select__toggle').on('click', function () {
    $(this).parent().find('.city-select__dropdown-list').toggle();
  });

  if ($('.promo-banners__carousel').length) {
    const promoBanners = new Flickity('.promo-banners__carousel', {
      cellAlign: 'left',
      wrapAround: true,
      prevNextButtons: false,
      pageDots: true,
      autoPlay: 5000,
      pauseAutoPlayOnHover: false,
      cellSelector: '.promo-banners__promo-banner',
    });

    $('.promo-banners__control--prev').on('click', () => {
      promoBanners.previous(false, false);
    });

    $('.promo-banners__control--next').on('click', () => {
      promoBanners.next(false, false);
    });
  }

  $('.promo-banners__carousel .dot').each(function () {
    $(this).append(
      '<svg height="21" width="21"><circle cx="11" cy="11" r="8" stroke="#428bca" stroke-width="2" fill="none" /></svg>'
    );
  });

  if ($('.brands__brands-carousel').length) {
    const promo_brands = new Flickity('.brands__brands-carousel', {
      cellAlign: 'left',
      contain: true,
      wrapAround: true,
      prevNextButtons: false,
      pageDots: false,
      cellSelector: '.brands__item',
    });

    $('.brands__control--prev').on('click', () => {
      promo_brands.previous(false, false);
    });

    $('.brands__control--next').on('click', () => {
      promo_brands.next(false, false);
    });
  }

  const featured_carousels = document.querySelectorAll(
    '.featured-products__products'
  );

  if (featured_carousels.length) {
    featured_carousels.forEach((el) => {
      const featured = new Flickity(el, {
        cellAlign: 'left',
        contain: true,
        wrapAround: true,
        prevNextButtons: false,
        cellSelector: '.products__product-item',
      });

      $(el)
        .find('.featured-products__control--prev')
        .on('click', () => {
          featured.previous(false, false);
        });

      $(el)
        .find('.featured-products__control--next')
        .on('click', () => {
          featured.next(false, false);
        });

      $('.tabs__link').on('click', (e) => {
        e.preventDefault();

        setTimeout(() => {
          featured.reloadCells();
          featured.resize();
        }, 0); // Без комментариев =/
      });
    });
  }

  if ($('.testimonials__list').length) {
    const certificates = new Flickity('.testimonials__list', {
      cellAlign: 'left',
      contain: true,
      wrapAround: true,
      prevNextButtons: false,
      cellSelector: '.testimonials__item',
    });

    $('.testimonials__control--prev').on('click', () => {
      certificates.previous(false, false);
    });

    $('.testimonials__control--next').on('click', () => {
      certificates.next(false, false);
    });
  }

  if ($('.discounted-products__products').length) {
    const discounted_products = new Flickity('.discounted-products__products', {
      cellAlign: 'left',
      contain: true,
      wrapAround: true,
      prevNextButtons: false,
      cellSelector: '.products__product-item',
    });

    $('.discounted-products__control--prev').on('click', () => {
      discounted_products.previous(false, false);
    });

    $('.discounted-products__control--next').on('click', () => {
      discounted_products.next(false, false);
    });
  }

  if ($('.related-products__carousel').length) {
    const related_products = new Flickity('.related-products__carousel', {
      cellAlign: 'left',
      contain: true,
      wrapAround: true,
      prevNextButtons: false,
      pageDots: false,
      cellSelector: '.related-product',
    });

    $('.related-products__control--prev').on('click', () => {
      related_products.previous(false, false);
    });

    $('.related-products__control--next').on('click', () => {
      related_products.next(false, false);
    });
  }

  $('.bonus-points__control[type="radio"]').on('change', function () {
    const target = $(this).data('toggle');

    $('.bonus-points__pane').removeClass('bonus-points__pane--active');
    $(`.bonus-points__pane[data-pane="${target}"]`).addClass(
      'bonus-points__pane--active'
    );
  });

  $('[data-action="show_more"]').on('click', function (e) {
    e.preventDefault();
    $(this).hide();
    $(this).parent().find('[data-target="show_more"]').slideToggle();
  });

  $('.filters__group-title').on('click', function () {
    $(this).parent().toggleClass('filters__group--expanded');
    $(this).parent().find('.filters__form').slideToggle();
  });

  $('.question-item__toggle').on('click', function () {
    $(this)
      .parents()
      .eq(1)
      .toggleClass('faq-questions__question-item--expanded');
  });

  $('.product-page__specifications-link').on('click', function (e) {
    const target = $(this).attr('href');
    e.preventDefault();
    $('.tabs__pane').hide();
    $(`.tabs__pane${target}`).show();

    $('html, body').animate(
      {
        scrollTop: $(`.tabs__pane${target}`).offset().top,
      },
      1000
    );
  });

  $('.tabs__link:not(.js-tab-button)').on('click', function (e) {
    e.preventDefault();
    const target = $(this).attr('href');
    history.pushState({}, 'Tabs', location.origin + location.pathname + target);
    $(this)
      .parents()
      .eq(1)
      .find('.tabs__link')
      .removeClass('tabs__link--active');
    $(this).addClass('tabs__link--active');
    $(this).parents().eq(3).find('.tabs__pane').hide();
    $(`.tabs__pane${target}`).show();
  });

  $('.faq-tabs__link').on('click', function (e) {
    const target = $(this).attr('href');
    e.preventDefault();
    $('.faq-tabs__link').removeClass('faq-tabs__link--active');
    $(this).addClass('faq-tabs__link--active');
    $('.faq-page__pane').hide();
    $(`.faq-page__pane${target}`).show();
  });

  $('.points-list__pickup-point').on('click', function () {
    $('.points-list__pickup-point--selected').removeClass(
      'points-list__pickup-point--selected'
    );
    $(this).addClass('points-list__pickup-point--selected');
  });

  $('.footer__menu-toggler').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('footer__menu-toggler--expanded');
    $(this).parents().eq(1).find('.footer__footer-menu').slideToggle();
  });

  if ($('.product-gallery').length) {
    new Flickity('.product-gallery__main', {
      wrapAround: false,
      prevNextButtons: false,
      pageDots: false,
      cellSelector: '.product-gallery__img',
    });

    new FlickityAsNav('.product-gallery__nav', {
      asNavFor: '.product-gallery__main',
      wrapAround: false,
      pageDots: false,
      contain: true,
      draggable: false,
      adaptiveHeight: true,
    });
  }

  /* $('.header__toggler').on('click', function (e) {
    e.preventDefault()
    $('.header__header-menu').toggle()
    $('html').toggleClass('locked')
  })

  $('.header-menu__close').on('click', function (e) {
    e.preventDefault()
    $('.header__header-menu').toggle()
    $('html').toggleClass('locked')
  }) */

  if (window.matchMedia('(max-width: 991px)').matches) {
    if ($('.company-advantages__list').length) {
      const company_advantages = new Flickity('.company-advantages__list', {
        wrapAround: true,
        prevNextButtons: false,
        pageDots: true,
        cellAlign: 'left',
      });

      $('.company-advantages__control--prev').on('click', () => {
        company_advantages.previous(false, false);
      });

      $('.company-advantages__control--next').on('click', () => {
        company_advantages.next(false, false);
      });
    }

    if ($('.subcategories__list').length) {
      document.querySelectorAll('.subcategories__list').forEach((listEl) => {
        new Flickity(listEl, {
          wrapAround: true,
          prevNextButtons: false,
          pageDots: false,
          cellAlign: 'left',
        });
      });
    }

    if ($('.discount-conditions__list').length) {
      new Flickity('.discount-conditions__list', {
        wrapAround: true,
        prevNextButtons: false,
        pageDots: true,
        cellAlign: 'left',
      });
    }

    if ($('.benefits__list').length) {
      const benefits_carousel = new Flickity('.benefits__list', {
        wrapAround: true,
        prevNextButtons: false,
        pageDots: true,
        cellAlign: 'left',
      });

      $('.benefits__control--prev').on('click', () => {
        benefits_carousel.previous(false, false);
      });

      $('.benefits__control--next').on('click', () => {
        benefits_carousel.next(false, false);
      });
    }

    if ($('.company-rules__list').length) {
      const company_rules = new Flickity('.company-rules__list', {
        wrapAround: true,
        prevNextButtons: false,
        pageDots: true,
        cellAlign: 'left',
      });

      $('.company-rules__control--prev').on('click', () => {
        company_rules.previous(false, false);
      });

      $('.company-rules__control--next').on('click', () => {
        company_rules.next(false, false);
      });
    }


    $('[data-toggle="orderby"]').on('click', (e) => {
      e.preventDefault();
      $('.catalogue__sort-by').toggleClass('catalogue__sort-by--expanded');
    });

    $('.sidebar__close').on('click', (e) => {
      e.preventDefault();
      $('.catalogue__sidebar').toggleClass('catalogue__sidebar--expanded');
      $('html').toggleClass('locked');
    });

    $('body').on('click', (e) => {
      if ($(e.target).closest('.catalogue__sort-by--expanded').length) return;
      if (e.target.dataset.toggle == 'orderby') return;

      $('.catalogue__sort-by').removeClass('catalogue__sort-by--expanded');
    });
  }

  /* $('a:not(.tabs__link)').on('click', function (event) {
    if (this.hash !== '') {
      event.preventDefault()
      const hash = this.hash
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800)
    }
  }) */

  function init_comparison_table(columns = 4) {
    $('.comparison-table__wrap').each(function () {
      const compare_columns = columns;
      const product_el = $(this).find('.comparison-table__product');
      const table_width = $(this).find('.comparison-table__products').width();
      const comparison_items_count = product_el.length;

      product_el.css({
        width: table_width / compare_columns,
        minWidth: table_width / compare_columns,
      });

      const item_width = product_el.width();

      $(this)
        .find('.comparison-table__products-list')
        .width(comparison_items_count * item_width);

      const item_header_height = $(this)
        .find('.comparison-table__product-header')
        .height();

      $(this).find('.comparison-table__actions').height(item_header_height);
    });
  }

  if (window.matchMedia('(min-width: 1300px)').matches) {
    init_comparison_table(4);
  } else if (
    window.matchMedia('(max-width: 1300px) and (min-width: 991px)').matches
  ) {
    init_comparison_table(2.5);
  } else if (
    window.matchMedia('(max-width: 991px) and (min-width: 768px)').matches
  ) {
    init_comparison_table(2);
  } else if (window.matchMedia('(max-width: 768px)').matches) {
    init_comparison_table(1.6);
  }

  $('.comparison-table__tabs .tabs__link').on('click', () => {
    if (window.matchMedia('(min-width: 1300px)').matches) {
      init_comparison_table(4);
    } else if (
      window.matchMedia('(max-width: 1300px) and (min-width: 991px)').matches
    ) {
      init_comparison_table(2.5);
    } else if (
      window.matchMedia('(max-width: 991px) and (min-width: 768px)').matches
    ) {
      init_comparison_table(2);
    } else if (window.matchMedia('(max-width: 768px)').matches) {
      init_comparison_table(1.6);
    }
  });
});

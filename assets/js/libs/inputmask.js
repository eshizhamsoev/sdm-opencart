import Inputmask from 'inputmask';

const inputMask = new Inputmask('8 (999) 999-99-99');

export function initInputMask(selector) {
  let inputEl;

  if (typeof selector === 'string') {
    inputEl = document.querySelector(selector);
  } else {
    inputEl = selector;
  }

  if (!inputEl) return;

  inputMask.mask(inputEl);

  inputEl.addEventListener('keyup', () => {
    const sliced = inputEl.value.slice(0, 4);

    if (sliced === '8 (8' || sliced === '8 (7') {
      inputEl.value = '';
    }
  });
}

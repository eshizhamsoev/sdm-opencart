import Mmenu from 'mmenu-js';

export function initMmenu() {
  const element = document
    .querySelector('[data-template-item]')
    .content.cloneNode(true);
  new Mmenu(
    '#mmenu',
    {
      extensions: ['pagedim-black'],
      dropdown: false, // fixed header disappear
      navbars: [
        {
          position: 'top',
          content: [element],
        },
        {
          position: 'bottom',
          content: [
            `<!--<div class="header-menu__city-select city-select">
                        <div class="city-select__dropdown">
                            <button class="city-select__toggle">
                                Москва
                                <svg xmlns="http://www.w3.org/2000/svg" width="9" height="5" viewBox="0 0 9 5">
                                    <path
                                            d="M8.8 0.9v0l-3.5 3.3v0l0 0v0l-0.9 0.8v0l-4.4-4.1v0l0.9-0.8v0l3.5 3.3v0l3.5-3.3v0z"
                                            fill="#003cff"/>
                                    <path
                                            d="M8.8 0.9v0l-3.5 3.3v0l0 0v0l-0.9 0.8v0l-4.4-4.1v0l0.9-0.8v0l3.5 3.3v0l3.5-3.3v0z"
                                            fill="#144da4"/>
                                </svg>
                            </button>
                            <ul class="city-select__dropdown-list">
                                <li class="city-select__dropdown-item">
                                    <a class="city-select__dropdown-link" href="#">Москва</a>
                                </li>
                                <li class="city-select__dropdown-item">
                                    <a class="city-select__dropdown-link" href="#">Москва</a>
                                </li>
                                <li class="city-select__dropdown-item">
                                    <a class="city-select__dropdown-link" href="#">Москва</a>
                                </li>
                            </ul>
                        </div>

                        <div class="header__pickup">Пункты выдачи<span class="header__pickup-counter">17</span></div>
                    </div>-->

                    <a href="/index.php?route=account/account" class="header-menu__account-link">
                        <svg width="15" height="14" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 15 14">
                            <path
                                    d="M14.19213,10.72387v1.12235c0,0.94541 -0.76115,1.71087 -1.70063,1.71087h-10.20323c-0.93966,0 -1.70063,-0.76546 -1.70063,-1.71087v-1.12648c0,-1.22707 1.84755,-2.27954 4.43933,-2.68615c-0.63959,-0.62167 -1.03834,-1.49357 -1.03834,-2.45921v-1.71014c0,-1.88982 1.52283,-3.42151 3.40126,-3.42151c1.87828,0 3.40111,1.53169 3.40111,3.42151v1.71014c0,0.96581 -0.39862,1.83788 -1.0387,2.45966c2.59203,0.40739 4.43983,1.46309 4.43983,2.68982zM7.3899,7.85515c1.25235,0 2.26738,-1.02115 2.26738,-2.28076v-1.71014c0,-1.2599 -1.01503,-2.28082 -2.26738,-2.28082c-1.25223,0 -2.2674,1.02092 -2.2674,2.28082v1.71014c0,1.25962 1.01517,2.28076 2.2674,2.28076zM13.05841,10.72387c0,-0.13314 -0.3269,-0.56304 -1.29787,-0.97372c-1.13457,-0.47962 -2.72778,-0.75459 -4.37063,-0.75459c-1.64391,0 -3.23736,0.27436 -4.37153,0.75275c-0.97048,0.40928 -1.297,0.83857 -1.297,0.97144v1.12648c0,0.31465 0.25415,0.57057 0.5669,0.57057h10.20323c0.31263,0 0.5669,-0.25591 0.5669,-0.57057z"
                                    fill="#000000" fill-opacity="1"></path>
                        </svg>
                        Личный кабинет
                    </a>
                </div>`,
          ],
        },
      ],
    },
    {
      offCanvas: {
        page: {
          selector: '#page',
        },
      },
      language: 'ru',
    }
  );
}

export function initDummyTabs() {
  const contentEls = document.querySelectorAll('.js-tab-content');

  const tabBtns = document.querySelectorAll('.js-tab-button');

  tabBtns.forEach((tabBtn) => {
    tabBtn.addEventListener('click', () => {
      contentEls.forEach((contentEl) => {
        contentEl.style.display = 'none';
      });

      document.querySelector(
        `.js-tab-content[data-tab-id="${  tabBtn.dataset.tabId  }"]`
      ).style.display = 'block';

      tabBtns.forEach((tabBtnEl) => {
        tabBtnEl.classList.remove('active');
      });

      tabBtn.classList.add('active');
    });
  });
}

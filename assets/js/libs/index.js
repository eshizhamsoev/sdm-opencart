import './svg-sprite';
import { initMmenu } from '@/js/libs/mmenu';
import { initDummyTabs } from '@/js/libs/dummy-tabs';

initMmenu();
initDummyTabs();

export class Debounce {
  constructor(delay = 300) {
    this.delay = delay;
    this.timerId = null;
  }

  exec(callback) {
    if (this.timerId !== null) clearTimeout(this.timerId);

    this.timerId = setTimeout(() => {
      callback();
      this.timerId = null;
    }, this.delay);
  }
}

import { setDefaultFormHandler } from '@/js/call-me-back/default';

export function handleChoseForms() {
  setDefaultFormHandler('chose');
}

export function sendCallMeBack(body, callback = () => {}) {
  const date = new Date();

  body.append(
    'time',
    `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
  );
  body.append('url_page', location.href);

  const xhr = new XMLHttpRequest();

  xhr.open(
    'post',
    '/index.php?route=octemplates/module/oct_popup_call_phone/send'
  );

  xhr.setRequestHeader(
    'accept',
    'application/json, text/javascript, */*; q=0.01'
  );
  xhr.setRequestHeader('cache-control', 'no-cache');
  // xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
  xhr.setRequestHeader('pragma', 'no-cache');
  xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');

  xhr.send(body);

  xhr.onload = () => callback(xhr.response);
}

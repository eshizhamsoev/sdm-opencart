import { setDefaultFormHandler } from '@/js/call-me-back/default';

export function handlePartnershipForm() {
  setDefaultFormHandler('partnership');
}

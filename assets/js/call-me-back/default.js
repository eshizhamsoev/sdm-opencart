import { sendCallMeBack } from '@/js/call-me-back/index';

export function setDefaultFormHandler(containerName) {
  const formEls = document.querySelectorAll(`.js-${  containerName}`);

  if (!formEls.length) {
    return;
  }

  formEls.forEach((formEl) => {
    const name = formEl.querySelector(`.js-${  containerName  }-name`);
    const telephone = formEl.querySelector(
      `.js-${  containerName  }-telephone`
    );

    let comment = formEl.querySelector(`.js-${  containerName  }-comment`);
    if (!comment) {
      comment = {
        value: '',
      };
    }

    let type = formEl.querySelector(`.js-${  containerName  }-type`);
    if (!type) {
      type = {
        value: '1',
      };
    }

    const btn = formEl.querySelector(`.js-${  containerName  }-btn`);

    btn.addEventListener('click', () => {
      const body = new FormData();

      body.append('name', name.value);
      body.append('telephone', telephone.value);
      body.append('comment', comment.value);
      body.append('type', type.value);
      body.append('agree', 'on');

      btn.setAttribute('disabled', 'true');

      sendCallMeBack(body, (result) => {
        btn.removeAttribute('disabled');

        const json = JSON.parse(result);

        if (json.error) {
          $('#us-callback-modal .text-danger').remove();
          let errorOption = '';

          $.each(json.error, (i, val) => {
            errorOption += `<div class="alert-text-item">${  val  }</div>`;
          });

          usNotify('danger', errorOption);
        } else if (json.output) {
            usNotify('success', json.output);
            name.value = '';
            telephone.value = '';
            comment.value = '';
          }
      });
    });
  });
}

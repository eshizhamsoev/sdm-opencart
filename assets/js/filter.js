class Filter {

  open() {
    $('html').addClass('locked');
    document.querySelector('.catalogue__sidebar').classList.add('catalogue__sidebar--expanded');
  }

  close() {
    $('html').removeClass('locked');
    document.querySelector('.catalogue__sidebar').classList.remove('catalogue__sidebar--expanded');
  }

  isOpened() {
    return document.querySelector('.catalogue__sidebar').classList.contains('catalogue__sidebar--expanded');
  }
}

export function initFilter(options) {
  const isMobile = $('.ocfilter-mobile').is(':visible')
  if (isMobile) {
    $('.ocf-offcanvas-body').html($('#ocfilter').remove().get(0).outerHTML)
  }


  const filter = new Filter()

  $('#page').append($('.ocfilter-mobile').remove().get(0).outerHTML)


  document.addEventListener('click', (e) => {
    if (filter.target.contains(e.target)) {
      filter.close()
    }
  }, {capture: true})

  $('[data-toggle="filters"]').on('click', (e) => {
    e.preventDefault();

    if (filter.isOpened()) {
      filter.close()
    } else {
      filter.open()
    }
  });


  setTimeout(() => {
    $('#ocfilter').ocfilter({...options, mobile: isMobile})
  }, 1)
}

export function initReview() {
  const form = $('#form-review');
  const productId = form.data('product-id');
  form.on('submit', (e) => {
    e.preventDefault();
    $.ajax({
      url: `/index.php?route=product/product/write&product_id=${productId}`,
      type: 'post',
      dataType: 'json',
      data: form.serialize(),
      beforeSend() {
        $('#button-review').button('loading');
      },
      complete() {
        $('#button-review').button('reset');
      },
      success(json) {
        $('.alert-dismissible').remove();
        if (json.error) {
          const errorText = Object.values(json.error).join(', ');
          form.after(
            `<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ${errorText}</div>`
          );
        }

        if (json.success) {
          form.after(
            `<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ${json.success}</div>`
          );

          form.find("input[name='name']").val('');
          form.find("textarea[name='text']").val('');
          form.find("input[name='rating']:checked").prop('checked', false);
        }
      },
    });
  });
}

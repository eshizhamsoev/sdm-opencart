export function reachPaymentGoal() {
  if (window.ym) {
    window.ym('43808394', 'reachGoal', 'order_step_payment');
  }
}

export function initButtonConfirmListener() {
  const buttonConfirm = document.querySelector('[data-payment-button]');
  if (buttonConfirm) {
    buttonConfirm.addEventListener('click', reachPaymentGoal);
  }
}

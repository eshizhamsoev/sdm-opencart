export function handleSearchForms() {
  const searchEl = document.querySelector('.js-search');
  const btnEl = searchEl.querySelector('.js-search-button');
  const inputEl = searchEl.querySelector('.js-search-value');

  function search() {
    const url = `${searchEl.dataset.searchLink  }&search=${  inputEl.value}`;

    location.assign(url);
  }

  btnEl.addEventListener('click', search);
  inputEl.addEventListener('keyup', (e) => {
    if (e.key === 'Enter') {
      search();
    }
  });

  document
    .querySelectorAll('.js-header-search-mobile-button')
    .forEach((mobileButton) => {
      mobileButton.addEventListener('click', () => {
        searchEl.classList.toggle('active');
      });
    });
}

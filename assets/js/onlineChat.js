const button = document.querySelector('.messengers__link_type_online-chat');
if (button) {
  button.addEventListener('click', (event) => {
    event.preventDefault();
    const clickEvent = new Event('click', { bubbles: true });
    const widget = document.querySelector(
      '.b24-widget-button-openline_livechat span'
    );
    widget.dispatchEvent(clickEvent);
  });
}

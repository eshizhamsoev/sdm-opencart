export function updateHeader({ position, quantity, total = null }) {
  const positionClassName = `.js-header-${  position}`;
  const positionEl = document.querySelector(positionClassName);

  if (quantity > 0) {
    positionEl.classList.remove('empty');
  } else {
    positionEl.classList.add('empty');
  }

  const quantityEl = positionEl.querySelector(`${positionClassName  }-quantity`);
  quantityEl.textContent = quantity;

  const totalEl = positionEl.querySelector(`${positionClassName  }-total`);
  if (totalEl && total !== null) {
    totalEl.textContent = total;
  }
}

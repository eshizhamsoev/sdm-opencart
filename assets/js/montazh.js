import Swiper, { Navigation, Pagination } from 'swiper';

const burger = document.querySelector('.js-burger');
if (burger) {
  const menu = document.querySelector('.js-burger-menu');
  burger.addEventListener('click', () => {
    menu.classList.toggle('active');
  });
  document.querySelector('body').addEventListener('click', (e) => {
    if (!e.target.closest('.js-burger')) {
      menu.classList.remove('active');
    }
  });
}

const tabs = document.querySelector('.js-tabs');
if (tabs) {
  const btns = tabs.querySelectorAll('.js-tab');
  const contents = tabs.querySelectorAll('.js-content');
  btns.forEach((el) => {
    el.addEventListener('click', () => {
      const name = el.dataset.tab;
      const content = tabs.querySelector(`.js-content[data-tab=${name}]`);
      for (const item of btns) {
        item.classList.remove('active');
      }
      for (const item of contents) {
        item.classList.remove('active');
      }
      content.classList.add('active');
      el.classList.add('active');
    });
  });
}

new Swiper('.examples-montazh__swiper', {
  modules: [Pagination, Navigation],
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.examples-montazh__btn_next',
    prevEl: '.examples-montazh__btn_prev',
  },
  spaceBetween: 20,
});

import Swiper, { Navigation, Pagination, Autoplay } from 'swiper';

new Swiper('.main-carousel__swiper', {
  modules: [Navigation, Pagination, Autoplay],
  loop: true,
  navigation: {
    nextEl: '.main-carousel__arrow_next',
    prevEl: '.main-carousel__arrow_prev',
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
  },
});

const goals = {
  // Быстрый заказ 1 клик
  quickOrder: {
    yandex: 'order_quick_submit',
    google: {
      category: 'order',
      action: 'quick',
    },
  },

  // Заказ обратного звонка
  callback: {
    yandex: 'submit_zvonok',
    google: {
      category: 'form',
      action: 'feedback',
    },
  },

  // Клик на номеру телефона
  phoneNumber: {
    yandex: 'click_po_nomeru',
    google: {
      category: 'click',
      action: 'phone',
    },
  },

  // Клик по info@sdmclimate.ru
  mailInfo: {
    yandex: 'click_info',
    google: {
      category: 'click',
      action: 'info',
    },
  },

  // Клик по order@sdmclimate.ru
  mailOrder: {
    yandex: 'click_order',
    google: {
      category: 'click',
      action: 'order',
    },
  },

  // Переход в социальные сети
  clickSocial: {
    yandex: 'click_social',
    google: {
      category: 'click',
      action: 'social',
    },
  },

  //  Покупка через корзину
  purchase: {
    yandex: 'click_buy',
    google: {
      category: 'order',
      action: 'cart',
    },
  },
};

export function reachGoal(goalName) {
  const goal = goals[goalName];
  if (!goal) {
    console.error(`Неизвестная цель ${goalName}`);
    return;
  }
  try {
    window.ga.getAll().forEach((item) => {
      item.send('event', goal.google.category, goal.google.action, {
        useBeacon: true,
      });
    });
  } catch (e) {
    console.error('Ошибка отправки цели в ga');
  }
  try {
    window.ym(43808394, 'reachGoal', goal.ya);
  } catch (e) {
    console.error('Ошибка отправки цели в ym');
  }
}

export function initGoals() {
  $(document).ready(() => {
    $('a[href^="tel:"]').click(() => {
      reachGoal('phoneNumber');
    });
    $('a[href^="mailto:order@sdmclimate.ru"]').click(() => {
      reachGoal('mailOrder');
    });
    $('a[href^="mailto:info@sdmclimate.ru"]').click(() => {
      reachGoal('mailInfo');
    });
    $('.socials a').click(() => {
      reachGoal('clickSocial');
    });
    $(document).on('click', '#simplecheckout_button_confirm', () => {
      reachGoal('purchase');
    });
  });
}

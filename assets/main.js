import '@/js/trash';
import '@/js/libs';
import '@/js/montazh';
import '@/js/swiper';
import '@/js/onlineChat';
import { initReview } from '@/js/review';

import { handleSearchForms } from '@/js/search-form';
import { handlePartnershipForm } from '@/js/call-me-back/partnership';

import { initCart } from '@/js/cart';
import { handleChoseForms } from '@/js/call-me-back/chose';

handleSearchForms();
handlePartnershipForm();
handleChoseForms();

initCart();
initReview();

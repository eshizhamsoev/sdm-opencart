import '@/js/libs/modernizr-webp.min';
import { updateHeader } from '@/js/header/update';
import { initInputMask } from '@/js/libs/inputmask';
import { initGoals, reachGoal } from '@/js/goals';

window.updateHeader = updateHeader;
window.initInputMask = initInputMask;
// window.sendCallMeBack = sendCallMeBack

initGoals();

window.reachGoal = reachGoal;
